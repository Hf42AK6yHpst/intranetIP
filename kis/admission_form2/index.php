<?php
//Using: Pun
/**
 * Change Log:
 * 2018-09-03 Pun
 *  - File created
 */
$PATH_WRT_ROOT = "../../";

include_once("{$PATH_WRT_ROOT}includes/global.php");
include_once("{$PATH_WRT_ROOT}includes/libdb.php");
include_once("{$PATH_WRT_ROOT}includes/libinterface.php");
include_once("{$PATH_WRT_ROOT}includes/kis/libkis.php");
include_once("{$PATH_WRT_ROOT}includes/kis/libkis_ui.php");
include_once("{$PATH_WRT_ROOT}includes/kis/libkis_utility.php");
include_once("{$PATH_WRT_ROOT}includes/kis/libkis_apps.php");
// include_once($PATH_WRT_ROOT."includes/json.php");

include_once("../config.php");


// for the customization
include_once("{$PATH_WRT_ROOT}includes/admission/{$setting_path_ip_rel}/config.php");
$intranet_session_language = $admission_cfg['DefaultLang'];
include_once("{$PATH_WRT_ROOT}includes/admission/{$setting_path_ip_rel}/libadmission_cust.php");
include_once("{$PATH_WRT_ROOT}includes/admission/{$setting_path_ip_rel}/libadmission_ui_cust.php");

intranet_opendb();

// ### Lang START ####
if ($intranet_session_language == 'en') {
    if ($admission_cfg['IsBilingual']) {
        $intranet_session_language = 'b5';
        include("{$PATH_WRT_ROOT}lang/lang.{$intranet_session_language}.php");
        include("{$PATH_WRT_ROOT}lang/kis/lang_common_{$intranet_session_language}.php");
        include("{$PATH_WRT_ROOT}lang/kis/apps/lang_admission_{$intranet_session_language}.php");
        include("{$PATH_WRT_ROOT}lang/admission_lang.{$intranet_session_language}.php");
        $LangB5 = $Lang;
        $kis_lang_b5 = $kis_lang;
        unset($Lang);
        unset($kis_lang);
    }

    $intranet_session_language = 'en';
    include("{$PATH_WRT_ROOT}lang/lang.{$intranet_session_language}.php");
    include("{$PATH_WRT_ROOT}lang/kis/lang_common_{$intranet_session_language}.php");
    include("{$PATH_WRT_ROOT}lang/kis/apps/lang_admission_{$intranet_session_language}.php");
    include("{$PATH_WRT_ROOT}lang/admission_lang.{$intranet_session_language}.php");
    $LangEn = $Lang;
    $kis_lang_en = $kis_lang;
} else {
    if ($admission_cfg['IsBilingual']) {
        $intranet_session_language = 'en';
        include("{$PATH_WRT_ROOT}lang/lang.{$intranet_session_language}.php");
        include("{$PATH_WRT_ROOT}lang/kis/lang_common_{$intranet_session_language}.php");
        include("{$PATH_WRT_ROOT}lang/kis/apps/lang_admission_{$intranet_session_language}.php");
        include("{$PATH_WRT_ROOT}lang/admission_lang.{$intranet_session_language}.php");
        $LangEn = $Lang;
        $kis_lang_en = $kis_lang;
        unset($Lang);
        unset($kis_lang);
    }

    $intranet_session_language = 'b5';
    include("{$PATH_WRT_ROOT}lang/lang.{$intranet_session_language}.php");
    include("{$PATH_WRT_ROOT}lang/kis/lang_common_{$intranet_session_language}.php");
    include("{$PATH_WRT_ROOT}lang/kis/apps/lang_admission_{$intranet_session_language}.php");
    include("{$PATH_WRT_ROOT}lang/admission_lang.{$intranet_session_language}.php");
    $LangB5 = $Lang;
    $kis_lang_b5 = $kis_lang;
}
// ### Lang END ####

$libkis = new kis('');
$lac = new admission_cust();
$lauc = new admission_ui_cust();
$li = new interface_html();

if (!$plugin['eAdmission']) {
    include_once("{$PATH_WRT_ROOT}includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT("", "../");
    exit();
}

$_SESSION['SSV_PRIVILEGE']['school']['name'] = GET_SCHOOL_NAME();
$school = $libkis->getUserSchool();
$next_school_year_basic_settings = $lac->getBasicSettings('',array('generalInstruction'));
$basic_settings = $lac->getBasicSettings('99999');

#### Get next school year name START ####
$academicYearB5 = getAcademicYearByAcademicYearID($lac->schoolYearID, 'b5');
$academicYearEn = getAcademicYearByAcademicYearID($lac->schoolYearID, 'en');
#### Get next school year name END ####


$class_level = $lac->getClassLevel();
$application_setting = $lac->getApplicationSetting();

#### Get choose class input START ####
$avaliableClasses = array();
if ($application_setting) {
    $hasClassLevelApply = 0;

    foreach ($application_setting as $key => $value) {
        if ($lac->isInternalUse($_GET['token']) && $value['AllowInternalUse'] && $value['SchoolYearID'] == $lac->schoolYearID || date('Y-m-d H:i') >= $value['StartDate'] && date('Y-m-d H:i') <= $value['EndDate'] || ($sys_custom['KIS_Admission']['PreviewFormMode'] && date('Y-m-d H:i') >= $value['PreviewStartDate'] && date('Y-m-d H:i') <= $value['PreviewEndDate'])) {
            $hasClassLevelApply = 1;
            $numOFQuotaLeft = $lac->NumOfQuotaLeft($key, $value['SchoolYearID']);

            $avaliableClasses[] = array(
                'id' => $key,
                'className' => $value['ClassLevelName'],
                'quotaLeft' => $numOFQuotaLeft,
            );
        }
    }
}
#### Get choose class input END ####


$allowToPreview = $sys_custom['KIS_Admission']['PreviewFormMode'] && $lac->IsPreviewPeriod();
$isInternalUse = $lac->isInternalUse($_GET['token']);

#### Check isSubmitted START ####
$isValidForAdmission = true;
$isSubmitted = false;
if (
    ($_SESSION["platform"] != "KIS" || !$_SESSION["UserID"] || !$_SESSION["SSV_USER_ACCESS"]["eAdmin-eAdmission"] || $_GET['token']) &&
    $sys_custom['KIS_Admission']['IntegratedCentralServer'] &&
    !$allowToPreview
) {
    $ch = curl_init();

    curl_setopt($ch, CURLOPT_HEADER, 0);

    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

    // set URL and other appropriate options

    // curl_setopt($ch, CURLOPT_URL, "http://192.168.0.146:31002/test/queue/reserved.php?token=f590bac6181c28fda4ec6812e7f2ae4f92&FromKIS=1");
    // # the host name should be dynamic
    $formCode = $_REQUEST['sus_status'];
    curl_setopt($ch, CURLOPT_URL, $admission_cfg['IntegratedCentralServer'] . "reserved.php?token=" . $_GET['token'] . "&FromKIS=1");

    // grab URL and pass it to the browser

    $data_from_checking = curl_exec($ch);

    $data_collected = unserialize($data_from_checking);


    if (
        (!$data_collected['AllowApplyNow'] || $data_collected['ErrorCode'] != '' || $lac->hasToken($_GET['token']) > 0) &&
        ($_SESSION["platform"] != "KIS" || !$_SESSION["UserID"] || !$_SESSION["SSV_USER_ACCESS"]["eAdmin-eAdmission"] || $_GET['token'])
    ) {
        $isValidForAdmission = false;

        if ($_GET['token'] && $lac->hasToken($_GET['token']) > 0) {
            $isSubmitted = true;
        }
    }
} else {
    $data_collected['ApplyFor'] = '';
    $data_collected['IDNumber'] = '';
}
#### Check isSubmitted END ####

###################################### UI START ######################################
$isIndex = true;
include(__DIR__ . '/header.php');
?>

    <div id="blkWelcomeUpper">
        <div>
            <img id="imgSchLogo" src="<?= is_file('/file/kis/admission/images/logo.png') ? '/file/kis/admission/images/logo.png' : $school['logo'] ?>">
            <span>
			<?php
            if ($sys_custom['KIS_Admission']['JOYFUL']['Settings']):
                if ($basic_settings['schoolnamechi']?$basic_settings['schoolnamechi']:$admission_cfg['SchoolName']['b5']):
                    ?>
                    <div>
        				<span id="lblSchName-chi"><?= $basic_settings['schoolnamechi']?$basic_settings['schoolnamechi']:$admission_cfg['SchoolName']['b5'] ?></span>
        			</div>
                <?php
                endif;
                if ($basic_settings['schoolnameeng']?$basic_settings['schoolnameeng']:$admission_cfg['SchoolName']['en']):
                    ?>
                    <div>
        				<span id="lblSchName-eng"><?= $basic_settings['schoolnameeng']?$basic_settings['schoolnameeng']:$admission_cfg['SchoolName']['en'] ?></span>
        			</div>
                <?php
                endif;
            elseif ($admission_cfg['SchoolName']):
                if ($basic_settings['schoolnamechi']?$basic_settings['schoolnamechi']:$admission_cfg['SchoolName']['b5']):
                    ?>
                    <div>
        				<span id="lblSchName-chi"><?= $basic_settings['schoolnamechi']?$basic_settings['schoolnamechi']:$admission_cfg['SchoolName']['b5'] ?></span>&nbsp;<span><?=$admission_cfg['AdmissionSystemTitle']['b5']?$admission_cfg['AdmissionSystemTitle']['b5']:'網上收生系統'?></span>
        			</div>
                <?php
                endif;
                if ($basic_settings['schoolnameeng']?$basic_settings['schoolnameeng']:$admission_cfg['SchoolName']['en']):
                    ?>
                    <div>
        				<span id="lblSchName-eng"><?= $basic_settings['schoolnameeng']?$basic_settings['schoolnameeng']:$admission_cfg['SchoolName']['en'] ?></span>&nbsp;<span><?=$admission_cfg['AdmissionSystemTitle']['en']?$admission_cfg['AdmissionSystemTitle']['en']:'eAdmission System'?></span>
        			</div>
                <?php
                endif;
            else:
                ?>
                <div>
    				<span id="lblSchName-chi"><?= GET_SCHOOL_NAME() ?></span>&nbsp;<span>網上收生系統</span>
    			</div>
            <?php
            endif;
            ?>
		</span>
        </div>
		<?php
        if ($sys_custom['KIS_Admission']['JOYFUL']['Settings']):
        ?>
        	<div>
            <span><?=$admission_cfg['AdmissionSystemTitle']['b5']?$admission_cfg['AdmissionSystemTitle']['b5']:'網上收生系統'?></span>
            &nbsp;<span><?=$admission_cfg['AdmissionSystemTitle']['en']?$admission_cfg['AdmissionSystemTitle']['en']:'eAdmission System'?></span>
    		</div>
    	<?php
        endif;
        ?>
    </div>

<?php if ($data_collected['ErrorMsg'] == 'access denied'): ?>
    <article id="blkWelcome">
        <section class="form sheet" id="blkWelcomeMsg">
            <div class="item">
                <span>
                    <?=$lauc->getLangStr(array(
                        'b5' => '這個網頁無法使用，請重新嘗試。',
                        'en' => 'You cannot access this page directly. Please try again!',
                    ), '<br/>')?>
                </span>
            </div>
        </section>
    </article>

<?php else: ?>

    <form action="form.php" method="POST">
        <input type="hidden" name="token" value="<?= $_GET['token'] ?>"/>
        <input type="hidden" name="BirthCertNo" value="<?= $data_collected['IDNumber'] ?>"/>
        <article id="blkWelcome">
            <div id="lblWelcomeTitle">
	            <?=$lauc->getLangStr(array(
		            'b5' => '<span>'.str_replace('<!--SchoolYear-->', $academicYearB5, $LangB5['Admission']['applicationtitle2']) . ' '. ($isInternalUse ? '<font color="red">(校方專用)</font>' : '') . '</span>',
		            'en' => '<span>'.str_replace('<!--SchoolYear-->', $academicYearEn, $LangEn['Admission']['applicationtitle2']) . ' '. ($isInternalUse ? '<font color="red">(Internal Use)</font>' : '') . '</span>',
	            ))?>
            </div>

            <section class="form sheet" id="blkWelcomeMsg">
                <?php
                if ($lac->schoolYearID) {
                    echo $next_school_year_basic_settings['generalInstruction'];
                } else {
                    ?>
                    <fieldset class="warning_box">
                        <legend><?= $Lang['Admission']['warning'] ?></legend>
                        <ul>
                            <li><?= $Lang['Admission']['msg']['noclasslevelapply'] ?></li>
                        </ul>
                    </fieldset>
                    <?php
                }
                ?>
            </section>

            <?php if ($isSubmitted && $sys_custom['KIS_Admission']['IntegratedCentralServer']): ?>
                <section class="form sheet">
                    <div class="item">
                        <span>
                            <?=$lauc->getLangStr(array(
                                'b5' => '閣下已遞交網上申請表，申請通知電郵已發送，請檢查閣下在申請表填寫的電郵。',
                                'en' => 'You have applied the admission! Please check your Email to get the admission Notification!',
                            ), '<br/>')?>
                        </span>
                    </div>
                </section>
            <?php else: ?>
                <section class="form sheet">
                    <div class="item">
                        <div class="itemLabel requiredLabel">
	                        <?=$lauc->getLangStr(array(
		                        'b5' => ($sys_custom['KIS_Admission']['JOYFUL']['Settings']?$LangB5['Admission']['levelApplyFor']:$LangB5['Admission']['applyLevel']),
		                        'en' => ($sys_custom['KIS_Admission']['JOYFUL']['Settings']?$LangEn['Admission']['levelApplyFor']:$LangEn['Admission']['applyLevel']),
	                        ))?>
                        </div>
                        <span class="itemInput itemInput-choice">

        				<?php
                        if ($avaliableClasses):
                            foreach ($avaliableClasses as $index => $avaliableClass):
                                if ($data_collected['ApplyFor']) {
                                    $checked = ($avaliableClass['id'] == $data_collected['ApplyFor']) ? 'checked' : '';
                                } else {
                                    $checked = ($index == 0) ? 'checked' : '';
                                }
                                $disabled = '';
                                if($avaliableClass['quotaLeft'] <= 0){
                                	$disabled = 'disabled';
                                }
                                ?>
                                <span>
                					<input type="radio" id="sus_status_<?= $avaliableClass['id'] ?>" name="sus_status"
                                           value="<?= $avaliableClass['id'] ?>" <?= $checked ?> <?=$disabled?> />
                					<label for="sus_status_<?= $avaliableClass['id'] ?>"><?= $avaliableClass['className'] ?></label>

                					<?php if ($avaliableClass['quotaLeft'] <= 0): ?>
                                        <span style="color:red;margin-right: 30px;">
                                            (
                                                <?=$lauc->getLangStr(array(
                                                    'b5' => $LangB5['Admission']['IsFull'],
                                                    'en' => $LangEn['Admission']['IsFull'],
                                                ))?>
                                            )
                                        </span>
                                    <?php endif; ?>
                				</span>
                            <?php
                            endforeach;
                        else:
                            ?>
                            <span style="color:red;">
                                <?=$lauc->getLangStr(array(
                                    'b5' => $LangB5['Admission']['msg']['noclasslevelapply'],
                                    'en' => $LangEn['Admission']['msg']['noclasslevelapply'],
                                ), '<br />')?>
        					</span>
                        <?php
                        endif;
                        ?>
    				</span>
                    </div>
                    <?php
                    if ($admission_cfg['Lang'] && !$admission_cfg['HideSelectLang']):
                        ?>
                        <div class="item">
                            <div class="itemLabel requiredLabel">
	                            <?=$lauc->getLangStr(array(
		                            'b5' => $LangB5['Admission']['Language'],
		                            'en' => $LangEn['Admission']['Language'],
	                            ))?>
                            </div>
                            <span class="itemInput itemInput-choice">
            				<?php
                            foreach ($admission_cfg['Lang'] as $val => $code):
                                $checked = ($code == $admission_cfg['DefaultLang']) ? 'checked' : '';
                                ?>
                                <span>
                					<input type="radio" id="lang_<?= $val ?>" name="lang"
                                           value="<?= $val ?>" <?= $checked ?> />
                					<label for="lang_<?= $val ?>"><?= $Lang['Admission']['LanguagesTransDisplay'][$code] ?></label>
                				</span>
                            <?php
                            endforeach;
                            ?>
        				</span>
                        </div>
                    <?php
                    else:
                        ?>
                        <input type="hidden" name="lang" value="<?= $admission_cfg['DefaultLang'] ?>"/>
                    <?php
                    endif;
                    ?>
                </section>
            <?php endif; ?>
        </article>

        <div id="blkButtons" class="layout-m">
            <div onclick="$('form').submit()" class="button floatR">
	            <?=$lauc->getLangStr(array(
		            'b5' => $LangB5['Admission']['Start'],
		            'en' => $LangEn['Admission']['Start'],
	            ))?>
            </div>
        </div>
    </form>
<?php endif; ?>

    <div id="blkFooter" class="layout-m">
        <span id="lbleClass"><span>Powered by</span><a href="http://eclass.com.hk" title="eClass" target="_blank"><img
                        src="/images/kis/eadmission/eClassLogo.png"></a></span>
    </div>

<?php
include(__DIR__ . '/footer.php');
