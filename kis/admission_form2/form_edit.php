<?php
// modifying by: Henry

/**
 * ******************
 * Log :
 * Date 2018-09-19 [Henry]
 * File Created
 *
 * ******************
 */
$PATH_WRT_ROOT = "../../";
$intranet_root = "../../";

include_once ("{$PATH_WRT_ROOT}includes/global.php");
include_once ("{$PATH_WRT_ROOT}includes/libdb.php");
include_once ("{$PATH_WRT_ROOT}includes/libinterface.php");
include_once ("{$PATH_WRT_ROOT}includes/kis/libkis.php");
include_once ("{$PATH_WRT_ROOT}includes/kis/libkis_ui.php");
include_once ("{$PATH_WRT_ROOT}includes/kis/libkis_utility.php");
include_once ("{$PATH_WRT_ROOT}includes/kis/libkis_apps.php");
include_once ("{$PATH_WRT_ROOT}includes/json.php");
include_once ("../config.php");
if($sys_custom['KIS_Admission']['DynamicAdmissionForm']){
    include_once ("{$PATH_WRT_ROOT}includes/admission/HelperClass/DynamicAdmissionFormSystem/DynamicAdmissionFormSystem.php");
    include_once ("{$PATH_WRT_ROOT}includes/admission/HelperClass/DynamicAdmissionFormSystem/DynamicAdmissionFormSystemUi.php");
}

$IsUpdate = true;

// for the customization
include_once ("{$PATH_WRT_ROOT}includes/admission/{$setting_path_ip_rel}/config.php");
$intranet_session_language = $admission_cfg['DefaultLang'];
include_once ("{$PATH_WRT_ROOT}includes/admission/{$setting_path_ip_rel}/libadmission_cust.php");
include_once ("{$PATH_WRT_ROOT}includes/admission/{$setting_path_ip_rel}/libadmission_ui_cust.php");


intranet_opendb();

// ### Lang START ####
if ($intranet_session_language == 'en') {
    $intranet_session_language = 'b5';
    include ("{$PATH_WRT_ROOT}lang/lang.{$intranet_session_language}.php");
    include ("{$PATH_WRT_ROOT}lang/kis/lang_common_{$intranet_session_language}.php");
    include ("{$PATH_WRT_ROOT}lang/kis/apps/lang_admission_{$intranet_session_language}.php");
    include ("{$PATH_WRT_ROOT}lang/admission_lang.{$intranet_session_language}.php");
    $LangB5 = $Lang;
    $kis_lang_b5 = $kis_lang;
    unset($Lang);
    unset($kis_lang);

    $intranet_session_language = 'en';
    include ("{$PATH_WRT_ROOT}lang/lang.{$intranet_session_language}.php");
    include ("{$PATH_WRT_ROOT}lang/kis/lang_common_{$intranet_session_language}.php");
    include ("{$PATH_WRT_ROOT}lang/kis/apps/lang_admission_{$intranet_session_language}.php");
    include ("{$PATH_WRT_ROOT}lang/admission_lang.{$intranet_session_language}.php");
    $LangEn = $Lang;
    $kis_lang_en = $kis_lang;
} else {
    $intranet_session_language = 'en';
    include ("{$PATH_WRT_ROOT}lang/lang.{$intranet_session_language}.php");
    include ("{$PATH_WRT_ROOT}lang/kis/lang_common_{$intranet_session_language}.php");
    include ("{$PATH_WRT_ROOT}lang/kis/apps/lang_admission_{$intranet_session_language}.php");
    include ("{$PATH_WRT_ROOT}lang/admission_lang.{$intranet_session_language}.php");
    $LangEn = $Lang;
    $kis_lang_en = $kis_lang;
    unset($Lang);
    unset($kis_lang);

    $intranet_session_language = 'b5';
    include ("{$PATH_WRT_ROOT}lang/lang.{$intranet_session_language}.php");
    include ("{$PATH_WRT_ROOT}lang/kis/lang_common_{$intranet_session_language}.php");
    include ("{$PATH_WRT_ROOT}lang/kis/apps/lang_admission_{$intranet_session_language}.php");
    include ("{$PATH_WRT_ROOT}lang/admission_lang.{$intranet_session_language}.php");
    $LangB5 = $Lang;
    $kis_lang_b5 = $kis_lang;
}
// ### Lang END ####

$libkis = new kis('');
$lac = new admission_cust();
$lauc = new admission_ui_cust();
$li = new interface_html();
$json = new JSON_obj();

if (! $plugin['eAdmission']) {
    include_once ("{$PATH_WRT_ROOT}includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT("", "../");
    exit();
}

$_SESSION['SSV_PRIVILEGE']['school']['name'] = GET_SCHOOL_NAME();
$school = $libkis->getUserSchool();
$basic_settings = $lac->getBasicSettings('99999');

#### Get next school year name START ####
$academicYearB5 = getAcademicYearByAcademicYearID($lac->schoolYearID, 'b5');
$academicYearEn = getAcademicYearByAcademicYearID($lac->schoolYearID, 'en');
#### Get next school year name END ####


$class_level = $lac->getClassLevel();
$application_setting = $lac->getApplicationSetting();

if(!$_SESSION["UserID"]){
	$time = $_SERVER['REQUEST_TIME'];
	/**
	 * for a 30 minute timeout, specified in seconds
	 */
	$timeout_duration = 3600;

	/**
	 * Here we look for the user・s LAST_ACTIVITY timestamp. If
	 * it・s set and indicates our $timeout_duration has passed,
	 * blow away any previous $_SESSION data and start a new one.
	 */
	if (isset($_SESSION['LAST_ACTIVITY']) && ($time - $_SESSION['LAST_ACTIVITY']) > $timeout_duration) {
		unset($_SESSION['KIS_ApplicationID']);
		unset($_SESSION['KIS_StudentDateOfBirth']);
		unset($_SESSION['KIS_StudentBirthCertNo']);
	}
	/**
	 * Finally, update LAST_ACTIVITY so that our timeout
	 * is based on it and not the user・s login time.
	 */
	$_SESSION['LAST_ACTIVITY'] = $time;
}
if($_REQUEST['StudentDateOfBirthYear'] || $_REQUEST['StudentDateOfBirthMonth'] || $_REQUEST['StudentDateOfBirthDay']){
	$_REQUEST['InputStudentDateOfBirth'] = $_REQUEST['StudentDateOfBirthYear'].'-'.$_REQUEST['StudentDateOfBirthMonth'].'-'.$_REQUEST['StudentDateOfBirthDay'];
}
$result = $lac->getApplicationResult($_REQUEST['InputStudentDateOfBirth'], $_REQUEST['InputStudentBirthCertNo'], $lac->schoolYearID, $_REQUEST['InputApplicationID']);
if($result['ApplicationID'] != ''){
	$_SESSION['KIS_ApplicationID'] = $result['ApplicationID'];
	$_SESSION['KIS_StudentDateOfBirth'] = $_REQUEST['InputStudentDateOfBirth'];
	$_SESSION['KIS_StudentBirthCertNo'] = $_REQUEST['InputStudentBirthCertNo'];
}

if($_REQUEST['InputStudentDateOfBirth'] == '' || $_REQUEST['InputStudentBirthCertNo'] == '' || $_REQUEST['InputApplicationID'] == '' || $result['ApplicationID'] == ''){
	header("Location: index_edit.php?err=1");
    exit();
}

#### Step START ####
if($sys_custom['KIS_Admission']['EditUploadDocumentMode']){
    $stepArr = array(
        array(
            'id'    => 'pageDocsUpload',
            'title' => $lauc->getLangStr( array(
                'b5' => $LangB5['Admission']['docsUpload'],
                'en' => $LangEn['Admission']['docsUpload'],
            )),
        ),
        array(
            'id'    => 'pageConfirmation',
            'title' => $lauc->getLangStr( array(
                'b5' => $LangB5['Admission']['confirmation'],
                'en' => $LangEn['Admission']['confirmation'],
            )),
        ),
        array(
            'id'    => 'pageFinish',
            'title' => $lauc->getLangStr( array(
                'b5' => $LangB5['Admission']['finish'],
                'en' => $LangEn['Admission']['finish'],
            )),
        ),
    );
}else{
    $stepArr = array(
        array(
            'id'    => 'pagePersonalInfo',
            'title' => $lauc->getLangStr( array(
                'b5' => $LangB5['Admission']['personalInfo'],
                'en' => $LangEn['Admission']['personalInfo'],
            )),
        ),
        array(
            'id'    => 'pageDocsUpload',
            'title' => $lauc->getLangStr( array(
                'b5' => $LangB5['Admission']['docsUpload'],
                'en' => $LangEn['Admission']['docsUpload'],
            )),
        ),
        array(
            'id'    => 'pageConfirmation',
            'title' => $lauc->getLangStr( array(
                'b5' => $LangB5['Admission']['confirmation'],
                'en' => $LangEn['Admission']['confirmation'],
            )),
        ),
        array(
            'id'    => 'pageFinish',
            'title' => $lauc->getLangStr( array(
                'b5' => $LangB5['Admission']['finish'],
                'en' => $LangEn['Admission']['finish'],
            )),
        ),
    );
}

$stepArr = $lauc->applyFilter($lauc::FILTER_ADMISSION_FORM_WIZARD_STEPS, $stepArr);
#### Step END ####



###################################### UI START ######################################
include(__DIR__.'/header.php');
include_once ("{$PATH_WRT_ROOT}includes/admission/{$setting_path_ip_rel}/commonJs.php");
?>

<form autocomplete="off" id="form1" name="form1" method="POST" action="confirm_update_edit.php" enctype="multipart/form-data">
    <input autocomplete="false" name="hidden" type="text" style="display:none;">
    <div id="blkSteps">
    	<span id="btnPrevStep" class="icon icon-button fa-chevron-left"></span>
    	<span id="btnNextStep" class="icon icon-button fa-chevron-right"></span>

    	<div id="stepContainer">
        	<!--span class="step step-current">
        		<div class="step-order">1</div>
        		<div class="step-description"><div>個人資料</div><div>Personal Info</div></div>
        	</span>
        	<span class="step" id="btnGoUpload">
        		<div class="step-order">2</div>
        		<div class="step-description"><div>文件上傳</div><div>Docs Upload</div></div>
        	</span>
        	<span class="step" id="btnGoConfirm">
        		<div class="step-order">3</div>
        		<div class="step-description"><div>確認</div><div>Confirmation</div></div>
        	</span>
        	<span class="step">
        		<div class="step-order">4</div>
        		<div class="step-description"><div>提交</div><div>Submission</div></div>
        	</span>
        	<span class="step">
        		<div class="step-order">5</div>
        		<div class="step-description"><div>付款</div><div>Payment</div></div>
        	</span>
        	<span class="step">
        		<div class="step-order fa-flag icon"></div>
        		<div class="step-description"><div>完成</div><div>Finish</div></div>
        	</span-->
    	</div>
    </div>

    <article>
    	<?= $lauc->getApplicationUpdateForm($BirthCertNo,$sus_status) ?>
    </article>

    <div id="blkButtons">
    	<div class="button floatR" id="btnSubmit">
            <?=$lauc->getLangStr( array(
			    'b5' => $LangB5['Btn']['Submit'],
			    'en' => $LangEn['Btn']['Submit'],
		    ))?>
        </div>
    	<div class="button floatR" id="btnContinue">
		    <?=$lauc->getLangStr( array(
			    'b5' => $LangB5['Admission']['continue'],
			    'en' => $LangEn['Admission']['continue'],
		    ))?>
        </div>
    	<div class="button button-secondary floatR" id="btnCancel">
		    <?=$lauc->getLangStr( array(
			    'b5' => $LangB5['Admission']['cancel'],
			    'en' => $LangEn['Admission']['cancel'],
		    ))?>
        </div>
    	<div class="button button-secondary floatL" id="btnBack">
		    <?=$lauc->getLangStr( array(
			    'b5' => $LangB5['Admission']['back'],
			    'en' => $LangEn['Admission']['back'],
		    ))?>
        </div>
    </div>

	<input type="hidden" name="sus_status" value="<?=$sus_status?>" />
    <input type="hidden" name="SchoolYearID" value="<?=$lac->schoolYearID?>" />
    <input type="hidden" name="isEdit" value="1" />
    <input type="hidden" name="isTeacher" value="<?= ($lac->isInternalUse($_GET['token']))?1:0 ?>" />
</form>

<div id="blkFooter">
	<span id="lbleClass"><span>Powered by</span><a href="http://eclass.com.hk" title="eClass" target="_blank"><img src="/images/kis/eadmission/eClassLogo.png"></a></span>
</div>

<div id="blkTopBar" class="transition">
	<div id="blkTitle">
		<img id="imgSchLogo" src="<?= is_file('/file/kis/admission/images/logo.png') ? '/file/kis/admission/images/logo.png' : $school['logo'] ?>">
    	<?php if($lang == 'en' || $admission_cfg['IsBilingual']): ?>
    		<span id="lblSchName-chi"><?=$basic_settings['schoolnamechi']?$basic_settings['schoolnamechi']:$admission_cfg['SchoolName']['b5']?></span> <?=$admission_cfg['AdmissionSystemTitle']['b5']?$admission_cfg['AdmissionSystemTitle']['b5']:$LangB5['Admission']['eAdmissionSystem'] ?><?=$admission_cfg['IsBilingual']?' | ':''?>
    	<?php endif;if($lang == 'b5' || $admission_cfg['IsBilingual']): ?>
    		<span id="lblSchName-eng"><?=$basic_settings['schoolnameeng']?$basic_settings['schoolnameeng']:$admission_cfg['SchoolName']['en']?></span> <?=$admission_cfg['AdmissionSystemTitle']['en']?$admission_cfg['AdmissionSystemTitle']['en']:$LangEn['Admission']['eAdmissionSystem'] ?>
    	<?php endif; ?>
    </div>
</div>

<div id="cancel-dialog" class="dialog-container hide">
	<span class="dialog dialog-yesno" id="blkCancel">
		<div>
		    <?=$lauc->getLangStr( array(
			    'b5' => $LangB5['Admission']['msg']['confirmCancelModification'],
			    'en' => $LangEn['Admission']['msg']['confirmCancelModification'],
		    ))?>
		</div>
		<div class="dialog-buttons">
			<span class="closeDialog-button button">
                <?=$lauc->getLangStr( array(
                    'b5' => $LangB5['Admission']['back'],
                    'en' => $LangEn['Admission']['back'],
                ))?>
            </span>
			<span id="btnConfirmCancelEdit" class="button button-secondary">
                <?=$lauc->getLangStr( array(
                    'b5' => $LangB5['Admission']['confirm'],
                    'en' => $LangEn['Admission']['confirm'],
                ))?>
            </span>
		</div>
	</span>
</div>

<div id="submit-dialog" class="dialog-container hide">
	<span class="dialog dialog-yesno" id="blkCancel">
		<div>
            <?=$lauc->getLangStr( array(
                'b5' => $LangB5['Admission']['msg']['suresubmit'],
                'en' => $LangEn['Admission']['msg']['suresubmit'],
            ))?>
		</div>
		<div class="dialog-buttons">
			<span id="btnConfirmSubmit" class="button">
                <?=$lauc->getLangStr( array(
                    'b5' => $LangB5['Admission']['confirmSubmit'],
                    'en' => $LangEn['Admission']['confirmSubmit'],
                ))?>
            </span>
			<span class="closeDialog-button button button-secondary">
                <?=$lauc->getLangStr( array(
	                'b5' => $LangB5['Admission']['back'],
	                'en' => $LangEn['Admission']['back'],
                ))?>
            </span>
		</div>
	</span>
</div>

<script id="stepHtml" type="text/html">
    <span class="step <%= cssClass %>">
        <div class="step-order <%= stepOrderCssClass %>"><%= stepOrder %></div>
        <div class="step-description"><div><%= title %></div></div>
    </span>
</script>


<?php
include(__DIR__.'/footer.php');
