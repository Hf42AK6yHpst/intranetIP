<?php

$tabArray = array('awards','activities');

if($sys_custom['iPf']['chiuchunkg']['SBS']){ // CCKG Cust
	$tabArray[] = 'portfoliodata';
}
global $plugin;
if($plugin['SDAS_module']['KISMode']){
	$tabArray[] = 'schoolaward';
	$tabArray[] = 'synctocees';
}

?>
<script>
$(function(){
    kis.iportfolio.teacher_award_edit_init({
		please_fill_in:"<?=$kis_lang['please_fill_in']?>",
		please_select_student: '<?=$kis_lang['msg']['please_select_student']?> ',
		award_title: '<?=strtolower($kis_lang['award_title'])?> ',
		award_date: '<?=strtolower($kis_lang['award_date'])?> '
	});
});
</script>
<div class="main_content">
<form id='award_form'>
    <div class="main_content_detail">
        <? kis_ui::loadModuleTab($tabArray, 'awards', '#apps/iportfolio/schoolrecords/') ?>
        <p class="spacer"></p>
        <?=$kis_data['NavigationBar']?>
        <p class="spacer"></p>
         <div class="table_board">
			<table class="form_table">
			<?php if($kis_data['school_record_action']=='new'&&empty($kis_data['studentId'])){ ?>
				<tr class="mail_compose_table">
					<td class="field_title"><span class="tabletextrequire">*</span><?=$kis_lang['student']?></td>
					<td>
						<div class="mail_to">
							<div class="mail_to_list">
							<? kis_ui::loadTemplate('schoolrecords/select_users',array('users'=>$draft['recipients'], 'form_name'=>'target_user')) ?>
							</div>
							<div class="mail_to_btn">
								<div class="mail_icon_form"><a href="#" class="btn_select_ppl"><?=$kis_lang['select']?></a></div>
							</div>
						</div>
					</td>
				</tr>
			<?php } ?>	
			    <tr>
					<td class="field_title"><?=$kis_lang['schoolyear']?></td>
					<td><?=$kis_data['select_academicYear']?></td>
				</tr>		
			    <tr>
					<td class="field_title"><?=$kis_lang['term']?></td>
					<td><span id="span_term"><?=$kis_data['select_academicYearTerm']?></span></td>
				</tr>					
			    <tr>
					<td class="field_title"><span class="tabletextrequire">*</span><?=$kis_lang['award_title']?></td>
					<td ><?=kis_iportfolio::getFormField("text","award_name",$kis_data['RetrieveList']['award_name'],'edit',' class="textboxtext"')?></td>
				</tr>
			    <tr>
					<td class="field_title"><span class="tabletextrequire">*</span><?=$kis_lang['award_date']?></td>
					<td ><?=kis_iportfolio::getFormField("text","award_date",$kis_data['RetrieveList']['award_date'],'edit')?></td>
				</tr>
				<?php global $plugin; 
				if($plugin['SDAS_module']['KISMode']){
					include_once($PATH_WRT_ROOT.'includes/cust/student_data_analysis_system_kis/libSDAS.php');
					include_once($PATH_WRT_ROOT.'lang/kis/apps/lang_cees_'.$intranet_session_language.'.php');
				?>
				<tr>
					<td valign="top" nowrap="nowrap" class="field_title"><?=$Lang['CEES']['Management']['SchoolActivityReport']['Category']['SDAS_AllowSync']?></td>
					<td>
						<input type='checkbox' name='SDAS_AllowSync[]' id='SDAS_AllowSync_I' value='I' <?=( in_array('I',$kis_data['RetrieveList']['SDAS_AllowSync'])?'checked':'')?> />
						<label for='SDAS_AllowSync_I'><?=$Lang['CEES']['Management']['SchoolActivityReport']['Category']['SchoolActivityReport'].'('.$Lang['CEES']['Management']['SchoolActivityReport']['Category']['IntraSchool'].')'?></label>
						<input type='checkbox' name='SDAS_AllowSync[]' id='SDAS_AllowSync_M' value='M' <?=( in_array('M',$kis_data['RetrieveList']['SDAS_AllowSync'])?'checked':'')?>/>
						<label for='SDAS_AllowSync_M'><?=$Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['Title']?></label>
					</td>
				</tr>			
			    <tr>
			    	<td class="field_title"><?=$Lang['CEES']['Management']['SchoolActivityReport']['IntraSchool']['ParticipatedGroup']?></td>
					<td ><?=kis_iportfolio::getFormField("text","participatedGroup",$kis_data['RetrieveList']['participatedGroup'],'edit',' class="textboxtext"')?></td>
			    </tr>
				<?php }?>				
			    <tr>
					<td class="field_title"><?=$kis_lang['award_organization']?></td>
					<td ><?=kis_iportfolio::getFormField("text","organization",$kis_data['RetrieveList']['organization'],'edit',' class="textboxtext"')?></td>
				</tr>
			    <tr>
					<td class="field_title"><?=$kis_lang['subject_area']?></td>
					<td ><?=kis_iportfolio::getFormField("text","subject_area",$kis_data['RetrieveList']['subject_area'],'edit',' class="textboxtext"')?></td>
				</tr>				
			    <tr>
					<td class="field_title"><?=$kis_lang['remarks']?></td>
					<td><?=kis_iportfolio::getFormField("textarea","remarks",$kis_data['RetrieveList']['remarks'],'edit',' rows="4" wrap="virtual" class="textboxtext"')?></td>
				</tr>
				<col class="field_title" />
				<col  class="field_c" />
			</table>
			<p class="spacer"></p>
			<p class="spacer"></p><br />
        </div>
		<div class="edit_bottom">
		<?php if($kis_data['school_record_action']=='edit'){ ?>
			<input type="hidden" name="recordId" id="recordId" value="<?=$kis_data['RetrieveList']['award_id']?>">
			<input type="hidden" name="studentId" id="studentId" value="<?=$kis_data['RetrieveList']['user_id']?>">
		<?php }elseif($kis_data['school_record_action']=='new'&&!empty($kis_data['studentId'])){ ?>	
			<input type="hidden" name="target_user[]" value="<?=$kis_data['studentId']?>">
			<input type="hidden" name="studentId" id="studentId" value="<?=$kis_data['studentId']?>">
		<?php } ?>
			
			<input type="hidden" name="school_record_action" id="school_record_action" value="<?=$kis_data['school_record_action']?>">
			<input type="submit" id="submitBtn" name="submitBtn" class="formbutton" value="<?=$kis_lang['submit']?>" />
			<input type="button" id="cancelBtn" name="cancelBtn" class="formsubbutton" value="<?=$kis_lang['cancel']?>" />
		</div>
    </div>
</form>
</div>
					
<form class='mail_select_user'>
    <h2><?=$kis_lang['findusers']?></h2>
    
    <?=$kis_lang['keyword']?>: <input type="text" name="keyword" style="float:right" value=""/>
    <p class="spacer"></p>
    <?=$kis_lang['class']?>: <?=$kis_data['ClassList']?>
    <p class="spacer"></p>
    <input type="hidden" name="exclude_list" value=""/>
    <p class="spacer"></p>
    <div class="button">  
	<input class="formbutton" value="<?=$kis_lang['search']?>" type="submit"/>
	<input class="formsubbutton" value="<?=$kis_lang['close']?>" type="submit"/>
    </div>
    
    <a class="mail_select_all" href="#"><?=$kis_lang['addall']?></a>
    <p class="spacer"></p>
    <div class="search_results">
    </div>
    
    
</form>					