<?php
// Using: Pun

ini_set('memory_limit','128M');

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
iportfolio_auth("SPTA");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpf-sbs.php");
include_once($PATH_WRT_ROOT."includes/lib-growth-scheme.php");
require_once($PATH_WRT_ROOT."includes/mpdf/mpdf.php");

include_once($PATH_WRT_ROOT.'kis/init.php');
$libkis_iportfolio = $libkis->loadApp('iportfolio');


intranet_opendb();

######## Init START ########
$lpf = new libpf_sbs();

$mPDF = new mPDF(
    $mode='',
    $format='A4',
    $default_font_size=0,
    $default_font='msjh',
    $marginLeft=0,
    $marginRight=0,
    $marginTop=0,
    $marginBottom=0,
    $marginHeader=0,
    $marginFooter=0,
    $orientation='P'
);

$StudentIDArr = IntegerSafe($StudentIDArr);

$imgBase = "{$PATH_WRT_ROOT}file/iportfolio/cust/chiuchunkg/";
$emptyStr = '&nbsp;&nbsp;';

$mPDF->SetHTMLHeader('<img src="'.$imgBase.'header.jpg" />');
$mPDF->SetHTMLFooter('<img src="'.$imgBase.'footer.jpg" />');
######## Init END ########


######## Access Right START ########
$lpf->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);
$lpf->ACCESS_CONTROL("growth_scheme");


$lgs = new growth_scheme();

if ($ck_memberType != "T") {
	No_Access_Right_Pop_Up();
}
######## Access Right END ########


######## Student Info START ########
$studentIdSQL = implode("','", (array)$StudentIDArr);

$sql = "SELECT 
    iu.UserID,
    CONCAT(iu.ChineseName, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;', iu.EnglishName) as Name,
    iu.ClassName,
    iu.ClassNumber,
    DATE(iu.DateOfBirth) as birthday,
    iu.Gender as gender,
    iu.STRN,
	y.YearName,
	yc.ClassTitleB5,
	yc.ClassTitleEN
FROM 
	{$intranet_db}.INTRANET_USER iu
INNER JOIN
	YEAR_CLASS_USER ycu ON iu.UserID = ycu.UserID
INNER JOIN
	YEAR_CLASS yc ON ycu.YearClassID = yc.YearClassID					
INNER JOIN
	YEAR y ON yc.YearID = y.YearID				
WHERE 
	yc.AcademicYearID = '".$school_year_id."'
AND
	iu.UserID in ('{$studentIdSQL}')
ORDER BY
	iu.ClassName,
	iu.ClassNumber
	";
$rs = 	$lpf->returnResultSET($sql);
// debug_r($sql);

foreach($rs as $i=>$user){
	if($i>0){
	    $mPDF->AddPage();
	}
	
	$studentId = $user['UserID'];
	$studentName = $user['Name'];
	$studentYear = $user['YearName'];
	$studentClass = Get_Lang_Selection($user['ClassTitleB5'],$user['ClassTitleEN']);
	$studentNumber = $user['STRN']; // $user['ClassNumber'];
	$studentDOB = date("Y/m/d", strtotime($user['birthday']));
	if($studentDOB == '1970/01/01'){
		$studentDOB = '&nbsp;';
	}
	$studentGender = $user['gender'];
	######## Student Info END ########
	
	
	######## Student Portfolio Info START ########
	$data = array();
	$data['studentId'] = $studentId;
	$data['school_year_id'] = $school_year_id;
	$data['school_year_term_id'] = $school_year_term_id;
	$portfolioInfo = $libkis_iportfolio->getPortfolioRecord($data);
	
	$semesterArr = getSemesters($school_year_id);
	$semesterIdArr = array_keys($semesterArr);
	$isLastSemester = ($school_year_term_id == $semesterIdArr[ count($semesterIdArr)-1 ]);
	$academicYearName = getAYNameByAyId($school_year_id);
	$semesterName = $semesterArr[$school_year_term_id];
	
	if($printDate){
		$printDate = date("Y/m/d", strtotime($printDate));
		$printDateWeekDay = date("w", strtotime($printDate));
	}
	######## Student Portfolio Info END ########
	
	
	######## Set empty string width START ########
	if(count($portfolioInfo) == 0){
		$portfolioInfo['year_name'] = //
		$portfolioInfo['StartAgeYear'] = //
		$portfolioInfo['StartAgeMonth'] = //
		$portfolioInfo['StartWeight'] = //
		$portfolioInfo['StartHeight'] = //
		$portfolioInfo['EndAgeYear'] = //
		$portfolioInfo['EndAgeMonth'] = //
		$portfolioInfo['EndWeight'] = //
		$portfolioInfo['EndHeight'] = //
		$portfolioInfo['TotalSchoolDay'] = //
		$portfolioInfo['PresentDay'] = //
		$portfolioInfo['SickLeaveDay'] = //
		$portfolioInfo['OtherLeaveDay'] = //
		$portfolioInfo['LateDay'] = //
		$portfolioInfo['EarlyLeaveDay'] = $emptyStr;
	}else{
		foreach($portfolioInfo as $key=>$value){
			if($value == ''){
				$portfolioInfo[$key] = $emptyStr;
			}
		}
	}
	######## Set empty string width END ########
	
	
	######## Output START ########
	ob_start();
	?>
	<style>
    	@apage{
    		margin: 0;
    		padding: 0;
    		margin-top:-6mm;
    		
    		/*margin-header: 5mm;
    		margin-footer: 5mm;
    		header: html_myHTMLHeaderOdd;
    		footer: html_myHTMLFooterOdd;*/
    		
    	    /*background-image: url("<?=$imgBase ?>full.jpg");
            background-repeat: no-repeat;
            background-image-resize: 6;*/
    	}
    	body{
    		font-family: msjh !important;
    	}
    	
    	.portfolioTable{
    		border-collapse:collapse;
    		width:100%;
    	}
    	
    	.portfolioTable tr td{
    		border-width: 1px;
    	}
    	
    	.td_l{
    		border-left:0.1px solid black;
    	}
    	.td_t{
    		border-top:0.1px solid black;
    	}
    	.td_r{
    		border-right:0.1px solid black;
    	}
    	.td_b{
    		border-bottom:0.1px solid black;
    	}
    	
    	.td_title{
    	    font-size: 4mm;
    	}
    	
    	.td_content{
    	    font-size: 5mm;
    	}
	</style>
	
	
	
&nbsp;
<table class="portfolioTable" border="0" style="text-align:center; margin-top: 32mm;">
	<tr>
    	<td style="padding: 0;height: 8mm;font-size: 5mm;vertical-align:bottom;">
    		&nbsp;
    		<?=$academicYearName ?> 年度
    		<?=$semesterName ?>
    		<?=$studentYear ?>
    		&nbsp;
    	</td>
	</tr>
	<tr><td style="padding: 0;font-size: 5mm;"><?=$reportTitle ?></td></tr>
</table>



<table class="portfolioTable" border="0" style="margin-top: 0mm;">
	<tr>
		<td></td>
		<td class="td_b td_title" style="height: 7mm;padding-left:3mm;font-size: 4mm;">學生姓名</td>
		<td class="td_b td_content" colspan="7" style="padding-left:3mm;font-size: 4mm;"><?=$studentName ?>&nbsp;</td>
		<td></td>
	</tr>
	
	<tr>
		<td></td>
		
		<td class="td_b td_title" style="height: 7mm;padding-left:3mm;font-size: 4mm;">班別</td>
		<td class="td_b td_r td_content" style="padding-left:3mm;font-size: 4mm;"><?=$studentClass ?>&nbsp;</td>
		
		<td class="td_b td_title" style="text-align:right;font-size: 4mm;">學號</td>
		<td class="td_b td_r td_content" style="text-align:center;font-size: 4mm;">&nbsp;<?=$studentNumber ?>&nbsp;</td>
		
		<td class="td_b td_title" style="text-align:right;font-size: 4mm;">性別</td>
		<td class="td_b td_r td_content" style="text-align:center;font-size: 4mm;">
			&nbsp;<?php
			if($studentGender == 'M'){
				echo $Lang['General']['Male'];
			}else if($studentGender == 'F'){
				echo $Lang['General']['Female'];
			}
			?>&nbsp;
		</td>
		
		<td class="td_b td_title" style="text-align:right;font-size: 4mm;">出生日期</td>
		<td class="td_b td_content" style="text-align:center;font-size: 4mm;">&nbsp;<?=$studentDOB ?>&nbsp;</td>
		
		<td></td>
	</tr>
	<tr>
		<td style="width:10mm;"></td>
		<td style="width:23.75mm;"></td>
		<td style="width:23.75mm;"></td>
		<td style="width:12mm;"></td>
		<td style="width:35.5mm;"></td>
		<td style="width:12mm;"></td>
		<td style="width:35.5mm;"></td>
		<td style="width:20mm;"></td>
		<td style="width:27.5mm;"></td>
		<td style="width:10mm;"></td>
	</tr>
</table>
    
    <table class="portfolioTable" border="0" style="margin-top: 10mm;">
    	<tr>
    		<td></td>
    		<td class="td_b td_r td_title" style="height:6mm;text-align: center;" colspan="3">學期開始</td>
    		<td class="td_b td_title" style="text-align: center;" colspan="3">學期完結</td>
    		<td></td>
    	</tr>
    	
    	<tr>
    		<td></td>
    		<td class="td_title" style="text-align: center;padding:1mm;height:8mm;">年齡</td>
    		<td class="td_title" style="text-align: center;padding:1mm;">體重</td>
    		<td class="td_r td_title" style="text-align: center;padding:1mm;">體高</td>
    		<td class="td_title" style="text-align: center;padding:1mm;">年齡</td>
    		<td class="td_title" style="text-align: center;padding:1mm;">體重</td>
    		<td class="td_title" style="text-align: center;padding:1mm;">體高</td>
    		<td></td>
    	</tr>
    	
    	<tr>
    		<td></td>
    		<td class="td_l td_b td_r td_content" style="text-align: center;padding:3mm;font-size:4mm;">
    			<?=$portfolioInfo['StartAgeYear'] ?>&nbsp;歲&nbsp;<?=$portfolioInfo['StartAgeMonth'] ?>&nbsp;個月
    		</td>
    		<td class="td_l td_b td_r td_content" style="text-align: center;padding:3mm;font-size:4mm;">
    			<?=$portfolioInfo['StartWeight'] ?>&nbsp;千克
    		</td>
    		<td class="td_l td_b td_r td_content" style="text-align: center;padding:3mm;font-size:4mm;">
    			<?=$portfolioInfo['StartHeight'] ?>&nbsp;厘米
    		</td>
    		
    		<td class="td_l td_b td_r td_content" style="text-align: center;padding:3mm;font-size:4mm;">
    			<?=$portfolioInfo['EndAgeYear'] ?>&nbsp;歲&nbsp;<?=$portfolioInfo['EndAgeMonth'] ?>&nbsp;個月
    		</td>
    		<td class="td_l td_b td_r td_content" style="text-align: center;padding:3mm;font-size:4mm;">
    			<?=$portfolioInfo['EndWeight'] ?>&nbsp;千克
    		</td>
    		<td class="td_l td_b td_r td_content" style="text-align: center;padding:3mm;font-size:4mm;">
    			<?=$portfolioInfo['EndHeight'] ?>&nbsp;厘米
    		</td>
    		<td></td>
    	</tr>
    	
    	<tr>
    		<td style="width:10mm;"></td>
    		<td style="width:32mm;"></td>
    		<td style="width:31mm;"></td>
    		<td style="width:32mm;"></td>
    		<td style="width:32mm;"></td>
    		<td style="width:31mm;"></td>
    		<td style="width:32mm;"></td>
    		<td style="width:10mm;"></td>
    	</tr>
    </table>
    
    
    <table class="portfolioTable" border="0" style="margin-top: 5mm;">
    	<tr>
    		<td></td>
    		<td class="td_l td_b td_r td_title" style="text-align: center;padding:3mm 0mm;font-size:4mm;">
    			本學期上課&nbsp;<?=$portfolioInfo['TotalSchoolDay'] ?>&nbsp;天
    		</td>
    		<td class="td_l td_b td_r td_title" style="text-align: center;padding:3mm 0mm;font-size:4mm;">
    			該生上課&nbsp;<?=$portfolioInfo['PresentDay'] ?>&nbsp;天
    		</td>
    		<td class="td_l td_b td_r td_title" style="text-align: center;padding:3mm 0mm;font-size:4mm;">
    			病假&nbsp;<?=$portfolioInfo['SickLeaveDay'] ?>&nbsp;天
    		</td>
    		<td class="td_l td_b td_r td_title" style="text-align: center;padding:3mm 0mm;font-size:4mm;">
    			事假&nbsp;<?=$portfolioInfo['OtherLeaveDay'] ?>&nbsp;天
    		</td>
    		<td class="td_l td_b td_r td_title" style="text-align: center;padding:3mm 0mm;font-size:4mm;">
    			遲到&nbsp;<?=$portfolioInfo['LateDay'] ?>&nbsp;天
    		</td>
    		<td class="td_l td_b td_r td_title" style="text-align: center;padding:3mm 0mm;font-size:4mm;">
    			早退&nbsp;<?=$portfolioInfo['EarlyLeaveDay'] ?>&nbsp;天
    		</td>
    		<td></td>
    	</tr>
    	<tr>
    		<td style="width:10mm;"></td>
    		<td style="width:32mm;"></td>
    		<td style="width:31mm;"></td>
    		<td style="width:32mm;"></td>
    		<td style="width:32mm;"></td>
    		<td style="width:31mm;"></td>
    		<td style="width:32mm;"></td>
    		<td style="width:10mm;"></td>
    	</tr>
    </table>
    
    
    <?php if($isLastSemester){ ?>
        <table class="portfolioTable" border="0" style="margin-top: 5mm;">
            <tr>
            	<td></td>
        		<td class="td_t td_l td_r" style="vertical-align: top;padding:2mm;padding-bottom:0;">獎項：</td>
            	<td></td>
            </tr>
            <tr>
            	<td></td>
        		<?php if($isLastSemester){ ?>
                	<td class="td_l td_r td_content" style="height: 40mm; text-align: left;vertical-align: top;font-size: 15px;padding:2mm;padding-top:1mm;">
                		　<?=$portfolioInfo['Award']?>
                	</td>
            	<?php }else{ ?>
                	<td class="td_l td_r td_content" style="height: 100mm; text-align: left;vertical-align: top;font-size: 15px;padding:2mm;padding-top:1mm;">
                		　<?=$portfolioInfo['Award']?>
                	</td>
            	<?php } ?>
            	<td></td>
            </tr>
            <tr>
            	<td style="width:10mm;"></td>
            	<td class="td_l td_b td_r" style="width:190mm;"></td>
            	<td style="width:10mm;"></td>
        	</tr>
        </table>
    <?php } ?>
    
    
    <table class="portfolioTable" border="0" style="margin-top: 5mm;">
        <tr>
        	<td></td>
    		<td class="td_t td_l td_r" style="vertical-align: top;padding:2mm;padding-bottom:0;">特錄：</td>
        	<td></td>
        </tr>
        <tr>
        	<td></td>
    		<?php if($isLastSemester){ ?>
            	<td class="td_l td_r td_content" style="height: 40mm; text-align: left;vertical-align: top;font-size: 15px;padding:2mm;padding-top:1mm;">
            		　<?=$portfolioInfo['Remark']?>
            	</td>
        	<?php }else{ ?>
            	<td class="td_l td_r td_content" style="height: 94mm; text-align: left;vertical-align: top;font-size: 15px;padding:2mm;padding-top:1mm;">
            		　<?=$portfolioInfo['Remark']?>
            	</td>
        	<?php } ?>
        	<td></td>
        </tr>
        <tr>
        	<td style="width:10mm;"></td>
        	<td class="td_l td_b td_r" style="width:190mm;"></td>
        	<td style="width:10mm;"></td>
    	</tr>
    </table>
    
    
    <table class="portfolioTable" border="0" style="margin-top: 5mm;">
    	<tr>
    		<td></td>
    		<td class="td_t" colspan="2" style="vertical-align: top;height:30mm;">校監</td>
    		<td class="td_t" colspan="2" style="vertical-align: top;">校長</td>
    		<td class="td_t" colspan="4" style="vertical-align: top;">老師</td>
    		<td></td>
    	</tr>
    	<tr>
    		<td style="width:10mm;"></td>
    		<td class="td_l td_b" style="width:15mm;"></td>
    		<td class="td_l td_b" style="width:29mm;"></td>
    		<td class="td_l td_b" style="width:15mm;"></td>
    		<td class="td_l td_b" style="width:29mm;"></td>
    		<td class="td_l td_b" style="width:15mm;"></td>
    		<td class="td_l td_b" style="width:29mm;"></td>
    		<td class="td_l td_b" style="width:29mm;"></td>
    		<td class="td_l td_b td_r" style="width:29mm;"></td>
    		<td style="width:10mm;"></td>
    	</tr>
    </table>
    
<div style="position: fixed;bottom: 15mm;">
    <table class="portfolioTable" border="0" style="text-align:center;">
    	<tr>
    		<td></td>
    		<td class="td_title" style="vertical-align:bottom;padding-bottom:0;">家長簽署：</td>
    		<td class="td_b"></td>
    		<td></td>
    		<td class="td_title" style="vertical-align:bottom;padding-bottom:0;text-align: left;">日期：</td>
    		<td class="td_b">
    			&nbsp;<?php
    			if($printDate){
    				//echo "{$printDate} ({$Lang['SysMgr']['Homework']['WeekDay'][$printDateWeekDay]})";
    				echo $printDate;
    			}
    			?>&nbsp;
    		</td>
    		<td></td>
    	</tr>
    	<tr>
    		<td style="width:10mm"></td>
    		<td style="width:20mm"></td>
    		<td style="width:70mm"></td>
    		<td style="width:10mm"></td>
    		<td style="width:15mm"></td>
    		<td style="width:75mm"></td>
    		<td style="width:10mm"></td>
    	</tr>
    </table>
</div>
	
	<?php
	$html = ob_get_clean();
	$mPDF->WriteHTML($html);

} // End foreach($rs as $user)

$mPDF->Output();
?>