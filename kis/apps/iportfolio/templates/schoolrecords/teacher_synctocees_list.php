<?php

$tabArray = array('awards','activities');

include_once($intranet_root."/lang/kis/apps/lang_cees_".$intranet_session_language.".php");
if($sys_custom['iPf']['chiuchunkg']['SBS']){ // CCKG Cust
	$tabArray[] = 'portfoliodata';
}
$tabArray[] = 'schoolaward';
$tabArray[] = 'synctocees';
?>
<script>
$(function(){
	kis.iportfolio.teacher_synctocees_init({
		are_you_sure_to_sync: '<?=$Lang['CEES']['Managemnet']['SchoolActivityReport']['Hint']['SyncEnrolmentAlert']?>',
		are_you_sure_to_delete: '<?=$kis_lang['msg']['are_you_sure_to_delete']?> '
	});
});
</script>
<?php list($page,$amount,$total,$sortby,$order) = $kis_data['PageBar'];  
	if(!$total) $total = $kis_data['record'][0];
?>
<div class="main_content">
 <? kis_ui::loadModuleTab($tabArray, 'synctocees', '#apps/iportfolio/schoolrecords/') ?>
 <?=$kis_data['NavigationBar']?>
    <div class="table_board">
    	<div id="table_filter" style="margin-bottom: 10px">
			<?=$kis_data['select_academicYear']?>
		</div>
		<div id='Content_Block'>
		</div>
    </div>
</div>