<?php

$tabArray = array('awards','activities');

if($sys_custom['iPf']['chiuchunkg']['SBS']){ // CCKG Cust
	$tabArray[] = 'portfoliodata';
}

global $plugin;
if($plugin['SDAS_module']['KISMode']){
	$tabArray[] = 'schoolaward';
	$tabArray[] = 'synctocees';
}
?>
<script>
$(function(){
    kis.iportfolio.teacher_schoolrecord_list_init({
		are_you_sure_to_delete: '<?=$kis_lang['msg']['are_you_sure_to_delete']?> ',
		please_select_student: '<?=$kis_lang['msg']['please_select_student']?>',
	});
});
</script>
<div class="main_content">
 <? kis_ui::loadModuleTab($tabArray, $kis_data['recordType'], '#apps/iportfolio/schoolrecords/') ?>
    <div class="table_board">
        <div class="Content_tool">
	        <?php if($kis_data['recordType'] == 'portfoliodata'){ ?>
				<a href="#" class="tool_import import"><?=$kis_lang['import']?></a>
	        <?php }else{ ?>
	        	<a href="#" class="new"><?=$kis_lang['new']?></a>
			<?php } ?>
		</div>
		<p class="spacer"></p>
		<form class="filter_form">
			<div id="table_filter">
				<?=$kis_data['ClassList']?>
				<?=$kis_data['select_academicYear']?>
				<span id="span_term"><?=$kis_data['select_academicYearTerm']?></span>
				<input name="goBtn" type="submit" class="formsmallbutton" value="Go" />
			</div>   
			<div class="search">
				<input type="hidden" name="retrieve_type" id="retrieve_type" value="<?=$kis_data['recordType']?>"> 
				<input type="text" style="width:150px" name="keyword" id="keyword" placeholder="<?=$kis_lang['enter_student_name']?>" value="<?=$kis_data['keyword']?>"/>
			</div>
		</form>
        <p class="spacer"></p>&nbsp;
        
		<?php if($kis_data['recordType'] == 'portfoliodata'){ ?>
			<div class="common_table_tool common_table_tool_table">
				<a href="#" class="tool_print"><?=$kis_lang['print']?></a>
			</div>
		<?php } ?>
		
        <table class="common_table_list edit_table_list">
			<col   nowrap="nowrap"/>
			<thead>
				<tr>
					<th><?=$kis_lang['class_number']?></th>
					<th><?=$kis_lang['student']?></th>
					<th>
						<?php
							if($kis_data['recordType']=='portfoliodata'){
								echo $kis_lang['StudentAccount']['View'];
							}else if($kis_data['recordType']=='activities'){
								echo $kis_lang['activities_count'];
							}else{
								echo $kis_lang['awards_count'];
							}
						?>
					</th>
					<?php if($kis_data['recordType'] == 'portfoliodata'){ ?>
						<th>
							<input type="checkbox" id="checkmaster">
						</th>
					<?php } ?>
			  </tr>
			</thead>
			<tbody>
			<?php 
				$record_cnt = count($kis_data['record']);
				if($record_cnt>0){
					foreach($kis_data['record'] as $_classNumber => $_record){
			?>
					<tr>
						<td><?=$_classNumber?></td>
						<td><?=$_record['user_name']?></td>
						<td><?=$_record['count']?></td>
						
						<?php if($kis_data['recordType'] == 'portfoliodata'){ ?>
							<td>
								<input type="checkbox" name="studentIdAry[]" value="<?=$_record['user_id']?>" />
							</td>
						<?php } ?>
						
					</tr>
				<?php } ?>			
			<?php }else{ ?>
					<tr>
					  <td colspan="4"><?=$kis_lang['norecord']?></td>
					</tr>
			<?php } ?>				  
			</tbody>
		</table>
		<p class="spacer"></p>
		<p class="spacer"></p><br />
    </div>
</div>

<form id="printForm" style="display:none;" method="POST">
</form>