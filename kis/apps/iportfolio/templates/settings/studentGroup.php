<script>
$(function(){
    kis.iportfolio.settings_goruplist_init({
	message: '<?=$kis_lang['areyousureto']?>',
	remove: '<?=strtolower($kis_lang['remove'])?> ',
	please_fill_in: '<?=$kis_lang['please_fill_in']?> ',
	title: '<?=strtolower($kis_lang['grouplist_title'])?> ',
	description: '<?=strtolower($kis_lang['Settings']['description'])?> ',
	grouplist: '<?=strtolower($kis_lang['Settings']['grouplist'])?> ',
	
	});
	
});
</script>

<? kis_ui::loadModuleTab(array('studentgroup','teachergroup','subjectpanel'), 'studentgroup', '#apps/iportfolio/settings/') ?>
<p class="spacer"></p>

<div class="table_board">			
		<!--Filter Form-->
			<form class="filter_form" method="post">   
				<p class="spacer"></p>
				<div class="search"><!--<a href="#">Advanced</a>-->
					<input type="text" name="keyword" class="auto_submit" id="keyword" placeholder="<?=$kis_lang['Assessment']['Search']?>" value="<?=$kis_data['keyword']?>"/>
				</div>
			</form>
			<p class="spacer"></p>&nbsp;
            <p class="spacer"></p>
            <div class="Content_tool"><a href="#" class="new"><?=$kis_lang['new']?></a></div>
			<div class="common_table_tool common_table_tool_table">
					<a href="#" class="tool_edit"><?=$kis_lang['edit'] ?></a>
					<a href="#" class="tool_delete"><?=$kis_lang['delete'] ?></a>
			</div>
			<form>
				<table class="common_table_list">
                        <tr class="sub_table_top">
                          <th>#</th>
                          <th><? kis_ui::loadSortButton('title','grouplist_title', $sortby, $order)?></th>
                          <th><? kis_ui::loadSortButton('t_mc.memberCnt','member_total', $sortby, $order)?></th>
                          <th><? kis_ui::loadSortButton('lastupdate','last_update_date', $sortby, $order)?></th>
                          <th><input type="checkbox" name="checkmaster" onclick="(this.checked)?setChecked(1,this.form,'group_id[]'):setChecked(0,this.form,'group_id[]')"></th>
                        </tr>
               <?php if(count($kis_data['gorup_list']) == 0){
               	echo "<td colspan='5'>No Record</td>";
               }?>
               <?php for($i=0;$i<count($kis_data['gorup_list']);$i++){ ?>
               			<form>
                        <tr>
                          <td><?=$i+1 ?></td>
                          <td><?=$kis_data['gorup_list'][$i]['title'] ?></td>
                          <td><?=$kis_data['gorup_list'][$i]['totmember']?></td>
                          <td><?=$kis_data['gorup_list'][$i]['lastupdate'] ?></td>
                          <td><?=$kis_data['gorup_list'][$i]['checkbox']?></td>
                        </tr>
				<?php } ?>
                      </table>
             </form>
                    	  <p class="spacer"></p>
                      <?php list($page,$amount,$total,$sortby,$order) = $kis_data['PageBar'];  
                      ?>
						<? kis_ui::loadPageBar($page, $amount, $total, $sortby, $order) ?>     
                      	    	
							<p class="spacer"></p><br />	
						
        </div>
        
        		<!--FancyBox-->
		
		<div id='create_new_box' style="padding:5px;display:none;" class="pop_edit">
			<div class="pop_title">
				<span><?=$kis_lang['Settings']['NewGrouplist']?></span>
			</div>
			<div class="table_board" style="height:330px">

			<form id="grouplistForm" method="post">
				<table class="form_table">
					<tr>
						<td class="field_title"><?=$kis_lang['grouplist_title']?></td>
						<td ><input name="group_title" type="text" id="group_title" class="textboxtext" /></td>
					</tr>
					<tr>
						<td class="field_title"><?=$kis_lang['Settings']['Description']?></td>
						<td ><textarea name="group_desc" id="group_desc" cols="40" rows="5" wrap="virtual"></textarea></td>
					</tr>
				  <col class="field_title" />
				  <col  class="field_c" />
				</table>
			<input type="hidden" name="group_type" value="0">
			<input type="hidden" name="type" value="">
			<input type="hidden" name="groupId" value="">
		</form>	
      <p class="spacer"></p>
      </div>
        <div class="edit_bottom">         
           <input id="submitBtn" name="submitBtn" type="button" class="formbutton" value="<?=$kis_lang['submit']?>" />
           <input name="cancelBtn" type="button" class="formsubbutton" onclick="parent.$.fancybox.close();" value="<?=$kis_lang['cancel']?>" />
      </div>
		</div>