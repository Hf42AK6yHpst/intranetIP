<?php
// Editing by 
/*
 * 2020-11-02 (Ray): Added body temperature
 * 2019-09-23 (Ray): Added present absent reason at student take attendance page
 * 2019-08-22 (Ray): Added reason, remark select box
 * 2018-11-23 (Cameron): apply intranet_htmlspecialchars to reason and remark field
 * 2018-02-05 (Carlos): Added second selection to in/out times.
 * 2017-08-22 (Carlos): Pass more options into js kis.eattendance.takeattendance_students_init().
 * 2016-01-04 (Carlos): Changed the generation of AM/PM selection base on $kis_data['attendance_mode_data'].
 * 2014-02-27 (Carlos): cater lunch back time for pm in time
 * 2014-02-07 (Carlos): added master time selection 
 * 2013-10-16 (Carlos): removed last update column. Display last update by at table top. 
 * 2013-10-08 (Carlos): modified to use Submit button to update data together
 */
 
$master_in_time_selection = '<select id="master_in_school_hour" class="master_in_school_hour">';
for ($i=0; $i<=23; $i++){
	$master_in_time_selection .= '<option value="'.sprintf('%02d',$i).'">'.sprintf('%02d',$i).'</option>';
}
$master_in_time_selection .= '</select> : ';
$master_in_time_selection .= '<select id="master_in_school_min" class="master_in_school_min">';
for ($i=0; $i<=59; $i++){
	$master_in_time_selection .= '<option value="'.sprintf('%02d',$i).'">'.sprintf('%02d',$i).'</option>';
}
$master_in_time_selection .= '</select> : ';
$master_in_time_selection .= '<select id="master_in_school_sec" class="master_in_school_sec">';
for ($i=0; $i<=59; $i++){
	$master_in_time_selection .= '<option value="'.sprintf('%02d',$i).'">'.sprintf('%02d',$i).'</option>';
}
$master_in_time_selection .= '</select>';
				
$master_out_time_selection = '<select id="master_leave_school_hour" class="master_leave_school_hour">';
for ($i=0; $i<=23; $i++){
	$master_out_time_selection .= '<option value="'.sprintf('%02d',$i).'">'.sprintf('%02d',$i).'</option>';
}
$master_out_time_selection .= '</select> : ';
$master_out_time_selection .= '<select id="master_leave_school_min" class="master_leave_school_min">';
for ($i=0; $i<=59; $i++){
	$master_out_time_selection .= '<option value="'.sprintf('%02d',$i).'">'.sprintf('%02d',$i).'</option>';
}
$master_out_time_selection .= '</select> : ';
$master_out_time_selection .= '<select id="master_leave_school_sec" class="master_leave_school_sec">';
for ($i=0; $i<=59; $i++){
	$master_out_time_selection .= '<option value="'.sprintf('%02d',$i).'">'.sprintf('%02d',$i).'</option>';
}
$master_out_time_selection .= '</select>';

?>
<style>
    .custom-combobox {
        position: relative;
        display: inline-block;
    }
    .custom-combobox-toggle {
        position: absolute;
        top: 0;
        bottom: 0;
        margin-left: -1px;
        padding: 0;
    }
    .custom-combobox-input {
        margin: 0;
        padding: 5px 10px;
    }
    .custom-combobox-input.textarea_reason {
        width: 70px;
    }
    .custom-combobox-input.textarea_remark {
        width: 60px;
    }
    table.common_table_list.presetabsent_table tr td {
        background-color: #ffffff;
    }
</style>
<?=$kis_data['reason_js']?>
<script>
$(function(){
    kis.eattendance.takeattendance_students_init({
	date: "<?=$date?>",
	apm: "<?=$apm?>",
	"CannotTakeFutureDateRecord":"<?=$kis_data['CannotTakeFutureDateRecord']?>",
	"CannotTakePastDateRecord":"<?=$kis_data['DisableISmartCardPastDate']?>",
	"EditDaysBeforeRecord":"<?=$kis_data['EditDaysBeforeRecord']?>",
    "EditDaysBeforeRecordSelect":"<?=$kis_data['EditDaysBeforeRecordSelect']?>"
    },{
	present: '<?=$kis_lang['present']?>',	
	absent: '<?=$kis_lang['absent']?>',
	late: '<?=$kis_lang['late']?>',
	earlyleave: '<?=$kis_lang['earlyleave']?>',
	lateandearlyleave: '<?=$kis_lang['lateandearlyleave']?>',
	outing: '<?=$kis_lang['outing']?>'
    });
    
    $('input#cancel_btn').on('click',function(){
		$.address.value('/apps/eattendance/takeattendance/?class_id=<?=$class_id?>&date=<?=$date?>&time=<?=$time?>&r=' + (new Date()).getTime());
	});
	
	$('input#submit_btn').on('click',function(){
		var student_id_ary = [];
		var status_ary = [];
		var in_school_time_ary = [];
		var leave_school_time_ary = [];
		var reason_ary = [];
		var remark_ary = [];
		
		$('.student_id').each(function(i){
			var tr = $(this).closest('tr');
			
			student_id_ary.push(tr.find('.student_id').val());
			status_ary.push(tr.find('.status').val());
			in_school_time_ary.push(tr.find('.in_school_hour').val()+':'+tr.find('.in_school_min').val()+':'+tr.find('.in_school_sec').val());
			leave_school_time_ary.push(tr.find('.leave_school_hour').val()+':'+tr.find('.leave_school_min').val()+':'+tr.find('.leave_school_sec').val());
			reason_ary.push(tr.find('.reason').val());
			remark_ary.push(tr.find('.remark').val());
		});
		
		$('#module_page .board_main_content').append($('.board_loading_overlay').clone().show());
		$.post(
			'./apps/eattendance/ajax.php?action=updateattendance',
			{
				'class_id': '<?=$class_id?>',
				'date': '<?=$date?>',
				'apm': "<?=$apm?>",
				'student_id[]':student_id_ary,
				'status[]':status_ary,
				'in_school_time[]':in_school_time_ary,
				'leave_school_time':leave_school_time_ary,
				'reason[]':reason_ary,
				'remark[]':remark_ary 
			},
			function(responseData){
				$.address.value('/apps/eattendance/takeattendance/?class_id=<?=$class_id?>&date=<?=$date?>&time=<?=$time?>&r=' + (new Date()).getTime());
			}
		);
	});
});

function masterTimeChanged(obj,className)
{
	var selectedVal = $(obj).val();
	$('select.'+className).each(
		function(i){
			$(this).val(selectedVal);
		}
	);
}


$( function() {
    $.widget( "custom.combobox", {
        _create: function() {
            this.wrapper = $( "<span>" )
                .addClass( "custom-combobox" )
                .insertAfter( this.element );

            this.element.hide();
            this._createAutocomplete();
            this._createShowAllButton();
        },

        _createAutocomplete: function() {
            var selected = this.element.children( ":selected" ),
                value = selected.val() ? selected.text() : "";

            this.input = $( "<textarea>" )
                .appendTo( this.wrapper )
                .val( value )
                .attr( "title", "" )
                .addClass( "custom-combobox-input ui-widget ui-widget-content ui-corner-left" )
                .autocomplete({
                    delay: 0,
                    minLength: 0,
                    source: $.proxy( this, "_source" )
                })
                .tooltip({
                    classes: {
                        "ui-tooltip": "ui-state-highlight"
                    }
                });

            this._on( this.input, {
                autocompleteselect: function( event, ui ) {
                    ui.item.option.selected = true;
                    this._trigger( "select", event, {
                        item: ui.item.option
                    });
                }
            });
        },

        _createShowAllButton: function() {
            var input = this.input,
                wasOpen = false;

            $( "<a>" )
                .attr( "tabIndex", -1 )
                .tooltip()
                .appendTo( this.wrapper )
                .button({
                    icons: {
                        primary: "ui-icon-triangle-1-s"
                    },
                    text: false
                })
                .removeClass( "ui-corner-all" )
                .addClass( "custom-combobox-toggle ui-corner-right" )
                .on( "mousedown", function() {
                    wasOpen = input.autocomplete( "widget" ).is( ":visible" );
                })
                .on( "click", function() {
                    input.trigger( "focus" );

                    // Close if already visible
                    if ( wasOpen ) {
                        return;
                    }

                    // Pass empty string as value to search for, displaying all results
                    input.autocomplete( "search", "" );
                });
        },

        _source: function( request, response ) {
            var matcher = new RegExp( $.ui.autocomplete.escapeRegex(request.term), "i" );
            response( this.element.children( "option" ).map(function() {
                var text = $( this ).text();
                if ( this.value && ( !request.term || matcher.test(text) ) )
                    return {
                        label: text,
                        value: text,
                        option: this
                    };
            }) );
        },

        _destroy: function() {
            this.wrapper.remove();
            this.element.show();
        }
    });

    $(".input_reason").combobox();
    $(".input_reason").closest("td").find(".custom-combobox-input").addClass("textarea_reason");

    $(".input_remark").combobox();
    $(".input_remark").closest("td").find(".custom-combobox-input").addClass("textarea_remark");

    $('.custom-combobox .custom-combobox-input').on("autocompletechange", function() {
        var reason = $(this).closest("td").find(".text_input");
        reason.val($(this).val());
    });

    $('select.select_status').each(function() {
        var input_reason = $(this).closest("tr").find("select.input_reason");
        var reason = $(input_reason).closest("td").find(".reason");
        $(input_reason).closest("td").find(".textarea_reason").val(reason.val());
        var input_remark = $(this).closest("tr").find("select.input_remark");
        var remark = $(input_remark).closest("td").find(".remark");
        $(input_remark).closest("td").find(".textarea_remark").val(remark.val());
        updateReasonOption(this);
    });

    $('select.select_status').change(function() {
        updateReasonOption(this);
    });

    $( ".presetabsent_tooltip" ).tooltip({
        show: null,
        open: function(event, ui)
        {
            if (typeof(event.originalEvent) === 'undefined')
            {
                return false;
            }
            var $id = $(ui.tooltip).attr('id');
            $('div.ui-tooltip').not('#' + $id).remove();
        },
        close: function(event, ui)
        {
            ui.tooltip.hover(function()
            {
                $(this).stop(true).fadeTo(400, 1);
            },
            function()
            {
                $(this).fadeOut(400, function()
                {
                    $(this).remove();
                });
            });
        }
    });

});

function updateReasonOption(obj) {
    var input_reason = $(obj).closest("tr").find("select.input_reason");
    $(input_reason).find("option").remove();

    <?php if($kis_data['reason_js'] != '') { ?>
    var status = $(obj).val();
    if(status == 'absent') {
        createOptionFromJSON(input_reason, jArrayWordsAbsence);
    } else if(status == 'outing') {
        createOptionFromJSON(input_reason, jArrayWordsOuting);
    } else if(status == 'late') {
        createOptionFromJSON(input_reason, jArrayWordsLate);
    } else if(status == 'earlyleave') {
        createOptionFromJSON(input_reason, jArrayWordsEarly);
    } else if(status == 'lateandearlyleave') {
        createOptionFromJSON(input_reason, jArrayWordsLateEarly);
    }
    <?php } ?>
}

function createOptionFromJSON(obj, data) {
    $.each(data, function(k,v) {
        $(obj).append($("<option></option>")
            .attr("value", v)
            .text(v));
    });
}
</script>

<div class="main_content_detail">
    <p class="spacer"></p>
                      
    <form class="filter_form">
	<div id="table_filter">
	
	    <input name="date" readonly value="<?=$date?>" size="15" type="text"/>
	   
	    <select name="time">
		<?php 
		/*
		<option value="am" <?=$time=='am'?'selected':''?>>AM</option>
		<option value="pm" <?=$time=='pm'?'selected':''?>>PM</option>
		*/
		for($i=0;$i<count($kis_data['attendance_mode_data']);$i++){
		 	echo '<option value="'.$kis_data['attendance_mode_data'][$i][0].'" '.($time == $kis_data['attendance_mode_data'][$i][0]?'selected':'').'>'.$kis_data['attendance_mode_data'][$i][1].'</option>';
		}
		?>
	    </select>
	    <select name="class_id">
		<? foreach ($classes as $class): ?>
		    <option <?=$class['class_id']==$class_id?'selected':''?> value="<?=$class['class_id']?>"><?=$class['class_name_'.$lang]?></option>
		
		<? endforeach; ?>
	    </select>
	    <input name="" class="formsmallbutton" value="<?=$kis_lang['go']?>" type="submit"/>
	</div>
	<div class="search"><!--<a href="#">Advanced</a>-->
	    <input name="search" placeholder="<?=$kis_lang['search']?>" value="<?=$search?>" type="text">
	</div>
    </form>
    <p class="spacer"></p>
    <div class="table_board">
	<? if ($students): ?>
		<?php
		if($kis_data['class_confirm_record']['RecordID'] != '') {
			echo $kis_lang['lastupdated'].': '.$kis_data['class_confirm_record']['DateModified'].' '.$kis_lang['by'].' '.$kis_data['class_confirm_record']['ConfirmedUser'];
		}
		?>
        <div class="common_table_tool">
	    <a class="tool_set edit" style="display:none" href="#"><?=$kis_lang['setabsenttopresent']?></a>
	    <a href="#" class="view tool_edit"><?=$kis_lang['edit']?></a>
	    <a href="#" class="edit tool_edit" style="display:none"><?=$kis_lang['finish']?></a>
	</div>
	<? endif; ?>
        <table class="common_table_list edit_table_list">
	    <colgroup>
		<col style="width:3%"/>
		<col style="width:15%"/>
        <?php if($sys_custom['StudentAttendance']['RecordBodyTemperature']) { ?>
        <col style="width:5%"/>
        <?php } ?>
		<col style="width:15%"/>
		<col style="width:15%"/>
		<col style="width:15%"/>	
		<col style="width:15%"/>
		<col style="width:15%"/>
	    </colgroup>
	    <thead>
		<tr>
		    <th><? kis_ui::loadSortButton('user_class_number','#', $sortby, $order)?></th>
		    <th><? kis_ui::loadSortButton('user_name_'.$lang,'student', $sortby, $order)?></th>
            <?php if($sys_custom['StudentAttendance']['RecordBodyTemperature']){ ?>
            <th><?=$Lang['StudentAttendance']['BodyTemperature']?></th>
            <?php } ?>
		    <th><? kis_ui::loadSortButton($apm.'_status','status', $sortby, $order)?></th>
		    <th nowrap><? kis_ui::loadSortButton('in_school_time','in', $sortby, $order)?>
		    	<br /><span class="edit" style="display:none"><?=$master_in_time_selection?>
		    	<a href="javascript:void(0);" onclick="masterTimeChanged(document.getElementById('master_in_school_hour'),'in_school_hour');masterTimeChanged(document.getElementById('master_in_school_min'),'in_school_min');masterTimeChanged(document.getElementById('master_in_school_sec'),'in_school_sec');"><img width="12" border="0" height="12" src="/images/<?=$LAYOUT_SKIN?>/icon_assign.gif"></a>
		    	</span>
		    </th>
		    <th nowrap><? kis_ui::loadSortButton('leave_school_time','out', $sortby, $order)?>
		    	<br /><span class="edit" style="display:none"><?=$master_out_time_selection?>
		    	<a href="javascript:void(0);" onclick="masterTimeChanged(document.getElementById('master_leave_school_hour'),'leave_school_hour');masterTimeChanged(document.getElementById('master_leave_school_min'),'leave_school_min');masterTimeChanged(document.getElementById('master_leave_school_sec'),'leave_school_sec')"><img width="12" border="0" height="12" src="/images/<?=$LAYOUT_SKIN?>/icon_assign.gif"></a>
		    	</span>
		    </th>
		    <th><? kis_ui::loadSortButton($apm.'_reason','reason', $sortby, $order)?></th>
		    <th><? kis_ui::loadSortButton($apm.'_remark','remark', $sortby, $order)?></th>
		</tr>
	    </thead>
	    <tbody>
		
	<? if ($students): ?>
        <? $index = 0;?>
		<? foreach ($students as $i=>$student): ?>

		<tr class="attendance<?=$student[$apm.'_status']?>">
		    <td><?=$student['user_class_number']?></td>
		    <td><div class="stu_photo_name">
			<? if ($student['user_photo']): ?>
			<img src="<?=$student['user_photo']?>"/>
			<? endif; ?>
			<input type="hidden" class="student_id" value="<?=$student['user_id']?>"/>
			<input type="hidden" class="date" value="<?=$date?>"/>
			<span><?=$student['user_name_'.$lang]?></span></div></td>
			<?php
            if($sys_custom['StudentAttendance']['RecordBodyTemperature']) {
				$body_temperature = '-';
				if(isset($bodyTemperatureRecords) && is_array($bodyTemperatureRecords)) {
					if (isset($bodyTemperatureRecords[$student['user_id']])) {
						$temperature_status = $bodyTemperatureRecords[$student['user_id']][count($bodyTemperatureRecords[$student['user_id']]) - 1]['TemperatureStatus'];
						$temperature_value = $bodyTemperatureRecords[$student['user_id']][count($bodyTemperatureRecords[$student['user_id']]) - 1]['TemperatureValue'];
						$body_temperature = '<span style="color:' . ($temperature_status == 1 ? 'red' : 'green') . '" title="' . ($temperature_status == 1 ? $Lang['StudentAttendance']['BodyTemperatureAbnormal'] : $Lang['StudentAttendance']['BodyTemperatureNormal']) . '">' . $temperature_value . ' &#8451;</span>';
					}
				}
				echo "<td>".$body_temperature."</td>";
			}
            ?>
		    <td>
				<?php
				$presetabsent_img = '';
				if (in_array($student['user_id'], $PresetAbsentStudentAry)) {
					global $i_SmartCard_DailyOperation_Preset_Absence, $i_UserName, $i_Attendance_DayType, $i_Attendance_Reason, $i_Attendance_Remark;
					$user_id = $student['user_id'];
					$displayDocumentStatus = ($PresetAbsentStudentAssoAry[$user_id]['DocumentStatus'] == '1') ? $Lang['General']['Yes'] : $Lang['General']['No'];

					$preset_table = '<table class="common_table_list presetabsent_table">
                    <thead>
                        <tr>
                            <th colspan="2">'.$i_SmartCard_DailyOperation_Preset_Absence.'</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>'.$i_UserName.'</td>
                            <td>'.$student['user_name_' . $lang].'</td>
                        </tr>
                        <tr>
                            <td>'.$i_Attendance_DayType.'</td>
                            <td>'.$display_period.'</td>
                        </tr>
                        <tr>
                            <td>'.$i_Attendance_Reason.'</td>
                            <td>'.str_replace(array("'","\r\n", "\n", "\r"),array("\'","<br>", "<br>", "<br>"),$PresetAbsentStudentAssoAry[$user_id]['Reason']).'</td>
                        </tr>
                        <tr>
                            <td>'.$Lang['StudentAttendance']['ProveDocument'].'</td>
                            <td>'.$displayDocumentStatus.'</td>
                        </tr>
                        <tr>
                            <td>'.$i_Attendance_Remark.'</td>
                            <td>'.str_replace(array("'","\r\n", "\n", "\r"),array("\'","<br>", "<br>", "<br>"),$PresetAbsentStudentAssoAry[$user_id]['Remark']).'</td>
                        </tr>
                    </tbody>
                </table>';
					$preset_table = htmlspecialchars($preset_table, ENT_QUOTES);
					$presetabsent_img = '<img class="presetabsent_tooltip" src="/images/'.$LAYOUT_SKIN.'/preset_abs.gif" title="'.$preset_table.'">';
				}
				?>
			<span class="view status_view">
			<?=$student[$apm.'_status']? $kis_lang[$student[$apm.'_status']]:'--'?>
			</span>
			<span class="edit" style="display:none">
			<select class="status select_status">
			    <option <?=$student[$apm.'_status']=='present'?'selected':''?> value="present"><?=$kis_lang['present']?></option>
			    <option <?=$student[$apm.'_status']=='absent'?'selected':''?> value="absent"><?=$kis_lang['absent']?></option>
			    <option <?=$student[$apm.'_status']=='late'?'selected':''?> value="late"><?=$kis_lang['late']?></option>
			    <option <?=$student[$apm.'_status']=='earlyleave'?'selected':''?> value="earlyleave"><?=$kis_lang['earlyleave']?></option>
			    <option <?=$student[$apm.'_status']=='lateandearlyleave'?'selected':''?> value="lateandearlyleave"><?=$kis_lang['lateandearlyleave']?></option>
			    <option <?=$student[$apm.'_status']=='outing'?'selected':''?> value="outing"><?=$kis_lang['outing']?></option>
			</select>
			</span>
                <?=$presetabsent_img?>
		    </td>
		    <td>
			<span class="view in_school_time_view">
			<?php
				$in_time_field_prefix = 'in_school_';
				if($apm == 'pm'){
					if($kis_data['attendance_mode'] != 1) // not PM mode
					{
						$in_time_field_prefix = 'lunch_back_';
						echo $student['lunch_back_time']!='' && !in_array($student[$apm.'_status'],array('outing','absent'))?$student['lunch_back_time']:'--';
					}else if($student['in_school_time'] && !in_array($student[$apm.'_status'],array('outing','absent'))){
						echo $student['in_school_time'];
					}else{
						echo '--';
					}
				}else{
					echo $student['in_school_time']&&!in_array($student[$apm.'_status'],array('outing','absent'))?$student['in_school_time']:'--';
				}
			?>
			</span>
			<span class="edit" style="display:none">
			    <div class="absent_hide" <?=in_array($student[$apm.'_status'],array('outing','absent'))?'style="display:none"':''?>>
				<select class="in_school_hour">
				    <? for ($i=0; $i<=23; $i++): ?>
				    <option <?=$i==$student[$in_time_field_prefix.'hour']?'selected':''?> value="<?=sprintf('%02d',$i)?>"><?=sprintf('%02d',$i)?></option>
				    <? endfor; ?>
				</select> :
				<select class="in_school_min">
				    <? for ($i=0; $i<=59; $i++): ?>
				    <option <?=$i==$student[$in_time_field_prefix.'min']?'selected':''?> value="<?=sprintf('%02d',$i)?>"><?=sprintf('%02d',$i)?></option>
				    <? endfor; ?>
				</select> : 
				<select class="in_school_sec">
				    <? for ($i=0; $i<=59; $i++): ?>
				    <option <?=$i==$student[$in_time_field_prefix.'sec']?'selected':''?> value="<?=sprintf('%02d',$i)?>"><?=sprintf('%02d',$i)?></option>
				    <? endfor; ?>
				</select>
			    </div>
			</span>
		    </td>
		    <td>
			<span class="view leave_school_time_view">
			<?php
				$out_time_field_prefix = 'leave_school_';
				if($apm == 'am'){
					if($kis_data['attendance_mode'] == 2){ // whole day with lunch time
						$out_time_field_prefix = 'lunch_out_';
						echo $student['lunch_out_time']&&!in_array($student[$apm.'_status'],array('outing','absent'))?$student['lunch_out_time']:'--';
					}else{
						echo $student['leave_school_time']&&!in_array($student[$apm.'_status'],array('outing','absent'))?$student['leave_school_time']:'--';
					}
				}else{
					echo $student['leave_school_time']&&!in_array($student[$apm.'_status'],array('outing','absent'))?$student['leave_school_time']:'--';
				}
			?>
			</span>
			<span class="edit" style="display:none">
			    <div class="absent_hide" <?=in_array($student[$apm.'_status'],array('outing','absent'))?'style="display:none"':''?>>
				<select class="leave_school_hour">
				    <? for ($i=0; $i<=23; $i++): ?>
				    <option <?=$i==$student[$out_time_field_prefix.'hour']?'selected':''?> value="<?=sprintf('%02d',$i)?>"><?=sprintf('%02d',$i)?></option>
				    <? endfor; ?>
				</select> :
				<select class="leave_school_min">
				    <? for ($i=0; $i<=59; $i++): ?>
				    <option <?=$i==$student[$out_time_field_prefix.'min']?'selected':''?> value="<?=sprintf('%02d',$i)?>"><?=sprintf('%02d',$i)?></option>
				    <? endfor; ?>
				</select> : 
				<select class="leave_school_sec">
				    <? for ($i=0; $i<=59; $i++): ?>
				    <option <?=$i==$student[$out_time_field_prefix.'sec']?'selected':''?> value="<?=sprintf('%02d',$i)?>"><?=sprintf('%02d',$i)?></option>
				    <? endfor; ?>
				</select>
			    </div>
			</span>
		    </td>
		    <td>
			<span class="view reason_view">
			<?=$student[$apm.'_reason'] && in_array($student[$apm.'_status'],array('present'))?'--':intranet_htmlspecialchars($student[$apm.'_reason'])?>
			</span>
			<span class="edit" style="display:none">
                <span class="present_hide" <?=$student[$apm.'_status']=='present'?'style="display:none;"':''?>>
                    <textarea class="reason text_input" style="display:none;"><?=intranet_htmlspecialchars($student[$apm.'_reason'])?></textarea>
                    <select id="input_reason_<?=$index?>" class="input_reason">
                    </select>
                </span>
            </span>
		    </td>
		    <td>
			<span class="view remark_view">
			<?=$student[$apm.'_remark']?intranet_htmlspecialchars($student[$apm.'_remark']):'--'?>
			</span>
			<span class="edit" style="display:none">
			    <textarea class="remark text_input" style="display:none;"><?=intranet_htmlspecialchars($student[$apm.'_remark'])?></textarea>
			    <select id="input_remark_<?=$index?>" class="input_remark">
                    <? foreach($kis_data['teacher_remark_words'] as $word) : ?>
                        <option value="<?=$word?>"><?=$word?></option>
					<? endforeach; ?>
                </select>
            </span>
		    </td>
		</tr>
		<? $index++?>
		<? endforeach; ?>
		
	    </tbody>
	</table>
	<div class="edit_bottom">
        <input class="edit formbutton" value="<?=$kis_lang['submit']?>" type="button" style="display:none" id="submit_btn" />
        <input class="edit formsubbutton" value="<?=$kis_lang['cancel']?>" type="button" style="display:none" id="cancel_btn" />
    </div>
	 <? else: ?>
	</tbody></table>
	    <? kis_ui::loadNoRecord() ?>
	<? endif; ?>
        <p class="spacer"></p><p class="spacer"></p><br>
    </div>
</div> 