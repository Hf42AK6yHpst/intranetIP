<?php
// Editing by 
/*
 * 2017-08-22 (Carlos): Pass settings option into js kis.eattendance.takeattendance_classes_init().
 * 2016-01-04 (Carlos): Changed the generation of AM/PM selection base on $kis_data['attendance_mode_data'].
 * 2015-05-12 (Carlos): Added present counting to each class block.
 * 2015-04-21 (Carlos): Added statistics at the page bottom.
 * 2014-02-27 (Carlos): Use confirm tiem and person from $classes_confirm_record
 */

?>
<script>
$(function(){
    kis.eattendance.takeattendance_classes_init(
    	{
    		"CannotTakeFutureDateRecord":"<?=$kis_data['CannotTakeFutureDateRecord']?>",
    		"CannotTakePastDateRecord":"<?=$kis_data['DisableISmartCardPastDate']?>",
    		"EditDaysBeforeRecord":"<?=$kis_data['EditDaysBeforeRecord']?>",
    		"EditDaysBeforeRecordSelect":"<?=$kis_data['EditDaysBeforeRecordSelect']?>"
    	}
    );
});
</script>

<div class="main_content_detail">
    <p class="spacer"></p>
                      
    <form class="filter_form">
	<div id="table_filter">
	    <input name="date" readonly value="<?=$date?>" size="15" type="text"/>
	    
	    <select name="time">
	    <?php 
		/*
		 	<option value="am" <?=$time=='am'?'selected':''?>>AM</option>
			<option value="pm" <?=$time=='pm'?'selected':''?>>PM</option>
		 */
		 for($i=0;$i<count($kis_data['attendance_mode_data']);$i++){
		 	echo '<option value="'.$kis_data['attendance_mode_data'][$i][0].'" '.($time == $kis_data['attendance_mode_data'][$i][0]?'selected':'').'>'.$kis_data['attendance_mode_data'][$i][1].'</option>';
		 }
		?>
	    </select>
	    <input name="" class="formsmallbutton" value="<?=$kis_lang['go']?>" type="submit"/>
	</div>
	<div class="search"><!--<a href="#">Advanced</a>-->
	    <input name="search" placeholder="<?=$kis_lang['search']?>" value="<?=$search?>" type="text">
	</div>
    </form>
    <p class="spacer"></p>
    <div class="attendance_class_list">
	
	<?php
	$summary = array('present'=>0, 'late'=>0, 'earlyleave'=>0, 'absent'=>0, 'outing'=>0);
	?>
	
	<? foreach ($classes as $class): ?>
	<ul class="<?=$classes_confirm_record[$class['class_id']]['RecordID']? 'status_confirmed': ''?>" style="height:260px;">
	    <li class="class_name"> <a href="#/apps/eattendance/takeattendance/?class_id=<?=$class['class_id']?>&date=<?=$date?>&time=<?=$apm?>"><?=$class['class_name_'.$lang]?></a></li>
	    <li class=""><a href="javascript:void(0)" ><span><?=$kis_lang['present']?></span><em><?=$class['present']?></em></a></li>
	    <li class="row_late"><a href="javascript:void(0)"><span><?=$kis_lang['late']?></span><em><?=$class['late']?></em></a></li>
	    <li class="row_late"><a href="javascript:void(0)"><span><?=$kis_lang['earlyleave']?></span><em><?=$class['early_leave']?></em></a></li>
	    <li class="row_absent"><a href="javascript:void(0)"><span><?=$kis_lang['absent']?></span><em><?=$class['absent']?></em></a></li>
	    <li class="row_left"><a href="javascript:void(0)"><span><?=$kis_lang['outing']?></span><em><?=$class['outing']?></em></a></li>
	    <?php
	    	$total_present = $class['present'] + $class['late'];
	    	echo '<span style="display:block;font-weight:bold;padding-top:8px;float:right;">'.($total_present>0? $total_present.$kis_lang['person_present']:'').'</span><br style="clear:both;" />';
	    ?>
	    <? if ($classes_confirm_record[$class['class_id']]['RecordID']): ?>
	    <span class="date_time"><?=$kis_lang['lastupdated']?>:<br/><?=$classes_confirm_record[$class['class_id']]['DateModified']?><em>by <?=$classes_confirm_record[$class['class_id']]['ConfirmedUser']?></em></span>
	    <? endif; ?>
	    <?php
	    	$summary['present']+= $class['present'];
	    	$summary['late']+= $class['late'];
	    	$summary['earlyleave']+= $class['early_leave'];
	    	$summary['absent']+= $class['absent'];
	    	$summary['outing']+= $class['outing'];
	    ?>
	</ul>
	<? endforeach; ?>
	
    </div>
    
    <div class="attendance_class_list">
    	<ul style="height:auto;width:95%;">
    		<?php
    			$x = '<li class="class_name" style="background:none;box-shadow:none;text-decoration:underline;"><a href="javascript:void(0);" style="cursor:default;">'.$kis_lang['statistics'].'</a></li>';
    			$x .= '<li class=""><a href="javascript:void(0);" style="cursor:default;"><span>'.$kis_lang['present'].'</span><em>'.$summary['present'].'</em></a></li>';
    			$x .= '<li class="row_late"><a href="javascript:void(0);" style="cursor:default;"><span>'.$kis_lang['late'].'</span><em>'.$summary['late'].'</em></a></li>';
    			$x .= '<li class="row_late"><a href="javascript:void(0);" style="cursor:default;"><span>'.$kis_lang['earlyleave'].'</span><em>'.$summary['earlyleave'].'</em></a></li>';
    			$x .= '<li class="row_absent"><a href="javascript:void(0);" style="cursor:default;"><span>'.$kis_lang['absent'].'</span><em>'.$summary['absent'].'</em></a></li>';
    			$x .= '<li class="row_left"><a href="javascript:void(0);" style="cursor:default;"><span>'.$kis_lang['outing'].'</span><em>'.$summary['outing'].'</em></a></li>';
    			echo $x;
    		?>
    	</ul>
    </div>
</div>           