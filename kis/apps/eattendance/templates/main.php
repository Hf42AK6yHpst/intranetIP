<?php 
// Editing by 
/*
 * 2018-11-23 (Cameron): fixed: should not assign $classTeacherAppApplyleaveAry to $menuAry if it's not set
 * 2018-05-04 (Isaac) : added classTeacherAppApplyleave to the left menu
 */

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libteaching.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");

$UserID = $_SESSION["UserID"];
$lu = new libuser($UserID );
$withClass = false;
$isTeacher = $lu->isTeacherStaff();
if ($isTeacher)  # Teacher
{
    $lteaching = new libteaching();
    $class = $lteaching->returnTeacherClass($UserID);
    $lclass = new libclass();
    $classname = $class[0][1];
    if ($classname != "")
    {
        $withClass = true;
    }
}
$isClassTeacher = count((array)$class) > 0;

$menuAry = array();
$menuAry[] = 'takeattendance';

$classTeacherAppApplyleaveAry=array();
if($_SESSION["platform"]=="KIS" && $_SESSION["SSV_PRIVILEGE"]["plugin"]["attendancestudent"] 
    && $sys_custom["StudentAttendance"]["Class_Teacher_Approve_Leave_Request"] && $isClassTeacher&& $withClass){
       $classTeacherAppApplyleaveAry['isPopUp'] = true;
       $classTeacherAppApplyleaveAry['name'] = 'classTeacherAppApplyleave';
       $classTeacherAppApplyleaveAry['href'] = 'home/eAdmin/StudentMgmt/attendance/class_teacher/apply_leave_list.php';
    $menuAry[] = $classTeacherAppApplyleaveAry;
}

$menuAry[] = 'monthlyrecord';
$menuAry[] = '';
$menuAry[] = 'reminderrecord';
?>

<? if ($kis_user['type']==kis::$user_types['teacher']) : ?>

<div class="content_board_menu">
<? kis_ui::loadLeftMenu($menuAry, $q[0], "#/apps/eattendance/");?>
    <div class="main_content">
    <? kis_ui::loadTemplate($main_template, $kis_data); ?>
    </div><p class="spacer"></p>
</div>
		
<? else: ?>

<div class="main_content">
<? kis_ui::loadTemplate($main_template, $kis_data); ?>
</div> <p class="spacer"></p>

<? endif; ?>