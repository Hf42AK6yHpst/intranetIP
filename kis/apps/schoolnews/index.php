<?
// Editing by 
/*
 *
 */
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT.'kis/init.php');

$libkis_schoolnews = $libkis->loadApp('schoolnews');

### Db table config
$page   = $_GET['page']? IntegerSafe($_GET['page']): 1;
$amount = $_GET['amount']? IntegerSafe($_GET['amount']): 10;
$order  = $_GET['order']? $_GET['order']: 'desc';
$sortby = $_GET['sortby']? $_GET['sortby']: 'AnnouncementDate';
$search = standardizeFormGetValue($_GET['search']);
$newsType = standardizeFormGetValue($_GET['newsType']);


$kis_data['news_type_selection'] = $libkis_schoolnews->getSchoolNewsTypeSelection('newsTypeSel', 'newsType', $newsType);


list($total, $kis_data['data_array']) = $libkis_schoolnews->getUserRelatedSchoolNews($_SESSION['UserID'], $sortby, $order, $amount, $page, $search, $newsType);
$kis_data['num_of_data'] = count($kis_data['data_array']);


$kis_data['sortby'] = $sortby;
$kis_data['order'] = $order;
$kis_data['main_template'] = 'summary';
$kis_data['PageBar'] = array($page,$amount,$total,$sortby,$order);
kis_ui::loadTemplate('main', $kis_data, $format);
?>