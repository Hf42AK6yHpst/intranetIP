<? foreach ((array)$users as $i=>$user): ?>
    <span class="mail_user">
	
	<div class="mail_user_name">
	<?=$user['user_name_'.$lang]?$user['user_name_'.$lang]:$user['user_name_en']?> (<?=$user['user_class_name']?$user['user_class_name'].' ':''?><?=$kis_lang['userrecordtype_'.$user['user_type']]?>)
	<? 
    if ($user['children_name_en']){
    	if($user['children_name_'.$lang]){
    		$childrenNameArr = explode(',',$user['children_name_'.$lang]);
    	}
    	else{
    		$childrenNameArr = explode(',',$user['children_name_en']);
    	} 
    	$childrenClassNameArr = explode(',',$user['children_class_name']);
    	$childrenClassNumberArr = explode(',',$user['children_class_number']);
    	$displayedChidrenNameArr = array();
    	for($j=0; $j<count($childrenNameArr); $j++){
    		if(!in_array($childrenNameArr[$j], $displayedChidrenNameArr)){
    ?>
    			(<?=$childrenNameArr[$j]?><?=$childrenClassNameArr[$j]?' '.$childrenClassNameArr[$j]:''?>)
    <? 
    			$displayedChidrenNameArr[] = $childrenNameArr[$j];
    		}
    	}
    } 
    ?>
	</div>
	
	<div class="mail_icon_form">
	    <? if ($form_name): ?>
	    <input type="hidden" name="<?=$form_name?>" id="<?=$form_name?>" value="<?=$user['user_id']?>"/>
	    <? endif; ?>

	    <a href="#" class="btn_add" id="add_<?=$user['user_id']?>"></a>
		<a href="#" class="btn_remove" id="remove_<?=$user['user_id']?>"></a>

	</div>
	<p class="spacer"></p>
	
	<div class="mail_user_detail">
	    <? if ($user['user_photo']): ?>
		<img src="<?=$user['user_photo']?>"/>
	    <? endif; ?>
	    <div>
	    <?=$user['user_name_b5']?$user['user_name_b5'].'<br/>':''?>  <?=$user['user_name_en']?><br/>
	    
	    (<?=$user['user_class_name']?$user['user_class_name'].' ':''?><?=$kis_lang['userrecordtype_'.$user['user_type']]?>)
	    </div>
	    <p class="spacer"></p>
	</div>
    </span>
    <p class="spacer"></p>
<? endforeach; ?>
