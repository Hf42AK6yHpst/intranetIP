<?php
// Editing by 
/*
 * 2019-09-16 (Tommy): Added needing access or admin permission for export and import
 * 2015-02-18 (Omas): Added document.ready to change #calendar_event_list_detail height when screen height resolution < 900px 
 * 2013-10-07 (Carlos): added ClassGroupID filter to export and print links
 */
if($sys_custom['Class']['ClassGroupSettings']) {
	//include_once($intranet_root."/includes/libclassgroup.php");
	//$lclassgroup = new Class_Group();
	if($kis_user['type']==kis::$user_types['teacher']) {
		if($kis_data['class_group_id'] != '') {
			$ClassGroupIdAry = array($kis_data['class_group_id']);
		}
	}
	//else{
	//	$ClassGroupIdAry = $lclassgroup->Get_Class_GroupID_By_UserID($academic_year_id);
	//}
}

$displayed_events = array(); // store displayed events, prevent duplication
?>
<script>
$(document).ready( function() {
	var windowHeight = $(window).height();
	if(windowHeight < 750 ){
		$('#calendar_event_list_detail').height(420);
	}
});

$(function(){
    kis.calendar.calendar_menu_init({remove: '<?=$kis_lang['remove']?>'});
});
</script>
<div class="calendar_event_list">
    <div class="cal_clip"></div> 
    <div id="calendar_event_list_detail" class="calendar_event_list_detail <?=$calendar_menu_editable?'calendar_event_list_detail_edit':''?>">
	<ul>
	    
	    <? foreach ($months as $m=>$mon):
	    		$displayed_events = array(); // re-init for each month
	    ?>
			
		<li class="month_title calendar_events_month_<?=$m?>"><?=$mon['year']?> <?=$kis_lang['month_'.$mon['month']]?></li>
		<? foreach ($mon['days'] as $i=>$day):?>
		    <? foreach ($day['events'] as $event): ?>
		    <?php 
		    	if(!isset($displayed_events[$event['id']])){
		    		$displayed_events[$event['id']] = 1;
		    ?>
			    <li class="title_<?=kis_calendar::$event_types[$event["type"]]?> calendar_events_month_<?=$m?>">
				<em>
				<?=date('M j',$event['start_time'])?><?=$event['start_time']!=$event['end_time']? ' - '.date(date('M',$event['start_time'])==date('M',$event['end_time'])? 'j': 'M j',$event['end_time']):''?>
				</em>
				<span><u><?=$event['total_days']?></u></span>
				<div class="event_title"><?=$event["title"]?></div>
				<div style="display:none" class="event_id"><?=$event["id"]?></div>
				<? if ($calendar_menu_editable && false): ?>
				<div class="table_row_tool">
				    <a class="delete_dim" href="#" title="<?=$kis_lang['remove']?>"></a><a class="edit_dim" href="#"></a>
				</div>
				<? endif; ?>
				<p class="spacer"></p>
			    </li>
			<?php } ?>
		    <? endforeach; ?>
		<? endforeach; ?>
		<? if (!$mon['event_count']): ?>
		    <li class="calendar_events_month_<?=$m?>">
			<div><?=$kis_lang['norecord']?>!</div>
			<p class="spacer"></p>
		    </li>
		
		<? endif;?>
	    <? endforeach; ?>
	</ul>
    </div>         	
</div>
<? if ($calendar_menu_editable && $last_modifed_user): ?>
<div class="calendar_update"><?=$kis_lang['lastupdated']?>: <?=$last_modifed_user['name_'.$lang]?> <?=$last_modifed_user['last_modified_days']?> <?=$kis_lang['daysago'] ?></div>
<? endif; ?>
<div class="calendar_tool">
	<div class="Content_tool">
	    <? if ($calendar_menu_editable): ?>
	    <? if ($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-SchoolCalendar"]): ?>
	    <a href="/home/system_settings/school_calendar/import_holidays_events.php?AcademicYearID=<?=$academic_year_id?>" target="import_calendar" class="import"><?=$kis_lang['import']?></a>
	    <a href="/home/system_settings/school_calendar/export_holidays_events.php?AcademicYearID=<?=$academic_year_id?><?=$sys_custom['Class']['ClassGroupSettings'] && $kis_user['type']==kis::$user_types['teacher']?'&ClassGroupID='.implode(',',(array)$ClassGroupIdAry):''?>" target="export_calendar" class="export"><?=$kis_lang['export']?></a>
	    <? endif; ?>
	    <? endif; ?>
	    <a href="/home/system_settings/school_calendar/print_preview.php?p_color=0&p_month_event=&max_cal=2&academicYearId=<?=$academic_year_id?><?=$sys_custom['Class']['ClassGroupSettings'] && $kis_user['type']==kis::$user_types['teacher']?'&class_group_id='.implode(',',(array)$ClassGroupIdAry):''?>" target="print_calendar" class="print"><?=$kis_lang['print']?></a>
	
	</div>
</div>
<p class="spacer"></p>