<?php
//Using:
/**
 * Change Log:
 * 2019-02-14 Pun
 *  - File created
 */
?>
<style type="text/css">
table.common_table_list tr.move_selected td {
	background-color:#fbf786; border-top: 2px dashed #d3981a; border-bottom: 2px dashed #d3981a
}
</style>
<div class="main_content_detail">
	<?php
	    $currentPage = 'announcementsettings';
    	include 'moduleTab.php';
    ?>

    <p class="spacer"></p>
 	<? if(!empty($warning_msg)):?>
 		<?=$warning_msg?>
	<? endif; ?>
	<div class="common_table_tool">
    	<a class="tool_edit" href="#/apps/admission/briefing/announcementsettings_edit/"><?=$kis_lang['edit']?></a>
  	</div>
	<table class="form_table">
	     <tbody>
	     <tr>
	       <td class="field_title" style="width: 33%"><?=$kis_lang['announceroundapplicationresult']?></td>
	       <td><?=$basicSettings['briefingAnnouncement']>0?$kis_lang['yes']:$kis_lang['no']?></td>
	     </tr>
	   </tbody></table>
</div>
