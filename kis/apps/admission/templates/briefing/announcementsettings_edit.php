<?php
//Using:
/**
 * Change Log:
 * 2019-02-14 Pun
 *  - File created
 */
include_once($intranet_root."/lang/lang.".$intranet_session_language.".php");

?>
<script type="text/javascript">
kis.admission.interview_arrangement_init = function(){
	$('#application_settings').submit(function(){
				$.post('apps/admission/ajax.php?action=updateBriefingAnnouncementSettings', $(this).serialize(), function(success){
					$.address.value('/apps/admission/briefing/announcementsettings/?sysMsg='+success);
				});
		    return false;
		}); 	
	$('input#cancelBtn').click(function(){
		$.address.value('/apps/admission/briefing/announcementsettings/');
	});
};
$(function(){
	kis.admission.interview_arrangement_init();
});
</script>
<div class="main_content_detail">
	<?php
	    $currentPage = 'announcementsettings';
    	include 'moduleTab.php';
    ?>

<p class="spacer"></p>
<form id="application_settings">
	<?=$kis_data['NavigationBar']?>
	<p class="spacer"></p>
		<table id="FormTable" class="form_table">
        	<tbody>
			     <tr>
			       <td class="field_title" style="width:33%"><?=$kis_lang['announceroundapplicationresult']?></td>
			       <td><input type="checkbox" id="briefingAnnouncement" name="briefingAnnouncement" value="1" <?=($basicSettings['briefingAnnouncement']==1?'checked=1':'')?>"></td>
			     </tr>
            </tbody>
        </table>
        <div class="edit_bottom">
        	<input type="hidden" name="schoolYearId" id="schoolYearId" value="<?=$schoolYearID?>">
			<input type="submit" name="submitBtn" id="submitBtn" class="formbutton" value="<?=$kis_lang['submit']?>" />
            <input type="button" name="cancelBtn" id="cancelBtn" class="formsubbutton" value="<?=$kis_lang['cancel']?>" />
	    </div>
</form>
</div>