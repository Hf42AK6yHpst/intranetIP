<?php
// Editing by 

/**
 *	2015-07-31 (Henry): Created file
 **/
 
global $libkis_admission, $kis_admission_school;
?>
<script type="text/javascript" language="JavaScript">
$(function(){
    window.currentSchool = '<?=preg_replace('/\..*/', '', $kis_admission_school)?>';
	kis.admission.import_interview_arrangement_init({
		pleaseselectstatus:'<?=$kis_lang['msg']['selectapplicationstatus']?>',
		pleaseselectclasslevel:'<?=$kis_lang['Admission']['msg']['selectclass']?>',
		importinterviewinfobyarrangementornot:'<?=$kis_lang['interviewarrangementremarks'][0].'\n'.$kis_lang['msg']['importinterviewinfobyarrangement']?>'
	});
})
</script>
<div class="main_content_detail">
<form id="importForm" name="importForm" action='./apps/admission/ajax.php?action=importInterviewInfoByArrangement' method="post" enctype="multipart/form-data">
	<?php
    if(method_exists($lauc, 'getAdminModuleTab')) {
        kis_ui::loadModuleTab($lauc->getAdminModuleTab('interview'), '', '#/apps/admission/interview/');
    } elseif ($sys_custom['KIS_Admission']['InterviewForm'] || $moduleSettings['enableinterviewarrangement']) {
        kis_ui::loadModuleTab(array('interviewarrangement', 'interviewformbuilter', 'interviewforminput', 'interviewformresult', 'timeslotsettings', 'interviewusersettings', 'announcementsettings'), '', '#/apps/admission/interview/');
    } else if ($sys_custom['KIS_Admission']['InterviewArrangement']) {
        kis_ui::loadModuleTab(array('interviewarrangement', 'timeslotsettings', 'announcementsettings'), '', '#/apps/admission/interview/');
    }
    ?>
	<?=$kis_data['NavigationBar']?>
	<p class="spacer"></p><br>
	
		<table class="form_table">
        	<tbody>
            	<tr> 
                	<td class="field_title" style="width:30%;"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['applicationstatus']?></td>
                	<td style="width:70%;">
                	<?=$applicationStatus?>
                	<span class=\"tabletextremark\">
						<?=$kis_lang['CtrlMultiSelectMessage']?>
					</span>
                	</td>
                </tr>
                <!--<tr> 
                	<td class="field_title" style="width:30%;"><?=$kis_lang['Remark']?></td>
                	<td style="width:70%;">
                	<?
                	$ImportRemarkArr = $kis_lang['interviewarrangementremarks'];
					$ImportRemark = '<table cellpadding=0 cellspacing=0 border=0>';	
					for($i=0; $i < count($ImportRemarkArr); $i++)
						$ImportRemark .= '<tr><td valign=top width=20px class="tabletext">'.($i+1).'. </td><td>'.$ImportRemarkArr[$i].'</td></tr>';	 
					$ImportRemark .= '</table>';
					echo $ImportRemark;
					?>
                	</td>
                </tr>-->
                <?php if($sys_custom['KIS_Admission']['InterviewArrangementUseClassLevelFilter']){ ?>
                	<tr> 
                    	<td class="field_title" style="width:30%;"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['level']?></td>
                    	<td style="width:70%;">
    						<?php foreach($libkis_admission->classLevelAry as $classLevelId => $classLevel){ ?>
        						<label style="display: block;min-width:100px">
        							<input type="checkbox" name="classLevelIds[]" value="<?=$classLevelId ?>" checked/>
        							<?=$classLevel ?>
        						</label>
    						<?php } ?>
                    	</td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
        <div class="text_remark"><?=str_replace('*','<span style="color:red">*</span>',$kis_lang['requiredfield'])?></div>
        <?php if(!$sys_custom['KIS_Admission']['CREATIVE']['Settings']){ ?>
            <div class="text_remark" style="color:red">*<?=$kis_lang['remark'].': '.$ImportRemarkArr[0]?></div>
        <?php } ?>
		<div class="edit_bottom">
			<input type="hidden" name="round" id="round" value="<?=$selectInterviewRound?>" />
        	<input type="button" name="submitBtn" id="submitBtn" class="formbutton" value="<?=$kis_lang['confirm']?>" />
        	<input type="button" name="cancelBtn" id="cancelBtn" class="formsubbutton" value="<?=$kis_lang['cancel']?>" />
	    </div>
	    <!--<input type="hidden" name="schoolYearID" id="schoolYearID" value="<?=$schoolYearID?>" />-->
	    <input type="hidden" name="selectSchoolYearID" id="selectSchoolYearID" value="<?=$schoolYearID?>" />
        <p class="spacer"></p><br>  
	
</form>
</div>
