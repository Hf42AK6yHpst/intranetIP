<?php
// Editing by Henry
/****************************************
 *	2015-10-09 (Henry):	file created
 ****************************************/
?>
<script>
kis.admission.interview_user_settings_init({
	returnmsg:'<?=$kis_lang['Admission']['ReturnMessage'][$sysMsg]?>',
	msg_confirm_delete_email_list:'<?=$kis_lang['msg']['confirm_delete_interview_user']?>',
	msg_selectatleastonerecord:'<?=$kis_lang['msg']['selectonlyonerecord']?>',
	selectatleastonerecord:'<?=$kis_lang['msg']['selectonlyonerecord']?>',
	exportallrecordsornot:'<?=$kis_lang['Admission']['msg']['exportallrecordsornot']?>',
	printallrecordsornot:'<?=$kis_lang['Admission']['msg']['printallrecordsornot']?>',
	sendemailallrecordsornot:'<?=$kis_lang['Admission']['msg']['sendemailallrecordsornot']?>',
	importinterviewallrecordsornot:'<?=$kis_lang['Admission']['msg']['importinterviewallrecordsornot']?>',
	returnmsg:'<?=$kis_lang['Admission']['ReturnMessage'][$sysMsg]?>',
	close:'<?=$kis_lang['close']?>',
	importinterviewinfobyarrangementornot:'<?=$kis_lang['msg']['importinterviewinfobyarrangement']?>',
	cannoteditordeleteinterviewform:'<?=$kis_lang['msg']['cannoteditordeleteinterviewform']?>'
});
</script>
<div class="main_content_detail">
	<?php
    if (method_exists($lauc, 'getAdminModuleTab')) {
        kis_ui::loadModuleTab($lauc->getAdminModuleTab('interview'), 'interviewusersettings', '#/apps/admission/interview/');
    } elseif ($sys_custom['KIS_Admission']['InterviewForm'] || $moduleSettings['enableinterviewform']) {
        kis_ui::loadModuleTab(array('interviewarrangement', 'interviewformbuilter', 'interviewforminput', 'interviewformresult', 'timeslotsettings', 'interviewusersettings', 'announcementsettings'), 'interviewusersettings', '#/apps/admission/interview/');
    }
    ?>
 	 <p class="spacer"></p>
 	<? if(!empty($warning_msg)):?>
 		<?=$warning_msg?>
	<? endif; ?>
	<div class="Content_tool">
		<a href="#" class="tool_new new"><?=$kis_lang['new']?></a>
	</div>
	 
                      	  <div class="table_board">
                      	  <div class="common_table_tool common_table_tool_table">
                <!--<a href="#" class="tool_edit"><?=$kis_lang['edit']?></a>-->
				<a href="#" class="tool_delete"><?=$kis_lang['delete']?></a>
            </div>
            <p class="spacer"></p>
             <form id="user_list" name="user_list" method="post">
                      <table class="common_table_list edit_table_list">
                        <colgroup><col nowrap="nowrap">
                        </colgroup><thead>
                          <tr>
                            <th width="20">&nbsp;</th>
                            <th><?=$kis_lang['marker']?></th>
                            <th><input type="checkbox" name="checkmaster" onclick="(this.checked)?setChecked(1,this.form,'userAry[]'):setChecked(0,this.form,'userAry[]')"></th>
                          </tr>
                        </thead>
                        <tbody>
                  	<? 
                  		$idx = 0;
                  		//if(sizeof($interviewUserSettings) > 0){
			      		foreach($interviewUserSettings as $_classLevelId => $_classLevelRecord): 
			      				$idx++;
			      	?>
							 <tr>
	                            <td><?=$idx?></td>
	                            <td><?=$_classLevelRecord['UserName']?></td>
	                            <td><?=$libinterface->Get_Checkbox('user_'.$_classLevelRecord['UserID'], 'userAry[]', $_classLevelRecord['UserID'], '', $Class='', $Display='', $Onclick="unset_checkall(this, document.getElementById('user_list'));", $Disabled='')?></td>
	                          </tr>
					<? 		//}
						endforeach;
                  		//} 
					if($idx == 0){?>
                  		<tr>
                  			<td colspan="7" style="text-align:center;"><?=$kis_lang['norecord']?></td>
                  		</tr>  
					<?}?>	  
                        </tbody>
                      </table>
                      <input type="hidden" name="schoolYearID" id="schoolYearID" value="<?=$schoolYearID?>">
					</form>
                      <p class="spacer"></p>
                      <p class="spacer"></p>
                      <br>
                    </div></div>