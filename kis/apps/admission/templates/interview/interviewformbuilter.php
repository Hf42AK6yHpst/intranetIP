<?php
// Editing by Henry
?>
<script>
kis.admission.interview_form_builter_init({
	returnmsg:'<?=$kis_lang['Admission']['ReturnMessage'][$sysMsg]?>',
	msg_confirm_delete_email_list:'<?=$kis_lang['msg']['deleteinterviewtimeslot']?>',
	msg_selectatleastonerecord:'<?=$kis_lang['msg']['selectonlyonerecord']?>',
	selectatleastonerecord:'<?=$kis_lang['msg']['selectonlyonerecord']?>',
	exportallrecordsornot:'<?=$kis_lang['Admission']['msg']['exportallrecordsornot']?>',
	printallrecordsornot:'<?=$kis_lang['Admission']['msg']['printallrecordsornot']?>',
	sendemailallrecordsornot:'<?=$kis_lang['Admission']['msg']['sendemailallrecordsornot']?>',
	importinterviewallrecordsornot:'<?=$kis_lang['Admission']['msg']['importinterviewallrecordsornot']?>',
	returnmsg:'<?=$kis_lang['Admission']['ReturnMessage'][$sysMsg]?>',
	close:'<?=$kis_lang['close']?>',
	importinterviewinfobyarrangementornot:'<?=$kis_lang['msg']['importinterviewinfobyarrangement']?>',
	cannoteditordeleteinterviewform:'<?=$kis_lang['msg']['cannoteditordeleteinterviewform']?>'
});
</script>
<div class="main_content_detail">
	<?php
    if (method_exists($lauc, 'getAdminModuleTab')) {
        kis_ui::loadModuleTab($lauc->getAdminModuleTab('interview'), 'interviewformbuilter', '#/apps/admission/interview/');
    } elseif ($sys_custom['KIS_Admission']['InterviewForm'] || $moduleSettings['enableinterviewform']) {
        kis_ui::loadModuleTab(array('interviewarrangement', 'interviewformbuilter', 'interviewforminput', 'interviewformresult', 'timeslotsettings', 'interviewusersettings', 'announcementsettings'), 'interviewformbuilter', '#/apps/admission/interview/');
    }
    ?>
 	 <p class="spacer"></p>
 	<? if(!empty($warning_msg)):?>
 		<?=$warning_msg?>
	<? endif; ?>
	<div class="Content_tool">
		<a href="#" class="tool_new new"><?=$kis_lang['new']?></a>
	    <!--<div class="btn_option"  id="ExportDiv">
	    	<a id="btn_import" class="tool_import import" href="javascript:void(0);"><?=$kis_lang['importinterviewtimeslotinfo']?></a>
			<?if($sys_custom['KIS_Admission']['InterviewArrangement']){?>
			<a id="btn_import_by_arragement" class="tool_import_by_arragement import" href="javascript:void(0);"><?=$kis_lang['importinterviewinfobyarrangement']?></a>
			<?}?>
			<a id="btn_import" class="tool_export export" href="javascript:void(0);"><?=$kis_lang['exportAppliedDetails']?></a>
		</div>-->  
	</div>
	<div id="table_filter">
    <form class="filter_form"> 
        <?=$schoolYearSelection?>
        <!--<?=$libadmission->Get_Number_Selection('selectInterviewRound', '1', '3', $selectInterviewRound, '', 1, 0, '', 0, $kis_lang['round'])?>-->
    </form>
    </div>
	 <!--<form class="filter_form"> 
    	<div id="table_filter">
        	<?=$schoolYearSelection?>
        	<?=$classLevelSelection?>
   		</div>	
		<div class="search">
		    <input placeholder="<?=$kis_lang['search'] ?>" name="keyword" value="<?=$keyword?>" type="text"/>
		</div>
    </form>-->
    
                     <!---->
                    	
                      <!--<p class="spacer"></p>-->
                      	  <div class="table_board">
                      	  <div class="common_table_tool common_table_tool_table">
                <a href="#" class="tool_edit"><?=$kis_lang['edit']?></a>
				<a href="#" class="tool_delete"><?=$kis_lang['delete']?></a>
            </div>
            <p class="spacer"></p>
             <form id="interview_list" name="interview_list" method="post">
                      <table class="common_table_list edit_table_list">
                        <colgroup><col nowrap="nowrap">
                        </colgroup><thead>
                          <tr>
                            <th width="20">&nbsp;</th>
                            <th><?=$kis_lang['form']?></th>
                            <th><?=$kis_lang['email_template_title']?></th>
                            <th><?=$kis_lang['from']?> </th>
                            <th><?=$kis_lang['to']?></th>
                            <th><input type="checkbox" name="checkmaster" onclick="(this.checked)?setChecked(1,this.form,'interviewAry[]'):setChecked(0,this.form,'interviewAry[]')"></th>
                          </tr>
                        </thead>
                        <tbody>
                  	<? 
                  		$idx = 0;
			      		foreach($interviewSettingAry as $_classLevelId => $_classLevelRecord): 
			      			//foreach($_classLevelAry as $_classLevelRecord){
			      				$idx++;
			      				$SchoolYearArr = current(kis_utility::getAcademicYears(array("AcademicYearID"=>$_classLevelRecord['ClassLevelID'])));
			      				$formName = $SchoolYearArr['academic_year_name_'.$intranet_session_language];			
			      	?>
							 <tr>
	                            <td><?=$idx?></td>
	                            <td><a class="td_form_name" href="#/apps/admission/interview/interviewformbuilter_edit/<?=$_classLevelRecord['RecordID']?>/">
	                            <?
	                            $classIDArr = explode(',',$_classLevelRecord['ClassLevelID']);
	                            foreach($classIDArr as $aClass){
	                            	echo $classLevelAry[$aClass].'<br/>';
	                            }
	                            ?></a></td>
	                            <td><?=$_classLevelRecord['Title']?></td>
	                            <td><?=substr($_classLevelRecord['StartDate'], 0, -3)?></td>
	                            <td><?=substr($_classLevelRecord['EndDate'], 0, -3)?></td>
	                            <td><?=$libinterface->Get_Checkbox('interview_'.$_classLevelRecord['RecordID'], 'interviewAry[]', $_classLevelRecord['RecordID'], '', $Class='', $Display='', $Onclick="unset_checkall(this, document.getElementById('interview_list'));", $Disabled='')?>
	                            <input type="hidden" name="applied_<?=$_classLevelRecord['RecordID']?>" id="applied_<?=$_classLevelRecord['RecordID']?>" value="<?=$_classLevelRecord['ResultCount']?>">
	                            </td>
	                          </tr>
					<? 		//}
						endforeach; 
					if($idx == 0){?>
                  		<tr>
                  			<td colspan="7" style="text-align:center;"><?=$kis_lang['norecord']?></td>
                  		</tr>  
					<?}?>	  
                        </tbody>
                      </table>
                      <input type="hidden" name="schoolYearID" id="schoolYearID" value="<?=$schoolYearID?>">
					</form>
                      <p class="spacer"></p>
                      <p class="spacer"></p>
                      <br>
                    </div></div>