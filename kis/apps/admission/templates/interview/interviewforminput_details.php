<?php
// Editing by Henry
?>
<script>
kis.admission.interview_form_input_details_init({
	returnmsg:'<?=$kis_lang['Admission']['ReturnMessage'][$sysMsg]?>',
	msg_confirm_delete_email_list:'<?=$kis_lang['Admission']['msg']['confirm_delete_email_list']?>',
	msg_selectatleastonerecord:'<?=$kis_lang['Admission']['msg']['selectatleastonerecord']?>'
});
</script>
<div class="main_content_detail">
    <?php
    if (method_exists($lauc, 'getAdminModuleTab')) {
        kis_ui::loadModuleTab($lauc->getAdminModuleTab('interview'), 'interviewforminput', '#/apps/admission/interview/');
    } elseif (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-eAdmission"] && $_SESSION["SSV_USER_ACCESS"]["eAdmin-eAdmission-interview-marker"]) {
        kis_ui::loadModuleTab(array('interviewforminput'), 'interviewforminput', '#/apps/admission/interview/');
    } else if ($sys_custom['KIS_Admission']['InterviewForm'] || $moduleSettings['enableinterviewform']) {
        kis_ui::loadModuleTab(array('interviewarrangement', 'interviewformbuilter', 'interviewforminput', 'interviewformresult', 'timeslotsettings', 'interviewusersettings', 'announcementsettings'), 'interviewforminput', '#/apps/admission/interview/');
    }
    ?>
	<?=$kis_data['NavigationBar']?>
 	 <p class="spacer"></p>
 	<? if(!empty($warning_msg)):?>
 		<?=$warning_msg?>
	<? endif; ?>
	<div class="Content_tool">
	    <!--<div class="btn_option"  id="ExportDiv">
			<a id="btn_import" class="tool_import_interview export" href="javascript:void(0);"><?=$kis_lang['export']?></a>
		</div>-->  
	</div>
	 <!--<form class="filter_form"> 
    	<div id="table_filter">
        	<?=$schoolYearSelection?>
        	<?=$classLevelSelection?>
   		</div>	
		<div class="search">
		    <input placeholder="<?=$kis_lang['search'] ?>" name="keyword" value="<?=$keyword?>" type="text"/>
		</div>
    </form>-->
    
                     <!---->
                    	
                      <!--<p class="spacer"></p>-->
                      	  <div class="table_board">
                      	  <!--<div class="common_table_tool common_table_tool_table">
                <a href="#" class="tool_new"><?=$kis_lang['new']?></a>
                <a href="#" class="tool_edit"><?=$kis_lang['edit']?></a>
				<a href="#" class="tool_delete"><?=$kis_lang['delete']?></a>
            </div>-->
            <p class="spacer"></p>
            
            <form id="application_form" name="application_form" method="post">
	  	    <table class="common_table_list edit_table_list">
			<colgroup><col nowrap="nowrap">
			</colgroup><thead>
				<tr>
				  <th width="20">#</th>
				  <th><?kis_ui::loadSortButton('application_id','applicationno', $sortby, $order)?></th>
				  <th><?kis_ui::loadSortButton('student_name','studentname', $sortby, $order)?></th>
				  <th><?=$kis_lang['Admission']['personalPhoto']?></th>
				  <!--<th><?=$kis_lang['parentorguardian']?></th>
				  <th><?=$kis_lang['phoneno']?></th>
				  <?=$sys_custom['KIS_Admission']['ICMS']['Settings']?'<th>'.$kis_lang['Admission']['icms']['applyDayType'].'</th>':''?>
				  <th><?kis_ui::loadSortButton('application_status','status', $sortby, $order)?></th>-->
				   <th><?=$kis_lang['marker']?></th>
				   <th></th>
				  <!--<th><input type="checkbox" name="checkmaster" onclick="(this.checked)?setChecked(1,this.form,'applicationAry[]'):setChecked(0,this.form,'applicationAry[]')"></th>-->
			  </tr>
			</thead>
			<tbody>
				<?php 
					if(count($applicationDetails)>0){
						$idx=$amount*($page-1);
						foreach($applicationDetails as $_applicationId => $_applicationDetailsAry){
							$idx++;
							$_recordId = $_applicationDetailsAry['other_record_id'];
							$_status = $_applicationDetailsAry['application_status'];
							$_studentName = $_applicationDetailsAry['student_name'];		
							$_parentName = implode('<br/>',array_filter($_applicationDetailsAry['parent_name']));	
							$_parentPhone = implode('<br/>',array_filter($_applicationDetailsAry['parent_phone']));
							
							$_inputBy = '--';
							if($_applicationDetailsAry['InputBy']){
								foreach($_applicationDetailsAry['InputBy'] as $inputInfo){
									$dateModifiedText = '';
									if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eAdmission"])
										$dateModifiedText = ' ('.$inputInfo['DateModified'].')';
									
									$_inputBy = $inputInfo['InputBy'].$dateModifiedText.'<br/>';
								}
							}
							
							if($sys_custom['KIS_Admission']['ICMS']['Settings']){
								$classlevel = $applicationlistAry[$kis_data['classLevelID']]['ClassLevelName'];
								$_applyDayType = '<td>';
								for($i=1;$i<=3;$i++){
									if($_applicationDetailsAry['ApplyDayType'.$i])
										$_applyDayType .= '('.$i.') '.$kis_lang['Admission']['icms'][$classlevel]['TimeSlot'][$_applicationDetailsAry['ApplyDayType'.$i]].'<br/>';																				
								}
								$_applyDayType .= '</td>';
							}
							$tr_css = '';
							switch($_status){
								case 'pending':$tr_css = ' class="absent"';break;
							//	case 'paymentsettled':$tr_css = '';break;
								case 'waitingforinterview':$tr_css = ' class="waiting"';break;
								case 'confirmed':$tr_css = ' class="done"';break;
								case 'cancelled':$tr_css = ' class="draft"';break;								
							}
				?>			
							<tr<?=$tr_css?>>
							  <td><?=$idx?></td>
							  <td><a href="#/apps/admission/applicantslist/details/<?=$schoolYearID?>/<?=$_recordId?>/"><?=$_applicationId?></a><br></td>
							  <td><?=$sys_custom['KIS_Admission']['ICMS']['Settings'] || $sys_custom['KIS_Admission']['CSM']['Settings']?str_replace(",", " ", $_studentName):$_studentName?></td>
							  <td><img style="max-width: 300px;" src="<?=$_applicationDetailsAry['PhotoLink']?$_applicationDetailsAry['PhotoLink']:$blankphoto?>?_=<?=time()?>"/></td>
							  <!--<td><?=$_parentName?($sys_custom['KIS_Admission']['ICMS']['Settings']?str_replace(",", " ", $_parentName):$_parentName):'--'?></td>
							  <td nowrap="nowrap"><?=$_parentPhone?$_parentPhone:'--'?></td>
							  <?=$sys_custom['KIS_Admission']['ICMS']['Settings']?$_applyDayType:''?>
							  <td><?=$kis_lang['Admission']['Status'][$_status]?></td>-->
							  <td><?
							  $inputByArr = explode(',',$interviewFormResultAry[$_recordId]['name']);
							  $inputByIDArr = explode(',',$interviewFormResultAry[$_recordId]['id']);
							  $dateModifiedIDArr = explode(',',$interviewFormResultAry[$_recordId]['datemodified']);
							  //debug_pr($inputByIDArr);
							  if(!$interviewFormResultAry[$_recordId]['id']){
							  	echo '--';
							  }
							  else{
	                            for($i=0; $i < sizeof($inputByArr); $i++){
	                            	echo '<a href="#/apps/admission/interview/interviewforminput_details_view/'.$selectInterviewRound.'/'.($selectInterviewDate?$selectInterviewDate:0).'/'.$recordID.'/'.$_recordId.'/'.$inputByIDArr[$i].'/">'.$inputByArr[$i].' ('.$dateModifiedIDArr[$i].')</a><br/>';
	                            }
							  }
								?></td>
							  <td>
							  <?if(!$interviewFormSettingAry['RecordID'] || $interviewFormResultAry[$_recordId]['formid'] && $interviewFormResultAry[$_recordId]['formid'] != $interviewFormSettingAry['RecordID']){?>
							  --
							  <?}else{?>
							  <a href="#/apps/admission/interview/interviewforminput_details_add/<?=$selectInterviewRound?>/<?=($selectInterviewDate?$selectInterviewDate:0)?>/<?=$recordID?>/<?=$_recordId?>/"><?=$kis_lang['addoreditresult']?></a>
							  <?}?>
							  </td>
							  <!--<td><?=$libinterface->Get_Checkbox('application_'.$_recordId, 'applicationAry[]', $_recordId, '', $Class='', $Display='', $Onclick="unset_checkall(this, document.getElementById('application_form'));", $Disabled='')?></td>-->
							 </tr>
				<?php } 
					}else{ //no record	
				?>		
							<tr>
							<?if($sys_custom['KIS_Admission']['ICMS']['Settings']){?>
							  <td colspan="8" style="text-align:center;"><?=$kis_lang['norecord']?></td>
							<?}else{?>
							  <td colspan="7" style="text-align:center;"><?=$kis_lang['norecord']?></td>
							<?}?>
							</tr>				
				<?php } ?>		
							</tbody>
						</table>
						<input type="hidden" name="schoolYearID" id="schoolYearID" value="<?=$schoolYearID?>">
						<input type="hidden" name="classLevelID" id="classLevelID" value="<?=$classLevelID?>">
					</form>
					
             <!--<form id="interview_list" name="interview_list" method="post">
                      <table class="common_table_list edit_table_list">
                        <colgroup><col nowrap="nowrap">
                        </colgroup><thead>
                          <tr>
                            <th width="20">&nbsp;</th>
                            <th><?=$kis_lang['interviewdate']?></th>
                            <th><?=$kis_lang['timeslot']?> </th>
                            <th><?=$kis_lang['qouta']?></th>
                            <th><?=$kis_lang['applied']?></th>
                            <th><input type="checkbox" name="checkmaster" onclick="(this.checked)?setChecked(1,this.form,'interviewAry[]'):setChecked(0,this.form,'interviewAry[]')"></th>
                          </tr>
                        </thead>
                        <tbody>
                  	<? 
                  		$idx = 0; //debug_r($interviewListAry); debug_r($applicationDetails);
			      		foreach($interviewListAry as $_classLevelId => $_classLevelRecord): 
			      			//foreach($_classLevelAry as $_classLevelRecord){
			      				$idx++;	      			
			      	?>
							 <tr>
	                            <td><?=$idx?></td>
	                            <td><?=$_classLevelRecord['Date']?></td>
	                            <td><?=$_classLevelRecord['StartTime']?> ~ <?=$_classLevelRecord['EndTime']?></td>
	                            <td><?=$_classLevelRecord['Quota']?></td>
	                            <td><?=$_classLevelRecord['Applied']?>
	                            <input type="hidden" name="applied_<?=$_classLevelRecord['RecordID']?>" id="applied_<?=$_classLevelRecord['RecordID']?>" value="<?=$_classLevelRecord['Applied']?>">
	                            </td> 
	                            <td><?=$libinterface->Get_Checkbox('interview_'.$_classLevelRecord['RecordID'], 'interviewAry[]', $_classLevelRecord['RecordID'], '', $Class='', $Display='', $Onclick="unset_checkall(this, document.getElementById('interview_list'));", $Disabled='')?></td>
	                          </tr>
					<? 		//}
						endforeach; 
					if($idx == 0){?>
                  		<tr>
                  			<td colspan="7" style="text-align:center;"><?=$kis_lang['norecord']?></td>
                  		</tr>  
					<?}?>	  
                        </tbody>
                      </table>
                      <input type="hidden" name="schoolYearID" id="schoolYearID" value="<?=$schoolYearID?>">
					</form>-->
                      <p class="spacer"></p>
                      <p class="spacer"></p>
                      <br>
                    </div></div>