<?php
	//modifying by Henry

/**
 *  2015-08-14 (Henry): file create
 */
?>
<?php
list($page,$amount,$total,$sortby,$order,$keyword) = $kis_data['PageBar'];

# export
$menuBar = '<a id="btn_import" class="tool_export export" href="javascript:void(0);">'.$kis_lang['export'].'</a>';
?>

<script type="text/javascript">
kis.admission.interview_form_result_init({
	selectatleastonerecord:'<?=$kis_lang['Admission']['msg']['selectatleastonerecord']?>',
	exportallrecordsornot:'<?=$kis_lang['Admission']['msg']['exportallrecordsornot']?>',
	printallrecordsornot:'<?=$kis_lang['Admission']['msg']['printallrecordsornot']?>',
	sendemailallrecordsornot:'<?=$kis_lang['Admission']['msg']['sendemailallrecordsornot']?>',
	importinterviewallrecordsornot:'<?=$kis_lang['Admission']['msg']['importinterviewallrecordsornot']?>'
});
function js_Clicked_Option_Layer(jsOptionLayerDivID, jsOptionLayerBtnID)
{
	js_Hide_All_Option_Layer(jsOptionLayerDivID);
	js_ShowHide_Layer(jsOptionLayerDivID);

	if ($('div#' + jsOptionLayerDivID).css('visibility') == 'visible')
		$('a#' + jsOptionLayerBtnID).addClass('parent_btn');
	else
		$('a#' + jsOptionLayerBtnID).removeClass('parent_btn');
}

function js_Clicked_Option_Layer_Button(jsOptionLayerDivID, jsOptionLayerBtnID)
{
	MM_showHideLayers(jsOptionLayerDivID, '', 'hide');
	$('a#' + jsOptionLayerBtnID).removeClass('parent_btn');
}
</script>
<div class="main_content_detail"><?php
    if(method_exists($lauc, 'getAdminModuleTab')) {
        kis_ui::loadModuleTab($lauc->getAdminModuleTab('interview'), 'interviewformresult', '#/apps/admission/interview/');
    } elseif ($sys_custom['KIS_Admission']['InterviewForm'] || $moduleSettings['enableinterviewform']) {
        kis_ui::loadModuleTab(array('interviewarrangement', 'interviewformbuilter', 'interviewforminput', 'interviewformresult', 'timeslotsettings', 'interviewusersettings', 'announcementsettings'), 'interviewformresult', '#/apps/admission/interview/');
    }
    ?>
 	 <p class="spacer"></p>
 	<? if(!empty($warning_msg)):?>
 		<?=$warning_msg?>
	<? endif; ?>
	<p class="spacer"></p>
		<form class="filter_form">
			<div class="search">
				<input placeholder="<?=$kis_lang['search'] ?>" name="keyword" value="<?=$keyword?>" type="text">
			</div>
		</form>
		<!--<br style="clear:both;">
	</div>-->
	    <div class="Content_tool"><?=$menuBar?></div>
  		<p class="spacer"></p>
		<div class="table_board">
				<div id="table_filter">
					<form class="filter_form">
                        <?=$schoolYearSelection?>
                        <?=$libadmission->Get_Number_Selection('selectInterviewRound', '1', '3', $selectInterviewRound, '', 1, 0, '', 0, $kis_lang['round'])?>
                        <?=$classLevelSelection?>
                        <?=$applicationStatus?>
                        <?=$interviewStatusSelection?>
                        <?=$interviewFormQuestionSelection?>
                        <?=$interviewFormAnswerSelection?>
                        <?=$interviewDateSelection?>
					<!--<input type="hidden" name="selectSchoolYearID" id="selectSchoolYearID" value="<?=$schoolYearID?>"> -->
					</form>
				</div>
			<div class="common_table_tool common_table_tool_table">
				<a href="#" class="tool_set"><?=$kis_lang['changestatus']?></a>
				<!--<a href="#" class="tool_print"><?=$kis_lang['print']?></a>-->
            </div>
            <p class="spacer"></p>
            <form id="application_form" name="application_form" method="post">
	  	    <table class="common_table_list edit_table_list">
			<colgroup><col nowrap="nowrap">
			</colgroup>
                <thead>
                <tr>
                    <th width="20">#</th>
			        <?php if ( $sys_custom['KIS_Admission']['CREATIVE']['Settings'] ): ?>
                        <th><? kis_ui::loadSortButton( 'InterviewDate', 'interviewdate', $sortby, $order ) ?></th>
			        <?php endif; ?>
                    <th><? kis_ui::loadSortButton( 'application_id', 'applicationno', $sortby, $order ) ?></th>
                    <th><? kis_ui::loadSortButton( 'student_name', 'studentname', $sortby, $order ) ?></th>
                    <th><? kis_ui::loadSortButton( 'Marks', 'marks', $sortby, $order ) ?></th>
			        <? for ( $i = 0; $i < $interviewQuestionAnswer['maxNumOfInputBy']; $i ++ ) { ?>
                        <th><?= $kis_lang['teacher'] . ' ' . ( $i + 1 ) ?></th>
			        <? } ?>
			        <?= $sys_custom['KIS_Admission']['ICMS']['Settings'] ? '<th>' . $kis_lang['Admission']['icms']['applyDayType'] . '</th>' : '' ?>
                    <th><? kis_ui::loadSortButton( 'application_status', 'status', $sortby, $order ) ?></th>
                    <th><input type="checkbox" name="checkmaster"
                               onclick="(this.checked)?setChecked(1,this.form,'applicationAry[]'):setChecked(0,this.form,'applicationAry[]')">
                    </th>
                </tr>
                </thead>
                <tbody>
				<?php
					if($total>0){
						$idx=$amount*($page-1);
						foreach($applicationDetails as $_applicationId => $_applicationDetailsAry){
							$idx++;
							$_recordId = $_applicationDetailsAry['record_id'];
							$_status = $_applicationDetailsAry['application_status'];
							$_interviewDate = $_applicationDetailsAry['InterviewDate'];
							$_studentName = $_applicationDetailsAry['student_name'];
							$_parentName = implode('<br/>',array_filter($_applicationDetailsAry['parent_name']));
							$_parentPhone = implode('<br/>',array_filter($_applicationDetailsAry['parent_phone']));
							if($sys_custom['KIS_Admission']['ICMS']['Settings']){
								$classlevel = $applicationlistAry[$kis_data['classLevelID']]['ClassLevelName'];
								$_applyDayType = '<td>';
								for($i=1;$i<=3;$i++){
									if($_applicationDetailsAry['ApplyDayType'.$i])
										$_applyDayType .= '('.$i.') '.$kis_lang['Admission']['icms'][$classlevel]['TimeSlot'][$_applicationDetailsAry['ApplyDayType'.$i]].'<br/>';
								}
								$_applyDayType .= '</td>';
							}
							$tr_css = '';
							switch($_status){

								case $sys_custom['KIS_Admission']['MGF']['Settings'] && 'waitingforinterview':$tr_css = ' class="absent"';break;
								case $sys_custom['KIS_Admission']['MGF']['Settings'] && 'interviewed':$tr_css = ' class="waiting"';break;
								case $sys_custom['KIS_Admission']['MGF']['Settings'] && 'admitted':$tr_css = ' class="done"';break;
								case $sys_custom['KIS_Admission']['MGF']['Settings'] && 'notadmitted':$tr_css = ' class="draft"';break;

								case 'pending':$tr_css = ' class="absent"';break;
							//	case 'paymentsettled':$tr_css = '';break;
								case 'waitingforinterview':$tr_css = ' class="waiting"';break;
								case 'confirmed':$tr_css = ' class="done"';break;
								case 'cancelled':$tr_css = ' class="draft"';break;
							}
				?>
							<tr<?=$tr_css?>>
							  <td><?=$idx?></td>
                              <?php if ( $sys_custom['KIS_Admission']['CREATIVE']['Settings'] ): ?>
                                <td><?=($_interviewDate)?$_interviewDate:'--'?></td>
                              <?php endif; ?>
							  <td><a href="#/apps/admission/applicantslist/details/<?=$schoolYearID?>/<?=$_recordId?>/"><?=$_applicationId?></a><br></td>
							  <td><?=$sys_custom['KIS_Admission']['ICMS']['Settings'] || $sys_custom['KIS_Admission']['CSM']['Settings'] || $sys_custom['KIS_Admission']['MUNSANG']['Settings']?str_replace(",", " ", $_studentName):$_studentName?></td>
							  <td><?=(!is_null($_applicationDetailsAry['Marks'])?$_applicationDetailsAry['Marks']:'--')?></td>
							  <?=$sys_custom['KIS_Admission']['ICMS']['Settings']?$_applyDayType:''?>
							  <?for($i=0; $i<$interviewQuestionAnswer['maxNumOfInputBy']; $i++){
							  	//debug_pr($_applicationDetailsAry['QuestionAnswers']);
							  	$x = '<td>';

							  		if($_applicationDetailsAry['QuestionAnswers']){
							  			$pos = 0;
								  		foreach($_applicationDetailsAry['QuestionAnswers'] as $key => $value){
								  			if($i==$pos){
									  			for($j=0; $j<sizeof($_applicationDetailsAry['QuestionAnswers'][$key]['Questions']); $j++){
									  				$x .= '('.($j+1).') '.$_applicationDetailsAry['QuestionAnswers'][$key]['Questions'][$j].' ('.$_applicationDetailsAry['QuestionAnswers'][$key]['Answers'][$j].')<br/>';
									  			}
								  			}
								  			$pos++;
								  		}
							  		}
							  		else{
							  			$x .= '--';
							  		}
								$x .= '</td>';
								echo $x;
							  }?>
							  <td><?=$kis_lang['Admission']['Status'][$_status]?></td>
							  <td><?=$libinterface->Get_Checkbox('application_'.$_recordId, 'applicationAry[]', $_recordId, '', $Class='', $Display='', $Onclick="unset_checkall(this, document.getElementById('application_form'));", $Disabled='')?></td>
							 </tr>
				<?php }
					}else{ //no record
				?>
							<tr>
							<?if($sys_custom['KIS_Admission']['ICMS']['Settings']){?>
							  <td colspan="8" style="text-align:center;"><?=$kis_lang['norecord']?></td>
							<?}else{?>
							  <td colspan="7" style="text-align:center;"><?=$kis_lang['norecord']?></td>
							<?}?>
							</tr>
				<?php } ?>
							</tbody>
						</table>
						<input type="hidden" name="schoolYearID" id="schoolYearID" value="<?=$schoolYearID?>">
						<input type="hidden" name="classLevelID" id="classLevelID" value="<?=$classLevelID?>">
						<input type="hidden" name="selectStatus" id="selectStatus" value="<?=$selectStatus?>">
						<input type="hidden" name="selectInterviewRound" id="selectInterviewRound" value="<?=$selectInterviewRound?>">
						<input type="hidden" name="selectFormQuestionID" id="selectFormQuestionID" value="<?=$selectFormQuestionID?>">
						<input type="hidden" name="selectFormAnswerID" id="selectFormAnswerID" value="<?=$selectFormAnswerID?>">
						<input type="hidden" name="selectInterviewDate" id="selectInterviewDate" value="<?=$selectInterviewDate?>">
					</form>
          <p class="spacer"></p>
<? kis_ui::loadPageBar($page, $amount, $total, array(10, 20, 50, 100, 500, 1000)) ?>
<p class="spacer"></p><br>
	</div>
</div>

<!--FancyBox-->


    <?php
    if($sys_custom['KIS_Admission']['CREATIVE']['Settings']){
        //////// Cust status START ////////
        //// InterviewStatus START ////
            $interviewStatusArr = array();
            foreach ($admission_cfg['InterviewStatus'] as $key => $value) {
                $interviewStatusArr[] = array(
                    $value, $kis_lang['Admission']['CREATIVE']['InterviewStatus'][$key]
                );
            }
            $interviewStatusSelect = getSelectByArray(
                $interviewStatusArr,
                ' name="applicantInterviewStatus"',
                $applicantInterviewStatus,
                0,
                0,
                "- {$kis_lang['Admission']['pleaseSelect']} -"
            );
        //// InterviewStatus END ////


        //// AdmitStatus START ////
            $admitStatusArr = array();
            foreach ($admission_cfg['AdmitStatus'] as $key => $value) {
                $admitStatusArr[] = array(
                    $value, $kis_lang['Admission']['CREATIVE']['AdmitStatus'][$key]
                );
            }
            $admitStatusSelect = getSelectByArray(
                $admitStatusArr,
                ' name="AdmitStatus"',
                $AdmitStatus,
                0,
                0,
                "- {$kis_lang['Admission']['pleaseSelect']} -"
            );
        //// AdmitStatus END ////

        //// RatingClass START ////
            $ratingClassSelect = getSelectByValueDiffName(
                array_keys($kis_lang['Admission']['TimeSlot']),
                array_values($kis_lang['Admission']['TimeSlot']),
                ' name="RatingClass"',
                $RatingClass,
                1,
                0,
                "- {$kis_lang['Admission']['pleaseSelect']} -"
            );
        //// RatingClass END ////
        //////// Cust status END ////////
    ?>
        <div id='edit_box' style="padding:5px;display:none;" class="pop_edit">
            <form id="application_status_form" method="post">
                <input type="hidden" name="applicationIds" value=""/>
                <div class="pop_title">
                    <span><?= $kis_lang['changestatus'] ?></span>
                </div>
                <div class="table_board" style="height:330px">

                    <table class="form_table">

                        <tr>
                            <td width="1" class="field_title">
                                <input type="hidden" name="status" value=""/>
                                <input type="checkbox" class="toggleDisable" id="statusForm_applicationstatus"/>
                            </td>
                            <td width="30%" class="field_title">
                                <label for="statusForm_applicationstatus">
                                    <?= $kis_lang['Admission']['applicationstatus'] ?>
                                </label>
                            </td>
                            <td width="70%"><?= $statusSelection ?></td>
                        </tr>
                        <tr>
                            <td width="1" class="field_title">
                                <input type="checkbox" class="toggleDisable" id="statusForm_InterviewStatus"/>
                            </td>
                            <td width="30%" class="field_title">
                                <label for="statusForm_InterviewStatus">
                                    <?= $kis_lang['Admission']['CREATIVE']['InterviewFollowUp']['InterviewStatus'] ?>
                                </label>
                            </td>
                            <td width="70%">
                                <?= getSelectByArray(
                                    $interviewStatusArr,
                                    ' name="applicantInterviewStatus"',
                                    $applicantInterviewStatus,
                                    0,
                                    1
                                ); ?>
                            </td>
                        </tr>
                        <tr>
                            <td width="1" class="field_title">
                                <input type="checkbox" class="toggleDisable" id="statusForm_RegisterEmail"/>
                            </td>
                            <td width="30%" class="field_title">
                                <label for="statusForm_RegisterEmail">
                                    <?= $kis_lang['Admission']['CREATIVE']['InterviewFollowUp']['RegisterEmail'] ?>
                                </label>
                            </td>
                            <td width="70%">
                                <select name="RegisterEmail">
                                    <option value="1">
                                        <?= $kis_lang['Admission']['yes'] ?>
                                        (<?= $kis_lang['Admission']['date'] ?>: <?= date('Y-m-d') ?>)
                                    </option>
                                    <option value="0">
                                        <?= $kis_lang['Admission']['no'] ?>
                                    </option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td width="1" class="field_title">
                                <input type="checkbox" class="toggleDisable" id="statusForm_AdmitStatus"/>
                            </td>
                            <td width="30%" class="field_title">
                                <label for="statusForm_AdmitStatus">
                                    <?= $kis_lang['Admission']['CREATIVE']['InterviewFollowUp']['AdmitStatus'] ?>
                                </label>
                            </td>
                            <td width="70%">
                                <?= getSelectByArray(
                                    $admitStatusArr,
                                    ' name="AdmitStatus"',
                                    $AdmitStatus,
                                    0,
                                    1
                                ); ?>
                            </td>
                        </tr>
                        <tr>
                            <td width="1" class="field_title">
                                <input type="checkbox" class="toggleDisable" id="statusForm_RatingClass"/>
                            </td>
                            <td width="30%" class="field_title">
                                <label for="statusForm_RatingClass">
                                    <?= $kis_lang['Admission']['CREATIVE']['InterviewFollowUp']['RatingClass'] ?>
                                </label>
                            </td>
                            <td width="70%">
                                <?= getSelectByValueDiffName(
                                    array_keys($kis_lang['Admission']['TimeSlot']),
                                    array_values($kis_lang['Admission']['TimeSlot']),
                                    ' name="RatingClass"',
                                    $RatingClass,
                                    1,
                                    1
                                ); ?>
                            </td>
                        </tr>
                        <tr>
                            <td width="1" class="field_title">
                                <input type="checkbox" class="toggleDisable" id="statusForm_PaidReservedFee"/>
                            </td>
                            <td width="30%" class="field_title">
                                <label for="statusForm_PaidReservedFee">
                                    <?= $kis_lang['Admission']['CREATIVE']['InterviewFollowUp']['PaidReservedFee'] ?>
                                </label>
                            </td>
                            <td width="70%">
                                <select name="PaidReservedFee">
                                    <option value="0">
                                        <?= $kis_lang['Admission']['CREATIVE']['Paid'][0] ?>
                                    </option>
                                    <option value="1">
                                        <?= $kis_lang['Admission']['CREATIVE']['Paid'][1] ?>
                                    </option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td width="1" class="field_title">
                                <input type="checkbox" class="toggleDisable" id="statusForm_PaidRegistrationCertificate"/>
                            </td>
                            <td width="30%" class="field_title">
                                <label for="statusForm_PaidRegistrationCertificate">
                                    <?= $kis_lang['Admission']['CREATIVE']['InterviewFollowUp']['PaidRegistrationCertificate'] ?>
                                </label>
                            </td>
                            <td width="70%">
                                <select name="PaidRegistrationCertificate">
                                    <option value="0">
                                        <?= $kis_lang['Admission']['CREATIVE']['Paid'][0] ?>
                                    </option>
                                    <option value="1">
                                        <?= $kis_lang['Admission']['CREATIVE']['Paid'][1] ?>
                                    </option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td width="1" class="field_title">
                                <input type="checkbox" class="toggleDisable" id="statusForm_BookFeeEmail"/>
                            </td>
                            <td width="30%" class="field_title">
                                <label for="statusForm_BookFeeEmail">
                                    <?= $kis_lang['Admission']['CREATIVE']['InterviewFollowUp']['BookFeeEmail'] ?>
                                </label>
                            </td>
                            <td width="70%">
                                <select name="BookFeeEmail">
                                    <option value="1">
                                        <?= $kis_lang['Admission']['yes'] ?>
                                        (<?= $kis_lang['Admission']['date'] ?>: <?= date('Y-m-d') ?>)
                                    </option>
                                    <option value="0">
                                        <?= $kis_lang['Admission']['no'] ?>
                                    </option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td width="1" class="field_title">
                                <input type="checkbox" class="toggleDisable" id="statusForm_PaidBookFee"/>
                            </td>
                            <td width="30%" class="field_title">
                                <label for="statusForm_PaidBookFee">
                                    <?= $kis_lang['Admission']['CREATIVE']['InterviewFollowUp']['PaidBookFee'] ?>
                                </label>
                            </td>
                            <td width="70%">
                                <select name="PaidBookFee">
                                    <option value="0">
                                        <?= $kis_lang['Admission']['CREATIVE']['Paid'][0] ?>
                                    </option>
                                    <option value="1">
                                        <?= $kis_lang['Admission']['CREATIVE']['Paid'][1] ?>
                                    </option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td width="1" class="field_title">
                                <input type="checkbox" class="toggleDisable" id="statusForm_OpeningNoticeEmail"/>
                            </td>
                            <td width="30%" class="field_title">
                                <label for="statusForm_OpeningNoticeEmail">
                                    <?= $kis_lang['Admission']['CREATIVE']['InterviewFollowUp']['OpeningNoticeEmail'] ?>
                                </label>
                            </td>
                            <td width="70%">
                                <select name="OpeningNoticeEmail">
                                    <option value="1">
                                        <?= $kis_lang['Admission']['yes'] ?>
                                        (<?= $kis_lang['Admission']['date'] ?>: <?= date('Y-m-d') ?>)
                                    </option>
                                    <option value="0">
                                        <?= $kis_lang['Admission']['no'] ?>
                                    </option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td width="1" class="field_title">
                                <input type="checkbox" class="toggleDisable" id="statusForm_PaidTuitionFee"/>
                            </td>
                            <td width="30%" class="field_title">
                                <label for="statusForm_PaidTuitionFee">
                                    <?= $kis_lang['Admission']['CREATIVE']['InterviewFollowUp']['PaidTuitionFee'] ?>
                                </label>
                            </td>
                            <td width="70%">
                                <select name="PaidTuitionFee">
                                    <option value="0">
                                        <?= $kis_lang['Admission']['CREATIVE']['Paid'][0] ?>
                                    </option>
                                    <option value="1">
                                        <?= $kis_lang['Admission']['CREATIVE']['Paid'][1] ?>
                                    </option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td width="1" class="field_title">
                                <input type="checkbox" class="toggleDisable" id="statusForm_GiveUpOffer"/>
                            </td>
                            <td width="30%" class="field_title">
                                <label for="statusForm_GiveUpOffer">
                                    <?= $kis_lang['Admission']['CREATIVE']['InterviewFollowUp']['GiveUpOffer'] ?>
                                </label>
                            </td>
                            <td width="70%">
                                <select name="GiveUpOffer">
                                    <option value="1">
                                        <?= $kis_lang['Admission']['yes'] ?>
                                    </option>
                                    <option value="0">
                                        <?= $kis_lang['Admission']['no'] ?>
                                    </option>
                                </select>
                            </td>
                        </tr>

                    </table>
                    <p class="spacer"></p>
                </div>
                <div class="edit_bottom">
                    <input id="submitBtn" name="submitBtn" type="submit" class="formbutton"
                           value="<?= $kis_lang['submit'] ?>"/>
                    <input name="cancelBtn" type="button" class="formsubbutton" onclick="parent.$.fancybox.close();"
                           value="<?= $kis_lang['cancel'] ?>"/>
                </div>

            </form>
        </div>
        <script>
            $('#application_status_form .toggleDisable').click(function () {
                $(this).closest('tr').find('select').prop('disabled', !$(this).prop('checked'));
            });
            $('#application_status_form select').prop('disabled', true);
            $('#application_status_form').off('submit').submit(function () {
                var applicationIds = $.map($("input[name='applicationAry\[\]']:checked"), function (n) {
                    return n.value;
                }).join(',');

                $(this).find('[name="applicationIds"]').val(applicationIds);
                kis.showLoading();
                $.post('apps/admission/ajax.php?action=updateApplicationStatusByIds', $(this).serialize(), function (data) {
                    parent.$.fancybox.close();
                    kis.hideLoading().loadBoard();
                });
                return false;
            });
        </script>
    <? }else if($sys_custom['KIS_Admission']['STANDARD']['Settings']){?>
    	<div id='edit_box' style="padding:5px;display:none;" class="pop_edit">
            <form id="application_status_form" method="post">
                <input type="hidden" name="applicationIds" value=""/>
                <div class="pop_title">
                    <span><?= $kis_lang['changestatus'] ?></span>
                </div>
                <div class="table_board" style="height:330px">

                    <table class="form_table">

                        <tr>
                            <td width="1" class="field_title">
                                <input type="hidden" name="status" value=""/>
                                <input type="checkbox" class="toggleDisable" id="statusForm_applicationstatus"/>
                            </td>
                            <td width="30%" class="field_title">
                                <label for="statusForm_applicationstatus">
                                    <?= $kis_lang['Admission']['applicationstatus'] ?>
                                </label>
                            </td>
                            <td width="70%"><?= $statusSelection ?></td>
                        </tr>
                        <tr>
                            <td width="1" class="field_title">
                                <input type="checkbox" class="toggleDisable" id="statusForm_RatingClass"/>
                            </td>
                            <td width="30%" class="field_title">
                                <label for="statusForm_RatingClass">
                                    <?= $kis_lang['Admission']['CREATIVE']['InterviewFollowUp']['RatingClass'] ?>
                                </label>
                            </td>
                            <td width="70%">
                                <?= getSelectByValueDiffName(
                                    array_keys($kis_lang['Admission']['TimeSlot']),
                                    array_values($kis_lang['Admission']['TimeSlot']),
                                    ' name="RatingClass"',
                                    $RatingClass,
                                    1,
                                    1
                                ); ?>
                            </td>
                        </tr>
                    </table>
                    <p class="spacer"></p>
                </div>
                <div class="edit_bottom">
                    <input id="submitBtn" name="submitBtn" type="submit" class="formbutton"
                           value="<?= $kis_lang['submit'] ?>"/>
                    <input name="cancelBtn" type="button" class="formsubbutton" onclick="parent.$.fancybox.close();"
                           value="<?= $kis_lang['cancel'] ?>"/>
                </div>

            </form>
        </div>
        <script>
            $('#application_status_form .toggleDisable').click(function () {
                $(this).closest('tr').find('select').prop('disabled', !$(this).prop('checked'));
            });
            $('#application_status_form select').prop('disabled', true);
            $('#application_status_form').off('submit').submit(function () {
                var applicationIds = $.map($("input[name='applicationAry\[\]']:checked"), function (n) {
                    return n.value;
                }).join(',');

                $(this).find('[name="applicationIds"]').val(applicationIds);
                kis.showLoading();
                $.post('apps/admission/ajax.php?action=updateApplicationStatusByIds', $(this).serialize(), function (data) {
                    parent.$.fancybox.close();
                    kis.hideLoading().loadBoard();
                });
                return false;
            });
        </script>
    <? } else { ?>
        <div id='edit_box' style="padding:5px;display:none;" class="pop_edit">
            <form id="application_status_form" method="post">
                <div class="pop_title">
                    <span><?= $kis_lang['changestatus'] ?></span>
                </div>
                <div class="table_board" style="height:330px">

                    <table class="form_table">
                        <tr>
                            <td width="30%"
                                class="field_title"><?= $mustfillinsymbol ?><?= $kis_lang['Admission']['applicationstatus'] ?></td>
                            <td width="70%"><?= $statusSelection ?></td>
                        </tr>
                    </table>
                    <p class="spacer"></p>
                </div>
                <div class="edit_bottom">
                    <input id="submitBtn" name="submitBtn" type="submit" class="formbutton"
                           value="<?= $kis_lang['submit'] ?>"/>
                    <input name="cancelBtn" type="button" class="formsubbutton" onclick="parent.$.fancybox.close();"
                           value="<?= $kis_lang['cancel'] ?>"/>
                </div>

            </form>
        </div>
    <?php
    }
    ?>

	<?if($sys_custom['KIS_Admission']['CSM']['Settings']){?>
	<div id='edit_class' style="padding:5px;display:none;" class="pop_edit">
	<form id="application_class_form" method="post">
		<div class="pop_title">
			<span><?=$kis_lang['changeclass']?></span>
		</div>
		<div class="table_board" style="height:330px">

			<table class="form_table">
				<tr>
                   <td width="30%" class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['form']?></td>
                   <td width="70%"><?=$classChangeLevelSelection?></td>
                 </tr>
			</table>
		  <p class="spacer"></p>
	   </div>
	    <div class="edit_bottom">
	       <input id="submitBtn" name="submitBtn" type="submit" class="formbutton" value="<?=$kis_lang['submit']?>" />
	       <input name="cancelBtn" type="button" class="formsubbutton" onclick="parent.$.fancybox.close();" value="<?=$kis_lang['cancel']?>" />
	  	</div>

	</form>
	</div>
	<?}?>
