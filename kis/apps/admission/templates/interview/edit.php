<?php
// Editing by HENRY
include_once($intranet_root."/lang/lang.".$intranet_session_language.".php");

$linterface = $kis_data['libinterface'];

//if(!isset($kis_data['recordID'])){
//	exit;
//}

if($kis_data['recordID']!=''){
	//$schoolYearID  = $kis_data['interviewSettingAry'][0]['SchoolYearID'];
	$classLevelID = $kis_data['interviewSettingAry'][0]['ClassLevelID'];
	$date = $kis_data['interviewSettingAry'][0]['Date'];
	$startTime = $kis_data['interviewSettingAry'][0]['StartTime'];
	$endTime = $kis_data['interviewSettingAry'][0]['EndTime'];
	$groupName = $kis_data['interviewSettingAry'][0]['GroupName'];
	$quota = $kis_data['interviewSettingAry'][0]['Quota'];
	$applied = $kis_data['interviewSettingAry'][0]['Applied'];
	$round = $kis_data['interviewSettingAry'][0]['Round'];
}

?>
<script type="text/javascript">
$(function(){
	kis.admission.interview_edit_init({
		invaliddateformat:'<?=$kis_lang['Admission']['msg']['invaliddateformat']?>',
		enddateearlier: '<?=$kis_lang['Admission']['msg']['enddateearlier']?>',
		emptyapplicationdatewarning: '<?=$kis_lang['Admission']['msg']['emptyapplicationdatewarning']?>',
		selectatleastonetimeslot: '<?=$kis_lang['Admission']['msg']['selectatleastonetimeslot']?>',
		enterpositivenumber: '<?=$kis_lang['Admission']['msg']['enternumber']?>',
		quotashouldnotlessthanapplied: '<?=$kis_lang['Admission']['msg']['quotashouldnotlessthanapplied']?>',
		applied: '<?=$applied?>'
	});
});
</script>
<div class="main_content">
	<form id='interview_edit_form'>
	    <div class="main_content_detail">
	    	<?php
            if (method_exists($lauc, 'getAdminModuleTab')) {
                kis_ui::loadModuleTab($lauc->getAdminModuleTab('interview'), '', '#/apps/admission/interview/');
            } elseif ($sys_custom['KIS_Admission']['InterviewForm'] || $moduleSettings['enableinterviewform']) {
                kis_ui::loadModuleTab(array('interviewarrangement', 'interviewformbuilter', 'interviewforminput', 'interviewformresult', 'timeslotsettings', 'interviewusersettings', 'announcementsettings'), '', '#/apps/admission/interview/');
            } else if ($sys_custom['KIS_Admission']['InterviewArrangement'] || $moduleSettings['enableinterviewarrangement']) {
                kis_ui::loadModuleTab(array('interviewarrangement', 'timeslotsettings', 'announcementsettings'), '', '#/apps/admission/interview/');
            }
		    ?>
			<?=$kis_data['NavigationBar']?>
			<p class="spacer"></p>
			<div class="table_board">
				<table class="form_table">
					 <tr>
						<td class="field_title" width="25%"><?=$mustfillinsymbol?><?=$kis_lang['form']?></td>
						<td width="75%"><?=$classLevelSelection?></td>
					</tr>
				    <tr>
						<td class="field_title" width="25%"><?=$mustfillinsymbol?><?=$kis_lang['interviewdate']?></td>
						<td width="75%"><input type="text" name="interview_date" id="interview_date" value="<?=$date?>">&nbsp;<span class="error_msg" id="warning_interview_date"></span></td>
					</tr>				
				    <tr>
						<td class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['from']?></td>
						<td><?=$libadmission->Get_Time_Selection($libinterface,'start_time',$startTime,'',false)?>&nbsp;<span class="error_msg" id="warning_start_time"></span></td>
					</tr>		
				    <tr>
						<td class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['to']?></td>
						<td><?=$libadmission->Get_Time_Selection($libinterface,'end_time',($endTime?$endTime:'23:59:59'),'',false)?>&nbsp;<span class="error_msg" id="warning_end_time"></span></td>
					</tr>
					<tr>
						<td class="field_title">
						<?if($admission_cfg['interview_arrangment']['interview_group_type'] == 'Room'){?>
                    		<?=$kis_lang['interviewroom']?>
                    	<?}else{?>
                    		<?=$kis_lang['sessiongroup']?>
                    	<?}?>
						</td>
						<td>
						<?if($admission_cfg['interview_arrangment']['interview_group_name']){?>
							<?=getSelectByValue($admission_cfg['interview_arrangment']['interview_group_name'],' id="group_name" name="group_name" ',$groupName,1,0,'- '.$kis_lang['select'].' -')?>
						<?}else{?>	
							<?=$libadmission->Get_Alphabet_Selection('group_name', 'A', 'Z', $groupName)?>
						<?}?>
						</td>
					</tr>
					<tr>
						<td class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['qouta']?></td>
						<td><input type="text" name="quota" id="quota" value="<?=$quota?>" /></td>
					</tr>     
			    </table>
				<p class="spacer"></p>
				<p class="spacer"></p><br />
	        </div>
	        <div class="text_remark"><?=$kis_lang['requiredfield']?></div>
			<div class="edit_bottom">
				<input type="hidden" name="recordID" id="recordID" value="<?=$kis_data['recordID']?>">
				<input type="hidden" name="schoolYearID" id="schoolYearID" value="<?=$kis_data['schoolYearID']?>">
				<input type="hidden" name="round" id="round" value="<?=($round?$round:$selectInterviewRound)?>">
				<input type="submit" id="submitBtn" name="submitBtn" class="formbutton" value="<?=$kis_lang['submit']?>" />
				<input type="button" id="cancelBtn" name="cancelBtn" class="formsubbutton" value="<?=$kis_lang['cancel']?>" />
			</div>
		</div>
	</form>
</div>