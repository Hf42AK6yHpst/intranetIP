<?php
// Editing by Henry
?>
<script>
kis.admission.interview_form_input_details_view_init({
	returnmsg:'<?=$kis_lang['Admission']['ReturnMessage'][$sysMsg]?>',
	msg_confirm_delete_email_list:'<?=$kis_lang['Admission']['msg']['confirm_delete_email_list']?>',
	msg_selectatleastonerecord:'<?=$kis_lang['Admission']['msg']['selectatleastonerecord']?>'
});
</script>
<div class="main_content_detail">
	<?php
    if (method_exists($lauc, 'getAdminModuleTab')) {
        kis_ui::loadModuleTab($lauc->getAdminModuleTab('interview'), 'interviewforminput', '#/apps/admission/interview/');
    } elseif (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-eAdmission"] && $_SESSION["SSV_USER_ACCESS"]["eAdmin-eAdmission-interview-marker"]) {
        kis_ui::loadModuleTab(array('interviewforminput'), 'interviewforminput', '#/apps/admission/interview/');
    } else if ($sys_custom['KIS_Admission']['InterviewForm'] || $moduleSettings['enableinterviewform']) {
        kis_ui::loadModuleTab(array('interviewarrangement', 'interviewformbuilter', 'interviewforminput', 'interviewformresult', 'timeslotsettings', 'interviewusersettings', 'announcementsettings'), 'interviewforminput', '#/apps/admission/interview/');
    }
    ?>
	<?=$kis_data['NavigationBar']?>
 	 <p class="spacer"></p>
 	<? if(!empty($warning_msg)):?>
 		<?=$warning_msg?>
	<? endif; ?>
	<div class="Content_tool">
	    <!--<div class="btn_option"  id="ExportDiv">
			<a id="btn_import" class="tool_import_interview export" href="javascript:void(0);"><?=$kis_lang['export']?></a>
		</div>-->  
	</div>
	<div class="table_board">
		<!--<form id='interview_form' name='interview_form'>-->
		<table class="form_table">
			 <!--<tr>
				<td class="field_title" width="25%"><?=$kis_lang['form']?></td>
				<td width="75%">
					<?=$classLevelSelection?>
					<span class=\"tabletextremark\">
						<?=$kis_lang['CtrlMultiSelectMessage']?>
					</span>
				</td>
			</tr>-->
		    <tr>
				<td class="field_title"><?=$kis_lang['applicationno']?></td>
				<td><?=$studentInfo['applicationID']?></td>
			</tr>
			<tr>
				<td class="field_title"><?=$kis_lang['studentname']?></td>
				<td><?=Get_Lang_Selection($studentInfo['student_name_b5'],$studentInfo['student_name_en'])?></td>
			</tr>		
		    <tr>
				<td class="field_title"><?=$kis_lang['form']?></td>
				<td><?=$classLevelAry[$studentInfo['classLevelID']]?></td>
			</tr>
			<tr>
				<td class="field_title"><?=$kis_lang['marking']?></td>
				<td>
					<ol>
					  <!--<li class="ui-state-default">Drop here...</li>-->
					  <?
					  for($i=0; $i < sizeof($interviewQuestionAry); $i++){
					  	if($interviewQuestionAry[$i]['Type'] == 'text'){
					  		echo '<li>
								  	<div class="control-group">
									  <label class="lbl_text_required"></label>
									  <label class="control-label lbl_text_title" for="textinput">'.($interviewQuestionAry[$i]['Required']?$mustfillinsymbol:'').$interviewQuestionAry[$i]['Title'].'</label>
									  <div class="controls">
									    <input type="text" size="60px" readonly name="text_answer['.$interviewQuestionAry[$i]['QuestionID'].']" placeholder="'.$interviewQuestionAry[$i]['Name'].'" value="'.htmlspecialchars($interviewFormAnswerAry[$interviewQuestionAry[$i]['QuestionID']]).'" class="lbl_text_input input-xlarge">
									  	<br/><label class="lbl_text_remarks">'.$interviewQuestionAry[$i]['Remarks'].'</label>
									  </div>
									</div>
										<input type="hidden" name="question_id[]" value="'.$interviewQuestionAry[$i]['QuestionID'].'" />
								  </li>';
					  	}
					  	else if($interviewQuestionAry[$i]['Type'] == 'radio'){
					  		echo '<li>
								  	<div class="component"><!-- Multiple Radios (inline) -->
										<div class="control-group">
											<label class="lbl_text_required"></label>
										  <label class="control-label lbl_text_title" for="radios">'.($interviewQuestionAry[$i]['Required']?$mustfillinsymbol:'').$interviewQuestionAry[$i]['Title'].'</label>
										  <div class="lbl_text_input controls">';
										  	$option_array = explode(',',$interviewQuestionAry[$i]['Name']);
										  	
										  	for($j=0; $j<sizeof($option_array); $j++){
										  		$checked = '';
										  		if($interviewFormAnswerAry[$interviewQuestionAry[$i]['QuestionID']] == $option_array[$j]){
										  			$checked = 'checked';
										  		}
											    echo '<label class="radio inline" for="radios-'.$j.'-'.$interviewQuestionAry[$i]['QuestionID'].'">
											      <input type="radio" readonly name="text_answer['.$interviewQuestionAry[$i]['QuestionID'].']" id="radios-'.$j.'-'.$interviewQuestionAry[$i]['QuestionID'].'" value="'.$option_array[$j].'" '.$checked.' >'.$option_array[$j].'</label>';
										  	}
										  	
									echo '</div><label class="lbl_text_remarks">'.$interviewQuestionAry[$i]['Remarks'].'</label>
										</div>
										<input type="hidden" name="question_id[]" value="'.$interviewQuestionAry[$i]['QuestionID'].'" />
								  </li>';
					  	}
					  	else if($interviewQuestionAry[$i]['Type'] == 'select'){
					  		echo '<li>
								  	<div class="component"><!-- Multiple Radios (inline) -->
										<div class="control-group">
											<label class="lbl_text_required"></label>
										  <label class="control-label lbl_text_title" for="radios">'.($interviewQuestionAry[$i]['Required']?$mustfillinsymbol:'').$interviewQuestionAry[$i]['Title'].'</label>
										  <div class="lbl_text_input controls">
											<select id="selectbasic" readonly name="text_answer['.$interviewQuestionAry[$i]['QuestionID'].']" class="input-xlarge">';
	
										  	$option_array = explode(',',$interviewQuestionAry[$i]['Name']);
										  	
										  	for($j=0; $j<sizeof($option_array); $j++){
										  		$selected = '';
										  		if($interviewFormAnswerAry[$interviewQuestionAry[$i]['QuestionID']] == $option_array[$j]){
										  			$selected = 'selected';
										  		}
										  	
											    echo '<option '.$selected.' >'.$option_array[$j].'</option>';
										  	}
										  	
									echo '</select></div><label class="lbl_text_remarks">'.$interviewQuestionAry[$i]['Remarks'].'</label>
										</div>
									</div>
									<input type="hidden" name="question_id[]" value="'.$interviewQuestionAry[$i]['QuestionID'].'" />
								  </li>';
					  	}
					  }
					  ?>
					</ol>
				</td>
			</tr>
			<tr>
				<td class="field_title"><?=$kis_lang['marks']?></td>
				<td>
					<?=$interviewFormResultAry['marks']?>
				</td>
			</tr>
			<tr>
				<td class="field_title"><?=$kis_lang['marker']?></td>
				<td>
					<?=$interviewFormResultAry['name']?> (<?=$interviewFormResultAry['datemodified']?>)
				</td>
			</tr>
		</table>
        <!--<div class="text_remark"><?=$kis_lang['requiredfield']?></div>
		<div class="edit_bottom">
			<input type="hidden" name="recordID" id="recordID" value="<?=$kis_data['recordID']?>" />
			<input type="hidden" name="schoolYearID" id="schoolYearID" value="<?=$kis_data['schoolYearID']?>" />
			<input type="hidden" name="formID" id="formID" value="<?=$interviewFormSettingAry['RecordID']?>" />
			<input type="hidden" name="applicationID" id="applicationID" value="<?=$kis_data['applicationID']?>" />
			<input type="hidden" name="answerID" id="answerID" value="<?=$interviewFormAnswerAry['ResultID']?>" />
			<input type="hidden" name="selectInterviewDate" id="selectInterviewDate" value="<?=($kis_data['selectInterviewDate']?$kis_data['selectInterviewDate']:0)?>" />
			<input type="hidden" name="selectInterviewRound" id="selectInterviewRound" value="<?=$kis_data['selectInterviewRound']?>" />
			<input type="button" id="submitBtn" name="submitBtn" class="formbutton" value="<?=$kis_lang['submit']?>" />
			<input type="button" id="cancelBtn" name="cancelBtn" class="formsubbutton" value="<?=$kis_lang['cancel']?>" />
		</div>
		</form>-->
    </div>
</div>