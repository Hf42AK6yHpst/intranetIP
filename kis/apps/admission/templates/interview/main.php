<?php
// Editing by 
global $setting_path_ip_rel;
?>
    <script>
        kis.admission.interview_init({
            returnmsg: '<?=$kis_lang['Admission']['ReturnMessage'][$sysMsg]?>',
            msg_confirm_delete_email_list: '<?=$kis_lang['msg']['deleteinterviewtimeslot']?>',
            msg_selectatleastonerecord: '<?=$kis_lang['msg']['selectonlyonerecord']?>',
            selectatleastonerecord: '<?=$kis_lang['msg']['selectonlyonerecord']?>',
            selectatleastonerecord2: '<?=$kis_lang['Admission']['msg']['selectatleastonerecord']?>',
            exportallrecordsornot: '<?=$kis_lang['Admission']['msg']['exportallrecordsornot']?>',
            printallrecordsornot: '<?=$kis_lang['Admission']['msg']['printallrecordsornot']?>',
            sendemailallrecordsornot: '<?=$kis_lang['Admission']['msg']['sendemailallrecordsornot']?>',
            importinterviewallrecordsornot: '<?=$kis_lang['Admission']['msg']['importinterviewallrecordsornot']?>',
            returnmsg: '<?=$kis_lang['Admission']['ReturnMessage'][$sysMsg]?>',
            close: '<?=$kis_lang['close']?>',
            importinterviewinfobyarrangementornot: '<?=$kis_lang['msg']['importinterviewinfobyarrangement']?>'
        });
    </script>
    <div class="main_content_detail">
        <?php
        if(method_exists($lauc, 'getAdminModuleTab')){
            kis_ui::loadModuleTab($lauc->getAdminModuleTab('interview'), '', '#/apps/admission/interview/');
        }elseif ($sys_custom['KIS_Admission']['InterviewForm'] || $moduleSettings['enableinterviewform']) {
            kis_ui::loadModuleTab(array('interviewarrangement', 'interviewformbuilter', 'interviewforminput', 'interviewformresult', 'timeslotsettings', 'interviewusersettings', 'announcementsettings'), '', '#/apps/admission/interview/');
        } elseif ($sys_custom['KIS_Admission']['InterviewArrangement'] || $moduleSettings['enableinterviewarrangement']) {
            kis_ui::loadModuleTab(array('interviewarrangement', 'timeslotsettings', 'announcementsettings'), '', '#/apps/admission/interview/');
        }
        ?>
        <p class="spacer"></p>
        <? if (!empty($warning_msg)): ?>
            <?= $warning_msg ?>
        <? endif; ?>
        <div class="Content_tool">
            <a href="#" class="tool_new new"><?= $kis_lang['new'] ?></a>
            <div class="btn_option" id="ExportDiv">
                <a id="btn_import" class="tool_import import"
                   href="javascript:void(0);"><?= $kis_lang['importinterviewtimeslotinfo'] ?></a>

                <? if ($sys_custom['KIS_Admission']['InterviewArrangement'] || $moduleSettings['enableinterviewarrangement']) { ?>
                    <a id="btn_import_by_arragement" class="tool_import_by_arragement import"
                       href="javascript:void(0);"><?= $kis_lang['importinterviewinfobyarrangement'] ?></a>
                <? } ?>

                <? if (!$sys_custom['KIS_Admission']['InterviewArrangement'] || $moduleSettings['enableinterviewarrangement']) { ?>
                    <a id="btn_import_applicant" class="tool_import_applicant import"
                       href="javascript:void(0);"><?= $kis_lang['importinterviewapplicant'] ?></a>
                <? } ?>

                <a id="btn_import" class="tool_export export"
                   href="javascript:void(0);"><?= $kis_lang['exportAppliedDetails'] ?></a>
                   
                <?if($sys_custom['KIS_Admission']['MINGWAI']['Settings'] || $sys_custom['KIS_Admission']['MINGWAIPE']['Settings']){?>
				<a id="btn_import" class="tool_export_interview export"
                   href="javascript:void(0);"><?=$kis_lang['exportInterviewInfo']?></a>
				<?}?>                
            </div>
        </div>
        <div id="table_filter">
            <form class="filter_form">
                <?= $schoolYearSelection ?>
                <?= $libadmission->Get_Number_Selection('selectInterviewRound', '1', '3', $selectInterviewRound, '', 1, 0, '', 0, $kis_lang['round']) ?>
            </form>
        </div>
        <!--<form class="filter_form">
    	<div id="table_filter">
        	<?= $schoolYearSelection ?>
        	<?= $classLevelSelection ?>
   		</div>
		<div class="search">
		    <input placeholder="<?= $kis_lang['search'] ?>" name="keyword" value="<?= $keyword ?>" type="text"/>
		</div>
    </form>-->

        <!---->

        <!--<p class="spacer"></p>-->
        <div class="table_board">
            <?php if (!$sys_custom['KIS_Admission']['CREATIVE']['Settings']): ?>
            <? if ($sys_custom['KIS_Admission']['InterviewArrangement'] || $moduleSettings['enableinterviewarrangement']) { ?>
                <br/><?= $mustfillinsymbol . $kis_lang['numofapplicantbyarrangement'] . ': ' . ($numberOfApplicant ? $numberOfApplicantAssignedTointerview . '/' . $numberOfApplicant : '--') ?>
            <? } ?>
            <?php endif;?>
            <div class="common_table_tool common_table_tool_table">
                <a href="#" class="tool_email tool_send_email"><?= $kis_lang['sendemail'] ?></a>
                <?php if ($sys_custom['KIS_Admission']['CREATIVE']['Settings']): ?>
                    <a
                            class="tool_print tool_export_label export"
                            href="javascript:void(0);"
                    >
                        <?= $kis_lang['Admission']['CREATIVE']['exportApplicantLabel'] ?>
                    </a>
                    <script>
                        $('a.tool_export_label').click(function () {
                            var open_fancy_box = function () {
                                $.fancybox({
                                    'fitToView': false,
                                    'autoDimensions': false,
                                    'autoScale': false,
                                    'autoSize': false,
                                    'width': 300,
                                    'height': 250,

                                    'transitionIn': 'elastic',
                                    'transitionOut': 'elastic',
                                    'href': '#print_label_box'
                                });
                            };

                            var hasChecked = false;
                            $("input[name='interviewAry[]']").each(function () {
                                if (this.checked) {
                                    hasChecked = true;
                                    return;
                                }
                            });
                            $('#selectedApplicant').empty();
                            if (hasChecked) {
                                $("input[name='interviewAry[]']:checked").each(function () {
                                    $('#selectedApplicant')
                                        .append('<input name="interviewAry[]" value="' + $(this).val() + '" />');
                                });
                            } else {
                                //alert(lang.selectatleastonerecord);
                                if (!confirm("<?=$kis_lang['Admission']['msg']['exportallrecordsornot']?>")) {
                                    return false;
                                }
                                $("input[name='interviewAry[]']").each(function () {
                                    $('#selectedApplicant')
                                        .append('<input name="interviewAry[]" value="' + $(this).val() + '" />');
                                });
                            }
                            open_fancy_box();

                            return false;
                        });
                    </script>
                <?php endif; ?>
                <a href="#" class="tool_edit"><?= $kis_lang['edit'] ?></a>
                <?if($sys_custom['KIS_Admission']['MINGWAI']['Settings'] || $sys_custom['KIS_Admission']['MINGWAIPE']['Settings']){?>
    					<a href="#" class="tool_print tool_print_interview_form"><?=$kis_lang['Admission']['MINGWAI']['printinterviewform']?></a>
    			<?}?>
                <a href="#" class="tool_delete"><?= $kis_lang['delete'] ?></a>
            </div>
            <p class="spacer"></p>
            <form id="interview_list" name="interview_list" method="post">
	            <?php
	            if(file_exists("{$PATH_WRT_ROOT}includes/admission/{$setting_path_ip_rel}/template/interview/interview.php")):
		            include("{$PATH_WRT_ROOT}includes/admission/{$setting_path_ip_rel}/template/interview/interview.php");
	            else:
	            ?>
                <table class="common_table_list edit_table_list">
                    <colgroup>
                        <col nowrap="nowrap">
                    </colgroup>
                    <thead>
                    <tr>
                        <th width="20">&nbsp;</th>
                        <th><?= $kis_lang['form'] ?></th>
                        <th><?= $kis_lang['interviewdate'] ?></th>
                        <th><?= $kis_lang['timeslot'] ?> </th>
                        <th>
                            <? if ($admission_cfg['interview_arrangment']['interview_group_type'] == 'Room') { ?>
                                <?= $kis_lang['interviewroom'] ?>
                            <? } else { ?>
                                <?= $kis_lang['sessiongroup'] ?>
                            <? } ?>
                        </th>
                        <th><?= $kis_lang['quotaRemains'] ?></th>
                        <th><?= $kis_lang['applied'] ?></th>
                        <th><input type="checkbox" name="checkmaster"
                                   onclick="(this.checked)?setChecked(1,this.form,'interviewAry[]'):setChecked(0,this.form,'interviewAry[]')">
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <?
                    $idx = 0;
                    foreach ($interviewSettingAry as $_classLevelId => $_classLevelRecord):
                        //foreach($_classLevelAry as $_classLevelRecord){
                        $idx++;
                        $SchoolYearArr = current(kis_utility::getAcademicYears(array("AcademicYearID" => $_classLevelRecord['ClassLevelID'])));
                        $formName = $SchoolYearArr['academic_year_name_' . $intranet_session_language];
                        ?>
                        <tr>
                            <td><?= $idx ?></td>
                            <td><?= $classLevelAry[$_classLevelRecord['ClassLevelID']] ?></td>
                            <td><?= $_classLevelRecord['Date'] ?></td>
                            <td><?= substr($_classLevelRecord['StartTime'], 0, -3) ?>
                                ~ <?= substr($_classLevelRecord['EndTime'], 0, -3) ?></td>
                            <td><?= ($_classLevelRecord['GroupName'] ? $_classLevelRecord['GroupName'] : '--') ?></td>
                            <td>
                                <?= $_classLevelRecord['Quota'] ?>
                                (<span style="<?= (($_classLevelRecord['Quota'] - $_classLevelRecord['Applied']) > 0) ? '' : 'color:red;'; ?>"><?= ($_classLevelRecord['Quota'] - $_classLevelRecord['Applied']) ?></span>)
                            </td>
                            <td><?= /*($_classLevelRecord['Applied'] > 0)?*/
                                '<a href="#/apps/admission/interview/details/' . ($selectInterviewRound ? $selectInterviewRound : 1) . '/' . $_classLevelRecord['RecordID'] . '/">' . $_classLevelRecord['Applied'] . '</a>'/*:$_classLevelRecord['Applied']*/ ?>
                                <input type="hidden" name="applied_<?= $_classLevelRecord['RecordID'] ?>"
                                       id="applied_<?= $_classLevelRecord['RecordID'] ?>"
                                       value="<?= $_classLevelRecord['Applied'] ?>">
                            </td>
                            <td>
                                <?= $libinterface->Get_Checkbox('interview_' . $_classLevelRecord['RecordID'], 'interviewAry[]', $_classLevelRecord['RecordID'], '', $Class = '', $Display = '', $Onclick = "unset_checkall(this, document.getElementById('interview_list'));", $Disabled = '') ?>

                                <?php if (count((array)$interviewListAry[$_classLevelRecord['RecordID']])): ?>
                                    <input type="hidden" id="applicant_ids_<?= $_classLevelRecord['RecordID'] ?>"
                                           name="applicant_ids_<?= $_classLevelRecord['RecordID'] ?>"
                                           value="<?= implode(',', (array)$interviewListAry[$_classLevelRecord['RecordID']]) ?>"/>
                                <?php endif; ?>
                            </td>
                        </tr>
                    <? //}
                    endforeach;
                    if ($idx == 0) {
                        ?>
                        <tr>
                            <td colspan="7" style="text-align:center;"><?= $kis_lang['norecord'] ?></td>
                        </tr>
                    <? } ?>
                    </tbody>
                </table>
                <?php
                endif;
                ?>
                <input type="hidden" name="schoolYearID" id="schoolYearID" value="<?= $schoolYearID ?>">
                <input type="hidden" name="round" id="round" value="<?= $selectInterviewRound ?>">
            </form>
            <p class="spacer"></p>
            <p class="spacer"></p>
            <br>
        </div>
    </div>

<?php if ($sys_custom['KIS_Admission']['CREATIVE']['Settings']): ?>
    <div id='print_label_box' style="padding:5px;display:none;" class="pop_edit">
        <form method="post" action="/kis/admission_form/export_interview_label.php" target="_blank">
            <div id="selectedApplicant" style="display:none;"></div>
            <input type="hidden" name="schoolYearID" id="schoolYearID" value="<?= $schoolYearID ?>">
            <input type="hidden" name="round" id="round" value="<?= $selectInterviewRound ?>">

            <div class="pop_title">
                <span><?= $kis_lang['Admission']['CREATIVE']['exportApplicantLabel'] ?></span>
            </div>
            <div class="table_board" style="height:100px">

                <table class="form_table">
                    <tr>
                        <td class="field_title" style="width:50%">
                            <label for="showTime"><?= $kis_lang['Admission']['CREATIVE']['showTime'] ?></label>
                        </td>
                        <td style="width:50%">
                            <input type="checkbox" id="showTime" name="showTime" value="1" checked/>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:50%" class="field_title">
                            <label for="showRoom"><?= $kis_lang['Admission']['CREATIVE']['showRoom'] ?></label>
                        </td>
                        <td style="width:50%">
                            <input type="checkbox" id="showRoom" name="showRoom" value="1" checked/>
                        </td>
                    </tr>
                </table>
                <p class="spacer"></p>
            </div>
            <div class="edit_bottom">
                <input id="submitBtn" name="submitBtn" type="submit" class="formbutton"
                       value="<?= $kis_lang['submit'] ?>"/>
                <input name="cancelBtn" type="button" class="formsubbutton" onclick="parent.$.fancybox.close();"
                       value="<?= $kis_lang['cancel'] ?>"/>
            </div>

        </form>
    </div>
<?php endif; ?>
