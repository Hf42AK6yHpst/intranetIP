<script type="text/javascript">
kis.admission.application_timeslotsettings_init();
</script>
<div class="main_content_detail">
    <? kis_ui::loadModuleTab(array('applicationperiod','timeslotsettings','basicsettings'), 'timeslotsettings', '#/apps/admission/settings/')?>
    <p class="spacer"></p>
 	<? if(!empty($warning_msg)):?>
 		<?=$warning_msg?>
	<? endif; ?>
    <div id="table_filter">
    <form class="filter_form"> 
        <?=$schoolYearSelection?>
    </form>
    </div>
<!---->
    <p class="spacer"></p>
    <form id="timeslot_settings">
    <div id="div_timeslot_table" class="table_board">
  	    <table class="common_table_list edit_table_list">
			<col nowrap="nowrap"/>
			<thead>
				<tr>
				  <th class="row_title_class" width="20%"><?=$kis_lang['level']?></th>
			<? foreach($kis_lang['Admission']['TimeSlot'] as $_timeSlot): ?>
				  <th><div align="center"><?=$_timeSlot?></div></th>
			<? endforeach; ?>	  
			  </tr>
			  </thead>
			<tbody>
		<?php
			foreach ($classLevelAry as $_classLevelId => $_classLevelName):
				$_dayTypeStr = $applicationSetting[$_classLevelId]['DayType']; 
				$_dayType = !empty($_dayTypeStr)?explode(',',$_dayTypeStr):array();
		?>
				<tr id="tr_<?=$_classlevelId?>">
                  <td class="row_class_col"><?=$_classLevelName?></td>
      	<? 
      		foreach($kis_lang['Admission']['TimeSlot'] as $_key => $_timeSlot): 
      			$_hasCheck = (count($_dayType)>0)?(in_array($_key,$_dayType))?true:false:false;
      	?>
			<td<?=$_hasCheck?' class="row_present"':''?>><div align="center"><input type="checkbox" class="timeSlotSettings" name="timeSlotSettings[<?=$_classLevelId?>][]" value="<?=$_key?>"<?=$_hasCheck?' checked="checked""':''?> /></div></td>
		<? endforeach; ?>	 
			  </tr>
		<? $idx++; ?>
		<? endforeach; ?>	  
			
			</tbody>
		</table>
  		<p class="spacer"></p><p class="spacer"></p><br>
    </div>
	<div class="edit_bottom">       
		<input type="hidden" name="schoolYearId" id="schoolYearId" value="<?=$schoolYearID?>">  
	    <input id ="submit" name="submit" type="submit"  value="<?=$kis_lang['save']?>" class="formbutton" >
	</div>	 
	</form>     	  
</div>
                    	