<?php
// Editing by 
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT.'kis/init.php');
include_once($PATH_WRT_ROOT."includes/admission/".$setting_path_ip_rel."/config.php");
$libkis_admission = $libkis->loadApp('admission');

$Title = stripslashes(trim($_REQUEST['Title']));
$Content = stripslashes(trim($_REQUEST['Content']));
$RecordType = $_REQUEST['RecordType'];
$TemplateID = $_REQUEST['TemplateID'];
$TemplateType = $_REQUEST['TemplateType'];
$TemplateClassLevel = $_REQUEST['TemplateClassLevel'];
$AttachmentID = (array)$_REQUEST['AttachmentID']; // existing attachment ids

$email_attachment_type = $libkis_admission->getEmailAttachmentType('EmailTemplate');

if($TemplateID==''){
	$is_edit = false;
}else{
	$is_edit = true;
}

$updated_template_id = $libkis_admission->updateEmailTemplateRecord($Title,$Content,$RecordType,$TemplateID, $TemplateType, $TemplateClassLevel);

$resultAry = array();
if($is_edit){
	$attachmentRecords = $libkis_admission->getEmailAttachmentRecords($email_attachment_type, $TemplateID);
	$existing_attachment_ids = Get_Array_By_Key($attachmentRecords,'AttachmentID');
	
	$attachmentIdAryToRemove = array_values(array_diff($existing_attachment_ids,$AttachmentID));
	if(count($attachmentIdAryToRemove)>0){
		foreach($attachmentIdAryToRemove as $k => $id){
			$libkis_admission->deleteEmailAttachment($id);
		}
	}
}

if($updated_template_id != '' && $updated_template_id > 0)
{
	$resultAry['UpdateRecord'] = true;
	$resultAry['UploadFile'] = $libkis_admission->addEmailAttachment($email_attachment_type, $updated_template_id, $_FILES);
}else{
	$resultAry['UpdateRecord'] = false;
}

$sysMsg = !in_array(false,$resultAry)? "UpdateSuccess" : "UpdateUnsuccess";
header("Location: /kis/#apps/admission/settings/email_template/?sysMsg=".$sysMsg);
?>