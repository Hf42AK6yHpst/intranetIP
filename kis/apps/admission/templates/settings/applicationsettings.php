<?
// Editing by 

/**
 * **************************************
 * 2015-07-20 (Henry): added Interview Arrangement tab
 * **************************************
 */
?>
<script type="text/javascript">
$(function(){
    kis.admission.application_settings_init(
        {
            returnmsg:'<?=$kis_lang['Admission']['ReturnMessage'][$sysMsg]?>',
            close:'<?=$kis_lang['close']?>'
        }
        
    );
});
</script>
<div class="main_content_detail">
    <?
    
    if ($sys_custom['KIS_Admission']['ICMS']['Settings']) {
        kis_ui::loadModuleTab(array(
            'applicationsettings',
            'instructiontoapplicants',
            'attachmentsettings',
            'email_template'
        ), '', '#/apps/admission/settings/');
    } else if($sys_custom['KIS_Admission']['STANDARD']['Settings']){
    	kis_ui::loadModuleTab(array('applicationsettings',
            'instructiontoapplicants',
            'attachmentsettings',
            'email_template','printformheader','onlinepayment', 'modulesettings'), '', '#/apps/admission/settings/');
    }
      // else if($sys_custom['KIS_Admission']['InterviewArrangement']){
      // kis_ui::loadModuleTab(array('applicationsettings','instructiontoapplicants','attachmentsettings','email_template', 'interviewarrangement'), '', '#/apps/admission/settings/');
      // }
    else {
        kis_ui::loadModuleTab(array(
            'applicationsettings',
            'instructiontoapplicants',
            'attachmentsettings',
            'email_template'
        ), '', '#/apps/admission/settings/');
    }
    ?>
    <p class="spacer"></p>
     <? if(!empty($warning_msg)):?>
         <?=$warning_msg?>
    <? endif; ?>
    <div id="table_filter">
		<form class="filter_form"> 
        <?=$schoolYearSelection?>
    </form>
	</div>
	<!---->

	<p class="spacer"></p>
	<div class="table_board">
		<table class="common_table_list edit_table_list">
			<colgroup>
				<col nowrap="nowrap">
			</colgroup>
			<thead>
				<tr>
					<th width="20">&nbsp;</th>
                  	<?if(!$sys_custom['KIS_Admission']['ICMS']['Settings'] && !$sys_custom['KIS_Admission']['MUNSANG']['Settings']):?>
                        <th><?=$kis_lang['form']?></th>
					<th style="width:150px;"><?=$kis_lang['from']?></th>
					<th style="width:150px;"><?=$kis_lang['to']?></th>
					<th style="width:170px;"><?=$kis_lang['availabletimeslot']?></th>
                    <?else:?>
                      <th width="32%"><?=$kis_lang['form']?></th>
					<th width="32%"><?=$kis_lang['from']?></th>
					<th width="32%"><?=$kis_lang['to']?></th>
                    <?endif;?>
                    
                    <?php if($sys_custom['KIS_Admission']['PFK']['Settings']): ?>
                    	<th><?=$kis_lang['Admission']['PFK']['school']?></th>
                    <?php endif; ?>
                    
                </tr>
			</thead>
			<tbody>
            <?php
            $idx = 1;
            foreach ($applicationSetting as $_classlevelId => $_classLevelSettingAry) :
                if ($sys_custom['KIS_Admission']['ICMS']['Settings'])
                    $classlevel = $_classLevelSettingAry['ClassLevelName'];
                $_dayTypeAry = explode(',', $_classLevelSettingAry['DayType']);
                $_dayType = array();
                foreach ($_dayTypeAry as $_key) :
                    $_dayType[] = $sys_custom['KIS_Admission']['ICMS']['Settings'] ? $kis_lang['Admission']['icms'][$classlevel]['TimeSlot'][$_key] : $kis_lang['Admission']['TimeSlot'][$_key];
                endforeach
                ;
                ?>
                    <tr id="tr_<?=$_classlevelId?>">
					<td><?=$idx?></td>
					<td><a class="td_form_name"
						href="#/apps/admission/settings/edit/<?=$_classlevelId?>/"><?=$_classLevelSettingAry['ClassLevelName']?></a></td>
					<td><?=kis_ui::displayTableField($_classLevelSettingAry['StartDate'])?></td>
					<td><?=kis_ui::displayTableField($_classLevelSettingAry['EndDate'])?></td>
                                      <?if(!$sys_custom['KIS_Admission']['ICMS']['Settings'] && !$sys_custom['KIS_Admission']['MUNSANG']['Settings']){?>
                                      <td><?=implode($sys_custom['KIS_Admission']['ICMS']['Settings']?'<br/>':' / ',$_dayType)?></td>
                                      <?}?>
                    <?php
                if ($sys_custom['KIS_Admission']['PFK']['Settings']) :
                    $selectSchoolIdArr = explode(',', $_classLevelSettingAry['CustSetting']);
                    $selectSchoolNameArr = array();
                    foreach ($selectSchoolIdArr as $schoolId) {
                        $schoolType = $admission_cfg['SchoolType'][$schoolId];
                        $selectSchoolNameArr[] = $kis_lang['Admission']['PFK']['schoolType'][$schoolType];
                    }
                    $selectSchoolNameHtml = implode(', ', (array) $selectSchoolNameArr);
                    ?>
                    	<td><?=kis_ui::displayTableField($selectSchoolNameHtml)?></td>
                <?php
                    endif;
                ?>
                  </tr>
                            <? $idx++; ?>
                            <? endforeach; ?>      
                            
                            </tbody>
		</table>
		<p class="spacer"></p>
		<p class="spacer"></p>
		<br>
		<?=$kis_lang['Admission']['admissionformlink']?>: <a target="_blank" href="<?=(checkHttpsWebProtocol())? 'https':'http'?>://<?=$_SERVER['HTTP_HOST']?>/kis/admission_form/"><?=(checkHttpsWebProtocol())? 'https':'http'?>://<?=$_SERVER['HTTP_HOST']?>/kis/admission_form/</a>
		<br>
		*<?=$kis_lang['Admission']['admissionformlinkremark']?>
		<br><br>
		<?if(!$sys_custom['KIS_Admission']['MINGWAI']['Settings'] && !$sys_custom['KIS_Admission']['MINGWAIPE']['Settings']){?>
		<?=$kis_lang['Admission']['admissionformEditlink']?>: <a target="_blank" href="<?=(checkHttpsWebProtocol())? 'https':'http'?>://<?=$_SERVER['HTTP_HOST']?>/kis/admission_form/index_edit.php"><?=(checkHttpsWebProtocol())? 'https':'http'?>://<?=$_SERVER['HTTP_HOST']?>/kis/admission_form/index_edit.php</a>
		<? }?>
	</div>
</div>

