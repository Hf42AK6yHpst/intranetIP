<?
// Editing by Henry
/****************************************
 *	2015-07-20 (Henry):	added Interview Arrangement tab
 ****************************************/
?>
<script type="text/javascript">
$(function(){
	kis.admission.application_instruction_edit_init();
});
</script>
<div class="main_content_detail">
    <? 
    if($sys_custom['KIS_Admission']['ICMS']['Settings']){
    	kis_ui::loadModuleTab(array('applicationsettings','instructiontoapplicants','attachmentsettings','email_template'), 'instructiontoapplicants', '#/apps/admission/settings/');
    }
else if($sys_custom['KIS_Admission']['STANDARD']['Settings']){
    	kis_ui::loadModuleTab(array('applicationsettings',
            'instructiontoapplicants',
            'attachmentsettings',
            'email_template','printformheader','onlinepayment', 'modulesettings'), 'instructiontoapplicants', '#/apps/admission/settings/');
    }
//    else if($sys_custom['KIS_Admission']['InterviewArrangement']){
//	    kis_ui::loadModuleTab(array('applicationsettings','instructiontoapplicants','attachmentsettings','email_template', 'interviewarrangement'), 'instructiontoapplicants', '#/apps/admission/settings/');
//	}
    else{
    	kis_ui::loadModuleTab(array('applicationsettings','instructiontoapplicants','attachmentsettings','email_template'), 'instructiontoapplicants', '#/apps/admission/settings/');
    }
    ?>
    <p class="spacer"></p>
    <?=$kis_data['NavigationBar']?>
    <p class="spacer"></p>
    <div class="table_board">
    <form id="application_settings">
	    <table class="form_table">
	    	<tr>
	    		<td>
					<textarea id="generalInstruction" name="generalInstruction"><?=$basicSettings['generalInstruction']?></textarea>
		        </td>
	        </tr>       
	    </table>
        <div class="edit_bottom">
       		<input type="hidden" name="schoolYearId" id="schoolYearId" value="<?=$schoolYearID?>">
        	<input type="submit" id="submitBtn" class="formbutton" value="<?=$kis_lang['submit']?>" />
            <input type="button" id="cancelBtn" class="formsubbutton" value="<?=$kis_lang['cancel']?>" />
        </div>
    </form>
	</div>
	        				
</div>

   