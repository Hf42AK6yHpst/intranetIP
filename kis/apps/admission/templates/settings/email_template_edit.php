<?php
// Editing by Henry
/****************************************
 *    2015-07-20 (Henry):    added Interview Arrangement tab
 ****************************************/
include_once($intranet_root . "/lang/lang." . $intranet_session_language . ".php");

$linterface = $kis_data['libinterface'];

$template_type_active = $kis_data['libadmission']->getEmailTemplateType('Active');
$template_type_draft = $kis_data['libadmission']->getEmailTemplateType('Draft');

if (count($kis_data['emailTemplateRecords']) > 0) {
    $is_edit = true;
    $attachmentAry = $kis_data['libadmission']->getEmailAttachmentRecords($kis_data['libadmission']->getEmailAttachmentType('EmailTemplate'), $kis_data['emailTemplateRecords'][0]['TemplateID']);
} else {
    $is_edit = false;
    $attachmentAry = array();
}

?>
<script type="text/javascript">
    kis.admission.email_template_init = function (msg) {
        var msg_request_input_email_template_title = msg.request_input_email_template_title;
        var msg_request_input_email_template_content = msg.request_input_email_template_content;
        var msg_confirm_delete_email_template_attachment = msg.confirm_delete_email_template_attachment;

        var sysmsg = msg.returnmsg;
        if (sysmsg != '') {
            var returnmsg = sysmsg.split('|=|')[1];
            returnmsg += '<a href="javascript:void(0);" onclick="$(\'div#system_message\').hide();" >[' + msg.close + ']</a>';
            var msgclass = sysmsg.split('|=|')[0] == 1 ? 'system_success' : 'system_alert';
            $('div#system_message').removeClass();
            $('div#system_message').addClass(msgclass).show();
            $('div#system_message span').html(returnmsg);
            setTimeout(function () {
                $('#system_message').hide();
            }, 3000);
        }

        var editor = kis.editor('Content');

        var deleteUploadFile = function (obj) {
            $(obj).parent().remove();
        };

        var deleteAttachment = function (obj) {
            if (confirm(msg_confirm_delete_email_template_attachment)) {
                $(obj).parent().remove();
            }
        };

        var checkEmailTemplateRecord = function () {
            var valid = true;
            var warn_title = $('#WarnTitle');
            var warn_content = $('#WarnContent');
            var title = $.trim($('input#Title').val());
            var content = '';
            var trim_content = '';
            var status = $('input[name="RecordType"]:checked').val();
            var template_id = $('#TemplateID').val();
            if (editor) {
                content = FCKeditorAPI.GetInstance('Content').GetXHTML(false);
                trim_content = $.trim(content.replace(/(<br \/>)|(<br>)|(&nbsp;)/gi, ''));
            } else {
                content = $('#Content').val();
                trim_content = $.trim(content);
            }
            if (title == '') {
                valid = false;
                warn_title.show();
            } else {
                warn_title.hide();
            }
            if (trim_content == '') {
                valid = false;
                warn_content.show();
            } else {
                warn_content.hide();
            }
            if (!valid) return false;

            return true;
        };

        $('a[name="DeleteFileUpload\\[\\]"]:last').click(function () {
            deleteUploadFile(this);
        });

        $('#add_more_attachment').click(function () {
            var file_upload_count = parseInt($('input#FileUploadCount').val());
            file_upload_count += 1;
            var input_file = '<div><input type="file" name="FileUpload_' + file_upload_count + '" /><a href="javascript:void(0);" class="btn_remove" name="DeleteFileUpload[]" ></a></div>';
            $('div#FileUploadDiv').append(input_file);
            $('a[name="DeleteFileUpload\\[\\]"]:last').click(function () {
                deleteUploadFile(this);
            });
            $('input#FileUploadCount').val(file_upload_count);
        });

        $('a[name="AttachmentRemoveBtn\\[\\]"]').click(function () {
            deleteAttachment(this);
        });

        $('input#submitBtn').click(function () {
            var formObj = $('form#MailForm');
            if (checkEmailTemplateRecord()) {
                formObj.submit();
            }
        });

        $('input#cancelBtn').click(function () {
            $.address.value('/apps/admission/settings/email_template/');
        });
    };

    $(function () {
        kis.admission.email_template_init({
            'close': '<?=$kis_lang['close']?>',
            'delete': '<?=$kis_lang['delete']?>',
            'request_input_email_template_title': '<?=$kis_lang['msg']['requestinputemailtemplatetitle']?>',
            'request_input_email_template_content': '<?=$kis_lang['msg']['request_input_email_template_content']?>',
            'confirm_delete_email_template_attachment': '<?=$kis_lang['msg']['confirm_delete_email_template_attachment']?>',
            'returnmsg': '<?=$kis_lang['Admission']['ReturnMessage'][$sysMsg]?>'
        });

        <?php if($sys_custom['KIS_Admission']['HKUGAPS']['Settings'] || $sys_custom['KIS_Admission']['HKUGAC']['Settings'] || $sys_custom['KIS_Admission']['SFAEPS']['Settings']){ ?>
        $('#TemplateType').change(function () {
            if ($(this).val() == '0') {
                $('#emailTemplateClassLevelDiv').hide();
                $('#emailTemplateClassLevelDiv input').prop('disabled', true);
            } else {
                $('#emailTemplateClassLevelDiv').show();
                $('#emailTemplateClassLevelDiv input').prop('disabled', false);
            }
        }).change();
        <?php } ?>
    });
</script>
<div class="main_content_detail">
    <?
    if ($sys_custom['KIS_Admission']['ICMS']['Settings']) {
        kis_ui::loadModuleTab(array('applicationsettings', 'instructiontoapplicants', 'attachmentsettings', 'email_template'), 'email_template', '#/apps/admission/settings/');
    }
else if($sys_custom['KIS_Admission']['STANDARD']['Settings']){
    	kis_ui::loadModuleTab(array('applicationsettings',
            'instructiontoapplicants',
            'attachmentsettings',
            'email_template','printformheader','onlinepayment', 'modulesettings'), 'email_template', '#/apps/admission/settings/');
    }
//else if($sys_custom['KIS_Admission']['InterviewArrangement']){
//    kis_ui::loadModuleTab(array('applicationsettings','instructiontoapplicants','attachmentsettings','email_template', 'interviewarrangement'), 'email_template', '#/apps/admission/settings/');
//}
    else {
        kis_ui::loadModuleTab(array('applicationsettings', 'instructiontoapplicants', 'attachmentsettings', 'email_template'), 'email_template', '#/apps/admission/settings/');
    }
    ?>
    <p class="spacer"></p>
    <form id="MailForm" name="MailForm" method="POST"
          action="apps/admission/templates/settings/email_template_edit_update.php" enctype="multipart/form-data">
        <?= $kis_data['NavigationBar'] ?>
        <p class="spacer"></p><br>

        <div class="table_board">
            <table id="FormTable" class="form_table">
                <tbody>
                <?php if ($sys_custom['KIS_Admission']['HKUGAC']['Settings'] || $sys_custom['KIS_Admission']['HKUGAPS']['Settings'] || $sys_custom['KIS_Admission']['SFAEPS']['Settings']) { ?>
                    <tr>
                        <td class="field_title"
                            style="width:30%;"><?= $kis_lang['Admission']['HKUGAPS']['templateType'] ?><?= $mustfillinsymbol ?></td>
                        <td style="width:70%;">
                            <?php
                            $selectedNormal = 'selected';
                            $templateType = $kis_data['emailTemplateRecords'][0]['TemplateType'];
                            $langArr = array('b5', 'en');
                            ?>
                            <select id="TemplateType" name="TemplateType">
                                <option value="<?= $admission_cfg['EmailTemplateType']['Normal'] ?>" <?= $selectedNormal ?>>
                                    <?= $kis_lang['Admission']['HKUGAPS']['templateTypeType']['Normal'] ?>
                                </option>

                                <?php
                                for ($i = 1; $i <= 3; $i++) {
                                    $optgroupLabel = $kis_lang['Admission']['HKUGAPS']['templateTypeType']["Round{$i}_Interview"][$intranet_session_language];
                                    if ($sys_custom['KIS_Admission']['HKUGAC']['Settings']) {
                                        $optgroupLabel = $kis_lang['Admission']['HKUGAC']['templateTypeType']["Round{$i}_Interview"][$intranet_session_language];
                                    }
                                    ?>
                                    <optgroup label="<?= $optgroupLabel ?>">
                                        <?php
                                        foreach ($admission_cfg['EmailTemplateType'] as $key => $value) {
                                            if (strpos($key, "Round{$i}") !== 0) {
                                                continue;
                                            }
                                            foreach ($langArr as $lang) {
                                                $selected = ($templateType == $value[$lang]) ? 'selected' : '';
                                                ?>
                                                <option value="<?= $value[$lang] ?>" <?= $selected ?>>
                                                    <?php
                                                    if ($sys_custom['KIS_Admission']['HKUGAC']['Settings']) {
                                                        echo $kis_lang['Admission']['HKUGAC']['templateTypeType'][$key][$lang];
                                                    } else {
                                                        echo $kis_lang['Admission']['HKUGAPS']['templateTypeType'][$key][$lang];
                                                    }
                                                    ?>
                                                </option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </optgroup>
                                    <?php
                                }
                                ?>


                                <optgroup label="<?= $kis_lang['Admission']['others'] ?>">
                                    <?php
                                    $othersTemplateArr = array(
                                        'Admitted' => $admission_cfg['EmailTemplateType']['Admitted'],
                                        'NotAdmitted' => $admission_cfg['EmailTemplateType']['NotAdmitted']
                                    );
                                    foreach ($othersTemplateArr as $key => $value) {
                                        foreach ($langArr as $lang) {
                                            $selected = ($templateType == $value[$lang]) ? 'selected' : '';
                                            ?>
                                            <option value="<?= $value[$lang] ?>" <?= $selected ?>>
                                                <?php
                                                if ($sys_custom['KIS_Admission']['HKUGAC']['Settings']) {
                                                    echo $kis_lang['Admission']['HKUGAC']['templateTypeType'][$key][$lang];
                                                } else {
                                                    echo $kis_lang['Admission']['HKUGAPS']['templateTypeType'][$key][$lang];
                                                }
                                                ?>
                                            </option>
                                            <?php
                                        }
                                    }
                                    ?>
                                </optgroup>
                            </select>
                            <div id="emailTemplateClassLevelDiv" style="margin-top: 5px;">
                                <?php
                                $selectedClassLevelArr = explode(',', $kis_data['emailTemplateRecords'][0]['TemplateClassLevel']);
                                foreach ($kis_data['libadmission']->classLevelAry as $classLevelId => $classLevel) {
                                    $checked = (in_array($classLevelId, $selectedClassLevelArr)) ? 'checked' : '';
                                    ?>
                                    <label>
                                        <input type="checkbox" name="TemplateClassLevel[]"
                                               value="<?= $classLevelId ?>" <?= $checked ?> />
                                        <?= $classLevel ?>
                                    </label>
                                    <br/>
                                    <?php
                                }
                                ?>
                            </div>
                        </td>
                    </tr>
                <?php } ?>

                <tr>
                    <td class="field_title" style="width:30%;"><?= $kis_lang['subject'] ?><?= $mustfillinsymbol ?></td>
                    <td style="width:70%;">
                        <?= $linterface->GET_TEXTBOX("Title", "Title", $kis_data['emailTemplateRecords'][0]['Title'], $OtherClass = '', $OtherPar = array()) . '<br />' ?>
                        <?= $linterface->Get_Form_Warning_Msg("WarnTitle", $kis_lang['msg']['requestinputemailtemplatetitle'], $Class = '', $display = false) ?>
                    </td>
                </tr>
                <tr>
                    <td class="field_title"
                        style="width:30%;"><?= $kis_lang['email_template_content'] ?><?= $mustfillinsymbol ?></td>
                    <td style="width:70%;">
                        <textarea id="Content"
                                  name="Content"><?= $kis_data['emailTemplateRecords'][0]['Content'] ?></textarea><br/>
                        <div class="text_remark"><?= ($sys_custom['KIS_Admission']['MINGWAIPE']['Settings'] || $sys_custom['KIS_Admission']['MINGWAI']['Settings'] ? $kis_lang['emailvariableremarkForMingWai'] : ($sys_custom['KIS_Admission']['HKUGAPS']['Settings'] ? $kis_lang['emailvariableremarkForHKUGAPS'] : ($sys_custom['KIS_Admission']['HKUGAC']['Settings'] ? $kis_lang['emailvariableremarkForHKUGAC'] : ($sys_custom['KIS_Admission']['CREATIVE']['Settings'] ? $kis_lang['emailvariableremarkForCreative'] : ($sys_custom['KIS_Admission']['STANDARD']['Settings'] ? $kis_lang['emailvariableremarkForStandard'] : $kis_lang['emailvariableremark']))))) ?></div>
                        <?= $linterface->Get_Form_Warning_Msg("WarnContent", $kis_lang['msg']['request_input_email_template_content'], $Class = '', $display = false) ?>
                    </td>
                </tr>
                <tr>
                    <td class="field_title" style="width:30%;"><?= $kis_lang['status'] ?></td>
                    <td style="width:70%;">
                        <?= $linterface->Get_Radio_Button("RecordTypeActive", "RecordType", $template_type_active, $kis_data['emailTemplateRecords'][0]['RecordType'] == $template_type_active || $kis_data['emailTemplateRecords'][0]['RecordType'] == '', $Class = '', $Display = $kis_lang['email_template_active'], $Onclick = '', $Disabled = '') . '&nbsp;' ?>
                        <?= $linterface->Get_Radio_Button("RecordTypeDraft", "RecordType", $template_type_draft, $kis_data['emailTemplateRecords'][0]['RecordType'] == $template_type_draft, $Class = '', $Display = $kis_lang['email_template_draft'], $Onclick = '', $Disabled = '') ?>
                    </td>
                </tr>
                <tr>
                    <td class="field_title" style="width:30%;"><?= $kis_lang['email_template_attachment'] ?></td>
                    <td style="width:70%;">
                        <div class="mail_icon_form attachment_list">
                            <div id="AttachmentDiv">
                                <?php
                                $x = '';
                                for ($i = 0; $i < count($attachmentAry); $i++) {
                                    $size = $attachmentAry[$i]['SizeInBytes'];
                                    /*
                                    if($size < 1024){
                                        $display_size = '&nbsp;('.$size.' bytes)';
                                    }else if($size < 1024 * 1024){
                                        $display_size = '&nbsp;('.sprintf("%.2f",$size/1024).' KB)';
                                    }else{
                                        $display_size = '&nbsp;('.sprintf("%.2f",($size/(1024*1024))).' MB)';
                                    }
                                    */
                                    $display_size = '&nbsp;(' . $kis_data['libadmission']->getAttachmentDisplaySize($size) . ')';
                                    $x .= '<div>';
                                    $x .= '<a class="btn_attachment" href="/kis/apps/admission/ajax.php?action=viewEmailAttachment&AttachmentID=' . $attachmentAry[$i]['AttachmentID'] . '" id="AttachmentFile_' . $attachmentAry[$i]['AttachmentID'] . '" name="AttachmentFile[]">' . $attachmentAry[$i]['FileName'] . $display_size . '</a>';
                                    $x .= '<a class="btn_remove" href="javascript:void(0);" id="AttachmentRemoveBtn_' . $attachmentAry[$i]['AttachmentID'] . '" name="AttachmentRemoveBtn[]" alt="' . $kis_lang['remove'] . '"></a>';
                                    $x .= '<input type="hidden" name="AttachmentID[]" value="' . $attachmentAry[$i]['AttachmentID'] . '" />';
                                    $x .= '</div>';
                                }
                                echo $x;
                                ?>
                            </div>
                            <br style="clear:both;"/>
                            <div id="FileUploadDiv">
                                <div><input type="file" name="FileUpload_0"/><a href="javascript:void(0);"
                                                                                class="btn_remove"
                                                                                name="DeleteFileUpload[]"></a></div>
                            </div>
                            <br style="clear:both;"/>
                            <a href="javascript:void(0);" id="add_more_attachment"
                               class="btn_add"><?= $kis_lang['add'] ?></a>
                            <input type="hidden" id="FileUploadTmpFolder" name="FileUploadTmpFolder" value=""/>
                            <input type="hidden" id="FileUploadCount" name="FileUploadCount" value="0"/>
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
            <div class="text_remark step1" <?= $goto_step == 2 ? 'style="display:none;"' : '' ?>><?= str_replace('*', '<span style="color:red">*</span>', $kis_lang['requiredfield']) ?></div>
            <div class="edit_bottom">
                <input type="hidden" name="TemplateID" id="TemplateID"
                       value="<?= $kis_data['emailTemplateRecords'][0]['TemplateID'] ?>">
                <input type="button" name="submitBtn" id="submitBtn" class="formbutton"
                       value="<?= $kis_lang['submit'] ?>"/>
                <input type="button" name="cancelBtn" id="cancelBtn" class="formsubbutton"
                       value="<?= $kis_lang['cancel'] ?>"/>
            </div>
            <p class="spacer"></p><br>
        </div>
    </form>
</div>