<?php
// Editing by 
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT.'kis/init.php');
include_once($PATH_WRT_ROOT."includes/admission/".$setting_path_ip_rel."/config.php");
$libkis_admission = $libkis->loadApp('admission');

$SettingNameValueAry = array();
$SettingNameValueAry['schoolnamechi'] = stripslashes(trim($_REQUEST['schoolnamechi']));
$SettingNameValueAry['schoolnameeng'] = stripslashes(trim($_REQUEST['schoolnameeng']));
$SettingNameValueAry['schooladditionalinfo'] = stripslashes(trim($_REQUEST['schooladditionalinfo']));
$SettingNameValueAry['printformlabeltype'] = stripslashes(trim($_REQUEST['printformlabeltype']));
$SettingNameValueAry['applicationnoformatstartfrom'] = stripslashes(trim($_REQUEST['applicationnoformatstartfrom']));
$SettingNameValueAry['remarksnamechi'] = stripslashes(trim($_REQUEST['remarksnamechi']));
$SettingNameValueAry['remarksnameeng'] = stripslashes(trim($_REQUEST['remarksnameeng']));
$resultAry['SaveBasicSettings'] = $libkis_admission->saveBasicSettings(99999, $SettingNameValueAry);

$tmp_path = $_FILES['schoollogo']['tmp_name'];
$file_name = $_FILES['schoollogo']['name'];
//debug_pr($_FILES);die();
if($file_name != '' && $tmp_path != ''){ 
	
	if ($img_path = $tmp_path) {
    list ($width, $height, $img_type) = getimagesize($img_path);

	$sr = $width / $height;
    $rw = $width > kis::$personal_photo_width ? kis::$personal_photo_width:$width;
    $rh = $height > kis::$personal_photo_height ? kis::$personal_photo_height : $height;

    if ($sr > 1 / 1.3) {
        $rw = floor($rh * $sr);
    } else {
        $rh = floor($rw / $sr);
    }

    // Maximum size is 1MB for now
    if (filesize($img_path) < 10485760) {
        switch ($img_type) {
            case IMAGETYPE_GIF:
                $image = imagecreatefromgif($img_path);
                break;
            case IMAGETYPE_JPEG:
                $image = @imagecreatefromjpeg($img_path);
                if (!$image){
					$image = imagecreatefromstring(file_get_contents($img_path));
				}
                break;
            case IMAGETYPE_PNG:
	            $rimage = imagecreatetruecolor($rw, $rh);
	            imagealphablending($rimage, false);
				imagesavealpha($rimage, true); 
                $image = imagecreatefrompng($img_path);
                break;
        }
    }
}

if ($image) {
	$exif = @exif_read_data($img_path);

    //orientation handling [Start]
    if (!empty($exif['Orientation'])) {
    	ini_set('memory_limit','200M');
        switch ($exif['Orientation']) {
            case 3:
                $image = imagerotate($image, 180, 0);
                break;

            case 6:
                $image = imagerotate($image, -90, 0);
                $temp_width = $width;
                $width = $height;
                $height = $temp_width;
                break;

            case 8:
                $image = imagerotate($image, 90, 0);
                $temp_width = $width;
                $width = $height;
                $height = $temp_width;
                break;
        }
    }
    //orientation handling [End]

    $filename = kis_utility::getSaveFileName($file_name);
    $filename = $libkis_admission->encrypt_attachment($filename);
    $image_url = $libkis_admission->filepath . $id . '/personal_photo';
    $image_path = $file_path . $image_url;
    
	if($img_type!=IMAGETYPE_PNG){
		$rimage = imagecreatetruecolor($rw, $rh);
	}
    imagecopyresampled($rimage, $image, 0, 0, 0, 0, $rw, $rh, $width, $height);

	$base_dir = $file_path."/file/kis";

		if(!file_exists($base_dir) || !is_dir($base_dir)){
			mkdir($base_dir, 0777, true);
		}

		$base_dir = $file_path."/file/kis/admission";
		if(!file_exists($base_dir) || !is_dir($base_dir)){
			mkdir($base_dir, 0777, true);
		}

		$target_dir = $file_path."/file/kis/admission/images";
		if(!file_exists($target_dir) || !is_dir($target_dir)){
			mkdir($target_dir, 0777, true);
		}

	$target_path = $target_dir."/logo.png";
	if(file_exists($target_path)){
		unlink($target_path);
	}
	switch ($img_type) {
            case IMAGETYPE_GIF:
                imagegif($rimage, $target_path);
                break;
            case IMAGETYPE_JPEG:
                imagejpeg($rimage, $target_path, 100);
                break;
            case IMAGETYPE_PNG:
                imagepng($rimage, $target_path);
                break;
        }
	
	chmod($target_path,"0777");
	
}
}

$sysMsg = !in_array(false,$resultAry)? "UpdateSuccess" : "UpdateUnsuccess";
header("Location: /kis/#/apps/admission/settings/printformheader/?sysMsg=".$sysMsg);
?>