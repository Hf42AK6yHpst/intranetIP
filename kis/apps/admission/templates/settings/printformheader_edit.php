<?php
// using:

/**
 * **************************************
 * 2019-11-29 (Henry): file created
 * **************************************
 */
?>
<script type="text/javascript">
kis.admission.print_form_header_edit_init = function(msg){
	var instruction_editor = kis.editor('schooladditionalinfo');
	var schoolYearId = $('#schoolYearId').val();
	var msg_confirm_delete_email_template_attachment = msg.confirm_delete_email_template_attachment;
		
	var sysmsg = msg.returnmsg;
    if (sysmsg != '') {
        var returnmsg = sysmsg.split('|=|')[1];
        returnmsg += '<a href="javascript:void(0);" onclick="$(\'div#system_message\').hide();" >[' + msg.close + ']</a>';
        var msgclass = sysmsg.split('|=|')[0] == 1 ? 'system_success' : 'system_alert';
        $('div#system_message').removeClass();
        $('div#system_message').addClass(msgclass).show();
        $('div#system_message span').html(returnmsg);
        setTimeout(function () {
            $('#system_message').hide();
        }, 3000);
    }
    
   var deleteUploadFile = function (obj) {
        $(obj).parent().remove();
    };

    var deleteAttachment = function (obj) {
        if (confirm(msg_confirm_delete_email_template_attachment)) {
            $(obj).parent().remove();
            $('#FileUploadDiv').show();
        }
    };
    
    $('a[name="DeleteFileUpload\\[\\]"]:last').click(function () {
        deleteUploadFile(this);
    });
    
    $('a[name="AttachmentRemoveBtn\\[\\]"]').click(function () {
        deleteAttachment(this);
    });
        
	$('#print_form_header_form').submit(function(){
				instruction_editor.finish();
		}); 	
	$('input#cancelBtn').click(function(){
		$.address.value('/apps/admission/settings/printformheader/');
	});
	
	$('input#submitBtn').click(function(){
		var formObj = $('form#print_form_header_form');
		if(checkapplicationstartnumber()){
			formObj.submit();
		}return false;
	});  
	var checkapplicationstartnumber = function(){
		if(!is_positive_int($('#applicationnoformatstartfrom').val())){
			alert(msg.enterpositivenumber);
			$('#applicationnoformatstartfrom').focus();
			return false;
		}else if($('#applicationnoformatstartfrom').val() < 1 || $('#applicationnoformatstartfrom').val() > 5001){
			alert(msg.applicationnoformatstartfromreange);
			$('#applicationnoformatstartfrom').focus();
			return false;
		}
		return true;
	};
};
$(function(){
    kis.admission.print_form_header_edit_init({
        invaliddateformat:'<?=$kis_lang['Admission']['msg']['invaliddateformat']?>',
        enddateearlier: '<?=$kis_lang['Admission']['msg']['enddateearlier']?>',
        emptyapplicationdatewarning: '<?=$kis_lang['Admission']['msg']['emptyapplicationdatewarning']?>',
        selectatleastonetimeslot: '<?=$kis_lang['Admission']['msg']['selectatleastonetimeslot']?>',
        confirm_delete_email_template_attachment: '<?=$kis_lang['msg']['confirm_delete_email_template_attachment']?>','returnmsg': '<?=$kis_lang['Admission']['ReturnMessage'][$sysMsg]?>',
        enterpositivenumber: '<?=$kis_lang['Admission']['msg']['enternumber']?>',
        applicationnoformatstartfromreange: '<?=$kis_lang['applicationnoformatstartfromreange']?>'
    });
});
</script>
<div class="main_content">
	<form id='print_form_header_form' name="print_form_header_form" method="POST"
          action="apps/admission/templates/settings/printformheader_edit_update.php" enctype="multipart/form-data">
		<div class="main_content_detail">
    <?php
    kis_ui::loadModuleTab(array(
        'applicationsettings',
        'instructiontoapplicants',
        'attachmentsettings',
        'email_template',
		'printformheader',
		'onlinepayment',
		'modulesettings'
    ), 'printformheader', '#/apps/admission/settings/');
    ?>
        <p class="spacer"></p>
        <?=$kis_data['NavigationBar']?>
        <p class="spacer"></p>
			<div class="table_board">
				<table id="FormTable" class="form_table">
		        	<tbody>
		                <tr> 
					       <td width="20%" class="field_title"><?=$kis_lang['schoolnamechi']?></td>
					       <td width="50%"><input type="text" id="schoolnamechi" name="schoolnamechi" value="<?=$basicSettings['schoolnamechi']?>" style="width:500px" /></td>
					     </tr>
					     <tr> 
					       <td width="20%" class="field_title"><?=$kis_lang['schoolnameeng']?></td>
					       <td width="50%"><input type="text" id="schoolnameeng" name="schoolnameeng" value="<?=$basicSettings['schoolnameeng']?>" style="width:500px" /></td>
					     </tr>
					     <tr> 
					       <td width="20%" class="field_title"><?=$kis_lang['schoollogo']?></td>
					       <td width="50%">
					         <div class="mail_icon_form attachment_list">
	                            <div id="AttachmentDiv">
	                                <?php
	                                if(file_exists($file_path.'/file/kis/admission/images/logo.png')){
		                                $x = '';
	                                    $x .= '<div>';
	                                    $x .= '<img src="/file/kis/admission/images/logo.png" />';
	                                    $x .= '<input type="hidden" name="schoollogo" value="' . $basicSettings['schoollogo']['AttachmentID'] . '" />';
	                                    $x .= '</div>';
		                                echo $x;
	                                }
	                                ?>
	                            </div>
	                            <div id="FileUploadDiv">
	                                <div><input type="file" name="schoollogo"/><br/><br/><?=$kis_lang['imageformatinjpeggifpng']?></div> 
	                            </div>
	                            <input type="hidden" id="FileUploadTmpFolder" name="FileUploadTmpFolder" value=""/>
	                            <input type="hidden" id="FileUploadCount" name="FileUploadCount" value="0"/>
	                        </div>
					       </td>
					     </tr>
					     <tr> 
					       <td width="20%" class="field_title"><?=$kis_lang['schooladditionalinfo']?></td>
					       <td width="50%"><textarea id="schooladditionalinfo" name="schooladditionalinfo"><?=$basicSettings['schooladditionalinfo']?></textarea></td>
					     </tr>
					     <tr> 
					       <td width="20%" class="field_title"><?=$kis_lang['label']?></td>
					       <td width="50%">
					       <input type="radio" name="printformlabeltype" id="printformlabeltype_y" value="qrcode" <?=($basicSettings['printformlabeltype']=='qrcode')? 'checked' : '' ?> />
                            <label for="printformlabeltype_y"><?=$kis_lang['qrcode'] ?></label>

                            &nbsp;&nbsp; <input type="radio" name="printformlabeltype" id="printformlabeltype_n" value="barcode" <?=($basicSettings['printformlabeltype']=='qrcode')? '' : 'checked' ?> />
                            <label for="printformlabeltype_n"><?=$kis_lang['barcode']  ?></label>
					       </td>
					     </tr>
					     <tr> 
					       <td width="20%" class="field_title"><?=$kis_lang['applicationnoformatstartfrom']?></td>
					       <td width="50%">
					       	<?=$yearStart?> <input type="text" id="applicationnoformatstartfrom" name="applicationnoformatstartfrom" value="<?=$basicSettings['applicationnoformatstartfrom']?str_pad($basicSettings['applicationnoformatstartfrom'], 4, '0', STR_PAD_LEFT):'0001'?>" maxlength="4" size="4" />
                			(<?=$kis_lang['applicationnoformatstartfromreange']  ?>)
                			</td>
					     </tr>
					     <tr> 
					       <td width="20%" class="field_title"><?=$kis_lang['remarksnamechi']?></td>
					       <td width="50%"><input type="text" id="remarksnamechi" name="remarksnamechi" value="<?=$basicSettings['remarksnamechi']?>" style="width:500px" /></td>
					     </tr>
					     <tr> 
					       <td width="20%" class="field_title"><?=$kis_lang['remarksnameeng']?></td>
					       <td width="50%"><input type="text" id="remarksnameeng" name="remarksnameeng" value="<?=$basicSettings['remarksnameeng']?>" style="width:500px" /></td>
					     </tr>
		            </tbody>
		        </table>
				<p class="spacer"></p>
				<p class="spacer"></p>
				<br />
			</div>
			<div class="text_remark"><?=$kis_lang['requiredfield']?></div>
			<div class="edit_bottom">
				<input type="hidden" name="schoolYearId" id="schoolYearId"
					value="<?=$schoolYearID?>"> <input type="submit" id="submitBtn"
					name="submitBtn" class="formbutton"
					value="<?=$kis_lang['submit']?>" /> <input type="button"
					id="cancelBtn" name="cancelBtn" class="formsubbutton"
					value="<?=$kis_lang['cancel']?>" />
			</div>
		</div>
	</form>
</div>

<script>
$(function(){
	$('.multiLang .main_menu a').click(function(e){
		e.preventDefault();

		var $td = $(this).closest('td');
		var newLang = $(this).attr('href');
		newLang = newLang.substring(0, newLang.length - 1); // Remove last '/'

		$td.find('.main_menu li').removeClass('selected');
		$(this).closest('li').addClass('selected');

		$td.find('.multiLangContainer').hide();
		$td.find('.multiLangContainer.' + newLang).show();
	});
	$('.multiLang .main_menu').find('a:first').click();
});
</script>