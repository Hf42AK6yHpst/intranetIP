<?php
// Editing by Henry
/****************************************
 *	2019-11-29 (Henry):	created this page
 ****************************************/
?>
<script type="text/javascript">
$(function(){
	kis.admission.print_form_header_init({
		'close':'<?=$kis_lang['close']?>',
		'delete':'<?=$kis_lang['delete']?>',
		'confirm_delete_email_template':'<?=$kis_lang['msg']['deleteinterviewarrangement']?>',
		'returnmsg':'<?=$kis_lang['Admission']['ReturnMessage'][$sysMsg]?>'
	});
});
</script>
<div class="main_content_detail">
	<? 
    if($sys_custom['KIS_Admission']['ICMS']['Settings']){
    	kis_ui::loadModuleTab(array('applicationsettings','instructiontoapplicants','attachmentsettings','email_template','printformheader','onlinepayment', 'modulesettings'), 'printformheader', '#/apps/admission/settings/');
    }
//    else if($sys_custom['KIS_Admission']['TBCPK']['Settings']){
//    	kis_ui::loadModuleTab(array('applicationsettings','instructiontoapplicants'), 'instructiontoapplicants', '#/apps/admission/settings/');
//    }
//    else if($sys_custom['KIS_Admission']['InterviewArrangement']){
//	    kis_ui::loadModuleTab(array('applicationsettings','instructiontoapplicants','attachmentsettings','email_template', 'interviewarrangement'), 'instructiontoapplicants', '#/apps/admission/settings/');
//	}
    else{
    	kis_ui::loadModuleTab(array('applicationsettings','instructiontoapplicants','attachmentsettings','email_template','printformheader','onlinepayment', 'modulesettings'), 'printformheader', '#/apps/admission/settings/');
    }
    ?><p class="spacer"></p>
 	<? if(!empty($warning_msg)):?>
 		<?=$warning_msg?>
	<? endif; ?>
	<div class="common_table_tool">
    	<a class="tool_edit" href="#/apps/admission/settings/printformheader/edit/"><?=$kis_lang['edit']?></a>
  	</div>
	<table class="form_table">
	     <tbody>
	     <tr> 
	       <td width="20%" class="field_title"><?=$kis_lang['schoolnamechi']?></td>
	       <td width="50%"><?=$basicSettings['schoolnamechi']?$basicSettings['schoolnamechi']:'--'?></td>
	     </tr>
	     <tr> 
	       <td width="20%" class="field_title"><?=$kis_lang['schoolnameeng']?></td>
	       <td width="50%"><?=$basicSettings['schoolnameeng']?$basicSettings['schoolnameeng']:'--'?></td>
	     </tr>
	     <tr> 
	       <td width="20%" class="field_title"><?=$kis_lang['schoollogo']?></td>
	       <td width="50%"><?=is_file("/file/kis/admission/images/logo.png")?'<img src="/file/kis/admission/images/logo.png" />':'--'?></td>
	     </tr>
	     <tr> 
	       <td width="20%" class="field_title"><?=$kis_lang['schooladditionalinfo']?></td>
	       <td width="50%"><div class="admission_board">
					<p class="spacer"></p>
				    <div class="admission_complete_msg"><?=$basicSettings['schooladditionalinfo']?></div>
				</div></td>
	     </tr>
	     <tr> 
	       <td width="20%" class="field_title"><?=$kis_lang['label']?></td>
	       <td width="50%"><?=$basicSettings['printformlabeltype']=='qrcode'?$kis_lang['qrcode']:$kis_lang['barcode']?></td>
	     </tr>
	     <tr> 
	       <td width="20%" class="field_title"><?=$kis_lang['applicationnoformatstartfrom']?></td>
	       <td width="50%"><?=$yearStart?><?=$basicSettings['applicationnoformatstartfrom']?str_pad($basicSettings['applicationnoformatstartfrom'], 4, '0', STR_PAD_LEFT):'0001'?></td>
	     </tr>
	     <tr> 
	       <td width="20%" class="field_title"><?=$kis_lang['remarksnamechi']?></td>
	       <td width="50%"><?=$basicSettings['remarksnamechi']?$basicSettings['remarksnamechi']:'--'?></td>
	     </tr>
	     <tr> 
	       <td width="20%" class="field_title"><?=$kis_lang['remarksnameeng']?></td>
	       <td width="50%"><?=$basicSettings['remarksnameeng']?$basicSettings['remarksnameeng']:'--'?></td>
	     </tr>
	   </tbody></table>
</div>
