<?php
// Editing by Henry
/****************************************
 *	2015-07-20 (Henry):	added Interview Arrangement tab
 ****************************************/
include_once($intranet_root."/lang/lang.".$intranet_session_language.".php");

$linterface = $kis_data['libinterface'];

//$template_type_active = $kis_data['libadmission']->getEmailTemplateType('Active');
//$template_type_draft = $kis_data['libadmission']->getEmailTemplateType('Draft');

if(count($kis_data['interviewArrangementRecords'])>0){
	$is_edit = true;
}else{
	$is_edit = false;
}

?>
<script type="text/javascript">
kis.admission.interview_arrangement_init = function(msg){
	kis.datepicker('#Date');
		
	$('#Date').keyup(function(){
		var warning = '';
		if(this.value!=''&&!check_date_without_return_msg(this)){
			warning = lang.invaliddateformat;
		}
		$('span#warning_interview_date').html(warning);
	});
	
	var msg_request_input_email_template_title = msg.request_input_email_template_title;
	var msg_request_input_email_template_content = msg.request_input_email_template_content;
	var msg_confirm_delete_email_template_attachment = msg.confirm_delete_email_template_attachment;
	
	var sysmsg = msg.returnmsg;
	if(sysmsg!=''){
		var returnmsg = sysmsg.split('|=|')[1];
		returnmsg += '<a href="javascript:void(0);" onclick="$(\'div#system_message\').hide();" >['+msg.close+']</a>';
		var msgclass = sysmsg.split('|=|')[0]==1?'system_success':'system_alert';
		$('div#system_message').removeClass();
		$('div#system_message').addClass(msgclass).show();
		$('div#system_message span').html(returnmsg);
		setTimeout(function(){
			$('#system_message').hide();
		}, 3000);	
	}
	
	var checkEmailTemplateRecord = function(){
		var Date = $('#Date').val();
		var Quota = $('#Quota').val();
		var NumOfGroup = $('#NumOfGroup').val();
		
		//checking the date

		if(!check_date_without_return_msg(document.getElementById('Date'))){
			$('span#warning_Date').html(msg.invaliddateformat);
			$('#Date').focus();
			return false;
		}else if(compareTimeByObjId('Date','StartTime_hour :selected','StartTime_min :selected','StartTime_sec :selected','Date','EndTime_hour :selected', 'EndTime_min :selected','EndTime_sec :selected')==false){
			$('span#warning_StartTime').html(lang.enddateearlier);
			$('#StartTime').focus();
			return false;
		}else if(!is_positive_int(Quota)){
			alert(msg.enterpositivenumber);
			$('#Quota').focus();
			return false;
		}else if(!is_positive_int(NumOfGroup)){
			alert(msg.enterpositivenumber);
			$('#NumOfGroup').focus();
			return false;
		}
		return true;
	};
	
	$('#AddTimeslot').click(function(){
		var x ='<tr><td style="width:230px">\
					<?=preg_replace('/\s+/', ' ', str_replace("'", "\'", $libadmission->Get_Time_Selection($linterface,'StartTime_{{{index}}}','','',false)))?>&nbsp;<span class="error_msg" id="warning_StartTime_{{{index}}}"></span> ~ \
	                <?=preg_replace('/\s+/', ' ', str_replace("'", "\'", $libadmission->Get_Time_Selection($linterface,'EndTime_{{{index}}}','','',false)))?>&nbsp;<span class="error_msg" id="warning_EndTime_{{{index}}}"></span>\
	                <?=preg_replace('/\s+/', ' ', str_replace("'", "\'", $linterface->Get_Form_Warning_Msg("WarnContent", $kis_lang['msg']['request_input_email_template_content'], $Class='', $display=false)))?>\
	            </td><td><div class="table_row_tool"><a title="<?=$kis_lang['Delete']?>" id="DeleteTimeslot" onClick="$(this).parent().parent().parent().remove();" class="delete_dim" href="javascript:void(0);"></a></td></tr>';
	    x = x.replace(/{{{index}}}/g, $('#NumOfTimeslot').val()); 
	    $(x).insertBefore($('#timeslot_table_last'));
	    $('#NumOfTimeslot').val(parseInt($('#NumOfTimeslot').val())+1);
	});
	
	$('input#submitBtn').click(function(){
		var formObj = $('form#MailForm');
		if(checkEmailTemplateRecord()){
			formObj.submit();
		}
	});  
	
	$('input#cancelBtn').click(function(){
		$.address.value('/apps/admission/settings/interviewarrangement/');
	});
};

$(function(){
	kis.admission.interview_arrangement_init({
		'close':'<?=$kis_lang['close']?>',
		'delete':'<?=$kis_lang['delete']?>',
		'request_input_email_template_title':'<?=$kis_lang['msg']['requestinputemailtemplatetitle']?>',
		'request_input_email_template_content':'<?=$kis_lang['msg']['request_input_email_template_content']?>',
		'confirm_delete_email_template_attachment':'<?=$kis_lang['msg']['confirm_delete_email_template_attachment']?>',
		'returnmsg':'<?=$kis_lang['Admission']['ReturnMessage'][$sysMsg]?>',
		invaliddateformat:'<?=$kis_lang['Admission']['msg']['invaliddateformat']?>',
		enddateearlier: '<?=$kis_lang['Admission']['msg']['enddateearlier']?>',
		emptyapplicationdatewarning: '<?=$kis_lang['Admission']['msg']['emptyapplicationdatewarning']?>',
		selectatleastonetimeslot: '<?=$kis_lang['Admission']['msg']['selectatleastonetimeslot']?>',
		enterpositivenumber: '<?=$kis_lang['Admission']['msg']['enternumber']?>'
	});
});
</script>
<div class="main_content_detail">
<?
if($sys_custom['KIS_Admission']['ICMS']['Settings']){
	kis_ui::loadModuleTab(array('applicationsettings','instructiontoapplicants','attachmentsettings','email_template'), 'interviewarrangement', '#/apps/admission/settings/');
}
else if($sys_custom['KIS_Admission']['STANDARD']['Settings']){
    	kis_ui::loadModuleTab(array('applicationsettings',
            'instructiontoapplicants',
            'attachmentsettings',
            'email_template','printformheader','onlinepayment', 'modulesettings'), 'interviewarrangement', '#/apps/admission/settings/');
    }
else if($sys_custom['KIS_Admission']['InterviewArrangement']){
    kis_ui::loadModuleTab(array('applicationsettings','instructiontoapplicants','attachmentsettings','email_template', 'interviewarrangement'), 'interviewarrangement', '#/apps/admission/settings/');
}
else{
	kis_ui::loadModuleTab(array('applicationsettings','instructiontoapplicants','attachmentsettings','email_template'), 'interviewarrangement', '#/apps/admission/settings/');
}
?>
<p class="spacer"></p>
<form id="MailForm" name="MailForm" method="POST" action="apps/admission/templates/settings/interviewarrangement_edit_update.php" enctype="multipart/form-data">
	<?=$kis_data['NavigationBar']?>
	<p class="spacer"></p><br>
	
	<div class="table_board">
		<table id="FormTable" class="form_table">
        	<tbody>
                <tr> 
                	<td class="field_title" style="width:30%;"><?=$kis_lang['date']?><?=$mustfillinsymbol?></td>
                	<td style="width:70%;">
                		<input type="text" name="Date" id="Date" value="<?=$kis_data['interviewArrangementRecords'][$kis_data['recordID']]['Date']?>">&nbsp;<span class="error_msg" id="warning_Date"></span>
                	</td>
                </tr>
                <tr> 
                	<td class="field_title" style="width:30%;"><?=$kis_lang['timeslot']?><?=$mustfillinsymbol?></td>
                	<td style="width:70%;">
                		<table class="form_table" id="timeslot_table">
                			
                			<?for($i=0; $i<sizeof($kis_data['interviewArrangementRecords'][$kis_data['recordID']]['StartTime']); $i++){?>
	                			<tr>
	                			<td style="width:230px"><?=$libadmission->Get_Time_Selection($linterface,'StartTime_'.$i,$kis_data['interviewArrangementRecords'][$kis_data['recordID']]['StartTime'][$i],'',false)?>&nbsp;<span class="error_msg" id="warning_StartTime_<?=$i?>"></span> ~ 
	                				<?=$libadmission->Get_Time_Selection($linterface,'EndTime_'.$i,$kis_data['interviewArrangementRecords'][$kis_data['recordID']]['EndTime'][$i],'',false)?>&nbsp;<span class="error_msg" id="warning_EndTime_<?=$i?>"></span>
	                				<?=$linterface->Get_Form_Warning_Msg("WarnContent", $kis_lang['msg']['request_input_email_template_content'], $Class='', $display=false)?>
	                			</td>
	                			<td>
	                			<?if($i>0){?>
	                				<div class="table_row_tool"><a title="<?=$kis_lang['Delete']?>" id="DeleteTimeslot" onClick="$(this).parent().parent().parent().remove();" class="delete_dim" href="javascript:void(0);"></a>
	                			<?}?>
	                			</td>
	                			</tr>
                			<?}?>
                			<?if(sizeof($kis_data['interviewArrangementRecords'][$kis_data['recordID']]['StartTime']) <=0){?>
                				<tr>
	                			<td style="width:230px"><?=$libadmission->Get_Time_Selection($linterface,'StartTime_0','','',false)?>&nbsp;<span class="error_msg" id="warning_StartTime_0"></span> ~ 
	                				<?=$libadmission->Get_Time_Selection($linterface,'EndTime_0','','',false)?>&nbsp;<span class="error_msg" id="warning_EndTime_0"></span>
	                				<?=$linterface->Get_Form_Warning_Msg("WarnContent", $kis_lang['msg']['request_input_email_template_content'], $Class='', $display=false)?>
	                			</td>
	                			<td>
	                			</td>
	                			</tr>
                			<?}?>
                		<tr id="timeslot_table_last">
	                		<td colspan="2">
	                			<div class="table_row_tool"><a title="<?=$kis_lang['add']?>" id="AddTimeslot" class="add_dim" href="javascript:void(0);"></a></div>
	                		</td>
                		</tr>
                		</table>
                	</td>
                </tr>
                <tr> 
                	<td class="field_title" style="width:30%;"><?=$kis_lang['numofgroup']?><?=$mustfillinsymbol?></td>
                	<td style="width:70%;">
                		<?=$linterface->GET_TEXTBOX("NumOfGroup", "NumOfGroup", $kis_data['interviewArrangementRecords'][$kis_data['recordID']]['NumOfGroup'], $OtherClass='', $OtherPar=array()).'<br />'?>
                		<?=$linterface->Get_Form_Warning_Msg("WarnTitle", $kis_lang['msg']['requestinputemailtemplatetitle'], $Class='', $display=false)?>
                	</td>
                </tr>
                <tr>
                	<td class="field_title" style="width:30%;"><?=$kis_lang['qouta']?><?=$mustfillinsymbol?></td>
                	<td style="width:70%;">
            			<?=$linterface->GET_TEXTBOX("Quota", "Quota", $kis_data['interviewArrangementRecords'][$kis_data['recordID']]['Quota'], $OtherClass='', $OtherPar=array()).'<br />'?>
            			<?=$linterface->Get_Form_Warning_Msg("WarnTitle", $kis_lang['msg']['requestinputemailtemplatetitle'], $Class='', $display=false)?>
            		</td>
            	</tr>
            </tbody>
        </table>
        <div class="text_remark step1" <?=$goto_step==2?'style="display:none;"':''?>><?=str_replace('*','<span style="color:red">*</span>',$kis_lang['requiredfield'])?></div>
		<div class="edit_bottom">
			<input type="hidden" name="RecordID" id="RecordID" value="<?=$kis_data['recordID']?>">
			<input type="hidden" name="NumOfTimeslot" id="NumOfTimeslot" value="<?=(sizeof($kis_data['interviewArrangementRecords'][$kis_data['recordID']]['StartTime'])?sizeof($kis_data['interviewArrangementRecords'][$kis_data['recordID']]['StartTime']):1)?>">
        	<input type="button" name="submitBtn" id="submitBtn" class="formbutton" value="<?=$kis_lang['submit']?>" />
            <input type="button" name="cancelBtn" id="cancelBtn" class="formsubbutton" value="<?=$kis_lang['cancel']?>" />
	    </div>
        <p class="spacer"></p><br>
	</div>
</form>
</div>