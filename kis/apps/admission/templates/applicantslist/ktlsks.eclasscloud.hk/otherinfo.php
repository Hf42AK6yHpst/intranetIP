<?php

global $libkis_admission;


#### Get Current Study School Info START ####
$applicationInfo['studentApplicationInfoCust'] = $libkis_admission->getApplicationStudentInfoCust($kis_data['schoolYearID'],$classLevelID='',$applicationID='',$kis_data['recordID']);
$currentSchoolInfo = $applicationInfo['studentApplicationInfoCust'][0];
#### Get Current Study School Info END ####


#### Get Student Cust Info START ####
$applicationInfo['studentApplicationInfo'] = current($libkis_admission->getApplicationStudentInfo($kis_data['schoolYearID'],$classLevelID='',$applicationID='',$status='',$kis_data['recordID']));
$applicationInfo['studentCustInfo'] = $libkis_admission->getAllApplicationCustInfo($applicationInfo['applicationID']);

// $talents = array();
// foreach((array)$applicationInfo['studentCustInfo']['Talents'] as $talent){
// 	$talents[] = $talent['Value'];
// }

// $achievements = array();
// foreach((array)$applicationInfo['studentCustInfo']['Achievement'] as $achievement){
// 	$achievements[] = $achievement['Value'];
// }



if ($applicationInfo ['studentApplicationInfo'] ['language'] == 1) {
	$langStr = $kis_lang['Admission']['RMKG']['cantonese'];
} else if ($applicationInfo ['studentApplicationInfo'] ['language'] == 2) {
	$langStr = $kis_lang['Admission']['RMKG']['eng'];
} else if ($applicationInfo ['studentApplicationInfo'] ['language'] == 3) {
	$langStr = $kis_lang['Admission']['RMKG']['pth'];
}
#### Get Student Cust Info END ####


#### Get Relatives Info START ####
$applicationInfo['studentApplicationRelativesInfo'] = $libkis_admission->getApplicationRelativesInfoCust($kis_data['schoolYearID'],$classLevelID='',$applicationID='',$kis_data['recordID']);
#### Get Relatives Info END ####


?>
<table class="form_table">
	<tbody>                              
		<tr> 
			<td width="30%" class="field_title"><?=$kis_lang['form']?></td>
			<td width="70%"><?=kis_ui::displayTableField($classLevelAry[$applicationInfo['classLevelID']])?></td>
		</tr>
		
		<tr> 
			<td width="30%" class="field_title"><?=$kis_lang['Admission']['KTLMSKG']['MotherTongue']?></td>
			<td width="70%"><?=$langStr?></td>
		</tr>
<!-------------- Siblings START -------------->
<tr>
	<td colspan="4">
		<table class="form_table" style="font-size: 13px">
		
		<tr>
			<td rowspan="4" style="width: 300px;">
				<?=$kis_lang['Admission']['KTLMSKG']['siblings']?>
			</td>
			<td>&nbsp;</td>
			<td class="form_guardian_head"><center><?=$kis_lang['Admission']['name']?></center></td>
			<td class="form_guardian_head"><center><?=$kis_lang['Admission']['class']?></center></td>
			<td class="form_guardian_head"><center><?=$kis_lang['Admission']['relationship']?></center></td>
		</tr>
		<?php for($i=1;$i<=3;$i++){ ?>
		<tr>
			<td class="field_title" style="text-align:right;width:50px;">(<?=$i?>)</td>
			<td class="form_guardian_field">
				<center><?=kis_ui::displayTableField($applicationInfo['studentApplicationRelativesInfo'][$i-1]['OthersRelativeStudiedName'])?></center>
			</td>
			
			<td class="form_guardian_field">
				<center><?=kis_ui::displayTableField($applicationInfo['studentApplicationRelativesInfo'][$i-1]['OthersRelativeClassPosition'])?></center>
			</td>
			
			<td class="form_guardian_field">
				<center><?=kis_ui::displayTableField($applicationInfo['studentApplicationRelativesInfo'][$i-1]['OthersRelativeRelationship'])?></center>
			</td>
		</tr>
		<?php } ?>
		
		</table>
	</td>
</tr>
<!-------------- Siblings END -------------->


<!-------------- Referee START -------------->
<tr>
	<td class="field_title">
		<?=$kis_lang['Admission']['KTLMSKG']['introducedName']?>
	</td>
	<td colspan="3">
		<?=kis_ui::displayTableField($applicationInfo['studentCustInfo']['Referee_Name'][0]['Value'])?>
	</td>
</tr>

<tr>
	<td class="field_title">
		<?=$kis_lang['Admission']['KTLMSKG']['introducedationship']?>
	</td>
	<td colspan="3">
		<?php 
		if(count($applicationInfo['studentCustInfo']['Referee_Type']) == 0){
			echo kis_ui::displayTableField('');
		}else{
			foreach((array)$applicationInfo['studentCustInfo']['Referee_Type'] as $type){
				if($type['Value'] == $admission_cfg['RefereeType'][1]){
		?>
				<span><?=$kis_lang['Admission']['KTLMSKG']['RefereeType']['Committee']?></span><br />
			<?php
				}else if($type['Value'] == $admission_cfg['RefereeType'][2]){
			?>
				<span><?=$kis_lang['Admission']['KTLMSKG']['RefereeType']['Staff']?></span><br />
			<?php
				}else if($type['Value'] == $admission_cfg['RefereeType'][3]){
			?>
				<span><?=$applicationInfo['studentCustInfo']['Referee_Type_Other'][0]['Value']?></span><br />
		<?php 
				}
			} 
		}
		?>
	</td>
</tr>
<!-------------- Referee END -------------->

</tbody>
</table>
