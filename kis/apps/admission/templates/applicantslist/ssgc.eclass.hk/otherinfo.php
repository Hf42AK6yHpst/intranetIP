<?php

global $libkis_admission;


#### Get Student Cust Info START ####
$applicationCustInfo = $libkis_admission->getAllApplicationCustInfo($applicationInfo['applicationID']);
#### Get Student Cust Info START ####


?>

<table id="dataTable1" class="form_table" style="font-size: 13px">
<tr>
	<td rowspan="4" class="field_title" style="width: 30%;"><?=$star?><?=$kis_lang['Admission']['SSGC']['sibilingInfo'] ?></td>
	<td class="form_guardian_head"><center><?=$kis_lang['Admission']['name'] ?> </center></td>
	<td class="form_guardian_head"><center><?=$kis_lang['Admission']['Age'] ?></center></td>
	<td class="form_guardian_head"><center><?=$kis_lang['Admission']['gender'] ?></center></td>
	<td class="form_guardian_head"><center><?=$kis_lang['Admission']['SSGC']['schoolClass'] ?></center></td>
</tr>
<?php for($i=0;$i<3;$i++){ ?>
    <tr>
    	<td class="form_guardian_field">
    		<?= kis_ui::displayTableField($applicationCustInfo['SibilingName'][$i]['Value']) ?>
    	</td>
    	<td class="form_guardian_field">
    		<?= kis_ui::displayTableField($applicationCustInfo['SibilingAge'][$i]['Value']) ?>
    	</td>
    	<td class="form_guardian_field">
    		<?= kis_ui::displayTableField($kis_lang['Admission']['genderType'][$applicationCustInfo['SibilingGender'][$i]['Value']]) ?>
    	</td>
    	<td class="form_guardian_field">
    		<?= kis_ui::displayTableField($applicationCustInfo['SibilingSchoolClass'][$i]['Value']) ?>
    	</td>
    </tr>
<?php } ?>
    <tr>
    	<td class="field_title" >
    		<?=$kis_lang['Admission']['HasTwinsApply']?>
    	</td>
    	<td colspan="4">
    		<?php
		        echo ($applicationInfo['IsTwinsApplied'])? $kis_lang['Admission']['yes'] : $kis_lang['Admission']['no'];
    		    if($applicationInfo['IsTwinsApplied']){
    		        echo "<br />{$kis_lang['Admission']['name']}: {$applicationCustInfo['TwinName'][0]['Value']}";
    		    }
		    ?>
    	</td>
    </tr>
</table>


<table id="dataTable1" class="form_table" >
<colgroup>
    <col style="width:30%">
    <col style="width:100px">
    <col style="">
</colgroup>

<tbody>
<?php if($applicationCustInfo['PartBNotApplicable'][0]['Value']): ?>
    <tr>
    	<td class="field_title">
    		<?=$kis_lang['Admission']['SSGC']['partB']?>
    	</td>
    	<td colspan="2">
    		<?=$kis_lang['Admission']['SSGC']['notApplicable'] ?>
    	</td>
    </tr>
<?php else: ?>
    <tr>
    	<td class="field_title" colspan="3" style="text-align: center;">
    		<?=$kis_lang['Admission']['SSGC']['partB']?>
    	</td>
    </tr>
    <tr>
    	<td class="field_title" >
    		<?=$kis_lang['Admission']['SSGC']['parentIsStaff']?>
    	</td>
    	<td class="field_title" style="width: auto;">
			<label>(20 <?=$kis_lang['Admission']['SSGC']['points'] ?>)</label>
    	</td>
    	<td colspan="3">
    		<?php
		        echo ($applicationCustInfo['ParentIsStaff'][0]['Value'])? $kis_lang['Admission']['yes'] : $kis_lang['Admission']['no'];
    		    if($applicationCustInfo['ParentIsStaff'][0]['Value']){
    		        echo "<br />{$kis_lang['Admission']['SSGC']['parentName']}: {$applicationCustInfo['ParentIsStaff_Name'][0]['Value']}";
    		    }
		    ?>
    	</td>
    </tr>
    
    <tr>
    	<td class="field_title" >
    		<?=$kis_lang['Admission']['SSGC']['sisterIsSameSchool']?>
    	</td>
    	<td class="field_title" style="width: auto;">
			<label>(20 <?=$kis_lang['Admission']['SSGC']['points'] ?>)</label>
    	</td>
    	<td>
    		<?php
		        echo ($applicationCustInfo['SisterIsSameSchool'][0]['Value'])? $kis_lang['Admission']['yes'] : $kis_lang['Admission']['no'];
    		    if($applicationCustInfo['SisterIsSameSchool'][0]['Value']){
    		        echo "<br />{$kis_lang['Admission']['SSGC']['classAttending']}: {$applicationCustInfo['SisterIsSameSchool_NameClass'][0]['Value']}";
    		    }
		    ?>
    	</td>
    </tr>
    
    <tr>
    	<td class="field_title" >
    		<?=$kis_lang['Admission']['SSGC']['parentIsManager']?>
    	</td>
    	<td class="field_title" style="width: auto;">
			<label>(20 <?=$kis_lang['Admission']['SSGC']['points'] ?>)</label>
    	</td>
    	<td>
    		<?php
		        echo ($applicationCustInfo['ParentIsManager'][0]['Value'])? $kis_lang['Admission']['yes'] : $kis_lang['Admission']['no'];
    		    if($applicationCustInfo['ParentIsManager'][0]['Value']){
    		        echo "<br />{$kis_lang['Admission']['SSGC']['parentName']}: {$applicationCustInfo['ParentIsManager_Name'][0]['Value']}";
    		    }
		    ?>
    	</td>
    </tr>
    
    <tr>
    	<td class="field_title" >
    		<?=$kis_lang['Admission']['SSGC']['motherSisterPrimaryGraduate']?>
    	</td>
    	<td class="field_title" style="width: auto;">
			<label>(10 <?=$kis_lang['Admission']['SSGC']['points'] ?>)</label>
    	</td>
    	<td>
    		<?php
		        echo ($applicationCustInfo['MotherSisterPrimaryGraduate'][0]['Value'])? $kis_lang['Admission']['yes'] : $kis_lang['Admission']['no'];
    		    if($applicationCustInfo['MotherSisterPrimaryGraduate'][0]['Value']){
    		        echo "<br />{$kis_lang['Admission']['SSGC']['nameGraduation']}: {$applicationCustInfo['MotherSisterPrimaryGraduate_NameYearOfGraduation'][0]['Value']}";
    		    }
		    ?>
    	</td>
    </tr>
    
    <tr>
    	<td class="field_title" >
    		<?=$kis_lang['Admission']['SSGC']['sameReligious']?>
    	</td>
    	<td class="field_title" style="width: auto;">
			<label>(5 <?=$kis_lang['Admission']['SSGC']['points'] ?>)</label>
    	</td>
    	<td>
    		<?php
		        echo ($applicationCustInfo['SameReligious'][0]['Value'])? $kis_lang['Admission']['yes'] : $kis_lang['Admission']['no'];
		    ?>
    	</td>
    </tr>
    
    <tr>
    	<td class="field_title" >
    		<?=$kis_lang['Admission']['SSGC']['firstBorn']?>
    	</td>
    	<td class="field_title" style="width: auto;">
			<label>(5 <?=$kis_lang['Admission']['SSGC']['points'] ?>)</label>
    	</td>
    	<td>
    		<?php
		        echo ($applicationCustInfo['FirstBorn'][0]['Value'])? $kis_lang['Admission']['yes'] : $kis_lang['Admission']['no'];
		    ?>
    	</td>
    </tr>
    
    <tr>
    	<td class="field_title" >
    		<?=$kis_lang['Admission']['SSGC']['motherSecondGraduate']?>
    	</td>
    	<td colspan="2">
    		<?php
		        echo ($applicationCustInfo['MotherSecondGraduate'][0]['Value'])? $kis_lang['Admission']['yes'] : $kis_lang['Admission']['no'];
		        if(($applicationCustInfo['MotherSecondGraduate'][0]['Value'])){
		            echo "<br />{$kis_lang['Admission']['SSGC']['nameGraduation']}: {$applicationCustInfo['MotherSecondGraduate_NameYearOfGraduation'][0]['Value']}";
		        }
		    ?>
    	</td>
    </tr>
    
    <tr>
    	<td class="field_title" >
    		<?=$kis_lang['Admission']['SSGC']['siblingSameSchoolGraduate']?>
    	</td>
    	<td colspan="2">
    		<?php
		        echo ($applicationCustInfo['SiblingSameSchoolGraduate'][0]['Value'])? $kis_lang['Admission']['yes'] : $kis_lang['Admission']['no'];
		        if(($applicationCustInfo['SiblingSameSchoolGraduate'][0]['Value'])){
		            echo "<br />";
		            if($applicationCustInfo['SiblingSameSchoolGraduate_Type'][0]['Value'] == '0'){
		                echo "({$kis_lang['Admission']['SSGC']['currentSchoolStudying']}) {$kis_lang['Admission']['SSGC']['classAttending']}: {$applicationCustInfo['SiblingSameSchoolGraduate_NameClass'][0]['Value']}";
		            }else{
		                echo "({$kis_lang['Admission']['SSGC']['currentSchoolGraduate']}) {$kis_lang['Admission']['SSGC']['nameGraduation']}: {$applicationCustInfo['SiblingSameSchoolGraduate_NameYearOfGraduation'][0]['Value']}";
		            }
		        }
		    ?>
    	</td>
    </tr>
<?php endif; ?>
    
</tbody>

</table>
