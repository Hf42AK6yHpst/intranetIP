<?php

global $libkis_admission;


#### Get Student Cust Info START ####
$allCustInfo = $libkis_admission->getAllApplicationCustInfo($applicationInfo['applicationID']);
#### Get Student Cust Info START ####


?>
<input type="hidden" name="ApplicationID" value="<?=$applicationInfo['applicationID']?>" />

<style>
.otherQuestionRow label.disabled{
     color: #aaaaaa;
}
.partBDetails{
    margin-top: 5px;
}
.partBDetails label{
    margin-right: 5px;
    display: inline-block;
}
.partBDetails input[type="text"]{
     width: 300px;
}
</style>
<table id="dataTable1" class="form_table" style="font-size: 13px">
<tr>
	<td rowspan="4" class="field_title" style="width: 30%;"><?=$kis_lang['Admission']['SSGC']['sibilingInfo'] ?></td>
	<td class="form_guardian_head"><center><?=$kis_lang['Admission']['name'] ?> </center></td>
	<td class="form_guardian_head"><center><?=$kis_lang['Admission']['Age'] ?></center></td>
	<td class="form_guardian_head"><center><?=$kis_lang['Admission']['gender'] ?></center></td>
	<td class="form_guardian_head"><center><?=$kis_lang['Admission']['SSGC']['schoolClass'] ?></center></td>
</tr>
<?php for($i=0;$i<3;$i++){ ?>
    <tr>
    	<td class="form_guardian_field">
    		<input name="SibilingName<?=$i+1 ?>" type="text" id="SibilingName<?=$i+1 ?>" class="textboxtext" value="<?=$allCustInfo['SibilingName'][$i]['Value'] ?>"/>
    	</td>
    	<td class="form_guardian_field">
    		<input name="SibilingAge<?=$i+1 ?>" type="text" id="SibilingAge<?=$i+1 ?>" class="textboxtext" value="<?=$allCustInfo['SibilingAge'][$i]['Value'] ?>"/>
    	</td>
    	<td class="form_guardian_field">
    		<div style="text-align: center;">
    			<input type="radio" value="M" id="SibilingGenderM<?=$i+1 ?>" name="SibilingGender<?=$i+1 ?>" <?=$allCustInfo['SibilingGender'][$i]['Value'] == "M"?'checked':'' ?>>
    			<label for="SibilingGenderM<?=$i+1 ?>"><?=$kis_lang['Admission']['genderType']['M']?></label>
    			<input type="radio" value="F" id="SibilingGenderF<?=$i+1 ?>" name="SibilingGender<?=$i+1 ?>" <?=$allCustInfo['SibilingGender'][$i]['Value'] == "F"?'checked':'' ?>>
    			<label for="SibilingGenderF<?=$i+1 ?>"><?=$kis_lang['Admission']['genderType']['F']?></label>
    		</div>
    	</td>
    	<td class="form_guardian_field">
    		<input name="SibilingSchoolClass<?=$i+1 ?>" type="text" id="SibilingSchoolClass<?=$i+1 ?>" class="textboxtext" value="<?=$allCustInfo['SibilingSchoolClass'][$i]['Value'] ?>"/>
    	</td>
    </tr>
<?php } ?>
    <tr>
    	<td class="field_title" >
    		<?=$kis_lang['Admission']['HasTwinsApply']?>
    	</td>
    	<td colspan="4">
			<input type="radio" value="0" id="HasTwinsApplyN" name="HasTwinsApply" <?=($applicationInfo['IsTwinsApplied'] == 0)?'checked':'' ?> />
			<label for="HasTwinsApplyN"><?=$kis_lang['Admission']['no']?></label>
			<br />
			<input type="radio" value="1" id="HasTwinsApplyY" name="HasTwinsApply" <?=($applicationInfo['IsTwinsApplied'] == 1)?'checked':'' ?> />
			<label for="HasTwinsApplyY"><?=$kis_lang['Admission']['yes']?></label>
			(
				<?=$kis_lang['Admission']['name'] ?>:
				<input name="TwinName" type="text" id="TwinName" class="textboxtext" value="<?=$allCustInfo['TwinName'][0]['Value'] ?>" style="width: 200px;" />
			)
    	</td>
    </tr>
</table>


<table id="dataTable1" class="form_table" >
<colgroup>
    <col style="width:30%">
    <col style="width:100px">
    <col style="">
</colgroup>

<tbody>
    <tr>
    	<td class="field_title">
    		<?=$kis_lang['Admission']['SSGC']['partB']?>
    	</td>
    	<td colspan="2">
			<input type="checkbox" value="1" id="partBNotApplicable" name="PartBNotApplicable" <?=(!isset($allCustInfo['PartBNotApplicable'][0]['Value']) || ($allCustInfo['PartBNotApplicable'][0]['Value'] == 1))?'checked':'' ?> />
			<label for="partBNotApplicable"><?=$kis_lang['Admission']['SSGC']['notApplicable']?></label>
    	</td>
    </tr>
    <tr class="otherQuestionRow">
    	<td class="field_title" >
    		<?=$kis_lang['Admission']['SSGC']['parentIsStaff']?>
    	</td>
    	<td class="field_title" style="width: auto;">
			<label>(20 <?=$kis_lang['Admission']['SSGC']['points'] ?>)</label>
    	</td>
    	<td colspan="3">
    		<div class="partBQuestion">
    			<input type="radio" value="1" id="ParentIsStaffY" name="ParentIsStaff" <?=($allCustInfo['ParentIsStaff'][0]['Value'] == 1)?'checked':'' ?> />
    			<label for="ParentIsStaffY"><?=$kis_lang['Admission']['yes']?></label>&nbsp;&nbsp;
    			<input type="radio" value="0" id="ParentIsStaffN" name="ParentIsStaff" <?=($allCustInfo['ParentIsStaff'][0]['Value'] == 0)?'checked':'' ?> />
    			<label for="ParentIsStaffN"><?=$kis_lang['Admission']['no']?></label>
			</div>
			<div class="partBDetails">
				<label for="ParentIsStaff_Name">
   					<?=$mustfillinsymbol?>
					<?=$kis_lang['Admission']['SSGC']['parentName'] ?>
				</label>
				
    			<input name="ParentIsStaff_Name" type="text" id="ParentIsStaff_Name" value="<?=$allCustInfo['ParentIsStaff_Name'][0]['Value'] ?>" />
			</div>
    	</td>
    </tr>
    
    <tr class="otherQuestionRow">
    	<td class="field_title" >
    		<?=$kis_lang['Admission']['SSGC']['sisterIsSameSchool']?>
    	</td>
    	<td class="field_title" style="width: auto;">
			<label>(20 <?=$kis_lang['Admission']['SSGC']['points'] ?>)</label>
    	</td>
    	<td>
    		<div class="partBQuestion">
    			<input type="radio" value="1" id="SisterIsSameSchoolY" name="SisterIsSameSchool" <?=($allCustInfo['SisterIsSameSchool'][0]['Value'] == 1)?'checked':'' ?> />
    			<label for="SisterIsSameSchoolY"> <?=$kis_lang['Admission']['yes']?></label>&nbsp;&nbsp;
    			<input type="radio" value="0" id="SisterIsSameSchoolN" name="SisterIsSameSchool" <?=($allCustInfo['SisterIsSameSchool'][0]['Value'] == 0)?'checked':'' ?> />
    			<label for="SisterIsSameSchoolN"> <?=$kis_lang['Admission']['no']?></label>
			</div>
			<div class="partBDetails">
				<label for="SisterIsSameSchool_NameClass">
   					<?=$mustfillinsymbol?>
					<?=$kis_lang['Admission']['SSGC']['classAttending'] ?>
				</label>
				
    			<input name="SisterIsSameSchool_NameClass" type="text" id="SisterIsSameSchool_NameClass" value="<?=$allCustInfo['SisterIsSameSchool_NameClass'][0]['Value'] ?>" />
			</div>
    	</td>
    </tr>
    
    <tr class="otherQuestionRow">
    	<td class="field_title" >
    		<?=$kis_lang['Admission']['SSGC']['parentIsManager']?>
    	</td>
    	<td class="field_title" style="width: auto;">
			<label>(20 <?=$kis_lang['Admission']['SSGC']['points'] ?>)</label>
    	</td>
    	<td>
    		<div class="partBQuestion">
    			<input type="radio" value="1" id="ParentIsManagerY" name="ParentIsManager" <?=($allCustInfo['ParentIsManager'][0]['Value'] == 1)?'checked':'' ?> />
    			<label for="ParentIsManagerY"><?=$kis_lang['Admission']['yes']?></label>&nbsp;&nbsp;
    			<input type="radio" value="0" id="ParentIsManagerN" name="ParentIsManager" <?=($allCustInfo['ParentIsManager'][0]['Value'] == 0)?'checked':'' ?> />
    			<label for="ParentIsManagerN"><?=$kis_lang['Admission']['no']?></label>
			</div>
			<div class="partBDetails">
				<label for="ParentIsManager_Name">
   					<?=$mustfillinsymbol?>
					<?=$kis_lang['Admission']['SSGC']['parentName'] ?>
				</label>
				
    			<input name="ParentIsManager_Name" type="text" id="ParentIsManager_Name" value="<?=$allCustInfo['ParentIsManager_Name'][0]['Value'] ?>" />
			</div>
    	</td>
    </tr>
    
    <tr class="otherQuestionRow">
    	<td class="field_title" >
    		<?=$kis_lang['Admission']['SSGC']['motherSisterPrimaryGraduate']?>
    	</td>
    	<td class="field_title" style="width: auto;">
			<label>(10 <?=$kis_lang['Admission']['SSGC']['points'] ?>)</label>
    	</td>
    	<td>
    		<div class="partBQuestion">
    			<input type="radio" value="1" id="MotherSisterPrimaryGraduateY" name="MotherSisterPrimaryGraduate" <?=($allCustInfo['MotherSisterPrimaryGraduate'][0]['Value'] == 1)?'checked':'' ?> />
    			<label for="MotherSisterPrimaryGraduateY"><?=$kis_lang['Admission']['yes']?></label>&nbsp;&nbsp;
    			<input type="radio" value="0" id="MotherSisterPrimaryGraduateN" name="MotherSisterPrimaryGraduate" <?=($allCustInfo['MotherSisterPrimaryGraduate'][0]['Value'] == 0)?'checked':'' ?> />
    			<label for="MotherSisterPrimaryGraduateN"><?=$kis_lang['Admission']['no']?></label>
			</div>
			<div class="partBDetails">
				<label for="MotherSisterPrimaryGraduate_NameYearOfGraduation">
   					<?=$mustfillinsymbol?>
					<?=$kis_lang['Admission']['SSGC']['nameGraduation'] ?>
				</label>
				
    			<input name="MotherSisterPrimaryGraduate_NameYearOfGraduation" type="text" id="MotherSisterPrimaryGraduate_NameYearOfGraduation" value="<?=$allCustInfo['MotherSisterPrimaryGraduate_NameYearOfGraduation'][0]['Value'] ?>" />
			</div>
    	</td>
    </tr>
    
    <tr class="otherQuestionRow">
    	<td class="field_title" >
    		<?=$kis_lang['Admission']['SSGC']['sameReligious']?>
    	</td>
    	<td class="field_title" style="width: auto;">
			<label>(5 <?=$kis_lang['Admission']['SSGC']['points'] ?>)</label>
    	</td>
    	<td>
    		<div class="">
    			<input type="radio" value="1" id="SameReligiousY" name="SameReligious" <?=($allCustInfo['SameReligious'][0]['Value'] == 1)?'checked':'' ?> />
    			<label for="SameReligiousY"><?=$kis_lang['Admission']['yes']?></label>&nbsp;&nbsp;
    			<input type="radio" value="0" id="SameReligiousN" name="SameReligious" <?=($allCustInfo['SameReligious'][0]['Value'] == 0)?'checked':'' ?> />
    			<label for="SameReligiousN"><?=$kis_lang['Admission']['no']?></label>
			</div>
    	</td>
    </tr>
    
    <tr class="otherQuestionRow">
    	<td class="field_title" >
    		<?=$kis_lang['Admission']['SSGC']['firstBorn']?>
    	</td>
    	<td class="field_title" style="width: auto;">
			<label>(5 <?=$kis_lang['Admission']['SSGC']['points'] ?>)</label>
    	</td>
    	<td>
    		<div class="">
    			<input type="radio" value="1" id="FirstBornY" name="FirstBorn" <?=($allCustInfo['FirstBorn'][0]['Value'] == 1)?'checked':'' ?> />
    			<label for="FirstBornY"><?=$kis_lang['Admission']['yes']?></label>&nbsp;&nbsp;
    			<input type="radio" value="0" id="FirstBornN" name="FirstBorn" <?=($allCustInfo['FirstBorn'][0]['Value'] == 0)?'checked':'' ?> />
    			<label for="FirstBornN"><?=$kis_lang['Admission']['no']?></label>
			</div>
    	</td>
    </tr>
    
    <tr class="otherQuestionRow">
    	<td class="field_title" >
    		<?=$kis_lang['Admission']['SSGC']['motherSecondGraduate']?>
    	</td>
    	<td colspan="2">
    		<div class="partBQuestion">
    			<input type="radio" value="1" id="MotherSecondGraduateY" name="MotherSecondGraduate" <?=($allCustInfo['MotherSecondGraduate'][0]['Value'] == 1)?'checked':'' ?> />
    			<label for="MotherSecondGraduateY"><?=$kis_lang['Admission']['yes']?></label>&nbsp;&nbsp;
    			<input type="radio" value="0" id="MotherSecondGraduateN" name="MotherSecondGraduate" <?=($allCustInfo['MotherSecondGraduate'][0]['Value'] == 0)?'checked':'' ?> />
    			<label for="MotherSecondGraduateN"><?=$kis_lang['Admission']['no']?></label>
			</div>
			<div class="partBDetails">
				<label for="MotherSecondGraduate_NameYearOfGraduation">
   					<?=$mustfillinsymbol?>
					<?=$kis_lang['Admission']['SSGC']['nameGraduation'] ?>
				</label>
				
    			<input name="MotherSecondGraduate_NameYearOfGraduation" type="text" id="MotherSecondGraduate_NameYearOfGraduation" value="<?=$allCustInfo['MotherSecondGraduate_NameYearOfGraduation'][0]['Value'] ?>" />
			</div>
    	</td>
    </tr>
    
    <tr class="otherQuestionRow">
    	<td class="field_title" >
    		<?=$kis_lang['Admission']['SSGC']['siblingSameSchoolGraduate']?>
    	</td>
    	<td colspan="2">
    		<div class="partBQuestion">
    			<input type="radio" value="1" id="SiblingSameSchoolGraduateY" name="SiblingSameSchoolGraduate" <?=($allCustInfo['SiblingSameSchoolGraduate'][0]['Value'] == 1)?'checked':'' ?> />
    			<label for="SiblingSameSchoolGraduateY"><?=$kis_lang['Admission']['yes']?></label>&nbsp;&nbsp;
    			<input type="radio" value="0" id="SiblingSameSchoolGraduateN" name="SiblingSameSchoolGraduate" <?=($allCustInfo['SiblingSameSchoolGraduate'][0]['Value'] == 0)?'checked':'' ?> />
    			<label for="SiblingSameSchoolGraduateN"><?=$kis_lang['Admission']['no']?></label>
			</div>
			<div class="partBDetails">
    			<div style="margin-top: 5px;">
    				<div style="display:inline-block">
            			<input type="radio" value="0" id="SiblingSameSchoolGraduate_TypeStuding" name="SiblingSameSchoolGraduate_Type" <?=(!$allCustInfo['SiblingSameSchoolGraduate_NameYearOfGraduation'][0]['Value'])?'checked':'' ?> />
            			<label for="SiblingSameSchoolGraduate_TypeStuding"><?=$kis_lang['Admission']['SSGC']['currentSchoolStudying'] ?></label>
        			</div>
        			
        			<div id="SiblingSameSchoolGraduate_NameClassDiv" style="display:inline-block">
        				<label for="SiblingSameSchoolGraduate_NameClass" style="">
   							<?=$mustfillinsymbol?>
        					<?=$kis_lang['Admission']['SSGC']['classAttending'] ?>
        				</label>
            			<input name="SiblingSameSchoolGraduate_NameClass" type="text" id="SiblingSameSchoolGraduate_NameClass" value="<?=$allCustInfo['SiblingSameSchoolGraduate_NameClass'][0]['Value'] ?>" />
            		</div>
				</div>
				<div style="margin-top: 5px;">
					<div style="display:inline-block">
            			<input type="radio" value="1" id="SiblingSameSchoolGraduate_TypeGraduate" name="SiblingSameSchoolGraduate_Type" <?=($allCustInfo['SiblingSameSchoolGraduate_NameYearOfGraduation'][0]['Value'])?'checked':'' ?> />
            			<label for="SiblingSameSchoolGraduate_TypeGraduate"><?=$kis_lang['Admission']['SSGC']['currentSchoolGraduate'] ?></label>
        			</div>
        			<div id="SiblingSameSchoolGraduate_NameYearOfGraduationDiv" style="display:inline-block">
        				<label for="SiblingSameSchoolGraduate_NameYearOfGraduation" style="">
   							<?=$mustfillinsymbol?>
        					<?=$kis_lang['Admission']['SSGC']['nameGraduation'] ?>
        				</label>
            			<input name="SiblingSameSchoolGraduate_NameYearOfGraduation" type="text" id="SiblingSameSchoolGraduate_NameYearOfGraduation" value="<?=$allCustInfo['SiblingSameSchoolGraduate_NameYearOfGraduation'][0]['Value'] ?>" />
            		</div>
        		</div>
			</div>
    	</td>
    </tr>
    
</tbody>

</table>

<script>
//// UI START ////
function updateUI(){
	var isNotApplicable = !!$('#partBNotApplicable:checked').length;
	$('.otherQuestionRow input, .otherQuestionRow select').prop('disabled', isNotApplicable);
	if(isNotApplicable){
		$('.otherQuestionRow label').addClass('disabled');
	}else{
		$('.otherQuestionRow label').removeClass('disabled');
	}

	$('.partBQuestion input:checked').each(function(index, element){
		var value = $(this).val();
		var $td = $(this).closest('td');
		var $detailsTd = $td.find('.partBDetails');
		if(value == '1'){
			$detailsTd.show();
			$detailsTd.find('input,select').prop('disabled', isNotApplicable);
		}else{
			$detailsTd.hide();
			$detailsTd.find('input,select').prop('disabled', true);
		}
	});

	if($('[name="SiblingSameSchoolGraduate_Type"]:checked').val() == '1'){
		$('#SiblingSameSchoolGraduate_NameClassDiv').hide().find('input,select').prop('disabled', true);
		$('#SiblingSameSchoolGraduate_NameYearOfGraduationDiv').show().find('input,select').prop('disabled', isNotApplicable);
	}else{
		$('#SiblingSameSchoolGraduate_NameClassDiv').show().find('input,select').prop('disabled', isNotApplicable);
		$('#SiblingSameSchoolGraduate_NameYearOfGraduationDiv').hide().find('input,select').prop('disabled', true);
	}
}
$('#partBNotApplicable, .partBQuestion input').click(updateUI);
$('[name="SiblingSameSchoolGraduate_Type"]').change(updateUI);
updateUI();
//// UI END ////

$('#applicant_form').unbind('submit').submit(function(e){
	e.preventDefault();
	
	var schoolYearId = $('#schoolYearId').val();
	var recordID = $('#recordID').val();
	var display = $('#display').val();
	var timeSlot = lang.timeslot.split(',');
	if(checkValidForm()){
		if($('#HasTwinsApplyN').prop('checked')){
			$('#TwinName').val('');
		}
		
		$.post('apps/admission/ajax.php?action=updateApplicationInfo', $(this).serialize(), function(success){
			$.address.value('/apps/admission/applicantslist/details/'+schoolYearId+'/'+recordID+'/'+display+'&sysMsg='+success);
		});
	}
	return false;
});

function checkIsChineseCharacter(str){
	return str.match(/^[\u3400-\u9FBF]*$/);
}

function checkIsEnglishCharacter(str){
	return str.match(/^[A-Za-z ,\-]*$/);
}

function checkNaNull(str){
	return (
    	($.trim(str).toLowerCase()=='沒有') || 
    	($.trim(str).toLowerCase()=='nil') ||
    	($.trim(str).toLowerCase()=='n.a.')
	);
}

function checkValidForm(){
	var re = /\S+@\S+\.\S+/;
	var form1 = applicant_form;

	/******** Other Info START ********/
	for(var i=1;i<=3;i++){
		/**** Sibilings Name START ****/
		if(
			$.trim($('#SibilingName'+i).val()) == '' &&
			$.trim($('#SibilingAge'+i).val() + $('#SibilingSchoolClass'+i).val()) != ''
		){
			alert(" <?=$kis_lang['Admission']['SHCK']['msg']['enterName']?>\n Please enter Name.");
			$('#SibilingName'+i)[0].focus();
			return false;
		}
		/**** Sibilings Name START ****/

		/**** Sibilings Age START ****/
		if($.trim($('#SibilingName'+i).val())!='' && $.trim($('#SibilingAge'+i).val())==''){
			alert(" <?=$kis_lang['Admission']['msg']['enterage']?>\n Please enter Age.");
			$('#SibilingAge'+i)[0].focus();
			return false;
		}
		if(
			!checkNaNull($('#SibilingAge'+i).val()) &&
			!/^[0-9]*$/.test($('#SibilingAge'+i).val())
		){
			alert(" <?=$kis_lang['Admission']['msg']['enternumber']?>\n Please enter number.");
			$('#SibilingAge'+i)[0].focus();
			return false;
		}
		/**** Sibilings Age START ****/

		/**** Sibilings Gender START ****/
		if($.trim($('#SibilingName'+i).val())!='' && $.trim($('input:radio[name=SibilingGender'+i+']:checked').val())==''){
			alert(" <?=$kis_lang['Admission']['msg']['selectgender']?>\n Please select Gender.");	
			$('#SibilingGenderM'+i)[0].focus();
			return false;
		}
		/**** Sibilings Gender END ****/
		
		/**** Sibilings School/Class START ****/
		if($.trim($('#SibilingName'+i).val())!='' && $.trim($('#SibilingSchoolClass'+i).val())==''){
			alert(" <?=$kis_lang['Admission']['SSGC']['msg']['enterSchoolClass']?>\n Please enter School & class.");
			$('#SibilingSchoolClass'+i)[0].focus();
			return false;
		}
		/**** Sibilings School/Class START ****/

		if($.trim($('#SibilingName'+i).val()) == ''){
			$('input:radio[name=SibilingGender'+i+']:checked').prop('checked', false);
		}
	}
	/******** Other Info END ********/
	return true;
}
</script>
