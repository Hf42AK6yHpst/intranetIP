<?php

global $libkis_admission;

######## Get Parent Cust Info START ########
$parentType = (isset($applicationInfo['F']))? 'F' : 'G';
//$applicationCustInfo = $libkis_admission->getAllApplicationCustInfo($applicationInfo['applicationID']);
######## Get Parent Cust Info END ########
?>
<table class="form_table">
	<colgroup>
        <col style="width:30%">
        <col style="width:35%">
        <col style="width:35%">
    </colgroup>
	<tr>
		<td>&nbsp;</td>
		<td class="form_guardian_head"><center><?=($parentType == 'F')? $kis_lang['Admission']['HKUGAPS']['father'] : $kis_lang['Admission']['SSGC']['guardian'] ?></center></td>
		<td class="form_guardian_head"><center><?=$kis_lang['Admission']['HKUGAPS']['mother'] ?></center></td>
	</tr>
	<tr>
		<td class="field_title">
    		<?=$kis_lang['Admission']['chinesename'] ?>
		</td>
		<td class="form_guardian_field">
			<?= kis_ui::displayTableField($applicationInfo[$parentType]['ChineseName']) ?>
    	</td>
		<td class="form_guardian_field">
			<?= kis_ui::displayTableField($applicationInfo['M']['ChineseName']) ?>
		</td>
	</tr>
	<tr>
		<td class="field_title">
    		<?=$kis_lang['Admission']['englishname'] ?>
		</td>
		<td class="form_guardian_field">
			<?= kis_ui::displayTableField($applicationInfo[$parentType]['EnglishName']) ?>	
		</td>
		<td class="form_guardian_field">
			<?= kis_ui::displayTableField($applicationInfo['M']['EnglishName']) ?>
		</td>
	</tr>
	
	<tr>
		<td class="field_title">
    		<?=$kis_lang['Admission']['occupation']?>
		</td>
		<td class="form_guardian_field">
			<?= kis_ui::displayTableField($applicationInfo[$parentType]['JobTitle']) ?>
		</td>
		<td class="form_guardian_field">
    		<?= kis_ui::displayTableField($applicationInfo['M']['JobTitle']) ?>
    	</td>
	</tr>
	
	<tr>
		<td class="field_title">
    		<?=$kis_lang['Admission']['SSGC']['organization']?>
		</td>
		<td class="form_guardian_field">
			<?= kis_ui::displayTableField($applicationInfo[$parentType]['Company']) ?>
		</td>
		<td class="form_guardian_field">
    		<?= kis_ui::displayTableField($applicationInfo['M']['Company']) ?>
    	</td>
	</tr>
	
	<tr>
		<td class="field_title">
    		<?=$kis_lang['Admission']['SHCK']['ContactNo']?>
		</td>
		<td class="form_guardian_field">
			<?= kis_ui::displayTableField(nl2br($applicationInfo[$parentType]['Mobile'])) ?>
		</td>
		<td class="form_guardian_field">
    		<?= kis_ui::displayTableField(nl2br($applicationInfo['M']['Mobile'])) ?>
    	</td>
	</tr>
	
	<tr>
		<td class="field_title">
    		<?=$kis_lang['Admission']['email']?>
		</td>
		<td class="form_guardian_field">
			<?= kis_ui::displayTableField($applicationInfo[$parentType]['Email']) ?>
		</td>
		<td class="form_guardian_field">
    		<?= kis_ui::displayTableField($applicationInfo['M']['Email']) ?>
    	</td>
	</tr>
	
</table>