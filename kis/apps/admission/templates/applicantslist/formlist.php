<?php
//modifying by 

/**
 *  2020-07-07 (Tommy): added mingwai and pe-mingwai cust print address
 *  2020-06-10 (Tommy): added SFAEPS cust import
 *  2019-10-18 (Tommy): added STCC cust field
 *  2017-09-13 (Pun):	added SSGC cust field
 *  2016-04-13 (Henry):	added custSelection box
 *  2016-01-25 (Omas) :	added paymentStatus Selection box
 * 	2015-10-12 (Henry):	added checking for $_SESSION["SSV_USER_ACCESS"]["eAdmin-eAdmission-interview-marker"]
 *  2015-09-23 (Henry): add resend notification email (developing)
 *  2014-04-02 (Henry): add import interview information btn
 */

global $setting_path_ip_rel, $PATH_WRT_ROOT;
if(file_exists("{$PATH_WRT_ROOT}/includes/admission/{$setting_path_ip_rel}/template/applicantsList/formlist.php")) {
    include("{$PATH_WRT_ROOT}/includes/admission/{$setting_path_ip_rel}/template/applicantsList/formlist.php");
    return;
}

global $kis_admission_school;

list($page,$amount,$total,$sortby,$order,$keyword) = $kis_data['PageBar'];

# Application form data
$exportFormData = "<a href=\"javascript:void(0);\" onclick=\"js_Clicked_Option_Layer('export_option', 'btn_export');\" class=\"sub_btn tool_export\">".$kis_lang['applicationformdata']."&nbsp;</a>";

# Application status
$exportStatus = "<a href=\"javascript:void(0);\" onclick=\"js_Clicked_Option_Layer('export_option', 'btn_export');\" class=\"sub_btn tool_export_status\">".$kis_lang['Admission']['applicationstatus']."&nbsp;</a>";

# Interview time
$exportInterviewTime = "<a href=\"javascript:void(0);\" onclick=\"js_Clicked_Option_Layer('export_option', 'btn_export');\" class=\"sub_btn tool_export_interview_time\">".$kis_lang['interviewtimeslot']."&nbsp;</a>";

# Application form data for EDB
$exportFormDataEDB = "<a href=\"javascript:void(0);\" onclick=\"js_Clicked_Option_Layer('export_option', 'btn_export');\" class=\"sub_btn tool_export_EDB\">".$kis_lang['applicationformdataEDB']."&nbsp;</a>";

# Export format for import student account
$exportStudentAccount = "<a href=\"javascript:void(0);\" onclick=\"js_Clicked_Option_Layer('export_option', 'btn_export');\" class=\"sub_btn tool_export_stu_acc\">".$kis_lang['exportforimportstuacc']."&nbsp;</a>";

# Export format for import parent account
$exportParentAccount = "<a href=\"javascript:void(0);\" onclick=\"js_Clicked_Option_Layer('export_option', 'btn_export');\" class=\"sub_btn tool_export_prt_acc\">".$kis_lang['exportforimportprtacc']."&nbsp;</a>";

# Export format for payment record
//if($sys_custom['KIS_Admission']['PayPal']){
//	$exportPaymentRecord = "<a href=\"javascript:void(0);\" onclick=\"js_Clicked_Option_Layer('export_option', 'btn_export');\" class=\"sub_btn tool_export_payment_record\">".$kis_lang['paymentrecord']."&nbsp;</a>";
//}

if($sys_custom['KIS_Admission']['SSGC']['Settings'] || $sys_custom['KIS_Admission']['MINGWAI']['Settings'] || $sys_custom['KIS_Admission']['MINGWAIPE']['Settings']){
    $exportCustRecord = "<a href=\"javascript:void(0);\" onclick=\"js_Clicked_Option_Layer('export_option', 'btn_export_cust');\" class=\"sub_btn tool_export_cust\">".$kis_lang['selectedapplicationformdata']."&nbsp;</a>";
}

# export
$menuBar = '
	<div class="btn_option"  id="ExportDiv">

		<a onclick="js_Clicked_Option_Layer(\'export_option\', \'btn_export\');" id="btn_export" class="export option_layer" href="javascript:void(0);"> '.$kis_lang['export'].'&nbsp;</a>
		<br style="clear: both;">
		<div onclick="js_Clicked_Option_Layer_Button(\'export_option\', \'btn_export\');" id="export_option" class="btn_option_layer" style="visibility: hidden;">
		'.$exportFormData.'
		'.($sys_custom['KIS_Admission']['UCCKE']['Settings'] || $sys_custom['KIS_Admission']['STCC']['Settings']?$exportFormDataEDB:'').'
		'.($sys_custom['KIS_Admission']['HKUGAC']['Settings'] || $sys_custom['KIS_Admission']['HKUGAPS']['Settings']?$exportStatus:'').'
		'.($sys_custom['KIS_Admission']['HKUGAC']['Settings']?$exportInterviewTime:'').'
		'.$exportStudentAccount.'
		'.$exportParentAccount.'
		'.$exportPaymentRecord.'
		'.$exportCustRecord.'
		</div>
	&nbsp;
	</div>';

	if($sys_custom['KIS_Admission']['MGF']['Settings'])
	$menuBar .= '<div class="btn_option"  id="ExportDiv">
		<a id="btn_import" class="tool_import_admission_form import" href="javascript:void(0);"> '.$kis_lang['importupdateadmissionform'].'</a>
	</div>
';

	if($sys_custom['KIS_Admission']['HKUGAPS']['Settings'])
	$menuBar .= '<div class="btn_option"  id="ExportDiv">
		<a id="btn_import" class="tool_import_admission_form import" href="javascript:void(0);"> '.$kis_lang['importandupdateadmissionform'].'</a>
	</div>
';
	if($sys_custom['KIS_Admission']['HKUGAC']['Settings'])
	$menuBar .= '<div class="btn_option"  id="ExportDiv">
		<a id="btn_import" class="tool_import_admission_form import" href="javascript:void(0);"> '.$kis_lang['Admission']['HKUGAC']['ImportStatus'].'</a>
	</div>
';
	if($sys_custom['KIS_Admission']['HKUGAPS']['Settings'] || $sys_custom['KIS_Admission']['SFAEPS']['Settings'])
	$menuBar .= '<div class="btn_option"  id="ExportDiv">
		<a id="btn_import" class="tool_import_admission_status import" href="javascript:void(0);"> '.$kis_lang['Admission']['HKUGAC']['ImportStatus'].'</a>
	</div>
';
	if($sys_custom['KIS_Admission']['UCCKE']['Settings'] || $sys_custom['KIS_Admission']['STCC']['Settings'])
	$menuBar .= '<div class="btn_option"  id="ExportDiv">
		<a id="btn_import" class="tool_import_admission_form import" href="javascript:void(0);"> '.$kis_lang['importapplicationformdataEDB'].'</a>
	</div>
';
	if($sys_custom['KIS_Admission']['TBCPK']['Settings'] || $sys_custom['KIS_Admission']['MUNSANG']['Settings'] || $sys_custom['KIS_Admission']['ICMS']['Settings'] || $sys_custom['KIS_Admission']['TSUENWANBCKG']['Settings'] || $sys_custom['KIS_Admission']['MINGWAI']['Settings'] || $sys_custom['KIS_Admission']['MINGWAIPE']['Settings'] || $sys_custom['KIS_Admission']['UCCKE']['Settings'] || $sys_custom['KIS_Admission']['STCC']['Settings'])
	$menuBar .= '<div class="btn_option"  id="ExportDiv">
		<a id="btn_import" class="tool_import_interview import" href="javascript:void(0);"> '.$kis_lang['importinterviewinfo'].'</a>
	</div>
';
	if($sys_custom['KIS_Admission']['MINGWAI']['Settings'] || $sys_custom['KIS_Admission']['MINGWAIPE']['Settings'])
		$menuBar .= '<div class="btn_option"  id="ExportDiv">
		<a id="btn_import" class="tool_import_briefing import" href="javascript:void(0);">'.$kis_lang['importibriefing'].'</a>
	</div>
';
	$menuBar .= '<div class="btn_option"  id="ExportDiv">
		<a id="btn_import" class="tool_download_application_attachment export" href="javascript:void(0);"> '.$kis_lang['downloadapplicationattachment'].'</a>
	</div>
';
	if($sys_custom['KIS_Admission']['UCCKE']['Settings'] || $sys_custom['KIS_Admission']['STCC']['Settings']){
	$menuBar .= '<div class="btn_option" >
					<a style="background-position: 0px -50px;" href="/kis/apps/admission/templates/applicantslist/attachement/index.php?schoolYearID='.$schoolYearID.'&classLevelId='.$classLevelId.'&status='.$status.'" target="_blank" >'.$kis_lang['attachementcheck'].'</a>
				</div>';
    }
    

?>
<style type="text/css">/*
.content_top_tool{
	clear:both;
	display: block;
}
.Conntent_tool{ float:left; padding:2px; display:block;	padding-bottom:5px;}*/
.Content_tool a.parent_btn {background-color:#e9eef2; border:1px solid #CCCCCC; border-bottom:none;z-index:999999;
position: static;
width: auto;}/*
.Conntent_tool .btn_option { display:block; float:left}
.Conntent_tool a, .Conntent_tool i{ padding-left:20px; display:block; float:left; height:20px; line-height:20px; 	color: #9966CC;	background:url(../../../images/2009a/content_icon.gif) 0px 0px no-repeat;  padding-right:3px; margin-right:3px}
.Conntent_tool a:hover{	color: #FF0000;}
.Conntent_tool i { color:#CCCCCC; background-image:url(../../../images/2009a/content_icon_dim.gif)}
.Conntent_tool a.new, .Conntent_tool i.new{ background-position:0px 0px;}
.Conntent_tool a.import, .Conntent_tool i.import{ background-position:0px -20px;}
.Conntent_tool a.export, .Conntent_tool i.export{ background-position:0px -40px;}
.Conntent_tool a.print, .Conntent_tool i.print{	background-position:0px -60px;}
.Conntent_tool a.email, .Conntent_tool i.email{	background-position:0px -160px;}
.Conntent_tool a.setting_content, .Conntent_tool i.setting_content{ background-position:0px -100px;}
.Conntent_tool a.copy, .Conntent_tool i.copy{ background-position:0px -220px;}
.Conntent_tool a.edit_content, .Conntent_tool i.edit_content{ background-position:0px -240px;}
.Conntent_tool a.upload, .Conntent_tool i.upload{			 background-position:0px -280px;}
.Conntent_tool a.new_folder, .Conntent_tool i.new_folder{			 background-position:0px -300px;}
.Conntent_tool a.view_stat, .Conntent_tool i.view_stat{			 background-position:0px -320px;}
.Conntent_tool a.generate, .Conntent_tool i.generate {			 background-position:0px -260px;}
.Conntent_tool a.blank_content, .Conntent_tool i.blank_content{			 background-position:0px -1000px; padding-left:5px;}
.Conntent_tool a.clear_record, .Conntent_tool i.clear_record{			 background-position:0px -340px;}
.Conntent_tool a.refresh_status, .Conntent_tool i.refresh_status{			 background-position:0px -360px;}
*/
/**btn sub option**/
.Conntent_tool .btn_option_layer{ margin-top:-1px; padding:0px; display:block; position:absolute;visibility:hidden;line-height:18px; z-index:99; float:left;background-color:#e9eef2; border:1px solid #CCCCCC}
.Conntent_tool .btn_option_layer a.sub_btn { clear:both; 	background: url(../../../images/2009a/btn_sub.gif) no-repeat 0px 0px;}
/****/
</style>
<script type="text/javascript">
    window.currentSchool = '<?=preg_replace('/\..*/', '', $kis_admission_school)?>';
kis.admission.application_form_init({
	selectatleastonerecord:'<?=$kis_lang['Admission']['msg']['selectatleastonerecord']?>',
	exportallrecordsornot:'<?=$kis_lang['Admission']['msg']['exportallrecordsornot']?>',
	printallrecordsornot:'<?=$kis_lang['Admission']['msg']['printallrecordsornot']?>',
	sendemailallrecordsornot:'<?=$kis_lang['Admission']['msg']['sendemailallrecordsornot']?>',
	importinterviewallrecordsornot:'<?=$kis_lang['Admission']['msg']['importinterviewallrecordsornot']?>',
	confirmresendnotificationemail:'<?=$kis_lang['msg']['confirmresendnotificationemail']?>',
	successresendnotificationemail:'<?=$kis_lang['msg']['successresendnotificationemail']?>',
	unsuccessresendnotificationemail:'<?=$kis_lang['msg']['unsuccessresendnotificationemail']?>'
});
function js_Clicked_Option_Layer(jsOptionLayerDivID, jsOptionLayerBtnID)
{
	js_Hide_All_Option_Layer(jsOptionLayerDivID);
	js_ShowHide_Layer(jsOptionLayerDivID);

	if ($('div#' + jsOptionLayerDivID).css('visibility') == 'visible')
		$('a#' + jsOptionLayerBtnID).addClass('parent_btn');
	else
		$('a#' + jsOptionLayerBtnID).removeClass('parent_btn');
}

function js_Clicked_Option_Layer_Button(jsOptionLayerDivID, jsOptionLayerBtnID)
{
	MM_showHideLayers(jsOptionLayerDivID, '', 'hide');
	$('a#' + jsOptionLayerBtnID).removeClass('parent_btn');
}
</script>
<div class="main_content">
	<?=$kis_data['NavigationBar']?>
	<p class="spacer"></p>
 	<? if(!empty($warning_msg)):?>
 		<?=$warning_msg?>
	<? endif; ?>
	<p class="spacer"></p>

	<!--<div class="content_top_tool">
		<div class="Conntent_tool"><?=$menuBar?></div>-->
		<form class="filter_form">
			<div class="search">
				<input placeholder="<?=$kis_lang['search'] ?>" name="keyword" value="<?=$keyword?>" type="text">
			</div>
		</form>
		<!--<br style="clear:both;">
	</div>-->
	    <div class="Content_tool"><?=$menuBar?></div>
  		<p class="spacer"></p>
		<div class="table_board">
				<div id="table_filter">
					<form class="filter_form">
					<?=$classLevelSelection?>
					<?=$applicationStatus?><?=$interviewStatusSelection?><?=$paymentStatus?> <?=$custSelection?>
					<input type="hidden" name="selectSchoolYearID" id="selectSchoolYearID" value="<?=$schoolYearID?>">
					</form>
				</div>
			<?if(!$_SESSION["SSV_USER_ACCESS"]["eAdmin-eAdmission"] && $_SESSION["SSV_USER_ACCESS"]["eAdmin-eAdmission-interview-marker"]){?>
    			<div class="common_table_tool common_table_tool_table">
    				<a href="#" class="tool_print tool_print_form">
    					<?=$kis_lang['print']?>
    				</a>
    				<?if($sys_custom['KIS_Admission']['MGF']['Settings']){?>
    					<a href="#" class="tool_print tool_print_reply_note"><?=$kis_lang['printreplynote']?></a>
    				<?} else if($sys_custom['KIS_Admission']['MINGWAI']['Settings'] || $sys_custom['KIS_Admission']['MINGWAIPE']['Settings']){?>
    					<a href="#" class="tool_print tool_print_interview_form"><?=$kis_lang['Admission']['MINGWAI']['printinterviewform']?></a>
    				<?}?>
                </div>
			<?}else{?>
    			<div class="common_table_tool common_table_tool_table">
    				<a href="#" class="tool_email tool_resend_email">
    					<?=$kis_lang['sendnotificationemail']?>
    				</a>
    				<a href="#" class="tool_email tool_send_email">
    					<?=$kis_lang['sendemail']?>
    				</a>
    				<?php if($sys_custom['KIS_Admission']['ApplicantChangeClassLevel']): ?>
        				<a href="#" class="tool_set_class">
        					<?=$kis_lang['changeclasslevel']?>
        				</a>
    				<?php endif; ?>
    				<a href="#" class="tool_set">
    					<?=$kis_lang['changestatus']?>
    				</a>
    				<?if(!$sys_custom['KIS_Admission']['EditUploadDocumentMode']){?>
    				<a href="#" class="tool_print tool_print_form">
    					<?=$kis_lang['print']?>
    				</a>
					<?}?>
                	<?if($sys_custom['KIS_Admission']['MGF']['Settings']){?>
    					<a href="#" class="tool_print tool_print_reply_note"><?=$kis_lang['printreplynote']?></a>
    				<?} else if($sys_custom['KIS_Admission']['MINGWAI']['Settings'] || $sys_custom['KIS_Admission']['MINGWAIPE']['Settings']){?>
    					<a href="#" class="tool_print tool_print_interview_form"><?=$kis_lang['Admission']['MINGWAI']['printinterviewform']?></a>
       					<a href="#" class="tool_print tool_export_label export" ><?= $kis_lang['Admission']['MINGWAI']['printAddressLabel'] ?></a> 				
    				<?}?>
                </div>
            <?}?>
            <p class="spacer"></p>
            <form id="application_form" name="application_form" method="post">
	  	    <table class="common_table_list edit_table_list">
			<colgroup><col nowrap="nowrap">
			</colgroup><thead>
				<tr>
				  <th width="20">#</th>
				  <th><?kis_ui::loadSortButton('application_id','applicationno', $sortby, $order)?></th>
				  <th><?kis_ui::loadSortButton('student_name','studentname', $sortby, $order)?></th>

				  <?php if($sys_custom['KIS_Admission']['UCCKE']['Settings'] ){ ?>
				  	<th><?=$kis_lang['Admission']['UCCKE']['HKID']?></th>
				  	<th>STRN</th>
				  <?php } ?>
				  
				  <?php if($sys_custom['KIS_Admission']['STCC']['Settings'] ){ ?>
				  	<th><?=$kis_lang['Admission']['UCCKE']['HKID']?> / <?=$kis_lang['Admission']['STCC']['Passport']?></th>
				  	<th>STRN</th>
				  <?php } ?>

				  <?php if($sys_custom['KIS_Admission']['SSGC']['Settings']){ ?>
				  	<th><?=$kis_lang['Admission']['SSGC']['partB']?></th>
				  <?php } ?>

				  <th><?=$kis_lang['parentorguardian']?></th>
				  <th><?=$kis_lang['phoneno']?></th>
				  <?=$sys_custom['KIS_Admission']['HKUGAPS']['Settings']?'<th>':''?>
				  <?if($sys_custom['KIS_Admission']['HKUGAPS']['Settings']){
				  	kis_ui::loadSortButton('langspokenathome','spokenLanguageForInterview', $sortby, $order);
				  }?>
				  <?=$sys_custom['KIS_Admission']['HKUGAPS']['Settings']?'</th>':''?>
				  <?=$sys_custom['KIS_Admission']['ICMS']['Settings']?'<th>'.$kis_lang['Admission']['icms']['applyDayType'].'</th>':''?>
				  <th><?kis_ui::loadSortButton('application_status','status', $sortby, $order)?></th>
				  <th><input type="checkbox" name="checkmaster" onclick="(this.checked)?setChecked(1,this.form,'applicationAry[]'):setChecked(0,this.form,'applicationAry[]')"></th>
			  </tr>
			</thead>
			<tbody>
				<?php
					if($total>0){
						$idx=$amount*($page-1);
						foreach($applicationDetails as $_applicationId => $_applicationDetailsAry){
							$idx++;
							$_recordId = $_applicationDetailsAry['record_id'];
							$_status = $_applicationDetailsAry['application_status'];
							$_studentName = $_applicationDetailsAry['student_name'];
							$_student_hkid = $_applicationDetailsAry['student_hkid'];
							$_student_strn = $_applicationDetailsAry['student_strn'];
							$_parentName = implode('<br/>',array_filter($_applicationDetailsAry['parent_name']));
							$_parentPhone = implode('<br/>',array_filter($_applicationDetailsAry['parent_phone']));
							if($sys_custom['KIS_Admission']['HKUGAPS']['Settings']){
								$langSpoken = '';
								if($_applicationDetailsAry['langspokenathome'] == 'Cantonese'){
									$langSpoken = $kis_lang['Admission']['Languages']['Cantonese'];
								}else if($_applicationDetailsAry['langspokenathome'] == 'Putonghua'){
									$langSpoken = $kis_lang['Admission']['Languages']['Putonghua'];
								}else if($_applicationDetailsAry['langspokenathome'] == 'English'){
									$langSpoken = $kis_lang['Admission']['Languages']['English'];
								}
								$_spokenLanguageForInterview = '<td>'.($langSpoken?$langSpoken:'--').'</td>';
							}
							if($sys_custom['KIS_Admission']['ICMS']['Settings']){
								$classlevel = $applicationlistAry[$kis_data['classLevelID']]['ClassLevelName'];
								$_applyDayType = '<td>';
								for($i=1;$i<=3;$i++){
									if($_applicationDetailsAry['ApplyDayType'.$i])
										$_applyDayType .= '('.$i.') '.$kis_lang['Admission']['icms'][$classlevel]['TimeSlot'][$_applicationDetailsAry['ApplyDayType'.$i]].'<br/>';
								}
								$_applyDayType .= '</td>';
							}
							$tr_css = '';
							switch($_status){
								case 'TSUENWANBCKG_waitingforinterview':$tr_css = ' class="waiting"';break;
								case 'TSUENWANBCKG_amconfirmed':$tr_css = ' class="done"';break;
								case 'TSUENWANBCKG_pmconfirmed':$tr_css = ' class="done"';break;
								case 'TSUENWANBCKG_reserve':$tr_css = ' class="draft"';break;
								case 'TSUENWANBCKG_notadmitted':$tr_css = ' class="draft"';break;
								case 'TSUENWANBCKG_absent':$tr_css = ' class="absent"';break;

								case $sys_custom['KIS_Admission']['MGF']['Settings'] && 'waitingforinterview':$tr_css = ' class="absent"';break;
								case $sys_custom['KIS_Admission']['MGF']['Settings'] && 'interviewed':$tr_css = ' class="waiting"';break;
								case $sys_custom['KIS_Admission']['MGF']['Settings'] && 'admitted':$tr_css = ' class="done"';break;
								case $sys_custom['KIS_Admission']['MGF']['Settings'] && 'notadmitted':$tr_css = ' class="draft"';break;

								case 'PICLC_checking':
								case 'pending':
								    $tr_css = ' class="absent"';
								    break;

							//	case 'paymentsettled':$tr_css = '';break;
								case 'waitingforinterview':$tr_css = ' class="waiting"';break;
								case 'confirmed':$tr_css = ' class="done"';break;
								case 'cancelled':$tr_css = ' class="draft"';break;
							}
				?>
							<tr<?=$tr_css?>>
							  <td><?=$idx?></td>
							  <td><a href="#/apps/admission/applicantslist/details/<?=$schoolYearID?>/<?=$_recordId?>/<?=($sys_custom['KIS_Admission']['MGF']['Settings']?"studentinfo/":"")?>"><?=$_applicationId?></a><br></td>
							  <td><?=$sys_custom['KIS_Admission']['ICMS']['Settings'] || $sys_custom['KIS_Admission']['CSM']['Settings'] || $sys_custom['KIS_Admission']['MUNSANG']['Settings'] || $sys_custom['KIS_Admission']['KTLMSKG']['Settings']?str_replace(",", " ", $_studentName):$_studentName?></td>

							  <?php
							  if($sys_custom['KIS_Admission']['SSGC']['Settings']){
							      $partB = array();
							      $partBQuestion = array(
							          'ParentIsStaff',
							          'SisterIsSameSchool',
							          'ParentIsManager',
							          'MotherSisterPrimaryGraduate',
							          'SameReligious',
							          'FirstBorn',
							          'MotherSecondGraduate',
							          'SiblingSameSchoolGraduate',
							      );
							      foreach($partBQuestion as $index => $field){
							          if($applicationCustDetails[$_applicationId][$field][0]['Value']){
							              $partB[] = $index + 1;
							          }
							      }
							  ?>
							  	<td><?= ($partB)? implode(',', $partB) : $kis_lang['Admission']['SSGC']['notApplicable'] ?></td>
							  <?php
							  }
							  ?>

							  <?php
							  if($sys_custom['KIS_Admission']['UCCKE']['Settings'] || $sys_custom['KIS_Admission']['STCC']['Settings']){
							  ?>
							  	<td><?=$_student_hkid?$_student_hkid:'--'?></td>
							  	<td><?=$_student_strn?$_student_strn:'--'?></td>
							  <?php
							  }
							  ?>

							  <td><?=$_parentName?($sys_custom['KIS_Admission']['ICMS']['Settings']?str_replace(",", " ", $_parentName):$_parentName):'--'?></td>
							  <td nowrap="nowrap"><?=$_parentPhone?$_parentPhone:'--'?></td>
							  <?=$sys_custom['KIS_Admission']['HKUGAPS']['Settings']?$_spokenLanguageForInterview:''?>
							  <?=$sys_custom['KIS_Admission']['ICMS']['Settings']?$_applyDayType:''?>
							  <td><?=$kis_lang['Admission']['Status'][$_status]?></td>
							  <td><?=$libinterface->Get_Checkbox('application_'.$_recordId, 'applicationAry[]', $_recordId, '', $Class='', $Display='', $Onclick="unset_checkall(this, document.getElementById('application_form'));", $Disabled='')?></td>
							 </tr>
				<?php }
					}else{ //no record
				?>
							<tr>
							<?if($sys_custom['KIS_Admission']['ICMS']['Settings']){?>
							  <td colspan="8" style="text-align:center;"><?=$kis_lang['norecord']?></td>
							<?}else{?>
							  <td colspan="7" style="text-align:center;"><?=$kis_lang['norecord']?></td>
							<?}?>
							</tr>
				<?php } ?>
							</tbody>
						</table>
						<input type="hidden" name="schoolYearID" id="schoolYearID" value="<?=$schoolYearID?>">
						<input type="hidden" name="classLevelID" id="classLevelID" value="<?=$classLevelID?>">
					</form>
          <p class="spacer"></p>
<? kis_ui::loadPageBar($page, $amount, $total, array(10, 20, 50, 100, 500, 1000)) ?>
<p class="spacer"></p><br>
	</div>
</div>

<!--FancyBox-->

	<div id='edit_box' style="padding:5px;display:none;" class="pop_edit">
    	<form id="application_status_form" method="post">
    		<div class="pop_title">
    			<span><?=$kis_lang['changestatus']?></span>
    		</div>
    		<div class="table_board" style="height:330px">

    			<table class="form_table">
    				<tr>
                       <td width="30%" class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['Admission']['applicationstatus']?></td>
                       <td width="70%"><?=$statusSelection?></td>
                     </tr>
    			</table>
    		  <p class="spacer"></p>
    	   </div>
    	    <div class="edit_bottom">
    	       <input id="submitBtn" name="submitBtn" type="submit" class="formbutton" value="<?=$kis_lang['submit']?>" />
    	       <input name="cancelBtn" type="button" class="formsubbutton" onclick="parent.$.fancybox.close();" value="<?=$kis_lang['cancel']?>" />
    	  	</div>

    	</form>
	</div>
	<?if($sys_custom['KIS_Admission']['ApplicantChangeClassLevel'] || $sys_custom['KIS_Admission']['CSM']['Settings']){?>
	<div id='edit_class' style="padding:5px;display:none;" class="pop_edit">
	<form id="application_class_form" method="post">
		<div class="pop_title">
			<span><?=$kis_lang['changeclass']?></span>
		</div>
		<div class="table_board" style="height:330px">

			<table class="form_table">
				<tr>
                   <td width="30%" class="field_title"><?=$mustfillinsymbol?><?=$kis_lang['form']?></td>
                   <td width="70%"><?=$classChangeLevelSelection?></td>
                 </tr>
			</table>
		  <p class="spacer"></p>
	   </div>
	    <div class="edit_bottom">
	       <input id="submitBtn" name="submitBtn" type="submit" class="formbutton" value="<?=$kis_lang['submit']?>" />
	       <input name="cancelBtn" type="button" class="formsubbutton" onclick="parent.$.fancybox.close();" value="<?=$kis_lang['cancel']?>" />
	  	</div>

	</form>
	</div>
	<?}?>