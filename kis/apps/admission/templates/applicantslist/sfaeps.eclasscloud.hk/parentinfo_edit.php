<?php

global $libkis_admission;

######## Get Parent Cust Info START ########
$applicationCustInfo = $libkis_admission->getAllApplicationCustInfo($applicationInfo['applicationID']);

######## Get Parent Cust Info END ########
?>
<input type="hidden" name="ApplicationID" value="<?= $applicationInfo['applicationID'] ?>"/>
<table class="form_table">
    <colgroup>
        <col style="width:15%">
        <col style="width:15%">
        <col style="width:15%">
        <col style="width:15%">
        <col style="width:15%">
    </colgroup>
    <tr>
        <td>&nbsp;</td>
        <td class="form_guardian_head">
            <center><?= $kis_lang['Admission']['PG_Type']['F'] ?></center>
        </td>
        <td class="form_guardian_head">
            <center><?= $kis_lang['Admission']['PG_Type']['M'] ?></center>
        </td>
        <td class="form_guardian_head">
            <center><?= $kis_lang['Admission']['PG_Type']['G'] ?></center>
        </td>
        <td class="form_guardian_head">
            <center><?= $kis_lang['Admission']['PG_Type']['EC'] ?></center>
        </td>
    </tr>
    <tr>
        <td class="field_title">
            <?= $kis_lang['Admission']['HKUGAPS']['nameChi'] ?>
        </td>
        <td class="form_guardian_field">
            <input name="G1ChineseName" type="text" id="G1ChineseName" class="textboxtext"
                   value="<?= $applicationInfo['F']['chinesename'] ?>"/>
        </td>
        <td class="form_guardian_field">
            <input name="G2ChineseName" type="text" id="G2ChineseName" class="textboxtext"
                   value="<?= $applicationInfo['M']['chinesename'] ?>"/>
        </td>
        <td class="form_guardian_field">
            <input name="G3ChineseName" type="text" id="G3ChineseName" class="textboxtext"
                   value="<?= $applicationInfo['G']['chinesename'] ?>"/>
        </td>
        <td class="form_guardian_field">
            <input name="G4ChineseName" type="text" id="G4ChineseName" class="textboxtext"
                   value="<?= $applicationInfo['E']['chinesename'] ?>"/>
        </td>
    </tr>
    <tr>
        <td class="field_title">
            <?= $kis_lang['Admission']['HKUGAPS']['nameEng'] ?>
        </td>
        <td class="form_guardian_field">
            <input name="G1EnglishName" type="text" id="G1EnglishName" class="textboxtext"
                   value="<?= $applicationInfo['F']['englishname'] ?>"/>
        </td>
        <td class="form_guardian_field">
            <input name="G2EnglishName" type="text" id="G2EnglishName" class="textboxtext"
                   value="<?= $applicationInfo['M']['englishname'] ?>"/>
        </td>
        <td class="form_guardian_field">
            <input name="G3EnglishName" type="text" id="G3EnglishName" class="textboxtext"
                   value="<?= $applicationInfo['G']['englishname'] ?>"/>
        </td>
        <td class="form_guardian_field">
            <input name="G4EnglishName" type="text" id="G4EnglishName" class="textboxtext"
                   value="<?= $applicationInfo['E']['englishname'] ?>"/>
        </td>
    </tr>
    <tr>
        <td class="field_title">
            <?= $kis_lang['Admission']['HKUGAPS']['contactNum'] ?>
        </td>
        <td class="form_guardian_field">
            <input name="G1MobileNo" type="text" id="G1MobileNo" class="textboxtext"
                   value="<?= $applicationInfo['F']['mobile'] ?>"/>
        </td>
        <td class="form_guardian_field">
            <input name="G2MobileNo" type="text" id="G2MobileNo" class="textboxtext"
                   value="<?= $applicationInfo['M']['mobile'] ?>"/>
        </td>
        <td class="form_guardian_field">
            <input name="G3MobileNo" type="text" id="G3MobileNo" class="textboxtext"
                   value="<?= $applicationInfo['G']['mobile'] ?>"/>
        </td>
         <td class="form_guardian_field">
            <input name="G4MobileNo" type="text" id="G4MobileNo" class="textboxtext"
                   value="<?= $applicationInfo['E']['mobile'] ?>"/>
        </td>
    </tr>
    <!--<tr>
        <td class="field_title">
            <?= $kis_lang['Admission']['HKUGAPS']['email'] ?>
        </td>
        <td class="form_guardian_field">
            <input name="G1Email" type="text" id="G1Email" class="textboxtext"
                   value="<?= $applicationInfo['F']['email'] ?>"/>
        </td>
        <td class="form_guardian_field">
            <input name="G2Email" type="text" id="G2Email" class="textboxtext"
                   value="<?= $applicationInfo['M']['email'] ?>"/>
        </td>
        <td class="form_guardian_field">
            <input name="G3Email" type="text" id="G3Email" class="textboxtext"
                   value="<?= $applicationInfo['G']['email'] ?>"/>
        </td>
    </tr>-->
    <tr>
        <td class="field_title">
            <?= $kis_lang['Admission']['occupation'] ?>
        </td>
        <td class="form_guardian_field">
            <input name="G1Occupation" type="text" id="G1Occupation" class="textboxtext"
                   value="<?= $applicationInfo['F']['occupation'] ?>"/>
        </td>
        <td class="form_guardian_field">
            <input name="G2Occupation" type="text" id="G2Occupation" class="textboxtext"
                   value="<?= $applicationInfo['M']['occupation'] ?>"/>
        </td>
        <td class="form_guardian_field">
            <input name="G3Occupation" type="text" id="G3Occupation" class="textboxtext"
                   value="<?= $applicationInfo['G']['occupation'] ?>"/>
        </td>
        <td class="form_guardian_field">
            <input name="G4Occupation" type="text" id="G4Occupation" class="textboxtext"
                   value="<?= $applicationInfo['E']['occupation'] ?>"/>
        </td>
    </tr>
</table>


<script>
    $('#applicant_form').unbind('submit').submit(function (e) {
        var schoolYearId = $('#schoolYearId').val();
        var recordID = $('#recordID').val();
        var display = $('#display').val();
        var timeSlot = lang.timeslot.split(',');
        if (checkValidForm()) {
            $.post('apps/admission/ajax.php?action=updateApplicationInfo', $(this).serialize(), function (success) {
                $.address.value('/apps/admission/applicantslist/details/' + schoolYearId + '/' + recordID + '/' + display + '&sysMsg=' + success);
            });
        }
        return false;
    });

    function checkIsChineseCharacter(str) {
        return str.match(/^[\u3400-\u9FBF]*$/);
    }

    function checkIsEnglishCharacter(str) {
        return str.match(/^[A-Za-z ,\-]*$/);
    }

    function checkNaNull(str) {
        return (
            ($.trim(str).toLowerCase() == '沒有') ||
            ($.trim(str).toLowerCase() == 'nil') ||
            ($.trim(str).toLowerCase() == 'n.a.')
        );
    }

    function checkValidForm() {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

        var form1 = $('#applicant_form')[0];
        var isTeacherInput = true;

        /******** Parent Info START ********/
        <?php for($i = 1;$i <= 4;$i++):?>
        /**** Name START ****/
        if ($.trim(form1.G<?=$i?>ChineseName.value) == '') {
            alert("<?=$kis_lang['Admission']['HKUGAPS']['msg']['enterParentName']?>\nPlease Enter Name of Parent.");
            form1.G<?=$i?>ChineseName.focus();
            return false;
        }

        if ($.trim(form1.G<?=$i?>EnglishName.value) == '') {
            alert("<?=$kis_lang['Admission']['HKUGAPS']['msg']['enterParentName']?>\nPlease Enter Name of Parent.");
            form1.G<?=$i?>EnglishName.focus();
            return false;
        }
        if (
            !checkNaNull(form1.G<?=$i?>EnglishName.value) &&
            !checkIsEnglishCharacter(form1.G<?=$i?>EnglishName.value)
        ) {
            alert("<?=$kis_lang['Admission']['msg']['enterenglishcharacter']?>\nPlease enter English character.");
            form1.G<?=$i?>EnglishName.focus();
            return false;
        }
        /**** Name END ****/

        /**** Contact Number START ****/
        if (!isTeacherInput && $.trim(form1.G<?=$i?>MobileNo.value) == '') {
            alert("<?=$kis_lang['Admission']['HKUGAPS']['msg']['enterContactNo']?>\nPlease Enter Contact Number.");
            form1.G<?=$i?>MobileNo.focus();
            return false;
        }
        if (
            !checkNaNull(form1.G<?=$i?>MobileNo.value) &&
            !/^[0-9]*$/.test(form1.G<?=$i?>MobileNo.value)
        ) {
            alert("<?=$kis_lang['Admission']['HKUGAPS']['msg']['invalidContactNoFormat']?>\nInvalid Contact Number Format.");
            form1.G<?=$i?>MobileNo.focus();
            return false;
        }
        /**** Contact Number END ****/

        /**** Email START ****/
        /*if (!isTeacherInput && $.trim(form1.G<?=$i?>Email.value) == '') {
            alert("<?=$kis_lang['Admission']['icms']['msg']['entermailaddress']?>\nPlease enter Email Address.");
            form1.G<?=$i?>Email.focus();
            return false;
        }
        if (
            (form1.G<?=$i?>Email.value != '') &&
            !checkNaNull(form1.G<?=$i?>Email.value) &&
            (!re.test(form1.G<?=$i?>Email.value))
        ) {
            alert("<?=$kis_lang['Admission']['icms']['msg']['invalidmailaddress']?>\nInvalid Email Format");
            form1.G<?=$i?>Email.focus();
            return false;
        }*/
        /**** Email END ****/

        /**** Occupation START ****/
        if (!isTeacherInput && $.trim(form<?=$i?>.G1Occupation.value) == '') {
            alert("<?=$kis_lang['Admission']['msg']['enteroccupation']?>\nPlease Enter Occupation.");
            form1.G<?=$i?>Occupation.focus();
            return false;
        }
        /**** Occupation END ****/

        /**** Institution START ****/
        if (!isTeacherInput && $.trim(form1.G<?=$i?>Institution.value) == '') {
            alert("<?=$kis_lang['Admission']['HKUGAPS']['msg']['enterInstitution']?>\nPlease Enter Institution.");
            form1.G<?=$i?>Institution.focus();
            return false;
        }
        /**** Institution END ****/
        <?php endfor; ?>
        /******** Parent Info END ********/
        return true;
    }
</script>