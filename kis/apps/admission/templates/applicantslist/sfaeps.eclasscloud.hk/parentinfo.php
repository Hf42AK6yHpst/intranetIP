<?php

global $libkis_admission;

######## Get Parent Cust Info START ########
$applicationCustInfo = $libkis_admission->getAllApplicationCustInfo($applicationInfo['applicationID']);
######## Get Parent Cust Info END ########
?>
<table class="form_table">
    <colgroup>
        <col style="width:15%">
        <col style="width:15%">
        <col style="width:15%">
        <col style="width:15%">
        <col style="width:15%">
    </colgroup>
    <tr>
        <td>&nbsp;</td>
        <td class="form_guardian_head">
            <center><?= $kis_lang['Admission']['PG_Type']['F'] ?></center>
        </td>
        <td class="form_guardian_head">
            <center><?= $kis_lang['Admission']['PG_Type']['M'] ?></center>
        </td>
        <td class="form_guardian_head">
            <center><?= $kis_lang['Admission']['PG_Type']['G'] ?></center>
        </td>
        <td class="form_guardian_head">
            <center><?= $kis_lang['Admission']['PG_Type']['EC'] ?></center>
        </td>
    </tr>
    <tr>
        <td class="field_title">
            <?= $kis_lang['Admission']['HKUGAPS']['nameChi'] ?>
        </td>
        <td class="form_guardian_field">
            <?= kis_ui::displayTableField($applicationInfo['F']['chinesename']) ?>
        </td>
        <td class="form_guardian_field">
            <?= kis_ui::displayTableField($applicationInfo['M']['chinesename']) ?>
        </td>
        <td class="form_guardian_field">
            <?= kis_ui::displayTableField($applicationInfo['G']['chinesename']) ?>
        </td>
        <td class="form_guardian_field">
            <?= kis_ui::displayTableField($applicationInfo['E']['chinesename']) ?>
        </td>
    </tr>
    <tr>
        <td class="field_title">
            <?= $kis_lang['Admission']['HKUGAPS']['nameEng'] ?>
        </td>
        <td class="form_guardian_field">
            <?= kis_ui::displayTableField($applicationInfo['F']['englishname']) ?>
        </td>
        <td class="form_guardian_field">
            <?= kis_ui::displayTableField($applicationInfo['M']['englishname']) ?>
        </td>
        <td class="form_guardian_field">
            <?= kis_ui::displayTableField($applicationInfo['G']['englishname']) ?>
        </td>
        <td class="form_guardian_field">
            <?= kis_ui::displayTableField($applicationInfo['E']['englishname']) ?>
        </td>
    </tr>
    <tr>
        <td class="field_title">
            <?= $kis_lang['Admission']['HKUGAPS']['contactNum'] ?>
        </td>
        <td class="form_guardian_field">
            <?= kis_ui::displayTableField($applicationInfo['F']['mobile']) ?>
        </td>
        <td class="form_guardian_field">
            <?= kis_ui::displayTableField($applicationInfo['M']['mobile']) ?>
        </td>
        <td class="form_guardian_field">
            <?= kis_ui::displayTableField($applicationInfo['G']['mobile']) ?>
        </td>
        <td class="form_guardian_field">
            <?= kis_ui::displayTableField($applicationInfo['E']['mobile']) ?>
        </td>
    </tr>
    <!--<tr>
        <td class="field_title">
            <?= $kis_lang['Admission']['HKUGAPS']['email'] ?>
        </td>
        <td class="form_guardian_field">
            <?= kis_ui::displayTableField($applicationInfo['F']['email']) ?>
        </td>
        <td class="form_guardian_field">
            <?= kis_ui::displayTableField($applicationInfo['M']['email']) ?>
        </td>
        <td class="form_guardian_field">
            <?= kis_ui::displayTableField($applicationInfo['G']['email']) ?>
        </td>
    </tr>-->
    <tr>
        <td class="field_title">
            <?= $kis_lang['Admission']['occupation'] ?>
        </td>
        <td class="form_guardian_field">
            <?= kis_ui::displayTableField($applicationInfo['F']['occupation']) ?>
        </td>
        <td class="form_guardian_field">
            <?= kis_ui::displayTableField($applicationInfo['M']['occupation']) ?>
        </td>
        <td class="form_guardian_field">
            <?= kis_ui::displayTableField($applicationInfo['G']['occupation']) ?>
        </td>
        <td class="form_guardian_field">
            <?= kis_ui::displayTableField($applicationInfo['E']['occupation']) ?>
        </td>
    </tr>
</table>

