<?php 
$PATH_WRT_ROOT = "../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_ui.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_utility.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_apps.php");
//include_once($PATH_WRT_ROOT."includes/json.php");

include_once("../../../../config.php");

//for the customization
include_once($PATH_WRT_ROOT."includes/admission/".$setting_path_ip_rel."/config.php");
$intranet_session_language = $admission_cfg['DefaultLang'];
include_once($PATH_WRT_ROOT."includes/admission/".$setting_path_ip_rel."/libadmission_cust.php");
include_once($PATH_WRT_ROOT."includes/admission/".$setting_path_ip_rel."/libadmission_ui_cust.php");

include_once($PATH_WRT_ROOT."lang/lang.".$intranet_session_language.".php");
include_once($PATH_WRT_ROOT."lang/kis/lang_common_".$intranet_session_language.".php");
include_once($PATH_WRT_ROOT."lang/kis/apps/lang_admission_".$intranet_session_language.".php");
include_once($PATH_WRT_ROOT."lang/admission_lang.".$intranet_session_language.".php");

intranet_auth();
intranet_opendb();

//$libjson 	= new JSON_obj();
$libkis 	= new kis('');
$lac		= new admission_cust();
$lauc		= new admission_ui_cust();

if($_GET['id'] != '' && $_GET['year'] != ''){
		
	$applicationAttachmentInfo = $lac->getApplicationAttachmentRecord($_GET['year'],array('applicationID'=>$_GET['id']));
	//debug_pr($applicationAttachmentInfo);
	
	$file = $intranet_root."/file/admission/".$applicationAttachmentInfo[$_GET['id']][$_GET['type']]['attachment_link'][0];
	$mime_type = mime_content_type($file);

    $filename = basename($file);
    if($sys_custom['KIS_Admission']['HKUGAC']['Settings']){
        $attachment_settings = $lac->getAttachmentSettings();
        $isExtra = false;
        foreach($attachment_settings as $attachment_setting){
            if($attachment_setting['AttachmentName'] == $_GET['type']){
                $isExtra = $attachment_setting['IsExtra'];
                break;
            }
        }

        $originalFileName = $applicationAttachmentInfo[$_GET['id']][$_GET['type']]['original_attachment_name'][0];
        $filename = ($isExtra && $originalFileName)?$originalFileName:$filename;
    }

	if (file_exists($file)) {
	    header('Content-Description: File Transfer');
	    header('Content-Type: '.$mime_type);
	    header('Content-Disposition: attachment; filename="'.$filename.'"');
	    header('Expires: 0');
	    header('Cache-Control: must-revalidate');
	    header('Pragma: public');
	    header('Content-Length: ' . filesize($file));
	    readfile($file);
	    exit;
	}
}
?>