<?php
/// Editing by: 
/*
 * 2015-02-16 (Henry): added option Payment Status
 * 2014-11-27 (Henry) [ip2.5.5.12.1]: file created
 */
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_ui.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_utility.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_apps.php");

//include_once($PATH_WRT_ROOT."includes/json.php");
include_once($PATH_WRT_ROOT."kis/config.php");
include_once($PATH_WRT_ROOT."lang/kis/lang_common_".$intranet_session_language.".php");
include_once($PATH_WRT_ROOT."lang/kis/apps/lang_admission_".$intranet_session_language.".php");
include_once($PATH_WRT_ROOT."lang/admission_lang.".$intranet_session_language.".php");

//for the customization
include_once($PATH_WRT_ROOT."includes/admission/".$setting_path_ip_rel."/config.php");
include_once($PATH_WRT_ROOT."includes/admission/".$setting_path_ip_rel."/libadmission_cust.php");
include_once($PATH_WRT_ROOT."includes/admission/".$setting_path_ip_rel."/libadmission_ui_cust.php");

intranet_auth();
intranet_opendb();

$ldb = new libdb();
$lfs = new libfilesystem();
$libkis 	= new kis('');
$lac		= new admission_cust();
$lauc		= new admission_ui_cust();

@SET_TIME_LIMIT(216000);
# Temp Assign memory of this page
ini_set("memory_limit", "1000M");

if(sizeof($ApplicationID)==0 && $AppID!="") {
	$ApplicationID = explode(',', $AppID);
	$cond = " AND r.ApplicationID IN ('".implode("','",$ApplicationID)."')";
}

if($FromNo!="" && $ToNo!="") {
	$cond  = " AND (r.ApplicationID >= '".$FromNo."' AND r.ApplicationID <= '".$ToNo."')";
//	debug_pr(similar_text($FromNo, $ToNo));
//	debug_pr($ToNo - $FromNo);
}
//exit();

$FolderPath = $file_path."/file/admission/";

if($type){
	$cond .=  " AND  r.AttachmentType = '".$type."'";
}
if($_SESSION['UserType']==USERTYPE_STAFF){
	
	$sql = "Select count(*)
			from ADMISSION_OTHERS_INFO r
			where  (r.ApplicationID >= '".$FromNo."' AND r.ApplicationID <= '".$ToNo."')";
			
	$countAttachmentAry = $lac->returnArray($sql);
	if($countAttachmentAry[0][0] > 5000){
		echo $kis_lang['msg']['rangeOfApplcationNoLimit'];
		exit();
	}
	
	if($sys_custom['KIS_Admission']['PayPal'] && $admission_cfg['PaymentStatus']){
		if($paymentStatus==$admission_cfg['PaymentStatus']['OnlinePayment']){
			$paymentStatus_cond = " left outer join ADMISSION_PAYMENT_INFO as pi ON o.ApplicationID = pi.ApplicationID";
			$cond .= " AND pi.ApplicationID IS NOT NULL  ";
		}
		else if($paymentStatus==$admission_cfg['PaymentStatus']['OtherPayment']){
			$paymentStatus_cond = " left outer join ADMISSION_PAYMENT_INFO as pi ON o.ApplicationID = pi.ApplicationID";
			$cond .= " AND pi.ApplicationID IS NULL  ";
		}
	}
	
	$sql = "Select 
        r.ApplicationID, 
        r.AttachmentType, 
        CONCAT('".$FolderPath."',o.RecordID,'/',IF(r.AttachmentType = 'personal_photo',r.AttachmentType,'other_files'),'/',r.AttachmentName),
        r.OriginalAttachmentName,
         s.IsExtra
    From ADMISSION_ATTACHMENT_RECORD r
    INNER JOIN
        ADMISSION_OTHERS_INFO o 
    ON r.ApplicationID = o.ApplicationID
    INNER JOIN
        ADMISSION_ATTACHMENT_SETTING s
    ON
        r.AttachmentType = s.AttachmentName
    ".$paymentStatus_cond." where 1 ".$cond;
			
	$attachmentAry = $lac->returnArray($sql);
	
//	debug_pr($attachmentAry);
//	exit();
	
	if($mode == 'zip'){
		$uFolder = "u".$UserID;
		$tempFolder = $FolderPath.$uFolder;
		
		if (!file_exists($tempFolder)) {
			$lfs->folder_new($tempFolder);
		}
			
		$ZipFileName = $uFolder.".zip";
		$ZipFilePath = $FolderPath.$ZipFileName;
		
		for($i=0; $i<sizeof($attachmentAry); $i++) { 
			list($ApplicationID, $AttachmentType, $FileFullPath, $OriginalAttachmentName, $IsExtra) = $attachmentAry[$i];

			$attachmentName = iconv("UTF-8", "BIG5", $ApplicationID.'_'.$AttachmentType.'.'.pathinfo($FileFullPath, PATHINFO_EXTENSION));
			if($IsExtra){
                $attachmentName = $ApplicationID.'_'.$OriginalAttachmentName;
            }
			
			$currentDir = getcwd();
			if(file_exists($FileFullPath))
				$lfs->file_copy($FileFullPath, $tempFolder.'/'.$attachmentName);
			chdir($currentDir);
		}
		
		$lfs->chmod_R($tempFolder, 0777);
		
		$lfs->file_remove($ZipFilePath);
		$lfs->file_zip($uFolder, $ZipFileName, $FolderPath);
		
		# remove the copy location
		$lfs->folder_remove_recursive($uFolder);
		
		if(file_exists($ZipFileName))
			output2browser(get_file_content($ZipFileName), $ZipFileName);
		$lfs->file_remove($ZipFileName);
	}
}

?>