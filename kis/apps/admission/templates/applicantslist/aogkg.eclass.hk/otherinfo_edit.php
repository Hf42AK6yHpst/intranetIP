<?php

global $libkis_admission;


#### Get Student Cust Info START ####
$allCustInfo = $libkis_admission->getAllApplicationCustInfo($applicationInfo['applicationID']);
#### Get Student Cust Info START ####


?>
<input type="hidden" name="ApplicationID" value="<?=$applicationInfo['applicationID']?>" />

<style>
.otherDiv{
    margin: 5px 0;
}
</style>


<table class="form_table otherInformation" style="font-size: 13px">
    <colgroup>
        <col style="width:30%">
        <col style="">
    </colgroup>

<tbody>
    <tr class="otherQuestionRow">
       	<td class="field_title">
    		<?=$kis_lang['Admission']['AOGKG']['currentStudySibiling']?>
    		<br />
    		
    	</td>
    	<td>
    		<div class="otherDiv">
    			<label for="CurrentStudySibiling_Name">
    				<?=$kis_lang['Admission']['name'] ?>
    				: 
    			</label>
    			<input 
        			id="CurrentStudySibiling_Name" 
        			name="CurrentStudySibiling_Name" 
        			class="textboxtext" 
        			style="width: 200px;"
        			value="<?=$allCustInfo['CurrentStudySibiling_Name'][0]['Value'] ?>" 
    			/>
    		</div>
    		<div class="otherDiv">
    			<label for="CurrentStudySibiling_Class">
    				<?=$kis_lang['Admission']['class'] ?>
    				: 
    			</label>
    			<input 
        			id="CurrentStudySibiling_Class" 
        			name="CurrentStudySibiling_Class" 
        			class="textboxtext" 
        			style="width: 200px;"
        			value="<?=$allCustInfo['CurrentStudySibiling_Class'][0]['Value'] ?>" 
    			/>
    		</div>
    	</td>
    </tr>
    
    <tr class="otherQuestionRow">
       	<td class="field_title">
    		<?=$kis_lang['Admission']['AOGKG']['alunmusSibiling']?>
    		<br />
    		
    	</td>
    	<td>
    		<div class="otherDiv">
    			<label for="AlunmusSibiling_Name">
    				<?=$kis_lang['Admission']['name'] ?>
    				: 
    			</label>
    			<input 
        			id="AlunmusSibiling_Name" 
        			name="AlunmusSibiling_Name" 
        			class="textboxtext" 
        			style="width: 200px;"
        			value="<?=$allCustInfo['AlunmusSibiling_Name'][0]['Value'] ?>" 
    			/>
    		</div>
    		<div class="otherDiv">
    			<label for="CurrentStudySibiling_GraduationYear">
    				<?=$kis_lang['Admission']['GradYear'] ?>
    				: 
    			</label>
    			<input 
        			id="CurrentStudySibiling_GraduationYear" 
        			name="CurrentStudySibiling_GraduationYear" 
        			class="textboxtext" 
        			style="width: 200px;"
        			value="<?=$allCustInfo['CurrentStudySibiling_GraduationYear'][0]['Value'] ?>" 
    			/>
    		</div>
    	</td>
    </tr>
    
    <tr class="otherQuestionRow">
       	<td class="field_title">
    		<?=$kis_lang['Admission']['AOGKG']['memberOfFanling']?>
    		<br />
    		
    	</td>
    	<td>
    		<div class="otherDiv">
    			<label for="MemberOfFanling_Name">
    				<?=$kis_lang['Admission']['name'] ?>
    				: 
    			</label>
    			<input 
        			id="MemberOfFanling_Name" 
        			name="MemberOfFanling_Name" 
        			class="textboxtext" 
        			style="width: 200px;"
        			value="<?=$allCustInfo['MemberOfFanling_Name'][0]['Value'] ?>" 
    			/>
    			&nbsp;
    			&nbsp;
    			(
    			<label for="MemberOfFanling_MembershipNumber">
    				<?=$kis_lang['Admission']['AOGKG']['membershipNumber'] ?>
    				: 
    			</label>
    			<input 
        			id="MemberOfFanling_MembershipNumber" 
        			name="MemberOfFanling_MembershipNumber" 
        			class="textboxtext" 
        			style="width: 200px;"
        			value="<?=$allCustInfo['MemberOfFanling_MembershipNumber'][0]['Value'] ?>" 
    			/>
    			)
    		</div>
    		<div class="otherDiv">
    			<label for="MemberOfFanling_Relationship">
    				<?=$kis_lang['Admission']['AOGKG']['membershipReleationship'] ?>
    				: 
    			</label>
        		<?php 
        		    $relationshipFChecked = ($allCustInfo['MemberOfFanling_Relationship'][0]['Value'] == 'F')? 'checked' : '';
        		    $relationshipMChecked = ($allCustInfo['MemberOfFanling_Relationship'][0]['Value'] == 'M')? 'checked' : '';
        		?>
    			<input type="radio" id="MemberOfFanling_RelationshipF" name="MemberOfFanling_Relationship" value="F" <?=$relationshipFChecked ?> />
    			<label for="MemberOfFanling_RelationshipF">
    				<?=$kis_lang['Admission']['PG_Type']['F'] ?>
    				
    			</label>
    			&nbsp;
    			<input type="radio" id="MemberOfFanling_RelationshipM" name="MemberOfFanling_Relationship" value="M" <?=$relationshipMChecked ?> />
    			<label for="MemberOfFanling_RelationshipM">
    				<?=$kis_lang['Admission']['PG_Type']['M'] ?>
    				
    			</label>
    		</div>
    	</td>
    </tr>
    
    <tr class="otherQuestionRow">
       	<td class="field_title">
    		<?=$kis_lang['Admission']['AOGKG']['twinApply']?>
    		
    	</td>
    	<td>
			<label for="TwinApply_Name">
				<?=$kis_lang['Admission']['name'] ?>
				: 
			</label>
			<input 
    			id="TwinApply_Name" 
    			name="TwinApply_Name" 
    			class="textboxtext" 
    			style="width: 200px;"
    			value="<?=$allCustInfo['TwinApply_Name'][0]['Value'] ?>" 
			/>
    	</td>
    </tr>
    
    <tr class="otherQuestionRow">
       	<td class="field_title">
    		<?=$kis_lang['Admission']['remarks']?>
    		
    	</td>
    	<td>
			<input 
    			id="Remarks" 
    			name="Remarks" 
    			class="textboxtext" 
    			value="<?=$allCustInfo['Remarks'][0]['Value'] ?>" 
			/>
    	</td>
    </tr>
    
</tbody>

</table>


<script>
//// UI START ////
function updateUI(){
	var isNotApplicable = !!$('#partBNotApplicable:checked').length;
	$('.otherQuestionRow input, .otherQuestionRow select').prop('disabled', isNotApplicable);
	if(isNotApplicable){
		$('.otherQuestionRow label').addClass('disabled');
	}else{
		$('.otherQuestionRow label').removeClass('disabled');
	}

	$('.partBQuestion input:checked').each(function(index, element){
		var value = $(this).val();
		var $td = $(this).closest('td');
		var $detailsTd = $td.find('.partBDetails');
		if(value == '1'){
			$detailsTd.show();
			$detailsTd.find('input,select').prop('disabled', isNotApplicable);
		}else{
			$detailsTd.hide();
			$detailsTd.find('input,select').prop('disabled', true);
		}
	});

	if($('[name="SiblingSameSchoolGraduate_Type"]:checked').val() == '1'){
		$('#SiblingSameSchoolGraduate_NameClassDiv').hide().find('input,select').prop('disabled', true);
		$('#SiblingSameSchoolGraduate_NameYearOfGraduationDiv').show().find('input,select').prop('disabled', isNotApplicable);
	}else{
		$('#SiblingSameSchoolGraduate_NameClassDiv').show().find('input,select').prop('disabled', isNotApplicable);
		$('#SiblingSameSchoolGraduate_NameYearOfGraduationDiv').hide().find('input,select').prop('disabled', true);
	}
}
$('#partBNotApplicable, .partBQuestion input').click(updateUI);
$('[name="SiblingSameSchoolGraduate_Type"]').change(updateUI);
updateUI();
//// UI END ////

$('#applicant_form').unbind('submit').submit(function(e){
	e.preventDefault();
	
	var schoolYearId = $('#schoolYearId').val();
	var recordID = $('#recordID').val();
	var display = $('#display').val();
	var timeSlot = lang.timeslot.split(',');
	if(checkValidForm()){
		$.post('apps/admission/ajax.php?action=updateApplicationInfo', $(this).serialize(), function(success){
			$.address.value('/apps/admission/applicantslist/details/'+schoolYearId+'/'+recordID+'/'+display+'&sysMsg='+success);
		});
	}
	return false;
});

function checkIsChineseCharacter(str){
	return str.match(/^[\u3400-\u9FBF]*$/);
}

function checkIsEnglishCharacter(str){
	return str.match(/^[A-Za-z ,\-]*$/);
}

function checkNaNull(str){
	return (
    	($.trim(str).toLowerCase()=='沒有') || 
    	($.trim(str).toLowerCase()=='nil') ||
    	($.trim(str).toLowerCase()=='n.a.')
	);
}

function checkValidForm(){
	var re = /\S+@\S+\.\S+/;
	var form1 = applicant_form;

	/******** Other Info START ********/
	/**** Current Study Sibiling Name START ****/
	if(
		$.trim(form1.CurrentStudySibiling_Name.value)!='' &&
		$.trim(form1.CurrentStudySibiling_Class.value)==''
	){
		alert(" <?=$kis_lang['Admission']['munsang']['msg']['enterClass']?>");
		form1.CurrentStudySibiling_Class.focus();
		return false;
	}
	/**** Current Study Sibiling Name END ****/
	
	/**** Alunmus Sibiling Name START ****/
	if(
		$.trim(form1.AlunmusSibiling_Name.value)!='' &&
		$.trim(form1.CurrentStudySibiling_GraduationYear.value)==''
	){
		alert(" <?=$kis_lang['Admission']['msg']['enterGradYear']?>");
		form1.CurrentStudySibiling_GraduationYear.focus();
		return false;
	}
	/**** Alunmus Sibiling Name END ****/
	
	/**** Member Of Fanling Name START ****/
	if($.trim(form1.MemberOfFanling_Name.value)!=''){
    	
    	if($.trim(form1.MemberOfFanling_MembershipNumber.value)==''){
    		alert(" <?=$kis_lang['Admission']['AOGKG']['msg']['enterMembershipNumber']?>");
    		form1.MemberOfFanling_MembershipNumber.focus();
    		return false;
    	}
    	
    	if($('input[name="MemberOfFanling_Relationship"]:checked').length == 0){
    		alert(" <?=$kis_lang['Admission']['AOGKG']['msg']['selectRelationship']?>");
    		form1.MemberOfFanling_Relationship.focus();
    		return false;
    	}
		
	}
	/**** Member Of Fanling Name END ****/
	/******** Other Info END ********/
	return true;
}
</script>
