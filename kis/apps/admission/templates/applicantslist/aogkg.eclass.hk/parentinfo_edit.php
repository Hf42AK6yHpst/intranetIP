<?php

global $libkis_admission;

$parentInfoArr = $applicationInfo;
$star = $mustfillinsymbol;

######## Get Parent Cust Info START ########
//$applicationCustInfo = $libkis_admission->getAllApplicationCustInfo($parentInfoArr['applicationID']);
######## Get Parent Cust Info END ########
?>
<input type="hidden" name="ApplicationID" value="<?=$applicationInfo['applicationID']?>" />

<table class="form_table parentInfo">
	<colgroup>
        <col style="width:30%">
        <col style="width:23%">
        <col style="width:23%">
        <col style="width:24%">
    </colgroup>
	<tr>
		<td>&nbsp;</td>
		<td class="form_guardian_head"><center><?=$kis_lang['Admission']['PG_Type']['F'] ?></center></td>
		<td class="form_guardian_head"><center><?=$kis_lang['Admission']['PG_Type']['M'] ?></center></td>
		<td class="form_guardian_head"><center><?=$kis_lang['Admission']['PG_Type']['G'] ?></center></td>
	</tr>
	<tr>
		<td class="field_title">
    		<?=$star?>
    		<?=$kis_lang['Admission']['chinesename'] ?>
		</td>
		<td class="form_guardian_field">
			<input name="G1ChineseName" type="text" id="G1ChineseName" class="textboxtext" value="<?=$parentInfoArr['F']['ChineseName'] ?>"/>
    	</td>
		<td class="form_guardian_field">
			<input name="G2ChineseName" type="text" id="G2ChineseName" class="textboxtext" value="<?=$parentInfoArr['M']['ChineseName'] ?>"/>
		</td>
		<td class="form_guardian_field">
			<input name="G3ChineseName" type="text" id="G3ChineseName" class="textboxtext" value="<?=$parentInfoArr['G']['ChineseName'] ?>"/>
		</td>
	</tr>
	
	<tr>
		<td class="field_title">
    		<?=$star?>
    		<?=$kis_lang['Admission']['englishname'] ?>
		</td>
		<td class="form_guardian_field">
			<input name="G1EnglishName" type="text" id="G1EnglishName" class="textboxtext" value="<?=$parentInfoArr['F']['EnglishName'] ?>"/>
		</td>
		<td class="form_guardian_field">
			<input name="G2EnglishName" type="text" id="G2EnglishName" class="textboxtext" value="<?=$parentInfoArr['M']['EnglishName'] ?>"/>
		</td>
		<td class="form_guardian_field">
			<input name="G3EnglishName" type="text" id="G3EnglishName" class="textboxtext" value="<?=$parentInfoArr['G']['EnglishName'] ?>"/>
		</td>
	</tr>
	
	<tr>
		<td class="field_title">
    		<?=$star?>
    		<?=$kis_lang['Admission']['phoneno'] ?>
		</td>
		<td class="form_guardian_field">
			<input name="G1Mobile" type="text" id="G1Mobile" class="textboxtext" value="<?=$parentInfoArr['F']['Mobile'] ?>"/>
		</td>
		<td class="form_guardian_field">
			<input name="G2Mobile" type="text" id="G2Mobile" class="textboxtext" value="<?=$parentInfoArr['M']['Mobile'] ?>"/>
		</td>
		<td class="form_guardian_field">
			<input name="G3Mobile" type="text" id="G3Mobile" class="textboxtext" value="<?=$parentInfoArr['G']['Mobile'] ?>"/>
		</td>
	</tr>
	
	<tr>
		<td class="field_title">
    		<?=$star?>
    		<?=$kis_lang['Admission']['AOGKG']['nameOfOffice'] ?>
		</td>
		<td class="form_guardian_field">
			<input name="G1Company" type="text" id="G1Company" class="textboxtext" value="<?=$parentInfoArr['F']['Company'] ?>"/>
		</td>
		<td class="form_guardian_field">
			<input name="G2Company" type="text" id="G2Company" class="textboxtext" value="<?=$parentInfoArr['M']['Company'] ?>"/>
		</td>
		<td class="form_guardian_field">
			<input name="G3Company" type="text" id="G3Company" class="textboxtext" value="<?=$parentInfoArr['G']['Company'] ?>"/>
		</td>
	</tr>
	
	<tr>
		<td class="field_title">
    		<?=$star?>
    		<?=$kis_lang['Admission']['AOGKG']['position'] ?>
		</td>
		<td class="form_guardian_field">
			<input name="G1JobPosition" type="text" id="G1JobPosition" class="textboxtext" value="<?=$parentInfoArr['F']['JobPosition'] ?>"/>
		</td>
		<td class="form_guardian_field">
			<input name="G2JobPosition" type="text" id="G2JobPosition" class="textboxtext" value="<?=$parentInfoArr['M']['JobPosition'] ?>"/>
		</td>
		<td class="form_guardian_field">
			<input name="G3JobPosition" type="text" id="G3JobPosition" class="textboxtext" value="<?=$parentInfoArr['G']['JobPosition'] ?>"/>
		</td>
	</tr>
	
	
	
</table>
<script>
$('#applicant_form').unbind('submit').submit(function(e){
	e.preventDefault();
	
	var schoolYearId = $('#schoolYearId').val();
	var recordID = $('#recordID').val();
	var display = $('#display').val();
	var timeSlot = lang.timeslot.split(',');
	if(checkValidForm()){
		$.post('apps/admission/ajax.php?action=updateApplicationInfo', $(this).serialize(), function(success){
			$.address.value('/apps/admission/applicantslist/details/'+schoolYearId+'/'+recordID+'/'+display+'&sysMsg='+success);
		});
	}
	return false;
});

function checkIsChineseCharacter(str){
	return str.match(/^[\u3400-\u9FBF]*$/);
}

function checkIsEnglishCharacter(str){
	return str.match(/^[A-Za-z ,\-]*$/);
}

function checkNaNull(str){
	return (
    	($.trim(str).toLowerCase()=='沒有') || 
    	($.trim(str).toLowerCase()=='nil') ||
    	($.trim(str).toLowerCase()=='n.a.')
	);
}

function checkValidForm(){
	var re = /\S+@\S+\.\S+/;
	var form1 = applicant_form;

	/******** Parent Info START ********/
	/**** Name START ****/
	var isValid = true;
	$('.parentInfo input').filter(function() {
        return this.name.match(/G\dChineseName/);
    }).each(function(){
    	if($.trim(this.value)==''){
    		alert(" <?=$kis_lang['Admission']['HKUGAPS']['msg']['enterParentName']?>");
    		this.focus();
    		return isValid = false;
    	}
    	if(
    		!checkNaNull(this.value) &&
    		!checkIsChineseCharacter(this.value)
    	){
    		alert(" <?=$kis_lang['Admission']['msg']['enterchinesecharacter']?>");	
    		this.focus();
    		return isValid = false;
    	}
    });
	if(!isValid) return false;
	
	$('.parentInfo input').filter(function() {
        return this.name.match(/G\dEnglishName/);
    }).each(function(){
    	if($.trim(this.value)==''){
    		alert(" <?=$kis_lang['Admission']['HKUGAPS']['msg']['enterParentName']?>");
    		this.focus();
    		return isValid = false;
    	}
    	if(
    		!checkNaNull(this.value) &&
    		!checkIsEnglishCharacter(this.value)
    	){
    		alert(" <?=$kis_lang['Admission']['msg']['enterenglishcharacter']?>");	
    		this.focus();
    		return isValid = false;
    	}
    });
	if(!isValid) return false;
	/**** Name END ****/

	/**** Contact Number START ****/
	var isValid = true;
	$('.parentInfo input').filter(function() {
        return this.name.match(/G\dMobile/);
    }).each(function(){
    	if($.trim(this.value)==''){
    		alert(" <?=$kis_lang['Admission']['msg']['enterphoneno']?>");
    		this.focus();
    		return isValid = false;
    	}
    	if(
			!checkNaNull(this.value) &&
			!/^[0-9]*$/.test(this.value)
    	){
    		alert(" <?=$kis_lang['Admission']['msg']['invalidphoneno']?>");	
    		this.focus();
    		return isValid = false;
    	}
    });
	if(!isValid) return false;
	/**** Contact Number END ****/

	/**** Name of the office START ****/
	var isValid = true;
	$('.parentInfo input').filter(function() {
        return this.name.match(/G\dCompany/);
    }).each(function(){
    	if($.trim(this.value)==''){
    		alert(" <?=$kis_lang['Admission']['AOGKG']['msg']['enterNameOfOffice']?>");
    		this.focus();
    		return isValid = false;
    	}
    });
	if(!isValid) return false;
	/**** Name of the office START ****/

	/**** Position START ****/
	var isValid = true;
	$('.parentInfo input').filter(function() {
        return this.name.match(/G\dJobPosition/);
    }).each(function(){
    	if($.trim(this.value)==''){
    		alert(" <?=$kis_lang['Admission']['AOGKG']['msg']['enterPosition']?>");
    		this.focus();
    		return isValid = false;
    	}
    });
	if(!isValid) return false;
	/**** Position START ****/
	/******** Parent Info END ********/
	return true;
}
</script>