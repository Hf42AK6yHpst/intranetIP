<?php
// Editing by Henry

/**
 *  2015-02-16 (Henry): added option Payment Status
 *	2014-04-02 (Henry): Created file
 **/
 
include_once($intranet_root."/lang/lang.".$intranet_session_language.".php");

$linterface = $kis_data['libinterface'];

?>
<script type="text/javascript" language="JavaScript">
$(function(){
	kis.admission.attachment_download_init({
		pleaseinputApplicationNo:'<?=$kis_lang['msg']['enterapplicationno']?>'
	});
})
</script>
<div class="main_content_detail">
<form id="downloadForm" name="downloadForm" action='./apps/admission/templates/applicantslist/download.php' method="post">
	<?=$kis_data['NavigationBar']?>
	<table id="ComposeMailTable" class="form_table">
    	<tbody>
        	<tr> 
            	<td class="field_title" style="width:30%;"><span style="color:red">*</span><?=$kis_lang['applicationno'].' ('.$kis_lang['from'].')'?></td>
            	<td style="width:70%;">
            	<input type="text"  id = "FromID" name="FromNo">
            	</td>
            </tr>
            <tr> 
            	<td class="field_title" style="width:30%;"><span style="color:red">*</span><?=$kis_lang['applicationno'].' ('.$kis_lang['to'].')'?></td>
            	<td style="width:70%;">
            	<input type="text"  id = "ToID" name="ToNo">
            	</td>
            </tr>
            <?if($sys_custom['KIS_Admission']['PayPal'] && $admission_cfg['PaymentStatus']){?>
			<tr class="step1">
            	<td class="field_title" style="width:30%;"><?=$kis_lang['status']?></td>
            	<td style="width:70%;">
            	<?=$paymentStatus?>
            	<??>
            	</td>
            </tr>
            <?}?>
            <tr class="step1"> 
            	<td class="field_title" style="width:30%;"><?=$kis_lang['type']?></td>
            	<td style="width:70%;">
            	<?=$libinterface->GET_SELECTION_BOX($AttachmentTypeArray, "name='type' id='type' class=''",$kis_lang['all'])?>
            	<??>
            	</td>
            </tr>
            <tr> 
            	<td class="field_title" style="width:30%;"><?=$kis_lang['downloadmode']?></td>
            	<td style="width:70%;">
            	<?=$libinterface->Get_Radio_Button('mode_zip', 'mode', 'zip', 1, '', 'ZIP')?>
                <?=$libinterface->Get_Radio_Button('mode_html', 'mode', 'html', 0, '', 'HTML')?>    
            	</td>
            </tr>
        </tbody>
    </table>
    <div class="text_remark"><?=str_replace('*','<span style="color:red">*</span>',$kis_lang['requiredfield'])?></div>
    <div class="text_remark"><?=$kis_lang['msg']['browserBusyWhenManyImage']?></div>
    <div class="text_remark">*<?=$kis_lang['msg']['rangeOfApplcationNoLimit']?></div>
	<div class="edit_bottom">
    	<input type="button" name="submitBtn" id="submitBtn" class="formbutton" value="<?=$kis_lang['submit']?>" />
		<input type="button" name="cancelBtn" id="cancelBtn" class="formsubbutton" value="<?=$kis_lang['cancel']?>" />	    
    	<input type="hidden" name="classLevelID" id="classLevelID" value="<?=$classLevelID?>" />
    </div>
    <p class="spacer"></p><br>  
</form>
</div>
