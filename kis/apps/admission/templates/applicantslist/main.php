<script>
    $(function () {
        kis.admission.application_init();
    });
</script>
<div class="main_content_detail">
    <p class="spacer"></p>
    <? if (!empty($warning_msg)): ?>
        <?= $warning_msg ?>
    <? endif; ?>
    <form class="filter_form">
        <div id="table_filter">
            <?= $schoolYearSelection ?>
        </div>
        <div class="search">
            <input placeholder="<?= $kis_lang['search'] ?>" name="keyword" value="<?= $keyword ?>" type="text"/>
        </div>
    </form>

    <!---->

    <p class="spacer"></p>
    <div class="table_board">
        <table class="common_table_list edit_table_list">
            <colgroup>
                <col nowrap="nowrap">
            </colgroup>
            <thead>
            <tr>
                <th width="20">&nbsp;</th>
                <th><?= $kis_lang['form'] ?></th>
                <th><?= $kis_lang['from'] ?> </th>
                <th><?= $kis_lang['to'] ?></th>
                <th><?= $sys_custom['KIS_Admission']['MGF']['Settings'] ? $kis_lang['Admission']['Status']['mosgraceful_caninterview'] : ($admission_cfg['StatusDisplayOnTable'] ? $kis_lang['Admission']['Status'][$admission_cfg['StatusDisplayOnTable'][1]] : $kis_lang['Admission']['Status']['pending']) ?></th>
                <th><?= $sys_custom['KIS_Admission']['MGF']['Settings'] ? $kis_lang['Admission']['Status']['interviewed'] : ($admission_cfg['StatusDisplayOnTable'] ? $kis_lang['Admission']['Status'][$admission_cfg['StatusDisplayOnTable'][2]] : $kis_lang['Admission']['Status']['paymentsettled']) ?></th>
                <th><?= $sys_custom['KIS_Admission']['MGF']['Settings'] ? $kis_lang['Admission']['Status']['admitted'] : ($admission_cfg['StatusDisplayOnTable'] ? $kis_lang['Admission']['Status'][$admission_cfg['StatusDisplayOnTable'][3]] : $kis_lang['Admission']['Status']['confirmed']) ?></th>
                <th><?= $kis_lang['totalnoofapplicants'] ?></th>
            </tr>
            </thead>
            <tbody>
            <?
            $idx = 0;
            foreach ($applicationlistAry as $_classLevelId => $_classLevelRecord):
                $idx++;
                ?>
                <tr>
                    <td><?= $idx ?></td>
                    <td>
                        <a href="#/apps/admission/applicantslist/listbyform/<?= $_classLevelId ?>/"><?= $_classLevelRecord['ClassLevelName'] ?></a>
                    </td>
                    <td><?= kis_ui::displayTableField($_classLevelRecord['StartDate']) ?></td>
                    <td><?= kis_ui::displayTableField($_classLevelRecord['EndDate']) ?></td>
                    <td>
                        <div align="center"><a
                                    href="#/apps/admission/applicantslist/listbyform/<?= $_classLevelId ?>/?selectSchoolYearID=<?= $schoolYearID ?>&selectStatus=<?= $sys_custom['KIS_Admission']['MGF']['Settings'] ? $StatusAry['mosgraceful_caninterview'] : ($admission_cfg['StatusDisplayOnTable'] ? $StatusAry[$admission_cfg['StatusDisplayOnTable'][1]] : $StatusAry['pending']) ?>"><?= $sys_custom['KIS_Admission']['MGF']['Settings'] ? $_classLevelRecord['Status']['mosgraceful_caninterview'] : ($admission_cfg['StatusDisplayOnTable'] ? $_classLevelRecord['Status'][$admission_cfg['StatusDisplayOnTable'][1]] : $_classLevelRecord['Status']['pending']) ?></a>
                        </div>
                    </td>
                    <td>
                        <div align="center"><a
                                    href="#/apps/admission/applicantslist/listbyform/<?= $_classLevelId ?>/?selectSchoolYearID=<?= $schoolYearID ?>&selectStatus=<?= $sys_custom['KIS_Admission']['MGF']['Settings'] ? $StatusAry['interviewed'] : ($admission_cfg['StatusDisplayOnTable'] ? $StatusAry[$admission_cfg['StatusDisplayOnTable'][2]] : $StatusAry['paymentsettled']) ?>"><?= $sys_custom['KIS_Admission']['MGF']['Settings'] ? $_classLevelRecord['Status']['interviewed'] : ($admission_cfg['StatusDisplayOnTable'] ? $_classLevelRecord['Status'][$admission_cfg['StatusDisplayOnTable'][2]] : $_classLevelRecord['Status']['paymentsettled']) ?></a>
                        </div>
                    </td>
                    <td>
                        <div align="center" class="style1"><a
                                    href="#/apps/admission/applicantslist/listbyform/<?= $_classLevelId ?>/?selectSchoolYearID=<?= $schoolYearID ?>&selectStatus=<?= $sys_custom['KIS_Admission']['MGF']['Settings'] ? $StatusAry['admitted'] : ($admission_cfg['StatusDisplayOnTable'] ? $StatusAry[$admission_cfg['StatusDisplayOnTable'][3]] : $StatusAry['confirmed']) ?>"><?= $sys_custom['KIS_Admission']['MGF']['Settings'] ? $_classLevelRecord['Status']['admitted'] : ($admission_cfg['StatusDisplayOnTable'] ? $_classLevelRecord['Status'][$admission_cfg['StatusDisplayOnTable'][3]] : $_classLevelRecord['Status']['confirmed']) ?></a>
                        </div>
                    </td>
                    <td>
                        <div align="center" class="style1"><?= $_classLevelRecord['TotalApplicant'] ?></div>
                    </td>
                </tr>
            <? endforeach; ?>
            </tbody>
        </table>
        <p class="spacer"></p>
        <p class="spacer"></p>
        <br>
    </div>
</div>