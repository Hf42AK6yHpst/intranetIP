<?php

global $libkis_admission;

######## Get Parent Cust Info START ########
//$applicationCustInfo = $libkis_admission->getAllApplicationCustInfo($applicationInfo['applicationID']);
######## Get Parent Cust Info END ########
?>
<table class="form_table">
	<colgroup>
        <col style="width:30%">
        <col style="width:35%">
        <col style="width:35%">
    </colgroup>
	<tr>
		<td>&nbsp;</td>
		<td class="form_guardian_head"><center><?=$kis_lang['Admission']['HKUGAPS']['father'] ?></center></td>
		<td class="form_guardian_head"><center><?=$kis_lang['Admission']['HKUGAPS']['mother'] ?></center></td>
	</tr>
	<tr>
		<td class="field_title">
    		<?=$kis_lang['Admission']['chinesename'] ?>
		</td>
		<td class="form_guardian_field">
			<?= kis_ui::displayTableField($applicationInfo['F']['ChineseName']) ?>
    	</td>
		<td class="form_guardian_field">
			<?= kis_ui::displayTableField($applicationInfo['M']['ChineseName']) ?>
		</td>
	</tr>
	<tr>
		<td class="field_title">
    		<?=$kis_lang['Admission']['englishname'] ?>
		</td>
		<td class="form_guardian_field">
			<?= kis_ui::displayTableField($applicationInfo['F']['EnglishName']) ?>	
		</td>
		<td class="form_guardian_field">
			<?= kis_ui::displayTableField($applicationInfo['M']['EnglishName']) ?>
		</td>
	</tr>
	
	<tr>
		<td class="field_title">
    		<?=$kis_lang['Admission']['religion']?>
		</td>
		<td class="form_guardian_field">
			<?= kis_ui::displayTableField($applicationInfo['F']['Company']) ?>
		</td>
		<td class="form_guardian_field">
    		<?= kis_ui::displayTableField($applicationInfo['M']['Company']) ?>
    	</td>
	</tr>
	
	<tr>
		<td class="field_title">
    		<?=$kis_lang['Admission']['occupation']?>
		</td>
		<td class="form_guardian_field">
			<?= kis_ui::displayTableField($applicationInfo['F']['JobTitle']) ?>
		</td>
		<td class="form_guardian_field">
    		<?= kis_ui::displayTableField($applicationInfo['M']['JobTitle']) ?>
    	</td>
	</tr>
	
	<tr>
		<td class="field_title">
    		<?=$kis_lang['Admission']['SHCK']['OfficeAddress']?>
		</td>
		<td class="form_guardian_field">
			<?= kis_ui::displayTableField(nl2br($applicationInfo['F']['OfficeAddress'])) ?>
		</td>
		<td class="form_guardian_field">
    		<?= kis_ui::displayTableField(nl2br($applicationInfo['M']['OfficeAddress'])) ?>
    	</td>
	</tr>
	
	<tr>
		<td class="field_title">
    		<?=$kis_lang['Admission']['SHCK']['OfficeTel']?>
		</td>
		<td class="form_guardian_field">
			<?= kis_ui::displayTableField($applicationInfo['F']['OfficeTelNo']) ?>
		</td>
		<td class="form_guardian_field">
    		<?= kis_ui::displayTableField($applicationInfo['M']['OfficeTelNo']) ?>
    	</td>
	</tr>
	
	<tr>
		<td class="field_title">
    		<?=$kis_lang['Admission']['SHCK']['MobileTel']?>
		</td>
		<td class="form_guardian_field">
			<?= kis_ui::displayTableField($applicationInfo['F']['Mobile']) ?>
		</td>
		<td class="form_guardian_field">
    		<?= kis_ui::displayTableField($applicationInfo['M']['Mobile']) ?>
    	</td>
	</tr>
</table>