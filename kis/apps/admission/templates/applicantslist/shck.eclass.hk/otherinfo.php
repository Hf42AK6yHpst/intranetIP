<?php

global $libkis_admission;

#### Get Student Cust Info START ####
$applicationCustInfo = $libkis_admission->getAllApplicationCustInfo($applicationInfo['applicationID']);
#### Get Student Cust Info START ####


?>

<table id="dataTable1" class="form_table" >
<colgroup>
    <col style="width:31%">
    <col style="width:1%">
    <col style="width:17%">
    <col style="width:17%">
    <col style="width:17%">
    <col style="width:17%">
</colgroup>

<tbody>
    <tr>
    	<td class="field_title" rowspan="5" >
    		<?=$kis_lang['Admission']['SHCK']['SibilingsStudyingInThisSchool']?>
    	</td>
    	<td colspan="3">
    		<?php
    		    if($applicationCustInfo['SibilingsStudyingInThisSchool'][0]['Value']){
    		        echo $kis_lang['Admission']['yes'];
    		    }else{
    		        echo $kis_lang['Admission']['no'];
    		    }?>
    	</td>
    </tr>
    <?php if($applicationCustInfo['SibilingsStudyingInThisSchool'][0]['Value']){ ?>
    	<tr>
    		<td>&nbsp;</td>
    		<td class="form_guardian_head"><center><?=$kis_lang['Admission']['name'] ?></center></td>
    		<td class="form_guardian_head"><center><?=$kis_lang['Admission']['SHCK']['Relationship'] ?></center></td>
    		<td class="form_guardian_head" colspan="2"><center><?=$kis_lang['Admission']['class']?></center></td>
    	</tr>
    	<?php for($i=0;$i<3;$i++){ ?>
        	<tr>
        		<td class="field_title" style="text-align:right;width:30px;">(<?=$i+1 ?>)</td>
        		<td class="form_guardian_field">
        			<?=kis_ui::displayTableField($applicationCustInfo['SibilingsStudyingInThisSchool_Name'][$i]['Value']) ?>
        		</td>
        		<td class="form_guardian_field">
        			<?=kis_ui::displayTableField($applicationCustInfo['SibilingsStudyingInThisSchool_Relationship'][$i]['Value']) ?>
        		</td>
        		<td class="form_guardian_field" colspan="2">
        			<?=kis_ui::displayTableField($applicationCustInfo['SibilingsStudyingInThisSchool_Class'][$i]['Value']) ?>
        		</td>
        	</tr>
		<?php } ?>
	<?php } ?>
</tbody>

<tbody>
    <tr>
    	<td class="field_title" rowspan="5" >
    		<?=$kis_lang['Admission']['SHCK']['FormerStudentInThisSchool']?>
    	</td>
    	<td colspan="3">
    		<?php
    		    if($applicationCustInfo['FormerStudentInThisSchool'][0]['Value']){
    		        echo $kis_lang['Admission']['yes'];
    		    }else{
    		        echo $kis_lang['Admission']['no'];
    		    }?>
    	</td>
    </tr>
    <?php if($applicationCustInfo['FormerStudentInThisSchool'][0]['Value']){ ?>
    	<tr>
    		<td>&nbsp;</td>
    		<td class="form_guardian_head"><center><?=$kis_lang['Admission']['name'] ?></center></td>
    		<td class="form_guardian_head"><center><?=$kis_lang['Admission']['SHCK']['Relationship'] ?></center></td>
    		<td class="form_guardian_head" colspan="2"><center><?=$kis_lang['Admission']['SHCK']['YearOfGraduation']?></center></td>
    	</tr>
    	<?php for($i=0;$i<3;$i++){ ?>
        	<tr>
        		<td class="field_title" style="text-align:right;width:30px;">(<?=$i+1 ?>)</td>
        		<td class="form_guardian_field">
        			<?=kis_ui::displayTableField($applicationCustInfo['FormerStudentInThisSchool_Name'][$i]['Value']) ?>
        		</td>
        		<td class="form_guardian_field">
        			<?=kis_ui::displayTableField($applicationCustInfo['FormerStudentInThisSchool_Relationship'][$i]['Value']) ?>
        		</td>
        		<td class="form_guardian_field" colspan="2">
        			<?=kis_ui::displayTableField($applicationCustInfo['FormerStudentInThisSchool_YearOfGraduation'][$i]['Value']) ?>
        		</td>
        	</tr>
		<?php } ?>
	<?php } ?>
</tbody>

<tbody>
    <tr>
    	<td class="field_title" rowspan="5" >
    		<?=$kis_lang['Admission']['SHCK']['SibilingsStudyingInOtherCanossianSchool']?>
    	</td>
    	<td colspan="3">
    		<?php
    		    if($applicationCustInfo['SibilingsStudyingInOtherCanossianSchool'][0]['Value']){
    		        echo $kis_lang['Admission']['yes'];
    		    }else{
    		        echo $kis_lang['Admission']['no'];
    		    }?>
    	</td>
    </tr>
    <?php if($applicationCustInfo['SibilingsStudyingInOtherCanossianSchool'][0]['Value']){ ?>
    	<tr>
    		<td>&nbsp;</td>
    		<td class="form_guardian_head"><center><?=$kis_lang['Admission']['name'] ?></center></td>
    		<td class="form_guardian_head"><center><?=$kis_lang['Admission']['SHCK']['Relationship'] ?></center></td>
    		<td class="form_guardian_head"><center><?=$kis_lang['Admission']['class']?></center></td>
    		<td class="form_guardian_head"><center><?=$kis_lang['Admission']['SHCK']['NameOfSchool']?></center></td>
    	</tr>
    	<?php for($i=0;$i<3;$i++){ ?>
        	<tr>
        		<td class="field_title" style="text-align:right;width:30px;">(<?=$i+1 ?>)</td>
        		<td class="form_guardian_field">
        			<?=kis_ui::displayTableField($applicationCustInfo['SibilingsStudyingInOtherCanossianSchool_Name'][$i]['Value']) ?>
        		</td>
        		<td class="form_guardian_field">
        			<?=kis_ui::displayTableField($applicationCustInfo['SibilingsStudyingInOtherCanossianSchool_Relationship'][$i]['Value']) ?>
        		</td>
        		<td class="form_guardian_field">
        			<?=kis_ui::displayTableField($applicationCustInfo['SibilingsStudyingInOtherCanossianSchool_Class'][$i]['Value']) ?>
        		</td>
        		<td class="form_guardian_field">
        			<?=kis_ui::displayTableField($applicationCustInfo['SibilingsStudyingInOtherCanossianSchool_NameOfSchool'][$i]['Value']) ?>
        		</td>
        	</tr>
		<?php } ?>
	<?php } ?>
</tbody>

<tbody>
    <tr>
    	<td class="field_title" rowspan="5" >
    		<?=$kis_lang['Admission']['SHCK']['FormerStudentInOtherCanossianSchool']?>
    	</td>
    	<td colspan="3">
    		<?php
    		    if($applicationCustInfo['FormerStudentInOtherCanossianSchool'][0]['Value']){
    		        echo $kis_lang['Admission']['yes'];
    		    }else{
    		        echo $kis_lang['Admission']['no'];
    		    }?>
    	</td>
    </tr>
    <?php if($applicationCustInfo['FormerStudentInOtherCanossianSchool'][0]['Value']){ ?>
    	<tr>
    		<td>&nbsp;</td>
    		<td class="form_guardian_head"><center><?=$kis_lang['Admission']['name'] ?></center></td>
    		<td class="form_guardian_head"><center><?=$kis_lang['Admission']['SHCK']['Relationship'] ?></center></td>
    		<td class="form_guardian_head"><center><?=$kis_lang['Admission']['SHCK']['YearOfGraduation']?></center></td>
    		<td class="form_guardian_head"><center><?=$kis_lang['Admission']['SHCK']['NameOfSchool']?></center></td>
    	</tr>
    	<?php for($i=0;$i<3;$i++){ ?>
        	<tr>
        		<td class="field_title" style="text-align:right;width:30px;">(<?=$i+1 ?>)</td>
        		<td class="form_guardian_field">
        			<?=kis_ui::displayTableField($applicationCustInfo['FormerStudentInOtherCanossianSchool_Name'][$i]['Value']) ?>
        		</td>
        		<td class="form_guardian_field">
        			<?=kis_ui::displayTableField($applicationCustInfo['FormerStudentInOtherCanossianSchool_Relationship'][$i]['Value']) ?>
        		</td>
        		<td class="form_guardian_field">
        			<?=kis_ui::displayTableField($applicationCustInfo['FormerStudentInOtherCanossianSchool_YearOfGraduation'][$i]['Value']) ?>
        		</td>
        		<td class="form_guardian_field">
        			<?=kis_ui::displayTableField($applicationCustInfo['FormerStudentInOtherCanossianSchool_NameOfSchool'][$i]['Value']) ?>
        		</td>
        	</tr>
		<?php } ?>
	<?php } ?>
</tbody>

<tbody>
    <tr>
    	<td class="field_title" rowspan="5" >
    		<?=$kis_lang['Admission']['SHCK']['EmployeeOfCanossianInstitutions']?>
    	</td>
    	<td colspan="3">
    		<?php
    		    if($applicationCustInfo['EmployeeOfCanossianInstitutions'][0]['Value']){
    		        echo $kis_lang['Admission']['yes'];
    		    }else{
    		        echo $kis_lang['Admission']['no'];
    		    }?>
    	</td>
    </tr>
    <?php if($applicationCustInfo['EmployeeOfCanossianInstitutions'][0]['Value']){ ?>
    	<tr>
    		<td>&nbsp;</td>
    		<td class="form_guardian_head"><center><?=$kis_lang['Admission']['name'] ?></center></td>
    		<td class="form_guardian_head"><center><?=$kis_lang['Admission']['SHCK']['Relationship'] ?></center></td>
    		<td class="form_guardian_head" colspan="2"><center><?=$kis_lang['Admission']['SHCK']['NameOfOurInstitution']?></center></td>
    	</tr>
    	<?php for($i=0;$i<3;$i++){ ?>
        	<tr>
        		<td class="field_title" style="text-align:right;width:30px;">(<?=$i+1 ?>)</td>
        		<td class="form_guardian_field">
        			<?=kis_ui::displayTableField($applicationCustInfo['EmployeeOfCanossianInstitutions_Name'][$i]['Value']) ?>
        		</td>
        		<td class="form_guardian_field">
        			<?=kis_ui::displayTableField($applicationCustInfo['EmployeeOfCanossianInstitutions_Relationship'][$i]['Value']) ?>
        		</td>
        		<td class="form_guardian_field" colspan="2">
        			<?=kis_ui::displayTableField($applicationCustInfo['EmployeeOfCanossianInstitutions_NameOfOurInstitution'][$i]['Value']) ?>
        		</td>
        	</tr>
		<?php } ?>
	<?php } ?>
</tbody>

</table>
