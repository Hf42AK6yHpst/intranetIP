<?php 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT.'kis/init.php');
include_once($PATH_WRT_ROOT."kis/config.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/admission/".$setting_path_ip_rel."/config.php");

$libinterface = new interface_html();
$libkis_admission = $libkis->loadApp('admission');
$applicationInfo = current($libkis_admission->getApplicationStudentInfo($schoolYearID,$classLevelID='',$applicationID='',$status='',$_GET['recordID']));

######## Get Student Cust Info START ########
$applicationCustInfo = $libkis_admission->getAllApplicationCustInfo($applicationInfo['applicationID']);
$applicationInfo['studentApplicationInfoCust'] = $libkis_admission->getApplicationStudentInfoCust($kis_data['schoolYearID'],$classLevelID='',$applicationID='',$kis_data['recordID']);

#### BD Reference START ####
$bdReference = $applicationCustInfo['BD_Ref_Num'][0]['Value'];
#### BD Reference END ####

#### Last School START ####
$lastSchool = $applicationInfo['studentApplicationInfoCust'][0]['OthersPrevSchName'];
$lastSchoolClass = $applicationInfo['studentApplicationInfoCust'][0]['OthersPrevSchClass'];
$classRepeated = $applicationCustInfo['Class_Repeated'][0]['Value'];
#### Last School END ####

#### Lang Spoken START ####
$langStr = '';
$Lang_Spoken = (array)$applicationCustInfo['Lang_Spoken'];
foreach($Lang_Spoken as $lang){
	if($lang['Value'] == 'Cantonese'){
		$langStr .= $kis_lang['Admission']['Languages']['Cantonese'];
	}else if($lang['Value'] == 'Putonghua'){
		$langStr .= $kis_lang['Admission']['Languages']['Putonghua'];
	}else if($lang['Value'] == 'English'){
		$langStr .= $kis_lang['Admission']['Languages']['English'];
	}else if($lang['Value'] == 'Others'){
		$langStr .= $applicationCustInfo['Lang_Spoken_Other'][0]['Value'];
	}
	$langStr .= '<br />';
}
// $langStr = substr($langStr, 0, strlen($langStr)-6); // Trim br
$langSpoken = $langStr;
#### Lang Spoken END ####

#### Lang Written START ####
$langStr = '';
$Lang_Written = (array)$applicationCustInfo['Lang_Written'];
foreach($Lang_Written as $lang){
	if($lang['Value'] == 'Chinese'){
		$langStr .= $kis_lang['Admission']['Languages']['Chinese'];
	}else if($lang['Value'] == 'English'){
		$langStr .= $kis_lang['Admission']['Languages']['English'];
	}else if($lang['Value'] == 'Others'){
		$langStr .= $applicationCustInfo['Lang_Written_Other'][0]['Value'];
	}
	$langStr .= '<br />';
}
$langWritten = $langStr;
#### Lang Written END ####

#### Elementary Chinese START ####
if($applicationCustInfo['Elementary_Chinese'][0]['Value']){
	$elementary = $kis_lang['Admission']['yes'];
}else{
	$elementary = $kis_lang['Admission']['no'];
}
#### Elementary Chinese END ####

#### Extra-curricular START ####
$extraCurricular = $applicationCustInfo['Extra_Curricular'][0]['Value'];
#### Extra-curricular END ####

#### Church Activities START ####
$actStr = '';
$Church_Activities = (array)$applicationCustInfo['Church_Activities'];
foreach($Church_Activities as $act){
	if($act['Value'] == 'SundayWorship'){
		$actStr .= $kis_lang['Admission']['UCCKE']['ChurchActivitesType'][0];
	}else if($act['Value'] == 'SundaySchool'){
		$actStr .= $kis_lang['Admission']['UCCKE']['ChurchActivitesType'][1];
	}else if($act['Value'] == 'Fellowship'){
		$actStr .= $kis_lang['Admission']['UCCKE']['ChurchActivitesType'][2];
	}else if($act['Value'] == 'Others'){
		$actStr .= $applicationCustInfo['Church_Activities_Other'][0]['Value'];
	}
	$actStr .= '<br />';
}
$churchActivities = $actStr;
#### Church Activities END ####
######## Get Student Cust Info END ########

?>
<link type="text/css" rel="stylesheet" media="screen" href="/templates/kis/css/common.css">
<body style="background: none !important; min-width:0;">
<table class="form_table">
	<tbody>
		<tr ><td><button onclick="window.open('/kis/#/apps/admission/applicantslist/edit/<?=$schoolYearID ?>/<?=$_GET['recordID'] ?>/studentinfo/')" class="formsubbutton" ><?=$kis_lang['edit'] ?></button></td></tr>
		<tr width="30" > 
			<td width="30%" class="field_title">
				<?= $kis_lang['Admission']['chinesename'] ?>
			</td>
			<td width="40%">
				<?=$applicationInfo['student_name_b5']?>
			</td>
		</tr>
		<tr>
			<td class="field_title">
				<?= $kis_lang['Admission']['englishname'] ?>
			</td>
			<td>
				<?=$applicationInfo['student_name_en']?>
			</td>
		</tr>
		<tr>   
			<td class="field_title">
				<?= $kis_lang['Admission']['dateofbirth'] ?>
			</td>
			<td>
				<?= kis_ui::displayTableField($applicationInfo['dateofbirth']) ?>
			</td>
		</tr>
		<tr>
			<td class="field_title">
				<?= $kis_lang['Admission']['gender'] ?>
			</td>
			<td>
				<?= kis_ui::displayTableField($kis_lang['Admission']['genderType'][$applicationInfo['gender']]) ?>
			</td>
		</tr>
		<tr>   
			<td class="field_title">
				<?= $kis_lang['Admission']['placeofbirth'] ?>
			</td>
			<td>
				<?= kis_ui::displayTableField($applicationInfo['placeofbirth']) ?>
			</td>
		</tr>
		<tr>
			<td class="field_title">
				<?= $kis_lang['Admission']['TSUENWANBCKG']['nationality'] ?>
			</td>
			<td>
				<?= kis_ui::displayTableField($applicationInfo['county']) ?>
			</td>
		</tr>
		<tr>
			<td class="field_title">
				<?= $kis_lang['Admission']['UCCKE']['HKID'] ?>
			</td>
			<td>
				<?= kis_ui::displayTableField($applicationInfo['birthcertno']) ?>
			</td>
		</tr>
		<tr>
			<td class="field_title">
				<?= $kis_lang['Admission']['UCCKE']['BdRefNo'] ?>
			</td>
			<td colspan="2">
				STRN: <?= kis_ui::displayTableField($bdReference) ?>
			</td>
		</tr>                
		<tr>
			<td class="field_title">
				<?= $kis_lang['Admission']['UCCKE']['CurrentStudySchool'] ?>
			</td>
			<td colspan="2">
				<?= kis_ui::displayTableField($lastSchool) ?>
			</td>
		</tr>
		<tr>
			<td class="field_title">
				<?= $kis_lang['Admission']['UCCKE']['ClassLastAttended'] ?>
			</td>
			<td colspan="2">
				<?= kis_ui::displayTableField($lastSchoolClass) ?>
			</td>
		</tr>
		<tr>   
			<td class="field_title">
				<?= $kis_lang['Admission']['UCCKE']['ClassRepeated'] ?>
			</td>
			<td colspan="2">
				<?= kis_ui::displayTableField($classRepeated) ?>
			</td>
		</tr>          
		<tr>
			<td class="field_title">
				<?= $kis_lang['Admission']['UCCKE']['EngAddress'] ?>
			</td>
			<td colspan="2">
				<?= kis_ui::displayTableField($applicationInfo['homeaddress']) ?>
			</td>
		</tr>
		<tr>   
			<td class="field_title">
				<?= $kis_lang['Admission']['UCCKE']['ChiAddress'] ?>
			</td>
			<td colspan="2">
				<?= kis_ui::displayTableField($applicationInfo['homeaddresschi']) ?>
			</td>
		</tr>
		<tr>   
			<td class="field_title">
				<?= $kis_lang['Admission']['UCCKE']['LangSpoken'] ?>
			</td>
			<td colspan="2">
				<?= kis_ui::displayTableField($langSpoken) ?>
			</td>
		</tr>
		<tr>   
			<td class="field_title">
				<?= $kis_lang['Admission']['UCCKE']['LangWritten'] ?>
			</td>
			<td colspan="2">
				<?= kis_ui::displayTableField($langWritten) ?>
			</td>
		</tr>
		<tr>   
			<td class="field_title">
				<?= $kis_lang['Admission']['UCCKE']['ElementryChinese'] ?>
			</td>
			<td colspan="2">
				<?= kis_ui::displayTableField($elementary) ?>
			</td>
		</tr>
		<tr>   
			<td class="field_title">
				<?= $kis_lang['Admission']['UCCKE']['ExtraCurricular'] ?>
			</td>
			<td colspan="2">
				<?= kis_ui::displayTableField($extraCurricular) ?>
			</td>
		</tr>
		<tr>   
			<td class="field_title">
				<?= $kis_lang['Admission']['UCCKE']['NameOfChurch'] ?>
			</td>
			<td colspan="2">
				<?= kis_ui::displayTableField($applicationInfo['religion']) ?>
			</td>
		</tr>
		<tr>   
			<td class="field_title">
				<?= $kis_lang['Admission']['UCCKE']['ChurchActivites'] ?>
			</td>
			<td colspan="2">
				<?= kis_ui::displayTableField($churchActivities) ?>
			</td>
		</tr>                                                                             
	</tbody>
</table>
</body>