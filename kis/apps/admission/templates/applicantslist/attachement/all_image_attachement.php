<?php 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT.'kis/init.php');
include_once($PATH_WRT_ROOT."kis/config.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/admission/".$setting_path_ip_rel."/config.php");

$libkis_admission = $libkis->loadApp('admission');

$sql = "Select 
    r.ApplicationID, 
    r.AttachmentType, 
    CONCAT(o.RecordID,'/',IF(r.AttachmentType = 'personal_photo',r.AttachmentType,'other_files'),'/',r.AttachmentName), 
    o.ApplyYear schoolYearID, 
    o.RecordID RecordID
From 
    ADMISSION_ATTACHMENT_RECORD r
INNER JOIN
	ADMISSION_OTHERS_INFO o
ON 
    r.ApplicationID = o.ApplicationID
where 
    o.RecordID = '{$_GET['recordID']}'
ORDER BY 
    r.DateInput desc
";
$rs = $libkis->returnArray($sql);
$attachmentAry = BuildMultiKeyAssoc($rs, array('AttachmentType'));

if(count($attachmentAry) == 0){
    echo $kis_lang['noattachement'];
    exit;
}else{
    
    
    
$rs = $libkis_admission->getAttachmentSettings();
$attachmentSettingAry = array('personal_photo');
foreach($rs as $r){
    $attachmentSettingAry[] = $r['AttachmentName'];
}


?>

<?php
$image_exts = array("gif","jpg","jpe","jpeg","png","bmp");
foreach($attachmentSettingAry as $attachmentSetting){
    $attachmentName = ($attachmentSetting == 'personal_photo')? $kis_lang['Admission']['personalPhoto'] : $attachmentSetting;

    $attachment = $attachmentAry[$attachmentSetting];
    $filetype = explode('.',$attachment[2]);
    $filetype = $filetype[1];
    
    $isImage = in_array(strtolower($filetype), $image_exts);
    
    $attachmentPath = '';
    if(isset($filetype) && $filetype !=''){
        $attachmentPath = $admission_cfg['FilePath'].$attachment[2];
    }
?>
	<h3 style="margin: 0"><?=$attachmentName ?></h3>

	<?php 
	if($attachmentPath && $isImage){ 
	?>
		<a href="<?=$attachmentPath ?>" target="_blank"><img src="<?=$attachmentPath ?>" style="max-width: 500px;" /></a>
	<?php 
	}else if($attachmentPath && !$isImage){
	?>
		<iframe src="<?=$attachmentPath ?>" style="width:80%;height: 99%;border:none;" />
	<?php
	}else{ 
	    echo $kis_lang['noattachement'];
	}
	?>
	<hr />
<?php
} // End foreach($attachmentSettingAry as $attachmentSetting)
?>





<?php
} // End if(count($attachmentAry) == 0)









