<?php

global $libkis_admission;

######## Get Parent Cust Info START ########
$applicationCustInfo = $libkis_admission->getAllApplicationCustInfo($applicationInfo['applicationID']);

######## Get Parent Cust Info END ########
?>
<input type="hidden" name="ApplicationID" value="<?=$applicationInfo['applicationID']?>" />
<table class="form_table">
	<colgroup>
        <col style="width:30%">
        <col style="width:35%">
        <col style="width:35%">
    </colgroup>
	<tr>
		<td>&nbsp;</td>
		<td class="form_guardian_head"><center><?=$kis_lang['Admission']['HKUGAPS']['father'] ?></center></td>
		<td class="form_guardian_head"><center><?=$kis_lang['Admission']['HKUGAPS']['mother'] ?></center></td>
	</tr>
	<tr>
		<td class="field_title">
    		<?=$kis_lang['Admission']['HKUGAPS']['nameChi'] ?>
		</td>
		<td class="form_guardian_field">
    			<input name="G1ChineseName" type="text" id="G1ChineseName" class="textboxtext" value="<?=$applicationInfo['F']['chinesename'] ?>"/>
		</td>
		<td class="form_guardian_field">
    			<input name="G2ChineseName" type="text" id="G2ChineseName" class="textboxtext" value="<?=$applicationInfo['M']['chinesename'] ?>"/>
		</td>
	</tr>
	<tr>
		<td class="field_title">
    		<?=$kis_lang['Admission']['HKUGAPS']['nameEng'] ?>
		</td>
		<td class="form_guardian_field">
    			<input name="G1EnglishName" type="text" id="G1EnglishName" class="textboxtext" value="<?=$applicationInfo['F']['englishname'] ?>"/>
		</td>
		<td class="form_guardian_field">
    			<input name="G2EnglishName" type="text" id="G2EnglishName" class="textboxtext" value="<?=$applicationInfo['M']['englishname'] ?>"/>
		</td>
	</tr>
	<tr>
		<td class="field_title">
    		<?=$kis_lang['Admission']['HKUGAPS']['contactNum']?>
		</td>
		<td class="form_guardian_field">
    			<input name="G1MobileNo" type="text" id="G1MobileNo" class="textboxtext" value="<?=$applicationInfo['F']['mobile'] ?>"/>
		</td>
		<td class="form_guardian_field">
    			<input name="G2MobileNo" type="text" id="G2MobileNo" class="textboxtext" value="<?=$applicationInfo['M']['mobile'] ?>"/>
		</td>
	</tr>
	<tr>
		<td class="field_title">
    		<?=$kis_lang['Admission']['HKUGAPS']['email']?>
		</td>
		<td class="form_guardian_field">
    			<input name="G1Email" type="text" id="G1Email" class="textboxtext" value="<?=$applicationInfo['F']['email'] ?>"/>
		</td>
		<td class="form_guardian_field">
    			<input name="G2Email" type="text" id="G2Email" class="textboxtext" value="<?=$applicationInfo['M']['email'] ?>"/>
		</td>
	</tr>
	<tr>
		<td class="field_title">
    		<?=$kis_lang['Admission']['occupation']?>
		</td>
		<td class="form_guardian_field">
    			<input name="G1Occupation" type="text" id="G1Occupation" class="textboxtext" value="<?=$applicationInfo['F']['occupation'] ?>"/>
		</td>
		<td class="form_guardian_field">
    			<input name="G2Occupation" type="text" id="G2Occupation" class="textboxtext" value="<?=$applicationInfo['M']['occupation'] ?>"/>
		</td>
	</tr>
	<tr>
		<td class="field_title">
    		<?=$kis_lang['Admission']['HKUGAPS']['nameInstitution']?>
		</td>
		<td class="form_guardian_field">
    			<input name="G1Institution" type="text" id="G1Institution" class="textboxtext" value="<?=$applicationInfo['F']['companyname'] ?>"/>
		</td>
		<td class="form_guardian_field">
    			<input name="G2Institution" type="text" id="G2Institution" class="textboxtext" value="<?=$applicationInfo['M']['companyname'] ?>"/>
		</td>
	</tr>
</table>

<table class="form_table" >
<tbody>
<tr>
	<td class="field_title" style="width:30%">
		<?=$kis_lang['Admission']['HKUGAPS']['guardianName']?>
	</td>
	<td style="width:30%">
		<input name="G3Name" type="text" id="G3Name" class="textboxtext" value="<?=$applicationInfo['G']['chinesename']?>"/>
	</td>
	<td style="width:40%"></td>
</tr>
<tr>
	<td class="field_title" style="width:30%">
		<?=$kis_lang['Admission']['HKUGAPS']['guardianReleation']?>
	</td>
	<td>
		<input name="G3Relationship" type="text" id="G3Relationship" class="textboxtext"  value="<?=$applicationInfo['G']['relationship']?>"/>
	</td>

</tr>
</tbody>
</table>
<script>
$('#applicant_form').unbind('submit').submit(function(e){
	var schoolYearId = $('#schoolYearId').val();
	var recordID = $('#recordID').val();
	var display = $('#display').val();
	var timeSlot = lang.timeslot.split(',');
	if(checkValidForm()){
		$.post('apps/admission/ajax.php?action=updateApplicationInfo', $(this).serialize(), function(success){
			$.address.value('/apps/admission/applicantslist/details/'+schoolYearId+'/'+recordID+'/'+display+'&sysMsg='+success);
		});
	}
	return false;
});

function checkIsChineseCharacter(str){
	return str.match(/^[\u3400-\u9FBF]*$/);
}

function checkIsEnglishCharacter(str){
	return str.match(/^[A-Za-z ,\-]*$/);
}

function checkNaNull(str){
	return (
    	($.trim(str).toLowerCase()=='沒有') || 
    	($.trim(str).toLowerCase()=='nil') ||
    	($.trim(str).toLowerCase()=='n.a.')
	);
}

function checkValidForm(){
	var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	/******** Parent Info START ********/
	/**** Name START ****/
	if($.trim(applicant_form.G1ChineseName.value)==''){
		alert("<?=$kis_lang['Admission']['HKUGAPS']['msg']['enterParentName']?>");
		applicant_form.G1ChineseName.focus();
		return false;
	}
	if(
		!checkNaNull(applicant_form.G1ChineseName.value) && 
		!checkIsChineseCharacter(applicant_form.G1ChineseName.value)
	){
		alert("<?=$kis_lang['Admission']['msg']['enterchinesecharacter']?>");	
		applicant_form.G1ChineseName.focus();
		return false;
	}
	
	if($.trim(applicant_form.G2ChineseName.value)==''){
		alert("<?=$kis_lang['Admission']['HKUGAPS']['msg']['enterParentName']?>");
		applicant_form.G2ChineseName.focus();
		return false;
	}
	if(
		!checkNaNull(applicant_form.G2ChineseName.value) &&
		!checkIsChineseCharacter(applicant_form.G2ChineseName.value)
	){
		alert("<?=$kis_lang['Admission']['msg']['enterchinesecharacter']?>");	
		applicant_form.G2ChineseName.focus();
		return false;
	}

	if($.trim(applicant_form.G1EnglishName.value)==''){
		alert("<?=$kis_lang['Admission']['HKUGAPS']['msg']['enterParentName']?>");
		applicant_form.G1EnglishName.focus();
		return false;
	}
	if(
		!checkNaNull(applicant_form.G1EnglishName.value) &&
		!checkIsEnglishCharacter(applicant_form.G1EnglishName.value)
	){
		alert("<?=$kis_lang['Admission']['msg']['enterenglishcharacter']?>");	
		applicant_form.G1EnglishName.focus();
		return false;
	}

	if($.trim(applicant_form.G2EnglishName.value)==''){
		alert("<?=$kis_lang['Admission']['HKUGAPS']['msg']['enterParentName']?>");
		applicant_form.G2EnglishName.focus();
		return false;
	}
	if(
		!checkNaNull(applicant_form.G2EnglishName.value) &&
		!checkIsEnglishCharacter(applicant_form.G2EnglishName.value)
	){
		alert("<?=$kis_lang['Admission']['msg']['enterenglishcharacter']?>");	
		applicant_form.G2EnglishName.focus();
		return false;
	}
	/**** Name END ****/
	
	/**** Contact Number START ****/
	/*if($.trim(applicant_form.G1MobileNo.value)==''){
		alert("<?=$kis_lang['Admission']['HKUGAPS']['msg']['enterContactNo']?>");
		applicant_form.G1MobileNo.focus();
		return false;
	}*/
	if(
		!checkNaNull(applicant_form.G1MobileNo.value) &&
		!/^[0-9]*$/.test(applicant_form.G1MobileNo.value)
	){
		alert("<?=$kis_lang['Admission']['HKUGAPS']['msg']['invalidContactNoFormat']?>");
		applicant_form.G1MobileNo.focus();
		return false;
	}
	/*if($.trim(applicant_form.G2MobileNo.value)==''){
		alert("<?=$kis_lang['Admission']['HKUGAPS']['msg']['enterContactNo']?>");
		applicant_form.G2MobileNo.focus();
		return false;
	}*/
	if(
		!checkNaNull(applicant_form.G2MobileNo.value) &&
		!/^[0-9]*$/.test(applicant_form.G2MobileNo.value)
	){
		alert("<?=$kis_lang['Admission']['HKUGAPS']['msg']['invalidContactNoFormat']?>");
		applicant_form.G2MobileNo.focus();
		return false;
	}
	/**** Contact Number END ****/

	/**** Email START ****/
	if($.trim(applicant_form.G1Email.value)==''){
		alert("<?=$kis_lang['Admission']['icms']['msg']['entermailaddress']?>");
		applicant_form.G1Email.focus();
		return false;
	}
	if(
		applicant_form.G1Email.value != '' &&
		!checkNaNull(applicant_form.G1Email.value) &&
    	!re.test(applicant_form.G1Email.value)
	){
		alert("<?=$kis_lang['Admission']['icms']['msg']['invalidmailaddress']?>");
		applicant_form.G1Email.focus();
		return false;
	}
	
	if($.trim(applicant_form.G2Email.value)==''){
		alert("<?=$kis_lang['Admission']['icms']['msg']['entermailaddress']?>");
		applicant_form.G2Email.focus();
		return false;
	}
	if(
		applicant_form.G2Email.value != '' &&
		!checkNaNull(applicant_form.G2Email.value) &&
    	!re.test(applicant_form.G2Email.value)
	){
		alert("<?=$kis_lang['Admission']['icms']['msg']['invalidmailaddress']?>");
		applicant_form.G2Email.focus();
		return false;
	}
	/**** Email END ****/

	/**** Occupation START ****/
	if($.trim(applicant_form.G1Occupation.value)==''){
		alert("<?=$kis_lang['Admission']['msg']['enteroccupation']?>");
		applicant_form.G1Occupation.focus();
		return false;
	}
	if($.trim(applicant_form.G2Occupation.value)==''){
		alert("<?=$kis_lang['Admission']['msg']['enteroccupation']?>");
		applicant_form.G2Occupation.focus();
		return false;
	}
	/**** Occupation END ****/

	/**** Institution START ****/
	if($.trim(applicant_form.G1Institution.value)==''){
		alert("<?=$kis_lang['Admission']['HKUGAPS']['msg']['enterInstitution']?>");
		applicant_form.G1Institution.focus();
		return false;
	}
	if($.trim(applicant_form.G2Institution.value)==''){
		alert("<?=$kis_lang['Admission']['HKUGAPS']['msg']['enterInstitution']?>");
		applicant_form.G2Institution.focus();
		return false;
	}
	/**** Institution END ****/
	/******** Parent Info END ********/
	return true;
}
</script>