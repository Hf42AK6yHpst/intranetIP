<?php

global $libkis_admission;

#### Get Admission Year START ####
$admission_year = getAcademicYearByAcademicYearID($libkis_admission->getNextSchoolYearID());
$admission_year_start = substr($admission_year, 0, 4);
#### Get Admission Year END ####

#### Get Student Cust Info START ####
$applicationCustInfo = $libkis_admission->getAllApplicationCustInfo($applicationInfo['applicationID']);
#### Get Student Cust Info START ####

#### Get Relatives Info START ####
$siblingsInfo = $libkis_admission->getApplicationRelativesInfoCust($kis_data['schoolYearID'],$classLevelID='',$applicationID='',$kis_data['recordID']);
$siblingsInfoArr = array();
foreach($siblingsInfo as $siblings){
	foreach($siblings as $para=>$info){
		$siblingsInfoArr[$siblings['type']][$para] = $info;
	}
}

#### Get Relatives Info END ####
?>
<style>
select:disabled{
    color: #ccc;
}
</style>

<input type="hidden" name="ApplicationID" value="<?=$applicationInfo['applicationID']?>" />
<table class="form_table" style="font-size: 13px">
<colgroup>
	<col width="30%">
	<col width="20%">
	<col width="30%">
	<col width="20%">
</colgroup>
<tr>
	<td class="field_title">
		<?=$kis_lang['Admission']['HKUGAPS']['memberOfHKUGA']?>
	</td>
	<td colspan="3">
				<?=$libinterface->Get_Radio_Button('memberOfHKUGAY', 'memberOfHKUGA', '1', ($applicationCustInfo['HKUGA_Member'][0]['Value']=='1'), '', $kis_lang['Admission']['yes'])?>  &nbsp;&nbsp;
    			
    			<label for="memberOfHKUGAName"><?=$kis_lang['Admission']['HKUGAPS']['memberName'] ?></label>&nbsp;
    			<input name="memberOfHKUGAName" type="text" id="memberOfHKUGAName" value="<?=$applicationCustInfo['HKUGA_Member_Name'][0]['Value'] ?>"/>&nbsp;&nbsp;
    			
    			<label for="memberOfHKUGANo"><?=$kis_lang['Admission']['HKUGAPS']['memberNo'] ?></label>&nbsp;
    			<input name="memberOfHKUGANo" type="text" id="memberOfHKUGANo" value="<?=$applicationCustInfo['HKUGA_Member_No'][0]['Value'] ?>"/>

    			<br/>
    			<?=$libinterface->Get_Radio_Button('memberOfHKUGAN', 'memberOfHKUGA', '0', ($applicationCustInfo['HKUGA_Member'][0]['Value']=='0'), '', $kis_lang['Admission']['no'])?>
	</td>
</tr>

<tr>
	<td class="field_title">
		<?=$kis_lang['Admission']['HKUGAPS']['memberOfHKUGAFoundation']?>
	</td>
	<td colspan="3">
				<?=$libinterface->Get_Radio_Button('memberOfHKUGAFoundationY', 'memberOfHKUGAFoundation', '1', ($applicationCustInfo['HKUGA_Foundation_Member'][0]['Value']=='1'), '', $kis_lang['Admission']['yes'])?>  &nbsp;&nbsp;
    			
    			<label for="memberOfHKUGAFoundationName"><?=$kis_lang['Admission']['HKUGAPS']['memberName'] ?> </label>&nbsp;
    			<input name="memberOfHKUGAFoundationName" type="text" id="memberOfHKUGAFoundationName" value="<?=$applicationCustInfo['HKUGA_Foundation_Member_Name'][0]['Value'] ?>"/>&nbsp;
				<br/>
				<?=$libinterface->Get_Radio_Button('memberOfHKUGAFoundationN', 'memberOfHKUGAFoundation', '0', ($applicationCustInfo['HKUGA_Foundation_Member'][0]['Value']=='0'), '', $kis_lang['Admission']['no'])?> 
	</td>
</tr>
</table>

<table id="dataTable1" class="form_table" style="font-size: 13px">
	<tr>
		<td rowspan="3" class="field_title">
			<?=$kis_lang['Admission']['HKUGAPS']['siblingInformation'] ?>
		</td>
		<td>&nbsp;</td>
		<td class="form_guardian_head"><center><?=$kis_lang['Admission']['HKUGAPS']['nameChi'] ?></center></td>
		<td class="form_guardian_head"><center><?=$kis_lang['Admission']['HKUGAPS']['nameEng'] ?></center></td>
		<td class="form_guardian_head"><center><?=$kis_lang['Admission']['HKUGAPS']['currentClass']?></center></td>
		<td class="form_guardian_head"><center><?=$kis_lang['Admission']['HKUGAPS']['graduationYear'] ?></center></td>
	</tr>
	<tr>
		<td class="field_title" style="text-align:right;width:50px;">(1)</td>
		<td class="form_guardian_field">
    		<input name="OthersRelativeStudiedNameChi1" type="text" id="OthersRelativeStudiedNameChi1" class="textboxtext" value="<?=$siblingsInfoArr[1]['name']?>"/>

		</td>
		<td class="form_guardian_field">
			<input name="OthersRelativeStudiedNameEng1" type="text" id="OthersRelativeStudiedNameEng1" class="textboxtext" value="<?=$siblingsInfoArr[1]['englishName']?>"/>
		</td>
		<td class="form_guardian_field">
	    	<input type="radio" name="EnableOthersRelativeSchoolInfo1" value="class1" <?=($siblingsInfoArr[1]['year'] == '')? 'checked' : '' ?> />
		    <select name="OthersRelativeClassPosition1" id="OthersRelativeClassPosition1">
				<option value="" ><?=$kis_lang['Admission']['PleaseSelect'] ?></option>
    		    <?php
    		    for($i=1;$i<=6;$i++){
        		    for($j='A';$j<='D';$j++){
        		        $loopClass = $i.$j;
        		        $selected = ($loopClass == $siblingsInfoArr[1]['classposition'])? 'selected' : '';
    	        ?>
	        			<option value="<?=$loopClass ?>" <?=$selected ?>><?=$loopClass ?></option>
    	        <?php
        		    }
    		    }
    		    ?>
		    </select>
		</td>
		<td class="form_guardian_field">
	    	<input type="radio" name="EnableOthersRelativeSchoolInfo1" value="year1" <?=($siblingsInfoArr[1]['year'] != '')? 'checked' : '' ?> />
		    <select name="OthersRelativeGraduationYear1" id="OthersRelativeGraduationYear1">
				<option value="" ><?=$kis_lang['Admission']['pleaseSelect'] ?></option>
    		    <?php
    		    for($i=2006;$i<$admission_year_start;$i++){
    		        $selected = ($i == $siblingsInfoArr[1]['year'])? 'selected' : '';
    	        ?>
	        			<option value="<?=$i ?>" <?=$selected ?>><?=$i ?></option>
    	        <?php
    		    }
    		    ?>
		    </select>
		</td>
	</tr>
	<tr>
		<td class="field_title" style="text-align:right;width:50px;">(2)</td>
		<td class="form_guardian_field">
    		<input name="OthersRelativeStudiedNameChi2" type="text" id="OthersRelativeStudiedNameChi2" class="textboxtext" value="<?=$siblingsInfoArr[2]['name']?>" />
		</td>
		<td class="form_guardian_field">
			<input name="OthersRelativeStudiedNameEng2" type="text" id="OthersRelativeStudiedNameEng2" class="textboxtext" value="<?=$siblingsInfoArr[2]['englishName']?>"/>
		</td>
		<td class="form_guardian_field">
	    	<input type="radio" name="EnableOthersRelativeSchoolInfo2" value="class2" <?=($siblingsInfoArr[2]['year'] == '')? 'checked' : '' ?> />
		    <select name="OthersRelativeClassPosition2" id="OthersRelativeClassPosition2">
				<option value="" ><?=$kis_lang['Admission']['pleaseSelect'] ?></option>
    		    <?php
    		    for($i=1;$i<=6;$i++){
        		    for($j='A';$j<='D';$j++){
        		        $loopClass = $i.$j;
        		        $selected = ($loopClass == $siblingsInfoArr[2]['classposition'])? 'selected' : '';
    	        ?>
	        			<option value="<?=$loopClass ?>" <?=$selected ?>><?=$loopClass ?></option>
    	        <?php
        		    }
    		    }
    		    ?>
		    </select>
		</td>
		<td class="form_guardian_field">
	    	<input type="radio" name="EnableOthersRelativeSchoolInfo2" value="year2" <?=($siblingsInfoArr[2]['year'] != '')? 'checked' : '' ?> />
		    <select name="OthersRelativeGraduationYear2" id="OthersRelativeGraduationYear2">
				<option value="" ><?=$kis_lang['Admission']['pleaseSelect'] ?></option>
    		    <?php
    		    for($i=2006;$i<$admission_year_start;$i++){
    		        $selected = ($i == $siblingsInfoArr[2]['year'])? 'selected' : '';
    	        ?>
	        			<option value="<?=$i ?>" <?=$selected ?>><?=$i ?></option>
    	        <?php
    		    }
    		    ?>
		    </select>
		</td>
	</tr>
</table>

<table id="dataTable1" class="form_table" style="font-size: 13px">
	<colgroup>
    	<col width="30%">
    	<col width="auto">
    	<col width="13%">
    	<col width="13%">
    	<col width="10%">
    	<col width="9%">
    	<col width="20%">
    	<col width="5%">
	</colgroup>
	<tr>
		<td rowspan="3" class="field_title">
			<?=$kis_lang['Admission']['HKUGAPS']['siblingOther'] ?>
		</td>
		<td>&nbsp;</td>
		<td class="form_guardian_head"><center><?=$kis_lang['Admission']['HKUGAPS']['nameChi'] ?></center></td>
		<td class="form_guardian_head"><center><?=$kis_lang['Admission']['HKUGAPS']['nameEng'] ?></center></td>
		<td class="form_guardian_head"><center><?=$kis_lang['Admission']['dateofbirth']?></center></td>
		<td class="form_guardian_head"><center><?=$kis_lang['Admission']['HKUGAPS']['twins'] ?></center></td>
		<td class="form_guardian_head"><center><?=$kis_lang['Admission']['HKUGAPS']['school'] ?></center></td>
		<td class="form_guardian_head"><center><?=$kis_lang['Admission']['HKUGAPS']['grades'] ?></center></td>
	</tr>
	<tr>
		<td class="field_title" style="text-align:right;width:50px;">(1)</td>
		<td class="form_guardian_field">
    			<input name="OthersRelativeNameChi1" type="text" id="OthersRelativeNameChi1" class="textboxtext" value="<?=$siblingsInfoArr[3]['name']?>" />
		</td>
		<td class="form_guardian_field">
    			<input name="OthersRelativeNameEng1" type="text" id="OthersRelativeNameEng1" class="textboxtext" value="<?=$siblingsInfoArr[3]['englishName']?>" />
		</td>
		<td class="form_guardian_field">
    			<input name="OthersRelativeBirth1" type="text" id="OthersRelativeBirth1" class="textboxtext" maxlength="10" size="15" value="<?=$siblingsInfoArr[3]['dob']?>">(YYYY-MM-DD)
		</td>
		<td class="form_guardian_field">
			<?=$libinterface->Get_Radio_Button('OthersRelativeTwin1Y', 'OthersRelativeTwin1', '1', ($siblingsInfoArr[3]['istwins']=='1'), '', $kis_lang['Admission']['yes'])?>
		<br/>
    		<?=$libinterface->Get_Radio_Button('OthersRelativeTwin1N', 'OthersRelativeTwin1', '0', ($siblingsInfoArr[3]['istwins']=='0'), '', $kis_lang['Admission']['no'])?>
		</td>
		<td class="form_guardian_field">
    			<input name="OthersRelativeSchool1" type="text" id="OthersRelativeSchool1" class="textboxtext" value="<?=$siblingsInfoArr[3]['schoolName']?>">
		</td>
		<td class="form_guardian_field">
    			<input name="OthersRelativeGrade1" type="text" id="OthersRelativeGrade1" class="textboxtext" value="<?=$siblingsInfoArr[3]['classposition']?>">
		</td>
	</tr>
	<tr>
		<td class="field_title" style="text-align:right;width:50px;">(2)</td>
		<td class="form_guardian_field">
    			<input name="OthersRelativeNameChi2" type="text" id="OthersRelativeNameChi2" class="textboxtext" value="<?=$siblingsInfoArr[4]['name']?>" />
		</td>
		<td class="form_guardian_field">
    			<input name="OthersRelativeNameEng2" type="text" id="OthersRelativeNameEng2" class="textboxtext" value="<?=$siblingsInfoArr[4]['englishName']?>"/>
		</td>
		<td class="form_guardian_field">
    			<input name="OthersRelativeBirth2" type="text" id="OthersRelativeBirth2" class="textboxtext" maxlength="10" size="15" value="<?=$siblingsInfoArr[4]['dob']?>">(YYYY-MM-DD)
		</td>
		<td class="form_guardian_field">
			<?=$libinterface->Get_Radio_Button('OthersRelativeTwin2Y', 'OthersRelativeTwin2', '1', ($siblingsInfoArr[4]['istwins']=='1'), '', $kis_lang['Admission']['yes'])?>
		<br/>
    		<?=$libinterface->Get_Radio_Button('OthersRelativeTwin2N', 'OthersRelativeTwin2', '0', ($siblingsInfoArr[4]['istwins']=='0'), '', $kis_lang['Admission']['no'])?>
		</td>
		<td class="form_guardian_field">
    			<input name="OthersRelativeSchool2" type="text" id="OthersRelativeSchool2" class="textboxtext" value="<?=$siblingsInfoArr[4]['schoolName']?>">
		</td>
		<td class="form_guardian_field">
    			<input name="OthersRelativeGrade2" type="text" id="OthersRelativeGrade2" class="textboxtext" value="<?=$siblingsInfoArr[4]['classposition']?>">
		</td>
	</tr>
</table>
<!-------------- Referee END -------------->


<script>
////UI Releated START ////
$('input[name^="EnableOthersRelativeSchoolInfo"]').change(function(){
	var selected = $(this).attr('checked');
	if(selected){
		var value = $(this).val();
		var $enableItem, $disableItem;

		if(value == 'class1'){
			$enableItem = $('#OthersRelativeClassPosition1');
			$disableItem = $('#OthersRelativeGraduationYear1');
		}else if(value == 'year1'){
			$disableItem = $('#OthersRelativeClassPosition1');
			$enableItem = $('#OthersRelativeGraduationYear1');
		}else if(value == 'class2'){
			$enableItem = $('#OthersRelativeClassPosition2');
			$disableItem = $('#OthersRelativeGraduationYear2');
		}else{
			$disableItem = $('#OthersRelativeClassPosition2');
			$enableItem = $('#OthersRelativeGraduationYear2');
		}
			
		$enableItem.removeAttr('disabled');
		$disableItem.attr('disabled', 'disabled');
	}
}).change();
//// UI Releated END////

$('#applicant_form').unbind('submit').submit(function(e){
	var schoolYearId = $('#schoolYearId').val();
	var recordID = $('#recordID').val();
	var display = $('#display').val();
	var timeSlot = lang.timeslot.split(',');
	if(checkValidForm()){
		$.post('apps/admission/ajax.php?action=updateApplicationInfo', $(this).serialize(), function(success){ 
			$.address.value('/apps/admission/applicantslist/details/'+schoolYearId+'/'+recordID+'/'+display+'&sysMsg='+success);
		});
	}
	return false;
});

function checkIsChineseCharacter(str){
	return str.match(/^[\u3400-\u9FBF]*$/);
}

function checkIsEnglishCharacter(str){
	return str.match(/^[A-Za-z ]*$/);
}

function checkNaNull(str){
	return (
    	($.trim(str).toLowerCase()=='沒有') || 
    	($.trim(str).toLowerCase()=='nil') ||
    	($.trim(str).toLowerCase()=='n.a.')
	);
}

function checkValidForm(){
	/******** Other Info START ********/
	/**** HKUGA Member START ****/
	if($('#memberOfHKUGAY').attr('checked')){
		if($.trim(applicant_form.memberOfHKUGAName.value)==''){
    		alert("<?=$kis_lang['Admission']['HKUGAPS']['msg']['enterMemberInfo']?>");
    		applicant_form.memberOfHKUGAName.focus();
    		return false;
		}
		if($.trim(applicant_form.memberOfHKUGANo.value)==''){
    		alert("<?=$kis_lang['Admission']['HKUGAPS']['msg']['enterMemberInfo']?>");
    		applicant_form.memberOfHKUGANo.focus();
    		return false;
		}
	}
	/**** HKUGA Member END ****/
	
	/**** HKUGA Education Foundation START ****/
	if($('#memberOfHKUGAFoundationY').attr('checked')){
		if($.trim(applicant_form.memberOfHKUGAFoundationName.value)==''){
    		alert("<?=$kis_lang['Admission']['HKUGAPS']['msg']['enterMemberInfo']?>");
    		applicant_form.memberOfHKUGAFoundationName.focus();
    		return false;
		}
	}
	/**** HKUGA Education Foundation END ****/
	
	/**** Sibling Studying START ****/
	if(applicant_form.OthersRelativeStudiedNameChi1.value!=''){
		if(
			!checkNaNull(applicant_form.OthersRelativeStudiedNameChi1.value) && 
			!checkIsChineseCharacter(applicant_form.OthersRelativeStudiedNameChi1.value)
		){
			alert("<?=$kis_lang['Admission']['msg']['enterchinesecharacter']?>");	
			applicant_form.OthersRelativeStudiedNameChi1.focus();
			return false;
		}
		if(
			!checkNaNull(applicant_form.OthersRelativeStudiedNameEng1.value) &&
			!checkIsEnglishCharacter(applicant_form.OthersRelativeStudiedNameEng1.value)
		){
			alert("<?=$kis_lang['Admission']['msg']['enterenglishcharacter']?>");	
			applicant_form.OthersRelativeStudiedNameEng1.focus();
			return false;
		}
		
		if($.trim(applicant_form.OthersRelativeStudiedNameEng1.value)==''){
    		alert("<?=$kis_lang['Admission']['HKUGAPS']['msg']['enterSiblingInfo']?>");
    		applicant_form.OthersRelativeStudiedNameEng1.focus();
    		return false;
		}
		if(applicant_form.EnableOthersRelativeSchoolInfo1.value=='class1' && $.trim(applicant_form.OthersRelativeClassPosition1.value)==''){
    		alert("<?=$kis_lang['Admission']['HKUGAPS']['msg']['enterSiblingInfo']?>");
    		applicant_form.OthersRelativeClassPosition1.focus();
    		return false;
		}
		if(applicant_form.EnableOthersRelativeSchoolInfo1.value=='year1' && $.trim(applicant_form.OthersRelativeGraduationYear1.value)==''){
    		alert("<?=$kis_lang['Admission']['HKUGAPS']['msg']['enterSiblingInfo']?>");
    		applicant_form.OthersRelativeGraduationYear1.focus();
    		return false;
		}
	}
	if(applicant_form.OthersRelativeStudiedNameChi2.value!=''){
		if(
			!checkNaNull(applicant_form.OthersRelativeStudiedNameChi2.value) &&
			!checkIsChineseCharacter(applicant_form.OthersRelativeStudiedNameChi2.value)
		){
			alert("<?=$kis_lang['Admission']['msg']['enterchinesecharacter']?>");	
			applicant_form.OthersRelativeStudiedNameChi2.focus();
			return false;
		}
		if(
			!checkNaNull(applicant_form.OthersRelativeStudiedNameEng2.value) &&
			!checkIsEnglishCharacter(applicant_form.OthersRelativeStudiedNameEng2.value)
		){
			alert("<?=$kis_lang['Admission']['msg']['enterenglishcharacter']?>");	
			applicant_form.OthersRelativeStudiedNameEng2.focus();
			return false;
		}

		if($.trim(applicant_form.OthersRelativeStudiedNameEng2.value)==''){
    		alert("<?=$kis_lang['Admission']['HKUGAPS']['msg']['enterSiblingInfo']?>");
    		applicant_form.OthersRelativeStudiedNameEng2.focus();
    		return false;
		}
		if(applicant_form.EnableOthersRelativeSchoolInfo2.value=='class2' && $.trim(applicant_form.OthersRelativeClassPosition2.value)==''){
    		alert("<?=$kis_lang['Admission']['HKUGAPS']['msg']['enterSiblingInfo']?>");
    		applicant_form.OthersRelativeClassPosition2.focus();
    		return false;
		}
		if(applicant_form.EnableOthersRelativeSchoolInfo2.value=='year2' && $.trim(applicant_form.OthersRelativeGraduationYear2.value)==''){
    		alert("<?=$kis_lang['Admission']['HKUGAPS']['msg']['enterSiblingInfo']?>");
    		applicant_form.OthersRelativeGraduationYear2.focus();
    		return false;
		}
	}
	/**** Sibling Studying END ****/
	
	/**** Other Sibling Info START ****/
	if(applicant_form.OthersRelativeNameChi1.value!=''){
		if(
			!checkNaNull(applicant_form.OthersRelativeNameChi1.value) &&
			!checkIsChineseCharacter(applicant_form.OthersRelativeNameChi1.value)
		){
			alert("<?=$kis_lang['Admission']['msg']['enterchinesecharacter']?>");	
			applicant_form.OthersRelativeNameChi1.focus();
			return false;
		}
		if(
			!checkNaNull(applicant_form.OthersRelativeNameEng1.value) &&
			!checkIsEnglishCharacter(applicant_form.OthersRelativeNameEng1.value)
		){
			alert("<?=$kis_lang['Admission']['msg']['enterenglishcharacter']?>");	
			applicant_form.OthersRelativeNameEng1.focus();
			return false;
		}
	
		if($.trim(applicant_form.OthersRelativeNameEng1.value)==''){
    		alert("<?=$kis_lang['Admission']['HKUGAPS']['msg']['enterSiblingInfo']?>");
    		applicant_form.OthersRelativeNameEng1.focus();
    		return false;
		}
		/** DOB START **/
		if($.trim(applicant_form.OthersRelativeBirth1.value)==''){
    		alert("<?=$kis_lang['Admission']['HKUGAPS']['msg']['enterSiblingInfo']?>");
    		applicant_form.OthersRelativeBirth1.focus();
    		return false;
		}
    	if(!applicant_form.OthersRelativeBirth1.value.match(/^[0-9]{4}\-(0[1-9]|1[012])\-(0[1-9]|[12][0-9]|3[01])/)){
    		if(applicant_form.OthersRelativeBirth1.value!=''){
    			alert("<?=$kis_lang['Admission']['msg']['invaliddateformat']?>");
    		}
    		else{
    			alert("<?=$kis_lang['Admission']['msg']['enterdateofbirth']?>");	
    		}
    		
    		applicant_form.OthersRelativeBirth1.focus();
    		return false;
    	}
    	/** DOB END **/
    	if($('[name="OthersRelativeTwin1"]:checked').length == 0){
    		alert("<?=$kis_lang['Admission']['HKUGAPS']['msg']['enterSiblingInfo']?>");
    		applicant_form.OthersRelativeTwin1Y.focus();
    		return false;
    	}
		if($.trim(applicant_form.OthersRelativeSchool1.value)==''){
    		alert("<?=$kis_lang['Admission']['HKUGAPS']['msg']['enterSiblingInfo']?>");
    		applicant_form.OthersRelativeSchool1.focus();
    		return false;
		}
		if($.trim(applicant_form.OthersRelativeGrade1.value)==''){
    		alert("<?=$kis_lang['Admission']['HKUGAPS']['msg']['enterSiblingInfo']?>");
    		applicant_form.OthersRelativeGrade1.focus();
    		return false;
		}
	}
	if(applicant_form.OthersRelativeNameChi2.value!=''){
		if(
			!checkNaNull(applicant_form.OthersRelativeNameChi2.value) &&
			!checkIsChineseCharacter(applicant_form.OthersRelativeNameChi2.value)
		){
			alert("<?=$kis_lang['Admission']['msg']['enterchinesecharacter']?>");	
			applicant_form.OthersRelativeNameChi2.focus();
			return false;
		}
		if(
			!checkNaNull(applicant_form.OthersRelativeNameEng2.value) &&
			!checkIsEnglishCharacter(applicant_form.OthersRelativeNameEng2.value)
		){
			alert("<?=$kis_lang['Admission']['msg']['enterenglishcharacter']?>");	
			applicant_form.OthersRelativeNameEng2.focus();
			return false;
		}
	
		if($.trim(applicant_form.OthersRelativeNameEng2.value)==''){
    		alert("<?=$kis_lang['Admission']['HKUGAPS']['msg']['enterSiblingInfo']?>");
    		applicant_form.OthersRelativeNameEng2.focus();
    		return false;
		}
		/** DOB START **/
		if($.trim(applicant_form.OthersRelativeBirth2.value)==''){
    		alert("<?=$kis_lang['Admission']['HKUGAPS']['msg']['enterSiblingInfo']?>");
    		applicant_form.OthersRelativeBirth2.focus();
    		return false;
		}
    	if(!applicant_form.OthersRelativeBirth2.value.match(/^[0-9]{4}\-(0[1-9]|1[012])\-(0[1-9]|[12][0-9]|3[01])/)){
    		if(applicant_form.OthersRelativeBirth2.value!=''){
    			alert("<?=$kis_lang['Admission']['msg']['invaliddateformat']?>");
    		}
    		else{
    			alert("<?=$kis_lang['Admission']['msg']['enterdateofbirth']?>");	
    		}
    		
    		applicant_form.OthersRelativeBirth2.focus();
    		return false;
    	}
    	/** DOB END **/
    	if($('[name="OthersRelativeTwin2"]:checked').length == 0){
    		alert("<?=$kis_lang['Admission']['HKUGAPS']['msg']['enterSiblingInfo']?>");
    		applicant_form.OthersRelativeTwin2Y.focus();
    		return false;
    	}
		if($.trim(applicant_form.OthersRelativeSchool2.value)==''){
    		alert("<?=$kis_lang['Admission']['HKUGAPS']['msg']['enterSiblingInfo']?>");
    		applicant_form.OthersRelativeSchool2.focus();
    		return false;
		}
		if($.trim(applicant_form.OthersRelativeGrade2.value)==''){
    		alert("<?=$kis_lang['Admission']['HKUGAPS']['msg']['enterSiblingInfo']?>");
    		applicant_form.OthersRelativeGrade2.focus();
    		return false;
		}
	}
	/**** Other Sibling Info END ****/
	/******** Other Info END ********/
	return true;
}
</script>