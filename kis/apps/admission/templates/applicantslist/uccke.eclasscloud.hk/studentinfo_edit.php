<?php

global $libkis_admission;

######## Get Student Cust Info START ########
$applicationCustInfo = $libkis_admission->getAllApplicationCustInfo($applicationInfo['applicationID']);
$applicationInfo['studentApplicationInfoCust'] = $libkis_admission->getApplicationStudentInfoCust($kis_data['schoolYearID'],$classLevelID='',$applicationID='',$kis_data['recordID']);

#### BD Reference START ####
$bdReference = $applicationCustInfo['BD_Ref_Num'][0]['Value'];
#### BD Reference END ####

#### Last School START ####
$lastSchool = $applicationInfo['studentApplicationInfoCust'][0]['OthersPrevSchName'];
$lastSchoolClass = $applicationInfo['studentApplicationInfoCust'][0]['OthersPrevSchClass'];
//$classRepeated = $applicationCustInfo['Class_Repeated'][0]['Value'];
#### Last School END ####

#### Lang Spoken START ####
/*$kis_langStr = '';
$kis_lang_Spoken = (array)$applicationCustInfo['Lang_Spoken'];
foreach($kis_lang_Spoken as $kis_lang){
	if($kis_lang['Value'] == 'Cantonese'){
		$kis_langSpokenCantonese = 'checked="checked"';
	}else if($kis_lang['Value'] == 'Putonghua'){
		$kis_langSpokenPutonghua = 'checked="checked"';
	}else if($kis_lang['Value'] == 'English'){
		$kis_langSpokenEnglish = 'checked="checked"';
	}else if($kis_lang['Value'] == 'Others'){
		$kis_langSpokenOthersChk = 'checked="checked"';
		$kis_langSpokenOthers = $applicationCustInfo['Lang_Spoken_Other'][0]['Value'];
	}
	$kis_langStr .= '<br />';
}
// $kis_langStr = substr($kis_langStr, 0, strlen($kis_langStr)-6); // Trim br
$kis_langSpoken = $kis_langStr;
#### Lang Spoken END ####

#### Lang Written START ####
$kis_langStr = '';
$kis_lang_Written = (array)$applicationCustInfo['Lang_Written'];
foreach($kis_lang_Written as $kis_lang){
	if($kis_lang['Value'] == 'Chinese'){
		$kis_langWrittenChinese = 'checked="checked"';
	}else if($kis_lang['Value'] == 'English'){
		$kis_langWrittenEnglish = 'checked="checked"';
	}else if($kis_lang['Value'] == 'Others'){
		$kis_langWrittenOthersChk = 'checked="checked"';
		$kis_langWrittenOthers = $applicationCustInfo['Lang_Written_Other'][0]['Value'];
	}
	$kis_langStr .= '<br />';
}
$kis_langWritten = $kis_langStr;*/
#### Lang Written END ####

#### Elementary Chinese START ####
if($applicationCustInfo['Elementary_Chinese'][0]['Value']){
	$elementaryY = 'checked="checked"';
}else{
	$elementaryN = 'checked="checked"';
}
#### Elementary Chinese END ####

#### Extra-curricular START ####
//$extraCurricular = $applicationCustInfo['Extra_Curricular'][0]['Value'];
#### Extra-curricular END ####

#### Church Activities START ####
/*$Church_Activities = (array)$applicationCustInfo['Church_Activities'];
foreach($Church_Activities as $act){
	if($act['Value'] == 'SundayWorship'){
		$SundayWorship = 'checked="checked"';
	}else if($act['Value'] == 'SundaySchool'){
		$SundaySchool = 'checked="checked"';
	}else if($act['Value'] == 'Fellowship'){
		$Fellowship = 'checked="checked"';
	}else if($act['Value'] == 'Others'){
		$ChurchActivitiesOthersChk = 'checked="checked"';
		$ChurchActivitiesOthers = $applicationCustInfo['Church_Activities_Other'][0]['Value'];
	}
}*/
#### Church Activities END ####
######## Get Student Cust Info END ########

?>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link type="text/css" rel="stylesheet" media="screen" href="/templates/jquery/thickbox.css">
<script type="text/javascript" src="/templates/jquery/thickbox-compressed.js"></script>
<script type="text/javascript" src="/templates/jquery/jquery.cropit.js"></script>

<input type="hidden" name="ApplicationID" value="<?=$applicationInfo['applicationID']?>" />
<table class="form_table">
	<tbody>
	<tr> 
		<td width="30%" class="field_title">
			<?=$mustfillinsymbol?><?=$kis_lang['Admission']['chinesename']?>
		</td>
		<td width="40%">
		<?=
			$libinterface->GET_TEXTBOX('student_name_b5', 'student_name_b5', $applicationInfo['student_name_b5'], $OtherClass='', $OtherPar=array())
		?>
		</td>
		<td width="30%" rowspan="7" width="145">
			<div id="studentphoto" class="student_info" style="margin:0px;">
				<img src="<?=$attachmentList['personal_photo']['link']?$attachmentList['personal_photo']['link']:$blankphoto?>?_=<?=time()?>"/>
				<div class="mail_icon_form" style="position: absolute;top: -5px;right: -40px; <?=$attachmentList['personal_photo']['link']?'':'display:none;'?>">
					<a id = "btn_remove" href="#" class="btn_remove"></a>
				</div>
				<div class="text_remark" style="text-align:center;">
					<?=$kis_lang['Admission']['msg']['clicktouploadphoto']?>
				</div>
			</div>
		</td>
	</tr>
	<tr>
		<td class="field_title">
			<?=$mustfillinsymbol?><?=$kis_lang['Admission']['englishname']?>
		</td>
		<td>
		<?=
			$libinterface->GET_TEXTBOX('student_name_en', 'student_name_en', $applicationInfo['student_name_en'], $OtherClass='', $OtherPar=array())
		?>
		</td>
	</tr>
	<tr>   
		<td class="field_title">
			<?=$mustfillinsymbol?><?=$kis_lang['Admission']['dateofbirth']?>
		</td>
		<td>
			<input type="text" name="dateofbirth" id="dateofbirth" value="<?=$applicationInfo['dateofbirth']?>">&nbsp;
			<span class="text_remark"><?=$kis_lang['Admission']['DateFormat']?></span>
		</td>
	</tr>
	<tr>
		<td class="field_title">
			<?=$kis_lang['Admission']['gender']?>
		</td>
		<td>
			<?=$libinterface->Get_Radio_Button('gender_M', 'gender', 'M', ($applicationInfo['gender']=='M'), '', $kis_lang['Admission']['genderType']['M'])?>
			<?=$libinterface->Get_Radio_Button('gender_F', 'gender', 'F', ($applicationInfo['gender']=='F'), '', $kis_lang['Admission']['genderType']['F'])?>                              
		</td>
	</tr>
	<tr>   
		<td class="field_title">
			<?=$kis_lang['Admission']['placeofbirth']?>
		</td>
		<td>
			<?=$libinterface->GET_TEXTBOX('placeofbirth', 'placeofbirth', $applicationInfo['placeofbirth'], $OtherClass='', $OtherPar=array())?>
		</td>
	</tr>
	<tr>
		<td class="field_title">
			<?=$kis_lang['Admission']['TSUENWANBCKG']['nationality']?>
		</td>
		<td>
			<?=$libinterface->GET_TEXTBOX('nationality', 'nationality', $applicationInfo['county'], $OtherClass='', $OtherPar=array())?>
		</td>
	</tr>
	<tr>
		<td class="field_title">
			<?=$kis_lang['Admission']['UCCKE']['HKID']?>
		</td>
		<td>
			<?=$libinterface->GET_TEXTBOX('BirthCertNo', 'BirthCertNo', $applicationInfo['birthcertno'], $OtherClass='', $OtherPar=array())?>
			<br/>
			<?=$kis_lang['Admission']['HKUGAPS']['msg']['birthcertnohints'] ?>
		</td>
	</tr>
	<tr>
		<td class="field_title">
			<?=$kis_lang['Admission']['UCCKE']['BdRefNo']?>
		</td>
		<td>
			STRN: <?=$libinterface->GET_TEXTBOX('BdRefNo', 'BdRefNo', $bdReference, $OtherClass='', $OtherPar=array())?>
		</td>
	</tr>                   
	<tr>
		<td class="field_title">
			<?=$kis_lang['Admission']['UCCKE']['CurrentStudySchool']?>
		</td>
		<td>
			<?=$libinterface->GET_TEXTBOX('LastSchool', 'LastSchool', $lastSchool, $OtherClass='', $OtherPar=array())?>
		</td>
	</tr>
	<tr>   
		<td class="field_title">
			<?=$kis_lang['Admission']['UCCKE']['ClassLastAttended']?>
		</td>
		<td>
			<input type="text" name="LastSchoolLevel" id="LastSchoolLevel" value="<?=$lastSchoolClass?>">
		</td>
	</tr>
	<tr>   
		<td class="field_title">
			<?=$kis_lang['Admission']['UCCKE']['ElementryChinese']?>
		</td>
		<td>
			<input type="radio" value="1" id="ElementaryY" name="Elementary" <?=$elementaryY?> >
			<label for="ElementaryY"><?=$kis_lang['Admission']['yes'] ?> </label>
			<input type="radio" value="0" id="ElementaryN" name="Elementary" <?=$elementaryN?> >
			<label for="ElementaryN"><?=$kis_lang['Admission']['no'] ?> </label>
		</td>
	</tr>
	<tr>   
		<td class="field_title">
			<?=$kis_lang['Admission']['UCCKE']['NameOfChurch']?>
		</td>
		<td>
			<?=$libinterface->GET_TEXTBOX('Church', 'Church', $applicationInfo['religion'], $OtherClass='', $OtherPar=array())?>
		</td>
	</tr>
	<tr>   
		<td class="field_title">
			<?=$mustfillinsymbol?><?=$kis_lang['Admission']['UCCKE']['EngAddress']?>
		</td>
		<td>
			<?=$libinterface->GET_TEXTBOX('StudentHomeAddress', 'StudentHomeAddress', $applicationInfo['homeaddress'], $OtherClass='', $OtherPar=array())?>
		</td>
	</tr>
	<tr>   
		<td class="field_title">
			<?=$mustfillinsymbol?><?=$kis_lang['Admission']['UCCKE']['ChiAddress']?>
		</td>
		<td>
			<?=$libinterface->GET_TEXTBOX('StudentHomeAddressChi', 'StudentHomeAddressChi', $applicationInfo['homeaddresschi'], $OtherClass='', $OtherPar=array())?>
		</td>
	</tr>
	
	
	<?php
	for($i=0;$i<sizeof($attachmentSettings);$i++) {
		$attachment_name = $attachmentSettings[$i]['AttachmentName'];
		
		$_filePath = $attachmentList[$attachment_name]['link'];
		if($_filePath){
			$_spanDisplay = '';
			$_buttonDisplay = ' style="display:none;"';
		}else{
			$_filePath = '';
			$_spanDisplay = ' style="display:none;"';
			$_buttonDisplay = '';
		}
		
		$_attachment = "<a href ='".$_filePath."' class='file_attachment' target='_blank'>". $kis_lang['view']."</a>";
		$_attachment .= '<div class="table_row_tool"><a href="#" class="delete_dim" title="'.$kis_lang['delete'].'"></a></div>';

		$accept = '*';
	    $allowCrop = false;
		if($i==0){
		    $allowCrop = true;
		    $accept = 'image/gif, image/jpeg, image/jpg, image/png';
		}
	?>
		<tr>
			<td class="field_title">
				<?=$attachment_name?>
			</td>
			<td id="<?=$attachment_name?>">
				<span class="view_attachment" <?=$_spanDisplay?>>
    				<?=$_attachment?>
    				<?php if($allowCrop){ ?>
        				<input 
            				type="button" 
            				class="formsmallbutton attachment_crop_btn" 
            				value="<?=$kis_lang['Admission']['mgf']['cropPhoto'] ?>"
            				style="margin-left: 5px;"
            				data-img-url="<?=$_filePath ?>"
            				data-id="uploader-<?=$attachment_name?>"
        				/>
    				<?php } ?>
				</span>
				<input 
    				type="button" 
    				class="attachment_upload_btn formsmallbutton" 
    				value="<?=$kis_lang['Upload']?>"  
    				id="uploader-<?=$attachment_name?>"
    				accept="<?=$accept ?>"
    				<?=$_buttonDisplay?>
				/>
			</td>
		</tr>	
	<?php
	}
	?>                                                                           
	</tbody>
</table>





<style>
/**** Image Cropper START ****/
.cropit-preview {
    width: 322px;
    height: 54mm;
    margin: 25px auto;
}
.cropit-preview-container {
    border: 1px solid lightgrey;
}
.cropit-preview-image-container{
    cursor: move;
    border: 1px solid red;
}
input.cropit-image-input {
    display: none;
}
input.cropit-image-zoom-input {
    position: relative;
}
input.cropit-image-zoom-input[disabled] {
    cursor: not-allowed;
}
#image-cropper {
    overflow: hidden;
}
.image-control{
    text-align: center;
    padding-bottom: 5px;
}
.cropit-preview-background {
    opacity: .2;
}
.rotateBtn {
    font-size: 2em;
    color: #737373;
    cursor: pointer;
}
.select-image-btn{
    margin-top: 5px;
}
/**** Image Cropper END ****/

/**** Save START ****/
#saveControlBtn{
    margin-top: 10px;
    padding: 10px;
    text-align: center;
}
/**** Save END ****/
</style>
<div id="cropPhotoDiv" style="display:none;">
	<!-- This wraps the whole cropper -->
	<div id="image-cropper">
		<div class="cropit-preview-container">
    		<!-- This is where the preview image is displayed -->
			<div class="cropit-preview"></div>
		</div>
		
		<div class="image-control">
    		<!-- This range input controls zoom -->
    		<!-- You can add additional elements here, e.g. the image icons -->
    		<input type="range" class="cropit-image-zoom-input" />
    		
    		<!-- This is where user selects new image -->
    		<span class="rotateBtn rotate-ccw-btn"><i class="fa fa-rotate-left" aria-hidden="true"></i></span>
    		<span class="rotateBtn rotate-cw-btn"><i class="fa fa-rotate-right" aria-hidden="true"></i></span>
    		<input type="file" class="cropit-image-input" />
    		<!--button 
    			type="button"
    			class="formsubbutton select-image-btn"
    		>
    			<?=$kis_lang['Admission']['mgf']['uploadNewImage'] ?>
    		</button-->
		</div>
	</div>
	
	<div id="saveControlBtn" class="edit_bottom">
    	<button type="button" class="formbutton" id="saveCropImage">
    		<?=$kis_lang['submit']?>
    	</button>
    	&nbsp;
    	<button type="button" class="formsubbutton" onclick="tb_remove()">
    		<?=$kis_lang['cancel']?>
    	</button>
	</div>
</div>


<script>
(function() {
	'use strict';


	/******** Crop Image START ********/
	var $imageCropper = $('#image-cropper');
	$imageCropper.cropit({
		imageBackground: true,
		imageBackgroundBorderWidth: 25,
		smallImage: 'stretch',
		maxZoom: 1.5
	});

    $('.rotate-cw-btn').click(function() {
    	$('#image-cropper').cropit('rotateCW');
    });
    $('.rotate-ccw-btn').click(function() {
    	$('#image-cropper').cropit('rotateCCW');
    });


	$('.attachment_crop_btn').click(function(e){
		$('#saveCropImage').data('id', $(this).data('id'));
		$imageCropper.cropit('imageSrc', $(this).data('imgUrl'));
		tb_show('','#TB_inline?height=360&width=380&inlineId=cropPhotoDiv&modal=true',null);
		return false;
	});
	$('#saveCropImage').click(function(){
		if(!confirm('<?=$kis_lang['Admission']['UCCKE']['confirmReplaceCurrentDocument'] ?>')){
			return false;
		}
	
		var newPhotoString = $imageCropper.cropit('export', {
            type: 'image/jpeg',
            quality: 1,
            originalSize: true
        });

		var attachment_type = $(this).data('id').split('-')[1];
        $.post('apps/admission/ajax.php?action=saveCroppedAttachment&type='+attachment_type+'&id='+$('#recordID').val(), {
        	'data': newPhotoString
        }, function(res){
			$('td[id="'+attachment_type+'"] span.view_attachment a.file_attachment').attr('href','./apps/admission/ajax.php?action=getAttachmentFile&schoolYearId='+$('#schoolYearId').val()+'&id='+$('#recordID').val()+'&type='+attachment_type);
            tb_remove();
        });
        return false;
	});
	/******** Crop Image END ********/
	



    $('#applicant_form').unbind('submit').submit(function(e){
    	var schoolYearId = $('#schoolYearId').val();
    	var recordID = $('#recordID').val();
    	var display = $('#display').val();
    	var timeSlot = lang.timeslot.split(',');
    	if(checkValidForm()){
    		$.post('apps/admission/ajax.php?action=updateApplicationInfo', $(this).serialize(), function(success){ 
    			$.address.value('/apps/admission/applicantslist/details/'+schoolYearId+'/'+recordID+'/'+display+'&sysMsg='+success);
    		});
    	}
    	return false;
    });
    
    function checkValidForm(){
    		
    	if($('#student_surname_b5').val() == ''){
    		alert(lang.studentssurname_b5_csm);
    		$('#student_surname_b5').focus();
    		return false;
    	}
    	
    	if($('#student_firstname_b5').val() == ''){
    		alert(lang.studentsfirstname_b5_csm);
    		$('#student_firstname_b5').focus();
    		return false;
    	}
    	
    	if($('#student_surname_en').val() == ''){
    		alert(lang.studentssurname_en_csm);
    		$('#student_surname_en').focus();
    		return false;
    	}
    	
    	if($('#student_firstname_en').val() == ''){
    		alert(lang.studentsfirstname_en_csm);
    		$('#student_firstname_en').focus();
    		return false;
    	}
    	
    	if(!check_date_without_return_msg($('#dateofbirth')[0])){
    		alert(lang.invaliddateformat);
    		$('#dateofbirth').focus();
    		return false;
    	}
    	return true;
    }
})();
</script>