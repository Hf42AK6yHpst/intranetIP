<?php

global $libkis_admission;


#### Get Current Study School Info START ####
$applicationInfo['studentApplicationInfoCust'] = $libkis_admission->getApplicationStudentInfoCust($kis_data['schoolYearID'],$classLevelID='',$applicationID='',$kis_data['recordID']);
$currentSchoolInfo = $applicationInfo['studentApplicationInfoCust'][0];
#### Get Current Study School Info END ####


#### Get Student Cust Info START ####
$applicationInfo['studentApplicationInfo'] = current($libkis_admission->getApplicationStudentInfo($kis_data['schoolYearID'],$classLevelID='',$applicationID='',$status='',$kis_data['recordID']));
$applicationInfo['studentCustInfo'] = $libkis_admission->getAllApplicationCustInfo($applicationInfo['applicationID']);

$talents = array();
foreach((array)$applicationInfo['studentCustInfo']['Talents'] as $talent){
	$talents[] = $talent['Value'];
}

$achievements = array();
foreach((array)$applicationInfo['studentCustInfo']['Achievement'] as $achievement){
	$achievements[] = $achievement['Value'];
}

$meritClass = explode('/', $applicationInfo['studentCustInfo']['Merit_Class'][0]['Value']);
$meritForm = explode('/', $applicationInfo['studentCustInfo']['Merit_Form'][0]['Value']);

$langFluency = array();
foreach((array)$applicationInfo['studentCustInfo']['Fluency'] as $fluency){
	$langFluency[$fluency['Value']] = $fluency;
}
#### Get Student Cust Info END ####


#### Get Relatives Info START ####
$applicationInfo['studentApplicationRelativesInfo'] = $libkis_admission->getApplicationRelativesInfoCust($kis_data['schoolYearID'],$classLevelID='',$applicationID='',$kis_data['recordID']);
#### Get Relatives Info END ####


#### Get Referee Type START ####
$checkedType = array();
$typeOther = '';
foreach((array)$applicationInfo['studentCustInfo']['Referee_Type'] as $type){
	if($type['Value'] == $admission_cfg['RefereeType'][1]){
		$checkedType[1] = ' checked';
	}else if($type['Value'] == $admission_cfg['RefereeType'][2]){
		$checkedType[2] = ' checked';
	}else if($type['Value'] == $admission_cfg['RefereeType'][3]){
		$checkedType[3] = ' checked';
		$typeOther = $applicationInfo['studentCustInfo']['Referee_Type_Other'][0]['Value'];
	} 
}
#### Get Referee Type END ####

?>
<table class="form_table">
	<tbody>                              
		<tr> 
			<td width="30%" class="field_title"><?=$kis_lang['form']?></td>
			<td width="70%"><?=$classLevel?></td>
		</tr>
		
<!-------------- Student Other Info START -------------->
<tr>
	<td class="field_title">
		<?=$mustfillinsymbol.$kis_lang['Admission']['KTLMSKG']['CurrentStudySchool']?> (<?=$kis_lang['Admission']['class']?>)
	</td>
	<td>
		<span>
			<input type="hidden" name="OthersPrevSchRecordID" value="<?=$currentSchoolInfo['RecordID']?>" />
			<input id="OthersPrevSchName" name="OthersPrevSchName" value="<?=$currentSchoolInfo['OthersPrevSchName']?>" type="text" class="textboxtext" style="width:40%" />
			(
			<?=$kis_lang['Admission']['class']?>:
			<input id="OthersPrevSchClass" name="OthersPrevSchClass" value="<?=$currentSchoolInfo['OthersPrevSchClass']?>" type="text" class="textboxtext" style="width:20%" />
			)
		</span> 
	</td>
</tr>

<tr>
	<td class="field_title">
		<?=$kis_lang['Admission']['KTLMSKG']['talents']?>
	</td>
	<td>
		<?php for($i=1;$i<=3;$i++){ ?>
			<input id="talents<?=$i?>" name="talents<?=$i?>" value="<?=$talents[$i-1]?>" type="text" class="textboxtext" style="margin-bottom:5px"/><br />
		<?php }?>
	</td>
</tr>

<tr>
	<td class="field_title">
		<?=$kis_lang['Admission']['KTLMSKG']['achievement']?>
	</td>
	<td>
		<?php for($i=1;$i<=3;$i++){ ?>
			<input id="achievements<?=$i?>" name="achievements<?=$i?>" value="<?=$achievements[$i-1]?>" type="text" class="textboxtext" style="margin-bottom:5px"/><br />
		<?php }?>
	</td>
</tr>

<tr>
	<td class="field_title">
		<?=$kis_lang['Admission']['KTLMSKG']['termAttain']?> 
	</td>
	<td>
		<input id="MeritClass" name="MeritClass" value="<?=trim($meritClass[0])?>" type="text" class="textboxtext" style="width: 50px" maxlength="3"/>
		/
		<input id="MeritClassTotal" name="MeritClassTotal" value="<?=trim($meritClass[1])?>" type="text" class="textboxtext" style="width: 50px" maxlength="3"/>
	</td>
</tr>

<tr>
	<td class="field_title">
		<?=$kis_lang['Admission']['KTLMSKG']['schoolAttain']?> 
	</td>
	<td>
		<input id="MeritForm" name="MeritForm" value="<?=trim($meritForm[0])?>" type="text" class="textboxtext" style="width: 50px" maxlength="3"/>
		/
		<input id="MeritFormTotal" name="MeritFormTotal" value="<?=trim($meritForm[1])?>" type="text" class="textboxtext" style="width: 50px" maxlength="3"/>
	</td>
</tr>

<tr>
	<td class="field_title">
		<?=$mustfillinsymbol.$kis_lang['Admission']['KTLMSKG']['fluency']?><br />
		(1 = The most fluent 最流暢)
	</td>
	<td>
		<?php for($i=1;$i<=3;$i++){ ?>
			<label for="fluency<?=$i?>" style="min-width:120px;display: inline-block;">
				<?php 
				if($i == 1){
					echo $kis_lang['Admission']['RMKG']['cantonese'];
				}else if($i == 2){
					echo $kis_lang['Admission']['RMKG']['eng'];
				}else if($i == 3){
					echo $kis_lang['Admission']['RMKG']['pth'];
				}
				?>
			</label>
			<?php 
				$selectedVal = $langFluency[$admission_cfg['LanguageType'][$i]]['Position'];
				$selected1 = ($selectedVal == '1')?'selected':'';
				$selected2 = ($selectedVal == '2')?'selected':'';
				$selected3 = ($selectedVal == '3')?'selected':'';
			?>
			<select id="fluency<?=$i?>" name="fluency<?=$i?>" class="fluency" style="display: inline-block;margin-bottom:5px;">
				<option <?=$selected1?>>1</option>
				<option <?=$selected2?>>2</option>
				<option <?=$selected3?>>3</option>
			</select><br />
		<?php } ?>
	</td>
</tr>

<tr>
	<td class="field_title">
		<?=$mustfillinsymbol.$kis_lang['Admission']['KTLMSKG']['MotherTongue']?> 
	</td>
	<td>
		<?php 
		for($i=1;$i<=3;$i++){ 
			$checked = '';
			if($applicationInfo['studentApplicationInfo']['language'] == $i){
				$checked = ' checked';
			}
		?>
			<input type="radio" value="<?=$i?>" id="MotherTongue<?=$i?>" name="MotherTongue" <?=$checked?>>
			<label for="MotherTongue<?=$i?>" style="min-width:120px;display: inline-block;">
				<?php 
				if($i == 1){
					echo $kis_lang['Admission']['RMKG']['cantonese'];
				}else if($i == 2){
					echo $kis_lang['Admission']['RMKG']['eng'];
				}else if($i == 3){
					echo $kis_lang['Admission']['RMKG']['pth'];
				}
				?>
			</label><br />
		<?php 
		} 
		?>
	</td>
</tr>
<!-------------- Student Other Info END -------------->


<!-------------- Siblings START -------------->
<tr>
	<td colspan="4">
		<table class="form_table" style="font-size: 13px">
		
		<tr>
			<td rowspan="4" style="width: 300px;">
				<?=$kis_lang['Admission']['KTLMSKG']['siblings']?>
			</td>
			<td>&nbsp;</td>
			<td class="form_guardian_head"><center><?=$kis_lang['Admission']['name']?></center></td>
			<td class="form_guardian_head"><center><?=$kis_lang['Admission']['class']?></center></td>
			<td class="form_guardian_head"><center><?=$kis_lang['Admission']['relationship']?></center></td>
		</tr>
		<?php 
		for($i=1;$i<=3;$i++){ 
			$recordId = $applicationInfo['studentApplicationRelativesInfo'][$i-1]['RecordID'];
			if(!$recordId){
				$recordId =  'new';
			}
		?>
		<tr>
			<td class="field_title" style="text-align:right;width:50px;">
				(<?=$i?>)
				<input type="hidden" name="OthersRelativeRecordID[]" value="<?=$recordId?>" />
			</td>
			<td class="form_guardian_field">
				<input id="OthersRelativeStudiedName<?=$i?>" name="OthersRelativeStudiedName[]" value="<?=$applicationInfo['studentApplicationRelativesInfo'][$i-1]['OthersRelativeStudiedName']?>" type="text" class="textboxtext" />
			</td>
			
			<td class="form_guardian_field">
				<input id="OthersRelativeClassPosition<?=$i?>" name="OthersRelativeClassPosition[]" value="<?=$applicationInfo['studentApplicationRelativesInfo'][$i-1]['OthersRelativeClassPosition']?>" type="text" class="textboxtext" />
			</td>
			
			<td class="form_guardian_field">
				<input id="OthersRelativeRelationship<?=$i?>" name="OthersRelativeRelationship[]" value="<?=$applicationInfo['studentApplicationRelativesInfo'][$i-1]['OthersRelativeRelationship']?>" type="text" class="textboxtext" />
			</td>
		</tr>
		<?php 
		} 
		?>
		
		</table>
	</td>
</tr>
<!-------------- Siblings END -------------->


<!-------------- Referee START -------------->
<tr>
	<td class="field_title">
		<?=$kis_lang['Admission']['KTLMSKG']['introducedName']?>
	</td>
	<td colspan="3">
		<input id="Referee_Name" name="Referee_Name" value="<?=$applicationInfo['studentCustInfo']['Referee_Name'][0]['Value']?>" type="text" class="textboxtext" />
	</td>
</tr>

<tr>
	<td class="field_title">
		<?=$kis_lang['Admission']['KTLMSKG']['introducedationship']?>
	</td>
	<td colspan="3">
		<input type="checkbox" value="1" id="RefereeType1" name="RefereeType1" <?=$checkedType[1]?>>
		<label for="RefereeType1"> <?=$kis_lang['Admission']['KTLMSKG']['RefereeType']['Committee']?></label><br />
		
		<input type="checkbox" value="2" id="RefereeType2" name="RefereeType2" <?=$checkedType[2]?>>
		<label for="RefereeType2"> <?=$kis_lang['Admission']['KTLMSKG']['RefereeType']['Staff']?></label><br />
		
		<input type="checkbox" value="3" id="RefereeType3" name="RefereeType3" <?=$checkedType[3]?>>
		<label for="RefereeType3">
			<?=$kis_lang['Admission']['KTLMSKG']['RefereeType']['Other']?>:&nbsp;
			<input name="RefereeTypeOther" type="text" id="RefereeTypeOther" value="<?=$typeOther?>" class="textboxtext" style="width:75%" onkeypress="$('#RefereeType2').attr('checked', 'checked')" />
		</label>
	</td>
</tr>

</tbody>
</table>
<!-------------- Referee END -------------->


<script>
$('#applicant_form').unbind('submit').submit(function(e){
	var schoolYearId = $('#schoolYearId').val();
	var recordID = $('#recordID').val();
	var display = $('#display').val();
	var timeSlot = lang.timeslot.split(',');
	if(checkValidForm()){
		$.post('apps/admission/ajax.php?action=updateApplicationInfo', $(this).serialize(), function(success){ 
			$.address.value('/apps/admission/applicantslist/details/'+schoolYearId+'/'+recordID+'/'+display+'&sysMsg='+success);
		});
	}
	return false;
});

function checkValidForm(){
	
	if($('#OthersPrevSchName').val() == '' || $('#OthersPrevSchClass').val() == ''){
		alert('<?=$kis_lang['Admission']['KTLMSKG']['msg']['enterPrevSchoolAndClass']?>');
		return false;
	}
	
	if( 
		( $('#MeritClass').val() != '' && parseInt($('#MeritClass').val()) != $('#MeritClass').val() ) || 
		( $('#MeritClassTotal').val() != '' && parseInt($('#MeritClassTotal').val()) != $('#MeritClassTotal').val() ) || 
		( $('#MeritForm').val() != '' && parseInt($('#MeritForm').val()) != $('#MeritForm').val() ) || 
		( $('#MeritFormTotal').val() != '' && parseInt($('#MeritFormTotal').val()) != $('#MeritFormTotal').val() )
	){
		alert('<?=$kis_lang['Admission']['KTLMSKG']['msg']['InvalidPlaceAttained']?>');
		return false;
	}
	
	if( 
		$('#fluency1').val() == $('#fluency2').val() ||
		$('#fluency2').val() == $('#fluency3').val() ||
		$('#fluency1').val() == $('#fluency3').val()
	){
		alert('<?=$kis_lang['Admission']['KTLMSKG']['msg']['InvalidFluency']?>');
		return false;
	}
	
	return true;
}
</script>