<?php

?>
<table class="form_table">
	<tbody>
		<tr> 
			<td width="30%" class="field_title">
				<?= $kis_lang['Admission']['chinesename'] ?> (<?= $kis_lang['Admission']['csm']['surname_b5'] ?>)
			</td>
			<td width="40%">
				<?php
				$tempStuEngName = explode(',', $applicationInfo['student_name_b5']);
				echo kis_ui::displayTableField($tempStuEngName[0]);
				?>
			</td>
			<td width="30%" rowspan="7" width="145">
				<div id="studentphoto" class="student_info" style="margin:0px;">
					<img src="<?= $attachmentList['personal_photo']['link'] ? $attachmentList['personal_photo']['link'] : $blankphoto ?>?_=<?= time() ?>"/>
				</div>
			</td>
		</tr>
		<tr>
			<td class="field_title">
				<?= $kis_lang['Admission']['chinesename'] ?> (<?= $kis_lang['Admission']['csm']['firstname_b5'] ?>)
			</td>
			<td>
				<?php
				$tempStuEngName = explode(',', $applicationInfo['student_name_b5']);
				echo kis_ui::displayTableField($tempStuEngName[1]);
				?>
			</td>
		</tr>
		<tr>
			<td class="field_title">
				<?= $kis_lang['Admission']['englishname'] ?> (<?= $kis_lang['Admission']['csm']['surname_b5'] ?>)
			</td>
			<td>
				<?php
				$tempStuEngName = explode(',', $applicationInfo['student_name_en']);
				echo kis_ui::displayTableField($tempStuEngName[0]);
				?>
			</td>
		</tr>
		<tr>
			<td class="field_title">
				<?= $kis_lang['Admission']['englishname'] ?> (<?= $kis_lang['Admission']['csm']['firstname_b5'] ?>)
			</td>
			<td>
				<?
				$tempStuEngName = explode(',', $applicationInfo['student_name_en']);
				echo kis_ui::displayTableField($tempStuEngName[1]);
				?>
			</td>
		</tr>
		<tr>   
			<td class="field_title">
				<?= $kis_lang['Admission']['dateofbirth'] ?>
			</td>
			<td>
				<?= kis_ui::displayTableField($applicationInfo['dateofbirth']) ?>
			</td>
		</tr>
		<tr>
			<td class="field_title">
				<?= $kis_lang['Admission']['gender'] ?>
			</td>
			<td>
				<?= kis_ui::displayTableField($kis_lang['Admission']['genderType'][$applicationInfo['gender']]) ?>
			</td>
		</tr>
		<tr>   
			<td class="field_title">
				<?= $kis_lang['Admission']['placeofbirth'] ?>
			</td>
			<td>
				<?= kis_ui::displayTableField($applicationInfo['placeofbirth']) ?>
			</td>
		</tr>
		<tr>
			<td class="field_title">
				<?= $kis_lang['Admission']['TSUENWANBCKG']['nationality'] ?>
			</td>
			<td>
				<?= kis_ui::displayTableField($applicationInfo['county']) ?>
			</td>
		</tr>
		<tr>
			<td class="field_title">
				<?= $kis_lang['Admission']['KTLMSKG']['IdType'] ?>
			</td>
			<td>
				<?php 
				if($applicationInfo['birthcerttype'] == $admission_cfg['BirthCertType']['hk']){
					echo kis_ui::displayTableField($kis_lang['Admission']['BirthCertType']['hk2']);
				}else if($applicationInfo['birthcerttype'] == $admission_cfg['BirthCertType']['others']){
					echo kis_ui::displayTableField($applicationInfo['birthcerttypeother']);
				}else{
					echo kis_ui::displayTableField('');
				}
				?>
			</td>
		</tr>
		<tr>
			<td class="field_title">
				<?= $kis_lang['Admission']['KTLMSKG']['IdNum'] ?>
			</td>
			<td>
				<?= kis_ui::displayTableField($applicationInfo['birthcertno']) ?>
			</td>
		</tr>
		<tr>
			<td class="field_title">
				<?= $kis_lang['Admission']['KTLMSKG']['address'] ?>
			</td>
			<td colspan="2">
				<?= kis_ui::displayTableField($applicationInfo['homeaddress']) ?>
			</td>
		</tr>                
		<tr>
			<td class="field_title">
				<?= $kis_lang['Admission']['KTLMSKG']['correspondenceAddress'] ?>
			</td>
			<td colspan="2">
				<?= kis_ui::displayTableField($applicationInfo['contactaddress']) ?>
			</td>
		</tr>
		<tr>   
			<td class="field_title">
				<?= $kis_lang['Admission']['munsang']['phone'] ?>
			</td>
			<td colspan="2">
				<?= kis_ui::displayTableField($applicationInfo['homephoneno']) ?>
			</td>
		</tr>          
		<tr>
			<td class="field_title">
				<?= $kis_lang['Admission']['religion'] ?>
			</td>
			<td colspan="2">
				<?= kis_ui::displayTableField($applicationInfo['religionOther']) ?>
			</td>
		</tr>
		<tr>   
			<td class="field_title">
				<?= $kis_lang['Admission']['contactEmail'] ?>
			</td>
			<td colspan="2">
				<?= kis_ui::displayTableField($applicationInfo['email']) ?>
			</td>
		</tr>
		<tr>
			<td class="field_title">
				<?= $kis_lang['Admission']['KTLMSKG']['twins'] ?>
			</td>
			<td>
				<?= ($applicationInfo['istwinsapplied'])? 'Y' : 'N' ?>
			</td>
		</tr>
		<?php if($applicationInfo['istwinsapplied']){ ?>
		<tr>
			<td class="field_title">
				<?= $kis_lang['Admission']['KTLMSKG']['twinsID'] ?>
			</td>
			<td>
				<?= kis_ui::displayTableField($applicationInfo['twinsapplicationid']) ?>
			</td>
		</tr>
		<?php } ?>
	
		<tr>
			<td class="field_title">
				<?= $kis_lang['Admission']['document'] ?>
			</td>
			<td colspan="2">
				<?php
					$attachmentAry = array();
					foreach ($attachmentList as $_type => $_attachmentAry) {
						if ($_type != 'personal_photo') {
							if ($_attachmentAry['link']) {
								//$attachmentAry[] = '<a href="'.$_attachmentAry['link'].'" target="_blank">'.$kis_lang['Admission'][$_type].'</a>';
								$attachmentAry[] = '<a href="' . $_attachmentAry['link'] . '" target="_blank">' . $_type . '</a>';
							}
						}
					}
				?>
				<?= count($attachmentAry) == 0 ? '--' : implode('<br/>', $attachmentAry); ?>
			</td>
		</tr>                                                                                 
	</tbody>
</table>