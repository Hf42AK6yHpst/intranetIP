<?php
// Editing by Henry

/**
 *	2016-08-09 (Henry): Created file
 **/
?>

<?php
list($page,$amount,$total,$sortby,$order,$keyword) = $kis_data['PageBar']; 
?>

<script type="text/javascript" language="JavaScript">
$(function(){
	kis.admission.reports_payment_report_init({
		invaliddateformat:'<?=$kis_lang['Admission']['msg']['invaliddateformat']?>',
		enddateearlier: '<?=$kis_lang['Admission']['msg']['enddateearlier']?>'
	});
})
</script>
<div class="main_content_detail">
<?
	kis_ui::loadModuleTab(array('paymentreport'), '', '#/apps/admission/reports/');
?>
<?=$kis_data['NavigationBar']?>
<p class="spacer"></p>
<div class="Content_tool">
    <div class="btn_option"  id="ExportDiv">
		<a id="btn_export" class="tool_export export" href="javascript:void(0);"><?=$kis_lang['export']?></a>
	</div>  
</div>
<p class="spacer"></p>
	&nbsp;<?=$kis_lang['totalpaymentamount']?>: <?=$totalAmount?><br/>&nbsp;<?=$kis_lang['totalloanamount']?>: <?=$totalLoanAmount?>
<p class="spacer"></p><br/>
    <table class="common_table_list edit_table_list">
		<colgroup><col nowrap="nowrap">
		</colgroup><thead>
			<tr>
			  <th width="20">#</th>
			  <th><?kis_ui::loadSortButton('date_input','date', $sortby, $order)?></th>
			  <th><?kis_ui::loadSortButton('application_id','applicationno', $sortby, $order)?></th>
			  <th><?kis_ui::loadSortButton('student_name','studentname', $sortby, $order)?></th>
			  <th><?kis_ui::loadSortButton('year_name','form', $sortby, $order)?></th>
			  <th><?kis_ui::loadSortButton('payment_amount','paymentamount', $sortby, $order)?></th>
			  <th><?kis_ui::loadSortButton('txn_id','paymentmethod', $sortby, $order)?></th>
			  <th><?kis_ui::loadSortButton('remarks','importRemarks', $sortby, $order)?></th>
			  <th><?=$kis_lang['Admission']['PaymentsStatus']?></th>
			  <th><?=$kis_lang['parentorguardian']?></th>
			  <th><?=$kis_lang['phoneno']?></th>
			  <th><?=$kis_lang['Admission']['RMKG']['schoolyear']?></th>
			  <th><?=$kis_lang['Admission']['applicationstatus']?></th>
		  </tr>
		</thead>
		<tbody>
			<?php 
				if($total>0){
					$idx=$amount*($page-1);
					foreach($paymentReportDetails as $_applicationId => $_paymentReportDetailsAry){
						$idx++;
						
						$_dateInput = $_paymentReportDetailsAry['date_input'];
						$_studentName = $_paymentReportDetailsAry['student_name'];
						$_yearName = $_paymentReportDetailsAry['year_name'];
						$_paymentAmount = $_paymentReportDetailsAry['payment_amount'];
						$_txn_id = $_paymentReportDetailsAry['txn_id'];
						$_remarks = $_paymentReportDetailsAry['remarks'];
						$_parentName = implode('<br/>',array_filter($_paymentReportDetailsAry['parent_name']));	
						$_parentPhone = implode('<br/>',array_filter($_paymentReportDetailsAry['parent_phone']));
						$_admission_year = getAcademicYearByAcademicYearID($_paymentReportDetailsAry['apply_year']);
						$_application_status = $kis_lang['Admission']['Status'][$_paymentReportDetailsAry['application_status']];
						$_payment_status = $_paymentReportDetailsAry['payment_status'];
						$tr_css = '';
			?>			
						<tr<?=$tr_css?>>
						  <td><?=$idx?></td>
						  <td><?=substr($_dateInput, 0, 10)?></td>
						  <td><?=$_applicationId?></td>
						  <td><?=$sys_custom['KIS_Admission']['ICMS']['Settings'] || $sys_custom['KIS_Admission']['CSM']['Settings'] || $sys_custom['KIS_Admission']['MUNSANG']['Settings']?str_replace(",", " ", $_studentName):$_studentName?></td>
						  <td><?=$_yearName?></td>
						  <td><?=$_paymentAmount?></td>
						  <td><?=$_txn_id?'Paypal':'--'?></td>
						  <td><?=$_remarks?$_remarks:'--'?></td>
						  <td><?=$_payment_status?$_payment_status:'--'?></td>
						  <td><?=$_parentName?($sys_custom['KIS_Admission']['ICMS']['Settings']?str_replace(",", " ", $_parentName):$_parentName):'--'?></td>
						  <td nowrap="nowrap"><?=$_parentPhone?$_parentPhone:'--'?></td>
						  <td><?=$_admission_year?></td>
						  <td><?=$_application_status?></td>
						 </tr>
			<?php } 
				}else{ //no record	
			?>		
						<tr>
						  <td colspan="13" style="text-align:center;"><?=$kis_lang['norecord']?></td>
						</tr>				
			<?php } ?>		
		</tbody>
	</table>
	<p class="spacer"></p>
		<? kis_ui::loadPageBar($page, $amount, $total, $sortby, $order) ?>      
	<p class="spacer"></p><br>
	<div class="edit_bottom">
		<input type="button" name="backBtn" id="backBtn" class="formsubbutton" value="<?=$kis_lang['back']?>" />
    </div>
</div>

<form class="filter_form">
<input type="hidden" name="start_date" id="start_date" value="<?=$start_date?>" />
<input type="hidden" name="end_date" id="end_date" value="<?=$end_date?>" />
</form>

<form id="export_form">
<input type="hidden" name="start_date" id="start_date" value="<?=$start_date?>" />
<input type="hidden" name="end_date" id="end_date" value="<?=$end_date?>" />
<input type="hidden" name="page" id="page" value="<?=$page?>" />
<input type="hidden" name="amount" id="amount" value="<?=$amount?>" />
<input type="hidden" name="total" id="total" value="<?=$total?>" />
<input type="hidden" name="sortby" id="sortby" value="<?=$sortby?>" />
<input type="hidden" name="order" id="order" value="<?=$order?>" />
<input type="hidden" name="keyword" id="keyword" value="<?=$keyword?>" />
</form>
