<?php
// Editing by 
/*
 * 2014-03-24 (Henry): Created file
 */
 
?>
<script type="text/javascript">
function borrowStatusChanged(selectedStatus)
{
	if(selectedStatus)
		window.location.href="#/apps/borrowrecord/?status="+selectedStatus;
	else
		window.location.href="#/apps/borrowrecord/";
}
</script>
<div class="main_content">       
    <div id="table_filter">
		<select name="status" onchange="borrowStatusChanged(this.value);">
		    <option value=""  <?=$status == ''?'selected':''?>> <?=$kis_lang['all']?> </option>
		    <option value="BORROWED" <?=$status == 'BORROWED'?'selected':''?>> <?=$kis_lang['borrowing']?> </option>
		    <option value="RETURNED" <?=$status == 'RETURNED'?'selected':''?>> <?=$kis_lang['returned']?> </option>
		</select>
    </div>    
	<p class="spacer"></p>                    	
	<div class="table_board">
	    <table class="common_table_list">
		<thead>
		    <tr>
			<th width="20">#</th>
			<th><? kis_ui::loadSortButton('title','item', $sortby, $order)?></th>
			<th><? kis_ui::loadSortButton('borrow_date','checkedoutdate', $sortby, $order)?></th>
			<th><? kis_ui::loadSortButton('due_date','duedate', $sortby, $order)?></th>
			<th><? kis_ui::loadSortButton('borrow_status','status', $sortby, $order)?></th>
		    </tr>
		</thead>
		<tbody>
		    <? if ($records): ?>
			<? foreach ($records as $i=>$record): ?>
			<tr class="<?=$record['borrow_status'] == '0'?'alert_row':($record['borrow_status'] == '1'?'':'past')?>">
			    <td><?=($page-1)*$amount+$i+1?></td>
			    <td><?=$record['title']?></td>
			    <td><?=substr($record['borrow_date'], 0, 10)?></td>
			    <td><?=$record['due_date']?></td>
			    <td><?=$record['borrow_status'] == '0'?'<span class="alert_late">'.$kis_lang['overdue'].' '.$record['overdue_days'].' '.$kis_lang['day'].'</span>':($record['borrow_status'] == '1'?$kis_lang['borrowing']:$kis_lang['returned'].'<span class="date_time"><em>'.substr($record['return_date'], 0, 10).'</em></span>')?></td>
			</tr>
			<? endforeach; ?>
		    </tbody></table>
		    <? else: ?>
		    </tbody></table>
			<div class="no_record">
			   <?=$kis_lang['norecord']?>!
			</div>
		    <? endif; ?>
	    <p class="spacer"></p>
	    <? kis_ui::loadPageBar($page, $amount, $total, $sortby, $order) ?>
	</div>
</div>