<?
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT.'kis/init.php');

    
$libkis_borrowrecord = $libkis->loadApp('borrowrecord');

$kis_data['page']   = $page?   $page: 1;
$kis_data['amount'] = $amount? $amount: 10;

list($kis_data['total'], $kis_data['records']) = $libkis_borrowrecord->getBorrowRecords(array('status'=>$status), $sortby, $order, $kis_data['amount'], $kis_data['page']);
$kis_data['main_template'] = 'main';
//$kis_data['schoolYearSelection'] = $libkis_admission->getAcademicYearSelection($kis_data['schoolYearID']);

kis_ui::loadTemplate($kis_data['main_template'], $kis_data);
?>