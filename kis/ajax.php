<?php
// Editing by
/*
 * 2015-08-26 (Carlos): added action [getpendingpaymentalert] to get the display layer of children's pending payment items for the parent.
 */
$PATH_WRT_ROOT = "../";
include_once($PATH_WRT_ROOT."kis/init.php");

switch ($action){

    case 'getnotifications':
	echo $libjson->encode($libkis->getNotificationCounts());
    break;
    case 'setcurrentchild':

	echo $libjson->encode($kis_user['type']==kis::$user_types['parent']? $libkis->changeCurrentStudent($child)->getStudentDetail(): array());

    break;
    case 'searchuser':

	$params = array(
		    'keyword'=>$search,
		    'excludes'=>$excludes,
		    'user_type'=>kis::$user_types[$type],
		    'class_id'=>$class_id
		);

	$users = kis_utility::getUsers($params);

	if (sizeof($users)>500){
	    $kis_data['error'] = $kis_lang['toomanyresults'];
	}else if(sizeof($users)==0){
	    $kis_data['error'] = $kis_lang['norecord'];
	}else{
	    $kis_data['users'] = $users;
	}
	echo $libjson->encode($kis_data);

    break;

    case 'getpendingpaymentalert':
    	echo $libkis->getPendingPaymentsDisplayLayer();
    break;

    default:

	$kis_data['school'] = $libkis->getUserSchool();
	$kis_data['user'] = $kis_user;
	$kis_data['available_apps'] = $libkis->getVisibleApps();
	$kis_data['background'] = $kis_config['background'][$kis_user['type']];
	$kis_data['lang'] = $intranet_session_language;

	echo $libjson->encode($kis_data);

    break;
}

?>
