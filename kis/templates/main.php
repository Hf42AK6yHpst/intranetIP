<!DOCTYPE html>
<html>
    
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<title>:: eClass KIS ::</title>
<link type="text/css" rel="stylesheet" href="/templates/jquery/jquery.fancybox.css">
<link type="text/css" rel="stylesheet" href="/templates/jquery/jquery.fancybox-thumbs.css">
<link type="text/css" rel="stylesheet" href="/templates/jquery/ui-1.9.2/jquery-ui-1.9.2.custom.min.css">
<link type="text/css" rel="stylesheet" href="/templates/kis/css/common.css">
<script type="text/javascript" src="/templates/jquery/jquery-1.8.3.min.js"></script>
<script type="text/javascript" src="/templates/jquery/jquery.address-1.5.min.js"></script>
<script type="text/javascript" src="/templates/jquery/jquery.masonry.min.js"></script>
<script type="text/javascript" src="/templates/jquery/jquery.fancybox.pack.js"></script>
<script type="text/javascript" src="/templates/jquery/jquery.fancybox-thumbs.js"></script>
<script type="text/javascript" src="/templates/jquery/ui-1.9.2/jquery-ui.custom.min.js"></script>
<script type="text/javascript" src="/templates/jquery/plupload/plupload.full.js"></script>
<script type="text/javascript" src="/templates/kis/js/kis.js"></script>
</head>

<body>
    <div id="container" style="display:none">
    <script>
	kis.init();
	
	$(function(){
	    
	    $(".fancybox-thumb").fancybox({
		prevEffect : 'fade',
		nextEffect : 'fade',
		padding: 0,
		autoSize: true,
		closeBtn: false,
		helpers: {
		    title: {type: 'outside'},
		    thumbs: {
			width: 50,
			height: 50
		    }
		}
	    });
	    
	    $('.btn_logout').click(function(e){
		var href = $(this).attr('href');
		
		kis.confirm({
		    content: ($(this).hasClass('b5')? "確定要":"Are you sure to")+$(this).attr('title')+'?',
		    target: $(this),
		    callback: function(){
			kis.logout();
		    }
		});
		return false;
	    });
	    
	     
	});
    </script>
    <div class="top_header">
	<a title="eClass KIS" class="logo_kis" href="./"></a>
	<div class="school_name"></div>
	<div class="user_btn">
	    
	    <a title="Log Out" class="btn_logout en" href="/logout.php"></a>
	    <a title="登出" class="btn_logout b5" href="/logout.php"></a>
	    
	    <a title="Change to English Language" class="btn_lang_eng b5" href="/lang.php?lang=en"></a>
	    <a title="Change to Chinese Language" class="btn_lang_chn en" href="/lang.php?lang=b5"></a>
	    
	    <span class="sep">|</span>
	<span><em class="user_type"></em>, <em class="user_name"></em></span>            
	</div>

	<div class="parent_btn" style="display:none;">
	    <a class="btn_menu btn_menu_on" href="#"><span class="en">Menu</span><span class="b5">選單</span><em></em></a>
	    <div class="student_name_btn"><div>
		<span class="student_name"><em class=""></em>
		    <span></span>
		</span>
		<span class="student_text en">with</span>
		<span class="student_text b5">與</span>
		<a class="btn_change_student" href="#"></a>
		<a class="btn_logs en" href="#/logs"><span>Logs</span></a>
		<a class="btn_logs b5" href="#/logs"><span>日誌</span></a>
	    </div></div>
	   
	</div>


    </div>
    <div class="container_slider">
	<div id="update_page" class="board" style="opacity:0">
	    <div class="board_top"><div class="board_top_right"><div class="board_top_bg"><div class="board_top_content">
		<div class="update_photo">
		    <img src=""/>
		</div>
		
		<div class="update_info">
			<h1></h1>
			<span class="en">Class</span>
			<span class="b5">班別</span>
			<a class="student_class"></a><br/>
			<span class="en">Class Teacher</span>
			<span class="b5">班主任</span>
			<a class="student_class_teacher"></a>
		</div>
		    
	    </div></div></div></div>
	    <div class="board_main"><div class="board_main_right"><div class="board_main_bg"><div class="board_main_content">
		    
    
	    </div></div></div></div>
	    
	    <div class="board_bottom"><div class="board_bottom_right"><div class="board_bottom_bg"></div></div></div>
	</div>
	
	
	<div id="main_portal" class="board">
		<div class="board_top"><div class="board_top_right"><div class="board_top_bg"><div class="board_top_content">
		
		
		</div></div></div></div>
		<div class="board_main"><div class="board_main_right"><div class="board_main_bg"><div class="board_main_content">
		    <!---->
		    <ul class="module_list">
			
		    </ul>    	
		    <!---->
			     <p class="spacer"></p>
			     
		</div></div></div></div>
		<div class="board_bottom"><div class="board_bottom_right"><div class="board_bottom_bg"></div></div></div>
		
	</div>
        
	<div id="module_page" class="board">
    
	    <div class="board_top"><div class="board_top_right"><div class="board_top_bg"><div class="board_top_content">
		<div class="navigation_bar">
			<a class="btn_home en" href="#">Home</a>
			<a class="btn_home b5" href="#">主頁</a>
			<a class="module_title"></a>
			
		</div>
		    
	    </div></div></div></div>
	    
	    <div class="board_main"><div class="board_main_right"><div class="board_main_bg"><div class="board_main_content">


		  
	    
	    </div></div></div></div>
	    
	<div class="board_bottom"><div class="board_bottom_right"><div class="board_bottom_bg"></div></div></div>
		
    </div>
	
    </div>
      <p class="spacer"></p>
    
    </div>
    <div class="footer"><a title="Powered by eClass" href="http://eclass.com.hk"></a><span></span></div>
    <div id="kis_dialog" style="display:none" title=""><div class="kis_dialog_content"></div>
	
	    <input type="button" class="formbutton en" value="OK">
	    <input type="button" class="formsubbutton en" value="Cancel">
	    
	    <input type="button" class="formbutton b5" value="確認">
	    <input type="button" class="formsubbutton b5" value="取消">
	
    </div>
    <div id="kis_overlay"></div>
    
    <div id="login_form" style="display:none;">
	<div class="logo">eClass Kindergarten System</div>
	<form method="post" name="form" >
	    
	    <span>Login ID </span><input type="text" required autofocus name="UserLogin" value=""/>
	    <p class="spacer"></p>
	    <span>Password </span><input type="password" required name="UserPassword"  value="" />
	     
	    <p class="spacer"></p>
	    <a href="#" class="forgotPw">forgot password?</a>
	    <input type="submit" value=" "  class="btn_login"/>
	    
    
	</form>
	<p class="spacer"></p>
	<div class="login_about">
		© <?=date('Y')?> <strong>BroadLearning Education (Asia) Limited</strong>. All rights reserved.
		<div class="browsers">Recommended browsers:
		    <a title="Google Chrome" target="_blank" href="http://www.google.com/chrome" class="chrome">12+</a>
		    <a title="Mozilla Firefox" target="_blank" href="http://www.mozilla.org" class="firefox">4+</a>
		    <a title="Internet Explorer" target="_blank" href="http://www.microsoft.com/ie" class="ie">9+</a>
		</div>
	</div>
    </div>

</body></html>