<?php
# modifying by: Henry
 
/********************
 * Log :
 * Date		2016-10-18 [Henry]
 * 			File Created
 * 
 ********************/
$PATH_WRT_ROOT = "../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_ui.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_utility.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_apps.php");
//include_once($PATH_WRT_ROOT."includes/json.php");
include_once("../config.php");

//for the customization
include_once($PATH_WRT_ROOT."includes/admission/".$setting_path_ip_rel."/config.php");
$intranet_session_language = $admission_cfg['DefaultLang'];
include_once($PATH_WRT_ROOT."includes/admission/".$setting_path_ip_rel."/libadmission_cust.php");
include_once($PATH_WRT_ROOT."includes/admission/".$setting_path_ip_rel."/libadmission_ui_cust.php");

include_once($PATH_WRT_ROOT."lang/lang.".$intranet_session_language.".php");
include_once($PATH_WRT_ROOT."lang/kis/lang_common_".$intranet_session_language.".php");
include_once($PATH_WRT_ROOT."lang/kis/apps/lang_admission_".$intranet_session_language.".php");
include_once($PATH_WRT_ROOT."lang/admission_lang.".$intranet_session_language.".php");




intranet_opendb();

//$libjson 	= new JSON_obj();
$libkis 	= new kis('');
$lac		= new admission_cust();
$lauc		= new admission_ui_cust();
$li			=new interface_html();

if (!$plugin['eAdmission'])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT("","../");
	exit;
}


## Maing
if (($q != '') && ($field != '')) {
	$limit = 100;
	$validStr = false;
	$extraFilter = '';
	# search with input keyword
	switch ($field) {
		case 'LastSchool':
			$table = 'ADMISSION_PRIMARY_SCH_NAME';
			$selectField = 'SchoolName';
			$orderBy = 'SchoolName';
			$validStr = true;
			break;
		default:
			$table = 'ADMISSION_AUTO_COMPLETE';
			$selectField = 'Suggestion';
			$orderBy = 'Suggestion';
			$extraFilter .= " AND Type='{$field}'";
			$validStr = true;
			break;
	}
	
	if ($validStr) {
		$filter = " where ".$selectField." is not null and ".$selectField." <> '' and ".$selectField." like '%".$q."%'".$extraFilter;
		$sql = "select distinct ".$selectField." from ".$table.$filter." order by ".$selectField." limit ".$limit;
		$result = $lac->returnArray($sql,null,2);	// numeric array only
		
		if(!empty($result)){
			foreach ($result as $row){
				$x .= $row[0]."|".$row[0]."\n";
			}
		}
	}
}

echo $x;