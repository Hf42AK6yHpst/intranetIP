<?php
# modifying by: Henry
 
/********************
 * Log :
 * Date		2013-10-08 [Henry]
 * 			File Created
 * 
 ********************/
$PATH_WRT_ROOT = "../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_ui.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_utility.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_apps.php");
//include_once($PATH_WRT_ROOT."includes/json.php");
include_once($PATH_WRT_ROOT."lang/kis/lang_common_".$intranet_session_language.".php");
include_once($PATH_WRT_ROOT."lang/kis/apps/lang_admission_".$intranet_session_language.".php");
include_once($PATH_WRT_ROOT."lang/admission_lang.".$intranet_session_language.".php");
include_once("../config.php");
include_once($PATH_WRT_ROOT."includes/settings.php");
//for the customization
include_once($PATH_WRT_ROOT."includes/admission/".$kis_admission_school."/config.php");
include_once($PATH_WRT_ROOT."includes/admission/".$kis_admission_school."/libadmission_cust.php");
include_once($PATH_WRT_ROOT."includes/admission/".$kis_admission_school."/libadmission_ui_cust.php");

intranet_opendb();

//$libjson 	= new JSON_obj();
$libkis 	= new kis('');
$lac		= new admission_cust();
$lauc		= new admission_ui_cust();

if (!$plugin['eAdmission'])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT("","../");
	exit;
}
//$kis_user 	= $libkis->getUserInfo();
//$q 		= explode('/',strtolower($q));
//$search		= trim($search);
//$request_ts	= $_SERVER['REQUEST_TIME'];
$_SESSION['SSV_PRIVILEGE']['school']['name'] = GET_SCHOOL_NAME();
$school = $libkis->getUserSchool(); 
//$user = $kis_user;
//$available_apps = $libkis->getVisibleApps();
//$background = $kis_config['background'][$kis_user['type']];


$libkis_admission = $libkis->loadApp('admission');

//$kis_data['school'] = $libkis->getUserSchool(); 
//	$kis_data['user'] = $kis_user;
//	$kis_data['available_apps'] = $libkis->getVisibleApps();
//	$kis_data['background'] = $kis_config['background'][$kis_user['type']];
//	$kis_data['lang'] = $intranet_session_language;
// kis_ui::loadTemplate($main_template, $kis_data, 'json');
	
$main_content = $lauc->getChooseClassForm();
            
include_once("common_tmpl.php");
?>
<script type="text/javascript">
function checkForm(form1) {
	
	if($('input:radio[name=sus_status]:checked').val() == null){
		alert("<?= $i_alert_pleasefillin?> Class Level");	
		form1.sus_status[0].focus();
		return false;
	}
	 else  {
		return true;
	}
}
function submit2(){
	$('#setItem').val(1);
	var obj = document.form1;
	if(!checkForm(obj)){
		return false;
	}
	obj.submit();
}
</script>
