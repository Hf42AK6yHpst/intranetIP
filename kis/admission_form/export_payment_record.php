<?php
//using Henry

/********************
 * 
 * Log :
 * 
 * Date		2015-01-19 [Henry]
 * 			export all data by status
 * 
 * Date		2013-12-31 [Henry]
 * 			File Created
 * 
 ********************/

$PATH_WRT_ROOT = "../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_ui.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_utility.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_apps.php");
//include_once($PATH_WRT_ROOT."includes/json.php");
include_once("../config.php");
include_once($PATH_WRT_ROOT."lang/kis/lang_common_".$intranet_session_language.".php");
include_once($PATH_WRT_ROOT."lang/kis/apps/lang_admission_".$intranet_session_language.".php");
include_once($PATH_WRT_ROOT."lang/admission_lang.".$intranet_session_language.".php");

//for the customization
include_once($PATH_WRT_ROOT."includes/admission/".$setting_path_ip_rel."/config.php");
include_once($PATH_WRT_ROOT."includes/admission/".$setting_path_ip_rel."/libadmission_cust.php");
include_once($PATH_WRT_ROOT."includes/admission/".$setting_path_ip_rel."/libadmission_ui_cust.php");

//for the export
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_opendb();
$libkis 	= new kis('');
$lac		= new admission_cust();
$lauc		= new admission_ui_cust();

# Temp Assign memory of this page
ini_set("memory_limit", "150M"); 

$lexport = new libexporttext();

$ExportArr = array();
$exportColumn = array();

if($_SESSION['UserType']==USERTYPE_STAFF){
	# Define Column Title
//	$exportColumn = $lac->getExportHeader();
	
	$exportColumn[0][] = $Lang['Admission']['applyLevel'];
	$exportColumn[0][] = $Lang['Admission']['applicationno'];
	$exportColumn[0][] = $Lang['Admission']['chinesename'];
	$exportColumn[0][] = $Lang['Admission']['englishname'];
	$exportColumn[0][] = $Lang['Admission']['birthcertno'];
	$exportColumn[0][] = "txn_id";
	$exportColumn[0][] = "receiver_email";
	$exportColumn[0][] = "mc_currency";
	$exportColumn[0][] = "mc_gross";
	$exportColumn[0][] = "inputdate";
	$exportColumn[0][] = $Lang['Admission']['otherremarks'];
//	
//	//student info header
//	$exportColumn[0][] = $Lang['Admission']['studentInfo'];
//	for($i=0; $i < count($exportHearder['studentInfo'])-2; $i++){
//		$exportColumn[0][] = "";
//	}
//	
//	//parent info header
//	$exportColumn[0][] = $Lang['Admission']['PGInfo']."(".$Lang['Admission']['PG_Type']['F'].")";
//	for($i=0; $i < count($exportHearder['parentInfoF'])-1; $i++){
//		$exportColumn[0][] = "";
//	}
//	$exportColumn[0][] = $Lang['Admission']['PGInfo']."(".$Lang['Admission']['PG_Type']['M'].")";
//	for($i=0; $i < count($exportHearder['parentInfoM'])-1; $i++){
//		$exportColumn[0][] = "";
//	}
//	$exportColumn[0][] = $Lang['Admission']['PGInfo']."(".$Lang['Admission']['PG_Type']['G'].")";
//	for($i=0; $i < count($exportHearder['parentInfoG'])-1; $i++){
//		$exportColumn[0][] = "";
//	}
//	
//	//other info header
//	$exportColumn[0][] = $Lang['Admission']['otherInfo'];
//	for($i=0; $i < count($exportHearder['otherInfo'])-1; $i++){
//		$exportColumn[0][] = "";
//	}
//	
//	//official use header
//	$exportColumn[0][] = $kis_lang['remarks'];
//	for($i=0; $i < count($exportHearder['officialUse'])-1; $i++){
//		$exportColumn[0][] = "";
//	}
//	
//	//sub header
//	$exportColumn[1] = array_merge(array($exportHearder[0]), $exportHearder['studentInfo'], $exportHearder['parentInfoF'], $exportHearder['parentInfoM'], $exportHearder['parentInfoG'], $exportHearder['otherInfo'], $exportHearder['officialUse']);
//	
	if(!$applicationAry){
		$applicationAry = array();
		if($sys_custom['KIS_Admission']['CSM']['Settings'])
			$result = $lac->getApplicationStatus($schoolYearID,$classLevelID,$applicationID='',$recordID='');
		else
			$result = $lac->getApplicationOthersInfo($schoolYearID,$classLevelID,$applicationID='',$recordID='');
		foreach($result as $aResult){
			if(!$sys_custom['KIS_Admission']['CSM']['Settings'] || $selectStatus == $aResult['statusCode'] || $selectStatus == '')
				$applicationAry[] = $aResult['RecordID'];
		}
	}
	//all data
	$applicationCnt = count($applicationAry);
	
	for($i=0;$i<$applicationCnt;$i++){
		$result = $lac->getExportPaymentRecord($schoolYearID,'','',$applicationAry[$i]);
		$ExportArr[] = $result;
	}
	//debug_pr($ExportArr);
	$filename = "admission_payment_record(".$result[0].").csv";
	
	//generate csv content
	$export_content = $lexport->GET_EXPORT_TXT_WITH_REFERENCE($ExportArr, $exportColumn, "\t", "\r\n", "\t", 0, "9");
	
	//Output The File To User Browser
	$lexport->EXPORT_FILE($filename, $export_content);
}
?>