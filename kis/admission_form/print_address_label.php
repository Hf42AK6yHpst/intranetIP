<?php
// using:
$PATH_WRT_ROOT = "../../";

include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "includes/kis/libkis.php");
include_once ($PATH_WRT_ROOT . "includes/kis/libkis_ui.php");
include_once ($PATH_WRT_ROOT . "includes/kis/libkis_utility.php");
include_once ($PATH_WRT_ROOT . "includes/kis/libkis_apps.php");
// include_once($PATH_WRT_ROOT."includes/json.php");
include_once ("../config.php");
// include_once($PATH_WRT_ROOT . "lang/kis/lang_common_" . $intranet_session_language . ".php");
// include_once($PATH_WRT_ROOT . "lang/kis/apps/lang_admission_" . $intranet_session_language . ".php");
// include_once($PATH_WRT_ROOT . "lang/admission_lang." . $intranet_session_language . ".php");

// ### Lang START ####
if ($intranet_session_language == 'en') {
    $intranet_session_language = 'b5';
    include ("{$PATH_WRT_ROOT}lang/lang.{$intranet_session_language}.php");
    include ("{$PATH_WRT_ROOT}lang/kis/lang_common_{$intranet_session_language}.php");
    include ("{$PATH_WRT_ROOT}lang/kis/apps/lang_admission_{$intranet_session_language}.php");
    include ("{$PATH_WRT_ROOT}lang/admission_lang.{$intranet_session_language}.php");
    $LangB5 = $Lang;
    $kis_lang_b5 = $kis_lang;
    unset($Lang);
    unset($kis_lang);

    $intranet_session_language = 'en';
    include ("{$PATH_WRT_ROOT}lang/lang.{$intranet_session_language}.php");
    include ("{$PATH_WRT_ROOT}lang/kis/lang_common_{$intranet_session_language}.php");
    include ("{$PATH_WRT_ROOT}lang/kis/apps/lang_admission_{$intranet_session_language}.php");
    include ("{$PATH_WRT_ROOT}lang/admission_lang.{$intranet_session_language}.php");
    $LangEn = $Lang;
    $kis_lang_en = $kis_lang;
} else {
    $intranet_session_language = 'en';
    include ("{$PATH_WRT_ROOT}lang/lang.{$intranet_session_language}.php");
    include ("{$PATH_WRT_ROOT}lang/kis/lang_common_{$intranet_session_language}.php");
    include ("{$PATH_WRT_ROOT}lang/kis/apps/lang_admission_{$intranet_session_language}.php");
    include ("{$PATH_WRT_ROOT}lang/admission_lang.{$intranet_session_language}.php");
    $LangEn = $Lang;
    $kis_lang_en = $kis_lang;
    unset($Lang);
    unset($kis_lang);

    $intranet_session_language = 'b5';
    include ("{$PATH_WRT_ROOT}lang/lang.{$intranet_session_language}.php");
    include ("{$PATH_WRT_ROOT}lang/kis/lang_common_{$intranet_session_language}.php");
    include ("{$PATH_WRT_ROOT}lang/kis/apps/lang_admission_{$intranet_session_language}.php");
    include ("{$PATH_WRT_ROOT}lang/admission_lang.{$intranet_session_language}.php");
    $LangB5 = $Lang;
    $kis_lang_b5 = $kis_lang;
}
// ### Lang END ####

// for the customization
include_once ($PATH_WRT_ROOT . "includes/admission/" . $setting_path_ip_rel . "/config.php");
include_once ($PATH_WRT_ROOT . "includes/admission/" . $setting_path_ip_rel . "/libadmission_cust.php");
// for the export
require_once ($PATH_WRT_ROOT . "includes/mpdf/mpdf.php");

intranet_opendb();
$libkis = new kis('');
$lac = new admission_cust();
$mpdf = new mPDF($mode = '', $format = 'A4', $default_font_size = 0, $default_font = 'msjh', $marginLeft = 0, $marginRight = 0, $marginTop = 0, $marginBottom = 0/*,
	    $marginHeader=9,
	    $marginFooter=9,
	    $orientation='P'*/
);
$mpdf->mirrorMargins = 1;

// Temp Assign memory of this page
ini_set("memory_limit", "150M");

$applicationDetails = array();
if ($_SESSION['UserType'] == USERTYPE_STAFF) {

    $result = $lac->getStudentAddress($applicationAry);
    for ($i = 0; $i < count($result); $i ++) {
        $address = "";
        $_applicationId = $result[$i]['application_id'];
        $address_lang = "en";

        $_address = $result[$i]['AddressRoom'] . $result[$i]['AddressFloor'] . $result[$i]['AddressBlock'] . $result[$i]['AddressBldg'] . $result[$i]['AddressStreet'] . $result[$i]['AddressDistrict'];

        if (preg_match("/[\x{4e00}-\x{9fa5}]+/u", $_address)) {
            $address_lang = "b5";
        } else {
            $address_lang = "en";
        }

        foreach ($admission_cfg['addressdistrict'] as $key => $langVal) {
            if ($langVal == $result[$i]['AddressEstate']) {
                $estate = ${'kis_lang_' . $address_lang}['Admission']['CHIUCHUNKG'][$key];
            }
        }

        $room = $result[$i]['AddressRoom'];
        $floor = $result[$i]['AddressFloor'];
        $block = $result[$i]['AddressBlock'];
        $bldg = $result[$i]['AddressBldg'];
        $street = $result[$i]['AddressStreet'];
        $district = $result[$i]['AddressDistrict'];

        $applicationDetails[$_applicationId]['recordId'] = $result[$i]['RecordID'];
        $studentName = $result[$i]['ChineseName'];
        if ($address_lang == "en") {
            if ($room != '')
                $address .= $kis_lang_en['Admission']['MINGWAI']['room'] . " " . $room . ", ";

            if ($room != '')
                $address .= $kis_lang_en['Admission']['MINGWAI']['floor'] . " " . $floor . ", ";

            if ($block != "")
                $address .= $kis_lang_en['Admission']['MINGWAI']['block'] . " " . $block . ", <br/>";
            else
                $address .= "<br/>";

            if ($bldg != "")
                $address .= $bldg . ", <br/>";

            if ($street != "")
                $address .= $street . ", <br/>";

            if ($street != "")
                $address .= $district . ", ";

            if ($estate != "")
                $address .= $estate;
        } else {
            if ($estate != "")
                $address .= $estate;

            if ($district != "")
                $address .= $district . "<br/>";
            else
                $address .= "<br/>";

            if ($street != "")
                $address .= $street . "<br/>";

            if ($bldg != "")
                $address .= $bldg . "<br/>";

            if ($block != "")
                $address .= $block . $kis_lang_b5['Admission']['MINGWAI']['block'];

            if ($floor != "")
                $address .= $floor . $kis_lang_b5['Admission']['MINGWAI']['floor'];

            if ($room != "")
                $address .= $room . $kis_lang_b5['Admission']['MINGWAI']['room'];
        }
        $applicationDetails[$_applicationId]['studentName'] = $studentName . "家長" . " (" . $_applicationId . ")";
        $applicationDetails[$_applicationId]['address'] = $address;
    }

    $applicationDetails = array_values($applicationDetails);
    // for ($i = 0; $i < 5; $i++)
    // $applicationDetails = array_merge($applicationDetails, $applicationDetails);
    $applicationDetails = array_chunk($applicationDetails, 10);
    ob_start();
    ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
            "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style>
body {
	background-color: #fff;
	color: #000;
	font-family: Calibri, Arial, Helvetica, Verdana, sans-serif, "msjh";
	font-size: 12px;
	line-height: 1.2em;
	margin: 0;
	padding: 0;
}

td { //
	border: 1px solid black;
}

.label {
	width: 75mm;
	height: 37mm;
	font-size: 18px;
	text-align: left;
	vertical-align: middle;
	padding-top: 2mm;
}
</style>
</head>
<body>
	<div id="content">
		<div class="page_wrapper">

            <?php foreach ($applicationDetails as $applicationIndex => $applicationDetail): ?>
                <?php if ($applicationIndex > 0) { ?>
                    <div
				style="page-break-before: always; line-height: 0.1mm; height: 0.1mm; font-size: 0.1mm;">&nbsp;
			</div>
                <?php } ?>

                <table cellpadding="0" cellspacing="0">
                <?php $count = 0;?>
                    <?php
        foreach ($applicationDetail as $detail) :
            ?>
                        <?php

if ($count == 0) {
                echo "<tr>";
            }
            ?>
                        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                        <?php

if ($count == 1) {
                echo "<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>";
            }
            ?>
                            <td class="label"><b><?= $detail['address'] ?></b>
					<br />
				<br /> <span style="font-size: 5mm;"><b><?= $detail['studentName'] ?></b></span>
				</td>
                        <?php $count++;?>
                        <?php

if ($count == 2) {
                echo "</tr>";
                $count = 0;
            }
            ?>
                    <?php endforeach; ?>
                </table>
            <?php endforeach; ?>
        </div>
	</div>
</body>
</html>
<?php
    $pageHeader = ob_get_clean();

    $mpdf->WriteHTML($pageHeader);
    $mpdf->Output();
}
