<?php
# modifying by: Henry
 
/********************
 * Log :
 * Date		2013-10-21 [Henry]
 * 			File Created
 * 
 ********************/
$PATH_WRT_ROOT = "../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_ui.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_utility.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_apps.php");
//include_once($PATH_WRT_ROOT."includes/json.php");
include_once("../config.php");

//for the customization
include_once($PATH_WRT_ROOT."includes/admission/".$setting_path_ip_rel."/config.php");
$intranet_session_language = $admission_cfg['DefaultLang'];
include_once($PATH_WRT_ROOT."includes/admission/".$setting_path_ip_rel."/libadmission_cust.php");
include_once($PATH_WRT_ROOT."includes/admission/".$setting_path_ip_rel."/libadmission_ui_cust.php");

include_once($PATH_WRT_ROOT."lang/lang.".$intranet_session_language.".php");
include_once($PATH_WRT_ROOT."lang/kis/lang_common_".$intranet_session_language.".php");
include_once($PATH_WRT_ROOT."lang/kis/apps/lang_admission_".$intranet_session_language.".php");
include_once($PATH_WRT_ROOT."lang/admission_lang.".$intranet_session_language.".php");


intranet_opendb();

//$libjson 	= new JSON_obj();
$libkis 	= new kis('');
$lac		= new admission_cust();
$lauc		= new admission_ui_cust();
$li			=new interface_html();

if (!$plugin['eAdmission'])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT("","../");
	exit;
}

$libkis_admission = $libkis->loadApp('admission');
	
//Get the intruction content
//$applicationSetting = $libkis_admission->getApplicationSetting($libkis_admission->schoolYearID);
$result = $lac->getApplicationResult($_REQUEST['InputStudentDateOfBirth'], $_REQUEST['InputStudentBirthCertNo'], $libkis_admission->schoolYearID, $_REQUEST['InputApplicationID']);

//$x .= '<table class="form_table" style="font-size: 13px">';
//$x .= '<tr>';
//$x .= '<td class="field_title">申請編號 Application #</td>';
//$x .= '<td>'.$result['ApplicationID'].'</td>';
//$x .= '</tr>';
//$x .= '<td class="field_title">申請者姓名 Name of Applicant</td>';
//$x .= '<td>'.str_replace(',',' ',$result['ChineseName']).' ('.str_replace(',',' ',$result['EnglishName']).')</td>';
//$x .= '</tr>';
//$x .= '<tr>';
//$x .= '<td class="field_title">面試時段 Interview Timeslot</td>';
//if($result['Date'])
//	$x .= '<td>日期 Date: '.$result['Date'].' <br/>時段 Timeslot: '.substr($result['StartTime'], 0, -3).' ~ '.substr($result['EndTime'], 0, -3).($result['GroupName']?' <br/>'.($admission_cfg['interview_arrangment']['interview_group_type'] == 'Room'?'面試房間 Interview Room':'組別 Group').': '.$result['GroupName']:'').'</td>';
//else
//	$x .= '<td>面試時段尚未編排 No interview timeslot assigned</td>';
//$x .= '</tr>';
//$x .= '</table>';

$x = $result['ApplicationID'];

if($x != ''){
	$_SESSION['KIS_ApplicationID'] = $x;
	$_SESSION['KIS_StudentDateOfBirth'] = $_REQUEST['InputStudentDateOfBirth'];
	$_SESSION['KIS_StudentBirthCertNo'] = $_REQUEST['InputStudentBirthCertNo'];
}

echo $x;
//echo debug_pr($_REQUEST).debug_pr($_FILES);
?>