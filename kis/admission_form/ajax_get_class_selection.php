<?php
# modifying by: Henry
 
/********************
 * Log :
 * Date		2017-09-01 [Pun]
 * 			added  SSGC cust
 * 
 * Date		2015-07-15 [Omas]
 * 			added  $sys_custom['KIS_Admission']['CHIUCHUNKG']['Settings'] get a cust drop down list
 * 
 * Date		2013-10-21 [Henry]
 * 			File Created
 * 
 ********************/
$PATH_WRT_ROOT = "../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_ui.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_utility.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_apps.php");
//include_once($PATH_WRT_ROOT."includes/json.php");
include_once("../config.php");

//for the customization
include_once($PATH_WRT_ROOT."includes/admission/".$setting_path_ip_rel."/config.php");
$intranet_session_language = $admission_cfg['DefaultLang'];
include_once($PATH_WRT_ROOT."includes/admission/".$setting_path_ip_rel."/libadmission_cust.php");
include_once($PATH_WRT_ROOT."includes/admission/".$setting_path_ip_rel."/libadmission_ui_cust.php");

include_once($PATH_WRT_ROOT."lang/lang.".$intranet_session_language.".php");
include_once($PATH_WRT_ROOT."lang/kis/lang_common_".$intranet_session_language.".php");
include_once($PATH_WRT_ROOT."lang/kis/apps/lang_admission_".$intranet_session_language.".php");
include_once($PATH_WRT_ROOT."lang/admission_lang.".$intranet_session_language.".php");




intranet_opendb();

//$libjson 	= new JSON_obj();
$libkis 	= new kis('');
$lac		= new admission_cust();
$lauc		= new admission_ui_cust();
$li			=new interface_html();

if (!$plugin['eAdmission'])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT("","../");
	exit;
}

$libkis_admission = $libkis->loadApp('admission');

$fileData = $_REQUEST;
$formData = $_REQUEST;
//henry modified 20140127
if($setting_path_ip_rel == 'icms.eclass.hk')
	$applicationSetting = $libkis_admission->getApplicationSetting("all_year");
else
	$applicationSetting = $libkis_admission->getApplicationSetting($_REQUEST['SchoolYearID']);

$dayType = $applicationSetting[$_REQUEST['sus_status']]['DayType'];
$dayTypeArr = explode(',',$dayType);

//$classLevelName = $applicationSetting[$_REQUEST['sus_status']]['ClassLevelName'];
//foreach($dayTypeArr as $aDayType){
//			//$dayTypeOption .= $Lang['Admission']['TimeSlot'][$aDayType]." ".$li->Get_Number_Selection('OthersApplyDayType'.$aDayType, '1', count($dayTypeArr))." ";
//}
//////////////// new added


if($sys_custom['KIS_Admission']['SSGC']['Settings']){
	$classLevel = $lac->getClassLevel();
	$level = $classLevel[$_REQUEST['sus_status']];
	if($level != 'K1'){
	    echo $Lang['Admission']['TimeSlot'][$dayTypeArr[0]];
	    echo "<input type=\"hidden\" name=\"OthersApplyDayType1\" value=\"{$dayTypeArr[0]}\">";
	    exit;
	}
}

$selectedArray = array();
if($setting_path_ip_rel == 'icms.eclass.hk')
	$classLevel = $lac->getClassLevel();
for($i=0;$i<count($dayTypeArr);$i++){
	if($setting_path_ip_rel == 'icms.eclass.hk')
		$applyTimeSlotAry[] = array($i+1,$Lang['Admission']['icms'][$classLevel[$_REQUEST['sus_status']]]['TimeSlot'][$dayTypeArr[$i]]);
	else if($sys_custom['KIS_Admission']['CSM']['Settings'])
		$applyTimeSlotAry[] = array($dayTypeArr[$i],$Lang['Admission']['csm']['TimeSlot'][$dayTypeArr[$i]]);
	else
		$applyTimeSlotAry[] = array($dayTypeArr[$i],$Lang['Admission']['TimeSlot'][$dayTypeArr[$i]]);
	
	if($formData['OthersApplyDayType'][$i+1]){
	    $selectedArray[] = $formData['OthersApplyDayType'][$i+1];
	}
}
for($i=0;$i<count($dayTypeArr);$i++){
	if($setting_path_ip_rel == 'icms.eclass.hk'){
		$dayTypeOption .= $li->Get_Number_Selection('OthersApplyDayType'.($i+1), '1', $admission_cfg['DayTypeSelectQty'][$classLevel[$_REQUEST['sus_status']]])." ".$Lang['Admission']['icms'][$classLevel[$_REQUEST['sus_status']]]['TimeSlot'][$dayTypeArr[$i]]."<br/>";
	}
	else if($sys_custom['KIS_Admission']['CHIUCHUNKG']['Settings']){
		foreach((array)$admission_cfg['classtype'] as $_key => $_val){
			if($dayTypeArr[$i] == $_val){
				$CHIUCHUNKG_cust_Arr[$_key] = $_val;
				break;
			}
		}
	}
	else if($admission_cfg['SingleClassSelection']){
		//exit the loop
		break;
	}
	else if($setting_path_ip_rel == 'mosgraceful.eclass.hk' || $setting_path_ip_rel == 'ylsyk.eclass.hk'){
		$dayTypeOption .=$kis_lang['Admission']['Option'].' '.($i+1).' Option '.($i+1).' '.$li->GET_SELECTION_BOX($applyTimeSlotAry, "name='OthersApplyDayType".($i+1)."' id='OthersApplyDayType".($i+1)."' class='timeslotselection'",((count($dayTypeArr)==1 || $sys_custom['KIS_Admission']['CSM']['Settings'])?'':$kis_lang['Admission']['Nil'].' Not Applicable'));
	}
	else if($setting_path_ip_rel == 'pe-mingwai.eclass.hk' || $setting_path_ip_rel == 'mingwai.eclass.hk'){
		$dayTypeOption .=' Option '.($i+1).' '.$kis_lang['Admission']['Option'].' '.($i+1).$li->GET_SELECTION_BOX($applyTimeSlotAry, "name='OthersApplyDayType".($i+1)."' id='OthersApplyDayType".($i+1)."' class='timeslotselection'",((count($dayTypeArr)==1 || $sys_custom['KIS_Admission']['CSM']['Settings'])?'':'Not Applicable '.$kis_lang['Admission']['Nil']));
	}
	else if($sys_custom['KIS_Admission']['TBCPK']['Settings']){
		$dayTypeOption .= $kis_lang['Admission']['Option'].' '.($i+1).' '.($i+1).($i==0?'st':($i==1?'nd':'rd')).' Choice '.$li->GET_SELECTION_BOX($applyTimeSlotAry, "name='OthersApplyDayType".($i+1)."' id='OthersApplyDayType".($i+1)."' class='timeslotselection'",((count($dayTypeArr)==1 || $sys_custom['KIS_Admission']['CSM']['Settings'])?'':$kis_lang['Admission']['Nil'].' Not Applicable'));
	}
	else if($sys_custom['KIS_Admission']['SSGC']['Settings']){
		$dayTypeOption .=$kis_lang['Admission']['Option'].' '.($i+1).' Option '.($i+1).' '.$li->GET_SELECTION_BOX($applyTimeSlotAry, "name='OthersApplyDayType".($i+1)."' id='OthersApplyDayType".($i+1)."' class='timeslotselection' style='margin-top: 5px;'",(count($dayTypeArr)==1?'':$kis_lang['Admission']['Nil'].' Not Applicable'), $selectedArray[$i]) .'<br/>';
	}
	else if($sys_custom['KIS_Admission']['CSM']['Settings']){
		$dayTypeOption .=(count($dayTypeArr)>1?$kis_lang['Admission']['Option'].' '.($i+1).' ':'').$li->GET_SELECTION_BOX($applyTimeSlotAry, "name='OthersApplyDayType".($i+1)."' id='OthersApplyDayType".($i+1)."' class='timeslotselection'",(count($dayTypeArr)==1?'':$kis_lang['Admission']['Nil']), $selectedArray[$i]);
	}
	else
		$dayTypeOption .=$kis_lang['Admission']['Option'].' '.($i+1).' '.$li->GET_SELECTION_BOX($applyTimeSlotAry, "name='OthersApplyDayType".($i+1)."' id='OthersApplyDayType".($i+1)."' class='timeslotselection'",(count($dayTypeArr)==1?'':$kis_lang['Admission']['Nil']), $selectedArray[$i]);
}
if(!empty($CHIUCHUNKG_cust_Arr)&&$sys_custom['KIS_Admission']['CHIUCHUNKG']['Settings']){
	$dayTypeOption =  $lauc->getSelectByConfig($CHIUCHUNKG_cust_Arr,'ApplyClass');
}
else if($admission_cfg['SingleClassSelection']){
	foreach((array)$applyTimeSlotAry as $_optionInfoArr){
		$optionValue = $_optionInfoArr[0];
		$optionLang = $_optionInfoArr[1];
		$singleSelectArr[$optionValue] = $optionLang; 
	}
	$PlsSelectLang = $Lang['Admission']['PleaseSelect'];
	$dayTypeOption = getSelectByAssoArray($singleSelectArr,'id="ApplyClass" name="ApplyClass"','',0,0,$PlsSelectLang);
}
////////////////////
if($setting_path_ip_rel == 'icms.eclass.hk')
	$dayTypeOption .='('.$Lang['Admission']['icms']['msg']['applyDayTypeHints'].')';
else if($sys_custom['KIS_Admission']['CHIUCHUNKG']['Settings'])
	$dayTypeOption .= '';
else if($admission_cfg['SingleClassSelection'])
	$dayTypeOption .= '';
else if($setting_path_ip_rel == 'mosgraceful.eclass.hk'){
	$dayTypeOption .='<br/>('.$Lang['Admission']['icms']['msg']['applyDayTypeHints'].')';
	$dayTypeOption .='<br/>(Please choose your priorities(at least two), blank if not consider)';
}else if($setting_path_ip_rel == 'ylsyk.eclass.hk'){
	$dayTypeOption .='<br/>('.$Lang['Admission']['msg']['applyDayTypeHints'].')';
	$dayTypeOption .='<br/>(Please choose your priorities(at least one), blank if not consider")';
}else if($setting_path_ip_rel == 'pe-mingwai.eclass.hk' || $setting_path_ip_rel == 'mingwai.eclass.hk'){
	$dayTypeOption .='<br/>(Please choose your priorities(at least one), blank if not consider")';
	$dayTypeOption .='<br/>('.$Lang['Admission']['msg']['applyDayTypeHints'].')';
}else if($sys_custom['KIS_Admission']['TBCPK']['Settings']){
	$dayTypeOption .='<br/>('.$Lang['Admission']['msg']['applyDayTypeHints'].')';
	$dayTypeOption .='<br/>(Please prioritize your choices)';
}else if($sys_custom['KIS_Admission']['CSM']['Settings']){
	if(count($dayTypeArr) > 1){
		$dayTypeOption .='('.$Lang['Admission']['csm']['msg']['applyDayTypeHints'].')';
	}
}else if($sys_custom['KIS_Admission']['SSGC']['Settings']){
	$dayTypeOption .='('.$Lang['Admission']['icms']['msg']['applyDayTypeHints'].')';
	$dayTypeOption .='<br/>(Please choose your priorities(at least one), blank if not consider)';
}
else
	$dayTypeOption .='('.$Lang['Admission']['msg']['applyDayTypeHints'].')';
echo $dayTypeOption;
//echo debug_pr($_REQUEST).debug_pr($_FILES);
?>