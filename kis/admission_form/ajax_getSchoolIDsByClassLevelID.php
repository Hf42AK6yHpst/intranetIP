<?php
# modifying by: Henry
 
/********************
 * Log :
 * Date		2013-10-21 [Henry]
 * 			File Created
 * 
 ********************/
$PATH_WRT_ROOT = "../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_ui.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_utility.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_apps.php");
//include_once($PATH_WRT_ROOT."includes/json.php");
include_once("../config.php");

//for the customization
include_once($PATH_WRT_ROOT."includes/admission/".$setting_path_ip_rel."/config.php");
$intranet_session_language = $admission_cfg['DefaultLang'];
include_once($PATH_WRT_ROOT."includes/admission/".$setting_path_ip_rel."/libadmission_cust.php");
include_once($PATH_WRT_ROOT."includes/admission/".$setting_path_ip_rel."/libadmission_ui_cust.php");

include_once($PATH_WRT_ROOT."lang/lang.".$intranet_session_language.".php");
include_once($PATH_WRT_ROOT."lang/kis/lang_common_".$intranet_session_language.".php");
include_once($PATH_WRT_ROOT."lang/kis/apps/lang_admission_".$intranet_session_language.".php");
include_once($PATH_WRT_ROOT."lang/admission_lang.".$intranet_session_language.".php");




intranet_opendb();

//$libjson 	= new JSON_obj();
$libkis 	= new kis('');
$lac		= new admission_cust();
$lauc		= new admission_ui_cust();
$li			=new interface_html();

if (!$plugin['eAdmission'])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT("","../");
	exit;
}

$libkis_admission = $libkis->loadApp('admission');

$fileData = $_REQUEST;
$formData = $_REQUEST;

//should be add flag [start]
$schoolYearIDs = $lac->getSchoolIDsByClassLevelID($formData['sus_status']);
$academicYearID = array();
for($i=0; $i<count($schoolYearIDs); $i++){
	$academicYearID[] = array($schoolYearIDs[$i]['SchoolYearID'],getAcademicYearByAcademicYearID($schoolYearIDs[$i]['SchoolYearID']));
}
//should be add flag [end]

$x = $li->GET_SELECTION_BOX($academicYearID, "name='SchoolYearID' id='SchoolYearID'",'', $lac->schoolYearID);

echo $x;
//echo debug_pr($_REQUEST).debug_pr($_FILES);
?>