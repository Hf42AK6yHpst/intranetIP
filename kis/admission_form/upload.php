<?php
# modifying by: 
 
/********************
 * Log :
 * Date		2014-01-15 [Carlos]
 * 			Modified other attachments section to follow attachment settings
 * 
 * Date		2013-10-28 [Henry]
 * 			File Created
 * 
 ********************/
$PATH_WRT_ROOT = "../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_ui.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_utility.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_apps.php");
//include_once($PATH_WRT_ROOT."includes/json.php");
include_once("../config.php");
include_once($PATH_WRT_ROOT."lang/lang.".$intranet_session_language.".php");
include_once($PATH_WRT_ROOT."lang/kis/lang_common_".$intranet_session_language.".php");
include_once($PATH_WRT_ROOT."lang/kis/apps/lang_admission_".$intranet_session_language.".php");
include_once($PATH_WRT_ROOT."lang/admission_lang.".$intranet_session_language.".php");

//for the customization
include_once($PATH_WRT_ROOT."includes/admission/".$setting_path_ip_rel."/config.php");
include_once($PATH_WRT_ROOT."includes/admission/".$setting_path_ip_rel."/libadmission_cust.php");
include_once($PATH_WRT_ROOT."includes/admission/".$setting_path_ip_rel."/libadmission_ui_cust.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

intranet_opendb();

//$libjson 	= new JSON_obj();
$libkis 	= new kis('');
$lac		= new admission_cust();
$lauc		= new admission_ui_cust();
$lfs		=new libfilesystem();

//create and store the temp file path
//The path to store the uploaded files temporary
$tempFolderName = uniqid();
$tempFolderPath =$admission_cfg['FilePath']."/temp/".$tempFolderName;

//if the temp folder is generated before, then remove it
if($_REQUEST['tempFolderName'] != ""){
	$lfs->folder_remove_recursive($admission_cfg['FilePath']."/temp/".$_REQUEST['tempFolderName']);
}

//For debugging only 20131028
$lfs->folder_remove_recursive($admission_cfg['FilePath']."/temp");

//Create a temp folder
while($lfs->folder_new($tempFolderPath.'/personal_photo/') == 0){
	$tempFolderName = uniqid();
	$tempFolderPath =$admission_cfg['FilePath']."/temp/".$tempFolderName;
}

//for StudentPersonalPhoto
list($filename,$ext) = explode('.',$_FILES['StudentPersonalPhoto']['name']);
$timestamp = date("YmdHis");
$filename = base64_encode($filename.'_'.$timestamp);

if(!$lac->uploadAttachment("personal_photo",$_FILES['StudentPersonalPhoto'], $tempFolderPath.'/personal_photo'))
	$success = false;


$attachment_settings = $lac->getAttachmentSettings();
$file_index = 0;
while(isset($_FILES['OtherFile'.$file_index]) && isset($attachment_settings[$file_index]))
{
	list($filename,$ext) = explode('.',$_FILES['OtherFile'.$file_index]['name']);
	$timestamp = date("YmdHis");
	$filename = base64_encode($filename.'_'.$timestamp);
	
	if(!$lac->uploadAttachment($attachment_settings[$file_index]['AttachmentName'],$_FILES['OtherFile'.$file_index], $tempFolderPath.'/other_files')){
		$success = false;
	}
	
	$file_index+=1;
}

/*
//for OtherFile
list($filename,$ext) = explode('.',$_FILES['OtherFile']['name']);
$timestamp = date("YmdHis");
$filename = base64_encode($filename.'_'.$timestamp);
	
if(!$lac->uploadAttachment("birth_cert",$_FILES['OtherFile'], $tempFolderPath.'/other_files'))
	$success = false;

//for OtherFile1
list($filename,$ext) = explode('.',$_FILES['OtherFile1']['name']);
$timestamp = date("YmdHis");
$filename = base64_encode($filename.'_'.$timestamp);

if(!$lac->uploadAttachment("immunisation_record",$_FILES['OtherFile1'], $tempFolderPath.'/other_files'))
	$success = false;
*/

sleep(1);
?>
<script language="javascript" type="text/javascript">window.top.window.stopUpload('<?php echo $tempFolderName; ?>');</script>   