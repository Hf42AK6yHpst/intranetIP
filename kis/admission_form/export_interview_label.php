<?php
$PATH_WRT_ROOT = "../../";

include_once($PATH_WRT_ROOT . "includes/global.php");
include_once($PATH_WRT_ROOT . "includes/libdb.php");
include_once($PATH_WRT_ROOT . "includes/libinterface.php");
include_once($PATH_WRT_ROOT . "includes/kis/libkis.php");
include_once($PATH_WRT_ROOT . "includes/kis/libkis_ui.php");
include_once($PATH_WRT_ROOT . "includes/kis/libkis_utility.php");
include_once($PATH_WRT_ROOT . "includes/kis/libkis_apps.php");
//include_once($PATH_WRT_ROOT."includes/json.php");
include_once("../config.php");
include_once($PATH_WRT_ROOT . "lang/kis/lang_common_" . $intranet_session_language . ".php");
include_once($PATH_WRT_ROOT . "lang/kis/apps/lang_admission_" . $intranet_session_language . ".php");
include_once($PATH_WRT_ROOT . "lang/admission_lang." . $intranet_session_language . ".php");

//for the customization
include_once($PATH_WRT_ROOT . "includes/admission/" . $setting_path_ip_rel . "/config.php");
include_once($PATH_WRT_ROOT . "includes/admission/" . $setting_path_ip_rel . "/libadmission_cust.php");
//for the export
require_once($PATH_WRT_ROOT . "includes/mpdf/mpdf.php");


intranet_opendb();
$libkis = new kis('');
$lac = new admission_cust();
$mpdf = new \mPDF($mode = '', $format = 'A4', $default_font_size = 0, $default_font = 'msjh', $marginLeft = 0, $marginRight = 0, $marginTop = 0, $marginBottom = 0/*,
	    $marginHeader=9,
	    $marginFooter=9,
	    $orientation='P'*/
);
$mpdf->mirrorMargins = 1;

# Temp Assign memory of this page
ini_set("memory_limit", "150M");

#### Helper START ####
function sortResult($a,$b){
    if($a['ClassLevelID'] != $b['ClassLevelID']){
        return $a['ClassLevelID'] < $b['ClassLevelID'];
    }elseif($a['Date'] != $b['Date']){
        return strcmp($a['Date'], $b['Date']);
    }elseif($a['StartTime'] != $b['StartTime']){
        return strcmp($a['StartTime'], $b['StartTime']);
    }elseif($a['EndTime'] != $b['EndTime']){
        return strcmp($a['EndTime'], $b['EndTime']);
    }elseif($a['GroupName'] != $b['GroupName']){
        return strcmp($a['GroupName'], $b['GroupName']);
    }
    return strcmp($a['application_id'], $b['application_id']);
}
#### Helper END ####

$applicationDetails = array();
if ($_SESSION['UserType'] == USERTYPE_STAFF) {
    $result = $lac->getInterviewListAry($interviewAry, $date = '', $startTime = '', $endTime = '', $keyword = '', '', '', $round);

    for ($i = 0; $i < count($result); $i++) {
        $_applicationId = $result[$i]['application_id'];
        if($filterApplicantAry && !in_array($result[$i]['other_record_id'],(array)$filterApplicantAry)){
            continue;
        }

        //$date = $result[$i]['Date'];
        $startTime = substr($result[$i]['StartTime'], 0, -3);
        $EndTime = substr($result[$i]['EndTime'], 0, -3);
        $GroupName = $result[$i]['GroupName'];

        $applicationDetails[$_applicationId]['application_id'] = $result[$i]['application_id'];
        $applicationDetails[$_applicationId]['student_name'] = $result[$i]['student_name'];
        $applicationDetails[$_applicationId]['ClassLevelID'] = $result[$i]['ClassLevelID'];
        $applicationDetails[$_applicationId]['Date'] = $result[$i]['Date'];
        $applicationDetails[$_applicationId]['StartTime'] = $result[$i]['StartTime'];
        $applicationDetails[$_applicationId]['EndTime'] = $result[$i]['EndTime'];
        $applicationDetails[$_applicationId]['GroupName'] = $result[$i]['GroupName'];
        $applicationDetails[$_applicationId]['Details'] = '';
        if($_POST['showTime']){
            $applicationDetails[$_applicationId]['Details'] .= "{$startTime}-{$EndTime}";
        }
        if($_POST['showRoom']){
            $applicationDetails[$_applicationId]['Details'] .= " ({$GroupName})";
        }
    }
    usort($applicationDetails, 'sortResult');
    
    $applicationDetails = array_values($applicationDetails);
//    for ($i = 0; $i < 5; $i++)
//        $applicationDetails = array_merge($applicationDetails, $applicationDetails);
    $applicationDetails = array_chunk($applicationDetails, 7);
    ob_start();
    ?>
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
            "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <style>

            body {
                background-color: #fff;
                color: #000;
                font-family: Calibri, Arial, Helvetica, Verdana, sans-serif, "msjh";
                font-size: 12px;
                line-height: 1.2em;
                margin: 0;
                padding: 0;
            }

            td {
                //border: 1px solid black;
            }

            .label {
                width: 70mm;
                height: 32mm;
                font-size: 6mm;
                text-align: center;
                vertical-align: middle;
                padding-top: 2mm;
            }

            .labelDate {
                width: 70mm;
                height: 10mm;
                font-size: 3mm;
                vertical-align: bottom;
                padding: 3mm;
                padding-left: 5mm;
            }

            .name {
                height: 20mm;
                vertical-align: middle;
                border: 1px solid red;
                display: block;
                width: 100%
            }

            .dateTime {
                font-size: 3mm;
                margin-top: 5mm;
                height: 10mm;
                vertical-align: bottom;
            }
        </style>
    </head>
    <body>
    <div id="content">
        <div class="page_wrapper">

            <?php foreach ($applicationDetails as $applicationIndex => $applicationDetail): ?>
                <?php if ($applicationIndex > 0) { ?>
                    <div style="page-break-before: always;line-height: 0.1mm;height: 0.1mm;font-size: 0.1mm;">&nbsp;
                    </div>
                <?php } ?>

                <table cellpadding="0" cellspacing="0">
                    <?php
                    foreach ($applicationDetail as $detail):
                        ?>
                        <tr>
                            <td class="label">
                                <?= $detail['student_name'] ?>
                            </td>
                            <td class="label">
                                <?= $detail['student_name'] ?>
                                <br/>
                                <span style="font-size: 4mm;">(家長)</span>
                            </td>
                            <td class="label">
                                <?= $detail['student_name'] ?>
                                <br/>
                                <span style="font-size: 4mm;">(家長)</span>
                            </td>
                        </tr>
                        <tr>
                            <td class="labelDate">
                                <?= $detail['Details'] ?>
                            </td>
                            <td class="labelDate">
                                <?= $detail['Details'] ?>
                            </td>
                            <td class="labelDate">
                                <?= $detail['Details'] ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </table>
            <?php endforeach; ?>
        </div>
    </div>
    </body>
    </html>
    <?php
    $pageHeader = ob_get_clean();

    $mpdf->WriteHTML($pageHeader);
    $mpdf->Output();
}
