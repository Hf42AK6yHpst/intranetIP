<?php
# modifying by: Henry
 
/********************
 * Log :
 * Date		2014-01-15 [Carlos]
 * 			Modified other attachments section to follow attachment settings
`* 
 * Date		2013-10-08 [Henry]
 * 			File Created
 * 
 ********************/
 
$PATH_WRT_ROOT = "../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_ui.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_utility.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_apps.php");
//include_once($PATH_WRT_ROOT."includes/json.php");

include_once("../config.php");

//for the customization
include_once($PATH_WRT_ROOT."includes/admission/".$setting_path_ip_rel."/config.php");
$intranet_session_language = $admission_cfg['DefaultLang'];
include_once($PATH_WRT_ROOT."includes/admission/".$setting_path_ip_rel."/libadmission_cust.php");
include_once($PATH_WRT_ROOT."includes/admission/".$setting_path_ip_rel."/libadmission_ui_cust.php");

include_once($PATH_WRT_ROOT."lang/lang.".$intranet_session_language.".php");
include_once($PATH_WRT_ROOT."lang/kis/lang_common_".$intranet_session_language.".php");
include_once($PATH_WRT_ROOT."lang/kis/apps/lang_admission_".$intranet_session_language.".php");
include_once($PATH_WRT_ROOT."lang/admission_lang.".$intranet_session_language.".php");


intranet_opendb();

//$libjson 	= new JSON_obj();
$libkis 	= new kis('');
$lac		= new admission_cust();
$lauc		= new admission_ui_cust();
$li			=new interface_html();

if (!$plugin['eAdmission'])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT("","../");
	exit;
}

$_SESSION['SSV_PRIVILEGE']['school']['name'] = GET_SCHOOL_NAME();
$school = $libkis->getUserSchool(); 

//$libkis_admission = $libkis->loadApp('admission');
//Get the index content
$basic_settings = $lac->getBasicSettings();

if($lac->schoolYearID)
	$instruction = $basic_settings['generalInstruction'];
else{
	//$instruction = $lac->displayWarningMsg('notyetsetnextschoolyear');
	$instruction = '<fieldset class="warning_box">
						<legend>'.$Lang['Admission']['warning'].'</legend>
						<ul>
							<li>'.$Lang['Admission']['msg']['noclasslevelapply'].'</li>
						</ul>
					</fieldset>';
}	
	
$religion_selection = $lac->displayPresetCodeSelection("RELIGION", "StudentReligion", $result[0]['RELIGION']);
//$religion_selected = $lac->returnPresetCodeName("RELIGION", $formData['StudentReligion']);

$main_content = '<form id="form1" name="form1" method="POST" action="confirm_update.php" onSubmit="return submitForm()" ENCTYPE="multipart/form-data">';     

//The index page	
$main_content .= "<div id='step_index' style='display:auto'>";
$main_content .= $lauc->getIndexContent($instruction);
//$main_content .=$li->GET_ACTION_BTN('Back', "button", "goto('step_instruction','step_input_form')", "SubmitBtn", "", 0, "formbutton");
//$main_content .=$li->GET_ACTION_BTN('Next', "button", "goto('step_index','step_instruction')", "SubmitBtn", "", 0, "formbutton");
$main_content .= "</div>";

//The instruction page	
$main_content .= "<div id='step_instruction' style='display:none'>";
//$main_content .= $lauc->getInstructionContent($instruction);
//$main_content .=$li->GET_ACTION_BTN('Back', "button", "goto('step_instruction','step_index')", "SubmitBtn", "", 0, "formbutton");
//$main_content .=$li->GET_ACTION_BTN('Next', "button", "goto('step_instruction','step_input_form')", "SubmitBtn", "", 0, "formbutton");
$main_content .= "</div>";

//The input info page
$main_content .= "<div id='step_input_form' style='display:none'>";
$main_content .= $lauc->getApplicationForm();
//$main_content .=$li->GET_ACTION_BTN('Back', "button", "goto('step_input_form','step_instruction')", "SubmitBtn", "", 0, "formbutton");
//$main_content .=$li->GET_ACTION_BTN('Next', "button", "goto('step_input_form','step_docs_upload')", "SubmitBtn", "", 0, "formbutton");
$main_content .= "</div>";

//The docs upload page
$main_content .= "<div id='step_docs_upload' style='display:none'>";
$main_content .= $lauc->getDocsUploadForm();
//$main_content .=$li->GET_ACTION_BTN('Back', "button", "goto('step_docs_upload','step_input_form')", "SubmitBtn", "", 0, "formbutton");
//$main_content .=$li->GET_ACTION_BTN('Next', "button", "goto('step_docs_upload','step_confirm')", "SubmitBtn", "", 0, "formbutton");
//$main_content .=$li->GET_ACTION_BTN('Next', "button", "goto('step_docs_upload', 'step_confirm')", "SubmitBtn", "", 0, "formbutton");
$main_content .= "</div>";

//The docs upload page
$main_content .= "<div id='step_confirm' style='display:none'>";
//$main_content .= $lauc->getConfirmPageContent();
//$main_content .=$li->GET_ACTION_BTN('Back', "button", "goto('step_confirm','step_docs_upload')", "SubmitBtn", "", 0, "formbutton");
//$main_content .=$li->GET_ACTION_BTN('Next', "button", "goto('step_docs_upload','step_confirm')", "SubmitBtn", "", 0, "formbutton");
$main_content .= "</div>";

//Henry modifying 20131028
//$main_content .= '<iframe id="upload_target" name="upload_target" src="#" style="display:none"></iframe>';
//$main_content .= '<input type="hidden" name="tempFolderName" id="tempFolderName" value=""/>';

$main_content .= '</form>';

include_once("common_tmpl.php");
include_once($PATH_WRT_ROOT."includes/admission/".$setting_path_ip_rel."/commonJs.php");
?>


