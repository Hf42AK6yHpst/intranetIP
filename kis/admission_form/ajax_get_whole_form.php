<?php
// modifying by: Pun
/**
 * Change Log:
 * 2018-02-02 Pun
 * - File Created
 */
$PATH_WRT_ROOT = "../../";

include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "includes/kis/libkis.php");
include_once ($PATH_WRT_ROOT . "includes/kis/libkis_ui.php");
include_once ($PATH_WRT_ROOT . "includes/kis/libkis_utility.php");
include_once ($PATH_WRT_ROOT . "includes/kis/libkis_apps.php");
// include_once($PATH_WRT_ROOT."includes/json.php");
include_once ("../config.php");

// for the customization
include_once ($PATH_WRT_ROOT . "includes/admission/" . $setting_path_ip_rel . "/config.php");
$intranet_session_language = $admission_cfg['DefaultLang'];
include_once ($PATH_WRT_ROOT . "includes/admission/" . $setting_path_ip_rel . "/libadmission_cust.php");
include_once ($PATH_WRT_ROOT . "includes/admission/" . $setting_path_ip_rel . "/libadmission_ui_cust.php");

intranet_opendb();
$lauc = new admission_ui_cust();

// ### Lang START ####
if (method_exists($lauc, 'getAdmissionLang')) {
    $ApplicationID = $_POST['InputApplicationID'];
    $intranet_session_language = ($admission_cfg['Lang'][$lang]) ? $admission_cfg['Lang'][$lang] : $lauc->getAdmissionLang($ApplicationID, true);
    include ($PATH_WRT_ROOT . "lang/lang.{$intranet_session_language}.php");
    include ($PATH_WRT_ROOT . "lang/kis/lang_common_{$intranet_session_language}.php");
    include ($PATH_WRT_ROOT . "lang/kis/apps/lang_admission_{$intranet_session_language}.php");
    include ($PATH_WRT_ROOT . "lang/admission_lang.{$intranet_session_language}.php");
} elseif ($intranet_session_language == 'en') {
    $intranet_session_language = 'b5';
    include ($PATH_WRT_ROOT . "lang/lang.{$intranet_session_language}.php");
    include ($PATH_WRT_ROOT . "lang/kis/lang_common_{$intranet_session_language}.php");
    include ($PATH_WRT_ROOT . "lang/kis/apps/lang_admission_{$intranet_session_language}.php");
    include ($PATH_WRT_ROOT . "lang/admission_lang.{$intranet_session_language}.php");
    $LangB5 = $Lang;
    $kis_lang_b5 = $kis_lang;
    unset($Lang);
    unset($kis_lang);
    
    $intranet_session_language = 'en';
    include ($PATH_WRT_ROOT . "lang/lang.{$intranet_session_language}.php");
    include ($PATH_WRT_ROOT . "lang/kis/lang_common_{$intranet_session_language}.php");
    include ($PATH_WRT_ROOT . "lang/kis/apps/lang_admission_{$intranet_session_language}.php");
    include ($PATH_WRT_ROOT . "lang/admission_lang.{$intranet_session_language}.php");
    $LangEn = $Lang;
    $kis_lang_en = $kis_lang;
} else {
    $intranet_session_language = 'en';
    include ($PATH_WRT_ROOT . "lang/lang.{$intranet_session_language}.php");
    include ($PATH_WRT_ROOT . "lang/kis/lang_common_{$intranet_session_language}.php");
    include ($PATH_WRT_ROOT . "lang/kis/apps/lang_admission_{$intranet_session_language}.php");
    include ($PATH_WRT_ROOT . "lang/admission_lang.{$intranet_session_language}.php");
    $LangEn = $Lang;
    $kis_lang_en = $kis_lang;
    unset($Lang);
    unset($kis_lang);
    
    $intranet_session_language = 'b5';
    include ($PATH_WRT_ROOT . "lang/lang.{$intranet_session_language}.php");
    include ($PATH_WRT_ROOT . "lang/kis/lang_common_{$intranet_session_language}.php");
    include ($PATH_WRT_ROOT . "lang/kis/apps/lang_admission_{$intranet_session_language}.php");
    include ($PATH_WRT_ROOT . "lang/admission_lang.{$intranet_session_language}.php");
    $LangB5 = $Lang;
    $kis_lang_b5 = $kis_lang;
}
// ### Lang END ####

// $libjson = new JSON_obj();
$libkis = new kis('');
$lac = new admission_cust();
$li = new interface_html();

if (! $plugin['eAdmission']) {
    include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT("", "../");
    exit();
}

$libkis_admission = $libkis->loadApp('admission');

$applicationSetting = $libkis_admission->getApplicationSetting($libkis_admission->schoolYearID);

// #### Get instruction START #### //
$instruction = $applicationSetting[$_REQUEST['sus_status']]['FirstPageContent'];
if ($admission_cfg['MultipleLang']) {
    foreach ($admission_cfg['Lang'] as $index => $lang) {
        if ($lang == $intranet_session_language) {
            $instruction = $applicationSetting[$_REQUEST['sus_status']]["FirstPageContent{$index}"];
            break;
        }
    }
}
// #### Get instruction END #### //

if ($IsUpdate) {
    echo $lauc->getWholeApplicationUpdateForm($instruction, $ApplicationID);
} else {
    echo $lauc->getWholeApplicationForm($instruction, $libkis_admission->schoolYearID, $_REQUEST['sus_status'], $BirthCertNo);
}

$allowToUpdate = ($IsUpdate && $sys_custom['KIS_Admission']['ApplicantUpdateForm'] && ($lac->IsUpdatePeriod() || ($sys_custom['KIS_Admission']['UCCKE']['Settings'] && $lac->IsAfterUpdatePeriod())));
include ($PATH_WRT_ROOT . "includes/admission/" . $setting_path_ip_rel . "/commonJs.php");