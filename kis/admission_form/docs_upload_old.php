<?php
# modifying by: Henry
 
/********************
 * Log :
 * Date		2013-10-09 [Henry]
 * 			File Created
 * 
 ********************/
$PATH_WRT_ROOT = "../../";

include_once($PATH_WRT_ROOT."includes/settings.php");
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_ui.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_utility.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_apps.php");
include_once($PATH_WRT_ROOT."lang/kis/lang_common_".$intranet_session_language.".php");
include_once($PATH_WRT_ROOT."lang/kis/apps/lang_admission_".$intranet_session_language.".php");
include_once($PATH_WRT_ROOT."lang/admission_lang.".$intranet_session_language.".php");
include_once("../config.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
//for the customization
include_once($PATH_WRT_ROOT."includes/admission/".$kis_admission_school."/config.php");
include_once($PATH_WRT_ROOT."includes/admission/".$kis_admission_school."/libadmission_cust.php");
include_once($PATH_WRT_ROOT."includes/admission/".$kis_admission_school."/libadmission_ui_cust.php");

intranet_opendb();

$libkis 	= new kis('');
$libkis_admission = $libkis->loadApp('admission');
$lac		= new admission_cust($libkis_admission);
$lauc		= new admission_ui_cust();
$lfs		=new libfilesystem();
if (!$plugin['eAdmission'])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT("","../");
	exit;
}

$_SESSION['SSV_PRIVILEGE']['school']['name'] = GET_SCHOOL_NAME();
$school = $libkis->getUserSchool(); 

//$lfs->chmod_R($admission_cfg['FilePath'], 0644);

//The path to store the uploaded files temporary
$tempFolderPath =$admission_cfg['FilePath']."/temp/".uniqid();

//For debugging only
$lfs->folder_remove_recursive($admission_cfg['FilePath']."/temp");

//Create a temp folder
while($lfs->folder_new($tempFolderPath.'/personal_photo/') == 0){
	$tempFolderPath =$admission_cfg['FilePath']."/temp/".uniqid();
}
//////////debug_r($lac->uploadTempAttachment("personal_photo",$_FILES['StudentPersonalPhoto'], $tempFolderPath.'/personal_photo'));
/*
debug_pr("The selected class id is ".$_REQUEST['hidden_class']);

debug_pr("This path of the temp folder\n".$tempFolderPath);

//debug_pr("User uploaded photo\n".$_FILES['StudentPersonalPhoto']);

//Upload file to temp folder
debug_pr("Success to upload photo file to temp?\n".move_uploaded_file($_FILES['StudentPersonalPhoto']['tmp_name'], $tempFolderPath.'/personal_photo/'.$_FILES['StudentPersonalPhoto']['name']));

debug_pr("Infomation in hidden variable");
debug_pr(array(&$_FILES, &$_REQUEST));*/
//debug_pr($StudentEngName);
$main_content = $lauc->getDocsUploadForm();
include_once("common_tmpl.php");
?>
<script type="text/javascript">
function checkForm(form1) {
	//For debugging only
	return true;
	var otherFileExt = form1.OtherFile.files[0].name.split('.').pop().toUpperCase();
	var otherFileSize = form1.OtherFile.files[0].size;
	var maxFileSize = 10 * 1024 * 1024;
	
	if(form1.OtherFile.value==''){
		alert("<?= $i_alert_pleasefillin?> Student Personal Photo");	
		form1.OtherFile.focus();
		return false;
	} else if(otherFileExt !='JPG' && otherFileExt !='JPEG' && otherFileExt !='PNG' && otherFileExt !='GIF' && otherFileExt !='PDF'){
		alert("<?= $i_alert_pleasefillin?> Invalid file format");	
		form1.OtherFile.focus();
		return false;
	} else if(otherFileSize > maxFileSize){
		alert("<?= $i_alert_pleasefillin?> File too large");	
		form1.OtherFile.focus();
		return false;
	} else  {
		return true;
	}
}
function submit2(){
	$('#setItem').val(1);
	var obj = document.form1;
	if(!checkForm(obj)){
		return false;
	}
	obj.submit();
}
</script>

