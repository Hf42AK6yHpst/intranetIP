<?php
// modifying by: 

/**
 * ******************
 * Log :
 * Date 2019-07-11 [Henry]
 * Ignore time checking for getDecryptedText
 * 
 * Date 2015-10-14 [Henry]
 * File Created
 *
 * ******************
 */
$PATH_WRT_ROOT = "../../";

include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "includes/kis/libkis.php");
include_once ($PATH_WRT_ROOT . "includes/kis/libkis_ui.php");
include_once ($PATH_WRT_ROOT . "includes/kis/libkis_utility.php");
include_once ($PATH_WRT_ROOT . "includes/kis/libkis_apps.php");

include_once ("../config.php");

include_once ($PATH_WRT_ROOT . "includes/libfilesystem.php");

// for the customization
include_once ($PATH_WRT_ROOT . "includes/admission/" . $setting_path_ip_rel . "/config.php");
$intranet_session_language = $admission_cfg['DefaultLang'];
include_once ($PATH_WRT_ROOT . "includes/admission/" . $setting_path_ip_rel . "/libadmission_cust.php");
include_once ($PATH_WRT_ROOT . "includes/admission/" . $setting_path_ip_rel . "/libadmission_ui_cust.php");

// Get the intruction content
parse_str(getDecryptedText(urldecode($_REQUEST['id']), $admission_cfg['FilePathKey'], 1000000), $output);

intranet_opendb();
$lauc = new admission_ui_cust();

// ### Lang START ####
if (method_exists($lauc, 'getAdmissionLang')) {
    $intranet_session_language = $lauc->getAdmissionLang($output['ApplicationID'], true);
    include ($PATH_WRT_ROOT . "lang/lang.{$intranet_session_language}.php");
    include ($PATH_WRT_ROOT . "lang/kis/lang_common_{$intranet_session_language}.php");
    include ($PATH_WRT_ROOT . "lang/kis/apps/lang_admission_{$intranet_session_language}.php");
    include ($PATH_WRT_ROOT . "lang/admission_lang.{$intranet_session_language}.php");
} elseif ($intranet_session_language == 'en') {
    $intranet_session_language = 'b5';
    include ($PATH_WRT_ROOT . "lang/lang.{$intranet_session_language}.php");
    include ($PATH_WRT_ROOT . "lang/kis/lang_common_{$intranet_session_language}.php");
    include ($PATH_WRT_ROOT . "lang/kis/apps/lang_admission_{$intranet_session_language}.php");
    include ($PATH_WRT_ROOT . "lang/admission_lang.{$intranet_session_language}.php");
    $LangB5 = $Lang;
    $kis_lang_b5 = $kis_lang;
    unset($Lang);
    unset($kis_lang);
    
    $intranet_session_language = 'en';
    include ($PATH_WRT_ROOT . "lang/lang.{$intranet_session_language}.php");
    include ($PATH_WRT_ROOT . "lang/kis/lang_common_{$intranet_session_language}.php");
    include ($PATH_WRT_ROOT . "lang/kis/apps/lang_admission_{$intranet_session_language}.php");
    include ($PATH_WRT_ROOT . "lang/admission_lang.{$intranet_session_language}.php");
    $LangEn = $Lang;
    $kis_lang_en = $kis_lang;
} else {
    $intranet_session_language = 'en';
    include ($PATH_WRT_ROOT . "lang/lang.{$intranet_session_language}.php");
    include ($PATH_WRT_ROOT . "lang/kis/lang_common_{$intranet_session_language}.php");
    include ($PATH_WRT_ROOT . "lang/kis/apps/lang_admission_{$intranet_session_language}.php");
    include ($PATH_WRT_ROOT . "lang/admission_lang.{$intranet_session_language}.php");
    $LangEn = $Lang;
    $kis_lang_en = $kis_lang;
    unset($Lang);
    unset($kis_lang);
    
    $intranet_session_language = 'b5';
    include ($PATH_WRT_ROOT . "lang/lang.{$intranet_session_language}.php");
    include ($PATH_WRT_ROOT . "lang/kis/lang_common_{$intranet_session_language}.php");
    include ($PATH_WRT_ROOT . "lang/kis/apps/lang_admission_{$intranet_session_language}.php");
    include ($PATH_WRT_ROOT . "lang/admission_lang.{$intranet_session_language}.php");
    $LangB5 = $Lang;
    $kis_lang_b5 = $kis_lang;
}
// ### Lang END ####

$libkis = new kis('');
$lac = new admission_cust();
$lfs = new libfilesystem();

if (! $plugin['eAdmission']) {
    include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT("", "../");
    exit();
}

$_SESSION['SSV_PRIVILEGE']['school']['name'] = GET_SCHOOL_NAME();
$school = $libkis->getUserSchool();

$libkis_admission = $libkis->loadApp('admission');

$applicationSetting = $lac->getApplicationSetting($output['SchoolYearID'] ? $output['SchoolYearID'] : $lac->schoolYearID);

// #### Get last content START #### //
$lastContent = $applicationSetting[$output['sus_status']]['LastPageContent'];
if ($admission_cfg['MultipleLang']) {
    foreach ($admission_cfg['Lang'] as $index => $lang) {
        if ($lang == $intranet_session_language) {
            $lastContent = $applicationSetting[$output['sus_status']]["LastPageContent{$index}"];
            break;
        }
    }
}
// #### Get last content END #### //

if ($sys_custom['KIS_Admission']['MGF']['Settings'])
    $main_content = $lauc->getUpdateFinishPageContent($output['ApplicationID'], $lastContent, $output['SchoolYearID'], $output['sus_status']);
else
    $main_content = $lauc->getUpdateFinishPageContent($output['ApplicationID'], $lastContent, $output['SchoolYearID']);

include_once ("common_tmpl.php");
?>

