<?php
//using: 

/********************
 * 
 * Log :
 * Date		2020-07-02 [Tommy]
 * 			File Created
 * 
 ********************/

$PATH_WRT_ROOT = "../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_ui.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_utility.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_apps.php");
//include_once($PATH_WRT_ROOT."includes/json.php");
include_once("../config.php");
include_once($PATH_WRT_ROOT."lang/kis/lang_common_".$intranet_session_language.".php");
include_once($PATH_WRT_ROOT."lang/kis/apps/lang_admission_".$intranet_session_language.".php");
include_once($PATH_WRT_ROOT."lang/admission_lang.".$intranet_session_language.".php");

//for the customization
include_once($PATH_WRT_ROOT."includes/admission/".$setting_path_ip_rel."/config.php");
include_once($PATH_WRT_ROOT."includes/admission/".$setting_path_ip_rel."/libadmission_cust.php");
include_once($PATH_WRT_ROOT."includes/admission/".$setting_path_ip_rel."/libadmission_ui_cust.php");

//for the export
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_opendb();
$libkis 	= new kis('');
$lac		= new admission_cust();
$lauc		= new admission_ui_cust();

# Temp Assign memory of this page
ini_set("memory_limit", "250M"); 

$lexport = new libexporttext();

$ExportArr = array();
$exportColumn = array();

$interviewGroupName = $admission_cfg['interview_arrangment']['interview_group_name'];

if($_SESSION['UserType']==USERTYPE_STAFF){
    $academicYear = $lac->getYearName();
	# Define Column Title
	//$exportColumn = $lac->getExportHeader();
	
    //all data
    $classLevel = $lac->getClassLevel();

    $result = $lac->getInterviewListAryCust($interviewAry,  $date='', $startTime='', $endTime='', $keyword='', '', '', $round);
    $locationNum = 0;
    
    $interviewGroupArr = $lac->getInterviewGroupSetting($interviewAry);

    for($i=0;$i<count($result);$i++){
        $_applicationId = $result[$i]['application_id'];
        
        $interviewDetails[$_applicationId]['InterviewDate'] = $result[$i]["Date"];
        $interviewDetails[$_applicationId]['StartTime'] = substr($result[$i]["StartTime"], 0, -3);
        $interviewGroupID = $lac->getInterviewGroupID($_applicationId, $interviewGroupArr, $round);
        $interviewDetails[$_applicationId]['InterviewGroup'] = $interviewGroupID;
        $interviewDetails[$_applicationId]['ApplicationID'] = $result[$i]["application_id"];
        $interviewDetails[$_applicationId]['ClassLevelName'] = $classLevel[$result[$i]["ClassLevelID"]];
        $interviewDetails[$_applicationId]['ChineseName'] = $result[$i]["ChineseName"];
        $interviewDetails[$_applicationId]['EnglishName'] = $result[$i]["EnglishName"];
        $interviewDetails[$_applicationId]['dob'] = $result[$i]["dob"];
        $interviewDetails[$_applicationId]['gender'] = $result[$i]["gender"];
        $interviewDetails[$_applicationId]['briefing'] = $result[$i]["briefing"] == ''? "N":"Y";
        $interviewDetails[$_applicationId]['GroupName'] = $result[$i]["GroupName"];
    }
   
    foreach($classLevel as $classlvlId => $classlvlName){
//         if($classlvlName == "K1" || $classlvlName == "K2"){
        for($time = 0; $time <= 1; $time++){
            if($time == 0){
                $timezone = "AM";
            }else{
                $timezone = "PM";
            }
            
            $form = $lac->getClassLevelLetter($classlvlName);

            if($locationNum == 0){
                for($k = 0; $k < 7; $k++){
                    $exportColumn[$locationNum][] = '';
                }
                $exportColumn[$locationNum][] = $academicYear[0]." - ".$academicYear[1] . "-" . "I" . $form . $timezone;
            }else{
                for($k = 0; $k < 7; $k++){
                    $dataArray[$locationNum][] = '';
                }
                $dataArray[$locationNum][] = $academicYear[0]." - ".$academicYear[1] . "-" . "I" . $form . $timezone;
                $locationNum++;
            }
            
            foreach($interviewGroupName as $color){
                $count = 1;
                
                $dataArray[$locationNum][] = '';
                $dataArray[$locationNum][] = $kis_lang['interviewdate'];
                $dataArray[$locationNum][] = $kis_lang['interviewtime'];
                $dataArray[$locationNum][] = $kis_lang['interviewgroup'];
                $dataArray[$locationNum][] = $kis_lang['applicationno'];
                $dataArray[$locationNum][] = $kis_lang['applicationclassform'];
                $dataArray[$locationNum][] = $kis_lang['chinesename'];
                $dataArray[$locationNum][] = $kis_lang['englishname'];
                $dataArray[$locationNum][] = $kis_lang['birthday'];
                $dataArray[$locationNum][] = $kis_lang['sex'];
                $dataArray[$locationNum][] = $kis_lang['attendbriefing'];
                $dataArray[$locationNum][] = $kis_lang['interviewresult'];
                $dataArray[$locationNum][] = $kis_lang['reservationfee'];
                $dataArray[$locationNum][] = $kis_lang['miscellaneous'];
                $dataArray[$locationNum][] = $kis_lang['interviewremarks'];
                $locationNum++;
                
                if($interviewDetails){
                foreach($interviewDetails as $_applicationId => $_interviewDetailsAry){		
                    if($_interviewDetailsAry['ClassLevelName'] == $classlvlName && $_interviewDetailsAry["GroupName"] == $color && substr($_interviewDetailsAry["StartTime"], 0, -3) <= "12:00" && $timezone == "AM"){
                        $dataArray[$locationNum][] = $count;
                        $dataArray[$locationNum][] = $_interviewDetailsAry['InterviewDate'];
                        $dataArray[$locationNum][] = $_interviewDetailsAry['StartTime'];
                        $dataArray[$locationNum][] = $_interviewDetailsAry['InterviewGroup'];
                        $dataArray[$locationNum][] = $_interviewDetailsAry['ApplicationID'];
                        $dataArray[$locationNum][] = $_interviewDetailsAry['ClassLevelName'];
                        $dataArray[$locationNum][] = $_interviewDetailsAry['ChineseName'];
                        $dataArray[$locationNum][] = $_interviewDetailsAry['EnglishName'];
                        $dataArray[$locationNum][] = $_interviewDetailsAry['dob'];
                        $dataArray[$locationNum][] = $_interviewDetailsAry['gender'];
                        $dataArray[$locationNum][] = $_interviewDetailsAry['briefing'];
                        $dataArray[$locationNum][] = '';
                        $dataArray[$locationNum][] = '';
                        $dataArray[$locationNum][] = '';
                        $dataArray[$locationNum][] = '';
                        
                        $count++;
                        $dataArray[$locationNum][] = '';
                        $locationNum++;
                    }elseif($_interviewDetailsAry['ClassLevelName'] == $classlvlName && $_interviewDetailsAry["GroupName"] == $color && substr($_interviewDetailsAry["StartTime"], 0, -3) > "12:00" && $timezone == "PM"){
                        $dataArray[$locationNum][] = $count;
                        $dataArray[$locationNum][] = $_interviewDetailsAry['InterviewDate'];
                        $dataArray[$locationNum][] = $_interviewDetailsAry['StartTime'];
                        $dataArray[$locationNum][] = $_interviewDetailsAry['InterviewGroup'];
                        $dataArray[$locationNum][] = $_interviewDetailsAry['ApplicationID'];
                        $dataArray[$locationNum][] = $_interviewDetailsAry['ClassLevelName'];
                        $dataArray[$locationNum][] = $_interviewDetailsAry['ChineseName'];
                        $dataArray[$locationNum][] = $_interviewDetailsAry['EnglishName'];
                        $dataArray[$locationNum][] = $_interviewDetailsAry['dob'];
                        $dataArray[$locationNum][] = $_interviewDetailsAry['gender'];
                        $dataArray[$locationNum][] = $_interviewDetailsAry['briefing'];
                        $dataArray[$locationNum][] = '';
                        $dataArray[$locationNum][] = '';
                        $dataArray[$locationNum][] = '';
                        $dataArray[$locationNum][] = '';
                        
                        $count++;
                        $dataArray[$locationNum][] = '';
                        $locationNum++;
                    }
                }
                if(!$dataArray[$locationNum]){
                    $dataArray[$locationNum][] = '';
                    $locationNum++;
                }
            }
            }
            $dataArray[$locationNum][] = '';
            $locationNum += 2;
        }
//         }
    }

		$filename = "interview_list.csv";
	
	//generate csv content
		$export_content = $lexport->GET_EXPORT_TXT_WITH_REFERENCE($dataArray, $exportColumn, "\t", "\r\n", "\t", 0, "9");
	//Output The File To User Browser
	$lexport->EXPORT_FILE($filename, $export_content);
}
?>