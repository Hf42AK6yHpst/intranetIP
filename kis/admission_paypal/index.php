<?php
# modifying by: Henry
 
/********************
 * Log :
 * Date		2015-07-22 [Henry]
 * 			File Created
 * 
 ********************/
 
$PATH_WRT_ROOT = "../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_ui.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_utility.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_apps.php");
//include_once($PATH_WRT_ROOT."includes/json.php");

include_once("../config.php");

//for the customization
include_once($PATH_WRT_ROOT."includes/admission/".$setting_path_ip_rel."/config.php");
$intranet_session_language = $admission_cfg['DefaultLang'];
include_once($PATH_WRT_ROOT."includes/admission/".$setting_path_ip_rel."/libadmission_cust.php");
include_once($PATH_WRT_ROOT."includes/admission/".$setting_path_ip_rel."/libadmission_ui_cust.php");

include_once($PATH_WRT_ROOT."lang/lang.".$intranet_session_language.".php");
include_once($PATH_WRT_ROOT."lang/kis/lang_common_".$intranet_session_language.".php");
include_once($PATH_WRT_ROOT."lang/kis/apps/lang_admission_".$intranet_session_language.".php");
include_once($PATH_WRT_ROOT."lang/admission_lang.".$intranet_session_language.".php");


intranet_opendb();

//$libjson 	= new JSON_obj();
$libkis 	= new kis('');
$lac		= new admission_cust();
$lauc		= new admission_ui_cust();
$li			=new interface_html();

if (!$plugin['eAdmission'])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT("","../");
	exit;
}

$_SESSION['SSV_PRIVILEGE']['school']['name'] = GET_SCHOOL_NAME();
$school = $libkis->getUserSchool();

$ApplicationNo = $_REQUEST['token'];

//The docs upload page
$main_content .= "<div id='step_index' style='".($ApplicationNo?'display:none':'')."'>";
//$main_content .= $lauc->getConfirmPageContent();
//$main_content .=$li->GET_ACTION_BTN('Back', "button", "goto('step_confirm','step_docs_upload')", "SubmitBtn", "", 0, "formbutton");
//$main_content .=$li->GET_ACTION_BTN('Next', "button", "goto('step_docs_upload','step_confirm')", "SubmitBtn", "", 0, "formbutton");
//$main_content .= '<table cellpadding="10" cellspacing="0">
//	<thead>
//		<tr>
//	    	<th colspan="2">eAdmission 面試編排結果</th>
//		</tr>
//	</thead>
//<tbody>
//<tr><td align="right" nowrap="nowarp">輸入出世紙號碼： </td><td nowrap="nowarp"><input type="text" name="BCN" id="BCN" /><br />不需要輸入括號，例如：1234567(8)，請輸入 "12345678"。</td></tr>
//<tr><td align="right" nowrap="nowarp">輸入生日日期： </td><td nowrap="nowarp"><input type="text" name="BCN2" id="BCN2" /><br /><font size="-1">Please check the information carefully as it cannot edit after submitted. <br />請仔細閲讀提交的資料，提交後將無法作任何更改。</font></td></tr>
//
//<tr>
//  		<td colspan="2" align="center"  class="padtop25 padbottom25">
//			<div><input type="submit" name="Submit" value=" Submit  提交  " style="font-size:18px;FONT-FAMILY: \'微軟正黑體\', Verdana, Arial, Helvetica, Mingliu, Sans-Serif;" /></div>
//		</td>
//		</tr>
//	</tbody>
//</table>';

$star = '<font style="color:red;">*</font>';

$x = '';
$x .= '<div class="admission_board" style="min-height: 150px">';
$x .= '<h1>eAdmission 付款頁面<!-- '.date('Y',getStartOfAcademicYear('',$lac->schoolYearID)).'--><br/>eAdmission Payment Page<!-- '.date('Y',getStartOfAcademicYear('',$lac->schoolYearID)).'--></h1>';
$x .='<form id="form1" name="form1">';
$x .= '<table class="form_table" style="font-size: 13px">';
$x .= '<tr>';
$x .= '<td class="field_title">'.$star.'輸入申請編號 Application Number</td>';
$x .= '<td><input style="width:200px" name="ApplicationNum" type="text" id="ApplicationNum" class="textboxtext" maxlength="16"/></td>';
$x .= '</tr>';
$x .= '<tr>';
$x .= '<td class="field_title">'.$star.'輸入出世紙號碼 Birth Certificate Number</td>';
$x .= '<td><input style="width:200px" name="StudentBirthCertNo" type="text" id="StudentBirthCertNo" class="textboxtext" maxlength="16" size="8"/><br />不需要輸入括號，例如：1234567(8)，請輸入 "12345678"。<br/>No need to enter the brackets. Eg. A123456(7), please enter "A1234567".</td>';
$x .= '</tr>';
//$x .= '<tr>';
//$x .= '<td class="field_title">'.$star.'輸入出生日期 Date of Birth</td>';
//$x .= '<td><input style="width:200px" name="StudentDateOfBirth" type="text" id="StudentDateOfBirth" class="textboxtext" maxlength="10" size="15"/>(YYYY-MM-DD)</td>';
//$x .= '</tr>';
$x .= '</table>';
$x .= '<input type="hidden" name="token" value="'.$ApplicationNo.'">';
$x .='</form>';

$x .= '</div>
	<div class="edit_bottom">
		'.$lauc->GET_ACTION_BTN($Lang['Btn']['Submit'].' Submit', "button", "goto('step_index','step_result')", "SubmitBtn", "", 0, "formbutton").'
	</div>
	<p class="spacer"></p>';
	
$x .= '</div>';

$main_content .= $x;

$main_content .= "</div>";

$main_content .= "<div id='step_result' style='".($ApplicationNo?'':'display:none')."'>";

$x = '';
$x .= '<div class="admission_board" style="min-height: 150px">';
$x .= '<h1>eAdmission 付款頁面<!-- '.date('Y',getStartOfAcademicYear('',$lac->schoolYearID)).'--><br/>eAdmission Payment Page<!-- '.date('Y',getStartOfAcademicYear('',$lac->schoolYearID)).'--></h1>';
//$x .= '<table class="form_table" style="font-size: 13px">';
//$x .= '<tr>';
//$x .= '<td class="field_title">申請編號 Application #</td>';
//$x .= '<td><div id="divInterviewResult"></div></td>';
//$x .= '</tr>';
//$x .= '<td class="field_title">申請者姓名 Name of Applicant</td>';
//$x .= '<td><div id="divInterviewResult"></div></td>';
//$x .= '</tr>';
//$x .= '<tr>';
//$x .= '<td class="field_title">面試時段 Interview Timeslot</td>';
//$x .= '<td><div id="divInterviewResult"></div></td>';
//$x .= '</tr>';
//$x .= '</table>';
$x .='<div id="divInterviewResult">';
$result = $lac->getPaymentResult($_REQUEST['StudentDateOfBirth'], $_REQUEST['StudentBirthCertNo'], $_REQUEST['token'],$libkis_admission->schoolYearID);
if($result){
$x .= '<table class="form_table" style="font-size: 13px">';
$x .= '<tr>';
$x .= '<td class="field_title">申請編號 Application Number</td>';
$x .= '<td>'.$result[0]['ApplicationID'].'</td>';
$x .= '</tr>';
$x .= '<td class="field_title">申請者姓名 Name of Applicant</td>';
$x .= '<td>'.str_replace(',',' ',$result[0]['ChineseName']).' ('.str_replace(',',' ',$result[0]['EnglishName']).')</td>';
$x .= '</tr>';
$x .= '<tr>';
$x .= '<td class="field_title">付款結果 Payment Result</td>';

$hasPaid = 0;
$y .= '<td>';
foreach($result as $aResult){
	if($aResult['payment_status'] == 'Completed'){
		$hasPaid = 1;
		$displayStatus = '已付款 Paid';
	}
	else if($aResult['payment_status'] == 'Refunded'){
		$displayStatus = '已退款 Refunded';
	}
	$y .= '<font color="green">'.$displayStatus.'</font><br/>(付款項目 Payment Item) '.$aResult['item_name'].' <br/>(付款金額 Payment Total) '.$aResult['mc_currency'].' '.$aResult['mc_gross'].' <br/>(付款日期 Payment Date) '.date('Y-m-d H:i:s', strtotime($aResult['payment_date'])).' <br/>(付款人電郵 Payer Email) '.$aResult['payer_email'].'<br/>';
}
$y.='</td>';

if($hasPaid)
	$x .= $y;
else if($result[0]['Status'] >= $admission_cfg['Status']['paymentsettled'] && $result[0]['Status'] != $admission_cfg['Status']['cancelled']){
	$x .= '<td><font color="green">已付款 Paid</font><br/>(人手記錄 Manual)</td>';
}
else{
	$x .= '<td><font color="red">未付款 Unpaid</font> '.$lauc->getPayPalButton(MD5($result[0]['ApplicationID'])).'<div style="font-size: 1.3em;line-height:1.3"><font color="red">*</font> 現在前往 Paypal 付款，完成後，必須於付款頁面按<br/> "<font color="red">返回'.$admission_cfg['paypal_name'].'</font>" <br/>方能完成整個付款及報名程序。<br><br><font color="red">*</font> After paying the fee using Paypal, you MUST click<br/> "<font color="red">Back to '.$admission_cfg['paypal_name'].'</font>" <br/> to complete the whole procedure.</div></td>';
}
$x .= '</tr>';
$x .= '</table>';
}
else{
	$x .= '<table class="form_table" style="font-size: 13px">';
	$x .= '<tr>';
	$x .= '<td><font color="red">The Token is invalid!</font></td>';
	$x .= '</tr>';
	$x .='</table>';
}
if($hasPaid){
	$x .='<p style="font-size: 2em;color: #1D8908;text-align: center;">申請已完成。<br/>謝謝您使用網上報名服務！<br/>Application completed, with thanks for lodging your application online.</p>';
	//$x .='<p style="font-size: 1.5em;font-weight: normal;text-align: center;">謝謝您使用網上報名服務！<br/>Thank you for using our online application!</p>';
}
$x .='</div>';

if(!$ApplicationNo){
$x .= '</div>
	<div class="edit_bottom">
		'.$lauc->GET_ACTION_BTN($Lang['Admission']['finish'].' Finish', "button", "location.href='index.php'", "SubmitBtn", "", 0, "formbutton").'
	</div>
	<p class="spacer"></p>';
}

if($hasPaid){
	$x .= '</div>
	<div id="close_bottom" class="edit_bottom">
		'.$lauc->GET_ACTION_BTN('關閉 Close', "button", "if(window.opener != null){window.opener.document.getElementById('finish_page_finish_button').style.display='';window.opener.document.getElementById('finish_page_payment_button').style.display='none';}; window.close();", "SubmitBtn", "", 0, "formbutton").'
	</div>
	<p class="spacer"></p>';
}

$main_content .= $x;

$main_content .= "</div>";


include_once("common_tmpl.php");
//include_once($PATH_WRT_ROOT."includes/admission/".$setting_path_ip_rel."/commonJs.php");
?>
<link type="text/css" rel="stylesheet" media="screen" href="/templates/jquery/ui-1.9.2/jquery-ui-1.9.2.custom.min.css">
<script type="text/javascript" src="/templates/jquery/ui-1.9.2/jquery-ui.custom.min.js"></script>
<script type="text/javascript" src="/templates/jquery/ui-1.9.2/jquery.ui.datepicker-zh-HK.js"></script>
<script type="text/javascript" src="/templates/kis/js/config.js"></script>
<script type="text/javascript" src="/templates/kis/js/kis.js"></script>
<style>
.ui-autocomplete {max-height: 200px;max-width: 200px;overflow-y: auto;overflow-x: hidden;font-size: 12px;font-family: Verdana, "微軟正黑體";}
.ui-autocomplete-category{font-style: italic;}
.ui-datepicker{font-size: 12px;width: 210px;font-family: Verdana, "微軟正黑體";}
.ui-datepicker select.ui-datepicker-month, .ui-datepicker select.ui-datepicker-year {width:auto;}
.ui-selectable tr.ui-selecting td, .ui-selectable tr.ui-selected td{background-color: #fff7a3}
</style>
<script type="text/javascript">
<?if($result['payment_status'] == 'Completed'){?>
$( document ).ready(function() {
	if(!(window.opener != null)){
		$('#close_bottom').hide();
	}
});
<?}?>
function focusAndGo(){
   window.focus();
   window.location.reload();
}

kis.datepicker('#StudentDateOfBirth');

function goto(current,page){

	if(current == 'step_index' && page == 'step_result'){
		
		if(form1.ApplicationNum.value==''){
			alert("請輸入申請編號\nPlease enter Application Number.");	
			form1.ApplicationNum.focus();
			return false;
		}
		else if(form1.StudentBirthCertNo.value==''){
			alert("<?=$Lang['Admission']['munsang']['msg']['enterbirthcertno']?>\nPlease enter Birth Certificate Number.");	
			form1.StudentBirthCertNo.focus();
			return false;
		}
//		else if(!/^[a-zA-Z][0-9]{6}(a|A|[0-9])$/.test(form1.StudentBirthCertNo.value)){
//			alert("<?=$Lang['Admission']['munsang']['msg']['invalidbirthcertificatenumber']?>\nInvalid Birth Certificate Number.");	
//			form1.StudentBirthCertNo.focus();
//			return false;
//		}
//		else if(!form1.StudentDateOfBirth.value.match(/^[0-9]{4}\-(0[1-9]|1[012])\-(0[1-9]|[12][0-9]|3[01])/)){
//			if(form1.StudentDateOfBirth.value!=''){
//				alert("<?=$Lang['Admission']['msg']['invaliddateformat']?>\nInvalid Date Format");
//			}
//			else{
//				alert("<?=$Lang['Admission']['msg']['enterdateofbirth']?>\nPlease enter Date of Birth.");	
//			}
//			
//			form1.StudentDateOfBirth.focus();
//			return false;
//		}
//		
		/* Clear result div*/
		   $("#DayTypeOption").html('');
		
		   /* Get some values from elements on the page: */
		   var values = $("#form1").serialize();
			
		   /* Send the data using post and put the results in a div */
		   $.ajax({
		       url: "ajax_get_payment_result.php",
		       type: "post",
		       data: values,
		       success: function(data){
		           //alert("debugging: The classlevel is updated!");
		           if(data==0){
		           		alert("申請編號或出世紙號碼不正確\nIncorrect Application Number or Birth Certificate Number.");	
						form1.StudentBirthCertNo.focus();
						return false;
		           }
		           $("#divInterviewResult").html(data);
		           document.getElementById(current).style.display = "none";
				   document.getElementById(page).style.display = "";
		       },
		       error:function(){
		           //alert("failure");
		           $("#result").html('There is error while submit');
		       }
		   });
	}
	
}

</script>