<?php
# modifying by: Henry
 
/********************
 * Log :
 * Date		2013-10-21 [Henry]
 * 			File Created
 * 
 ********************/
$PATH_WRT_ROOT = "../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_ui.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_utility.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_apps.php");
//include_once($PATH_WRT_ROOT."includes/json.php");
include_once("../config.php");

//for the customization
include_once($PATH_WRT_ROOT."includes/admission/".$setting_path_ip_rel."/config.php");
$intranet_session_language = $admission_cfg['DefaultLang'];
include_once($PATH_WRT_ROOT."includes/admission/".$setting_path_ip_rel."/libadmission_cust.php");
include_once($PATH_WRT_ROOT."includes/admission/".$setting_path_ip_rel."/libadmission_ui_cust.php");

include_once($PATH_WRT_ROOT."lang/lang.".$intranet_session_language.".php");
include_once($PATH_WRT_ROOT."lang/kis/lang_common_".$intranet_session_language.".php");
include_once($PATH_WRT_ROOT."lang/kis/apps/lang_admission_".$intranet_session_language.".php");
include_once($PATH_WRT_ROOT."lang/admission_lang.".$intranet_session_language.".php");


intranet_opendb();

//$libjson 	= new JSON_obj();
$libkis 	= new kis('');
$lac		= new admission_cust();
$lauc		= new admission_ui_cust();
$li			=new interface_html();

if (!$plugin['eAdmission'])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT("","../");
	exit;
}

$libkis_admission = $libkis->loadApp('admission');
	
//Get the intruction content
//$applicationSetting = $libkis_admission->getApplicationSetting($libkis_admission->schoolYearID);
$result = $lac->getPaymentResult($_REQUEST['ApplicationNum'], $_REQUEST['StudentBirthCertNo'], $_REQUEST['token'], $libkis_admission->schoolYearID);

if($result){
$x .= '<table class="form_table" style="font-size: 13px">';
$x .= '<tr>';
$x .= '<td class="field_title">申請編號 Application number</td>';
$x .= '<td>'.$result[0]['ApplicationID'].'</td>';
$x .= '</tr>';
$x .= '<td class="field_title">申請者姓名 Name of Applicant</td>';
$x .= '<td>'.str_replace(',',' ',$result[0]['ChineseName']).' ('.str_replace(',',' ',$result[0]['EnglishName']).')</td>';
$x .= '</tr>';
$x .= '<tr>';
$x .= '<td class="field_title">付款結果 Payment Result</td>';
$hasPaid = 0;
$y .= '<td>';
foreach($result as $aResult){
	if($aResult['payment_status'] == 'Completed'){
		$hasPaid = 1;
		$displayStatus = '已付款 Paid';
	}
	else if($aResult['payment_status'] == 'Refunded'){
		$displayStatus = '已退款 Refunded';
	}
	$y .= '<font color="green">'.$displayStatus.'</font><br/>(付款項目 Payment Item) '.$aResult['item_name'].' <br/>(付款金額 Payment Total) '.$aResult['mc_currency'].' '.$aResult['mc_gross'].' <br/>(付款日期 Payment Date) '.date('Y-m-d H:i:s', strtotime($aResult['payment_date'])).' <br/>(付款人電郵 Payer Email) '.$aResult['payer_email'].'<br/>';
}
$y.='</td>';

if($hasPaid)
	$x .= $y;
else if($result[0]['Status'] >= $admission_cfg['Status']['paymentsettled'] && $result[0]['Status'] != $admission_cfg['Status']['cancelled']){
	$x .= '<td><font color="green">已付款 Paid</font><br/>(人手記錄 Manual)</td>';
}
else{
	$x .= '<td><font color="red">未付款 Unpaid</font> '.$lauc->getPayPalButton(MD5($result[0]['ApplicationID'])).'<div style="font-size: 1.3em;line-height:1.3"><font color="red">*</font> 現在前往 Paypal 付款，完成後，必須於付款頁面按<br/> "<font color="red">返回'.$admission_cfg['paypal_name'].'</font>" <br/>方能完成整個付款及報名程序。<br><br><font color="red">*</font> After paying the fee using Paypal, you MUST click<br/> "<font color="red">Back to '.$admission_cfg['paypal_name'].'</font>" <br/> to complete the whole procedure.</div></td>';
}
$x .= '</tr>';
$x .= '</table>';
}
else{
	$x .= '<table class="form_table" style="font-size: 13px">';
	$x .= '<tr>';
	$x .= '<td><font color="red">The Token is invalid!</font></td>';
	$x .= '</tr>';
	$x .='</table>';
}
if($hasPaid){
	$x .='<p style="font-size: 2em;color: #1D8908;text-align: center;">申請已完成。<br/>謝謝您使用網上報名服務！<br/>Application completed, with thanks for lodging your application online.</p>';
	//$x .='<p style="font-size: 1.5em;font-weight: normal;text-align: center;">謝謝您使用網上報名服務！<br/>Thank you for using our online application!</p>';
}
echo $result?$x:$result;
//echo debug_pr($_REQUEST).debug_pr($_FILES);
?>