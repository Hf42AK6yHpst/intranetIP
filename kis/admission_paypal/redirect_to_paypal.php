<?php
# modifying by: Henry
 
/********************
 * Log :
 * Date		2015-07-22 [Henry]
 * 			File Created
 * 
 ********************/

$PATH_WRT_ROOT = "../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_ui.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_utility.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_apps.php");
//include_once($PATH_WRT_ROOT."includes/json.php");

include_once("../config.php");

//for the customization
include_once($PATH_WRT_ROOT."includes/admission/".$setting_path_ip_rel."/config.php");
$intranet_session_language = $admission_cfg['DefaultLang'];
include_once($PATH_WRT_ROOT."includes/admission/".$setting_path_ip_rel."/libadmission_cust.php");
include_once($PATH_WRT_ROOT."includes/admission/".$setting_path_ip_rel."/libadmission_ui_cust.php");

include_once($PATH_WRT_ROOT."lang/lang.".$intranet_session_language.".php");
include_once($PATH_WRT_ROOT."lang/kis/lang_common_".$intranet_session_language.".php");
include_once($PATH_WRT_ROOT."lang/kis/apps/lang_admission_".$intranet_session_language.".php");
include_once($PATH_WRT_ROOT."lang/admission_lang.".$intranet_session_language.".php");


intranet_opendb();

//$libjson 	= new JSON_obj();
$libkis 	= new kis('');
$lac		= new admission_cust();
$lauc		= new admission_ui_cust();
//$li			=new interface_html();

if (!$plugin['eAdmission'])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT("","../");
	exit;
}
$libkis_admission = $libkis->loadApp('admission');

$ApplicationNo = $_REQUEST['token'];

echo 'Loading 載入中 ...';
echo '<div style="display:none">';
echo $lauc->getPayPalButton($ApplicationNo);
echo '</div>';

$result = $lac->getPaymentResult('', '', $_REQUEST['token'],$libkis_admission->schoolYearID);

$hasPaidByAlipay = false;
$basicSettings = $lac->getBasicSettings(99999, array (
	'enablepaypal',
	'enablealipayhk'
));
if ($sys_custom['ePayment']['Alipay'] && $basicSettings['enablealipayhk']) {
    $result2 = $lac->getAlipayPaymentResult('', '', $_REQUEST['token'],$libkis_admission->schoolYearID);
    if($result2){
		foreach($result2 as $aResult2){
			if($aResult2['RecordStatus'] == '1' ){
				$hasPaidByAlipay = true;
				break;
			}
		}
    }
}

if($result){
	foreach($result as $aResult){
		if(($hasPaidByAlipay || $aResult['payment_status'] == 'Completed') && $aResult['Status'] >= $admission_cfg['Status']['paymentsettled'] && $aResult['Status'] != $admission_cfg['Status']['cancelled']){
			$hasPaid = 1;
			break;
		}
	}
//	if($hasPaid){
//		
//	}
}
//else{
//	
//}

if(!$hasPaid){
	$ApplicationID = $lac->decodeMD5ApplicationID($_REQUEST['token']);
	$onlinePaymentStartTime = $lac->getApplicationCustInfo($ApplicationID, 'onlinePaymentStartTime');
	$lac->updateApplicationCustInfo(array(
		'filterApplicationId' => $ApplicationID,
		'filterCode' => 'onlinePaymentStartTime',
		'Value' => date("Y-m-d H:i:s")
	));
}

?>
<script type="text/javascript" src="/templates/jquery/jquery-1.8.0.min.js"></script>
<script type="text/javascript">
<?if(!$hasPaid){?>
$(document).ready(function() {
	<?if($onlinePaymentStartTime){?>
	if(confirm("Continue to pay?")){
		document.forms[0].submit();
	}else{
		window.opener.location.reload();
		window.close();
	}
	<?}else{?>
	document.forms[0].submit();
	<?}?>
});
<?}else{?>
$(document).ready(function() {
	window.opener.location.reload();
	window.close();
});
<?}?>
</script>