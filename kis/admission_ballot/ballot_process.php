<?php
// Editing by 

/****************************
 * Change Log:
 * 20150319 (Henry)
 * - added Sequence, IsTwins, IsTwinsApplied
 * 
 ****************************/
set_time_limit(120);
$PATH_WRT_ROOT = "../../";
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."kis/init.php");
require_once($PATH_WRT_ROOT."includes/admission/sinmeng.eclass.hk/config.php");
require_once($PATH_WRT_ROOT."includes/admission/sinmeng.eclass.hk/libadmission_cust.php");
//include_once($PATH_WRT_ROOT."lang/lang.".$intranet_session_language.".php");
//include_once($PATH_WRT_ROOT."lang/kis/lang_common_".$intranet_session_language.".php");
require_once($PATH_WRT_ROOT."lang/kis/apps/lang_admission_".$intranet_session_language.".php");

intranet_auth();
intranet_opendb();

$libadmission_cust = new admission_cust();
$li = new libdb();

$SchoolYearID = $_REQUEST['SchoolYearID']? $_REQUEST['SchoolYearID'] : $libadmission_cust->schoolYearID;
$YearID = $_REQUEST['YearID'];

$ClearResult = $_REQUEST['ClearResult'] == '1';
if($ClearResult){
	if(empty($SchoolYearID)){
		echo "不正確的操作。";
		intranet_closedb();
		exit;
	}
	$result = array();
	$sql = "SELECT RecordID,ApplicationID FROM ADMISSION_APPLICATION_BALLOT_LOG WHERE SchoolYearID='$SchoolYearID' ";
	if(!empty($YearID) && $YearID > 0){
		$sql .= " AND YearID='$YearID'";
	}
	$records = $li->returnResultSet($sql);
	$record_count = count($records);
	for($i=0;$i<$record_count;$i++){
		$sql = "UPDATE ADMISSION_APPLICATION_STATUS SET Status='".$admission_cfg['Status']['pending']."' WHERE SchoolYearID='$SchoolYearID' AND ApplicationID='".$records[$i]['ApplicationID']."'";
		$result['Update_'.$records[$i]['ApplicationID']] = $li->db_db_query($sql);
	}
	$sql = "DELETE FROM ADMISSION_APPLICATION_BALLOT_LOG WHERE SchoolYearID='$SchoolYearID' ";
	if(!empty($YearID) && $YearID > 0){
		$sql .= " AND YearID='$YearID'";
	}
	$result['DeleteLog'] = $li->db_db_query($sql);
	echo !in_array(false,$result)? "已清除抽籤結果。" : "很抱歉，清除抽籤結果中出現錯誤。";
	
	intranet_closedb();
	exit;
}

// Ballot Group Quota Info
$BALLOT_GROUPS = array(
					'3' => array( // assume only have three classes
								array('Total'=>20, 'Special'=>0, 'SpecialSpare'=>0, 'General'=>20, 'GeneralSpare'=>10), // BallotGroupIndex 0 嬰兒班
								array('Total'=>72, 'Special'=>0, 'SpecialSpare'=>0, 'General'=>72, 'GeneralSpare'=>20), // BallotGroupIndex 1 幼兒班1
								array('Total'=>38, 'Special'=>0, 'SpecialSpare'=>0, 'General'=>38, 'GeneralSpare'=>20) // BallotGroupIndex 2 幼兒班2
							), // Whole Day
					'1' => array(
								array('Total'=>30, 'General'=>30, 'GeneralSpare'=>15) // 幼兒班2
							), // Half Day AM
					'2' => array(
								array('Total'=>30, 'General'=>30, 'GeneralSpare'=>15) // 幼兒班2
							) // Half Day PM
				);
				
// ApplicationID in Ballot Group result
$BALLOT_APPLICANTS = array(
						'3' => array( 
									array('Total'=>array(), 'Special'=> array(), 'SpecialSpare'=> array(), 'General'=> array(), 'GeneralSpare'=>array()), // BallotGroupIndex 0
									array('Total'=>array(), 'Special'=> array(), 'SpecialSpare'=> array(), 'General'=> array(), 'GeneralSpare'=>array()), // BallotGroupIndex 1
									array('Total'=>array(), 'Special'=>array(), 'SpecialSpare'=>array(), 'General'=>array(), 'GeneralSpare'=>array()) // BallotGroupIndex 2
								), // Whole Day
						'1' => array(
									array('Total'=>array(), 'General'=>array(), 'GeneralSpare'=>array())
								), // Half Day AM
						'2' => array(
									array('Total'=>array(), 'General'=>array(), 'GeneralSpare'=>array())
								) // Half Day PM
					);

$ViewResult = $_REQUEST['ViewResult'] == '1';
if($ViewResult){
	if(empty($SchoolYearID)){
		echo "不正確的操作。";
		intranet_closedb();
		exit;
	}
	
	echo displayBallotResultTable($SchoolYearID,$YearID,$ShowChoice);
	
	intranet_closedb();
	exit;
}


if(empty($SchoolYearID) || empty($YearID)){
	echo "不正確的操作。";
	intranet_closedb();
	exit;
}

/*
 	$admission_cfg['Status'] = array();
	$admission_cfg['Status']['pending']	= 1;
	$admission_cfg['Status']['paymentsettled']	= 2;
	$admission_cfg['Status']['waitingforinterview']	= 3;
	$admission_cfg['Status']['confirmed']	= 4;
	$admission_cfg['Status']['cancelled']	= 5;
	$admission_cfg['Status']['reserve'] = 6; 
 */

function insertBallotLog($SchoolYearID,$YearID,$ApplicationID,$BallotGroupType,$BallotGroupIndex,$SpecialNormal,$Status)
{
	global $li;
	$sql = "INSERT INTO ADMISSION_APPLICATION_BALLOT_LOG 
			(SchoolYearID,YearID,ApplicationID,BallotGroupType,BallotGroupIndex,SpecialNormal,Status,DateInput,InputBy)
			 VALUES ('$SchoolYearID','$YearID','$ApplicationID','$BallotGroupType','$BallotGroupIndex','$SpecialNormal','$Status',NOW(),'".$_SESSION['UserID']."')";
	$insert_success = $li->db_db_query($sql);
	
	$sql = "UPDATE ADMISSION_APPLICATION_STATUS SET Status='$Status' WHERE SchoolYearID='$SchoolYearID' AND ApplicationID='$ApplicationID'";
	$update_success = $li->db_db_query($sql);
	
	return $insert_success && $update_success;
}

function displayBallotResultTable($SchoolYearID,$YearID,$ShowChoice=0)
{
	global $li, $Lang, $kis_lang, $admission_cfg, $BALLOT_GROUPS;
	
	$title_arr = array('0'=>"嬰兒組",'1'=>"1至2歲組",'2'=>"2至3歲組");
	$group_type_arr = array('3'=>"全日托", '1'=>"上午組", '2'=>"下午組");
	$address_arr = array('1'=>"風順堂區",'2'=>"大堂區",'3'=>"望德堂區",'4'=>"聖安多尼堂區",'5'=>"青洲區及花地瑪堂區",'6'=>"氹仔", '7'=>"路環", '8'=>"其他");
	
	$sql = "SELECT 
				s.ApplicationID,
				s.ChineseName,
				s.EnglishName,
				b.YearID,
				b.BallotGroupType,
				b.BallotGroupIndex,
				b.SpecialNormal,
				b.Status,
				o.ApplyDayType1,
				o.ApplyDayType2,
				o.ApplyDayType3,
				b.Sequence,
				s.IsTwins,
				s.IsTwinsApplied,
				s.DOB,
				s.Address
			FROM ADMISSION_APPLICATION_BALLOT_LOG as b 
			INNER JOIN ADMISSION_STU_INFO as s ON s.ApplicationID=b.ApplicationID 
			INNER JOIN ADMISSION_PG_INFO as p ON p.ApplicationID=s.ApplicationID 
			INNER JOIN ADMISSION_OTHERS_INFO as o ON o.ApplicationID=s.ApplicationID 
			WHERE b.SchoolYearID='$SchoolYearID' 
			GROUP BY b.ApplicationID 
			ORDER BY b.RecordID";
	$records = $li->returnResultSet($sql);
	$record_count = count($records);
	
	$data = array('3'=>array('0'=>array('Special'=>array(),'SpecialSpare'=>array(),'General'=>array(),'GeneralSpare'=>array()),
							 '1'=>array('Special'=>array(),'SpecialSpare'=>array(),'General'=>array(),'GeneralSpare'=>array()),
							 '2'=>array('Special'=>array(),'SpecialSpare'=>array(),'General'=>array(),'GeneralSpare'=>array())),
				  '1'=>array('0'=>array('General'=>array(),'GeneralSpare'=>array())), 
				  '2'=>array('0'=>array('General'=>array(),'GeneralSpare'=>array()))
				  );
	
	for($i=0;$i<$record_count;$i++){
		$application_id = $records[$i]['ApplicationID'];
		$chinese_name = $records[$i]['ChineseName'];
		$english_name = $records[$i]['EnglishName'];
		$year_id = $records[$i]['YearID'];
		$ballot_group_type = $records[$i]['BallotGroupType'];
		$ballot_group_index = $records[$i]['BallotGroupIndex'];
		$special_general = $records[$i]['SpecialNormal'];
		$status = $records[$i]['Status'];
		$apply_day_type1 = $records[$i]['ApplyDayType1'];
		$apply_day_type2 = $records[$i]['ApplyDayType2'];
		$apply_day_type3 = $records[$i]['ApplyDayType3'];
		$sequence = $records[$i]['Sequence'];
		$is_twins = $records[$i]['IsTwins'];
		$is_twins_applied = $records[$i]['IsTwinsApplied'];
		$date_of_birth = $records[$i]['DOB'];
		$address = $records[$i]['Address'];
		
		$star = '';
		if($is_twins == 'Y' && $is_twins_applied == 'Y'){
			$star = '<font color="red">*</font>';
		}
		
		$display_name = $sequence.'. '.$star.$application_id.' '.$chinese_name.' '.$english_name;
		if($ShowChoice == '1'){
			$display_name .= '<br />(1)'.$group_type_arr[$apply_day_type1].' (2)'.$group_type_arr[$apply_day_type2].' (3)'.$group_type_arr[$apply_day_type3].' ('.$date_of_birth.')'.' ('.($address_arr[$address]?$address_arr[$address]:$address).')';
		}
		
		$field = '';
		if($special_general == '1'){
			$field.='Special';
		}else{
			$field.='General';
		}
		if($status == $admission_cfg['Status']['reserve']){
			$field .= 'Spare';
		}
		
		$data[$ballot_group_type][$ballot_group_index][$field][] = $display_name;
		
	}
	$css = ' style="border:1px solid black;"';
	$x .= '<h2>抽籤結果</h2>';
	$x .= '<table border="0" cellspacing="0" cellpadding="3" border-collapse="collapse"'.$css.'>';
		$x .= '<tr>';
			$x .= '<td'.$css.'>班組</td>';
// 			$x .= '<td'.$css.'>單親 / 特殊定庭<br>正選</td>';
// 			$x .= '<td'.$css.'>單親 / 特殊定庭<br>後備</td>';
			$x .= '<td'.$css.'>一般正選</td>';
			$x .= '<td'.$css.'>一般後備</td>';
		$x .= '</tr>';
	foreach($data['3'] as $key => $arr)
	{
		$x .= '<tr>';
			$x .= '<td'.$css.'>'.$group_type_arr['3'].' - '.$title_arr[$key].'</td>';
// 			$x .= '<td'.$css.'>'.count($data['3'][$key]['Special']).'/'.$BALLOT_GROUPS['3'][$key]['Special'].'人<br>'.implode('<br>',$data['3'][$key]['Special']).'</td>';
// 			$x .= '<td'.$css.'>'.count($data['3'][$key]['SpecialSpare']).'/'.$BALLOT_GROUPS['3'][$key]['SpecialSpare'].'人<br>'.implode('<br>',$data['3'][$key]['SpecialSpare']).'</td>';
			$x .= '<td'.$css.'>'.count($data['3'][$key]['General']).'/'.$BALLOT_GROUPS['3'][$key]['General'].'人<br>'.implode('<br>',$data['3'][$key]['General']).'</td>';
			$x .= '<td'.$css.'>'.count($data['3'][$key]['GeneralSpare']).'/'.$BALLOT_GROUPS['3'][$key]['GeneralSpare'].'人<br>'.implode('<br>',$data['3'][$key]['GeneralSpare']).'</td>';
		$x .= '</tr>';
	}
	$x .= '<tr>';
		$x .= '<td'.$css.'>'.$group_type_arr['1'].' - '.$title_arr['2'].'</td>';
// 		$x .= '<td'.$css.'> N.A. </td>';
// 		$x .= '<td'.$css.'> N.A. </td>';
		$x .= '<td'.$css.'>'.count($data['1']['0']['General']).'/'.$BALLOT_GROUPS['1']['0']['General'].'人<br>'.implode('<br>',$data['1']['0']['General']).'</td>';
		$x .= '<td'.$css.'>'.count($data['1']['0']['GeneralSpare']).'/'.$BALLOT_GROUPS['1']['0']['GeneralSpare'].'人<br>'.implode('<br>',$data['1']['0']['GeneralSpare']).'</td>';
	$x .= '</tr>';
	$x .= '<tr>';
		$x .= '<td'.$css.'>'.$group_type_arr['2'].' - '.$title_arr['2'].'</td>';
// 		$x .= '<td'.$css.'> N.A. </td>';
// 		$x .= '<td'.$css.'> N.A. </td>';
		$x .= '<td'.$css.'>'.count($data['2']['0']['General']).'/'.$BALLOT_GROUPS['2']['0']['General'].'人<br>'.implode('<br>',$data['2']['0']['General']).'</td>';
		$x .= '<td'.$css.'>'.count($data['2']['0']['GeneralSpare']).'/'.$BALLOT_GROUPS['2']['0']['GeneralSpare'].'人<br>'.implode('<br>',$data['2']['0']['GeneralSpare']).'</td>';
	$x .= '</tr>';
	
	$x .= '</table>';
	
	return $x;
}


// Get year form 
$sql = "SELECT y.YearID 
		FROM YEAR as y 
		INNER JOIN YEAR_CLASS as yc ON yc.YearID=y.YearID 
		LEFT JOIN ADMISSION_OTHERS_INFO as i ON i.ApplyLevel=y.YearID AND i.ApplyYear=yc.AcademicYearID 
		WHERE yc.AcademicYearID='$SchoolYearID'
		GROUP BY y.YearID 
		ORDER BY y.Sequence";
$form_list = $li->returnResultSet($sql);
$form_list_count = count($form_list);

$yearIdToBallotGroupIndex = array();
for($i=0;$i<$form_list_count;$i++){
	$yearIdToBallotGroupIndex[$form_list[$i]['YearID']] = $i;
}


$YEAR_WHOLE_DAY_QUOTA = $BALLOT_GROUPS['3'][$yearIdToBallotGroupIndex[$YearID]]['Special'] + $BALLOT_GROUPS['3'][$yearIdToBallotGroupIndex[$YearID]]['SpecialSpare'] + $BALLOT_GROUPS['3'][$yearIdToBallotGroupIndex[$YearID]]['General'] + $BALLOT_GROUPS['3'][$yearIdToBallotGroupIndex[$YearID]]['GeneralSpare'];

// Get appending applicants
$sql = "SELECT 
			s.ApplicationID,
			s.ChineseName,
			s.EnglishName,
			s.IsTwins,
			s.IsTwinsApplied,
			s.TwinsApplicationID,
			p.lsSingleParents,
			o.ApplyDayType1,
			o.ApplyDayType2,
			o.ApplyDayType3,
			o.ApplyLevel,
			a.Status 
		FROM ADMISSION_STU_INFO as s 
		INNER JOIN ADMISSION_PG_INFO as p ON p.ApplicationID=s.ApplicationID 
		INNER JOIN ADMISSION_OTHERS_INFO as o ON o.ApplicationID=s.ApplicationID 
		LEFT JOIN ADMISSION_APPLICATION_STATUS as a ON a.ApplicationID=s.ApplicationID 
		WHERE o.ApplyYear='$SchoolYearID' AND o.ApplyLevel='$YearID' AND a.Status='".$admission_cfg['Status']['pending']."' 
		GROUP BY s.ApplicationID 
		ORDER BY s.ApplicationID";
$applicant_list = $li->returnResultSet($sql);
$applicant_count = count($applicant_list);
//debug_r($sql);
$allocated_application_id = array(); // applicants that get whole day slot
$half_day_applicants = array(); // applicants that firt choice is not whole day or applicants that cannot get first choice of whole day 
$half_day_application_id = array();
$allocated_half_day_application_id = array();


// initialize ballot result with existing result
$sql = "SELECT * FROM ADMISSION_APPLICATION_BALLOT_LOG WHERE SchoolYearID='$SchoolYearID' AND YearID='$YearID'";
$records = $li->returnResultSet($sql);
$record_count = count($records);
for($i=0;$i<$record_count;$i++){
	$application_id = $records[$i]['ApplicationID'];
	$ballot_group_type = $records[$i]['BallotGroupType'];
	$ballot_group_index = $records[$i]['BallotGroupIndex'];
	$special_normal = $records[$i]['SpecialNormal'];
	$status = $records[$i]['Status'];
	
	$field = '';
	if($special_normal == '1') // special
	{
		$field .= 'Special';
	}else{ // 2
		$field .= 'General';
	}
	if($status == $admission_cfg['Status']['reserve']){
		$field .= 'Spare';
		$BALLOT_APPLICANTS[$ballot_group_type][$ballot_group_index]['Total'][] = $application_id;
	}
	
	$BALLOT_APPLICANTS[$ballot_group_type][$ballot_group_index][$field][] = $application_id;
	if($ballot_group_type == '3'){
		$allocated_application_id[] = $application_id;
	}else if($ballot_group_type == '1' || $ballot_group_type == '2'){
		$half_day_application_id[] = $application_id;
		$allocated_half_day_application_id[] = $application_id;
	}
}

$sql = "SELECT 
			s.ApplicationID 
		FROM ADMISSION_STU_INFO as s 
		INNER JOIN ADMISSION_PG_INFO as p ON p.ApplicationID=s.ApplicationID 
		INNER JOIN ADMISSION_OTHERS_INFO as o ON o.ApplicationID=s.ApplicationID 
		LEFT JOIN ADMISSION_APPLICATION_STATUS as a ON a.ApplicationID=s.ApplicationID 
		WHERE o.ApplyYear='$SchoolYearID' AND o.ApplyLevel='$YearID' AND a.Status='".$admission_cfg['Status']['pending']."' AND o.ApplyDayType1='3' 
		GROUP BY s.ApplicationID";
$tmp = $li->returnResultSet($sql);
$whole_day_applicant_count = count($tmp) + count($BALLOT_APPLICANTS['3'][$yearIdToBallotGroupIndex[$YearID]]['Special']) + count($BALLOT_APPLICANTS['3'][$yearIdToBallotGroupIndex[$YearID]]['SpecialSpare']) + count($BALLOT_APPLICANTS['3'][$yearIdToBallotGroupIndex[$YearID]]['General']) + count($BALLOT_APPLICANTS['3'][$yearIdToBallotGroupIndex[$YearID]]['GeneralSpare']);

// Stage 1 : Allocate to Whole Day (which is the first choice)
while( count($allocated_application_id) < $applicant_count && count($allocated_application_id) < $YEAR_WHOLE_DAY_QUOTA && count($allocated_application_id)<$whole_day_applicant_count){ // loop until all applicants are allocated AND there are still empty slots
	// pick up a applicant
	$rand_index = mt_rand(0,$applicant_count-1); // mt_rand() is boundary inclusive
	$application_id = $applicant_list[$rand_index]['ApplicationID'];
	
	if(in_array($application_id,$allocated_application_id)){
		continue;
	}
	
	$status = $applicant_list[$rand_index]['Status']; 
	$year_id = $applicant_list[$rand_index]['ApplyLevel'];
	$apply_day_type = $applicant_list[$rand_index]['ApplyDayType1'];
	$is_single_parent = $applicant_list[$rand_index]['lsSingleParents'] == 'Y';
	$is_twins = $applicant_list[$rand_index]['IsTwins'] == 'Y' && $applicant_list[$rand_index]['IsTwinsApplied'] == 'Y' && $applicant_list[$rand_index]['TwinsApplicationID']!='';
	$twins_application_id = $applicant_list[$rand_index]['TwinsApplicationID'];
	
	if($apply_day_type == '3'){ // first choice is WHOLE DAY
	
		$ballot_group_index = $yearIdToBallotGroupIndex[$year_id];
		
		if(isset($BALLOT_GROUPS[$apply_day_type][$ballot_group_index])){
			$max_special = $BALLOT_GROUPS[$apply_day_type][$ballot_group_index]['Special'];
			$max_special_spare = $BALLOT_GROUPS[$apply_day_type][$ballot_group_index]['SpecialSpare'];
			$special_arr = $BALLOT_APPLICANTS[$apply_day_type][$ballot_group_index]['Special'];
			$special_spare_arr = $BALLOT_APPLICANTS[$apply_day_type][$ballot_group_index]['SpecialSpare'];
			
			// is single parent or special AND have available special slots or special spare slots
			if($is_single_parent && (count($special_arr) < $max_special || count($special_spare_arr) < $max_special_spare)){
				
				if(count($special_arr) < $max_special && !in_array($application_id,$special_arr)){
					// have available slots in special session
					$BALLOT_APPLICANTS[$apply_day_type][$ballot_group_index]['Special'][] = $application_id;
					$BALLOT_APPLICANTS[$apply_day_type][$ballot_group_index]['Total'][] = $application_id;
					
					// process twins together
					if($is_twins && !in_array($twins_application_id, $allocated_application_id)){
						if(count($BALLOT_APPLICANTS[$apply_day_type][$ballot_group_index]['Special']) >= $max_special){
							$BALLOT_APPLICANTS[$apply_day_type][$ballot_group_index]['SpecialSpare'][] = $twins_application_id;
							insertBallotLog($SchoolYearID,$year_id,$twins_application_id,$apply_day_type,$ballot_group_index,'1',$admission_cfg['Status']['reserve']);
						}else{
							$BALLOT_APPLICANTS[$apply_day_type][$ballot_group_index]['Special'][] = $twins_application_id;
							$BALLOT_APPLICANTS[$apply_day_type][$ballot_group_index]['Total'][] = $twins_application_id;
							insertBallotLog($SchoolYearID,$year_id,$twins_application_id,$apply_day_type,$ballot_group_index,'1',$admission_cfg['Status']['confirmed']);
						}
						$allocated_application_id[] = $twins_application_id;
					}
					
					$allocated_application_id[] = $application_id;
					insertBallotLog($SchoolYearID,$year_id,$application_id,$apply_day_type,$ballot_group_index,'1',$admission_cfg['Status']['confirmed']);
					
				}else if(count($special_spare_arr) < $max_special_spare && !in_array($application_id,$special_spare_arr)){
					// no more special slots but have spare special slot
					$BALLOT_APPLICANTS[$apply_day_type][$ballot_group_index]['SpecialSpare'][] = $application_id;
					
					// process twins together
					if($is_twins && !in_array($twins_application_id, $allocated_application_id)){
						$BALLOT_APPLICANTS[$apply_day_type][$ballot_group_index]['SpecialSpare'][] = $twins_application_id;
						$allocated_application_id[] = $twins_application_id;
						insertBallotLog($SchoolYearID,$year_id,$twins_application_id,$apply_day_type,$ballot_group_index,'1',$admission_cfg['Status']['reserve']);
					}
					
					$allocated_application_id[] = $application_id;
					insertBallotLog($SchoolYearID,$year_id,$application_id,$apply_day_type,$ballot_group_index,'1',$admission_cfg['Status']['reserve']);
				}else{
					// no more available special slots and spare slots
					if($ballot_group_index == 2 && !in_array($application_id,$half_day_application_id)){ // put to second choice HALF DAY AM/PM or third choice HALF DAY AM/PM
						$half_day_applicants[] = $applicant_list[$rand_index];
						$half_day_application_id[] = $application_id;
					}
					//$allocated_application_id[] = $application_id;
				}
			}else{ // general
				$max_general = $BALLOT_GROUPS[$apply_day_type][$ballot_group_index]['General'];
				$max_general_spare = $BALLOT_GROUPS[$apply_day_type][$ballot_group_index]['GeneralSpare'];
				$general_arr = $BALLOT_APPLICANTS[$apply_day_type][$ballot_group_index]['General'];
				$general_spare_arr = $BALLOT_APPLICANTS[$apply_day_type][$ballot_group_index]['GeneralSpare'];
				
				if(count($general_arr) < $max_general && !in_array($application_id,$general_arr)){
					// have available general slots 
					$BALLOT_APPLICANTS[$apply_day_type][$ballot_group_index]['General'][] = $application_id;
					$BALLOT_APPLICANTS[$apply_day_type][$ballot_group_index]['Total'][] = $application_id;
					
					// process twins together
					if($is_twins && !in_array($twins_application_id, $allocated_application_id)){
						if(count($BALLOT_APPLICANTS[$apply_day_type][$ballot_group_index]['GENERAL']) >= $max_general){
							$BALLOT_APPLICANTS[$apply_day_type][$ballot_group_index]['GeneralSpare'][] = $twins_application_id;
							insertBallotLog($SchoolYearID,$year_id,$twins_application_id,$apply_day_type,$ballot_group_index,'2',$admission_cfg['Status']['reserve']);
						}else{
							$BALLOT_APPLICANTS[$apply_day_type][$ballot_group_index]['General'][] = $twins_application_id;
							$BALLOT_APPLICANTS[$apply_day_type][$ballot_group_index]['Total'][] = $twins_application_id;
							insertBallotLog($SchoolYearID,$year_id,$twins_application_id,$apply_day_type,$ballot_group_index,'2',$admission_cfg['Status']['confirmed']);
						}
						$allocated_application_id[] = $twins_application_id;
					}
					
					$allocated_application_id[] = $application_id;
					insertBallotLog($SchoolYearID,$year_id,$application_id,$apply_day_type,$ballot_group_index,'2',$admission_cfg['Status']['confirmed']);
					
				}else if(count($general_spare_arr) < $max_general_spare && !in_array($application_id,$general_spare_arr)){
					// no more general slots but have spare general slots
					$BALLOT_APPLICANTS[$apply_day_type][$ballot_group_index]['GeneralSpare'][] = $application_id;
					
					// process twins together
					if($is_twins && !in_array($twins_application_id, $allocated_application_id)){
						$BALLOT_APPLICANTS[$apply_day_type][$ballot_group_index]['GeneralSpare'][] = $twins_application_id;
						$allocated_application_id[] = $twins_application_id;
						insertBallotLog($SchoolYearID,$year_id,$twins_application_id,$apply_day_type,$ballot_group_index,'2',$admission_cfg['Status']['reserve']);
					}
					
					$allocated_application_id[] = $application_id;
					insertBallotLog($SchoolYearID,$year_id,$application_id,$apply_day_type,$ballot_group_index,'2',$admission_cfg['Status']['reserve']);
				}else{
					// no more general slots or spare general slots
					if($ballot_group_index == 2 && !in_array($application_id,$half_day_application_id)){ // put to second choice HALF DAY AM/PM or third choice HALF DAY AM/PM
						$half_day_applicants[] = $applicant_list[$rand_index];
						$half_day_application_id[] = $application_id;
					}
					//$allocated_application_id[] = $application_id;
				}
			}
		}
		//$allocated_application_id[] = $application_id;
	}else if($apply_day_type == '1' || $apply_day_type == '2'){
		if($ballot_group_index == 2 && !in_array($application_id,$half_day_application_id)){ // put to second choice HALF DAY AM/PM or third choice HALF DAY AM/PM
			$half_day_applicants[] = $applicant_list[$rand_index];
			$half_day_application_id[] = $application_id;
		}
		//$allocated_application_id[] = $application_id;
	}else{ // invalid ApplyDayType
		$allocated_application_id[] = $application_id;
	}
	usleep(10);
} // end of whole day

if($yearIdToBallotGroupIndex[$YearID] != '2'){ // NOT 幼兒班2
	//debug_r($BALLOT_APPLICANTS);
	echo displayBallotResultTable($SchoolYearID,$YearID);
	intranet_closedb();
	exit;
}

// put the remaining not drawed applicants in step 1 to half day applicants list
for($i=0;$i<$applicant_count;$i++){
	$application_id = $applicant_list[$i]['ApplicationID'];
	
	if(!in_array($application_id,$allocated_application_id) && !in_array($application_id,$half_day_application_id)){
		$half_day_applicants[] = $applicant_list[$i];
		$half_day_application_id[] = $application_id;
	}
}

$half_day_applicant_count = count($half_day_applicants);
//$allocated_half_day_application_id = array();
$remain_half_day_applicants = array();
$remain_half_day_application_id = array();

$YEAR_AM_QUOTA = $BALLOT_GROUPS['1'][0]['General'] + $BALLOT_GROUPS['1'][0]['GeneralSpare'];
$YEAR_PM_QUOTA = $BALLOT_GROUPS['2'][0]['General'] + $BALLOT_GROUPS['2'][0]['GeneralSpare'];
$YEAR_HALF_DAY_QUOTA = $YEAR_AM_QUOTA + $YEAR_PM_QUOTA;

// Stage 2 : Allocate to half day 
while(count($allocated_half_day_application_id) < $half_day_applicant_count && count($allocated_half_day_application_id)<$YEAR_HALF_DAY_QUOTA)
{
	// pick up a applicant
	$rand_index = mt_rand(0,$half_day_applicant_count-1); // mt_rand() is boundary inclusive
	$application_id = $half_day_applicants[$rand_index]['ApplicationID'];
	
	if(in_array($application_id,$allocated_half_day_application_id) || in_array($application_id,$remain_half_day_application_id)){
		continue;
	}
	
	$status = $half_day_applicants[$rand_index]['Status']; 
	$year_id = $half_day_applicants[$rand_index]['ApplyLevel'];
	$apply_day_type1 = $half_day_applicants[$rand_index]['ApplyDayType1'];
	$apply_day_type2 = $half_day_applicants[$rand_index]['ApplyDayType2'];
	$apply_day_type3 = $half_day_applicants[$rand_index]['ApplyDayType3'];
	$apply_day_types = array();
	// exclude (whole day) 3
	if($apply_day_type1 != '3' && $apply_day_type1 != '0'){
		$apply_day_types[] = $apply_day_type1;
	}
	if($apply_day_type2 != '3' && $apply_day_type2 != '0'){
		$apply_day_types[] = $apply_day_type2;
	}
	if($apply_day_type3 != '3' && $apply_day_type3 != '0'){
		$apply_day_types[] = $apply_day_type3;
	}
	if(count($apply_day_types)==0){
		$apply_day_types[] = '1';
		$apply_day_types[] = '2';
	}
	if(count($apply_day_types)==1){
		if(!in_array('1',$apply_day_types)) $apply_day_types[] = '1';
		if(!in_array('2',$apply_day_types)) $apply_day_types[] = '2';
	}
	
	$is_twins = $half_day_applicants[$rand_index]['IsTwins'] == 'Y' && $half_day_applicants[$rand_index]['IsTwinsApplied'] && $half_day_applicants[$rand_index]['TwinsApplicationID']!='';
	$twins_application_id = $half_day_applicants[$rand_index]['TwinsApplicationID'];
	
	for($i=0;$i<count($apply_day_types);$i++)
	{
		if(in_array($application_id,$allocated_half_day_application_id)){
			continue;
		}
		$apply_day_type = $apply_day_types[$i];
		$max_general = $BALLOT_GROUPS[$apply_day_type][0]['General'];
		$max_general_spare = $BALLOT_GROUPS[$apply_day_type][0]['GeneralSpare'];
		$general_arr = $BALLOT_APPLICANTS[$apply_day_type][0]['General'];
		$general_spare_arr = $BALLOT_APPLICANTS[$apply_day_type][0]['GeneralSpare'];
		
		if(count($general_arr) < $max_general && !in_array($application_id,$general_arr)){
			// have available general slots 
			$BALLOT_APPLICANTS[$apply_day_type][0]['General'][] = $application_id;
			$BALLOT_APPLICANTS[$apply_day_type][0]['Total'][] = $application_id;
			
			// process twins together
			if($is_twins && !in_array($twins_application_id, $allocated_half_day_application_id)){
				if(count($BALLOT_APPLICANTS[$apply_day_type][0]['GENERAL']) >= $max_general){
					$BALLOT_APPLICANTS[$apply_day_type][0]['GeneralSpare'][] = $twins_application_id;
					insertBallotLog($SchoolYearID,$year_id,$twins_application_id,$apply_day_type,'0','2',$admission_cfg['Status']['reserve']);
				}else{
					$BALLOT_APPLICANTS[$apply_day_type][0]['General'][] = $twins_application_id;
					$BALLOT_APPLICANTS[$apply_day_type][0]['Total'][] = $twins_application_id;
					insertBallotLog($SchoolYearID,$year_id,$twins_application_id,$apply_day_type,'0','2',$admission_cfg['Status']['confirmed']);
				}
				$allocated_half_day_application_id[] = $twins_application_id;
			}
			
			$allocated_half_day_application_id[] = $application_id;
			insertBallotLog($SchoolYearID,$year_id,$application_id,$apply_day_type,'0','2',$admission_cfg['Status']['confirmed']);
		}else if(count($general_spare_arr) < $max_general_spare && !in_array($application_id,$general_spare_arr)){
			// no more general slots but have spare general slots
			$BALLOT_APPLICANTS[$apply_day_type][0]['GeneralSpare'][] = $application_id;
			
			// process twins together
			if($is_twins && !in_array($twins_application_id, $allocated_half_day_application_id)){
				$BALLOT_APPLICANTS[$apply_day_type][0]['GeneralSpare'][] = $twins_application_id;
				$allocated_half_day_application_id[] = $twins_application_id;
				insertBallotLog($SchoolYearID,$year_id,$twins_application_id,$apply_day_type,'0','2',$admission_cfg['Status']['reserve']);
			}
			
			$allocated_half_day_application_id[] = $application_id;
			insertBallotLog($SchoolYearID,$year_id,$application_id,$apply_day_type,'0','2',$admission_cfg['Status']['reserve']);
		}else{
			// no more general slots or spare general slots
			if(!in_array($application_id,$remain_half_day_application_id)){
				$remain_half_day_applicants[] = $half_day_applicants[$rand_index];
				$remain_half_day_application_id[] = $application_id;
			}
			//$allocated_half_day_application_id[] = $application_id;
		}
		//$allocated_half_day_application_id[] = $application_id;
	}
	usleep(10);
}

// The remaining non-allocated/failed applicants
/*
for($i=0;$i<$half_day_applicant_count;$i++){
	$application_id = $half_day_applicants[$i]['ApplicationID'];
	
	if(!in_array($application_id,$allocated_application_id) && !in_array($application_id,$allocated_half_day_application_id) && !in_array($application_id,$remain_half_day_application_id)){
		$remain_half_day_applicants[] = $half_day_applicants[$i];
		$remain_half_day_application_id[] = $application_id;
	}
}
*/
//debug_r("Number of applicants is ". count($applicant_list));
//debug_r("Number of half day applicants is ".count($half_day_applicants));
//debug_r("Number of whole day: ".count($allocated_application_id));
//debug_r("Number of half day: ".count($allocated_half_day_application_id));
//debug_r("Number of remaining: ".count($remain_half_day_application_id));
//debug_r($BALLOT_APPLICANTS);
echo displayBallotResultTable($SchoolYearID,$YearID);

intranet_closedb();
?>