<?php
# using by Henry
/***************************************************************
*	modification log
*
*	2015-03-17 Henry
*	- exclude the cancelled records
*
****************************************************************/
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."kis/init.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
require_once($PATH_WRT_ROOT."includes/admission/sinmeng.eclass.hk/libadmission_cust.php");
require_once($PATH_WRT_ROOT."lang/kis/apps/lang_admission_".$intranet_session_language.".php");

intranet_auth();
intranet_opendb();

$libadmission_cust = new admission_cust();
$fcm = new form_class_manage();

$AcademicYearID = $libadmission_cust->schoolYearID;
$academic_year_obj  = new academic_year($AcademicYearID);
$academic_year_name = $academic_year_obj->YearNameB5;

$sql = "SELECT y.YearID, y.YearName, yc.AcademicYearID, COUNT(DISTINCT i.RecordID) as NumberOfApplicant  
		FROM YEAR as y 
		INNER JOIN YEAR_CLASS as yc ON yc.YearID=y.YearID 
		LEFT JOIN ADMISSION_OTHERS_INFO as i ON i.ApplyLevel=y.YearID AND i.ApplyYear=yc.AcademicYearID 
		LEFT JOIN ADMISSION_APPLICATION_STATUS as s ON s.ApplicationID = i.ApplicationID
		WHERE yc.AcademicYearID='$AcademicYearID' AND s.Status<>'5'
		GROUP BY y.YearID 
		ORDER BY y.Sequence";
$form_list = $libadmission_cust->returnResultSet($sql);
$form_list_count = count($form_list);
$total_applicants = 0;
for($i=0;$i<$form_list_count;$i++){
	$total_applicants += $form_list[$i]['NumberOfApplicant'];
}

intranet_closedb();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>:: 善明托兒所 <?=$academic_year_name?> 收生抽籤 ::</title>
<link href="css/content.css" rel="stylesheet" type="text/css" />
<script src="script/action.js" type="text/JavaScript"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
</head>

<script language="JavaScript" type="text/JavaScript">
/*
Use SetText() to input the big number.
*/
$(document).ready( function() {
	SetText(<?=$total_applicants?>);
});

</script>

<body>

<div class="page2_bg">
	<div class="p2_text_person"></div>
	<div class="p2_cloud"></div>
	<div class="count_text">
		<div id="text_4" class="t4">4</div>
		<div id="text_3" class="t3">5</div>
		<div id="text_2" class="t2">7</div>
		<div id="text_1" class="t1">0</div>
	</div>
	<div class="btn_contin basicBtn" onClick="ToPage('step3')"></div>
</div>

</body>
</html>