<?php 
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."kis/init.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
require_once($PATH_WRT_ROOT."includes/admission/sinmeng.eclass.hk/libadmission_cust.php");
require_once($PATH_WRT_ROOT."lang/kis/apps/lang_admission_".$intranet_session_language.".php");

intranet_auth();
intranet_opendb();

//$gpNum;
//$type;
//$YearID;

$libadmission_cust = new admission_cust();
$fcm = new form_class_manage();

$AcademicYearID = $_REQUEST['SchoolYearID']!=''?  $_REQUEST['SchoolYearID'] : $libadmission_cust->schoolYearID;
$academic_year_obj  = new academic_year($AcademicYearID);
$academic_year_name = $academic_year_obj->YearNameB5;

// Get year form 
$sql = "SELECT y.YearID 
		FROM YEAR as y 
		INNER JOIN YEAR_CLASS as yc ON yc.YearID=y.YearID 
		LEFT JOIN ADMISSION_OTHERS_INFO as i ON i.ApplyLevel=y.YearID AND i.ApplyYear=yc.AcademicYearID 
		WHERE yc.AcademicYearID='$AcademicYearID'
		GROUP BY y.YearID 
		ORDER BY y.Sequence";
$form_list = $libadmission_cust->returnResultSet($sql);
$form_list_count = count($form_list);

$yearIdToBallotGroupIndex = array();
$indexToYearId = array();
for($i=0;$i<$form_list_count;$i++){
	$yearIdToBallotGroupIndex[$form_list[$i]['YearID']] = $i;
	$indexToYearId[$i] = $form_list[$i]['YearID'];
}

// $goto_page = '';
// if($gpNum == 1){ //  嬰兒組
// 	// go to special.php
// 	$goto_page = 'special.php';
// }else if($gpNum == 2){ //  幼兒組1
// 	// go to special.php
// 	$goto_page = 'special.php';
// }else if($gpNum == 3){ //  幼兒組2
// 	if($type == 1){
// 		// go to special.php
// 		$goto_page = 'special.php';
// 	}else if($type == 2 || $type == 3){
// 		// go to general.php
// 		$goto_page = 'general.php';
// 	}
// }
$goto_page = 'general.php';

intranet_closedb();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>:: 善明托兒所 <?=$academic_year_name?> 收生抽籤 ::</title>
<link href="css/content.css" rel="stylesheet" type="text/css" />
<script src="script/action.js" type="text/JavaScript"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
</head>

<script language="JavaScript" type="text/JavaScript">
/*
gpNum : 1=baby , 2=child_1 , 3=child_2
type  : 1=fullday , 2=morning , 3=afternoon

Use Set_Gp_Type() to input the group and type, which user selected at page_3
*/
$(document).ready( function() {
	Set_Gp_Type(<?=$gpNum?>, <?=$type?>);
});

function CallStart() {
	/* your start button action here */
	//ToPage("page_6");
	var formObj = document.getElementById('form1');
	formObj.action = '<?=$goto_page?>';
	formObj.submit();
}

</script>

<body>
<form id="form1" name="form1" method="post" action="">
<div class="page5_bg">
	<div class="p5_img_gp">
		<div id="type" class="p4_type"></div>
		<div id="gp" class="p4_gp"></div>
	</div>
	<div class="btn_start" onClick="CallStart()"></div>
</div>
<input type="hidden" id="gpNum" name="gpNum" value="<?=$gpNum?>" />
<input type="hidden" id="type" name="type" value="<?=$type?>" />
<input type="hidden" id="SchoolYearID" name="SchoolYearID" value="<?=$AcademicYearID?>" />
<input type="hidden" id="YearID" name="YearID" value="<?=$YearID?>" />
</form>
</body>
</html>