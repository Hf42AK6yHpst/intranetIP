<?php 
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."kis/init.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
require_once($PATH_WRT_ROOT."includes/admission/sinmeng.eclass.hk/libadmission_cust.php");
require_once($PATH_WRT_ROOT."lang/kis/apps/lang_admission_".$intranet_session_language.".php");

intranet_auth();
intranet_opendb();

$libadmission_cust = new admission_cust();
$fcm = new form_class_manage();

$AcademicYearID = $libadmission_cust->schoolYearID;
$academic_year_obj  = new academic_year($AcademicYearID);
$academic_year_name = $academic_year_obj->YearNameB5;

intranet_closedb();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>:: 善明托兒所 <?=$academic_year_name?> 收生抽籤 ::</title>
<link href="css/content.css" rel="stylesheet" type="text/css" />
<script src="script/action.js" type="text/JavaScript"></script>
</head>

<body>

<div class="page1_bg">
	<div class="btn_enter basicBtn" onClick="ToPage('step2')"></div>
</div>

<div class="preload_area">
	<div class="page2_bg"></div>
	<div class="page3_bg"></div>
	<div class="page4_bg"></div>
	<div class="page5_bg"></div>
	<div class="page6_bg"></div>
	<div class="p6_fx_gp">
		<div class="p6_balloon normal t1"></div>
		<div class="p6_balloon break t1"></div>
		<div class="p6_balloon normal t2"></div>
		<div class="p6_balloon break t2"></div>
		<div class="p6_balloon normal t3"></div>
		<div class="p6_balloon break t3"></div>
		<div class="p6_balloon normal t4"></div>
		<div class="p6_balloon break t4"></div>
	</div>
</div>

</body>
</html>