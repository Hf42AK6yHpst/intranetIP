﻿/* Common */
/***************************************************************
*	using by Henry
*
*	modification log
*
*	2016-04-27 Henry
*	- modified SetShowGp to add No single parent apply text
*
*	2015-02-03 Henry
*	- modified SetPartTotal, SetShowGp to add Loading... waiting text
*
*	2015-02-03 Henry
*	- modified SetShowGp to add number of order of drawing
****************************************************************/
var fileExt = "php";
var balloonBroken = false;	/*20140806*/

function ToPage(page, param) {
	var url = page + "." + fileExt;
	if (param != undefined) {
		url += param;
	}
	MM_goToURL("self", url);
	return document.MM_returnValue;
}

function MM_goToURL() { //v3.0
	var i, args=MM_goToURL.arguments;
	document.MM_returnValue = false;
	for (i=0; i<(args.length-1); i+=2)
		eval(args[i]+".location='"+args[i+1]+"'");
}

/* extract GET url variables */
var QueryString = function () {
	// This function is anonymous, is executed immediately and 
	// the return value is assigned to QueryString!
	var query_string = {};
	var query = window.location.search.substring(1);
	var vars = query.split("&");
	for (var i=0;i<vars.length;i++) {
		var pair = vars[i].split("=");
		// If first entry with this name
		if (typeof query_string[pair[0]] === "undefined") {
			query_string[pair[0]] = pair[1];
			// If second entry with this name
		} else if (typeof query_string[pair[0]] === "string") {
			var arr = [ query_string[pair[0]], pair[1] ];
			query_string[pair[0]] = arr;
			// If third or later entry with this name
		} else {
			query_string[pair[0]].push(pair[1]);
		}
	}
	return query_string;
} ();


// ==================== page function =======================

function SetText(value) {
	$("#text_4").html( Math.floor(value/1000) );
	if (value < 1000) {
		$("#text_4").css("display", "none");
	}
	$("#text_3").html( Math.floor((value%1000)/100) );
	if (value < 100) {
		$("#text_3").css("display", "none");
	}
	$("#text_2").html( Math.floor((value%100)/10) );
	if (value < 10) {
		$("#text_2").css("display", "none");
	}
	$("#text_1").html( Math.floor(value%10) );
}

function DisableBtn(gpNum, type) {
	$("#btn_"+gpNum+"_"+type+" div").addClass("disable");
	$("#btn_"+gpNum+"_"+type).css("pointerEvents", "none");
}

function Set_Gp_Type(gpNum, type) {
	$("#gp").addClass("gp" + gpNum);
	if(type == 1){
		type = 4;
	}
	$("#type").addClass("type" + type);
}

function Set_Gp_Type_Part(gpNum, type, part) {
	$("#gp").addClass("gp" + gpNum);
	if(type == 1){
		type = 4;
	}
	$("#type").addClass("type" + type);
	$("#part").addClass("part" + part);
	$("#ballNormal").addClass("t" + part);
	$("#ballBreak").addClass("t" + part);
}
function SetPartTotal(num) {
	var clicked_btn_area = 0;
	$("#btn_area").click(function() {
		if(clicked_btn_area == 0){
			clicked_btn_area = 1;
			setTimeout(function(){ $("#part_num").html(num); }, 8000);
		}
	});
	//$("#part_num").html(num);
}
function SetShowGp(arr) {
	var str = "";
	for (var i = 0; i < arr.length; i++) {
		str += "<div class='draw_obj'><div class='text' style='font-size:38px'>";
		str += "<font size='5'>"+((draw_page_num*8)+(i+1))+".</font>"+arr[i];
		str += "</div></div>";
	}
	
	if(arr.length == 0){
		str += '<div style="color:red;font-size:42px;position:relative;left:60px;top:100px;height:200px;"><b>此組沒有申請名單</b></div>';
	}
	
	if(draw_page_num == 0){
		$("#showGp").html('<div style="font-size:42px;position:relative;left:200px;top:200px;z-index:10;"><b>電腦抽籤現在啟動..... 運算中</b></div>');
		
		var show_btn_next = 0;
		var show_endGp = 0;
		/*if($('.btn_next').css('display') != 'none')
		{
			$(".btn_next").hide();
			show_btn_next = 1;
		}*/
		if($('.end_line').css('display') != 'none')
		{
			$(".end_line").hide();
			$(".btn_back").hide();
			show_endGp = 1;
		}
	
		$(".btn_next").hide();
		var clicked_btn_area = 0;
		$("#btn_area").click(function() {
			if(clicked_btn_area == 0){
				clicked_btn_area = 1;
				setTimeout(function(){ 
					$("#showGp").html(str);
					/*if(show_btn_next == 1)
					{
						$(".btn_next").show();
					}*/
					if(show_endGp == 1)
					{
						$(".end_line").show();
						$(".btn_back").show();
					}
					
					$(".btn_next").show();
					
				}, 8000);
			}
		});
	}
	else
		$("#showGp").html(str);
}
/*20140806 start*/
function SetShowType(showType, isEnd) {
	if (showType == "firstShow") {
		balloonBroken = false;
		$("#btn_area").css("display", "block");
		$("#info_gp").css("display", "none");
		$("#part").css("display", "none");
		ShowBalloon();
	} else {
		balloonBroken = true;
		$("#balloonGp").css("display", "none");
		$("#info_gp").css("display", "block");
	}
	if (isEnd) {
		$("#endGp").css("display", "block");
	} else {
		$("#nextGp").css("display", "block");
	}
}
function ShowBalloon() {
	$("#balloonGp").css("display", "block");
}
function DoBreak() {
	if (!balloonBroken) {
		balloonBroken = true;
		$("#btn_area").css("display", "none");
	setTimeout( function() {
		$("#bee").removeClass("stay");
		$("#bee").animate( {left:"810px", top:"-160px", opacity:"1"}, 300);
		$("#bee").animate( {left:"810px", top:"-160px", opacity:"1"}, 500);
		$("#bee").animate( {left:"710px", top:"-10px", opacity:"0.8"}, 500, function() {
			$("#bee").css("display", "none");
			$("#ballNormal").css("display", "none");
			$("#ballBreak").addClass("balloon_broken");
		});
		setTimeout( function() {
			$("#info_gp").css( {display:"block", opacity:"0"} );
			$("#part").css("display", "block");
			$("#info_gp").animate( {opacity:"1"}, 1000, function() {
				$("#ballBreak").css("display", "none");
			});
		}, 1600);
	}, 500);
	}
}
/*20140806 end*/


