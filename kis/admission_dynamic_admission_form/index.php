<?php
// modifying by: Pun
/**
 * ******************
 * Log :
 * Date 2019-06-03 [Pun]
 *  - File created
 * ******************
 */
error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT);
ini_set('display_errors', 1);
$PATH_WRT_ROOT = "../../";

include_once("{$PATH_WRT_ROOT}includes/global.php");
include_once("{$PATH_WRT_ROOT}includes/libdb.php");
include_once("{$PATH_WRT_ROOT}includes/libinterface.php");
include_once("{$PATH_WRT_ROOT}includes/admission/{$setting_path_ip_rel}/config.php");
include_once("{$PATH_WRT_ROOT}includes/admission/HelperClass/DynamicAdmissionFormSystem/DynamicAdmissionFormSystem.php");
$intranet_session_language = $admission_cfg['DefaultLang'];
include_once("{$PATH_WRT_ROOT}includes/admission/{$setting_path_ip_rel}/libadmission_cust.php");
include_once("{$PATH_WRT_ROOT}includes/admission/{$setting_path_ip_rel}/libadmission_ui_cust.php");
include("{$PATH_WRT_ROOT}lang/lang.{$intranet_session_language}.php");
include("{$PATH_WRT_ROOT}lang/kis/lang_common_{$intranet_session_language}.php");
include("{$PATH_WRT_ROOT}lang/kis/apps/lang_admission_{$intranet_session_language}.php");
include("{$PATH_WRT_ROOT}lang/admission_lang.{$intranet_session_language}.php");
$LangB5 = $Lang;
$kis_lang_b5 = $kis_lang;
intranet_opendb();

#### Access right START ####
if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-eAdmission"]) {
    header('HTTP/1.0 403 Forbidden');
    exit;
}
#### Access right END ####


#### INIT START ####
$lac = new admission_cust();
$AcademicYearID = ($AcademicYearID) ? $AcademicYearID : $lac->schoolYearID;
$dafs = new \AdmissionSystem\DynamicAdmissionFormSystem($AcademicYearID);
#### INIT END ####


?>
<!DOCTYPE html>
<html>
<head>
    <!-- Standard Meta -->
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <!-- Site Properties -->
    <title>Theming - Semantic</title>
    <link rel="stylesheet" type="text/css" href="assets/Semantic-UI-CSS-master/semantic.min.css">
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"
            integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"
            integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU=" crossorigin="anonymous"></script>
    <script src="assets/Semantic-UI-CSS-master/semantic.min.js"></script>
    <script src="assets/main.js"></script>
    <style>
        body {
            background-color: #FFFFFF;
        }

        .ui.menu .item img.logo {
            margin-right: 1.5em;
        }

        .main.container {
            margin-top: 7em;
        }

        .wireframe {
            margin-top: 2em;
        }

        .ui.footer.segment {
            margin: 5em 0em 0em;
            padding: 5em 0em;
        }
    </style>
</head>
<body>
<div class="ui fixed inverted menu">
    <div class="ui container">
        <a href="index.php" class="header item">
            Dynamic admission form editor
        </a>
        <!--div class="ui simple dropdown item">
            Dropdown <i class="dropdown icon"></i>
            <div class="menu">
                <a class="item" href="#">Link Item</a>
                <a class="item" href="#">Link Item</a>
                <div class="divider"></div>
                <div class="header">Header Item</div>
                <div class="item">
                    <i class="dropdown icon"></i>
                    Sub Menu
                    <div class="menu">
                        <a class="item" href="#">Link Item</a>
                        <a class="item" href="#">Link Item</a>
                    </div>
                </div>
                <a class="item" href="#">Link Item</a>
            </div>
        </div-->
    </div>
</div>
<div class="main ui container">
    <?php if ($_GET['result'] === '1'): ?>
        <div class="ui green message"><i class="check icon"></i> Saved</div>
    <?php elseif ($_GET['result'] === '0'): ?>
        <div class="ui red message"><i class="times icon"></i> Save failed</div>
    <?php endif; ?>

    <?php if (!$sys_custom['KIS_Admission']['DynamicAdmissionForm']): ?>
        <div class="ui red message"><i class="flag icon"></i> Flag is not set
            $sys_custom['KIS_Admission']['DynamicAdmissionForm']
        </div>
    <?php endif; ?>

    <div class="sticky example">
        <div class="ui segment" id="mainDiv">
            <div class="left ui rail">
                <div class="ui sticky">
                    <form id="settingsForm" class="ui form" method="POST" enctype="multipart/form-data">
                        <h4 class="ui dividing header">Settings</h4>
                        <div class="field">
                            <label>Academic Year:</label>
                            <?= getSelectAcademicYear('AcademicYearID', 'class="ui fluid dropdown"', 1, 0, $AcademicYearID); ?>
                        </div>
                        <div class="field">
                            <label>Import / Export:</label>
                            <button id="export" type="button" class="ui primary button">
                                Export
                            </button>
                            <button id="import" type="button" class="ui red button">
                                Import new
                            </button>
                        </div>
                        <div class="field">
                            <label>Delete data:</label>
                            <button id="deleteCurrentYear" type="button" class="ui red button">
                                Delete all current year data
                            </button>
                        </div>
                        <input type="hidden" name="Action"/>
                    </form>
                </div>
            </div>
            <!--div class="right ui rail">
                <div class="ui sticky">
                <h3 class="ui header">Content</h3>
                </div>
            </div-->
            <?php include('group.part.php'); ?>
            <div class="ui divider"></div>
            <?php include('field.part.php'); ?>
        </div>
    </div>
</div>


<div class="ui modal" id="importModal">
    <i class="close icon"></i>
    <div class="header">
        Import
    </div>
    <div class="content">
        <form id="settingsForm" class="ui form" method="POST" action="update.php" enctype="multipart/form-data">
            <input type="hidden" name="AcademicYearID" value=""/>
            <input type="hidden" name="Action" value="Import"/>
            <textarea name="Data"></textarea>
        </form>
    </div>
    <div class="actions">
        <div class="ui black deny button">
            Cancel
        </div>
        <button class="ui positive right labeled red icon button">
            Save
            <i class="save icon"></i>
        </button>
    </div>
</div>


</body>
</html>