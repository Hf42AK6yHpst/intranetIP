<?php
// modifying by: Pun
/**
 * ******************
 * Log :
 * Date 2019-06-03 [Pun]
 *  - File created
 * ******************
 */
error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT);
ini_set('display_errors', 1);
$PATH_WRT_ROOT = "../../";

include_once("{$PATH_WRT_ROOT}includes/global.php");
include_once("{$PATH_WRT_ROOT}includes/libdb.php");
include_once("{$PATH_WRT_ROOT}includes/libinterface.php");
include_once("{$PATH_WRT_ROOT}includes/admission/{$setting_path_ip_rel}/config.php");
include_once("{$PATH_WRT_ROOT}includes/admission/HelperClass/DynamicAdmissionFormSystem/DynamicAdmissionFormSystem.php");
$intranet_session_language = $admission_cfg['DefaultLang'];
include_once("{$PATH_WRT_ROOT}includes/admission/{$setting_path_ip_rel}/libadmission_cust.php");
include_once("{$PATH_WRT_ROOT}includes/admission/{$setting_path_ip_rel}/libadmission_ui_cust.php");
include("{$PATH_WRT_ROOT}lang/lang.{$intranet_session_language}.php");
include("{$PATH_WRT_ROOT}lang/kis/lang_common_{$intranet_session_language}.php");
include("{$PATH_WRT_ROOT}lang/kis/apps/lang_admission_{$intranet_session_language}.php");
include("{$PATH_WRT_ROOT}lang/admission_lang.{$intranet_session_language}.php");
$LangB5 = $Lang;
$kis_lang_b5 = $kis_lang;
intranet_opendb();

#### Access right START ####
if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-eAdmission"]) {
    header('HTTP/1.0 403 Forbidden');
    exit;
}
#### Access right END ####

#### Prevent SQL injection / XSS attack START ####
$_POST['AcademicYearID'] = IntegerSafe($_POST['AcademicYearID']);
#### Prevent SQL injection / XSS attack END ####

#### INIT START ####
$dafs = new \AdmissionSystem\DynamicAdmissionFormSystem($_POST['AcademicYearID']);
#### INIT END ####

######## Group START ########
#### Save Group START ####
if ($_POST['Action'] == 'UpdateGroup') {
    $result = (int)$dafs->upsertGroup($_POST);
}
#### Save Group END ####

#### Delete Group START ####
if ($_POST['Action'] == 'DeleteGroup') {
    $result = (int)$dafs->deleteGroup($_POST['GroupID']);
}
#### Delete Group END ####
######## Group END ########

######## Field START ########
#### Save Field START ####
if ($_POST['Action'] == 'UpdateField') {
    $data = $_POST;
    $otherAttribute = array(
        'Width' => $_POST['Width'],
        'Groupped' => $_POST['Groupped'],
        'Extra' => $_POST['Extra'],
        'DbTableName' => $_POST['DbTableName'],
        'DbFieldName' => $_POST['DbFieldName'],
    );
    $data['Validation'] = json_encode($data['Validation']);
    $data['OtherAttribute'] = json_encode($otherAttribute, JSON_UNESCAPED_UNICODE);

    $result = (int)$dafs->upsertField($data);
}
#### Save Field END ####

#### Copy Field START ####
if ($_POST['Action'] == 'CopyField') {
    $result = (int)$dafs->copyField($_POST['FieldID']);
}
#### Copy Field END ####

#### Reorder Field START ####
if ($_POST['Action'] == 'ReorderField') {
    $result = (int)$dafs->reorderField($_POST['FieldID']);
}
#### Reorder Field END ####

#### Delete Field START ####
if ($_POST['Action'] == 'DeleteField') {
    $result = (int)$dafs->deleteField($_POST['FieldID']);
}
#### Delete Field END ####
######## Field END ########


######## Export START ########
if ($_POST['Action'] == 'Export') {
    $result = array(
        'Groups' => $dafs->getCurrentAcademicYearGroups(),
        'Fields' => $dafs->getCurrentAcademicYearFields(),
    );

    echo base64_encode(serialize($result));
    exit;
}
######## Export END ########

######## Import START ########
if ($_POST['Action'] == 'Import') {
    $academicYearId = $_POST['AcademicYearID'];
    $data = unserialize(base64_decode($Data));

    $groupIdMapping = array();
    $result = true;
    foreach ($data['Groups'] as $group) {
        $oldGroupID = $group['GroupID'];
        $group['GroupID'] = 0;
        $newGroupId = $dafs->upsertGroup($group);
        $result = $result && $newGroupId;
        $groupIdMapping[$oldGroupID] = $newGroupId;
    }
    foreach ($data['Fields'] as $field) {
        $field['GroupID'] = $groupIdMapping[$field['GroupID']];
        $field['FieldID'] = 0;
        $result = $dafs->upsertField($field) && $result;
    }
}
######## Import END ########

######## DeleteAllData START ########
if ($_POST['Action'] == 'DeleteAllData') {
    $result = (int)$dafs->deleteAllDataByAcademicYear($_POST['AcademicYearID']);
}
######## DeleteAllData END ########

//echo "Location: index.php?AcademicYearID={$_POST['AcademicYearID']}&GroupID={$_POST['GroupID']}&result={$result}";
//exit;


header("Location: index.php?AcademicYearID={$_POST['AcademicYearID']}&GroupID={$_POST['GroupID']}&result={$result}");
exit;
