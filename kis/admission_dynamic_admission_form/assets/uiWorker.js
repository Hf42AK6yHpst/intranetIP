var $ = window.jQuery;
$('.ui.sticky').sticky({
  context: '#mainDiv',
  offset: 45,
});
$('.remarkPopup').popup();
$('select.dropdown').dropdown();
$('.ui.checkbox').checkbox();
$('.menu .item').tab();
$('.ui.accordion').accordion();
$('.sortFieldContainer').sortable();
