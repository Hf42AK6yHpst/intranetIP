<?php

#### Get db table and field START ####
$dbTables = array('ADMISSION_STU_INFO','ADMISSION_PG_INFO','ADMISSION_OTHERS_INFO','ADMISSION_SIBLING','ADMISSION_STU_PREV_SCHOOL_INFO','ADMISSION_RELATIVES_AT_SCH_INFO', 'ADMISSION_CUST_INFO');
$excludeFields = array('RecordID', 'DateInput','InputBy','DateModified','ModifiedBy',);
$excludeFieldSql = implode("','", $excludeFields);

$dbTableFields = array();
foreach($dbTables as $dbTable){
    $sql = "SHOW COLUMNS FROM {$dbTable} WHERE Field NOT IN ('{$excludeFieldSql}')";
    $rs = $dafs->returnResultSet($sql);
    foreach($rs as $r){
        $dbTableFields["{$r['Field']} [{$r['Type']}]"][] = $dbTable;
    }
}
ksort($dbTableFields);
#### Get db table and field END ####

$groups = $dafs->getCurrentAcademicYearGroups();


?>
<h1>Field</h1>

<div class="ui top attached tabular menu">
    <?php
    foreach($groups as $index=>$group):
        $class = '';
        if(!$GroupID && $index == 0){
            $class = 'active';
        }elseif($GroupID == $group['GroupID']){
            $class = 'active';
        }
    ?>
    	<a class="item  <?=$class ?>" data-tab="field_tab_<?=$group['GroupID'] ?>">[<?=$group['GroupID'] ?>] <?=intranet_htmlspecialchars($group['TitleB5']) ?></a>
	<?php
	endforeach;
	?>
</div>

<?php
foreach($groups as $index=>$group):
    $fields = $dafs->getFieldsByGroupId($group['GroupID']);

    $class = '';
    if(!$GroupID && $index == 0){
        $class = 'active';
    }elseif($GroupID == $group['GroupID']){
        $class = 'active';
    }
?>
    <div class="ui bottom attached tab segment <?=$class ?>" data-tab="field_tab_<?=$group['GroupID'] ?>">
        <button class="ui icon button editField blue" data-group-id="<?=$group['GroupID'] ?>" data-field-id="0">
        	<i class="plus icon"></i>
        </button>
        <button class="ui icon button reorderField blue" data-group-id="<?=$group['GroupID'] ?>">
        	<i class="arrows alternate vertical icon"></i>
        </button>


        <table class="ui celled table">
        	<thead>
        		<tr>
            		<th>FieldID</th>
            		<th>TitleB5</th>
            		<th>TitleEn</th>
            		<th>TitleGb</th>
            		<th>Type</th>
            		<th>Width</th>
            		<th>OtherAttribute</th>
            		<th>Validation</th>
            		<th>Status</th>
            		<th></th>
            	</tr>
        	</thead>
        	<tbody>
            	<?php
            	foreach($fields as $field):
            	?>
            		<tr>
            			<td data-label="FieldID">
            				<?=$field['FieldID'] ?>
                			<?php if($field['Remark']): ?>
                    			<div class="ui icon remarkPopup" data-html="<?=nl2br(intranet_htmlspecialchars($field['Remark'])) ?>" style="display:inline-block">
                					<i class="info circle icon violet big"></i>
                				</div>
            				<?php endif; ?>
            			</td>

            		<?php if($field['Type'] == \AdmissionSystem\DynamicAdmissionFormSystem::FIELD_TYPE_DUMMY): ?>
            			<td data-label="Title" colspan="5" style="text-align: center;">
            				<?=$field['TitleB5'] ?>
            				<?=$field['TitleEn'] ?>
            				<?=$field['TitleGb'] ?>
            			</td>
            		<?php else: ?>
            			<td data-label="TitleB5">
                			<?php if($field['LabelB5']): ?>
                    			<div class="ui icon remarkPopup" data-html="<?=nl2br(intranet_htmlspecialchars($field['LabelB5'])) ?>" style="display:inline-block">
                					<i class="tag icon olive"></i>
                				</div>
            				<?php endif; ?>
            				<?=$field['TitleB5'] ?>
            			</td>
            			<td data-label="TitleEn">
                			<?php if($field['LabelEn']): ?>
                    			<div class="ui icon remarkPopup" data-html="<?=nl2br(intranet_htmlspecialchars($field['LabelEn'])) ?>" style="display:inline-block">
                					<i class="tag icon olive"></i>
                				</div>
            				<?php endif; ?>
            				<?=$field['TitleEn'] ?>
            			</td>
            			<td data-label="TitleGb">
                			<?php if($field['LabelGb']): ?>
                    			<div class="ui icon remarkPopup" data-html="<?=nl2br(intranet_htmlspecialchars($field['LabelGb'])) ?>" style="display:inline-block">
                					<i class="tag icon olive"></i>
                				</div>
            				<?php endif; ?>
            				<?=$field['TitleGb'] ?>
            			</td>
            			<td data-label="Type"><?=$field['Type'] ?></td>
            			<td data-label="Width"><?=$field['OtherAttributeArr']['Width'] ?></td>
            		<?php endif; ?>

            			<td data-label="OtherAttribute">
                			<?php if($field['OtherAttributeArr']['Extra']): ?>
                    			<div>
                    				<?=nl2br(stripslashes($field['OtherAttributeArr']['Extra'])) ?>
                				</div>
            				<?php endif; ?>

            				<?php if($field['OtherAttributeArr']['DbTableName']): ?>
    							<div style="margin-top: 1rem; color: blueviolet;">
                    				<?=$field['OtherAttributeArr']['DbTableName'] ?>
                    				<br />
                    				<?=explode(' ', $field['OtherAttributeArr']['DbFieldName'])[0] ?>
                				</div>
            				<?php endif; ?>
            			</td>
            			<td data-label="Validation">
                			<p>
                				<?=implode('<br />', (array)json_decode($field['Validation'], true)) ?>
            				</p>
            			</td>
            			<td data-label="Status">
            				<i class="eye large icon <?=($field['Status'])?'green':'slash grey' ?>"></i>
            			</td>
            			<td data-label="" style="white-space: nowrap;">
                			<button class="ui icon button editField blue" data-group-id="<?=$field['GroupID'] ?>" data-field-id="<?=$field['FieldID'] ?>">
                				<i class="edit icon"></i>
                			</button>
                			<form class="copyFieldForm" method="POST" action="update.php" style="display: inline-block;">
        						<input type="hidden" name="AcademicYearID" value="<?=$AcademicYearID ?>" />
                				<input type="hidden" name="Action" value="CopyField" />
                				<input type="hidden" name="GroupID" value="<?=$field['GroupID'] ?>" />
                				<input type="hidden" name="FieldID" value="<?=$field['FieldID'] ?>" />
                    			<button type="submit" class="ui icon button yellow">
                    				<i class="copy icon"></i>
                    			</button>
                			</form>
                			<form class="deleteFieldForm" method="POST" action="update.php" style="display: inline-block;">
        						<input type="hidden" name="AcademicYearID" value="<?=$AcademicYearID ?>" />
                				<input type="hidden" name="Action" value="DeleteField" />
                				<input type="hidden" name="GroupID" value="<?=$field['GroupID'] ?>" />
                				<input type="hidden" name="FieldID" value="<?=$field['FieldID'] ?>" />
                    			<button type="submit" class="ui icon button red">
                    				<i class="trash icon"></i>
                    			</button>
                			</form>
            			</td>
            		</tr>
        		<?php
        		endforeach;
        		?>
        	</tbody>
        </table>


    </div>
<?php
endforeach;
?>




<?php
foreach($groups as $group):
    $fields = $dafs->getFieldsByGroupId($group['GroupID']);
?>
    <div class="ui modal reorderModal" data-group-id="<?=$group['GroupID'] ?>" data-field-id="0">
    	<i class="close icon"></i>
    	<div class="header">
    		Reorder field in <?=intranet_htmlspecialchars($group['TitleB5']) ?>
    	</div>
    	<div class="content">
    		<form class="ui form fieldForm" method="POST" action="update.php">
    			<input type="hidden" name="AcademicYearID" value="<?=$AcademicYearID ?>" />
    			<input type="hidden" name="Action" value="ReorderField" />
    			<input type="hidden" name="GroupID" value="<?=$group['GroupID'] ?>" />

				<div class="ui segment">
                	<div class="ui relaxed divided list sortFieldContainer">
                    	<?php foreach($fields as $index=>$field): ?>
                    		<div class="item" style="cursor: grab;">
                    			<div class="content">
                    				<div class="header">
                        				<i class="bars grey icon"></i>
                                        [<?=$field['Type'] ?>]
                                        <?=$field['TitleB5'] ?>
                                    </div>
                    				<input type="hidden" name="FieldID[]" value="<?=$field['FieldID'] ?>" />
                    			</div>
                    		</div>
                		<?php endforeach; ?>
                	</div>
                </div>

    		</form>
    	</div>
    	<div class="actions">
    		<div class="ui black deny button">
    			Cancel
    		</div>
    		<div class="ui positive labeled icon button save">
    			Save
    			<i class="save icon"></i>
    		</div>
    	</div>
    </div>
<?php
endforeach;
?>

<?php
foreach($groups as $group):
?>
    <div class="ui modal fieldModal" data-group-id="<?=$group['GroupID'] ?>" data-field-id="0">
    	<i class="close icon"></i>
    	<div class="header">
    		Add Field to <?=intranet_htmlspecialchars($group['TitleB5']) ?>
    	</div>
    	<div class="content">
    		<form class="ui form fieldForm" method="POST" action="update.php">
    			<input type="hidden" name="AcademicYearID" value="<?=$AcademicYearID ?>" />
    			<input type="hidden" name="Action" value="UpdateField" />
    			<input type="hidden" name="GroupID" value="<?=$group['GroupID'] ?>" />
    			<input type="hidden" name="FieldID" value="0" />

            	<div class="ui styled accordion" style="width:100%;">

                	<div class="active title">
                		<i class="dropdown icon"></i>
                		Basic Information
                	</div>
                	<div class="active content">
                    	<div class="three fields">
                    		<div class="field">
                    			<label>TitleB5</label>
                    			<input type="text" name="TitleB5" value="">
                    		</div>
                    		<div class="field">
                    			<label>TitleEn</label>
                    			<input type="text" name="TitleEn" value="">
                    		</div>
                    		<div class="field">
                    			<label>TitleGb</label>
                    			<input type="text" name="TitleGb" value="">
                    		</div>
                    	</div>
                    	<div class="three fields">
                    		<div class="field">
                    			<label>LabelB5</label>
                    			<input type="text" name="LabelB5" value="">
                    		</div>
                    		<div class="field">
                    			<label>LabelEn</label>
                    			<input type="text" name="LabelEn" value="">
                    		</div>
                    		<div class="field">
                    			<label>LabelGb</label>
                    			<input type="text" name="LabelGb" value="">
                    		</div>
                    	</div>
                    	<div class="fields three">
                        	<div class="field">
                    			<label>Type</label>
                    			<?=getSelectByArray(
                    			    \AdmissionSystem\DynamicAdmissionFormSystem::listFieldTypes(),
                    			    ' name="Type" class="ui fluid dropdown"',
                    			    \AdmissionSystem\DynamicAdmissionFormSystem::FIELD_TYPE_TEXT,
                    			    0,1
                			    ) ?>
                        	</div>
                        	<div class="field">
                    			<label>Width</label>
                    			<?=getSelectByArray(
                    			    \AdmissionSystem\DynamicAdmissionFormSystem::listFieldWidths(),
                    			    ' name="Width" class="ui fluid dropdown"',
                    			    \AdmissionSystem\DynamicAdmissionFormSystem::FIELD_WIDTH_FULL,
                    			    0,1
                			    ) ?>
                        	</div>
                        	<div class="field">
                    			<label>Groupped</label>
                    			<?=getSelectByArray(
                    			    \AdmissionSystem\DynamicAdmissionFormSystem::listFieldGroupTypes(),
                    			    ' name="Groupped" class="ui fluid dropdown"',
                    			    \AdmissionSystem\DynamicAdmissionFormSystem::FIELD_GROUP_TYPE_SINGLE,
                    			    0,1
                			    ) ?>
                        	</div>
                    	</div>
                    	<div class="fields two">
                        	<div class="field">
                        		<label>Validation</label>
                        			<?=getSelectByArray(
                        			    \AdmissionSystem\DynamicAdmissionFormSystem::listFieldValidationTypes(),
                        			    ' name="Validation[]" multiple="multiple" class="ui fluid dropdown"',
                        			    '',
                        			    0,1
                    			    ) ?>
                        	</div>
                        	<div class="field">
                        		<label>Extra</label>
                        		<textarea name="Extra" rows="3"></textarea>
                        	</div>
                    	</div>
                    	<div class="field">
                    		<label>Remark</label>
                    		<textarea name="Remark" rows="3"></textarea>
                    	</div>
                    	<div class="inline field">
                    		<div class="ui toggle checkbox">
                    			<input type="checkbox" name="Status" value="1" class="hidden" checked>
                    			<label>Active</label>
                    		</div>
                    	</div>
                	</div>

                	<div class="title">
                		<i class="dropdown icon"></i>
                		DB Settings
                	</div>
                	<div class="content">
                    	<div class="fields two">
                        	<div class="field">
                        		<label>Table Name</label>
                        		<?=getSelectByValue(
                        		    $dbTables,
                        		    ' name="DbTableName" class="ui search selection"'
                    		    ); ?>
                        	</div>
                        	<div class="field">
                        		<label>Field</label>
                        		<select name="DbFieldName" class="ui search selection">
                            		<?php
                            		foreach($dbTableFields as $field => $tableArr):
                                        $tableStr = implode(',', $tableArr);
                            		?>
                            			<option data-tables="<?=$tableStr ?>"><?=$field ?></option>
                            		<?php
                            		endforeach;
                            		?>
                        		</select>
                        	</div>
                    	</div>
                	</div>
            	</div>

    		</form>
    	</div>
    	<div class="actions">
    		<div class="ui black deny button">
    			Cancel
    		</div>
    		<div class="ui positive labeled icon button save">
    			Save
    			<i class="save icon"></i>
    		</div>
    	</div>
    </div>
<?php
    $fields = $dafs->getFieldsByGroupId($group['GroupID']);
    foreach($fields as $index=>$field):
?>
    	<div class="ui modal fieldModal" data-group-id="<?=$field['GroupID'] ?>" data-field-id="<?=$field['FieldID'] ?>">
        	<i class="close icon"></i>
        	<div class="header">
        		Edit Field (FieldID: <?=$field['FieldID'] ?>)
        	</div>
        	<div class="content">
    			<form class="ui form fieldForm" method="POST" action="update.php">
    				<input type="hidden" name="AcademicYearID" value="<?=$AcademicYearID ?>" />
    				<input type="hidden" name="Action" value="UpdateField" />
    				<input type="hidden" name="GroupID" value="<?=$field['GroupID'] ?>" />
    				<input type="hidden" name="FieldID" value="<?=$field['FieldID'] ?>" />

                	<div class="ui styled accordion" style="width:100%;">

                    	<div class="active title">
                    		<i class="dropdown icon"></i>
                    		Basic Information
                    	</div>
                    	<div class="active content">
                        	<div class="three fields">
                        		<div class="field">
                        			<label>TitleB5</label>
                        			<input type="text" name="TitleB5" value="<?=intranet_htmlspecialchars($field['TitleB5']) ?>">
                        		</div>
                        		<div class="field">
                        			<label>TitleEn</label>
                        			<input type="text" name="TitleEn" value="<?=intranet_htmlspecialchars($field['TitleEn']) ?>">
                        		</div>
                        		<div class="field">
                        			<label>TitleGb</label>
                        			<input type="text" name="TitleGb" value="<?=intranet_htmlspecialchars($field['TitleGb']) ?>">
                        		</div>
                        	</div>
                        	<div class="three fields">
                        		<div class="field">
                        			<label>LabelB5</label>
                        			<input type="text" name="LabelB5" value="<?=intranet_htmlspecialchars($field['LabelB5']) ?>">
                        		</div>
                        		<div class="field">
                        			<label>LabelEn</label>
                        			<input type="text" name="LabelEn" value="<?=intranet_htmlspecialchars($field['LabelEn']) ?>">
                        		</div>
                        		<div class="field">
                        			<label>LabelGb</label>
                        			<input type="text" name="LabelGb" value="<?=intranet_htmlspecialchars($field['LabelGb']) ?>">
                        		</div>
                        	</div>
                        	<div class="fields three">
                            	<div class="field">
                        			<label>Type</label>
                        			<?=getSelectByArray(
                        			    \AdmissionSystem\DynamicAdmissionFormSystem::listFieldTypes(),
                        			    ' name="Type" class="ui fluid dropdown"',
                        			    $field['Type'],
                        			    0,1
                    			    ) ?>
                            	</div>
                            	<div class="field">
                        			<label>Width</label>
                        			<?=getSelectByArray(
                        			    \AdmissionSystem\DynamicAdmissionFormSystem::listFieldWidths(),
                        			    ' name="Width" class="ui fluid dropdown"',
                        			    $field['OtherAttributeArr']['Width'],
                        			    0,1
                    			    ) ?>
                            	</div>
                            	<div class="field">
                        			<label>Groupped</label>
                        			<?=getSelectByArray(
                        			    \AdmissionSystem\DynamicAdmissionFormSystem::listFieldGroupTypes(),
                        			    ' name="Groupped" class="ui fluid dropdown"',
                        			    $field['OtherAttributeArr']['Groupped'],
                        			    0,1
                    			    ) ?>
                            	</div>
                        	</div>
                        	<div class="fields two">
                            	<div class="field">
                            		<label>Validation</label>
                        			<?=getSelectByArray(
                        			    \AdmissionSystem\DynamicAdmissionFormSystem::listFieldValidationTypes(),
                        			    ' name="Validation[]" multiple="multiple" class="ui fluid dropdown"',
                        			    json_decode($field['Validation'], true),
                        			    0,1
                    			    ) ?>
                            	</div>
                            	<div class="field">
                            		<label>Extra</label>
                            		<textarea name="Extra" rows="3"><?=(stripslashes($field['OtherAttributeArr']['Extra'])) ?></textarea>
                            	</div>
                        	</div>
                        	<div class="field">
                        		<label>Remark</label>
                        		<textarea name="Remark" rows="3"><?=intranet_htmlspecialchars($field['Remark']) ?></textarea>
                        	</div>
                        	<div class="fields three">
                        		<div class="inline field">
                        			<label>Sequence</label>
                        			<input type="text" name="Sequence" value="<?=intranet_htmlspecialchars($field['Sequence']) ?>">
                        		</div>
                        	</div>
                        	<div class="inline field">
                        		<div class="ui toggle checkbox">
                        			<input type="checkbox" name="Status" class="hidden" value="1" <?=($field['Status'])?'checked':'' ?>>
                        			<label>Active</label>
                        		</div>
                        	</div>
                    	</div>

                    	<div class="title">
                    		<i class="dropdown icon"></i>
                    		DB Settings
                    	</div>
                    	<div class="content">
                        	<div class="fields two">
                            	<div class="field">
                            		<label>Table Name</label>
                            		<?=getSelectByValue(
                            		    $dbTables,
                            		    ' name="DbTableName" class="ui search selection "',
                            		    $field['OtherAttributeArr']['DbTableName']
                        		    ); ?>
                            	</div>
                            	<div class="field">
                            		<label>Field</label>
                            		<select name="DbFieldName" class="ui search selection ">
                                		<?php
                                		foreach($dbTableFields as $tbField => $tableArr):
                                            $tableStr = implode(',', $tableArr);
                                            $selected = ($field['OtherAttributeArr']['DbFieldName'] == $tbField)?'selected':'';
                                		?>
                                			<option data-tables="<?=$tableStr ?>" <?=$selected ?>><?=$tbField ?></option>
                                		<?php
                                		endforeach;
                                		?>
                            		</select>
                            	</div>
                        	</div>
                    	</div>
                	</div>
    			</form>
        	</div>
        	<div class="actions">
        		<div class="ui black deny button">
        			Cancel
        		</div>
        		<div class="ui positive labeled icon button save">
        			Save
        			<i class="save icon"></i>
        		</div>
        	</div>
        </div>
<?php
    endforeach;
endforeach;


