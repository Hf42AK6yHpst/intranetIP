<?php
# modifying by: 
 
/********************
 * Log :
 * Date		2019-07-11 [Henry]
 *  		Ignore time checking for getDecryptedText
 * 
 ********************/
 
$PATH_WRT_ROOT = "../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/lib.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_ui.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_utility.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_apps.php");

include_once("./config.php");

//for the customization
include_once($PATH_WRT_ROOT."includes/admission/".$setting_path_ip_rel."/config.php");
$intranet_session_language = $admission_cfg['DefaultLang'];
include_once($PATH_WRT_ROOT."includes/admission/".$setting_path_ip_rel."/libadmission_cust.php");
include_once($PATH_WRT_ROOT."includes/admission/".$setting_path_ip_rel."/libadmission_ui_cust.php");

include_once($PATH_WRT_ROOT."lang/lang.".$intranet_session_language.".php");
include_once($PATH_WRT_ROOT."lang/kis/lang_common_".$intranet_session_language.".php");
include_once($PATH_WRT_ROOT."lang/kis/apps/lang_admission_".$intranet_session_language.".php");
include_once($PATH_WRT_ROOT."lang/admission_lang.".$intranet_session_language.".php");
intranet_opendb();

$libkis 	= new kis('');

$_SESSION['SSV_PRIVILEGE']['school']['name'] = GET_SCHOOL_NAME();
$school = $libkis->getUserSchool(); 

$main_content = '<div class="admission_board">
                         <p class="spacer"></p>
                           <div class="admission_complete_msg" style="margin-top:30px">';

if(!isset($_REQUEST['q']) || $_REQUEST['q']== '')
{
	echo "Unauthorized access.";
	intranet_closedb();
	exit;
}

$decrypted_data = getDecryptedText($_REQUEST['q'], 'bXwKs7S93J2', 1000000);
$dataAry = explode("=",$decrypted_data);
if($dataAry[0] != 'receiverid' || $dataAry[1]==''){
	echo "Invalid operation.";
	intranet_closedb();
	exit;
}

$li = new libdb();

$sql = "UPDATE ADMISSION_EMAIL_RECEIVER SET AcknowledgeDate=NOW() WHERE ReceiverID='".$dataAry[1]."' AND AcknowledgeDate IS NULL";
$success = $li->db_db_query($sql);

if($success){
	$main_content .=  "<h1>You have successfully acknowledged receipt of the school's email. Thank you.</h1><h1>你已成功確認收到校方的郵件，謝謝。</h1>";
	$main_content .= '</div>
                      </div>
                      <p class="spacer"></p>';
	intranet_closedb();
	include_once("./admission_form/common_tmpl.php");
	exit;
}else{
	$main_content .=  "<h1>Acknowledgement failed.</h1><h1>你未能成功確認收到校方的郵件，謝謝。</h1>";
	intranet_closedb();
	$main_content .= '</div>
                      </div>
                      <p class="spacer"></p>';
	include_once("./admission_form/common_tmpl.php");
	exit;
}

intranet_closedb();
include_once("./admission_form/common_tmpl.php");
?>