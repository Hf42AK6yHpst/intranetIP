<?php
# modifying by: Pun
 
/********************
 * Log :
 * Date		2015-08-04 [Pun]
 * 			File Created
 * 
 ********************/

############ Header START ############
$PATH_WRT_ROOT = "../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

include_once($PATH_WRT_ROOT."includes/admission/libadmission_briefing_base.php");
############ Header END ############

intranet_opendb();

############ Init START ############
$abb		= new admission_briefing_base();
$BriefingID = $_REQUEST['BriefingID'];
############ Init END ############


############ Get Data START ############ 
$quota = $abb->getBriefingSessionRemainsQuota($BriefingID);
############ Get Data END ############ 


############ UI START ############ 
echo $quota;
?>