<?php
# modifying by: Pun
 
/********************
 * Log :
 * Date		2015-08-04 [Pun]
 * 			File Created
 * 
 ********************/


$allSession = $abb->getActiveBriefingSession('');
$allRemainsQuota = $abb->getAllBriefingSessionRemainsQuota('');

if(($sys_custom['KIS_Admission']['MINGWAIPE']['Settings'] || $sys_custom['KIS_Admission']['MINGWAI']['Settings'])){
	if(sizeof($allSession)>0){
		foreach($allSession as $session){
			$BriefingID = $session['BriefingID'];
			$isFull = $allRemainsQuota[$BriefingID] <= 1;
			if(!$isFull){
				?>
				<script>
					$.get('ajaxInstructionPage.php', {
						'BriefingID': <?=$BriefingID?>
					}, function(res){
						$('#step_index').hide();
						$('#step_instruction').html(res).show();
					});
				</script>
				<?break;
			}
		}
		if($isFull){
		echo '<div class="admission_board">
				<p class="spacer"></p>
				<fieldset class="warning_box">
					<legend>'.$Lang['Admission']['warning'].' Warning</legend>
					<ul>
						<li>新生簡介會已滿額。<br/>The quota of orientation day is full.</li>
					</ul>
				</fieldset>
			</div>';
		}
	}
	else if(sizeof($allSession)<=0){
		echo '<div class="admission_board">
				<p class="spacer"></p>
				<fieldset class="warning_box">
					<legend>'.$Lang['Admission']['warning'].' Warning</legend>
					<ul>
						<li>報名尚未開始。<br/>Application is not yet started.</li>
					</ul>
				</fieldset>
			</div>';				
	}
}
else{
?>
<div class="notice_paper">
	<div class="notice_paper_top">
		<div class="notice_paper_top_right">
			<div class="notice_paper_top_bg">
				<h1 class="notice_title"><?=$Lang['Admission']['onlineApplication']?> Online Application</h1>
			</div>
		</div>
	</div>
	<div class="notice_paper_content">
		<div class="notice_paper_content_right">
			<div class="notice_paper_content_bg">
				<div class="notice_content ">
					<div class="admission_content">
<!-- --------- Content START --------- -->
						<fieldset>
							<legend>簡介會 Briefing Session(s)</legend>
							<div class="admission_select_option">
								<?php
								if(sizeof($allSession)<=0){
									echo '<fieldset class="warning_box">
											<legend>'.$Lang['Admission']['warning'].' Warning</legend>
											<ul>
												<li>報名尚未開始。<br/>Application is not yet started.</li>
											</ul>
										</fieldset>';
								}
								else{ 
								foreach($allSession as $session){
									$_lastSessionDate = $date;
									
									$BriefingID = $session['BriefingID'];
									$title = $session['Title'];
									$date = substr($session['BriefingStartDate'], 0, 10);
									$startTime = substr($session['BriefingStartDate'], 11, 5);
									$endTime = substr($session['BriefingEndDate'], 11, 5);
									$enrolEndTime = substr($session['EnrolmentEndDate'], 0, 16);
									$isFull = $allRemainsQuota[$BriefingID] < 1;

									if($sys_custom['KIS_Admission']['HKUGAPS']['Settings']){
									    $isFull = false;
									}
									
									if($_lastSessionDate != $date){
										if($_lastSessionDate != ''){
											echo '<br />';
										}
										echo "<b>[{$date}]</b>";
									}
									?>
									<p>
										<?php 
										if($isFull){
											echo '<del>';
										}
										?>
										
										<input type="radio" name="BriefingID" value="<?=$BriefingID?>" id="briefing_<?=$BriefingID?>" class="selectBriefingID" <?=($isFull)?'disabled':''?>>
										<label for="briefing_<?=$BriefingID?>">
											<?=$title?> (<?=$startTime?>~<?=$endTime?>)
											&nbsp;
											<span><?=$Lang['Admission']['deadline']?> Enrollment ends: <?=$enrolEndTime?></span>
										</label>
										
										<?php 
										if($isFull){
											echo "</del>&nbsp;<font color=\"red\">{$Lang['Admission']['IsFull']} Full</font>";
										}
										?>
									</p>
								<?php
								}
								
								if($sys_custom['KIS_Admission']['MINGWAIPE']['Settings'] || $sys_custom['KIS_Admission']['MINGWAI']['Settings']){
									echo "</del>&nbsp;<font color=\"red\">{$Lang['Admission']['IsFull']} Full</font>";
								}
								
								}
								?>
							</div>
						</fieldset>		
						<?if(sizeof($allSession)>0){?>
						<div class="edit_bottom">
							<?= $li->GET_ACTION_BTN($Lang['Btn']['Next'].' Next', "button", "gotoInstruction()", "SubmitBtn", "", 0, "formbutton")?>
						</div>
						<?}?>
						<p class="spacer"></p>
						<br/>
						<span>
							建議使用 <a href="http://www.google.com/chrome/browser/" target="download_chrome">Google Chrome</a> 瀏覽器。
						</span>
						<br/>
						<span>
							Recommended to use <a href="http://www.google.com/chrome/browser/" target="download_chrome">Google Chrome</a> Browser.
						</span>
<!-- --------- Content END --------- -->
					</div>
					<p class="spacer"></p>
				</div>
			</div>
		</div>
	</div>
	<div class="notice_paper_bottom">
		<div class="notice_paper_bottom_right">
			<div class="notice_paper_bottom_bg"></div>
		</div>
	</div>
</div>

<script>
function gotoInstruction(){
	if($('.selectBriefingID:checked').length == 0){
		alert('<?=$Lang['Admission']['msg']['selectBriefing']?> Please select briefing session.');
		return false;
	}
	
	$.get('ajaxInstructionPage.php', {
		'BriefingID': $('.selectBriefingID:checked').val()
	}, function(res){
		$('#step_index').hide();
		$('#step_instruction').html(res).show();
	});
}
</script>
<?}?>