<?php
# modifying by: 
 
/********************
 * Log :
 * Date		2015-08-04 [Pun]
 * 			File Created
 * 
 ********************/

############ Header START ############
$PATH_WRT_ROOT = "../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_ui.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_utility.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_apps.php");

include_once("../config.php");

include_once($PATH_WRT_ROOT."includes/admission/".$setting_path_ip_rel."/config.php");
$intranet_session_language = $admission_cfg['DefaultLang'];
include_once($PATH_WRT_ROOT."includes/admission/".$setting_path_ip_rel."/libadmission_cust.php");
include_once($PATH_WRT_ROOT."includes/admission/libadmission_briefing_base.php");

include_once($PATH_WRT_ROOT."lang/lang.".$intranet_session_language.".php");
include_once($PATH_WRT_ROOT."lang/kis/lang_common_".$intranet_session_language.".php");
include_once($PATH_WRT_ROOT."lang/kis/apps/lang_admission_".$intranet_session_language.".php");
include_once($PATH_WRT_ROOT."lang/admission_lang.".$intranet_session_language.".php");
############ Header END ############

intranet_opendb();

############ Init START ############
$lac		= new admission_cust();
if(class_exists('admission_briefing')){
	$abb = new admission_briefing();
}else{
	$abb = new admission_briefing_base();
}

$session = $abb->getBriefingSession($BriefingID);
$schoolYearID = $session['SchoolYearID'];
############ Init END ############

$validForAdmission = true;
if ($sys_custom['KIS_Admission']['IntegratedCentralServer'] && ! $sys_custom['KIS_Admission']['MUNSANG']['Settings'] && $sys_custom['KIS_Admission']['EnableQueueForBriefing']) {
    $ch = curl_init();
    
    curl_setopt($ch, CURLOPT_HEADER, 0);
    
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    
    // set URL and other appropriate options
    
    // curl_setopt($ch, CURLOPT_URL, "http://192.168.0.146:31002/test/queue/reserved.php?token=f590bac6181c28fda4ec6812e7f2ae4f92&FromKIS=1");
    
    // # the host name should be dynamic
    if ($sys_custom['KIS_Admission']['MUNSANG']['Settings']) {
        $formCode = 'QUOTA_FOR_ALL';
    } else {
        $formCode = $_REQUEST['sus_status'];
    }
    
    $fromReceived = count($lac->getApplicationOthersInfo($lac->schoolYearID));
    
    curl_setopt($ch, CURLOPT_URL, $admission_cfg['IntegratedCentralServer'] . "reserved.php?token=" . $_REQUEST['token'] . "&FromKIS=1&FormCode=" . $formCode . "&FilledBy=" . $fromReceived);
    
    // curl_setopt($ch, CURLOPT_URL, $admission_cfg['IntegratedCentralServer']."reserved.php?token=".$_REQUEST['token']."&FromKIS=1");
    
    // grab URL and pass it to the browser
    
    $data_from_checking = curl_exec($ch);
    
    $data_collected = unserialize($data_from_checking);
    // debug_pr($data_collected);
    // $validForAdmission = true;
    // if($data_collected['InDate'] == date('Y-m-d') && $data_collected['timeFrom'] >= date('H:i') && $data_collected['timeTo'] <= date('H:i')){
    // $validForAdmission = true;
    // }
    // debug_r($validForAdmission);
    // debug_pr(date('H:i'));
    // if((!$data_collected['AllowApplyNow'] || $data_collected['ErrorCode'] != '') && ($_SESSION["platform"]!="KIS" || !$_SESSION["UserID"])){
    // //$validForAdmission = false
    // header("location: ".$PATH_WRT_ROOT."/kis/admission_form/access_deny.php?err=".$data_collected['ErrorCode']);
    // }
    $validForAdmission = true;
    
    // if(($_SESSION["platform"]!="KIS" || !$_SESSION["UserID"]) && ($_REQUEST['sus_status'] != $data_collected['ApplyFor'] || $_REQUEST['StudentBirthCertNo'] != $data_collected['IDNumber'] || !$_REQUEST['token'] || $lac->hasToken($_REQUEST['token']) > 0)){
    if (($_SESSION["platform"] != "KIS" || ! $_SESSION["UserID"] || ! $_SESSION["SSV_USER_ACCESS"]["eAdmin-eAdmission"] || $_GET['token']) && ((! $sys_custom['KIS_Admission']['MGF']['Settings'] && ! $sys_custom['KIS_Admission']['HKUGAPS']['Settings'] && ! $sys_custom['KIS_Admission']['KTLMSKG']['Settings'] && ! $sys_custom['KIS_Admission']['RMKG']['Settings'] && ! $sys_custom['KIS_Admission']['TSUENWANBCKG']['Settings'] && ! $sys_custom['KIS_Admission']['CHIUCHUNKG']['Settings'] && ! $sys_custom['KIS_Admission']['YLSYK']['Settings'] && ! $sys_custom['KIS_Admission']['SHCK']['Settings'] && ! $sys_custom['KIS_Admission']['SSGC']['Settings'] && ! $sys_custom['KIS_Admission']['AOGKG']['Settings'] && ! $sys_custom['KIS_Admission']['TSS']['Settings'] && $_REQUEST['StudentBirthCertNo'] != $data_collected['IDNumber']) || ! $_REQUEST['token'] || $lac->hasToken($_REQUEST['token']) > 0 || $data_collected['QuotaReached'] || ! $_REQUEST['sus_status']) || ($_REQUEST['StudentBirthCertNo'] && $lac->hasBirthCertNumber($_REQUEST['StudentBirthCertNo'], $_REQUEST['sus_status']))) {
        $validForAdmission = false;
        header("location: " . $PATH_WRT_ROOT . "/kis/admission_briefing/index.php?token={$_REQUEST['token']}&err=1");
        exit();
    }
}

############ Check Enrolment Time START ############ 
$isActive = $abb->isBriefingSessionActive($BriefingID);
if(!$isActive){
	$id = urlencode(getEncryptedText("Briefing_ApplicantID=0&SchoolYearID={$schoolYearID}&Error=TimeEnd",$admission_cfg['FilePathKey']));
	
	#### Send Email START ####
	$result = $abb->sendMailToNewApplicant(0,'','',$Email);
	#### Send Email END ####
	
	header("Location: finish.php?id=".$id);
	exit;
}
############ Check Enrolment Time END ############ 


############ Check Seat Quota START ############ 
$quota = $abb->getBriefingSessionRemainsQuota($BriefingID);
if($sys_custom['KIS_Admission']['HKUGAPS']['Settings']){
    $quota = PHP_INT_MAX;
}
if($quota < $SeatRequest){
	$id = urlencode(getEncryptedText("Briefing_ApplicantID=0&SchoolYearID={$schoolYearID}&Error=QuotaFull",$admission_cfg['FilePathKey']));
	
	#### Send Email START ####
	$result = $abb->sendMailToNewApplicant(0,'','',$Email);
	#### Send Email END ####
	
	header("Location: finish.php?id=".$id);
	exit;
}
############ Check Seat Quota END ############ 


############ Insert Data START ############
$Briefing_ApplicantID = $abb->insertBriefingApplicant($schoolYearID, array(
	'BriefingID' => $BriefingID,
	'Email' => $Email,
	'BirthCertNo' => $BirthCertNo,
	'StudentName' => $StudentName,
	'ParentName' => $ParentName,
	'StudentGender' => $StudentGender,
	'Kindergarten' => $Kindergarten,
	'PhoneNo' => $PhoneNo,
	'SeatRequest' => $SeatRequest,
));


if($plugin['eAdmission_devMode'] && $_SERVER['HTTP_HOST'] == '192.168.0.171:31002'){
    if($Briefing_ApplicantID == 0){
    	$error = '&Error=InsertError';
    }
}else{
    if($Briefing_ApplicantID == 0){
    	$error = '&Error=InsertError';
    	$result = $abb->sendMailToNewApplicant(0,'','',$Email);
    }else{
    	$result = $abb->sendMailToNewApplicant($Briefing_ApplicantID);
    }
}
############ Insert Data END ############

// add call back to the central server [start]
// # preview form function
if ($sys_custom['KIS_Admission']['IntegratedCentralServer'] && $sys_custom['KIS_Admission']['EnableQueueForBriefing']) {
    $ch = curl_init();
    
    curl_setopt($ch, CURLOPT_HEADER, 0);
    
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    
    curl_setopt($ch, CURLOPT_TIMEOUT, 60);
    // set URL and other appropriate options
    
    // curl_setopt($ch, CURLOPT_URL, "http://192.168.0.146:31002/test/queue/reserved.php?token=f590bac6181c28fda4ec6812e7f2ae4f92&FromKIS=1");
    // # the host name should be dynamic
    if ($sys_custom['KIS_Admission']['MUNSANG']['Settings']) {
        $formCode = 'QUOTA_FOR_ALL';
    } else {
        $formCode = $_REQUEST['sus_status'];
    }
    
    $fromReceived = count($lac->getApplicationOthersInfo($lac->schoolYearID));
    
    curl_setopt($ch, CURLOPT_URL, $admission_cfg['IntegratedCentralServer'] . "reserved.php?token=" . $_REQUEST['token'] . "&FromKIS=1&FormCode=" . $formCode . "&FilledBy=" . $fromReceived);
    
    // grab URL and pass it to the browser
    
    $data_from_checking = curl_exec($ch);
}
// add call back to the central server [end]

############ Redirect START ############ 
$id = urlencode(getEncryptedText("Briefing_ApplicantID={$Briefing_ApplicantID}&SchoolYearID={$schoolYearID}{$error}",$admission_cfg['FilePathKey']));
header("Location: finish.php?id=".$id);
exit;