<?php
# modifying by: 
 
/********************
 * Log :
 * Date		2015-08-04 [Pun]
 * 			File Created
 * 
 ********************/
 
$PATH_WRT_ROOT = "../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_ui.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_utility.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_apps.php");
//include_once($PATH_WRT_ROOT."includes/json.php");

include_once("../config.php");

//for the customization
include_once($PATH_WRT_ROOT."includes/admission/".$setting_path_ip_rel."/config.php");
$intranet_session_language = $admission_cfg['DefaultLang'];
include_once($PATH_WRT_ROOT."includes/admission/".$setting_path_ip_rel."/libadmission_cust.php");
include_once($PATH_WRT_ROOT."includes/admission/libadmission_briefing_base.php");

include_once($PATH_WRT_ROOT."lang/lang.".$intranet_session_language.".php");
include_once($PATH_WRT_ROOT."lang/kis/lang_common_".$intranet_session_language.".php");
include_once($PATH_WRT_ROOT."lang/kis/apps/lang_admission_".$intranet_session_language.".php");
include_once($PATH_WRT_ROOT."lang/admission_lang.".$intranet_session_language.".php");


intranet_opendb();

######## Access Right END ######## 


######## Init START ######## 
$libkis 	= new kis('');
$lac		= new admission_cust();

$moduleSettings = $lac->getBasicSettings(99999, array (
						'enablebriefingsession'
					));

######## Access Right START ######## 
if (!$plugin['eAdmission'] || !$sys_custom['KIS_Admission']['BriefingModule'] && !$moduleSettings['enablebriefingsession'])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT("","../");
	exit;
}

if(class_exists('admission_briefing')){
	$abb = new admission_briefing();
}else{
	$abb = new admission_briefing_base();
}
$li			= new interface_html();

$_SESSION['SSV_PRIVILEGE']['school']['name'] = GET_SCHOOL_NAME();
$school = $libkis->getUserSchool(); 
$schoolYearID = $lac->getNextSchoolYearID();
######## Init END ######## 

$isValidForAdmission = true;
if (($_SESSION["platform"] != "KIS" || ! $_SESSION["UserID"] || ! $_SESSION["SSV_USER_ACCESS"]["eAdmin-eAdmission"] || $_GET['token']) && $sys_custom['KIS_Admission']['IntegratedCentralServer'] && $sys_custom['KIS_Admission']['EnableQueueForBriefing']) {
    $ch = curl_init();
    
    curl_setopt($ch, CURLOPT_HEADER, 0);
    
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    
    // set URL and other appropriate options
    
    // curl_setopt($ch, CURLOPT_URL, "http://192.168.0.146:31002/test/queue/reserved.php?token=f590bac6181c28fda4ec6812e7f2ae4f92&FromKIS=1");
    // # the host name should be dynamic
    if ($sys_custom['KIS_Admission']['MUNSANG']['Settings']) {
        $formCode = 'QUOTA_FOR_ALL';
    } else {
        $formCode = $_REQUEST['sus_status'];
    }
    
    if ($sys_custom['KIS_Admission']['MUNSANG']['Settings']) {
        $fromReceived = count($lac->getApplicationOthersInfo($lac->schoolYearID));
        curl_setopt($ch, CURLOPT_URL, $admission_cfg['IntegratedCentralServer'] . "reserved.php?token=" . $_GET['token'] . "&FromKIS=1&FormCode=" . $formCode . "&FilledBy=" . $fromReceived);
    } else {
        curl_setopt($ch, CURLOPT_URL, $admission_cfg['IntegratedCentralServer'] . "reserved.php?token=" . $_GET['token'] . "&FromKIS=1");
    }
    
    // grab URL and pass it to the browser
    
    $data_from_checking = curl_exec($ch);
    
    $data_collected = unserialize($data_from_checking);
    // debug_pr($data_collected);
    
    // if($data_collected['InDate'] == date('Y-m-d') && $data_collected['timeFrom'] >= date('H:i') && $data_collected['timeTo'] <= date('H:i')){
    // $isValidForAdmission = true;
    // }
    // debug_r($isValidForAdmission);
    // debug_pr(date('H:i'));
    
    if ((! $data_collected['AllowApplyNow'] || $data_collected['ErrorCode'] != '' || $lac->hasToken($_GET['token']) > 0) && ($_SESSION["platform"] != "KIS" || ! $_SESSION["UserID"] || ! $_SESSION["SSV_USER_ACCESS"]["eAdmin-eAdmission"] || $_GET['token'])) {
        $isValidForAdmission = false;
        // header("location: ".$PATH_WRT_ROOT."/kis/admission_form/access_deny.php?err=".$data_collected['ErrorCode']);
        if ($_GET['token'] && $lac->hasToken($_GET['token']) > 0) {
            $data_collected['ErrorMsg'] = "閣下已遞交網上申請表，申請通知電郵已發送，請檢查閣下在申請表填寫的電郵。";
            $data_collected['ErrorMsg'] .= "<br/>You have applied the admission! Please check your Email to get the admission Notification!";
            if ($sys_custom['KIS_Admission']['HKUGAPS']['Settings']) {
                $data_collected['ErrorMsg'] = '因付款程序尚未完成，是次申請經已失效，<a href="http://eadmission.eclasscloud.hk/?af=hkugaps">請按此返回首頁重新報名。</a>';
                $data_collected['ErrorMsg'] .= '<br/>Due to payment failure, the application process has expired. <a href="http://eadmission.eclasscloud.hk/?af=hkugaps">Please return to home page and retry.</a>';
            }
            // Henry Added Start[20150120]
            if ($sys_custom['KIS_Admission']['CSM']['Settings']) {
                $otherInfoResult = $lac->getOtherInfoByToken($_GET['token']);
                $id = urlencode(getEncryptedText("ApplicationID=" . $otherInfoResult['ApplicationID'] . "&sus_status=" . $otherInfoResult['ApplyYear'] . "&SchoolYearID=" . $otherInfoResult['ApplyLevel'], $admission_cfg['FilePathKey']));
                header("Location: finish.php?id=" . $id);
                exit();
            }
            // Henry Added End[20150120]
        } else 
            if ($data_collected['QuotaReached']) {
                if ($sys_custom['KIS_Admission']['MUNSANG']['Settings']) {
                    header("Location: " . $admission_cfg['IntegratedCentralServer'] . '?af=' . $_SERVER['HTTP_HOST']);
                    exit();
                    // $data_collected['ErrorMsg'] = "2015-2016年度幼兒班申請已完結，多謝報讀民生書院幼稚園。<br/>";
                    // $data_collected['ErrorMsg'] .= "The application period for nursery class of 2015－2016 is now closed. Thank you for applying Munsang College Kindergarten.";
                }
            }
        $main_content = '<div class="admission_board">';
        $main_content .= '<div class="admission_complete_msg"><h1>' . $data_collected['ErrorMsg'] . '</h1></div>';
        $main_content .= '</div>';
    }
} else {
    $data_collected['ApplyFor'] = '';
    $data_collected['IDNumber'] = '';
}


######## UI START ########
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	
	<title>:: eClass KIS ::</title>
	
	<link href="/templates/kis/css/common.css" rel="stylesheet" type="text/css" />
	<link href='http://fonts.googleapis.com/css?family=Bubblegum+Sans' rel='stylesheet' type='text/css'>
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js" type="text/javascript"></script>
	<script type="text/javascript" src="/templates/jquery/jquery-1.8.0.min.js"></script>
	<script type="text/javascript" src="/templates/jquery/jquery.fancybox.js?v=2.1.0"></script>
	<link rel="stylesheet" type="text/css" href="/templates/jquery/jquery.fancybox.css?v=2.1.0" media="screen" />
	<script language="JavaScript" src="/templates/script.js"></script>
	
	<script type="text/javascript">
		function MM_goToURL() { //v3.0
			var i, args=MM_goToURL.arguments; document.MM_returnValue = false;
			for (i=0; i<(args.length-1); i+=2) eval(args[i]+".location='"+args[i+1]+"'");
		}
		<?=$javascript?>
	</script>
	
</head>
<body class="parent">
	<div id="container">
        <div class="top_header">
            <a href="./" class="logo" title="eClass KIS" style="background-image: url('<?=$school['logo']?>')"></a>
            <div class="school_name"><?=GET_SCHOOL_NAME()?></div>
        </div>
        <div class="board" id="module_wood_page" style="width:100%">
        	<div class="board_top"><div class="board_top_right"><div class="board_top_bg"><div class="board_top_content">
        	</div></div></div></div>
			<div class="board_main" style="padding-left: 30px;"><div class="board_main_right" style="padding-right: 30px;"><div class="board_main_bg"><div class="board_main_content">
				<div class="main_content">
<!-------------- Main Content START -------------->

					<div id='step_index' style='display:auto'>
						<?php 
						if ($isValidForAdmission || ! $sys_custom['KIS_Admission']['EnableQueueForBriefing']) {
							include('firstPage.tmpl.php');
						}
						else{
							echo $main_content;
						}
						?>
					</div>

					<div id='step_instruction' style='display:none'>
					</div>

					<div id='step_input_form' style='display:none'>
					</div>

<!-------------- Main Content END -------------->                    
				</div>
			</div></div></div></div>
			<div class="board_bottom"><div class="board_bottom_right"><div class="board_bottom_bg"></div></div></div>
		</div>
        <div class="footer"><a href="http://eclass.com.hk" title="eClass"></a><span>Powered by</span></div>
	</div>
</body>
</html>