<?php
# modifying by: Pun
 
/********************
 * Log :
 * Date		2015-08-04 [Pun]
 * 			File Created
 * 
 ********************/

############ Header START ############
$PATH_WRT_ROOT = "../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_ui.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_utility.php");
include_once($PATH_WRT_ROOT."includes/kis/libkis_apps.php");

include_once("../config.php");

include_once($PATH_WRT_ROOT."includes/admission/".$setting_path_ip_rel."/config.php");
$intranet_session_language = $admission_cfg['DefaultLang'];
include_once($PATH_WRT_ROOT."includes/admission/".$setting_path_ip_rel."/libadmission_cust.php");
include_once($PATH_WRT_ROOT."includes/admission/libadmission_briefing_base.php");

include_once($PATH_WRT_ROOT."lang/lang.".$intranet_session_language.".php");
include_once($PATH_WRT_ROOT."lang/kis/lang_common_".$intranet_session_language.".php");
include_once($PATH_WRT_ROOT."lang/kis/apps/lang_admission_".$intranet_session_language.".php");
include_once($PATH_WRT_ROOT."lang/admission_lang.".$intranet_session_language.".php");
############ Header END ############

intranet_opendb();

############ Init START ############
$abb		= new admission_briefing_base();
$li			= new interface_html();

$BriefingID = $_REQUEST['BriefingID'];
############ Init END ############


############ Get Data START ############ 
$session = $abb->getBriefingSession($BriefingID);
############ Get Data END ############ 


############ UI START ############ 
?>
<style>
.admission_board{
	min-height: 0;
}
</style>

<div class="admission_board">
	<p class="spacer"></p>
	<div>
		<?=$session['Instruction']?>
	</div>
	
	<div class="edit_bottom">
	<?if(!($sys_custom['KIS_Admission']['MINGWAIPE']['Settings'] || $sys_custom['KIS_Admission']['MINGWAI']['Settings'])){?>
		<input type="button" class="formsubbutton" onclick="MM_goToURL('parent','index.php');return document.MM_returnValue" value="取消 Cancel">
	<?}?>
		<input type="button" name="SubmitBtn" id="SubmitBtn" onclick="gotoForm()" class="formbutton" value="開始填寫表格 Begin Application">
	</div>
	<p class="spacer"></p>
</div>

<script>
function gotoForm(){
	$.get('ajaxFormPage.php', {
		'BriefingID': '<?=$BriefingID?>'
	}, function(res){
		$('#step_index').hide();
		$('#step_instruction').hide();
		$('#step_input_form').html(res).show();
	});
}
</script>