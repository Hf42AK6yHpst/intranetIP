
<div class="admission_board" style="min-height: 150px">
	<h1>
    	港大同學會小學 簡介會申請結果<br/>
    	HKUGA Primary School Briefing Session Application Result
	</h1>
	<table class="form_table" style="font-size: 13px">
		<tr>
			<td class="field_title"><font style="color:red;">*</font>輸入申請編號 Application Number</td>
			<td><input style="width:500px" name="ApplicationNo" type="text" id="ApplicationNo" class="textboxtext"/></td>
		</tr>
		<tr>
			<td class="field_title"><font style="color:red;">*</font>聯絡電郵 Contact Email</td>
			<td><input style="width:500px" name="ContactEmail" type="text" id="ContactEmail" class="textboxtext"/></td>
		</tr>
	</table>
    <div class="edit_bottom">
    	<input type="submit" name="SubmitBtn" id="SubmitBtn" class="formbutton" value="呈送 Submit" />
    </div>
</div>
<p class="spacer"></p>


<script>
window.checkForm = function(){
	var re = /\S+@\S+\.\S+/;
	
	if(briefingForm.ApplicationNo.value==''){
		alert("請輸入申請編號。\nPlease enter Application Number.");	
		briefingForm.ApplicationNo.focus();
		return false;
	}
	if(briefingForm.ContactEmail.value==''){
		alert("請輸入聯絡電郵。\nPlease enter Contact Email.");	
		briefingForm.ContactEmail.focus();
		return false;
	}else if($.trim(briefingForm.ContactEmail.value)!='' && !re.test(briefingForm.ContactEmail.value)){
		alert("電郵地址格式不符\nInvalid E-mail Format");
		briefingForm.ContactEmail.focus();
		return false;
	}
	return true;
}
</script>