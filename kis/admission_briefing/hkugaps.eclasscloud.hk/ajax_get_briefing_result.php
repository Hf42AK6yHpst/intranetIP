<?php

############ Get briefing applicant START ############
$briefingApplicant = $abb->getBriefingApplicantByApplicantID($ApplicationNo);
if(trim($ContactEmail) != trim($briefingApplicant['Email'])){
    ?>
	<script>alert('申請編號或聯絡電郵錯誤\nIncorrect Application Number or Contact Email.');</script>
<?php
    exit;
}
############ Get briefing applicant END ############ 

############ Get briefing session START ############ 
$briefingSession = $abb->getBriefingSession($briefingApplicant['BriefingID']);
############ Get briefing session END ############ 

$id = urlencode(getEncryptedText("Briefing_ApplicantID={$briefingApplicant['Briefing_ApplicantID']}", $admission_cfg['FilePathKey']));


$resultIsOpen = $lac->getBasicSettings($kis_data['schoolYearID'], array (
    'briefingAnnouncement',
));
$resultIsOpen = $resultIsOpen['briefingAnnouncement'];
?>


<!-------------- Main Content START -------------->
<form id="form1" action="print_briefing_result.php" method="POST" target="_blank">
    <input type="hidden" name="id" value="<?=$id?>" />
    <div class="admission_board">
    	<br />
    	
    	<div>
    		<h1>
    			<?=$Lang['Admission']['briefingApplicantInfo']?> Application Information
    		</h1>
    		<table class="form_table" style="font-size: 13px">
    		<colgroup>
    			<col style="width: 25%" />
    			<col style="width: 25%" />
    			<col style="width: 25%" />
    			<col style="width: 25%" />
    		</colgroup>
    		
    		
    		<tr>
    			<td class="field_title">
    				<?=$LangB5['Admission']['briefing']?>
    				<?=$LangEn['Admission']['briefing']?>
    			</td>
    			<td>
    				<?=$briefingSession['Title'] ?>
    			</td>
    			
    			<td class="field_title">
    				<?=$LangB5['Admission']['requestSeat']?>
    				<?=$LangEn['Admission']['requestSeat']?>
    			</td>
    			<td>
    				<?=$briefingApplicant['SeatRequest']?>
    			</td>
    		</tr>
    		
    		<tr>
    			<td class="field_title">
    				<?=$LangB5['Admission']['StartTime']?>
    				<?=$LangEn['Admission']['StartTime']?>
    			</td>
    			<td>
    				<?=substr($briefingSession['BriefingStartDate'], 11,5) ?>
    			</td>
    			
    			<td class="field_title">
    				<?=$LangB5['Admission']['EndTime']?>
    				<?=$LangEn['Admission']['EndTime']?>
    			</td>
    			<td>
    				<?=substr($briefingSession['BriefingEndDate'], 11,5) ?>
    			</td>
    		</tr>
    		
    		<tr>
    			<td>&nbsp;</td>
    		</tr>
    		
    		<tr>
    			<td class="field_title">
    				<?=$LangB5['Admission']['applicationno']?>
    				<?=$LangEn['Admission']['applicationno']?>
    			</td>
    			<td colspan="3">
    				<?=$briefingApplicant['ApplicantID']?>
    			</td>
    		</tr>
    		
    		<tr>
    			<td class="field_title">
    				<?=$LangB5['Admission']['student'].$LangB5['Admission']['name']?>
    				<?=$LangEn['Admission']['student'].$LangEn['Admission']['name']?>
    			</td>
    			<td>
    				<?=$briefingApplicant['StudentName']?>
    			</td>
    			<td class="field_title">
    				<?=$LangB5['Admission']['parent'].$LangB5['Admission']['name']?>
    				<?=$LangEn['Admission']['parent'].$LangEn['Admission']['name']?>
    			</td>
    			<td>
    				<?=$briefingApplicant['ParentName']?>
    			</td>
    		</tr>
    			
    		<tr>
    			<td class="field_title">
    				<?=$LangB5['Admission']['contactEmail']?>
    				<?=$LangEn['Admission']['contactEmail']?>
    			</td>
    			<td>
    				<?=$briefingApplicant['Email']?>
    			</td>
    
    			<td class="field_title">
    				<?=$LangB5['Admission']['phoneno']?>
					<?=$LangEn['Admission']['phoneno']?>
    			</td>
    			<td>
    				<?=$briefingApplicant['PhoneNo']?>
    			</td>
    		</tr>
    		
    		</table>
    	</div>
    	
    		<?php
    		if($resultIsOpen){
        		if($briefingApplicant['Status'] == $admission_cfg['BriefingStatus']['confirmed']){
        	?>
        	<?php
        		}else{
        	?>
        		<div style="width: 600px; margin: 35px auto 0 auto;">
            		<fieldset>
            			<legend>抽籤結果 Ballot Result</legend>
                		<p>由於名額已滿，抱歉未能安排座位。歡迎家長於以下的時段參觀課程展覽。謝謝！</p>
                		<p>We are sorry that admission of seminar cannot be arranged as the quota is filled.<br />
                		Parents are welcome to visit our Curriculum Exhibitions during the following period.<br />
                		Thank you!
                		</p>
                		<p>
                		<u>課程展覽 Curriculum Exhibitions</u><br />
                		日期 Date: 2019-01-02<br />
                		時間 Time: 12:34-15:16
                		</p>
            		</fieldset>
        		</div>
        	<?php
        		}
    		}else{
    		    echo '<h3 style="text-align: center;"><font color="black">(結果尚未公報)</font></h3>';
    		}
    		?>
 		
    	
    	<?php if($resultIsOpen && $briefingApplicant['Status'] == $admission_cfg['BriefingStatus']['confirmed']){ ?>
        	<div class="edit_bottom">
        		<input type="submit" class="formbutton" value="<?=$LangB5['Btn']['Print']?> <?=$LangEn['Btn']['Print'] ?>" />
        	</div>
    	<?php } ?>
    	<p class="spacer"></p>
    </div>
</form>

<script>
$('#briefingForm').hide();
</script>