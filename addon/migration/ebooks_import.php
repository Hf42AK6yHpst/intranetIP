<?php

@SET_TIME_LIMIT(60000);

$PATH_WRT_ROOT = "../../";

include_once('../check.php');
include_once($PATH_WRT_ROOT . "includes/global.php");
include_once($PATH_WRT_ROOT . "includes/libdb.php");

intranet_opendb();

$libdb = new libdb();


###########################################

$HandleArray = array("ALL", 868, 873, 875, 876, 1088);


############################################



//debug_r($db_table);
$file_contents = unserialize($db_table);


$filecontents = get_file_content("../../file/ebook_db_146.txt");
//debug($filecontents);
$db_table = unserialize($filecontents);

//debug_r($db_table);


for ($i=0; $i<sizeof($db_table["INTRANET_ELIB_BOOK"]); $i++)
{
	$CurrentObj = $db_table["INTRANET_ELIB_BOOK"][$i];
	if (in_array("ALL", $HandleArray) || in_array($CurrentObj["BookID"], $HandleArray))
	{
		$BookDataByID[$CurrentObj["BookID"]]["INTRANET_ELIB_BOOK"] = $CurrentObj;
	}	
}

for ($i=0; $i<sizeof($db_table["INTRANET_EBOOK_BOOK"]); $i++)
{
	$CurrentObj = $db_table["INTRANET_EBOOK_BOOK"][$i];
	if (in_array("ALL", $HandleArray) || in_array($CurrentObj["BookID"], $HandleArray))
	{
		$BookDataByID[$CurrentObj["BookID"]]["INTRANET_EBOOK_BOOK"] = $CurrentObj;
	}	
}

/*
for ($i=0; $i<sizeof($db_table["INTRANET_EBOOK_PAGE_INFO"]); $i++)
{
	$CurrentObj = $db_table["INTRANET_EBOOK_PAGE_INFO"][$i];
	if (in_array("ALL", $HandleArray) || in_array($CurrentObj["BookID"], $HandleArray))
	{
		$BookDataByID[$CurrentObj["BookID"]]["INTRANET_EBOOK_PAGE_INFO"] = $CurrentObj;
	}	
}
*/

//debug_r($BookDataByID);

function InsertEntry($TableName, $TableObj, $BookID)
{
	global $libdb;
	
	//debug($TableName);
	//debug_r($TableObj);
	
	$fields = "";
	$field_values = "";
	if ($TableName!="" && is_array($TableObj) && sizeof($TableObj)>0)
	{
		foreach ($TableObj as $FieldName => $FieldValue )
		{
			$fields .= (($fields!="") ? ", " : "") . $FieldName;
			if ($FieldValue === NULL)
			{
				$field_values .= (($field_values!="") ? ", " : "") . "NULL";
			} else
			{
				$field_values .= (($field_values!="") ? ", " : "") . "'".addslashes($FieldValue)."'";				
			}
		}
		if ($fields!="" && $field_values!="")
		{
			$sql = "INSERT {$TableName} ({$fields}) VALUES ({$field_values})";
			if (!$libdb->db_db_query($sql))
			{
					debug("encounter problem when inserting data to ".$TableName." for BookID = {$BookID}!");
			}
		} else
		{
			debug($TableName . " has no data / field to insert for BookID = {$BookID}!" );
		}
	} else
	{
		debug($TableName . " cannot be inserted for BookID = {$BookID}!" );
	}
} 

if (is_array($BookDataByID) && sizeof($BookDataByID)>0)
{
	foreach ($BookDataByID as $BookID => $BookObj )
	{
		if (is_array($BookObj) && sizeof($BookObj)>0)
		{
			foreach ($BookObj as $TableName => $TableObj )
			{
				InsertEntry($TableName, $TableObj, $BookID);
			}
		}
		# insert page info
		
		for ($i=0; $i<sizeof($db_table["INTRANET_EBOOK_PAGE_INFO"]); $i++)
		{
			$CurrentObj = $db_table["INTRANET_EBOOK_PAGE_INFO"][$i];
			if ($BookID == $CurrentObj["BookID"])
			{
				//$BookDataByID[$CurrentObj["BookID"]]["INTRANET_EBOOK_PAGE_INFO"] = $CurrentObj;
				InsertEntry("INTRANET_EBOOK_PAGE_INFO", $CurrentObj, $BookID);
			}	
		}
	}
}

debug_pr("===== ALL DONE ====");

?>