<?

$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/lib.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once("liberd.php");
include_once("liberd_ui.php");

intranet_opendb();

$lerd= new liberd();
$lerd_ui= new liberd_ui();

## Create Tmp Table
//if($print_tmp_erd)
//{
//	$code = preg_replace("/CREATE\s+TABLE\s+(IF\s+NOT\s+EXISTS\s+)?/","CREATE TEMPORARY TABLE ",$code);
//	$sqlArr = explode(";",$code);
//	foreach($sqlArr as $thisSql)
//	{
//		$TableName = trim(str_replace("CREATE TEMPORARY TABLE ","",$thisSql));
//		$TableName = substr($TableName,0,stripos($TableName,"("));
//		
//		if(trim($thisSql) && $libdb->db_db_query(trim($thisSql)))
//		{
//			$SelectedTable[] = $TableName;	
//		}
//		else
//		{
//			$sql_err .= $TableName."<br>".mysql_error()."<br>";
//		}
//	}
//}
	
$defaultsize = $CookieFontSize?$CookieFontSize:8;

include_once($PATH_WRT_ROOT."templates/{$LAYOUT_SKIN}/layout/print_header.php");   

echo $lerd_ui->Include_JS_CSS(); 
echo $lerd_ui->Get_Print_ERD_UI($SelectedTable);

?>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>/templates/jquery/jquery-1.3.2.min.js"></script>
<script src="<?=$PATH_WRT_ROOT?>/templates/jquery/ui.core.js"></script>
<script src="<?=$PATH_WRT_ROOT?>/templates/jquery/ui.draggable.js"></script>
<script>
var t;
var defaultsize = <?=$defaultsize?>;
var hasInitPosition = false;
function js_Toggle_Option()
{
	$("#setting_option").toggle("800");
}

function js_Toggle_Display(jsClass)
{
	jsCheck = $("input#Set"+jsClass).attr("checked");
	
	CookieVal = jsCheck?1:0;
	
	document.cookie =  'Cookie'+jsClass+'='+CookieVal+'; ';
	
	if(jsCheck)
		$("."+jsClass).show()
	else
		$("."+jsClass).hide()
}

function chsize(changes)
{
	if(defaultsize + changes >=1)
	{
		defaultsize += changes;
		$("#erd").find("table").each(function(){
			$(this).css("font-size",defaultsize+"px"); 
		});
		
		document.cookie =  'CookieFontSize='+defaultsize+'; ';
	} 
}

var positionArr = new Array();
function initPosition(){
	$("#erd").find("table.DBTableBox").each(function(){
		positionArr.push($(this).position()); 
	});
	
	loadPosition();
} 

function loadPosition()
{
	var i= 0;
	$("#erd").find("table.DBTableBox").each(function(){
		var thisPosition = positionArr[i];
		$(this).css("position","absolute");
		$(this).css("float","");
		$(this).css("top",thisPosition.top);
		$(this).css("left",thisPosition.left);
		i++; 
	});
}

//function initFontsizepanel()
//{
//	$("div#fontsizepanel").css("position","fixed");
//	$("div#fontsizepanel").css("left",0);
//	$("div#fontsizepanel").css("bottom",0);
//	$("div#fontsizepanel").css("opacity","0.7");
//}

function initErrDiv()
{
	$("div#err").css("position","fixed");
	$("div#err").css("left",0);
	$("div#err").css("top",0);
	$("div#err").css("opacity","0.7");
}

function initTableInfoDisplay()
{
	js_Toggle_Display("DBNULL");
	js_Toggle_Display("DBType");
	js_Toggle_Display("KeyTable");
	js_Toggle_Display("DBNameSpan");
	js_Toggle_Display("ShowPreview");
}

var tmpZ = 0;
function initPreviewLayer()
{
	$("table.DBTableBox").mouseenter(function(event){
		if($("input#SetShowPreview").attr("checked") && isDragging==0)
		{
//			$(this).css({"z-index":50});
			var tmpObj = $(event.currentTarget).clone();
			tmpObj.css({"position":"relative","left":0,"top":0});
			
			$("#PreviewLayer").html(tmpObj).show().find("table").css("font-size","10px");
			updatePreviewLayerPosition(event.currentTarget)
		}
		
	}).mouseleave(function(event){
		if($("input#SetShowPreview").attr("checked") && isDragging==0)
		{
			$("#PreviewLayer").hide();
//			$(this).css({"z-index":"auto"});
		}
	});
}

function updatePreviewLayerPosition(Obj)
{
	var pos = $(Obj).position();
	var curTargetTop = pos.top;
	var curTargetLeft = pos.left;
	var LayerLeft =  curTargetLeft + $(Obj).width() + 10;
	if(LayerLeft + $("#PreviewLayer>table.DBTableBox").width() > $(window).width())
		LayerLeft = curTargetLeft - $("#PreviewLayer>table.DBTableBox").width() - 10;
	var LayerTop =  curTargetTop;
	$("#PreviewLayer").css(
		{
			"left":LayerLeft,
			"top":LayerTop,
			"width":$("#PreviewLayer>table.DBTableBox").width(),
			"height":$("#PreviewLayer>table.DBTableBox").height()
		}
	);
}
isDragging = 0;
function initDragDrop()
{
	$("table.DBTableBox").draggable(
		{
			start: function(event, ui) 
			{
				isDragging = 1;
			},
			stop: function(event, ui) 
			{
				if(!hasInitPosition)
				{
			 		initPosition();
			 		hasInitPosition = true;
			 	}
			 	isDragging = 0;
			},
			drag: function(event, ui) 
			{
				updatePreviewLayerPosition(this)	
				
			}
		}
	);
}


function js_Init_Dbl_Click()
{
	$(".DBTableBox").dblclick(function(){
		js_Edit_DB_Table(this)
	});
}

function js_Edit_DB_Table(obj)
{
	var id = '';
	var html = '';
	var checked = '';
	var tableID = $(obj).attr('id');
	
	html += '<table>';
	
	$(obj).find("td.dbfield").find(".FieldRow").each(function(){
		checked = $(this).css("display")=="none"?"":"checked";
		id = "checkbox_"+$(this).attr('id');
		html += '<tr><td><input id="'+id+'" type="checkbox" value=1 '+checked+' onclick="js_Show_Hide_Field(\''+$(this).attr('id')+'\', this.checked, \''+tableID+'\');"><label for="'+id+'">'+$(this).attr('FieldName')+'</label></td></tr>';
	})
	html += '</table>';
	tb_show("show / hide field","#TB_inline?height=400&width=600&inlineId=FakeLayer");
	$("div#TB_ajaxContent").html(html)
}


function js_Show_Hide_Field(objID, checked, tableID)
{
	if(checked)
		$("#"+objID).show();
	else
		$("#"+objID).hide();
		
	js_Check_Hidden_Field(tableID)
}

function js_Check_Hidden_Field(tableID)
{
	if($("#"+tableID).find("table.FieldTable").find(".FieldName:hidden").length>0)
	{
		if($("#"+tableID).find("table.FieldTable").find(".hiddenSign").length<=0)
		{
			$("#"+tableID).find("table.FieldTable>tbody").append("<tr class='hiddenSign'><td>........</td></tr>")
		}
	}
	else
	{
		$("tr.hiddenSign").remove();
	}
}

$().ready(function(){
	initDragDrop()
	initErrDiv();
	initTableInfoDisplay();
	initPreviewLayer();
	js_Init_Dbl_Click();
});

</script>
<style type='text/css'>
	.dbfield {padding:0.08em}
	.erd{width:1084px; height:755px;border:1px solid #000;	}
	.DBTableBox{border:1px solid #000; float:left; margin:5px; }
	.DBTableBox, .DBTableBox table{font-size:<?=$defaultsize?>px; }
	.DBTableBox thead td{ border-bottom:1px solid #000;}
	.KeyTable{ border-top:1px dotted #000;}
	#fontsizepanel {background-color:#000; color:#fff; z-index:10; padding:2; position:absolute; top:0px; right:0px; opacity:0.7; filter:alpha(opacity=70); width:150px;}
	.showhidebtn {border:1px solid #fff; text-align:center; cursor:pointer;}
	#PreviewLayer .DBTableBox { background-color:#eee; color:#000; opacity:0.95; filter:alpha(opacity=95);}
	#PreviewLayer .DBTableBox, #PreviewLayer .DBTableBox table{font-size:10px;}
</style>

<style type='text/css' media='print'>
	.print_hide {display:none;}
	.erd {top:-50px;}
	.erd {border-width:0px;}
</style>

<?

include_once($PATH_WRT_ROOT."templates/{$LAYOUT_SKIN}/layout/print_footer.php");   

intranet_closedb();