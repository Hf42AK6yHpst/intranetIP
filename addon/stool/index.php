<?php
ini_set('display_errors',1);
error_reporting(E_ALL ^ E_NOTICE);

$PATH_WRT_ROOT = "";
include_once($PATH_WRT_ROOT."library/common_function.php");
include_once($PATH_WRT_ROOT."settings.php");

$curFullUrl = getCurPageURL();
$targetFolder = ($DefaultSettingsArr['Security']['RedirectFolder'])? $DefaultSettingsArr['Security']['RedirectFolder'] : 'qtool';

$curPath = dirname(__FILE__);
$curPathAry = explode('/', $curPath);
$curFolder = $curPathAry[count($curPathAry)-1];

$targetUrl = str_replace($curFolder, $targetFolder, $curFullUrl);

header('location: '.$targetUrl);
?>