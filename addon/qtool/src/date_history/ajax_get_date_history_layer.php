<?php
// using ivan
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."library/common_function.php");
include_once($PATH_WRT_ROOT."settings.php");
include_once($PATH_WRT_ROOT."library/lib_ui.php");
$libUI = new lib_ui();

$Charset = $_REQUEST['Charset'];

$x = '';
$x .= '<div>'."\n";
	$x .= '<table width="100%" align="center" valign="top" border="0" cellspacing="0" cellpadding="0">'."\n";
		$x .= '<tr>'."\n";
			$x .= '<td valign="top" width="30%">'.$libUI->GET_DATE_PICKER("HistoryDate", '', 'js_Reload_Date_History_Table();').'</td>'."\n";
			//$x .= '<td valign="top">'.$libUI->Get_Btn('View History', 'button', 'js_Reload_Date_History_Table();', 'ViewHistoryBtn').'</td>'."\n";
		$x .= '</tr>'."\n";
	$x .= '</table>'."\n";
$x .= '</div>'."\n";

$x .= '<br style="clear:both;" />'."\n";

$x .= '<div id="HistoryDetailsDiv" style="width:100%;">'.$libUI->Get_Date_History_Table(date('Y-m-d'), $Charset).'</div>'."\n";
	
echo $x;
?>