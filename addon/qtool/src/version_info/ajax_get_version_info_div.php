<?php
// using 

$InfoArr = array();

# v1.57.1
$InfoArr['1.57.1']['Date'] = '2017-12-21';
$InfoArr['1.57.1']['InfoArr']['Improvements'][] = 'Add school code display before the school name';

# v1.57
$InfoArr['1.57']['Date'] = '2017-11-10';
$InfoArr['1.57']['InfoArr']['Improvements'][] = 'Add "Parent Student" in quick search';

# v1.56
$InfoArr['1.56']['Date'] = '2017-06-02';
$InfoArr['1.56']['InfoArr']['Bug Fix'][] = 'MacBook wrongly submit the page after using the "Command" button';
$InfoArr['1.56']['InfoArr']['Improvements'][] = 'Add "Backup SQL" in show tables view';
$InfoArr['1.56']['InfoArr']['Improvements'][] = 'Add "Convert to backup SQL" function in the SQL textarea';

# v1.55
$InfoArr['1.55']['Date'] = '2015-11-13';
$InfoArr['1.55']['InfoArr']['Improvements'][] = 'Support "Ctrl+S" to submit query';
$InfoArr['1.55']['InfoArr']['Improvements'][] = 'Insert "primary key" symbol in the cursor position instead of appending at the end of the content';
$InfoArr['1.55']['InfoArr']['Improvements'][] = 'Keep the line break for the SQL statement in history table';
$InfoArr['1.55']['InfoArr']['Improvements'][] = 'Sort the queries by date in descending order in the query history layer now';

# v1.54
$InfoArr['1.54']['Date'] = '2015-06-05';
$InfoArr['1.54']['InfoArr']['Improvements'][] = 'Support MacBook "Command+Enter" to submit query';
$InfoArr['1.54']['InfoArr']['Improvements'][] = 'Added "Recent 100" in show table actions';
$InfoArr['1.54']['InfoArr']['Improvements'][] = 'Added [Add Primary Key] shortcut and system will replace "{{{PrimaryKey}}}" with the table\'s primary key automatically.';

# v1.53
$InfoArr['1.53']['Date'] = '2013-10-15';
$InfoArr['1.53']['InfoArr']['Bug Fix'][] = 'Fixed cannot view book title of Lib+ in EJ due to encoding issue.';

# v1.52
$InfoArr['1.52']['Date'] = '2013-05-16';
$InfoArr['1.52']['InfoArr']['Bug Fix'][] = 'Fixed Temp textarea missed "+" after form submit problem.';

# v1.51
$InfoArr['1.51']['Date'] = '2013-04-24';
$InfoArr['1.51']['InfoArr']['New Features'][] = 'Support different colour scheme to distinguish between IP and EJ sites.'; 
$InfoArr['1.51']['InfoArr']['Improvements'][] = 'Added square brackets prefix for LMS database.';
$InfoArr['1.51']['InfoArr']['Improvements'][] = 'Improved the sorting of the database drop down list.';

# v1.50
$InfoArr['1.50']['Date'] = '2012-11-09';
$InfoArr['1.50']['InfoArr']['New Features'][] = 'Support batch SQL statements processing now. You can input a delimiter to separate each SQL statements.'; 
$InfoArr['1.50']['InfoArr']['Improvement'][] = 'Support dynamic width for the UI now.';

# v1.41
$InfoArr['1.41']['Date'] = '2012-01-03';
$InfoArr['1.41']['InfoArr']['Improvements'][] = 'Added "Limit 100" and "Count(*)" short-cut for "show tables"'; 
$InfoArr['1.41']['InfoArr']['Improvements'][] = 'Added short-cut to copy table name to SQL textarea for "show tables"';

# v1.40
$InfoArr['1.40']['Date'] = '2011-12-12';
$InfoArr['1.40']['InfoArr']['New Features'][] = 'Support clients with global.php only now'; 
$InfoArr['1.40']['InfoArr']['New Features'][] = 'Added a comma icon in the Title of each columns to make a list of the corresponding column data (easier to build a "In (xxx,xxx,xxx)" SQL statement)';
$InfoArr['1.40']['InfoArr']['New Features'][] = 'Added client school name at the top left corner (the school name is retrieved from admin console)';

# v1.33
$InfoArr['1.33']['Date'] = '2011-04-01';
$InfoArr['1.33']['InfoArr']['Improvements'][] = 'Added a shot cut for "Show Tables Like"'; 
$InfoArr['1.33']['InfoArr']['Improvements'][] = 'An Export button (with hot-key Crtl+Alt+X) is added to export the SQL Result without submitting (esp. for huge data results SQL)';
$InfoArr['1.33']['InfoArr']['Bug Fix'][] = 'Quick-search function failed to replace all strings in the query';

# v1.32
$InfoArr['1.32']['Date'] = '2011-03-08';
$InfoArr['1.32']['InfoArr']['Improvements'][] = 'DB info layer is added for users to input the DB info if the client does not have settings.php';
$InfoArr['1.32']['InfoArr']['Improvements'][] = 'Textarea supports "tab" input now';
$InfoArr['1.32']['InfoArr']['Improvements'][] = 'Quick-search layer left menu is changed to jQuery without using any other plugins';
$InfoArr['1.32']['InfoArr']['Improvements'][] = 'Quick-search logic is simplified so that quick search item can be added easier';
$InfoArr['1.32']['InfoArr']['Improvements'][] = 'Quick-search uses default IntranetDB for some searching (e.g. User, Form Class, Academic Year, etc...) even if the system is switched to other DB';
$InfoArr['1.32']['InfoArr']['Improvements'][] = 'Added hot-key "Ctrl + Alt + T" for back-to-top function';

# v1.31
$InfoArr['1.31']['Date'] = '2010-12-20';
$InfoArr['1.31']['InfoArr']['Improvement'][] = 'The SQL textareas are draggable now';

# v1.30
$InfoArr['1.30']['Date'] = '2010-12-16';
$InfoArr['1.30']['InfoArr']['New Features'][] = 'Past History can be viewed (by clicking the "More..." in the History Table)';
$InfoArr['1.30']['InfoArr']['New Features'][] = 'A temp storage textarea is added';
$InfoArr['1.30']['InfoArr']['Improvements'][] = 'The SQL textarea is resizeable';
$InfoArr['1.30']['InfoArr']['Improvements'][] = 'An "Explain" button (with hot-key Crtl+Alt+E) is added';

# v1.23
$InfoArr['1.23']['Date'] = '2010-11-24';
$InfoArr['1.23']['InfoArr']['Improvements'][] = '"ReportCard (Rubrics)" is added in Quick-Search function';
$InfoArr['1.23']['InfoArr']['Improvements'][] = 'The input field is trimmed before searching';
$InfoArr['1.23']['InfoArr']['Improvements'][] = 'HTML entities can be displayed as "&amp;quot;" now';
$InfoArr['1.23']['InfoArr']['Improvements'][] = 'CSV can be exported even if the page is not fully loaded (good for batch data browsing)';

# v1.22
$InfoArr['1.22']['Date'] = '2010-01-19';
$InfoArr['1.22']['InfoArr']['New Feature'][] = 'Added Password For Security Issue';

# v1.21
$InfoArr['1.21']['Date'] = '2010-01-08';
$InfoArr['1.21']['InfoArr']['New Feature'][] = 'Support "Use" to change the Database Selection';
$InfoArr['1.21']['InfoArr']['Bug Fix'][] = 'Short Cut of Quick-Search did not work in Firefox';

# v1.20
$InfoArr['1.20']['Date'] = '';
$InfoArr['1.20']['InfoArr']['New Feature'][] = 'Quick-Search of some items (Short-cut: Ctrl + Alt + Q)';

# v1.10
$InfoArr['1.10']['Date'] = '';
$InfoArr['1.10']['InfoArr']['New Features'][] = 'Keep Daily Log of SQL submitted (depends of the server permission settings)';
$InfoArr['1.10']['InfoArr']['New Features'][] = 'Retrieve Databases Information from the eClass Intranet Settings automatically';
$InfoArr['1.10']['InfoArr']['New Features'][] = 'Integrate jQuery DataTable Plugin for Easier Searching';
$InfoArr['1.10']['InfoArr']['New Features'][] = 'Integrate jQuery FancyBox Plugin for Layer Display';

# v1.00
$InfoArr['1.00']['Date'] = '';
$InfoArr['1.00']['InfoArr']['Basic Features'][] = 'Support Basic MySQL statements';
$InfoArr['1.00']['InfoArr']['Basic Features'][] = 'Keep History for the current visit';
$InfoArr['1.00']['InfoArr']['Basic Features'][] = 'Provide Shortcut for different SQL Statements';
$InfoArr['1.00']['InfoArr']['Basic Features'][] = 'Provide Shortcut "Alt+Enter" for Query Submission';


$x = '';
$x .= '<div style="height:340px; overflow:auto;">'."\n";
	$x .= '<table width="95%" border="0" cellspacing="0" cellpadding="2">'."\n";
		$x .= '<tr><td>&nbsp;</td></tr>'."\n";
		
		foreach ((array)$InfoArr as $thisVersion => $thisInfoArr)
		{
			$thisDate = ($thisInfoArr['Date']=='')? '' : '('.$thisInfoArr['Date'].')';
			$thisVersionInfoArr = $thisInfoArr['InfoArr'];
			
			$x .= '<tr><td>v'.$thisVersion.' '.$thisDate.'</td></tr>'."\n";
			
			foreach ((array)$thisVersionInfoArr as $thisSectionTitle => $thisSectionInfoArr)
			{
				$thisNumOfInfo = count($thisSectionInfoArr);
				
				$x .= '<tr><td>'.$thisSectionTitle.'</td></tr>'."\n";
				$x .= '<tr><td>'."\n";
					$x .= '<ol>'."\n";
						for ($i=0; $i<$thisNumOfInfo; $i++)
						{
							$thisSectionPoint = $thisSectionInfoArr[$i];
							
							$x .= '<li>'."\n";
								$x .= $thisSectionPoint."\n";
							if (is_array($thisSectionPoint))
							{
								
							}
							$x .= '</li>'."\n";
						}
					$x .= '</ol>'."\n";
				$x .= '</td></tr>'."\n";
			}
			
			$x .= '<tr><td><hr /></td></tr>'."\n";
		}
	$x .= '</table>'."\n";
$x .= '</div>'."\n";

echo $x;

?>