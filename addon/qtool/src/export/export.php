<?php
# include DB settings and common function
$PATH_WRT_ROOT = '../../';
include_once($PATH_WRT_ROOT."library/common_function.php");
include_once($PATH_WRT_ROOT."settings.php");
include_once($PATH_WRT_ROOT."library/libdb.php");
include_once($PATH_WRT_ROOT."library/libexport.php");

$sql = stripslashes($_POST['SQL_Export']);
$Database = stripslashes($_POST['Database']);
$Charset = stripslashes($_POST['Charset']);
$Delimiter = trim(stripslashes($_POST['Delimiter']));
$OnTheFlyByPassLoginPin1= trim(stripslashes($_POST['OnTheFlyByPassLoginPin1']));
$OnTheFlyByPassLoginPin2= trim(stripslashes($_POST['OnTheFlyByPassLoginPin2']));
$CurSettingsArr = Get_Current_Settings_Array($OnTheFlyCanView, $OnTheFlyCanUpdate, $OnTheFlyByPassLogin, $Delimiter, $OnTheFlyByPassLoginPin1, $OnTheFlyByPassLoginPin2);

$libDB = new libdb($Database);
$libDB->Open_DB($CurSettingsArr);


### Get Export Array
$ResultSetArr = $libDB->returnArray($sql);
$ResultSetHeaderArr = $libDB->returnAssoArray($sql);

$HeaderTitleArr = array_keys($ResultSetHeaderArr[0]);
$exportHeader = array();
foreach ($HeaderTitleArr as $HeaderTitle)
{
	$exportHeader[] = $HeaderTitle;
}


$libExport = new libexport();
if (strtolower($Charset) == 'big5') {
	$exportContent = $libExport->GET_EXPORT_TXT($ResultSetArr, $exportHeader, ",", "\r\n", ",", 0, "11");
}
else {
	$exportContent = $libExport->GET_EXPORT_TXT($ResultSetArr, $exportHeader);
}
	
$libDB->Close_DB();

### Output the file to user browser
$filename = 'sql_tool.csv';
$libExport->EXPORT_FILE($filename, $exportContent, $Charset);
?>