<?php
// using ivan
$PATH_WRT_ROOT = '../../';
include_once($PATH_WRT_ROOT."library/common_function.php");
include_once($PATH_WRT_ROOT."settings.php");
include_once($PATH_WRT_ROOT."library/libdb.php");
include_once($PATH_WRT_ROOT."library/libfilesystem.php");

$libfilesystem = new libfilesystem();


# Get data
$TargetSql = stripslashes($_REQUEST['TargetSql']);
$Charset = stripslashes($_REQUEST['Charset']);
$Database = stripslashes($_REQUEST['Database']);
$SuccessArr = array();


### create folder and file
$LogFolderPath = $IntranetSettingsPathRoot.'file/';
$numOfPathPieces = count($DefaultSettingsArr['Log']['FilePathArr']);
for ($i=0; $i<$numOfPathPieces; $i++)
{
	$thisFolder = $DefaultSettingsArr['Log']['FilePathArr'][$i];
	$LogFolderPath .= $thisFolder.'/';
	if (!file_exists($LogFolderPath))
		$SuccessArr['CreateFolder'][$thisFolder] = $libfilesystem->folder_new($LogFolderPath);
}


### create folder /file/import_temp/query_log/[#Date]
$TodayDate = date('Ymd');
$LogFolderPath .= $TodayDate.'/';
$SuccessArr['CreateFolder']['#Date'] = $libfilesystem->folder_new($LogFolderPath);


### write content to the log
$LogFileName = $TodayDate.'.csv';
$LogFolderPath .= $LogFileName;

if (strtolower($Charset) == 'big5')
{
	$separator = ",";
	$csvPrefix = "";
	$convertContent = false;
}
else
{
	$separator = "\t";
	$csvPrefix = "\xFF\xFE";
	$convertContent = true;
}


### Construct new content row
$CurrentDateTime = date('H:i:s');
$UserIP = $_SERVER["REMOTE_ADDR"];
$TargetSql = trim(stripslashes(urldecode($_POST['TargetSql'])));
$TargetSql = Convert_SQL_to_log_SQL($TargetSql);

$thisInfoArr = array($CurrentDateTime, $UserIP, $Database, '"'.$TargetSql.'"');
$NewContentRow = '';
$NewContentRow .= implode($separator, $thisInfoArr);
$NewContentRow .= "\r\n";

if (file_exists($LogFolderPath) == true)
{
	if ($convertContent == true)
		$NewContentRow = mb_convert_encoding($NewContentRow, 'UTF-16LE', 'UTF-8');
		
	# Append Content
	$SuccessArr['AppendFile'] = $libfilesystem->file_append($NewContentRow, $LogFolderPath);
}
else
{
	# Build Header
	$Content = '';

	$HeaderArr = array('Time', 'IP', 'Database', 'SQL');
	$Content .= implode($separator, $HeaderArr);
	$Content .= "\r\n";
	
	$Content .= $NewContentRow;
	
	// do not convert the csv prefix
	if ($convertContent == true)
		$Content = mb_convert_encoding($Content, 'UTF-16LE', 'UTF-8');

	### Write to file
	$SuccessArr['WriteFile'] = $libfilesystem->file_write($csvPrefix.$Content, $LogFolderPath);
}


if (in_array(false, $SuccessArr))
	echo 0;
else
	echo 1;
?>