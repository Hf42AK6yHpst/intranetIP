<?php
$PATH_WRT_ROOT = '../../';
include_once($PATH_WRT_ROOT."library/common_function.php");
include_once($PATH_WRT_ROOT."settings.php");


$task = $_POST['task'];
if ($task == 'generatePin2') {
	$pin1 = $_POST['pin1'];
	
	$pin1Valid = checkValidPin1($pin1);
	if ($pin1Valid) {
		$returnVal = generatePin2Key($pin1);
	}
	else {
		$returnVal = 'error';
	}
	
	echo $returnVal;
}
else if ($task == 'validatePin2') {
	$pin2 = $_POST['pin2'];
	
	echo (checkValidPin2($pin2))? '1' : '0';
}
?>