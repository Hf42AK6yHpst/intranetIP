<?php
$PATH_WRT_ROOT = '../../';
include_once($PATH_WRT_ROOT."library/common_function.php");
include_once($PATH_WRT_ROOT."settings.php");

$able2View = (int)$_GET['able2View'];
$able2Update = (int)$_GET['able2Update'];

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html" charset="UTF-8" />
		<title>eClass SQL Tool Pin Login</title>
		<script language="javascript" src="../../../../templates/jquery/jquery-1.3.2.min.js"></script>
		<script language="javascript" src="../../../../templates/script.js"></script>
		<script language="javascript">
			$(document).ready( function() {
				$('input#pin1Tb').focus();
			});
			
			function generatePin2() {
				$.post(
					"ajax_task.php", 
					{
						task: 'generatePin2',
						pin1: $('input#pin1Tb').val()
					},
					function(returnData) {
						if (returnData == 'error') {
							$('div#pin1ErrorDiv').show();
							$('div#pin2KeyDiv').html('');
						}
						else {
							$('div#pin1ErrorDiv').hide();
							$('div#pin2KeyDiv').html(returnData);
							$('input#pin2Tb').focus();
						}
					}
				);
			}
			
			function validatePin2() {
				$.post(
					"ajax_task.php", 
					{
						task: 'validatePin2',
						pin2: $('input#pin2Tb').val()
					},
					function(returnData) {
						if (returnData == '1') {
							$('div#pin2ErrorDiv').hide();
							$('input#OnTheFlyByPassLoginPin1').val($('input#pin1Tb').val());
							$('input#OnTheFlyByPassLoginPin2').val($('input#pin2Tb').val());
							$('form#form1').attr('action', '../../').attr('method', 'post').submit();
						}
						else {
							$('div#pin2ErrorDiv').show();
						}
					}
				);
			}
		</script>
		<style>
			html { font-size:13.3333px; font-family:Arial; }
		</style>
	</head>
	<body>
		<br />
		<form id="form1" name="form1">
			<table>
				<tr>
					<td style="width:150px; vertical-align:top;">Pin 1</td>
					<td>
						<input type="input" id="pin1Tb"> <input type="button" value="Submit" onclick="generatePin2();">
						<div id="pin1ErrorDiv" style="color:red; font-weight:bold; display:none;">Invalid</div>
						<hr />
					</td>
				</tr>
				<tr id=""pin2Tr">
					<td style="vertical-align:top;">Pin 2</td>
					<td>
						<div id="pin2KeyDiv"></div>
						<input type="input" id="pin2Tb"> <input type="button" value="Submit" onclick="validatePin2();">
						<div id="pin2ErrorDiv" style="color:red; font-weight:bold; display:none;">Invalid</div>
					</td>
				</tr>
			</table>
			<br />
			
			<input type="hidden" id="OnTheFlyCanView" name="OnTheFlyCanView" value="<?=$able2View?>">
			<input type="hidden" id="OnTheFlyCanUpdate" name="OnTheFlyCanUpdate" value="<?=$able2Update?>">
			<input type="hidden" id="OnTheFlyByPassLoginPin1" name="OnTheFlyByPassLoginPin1" value="">
			<input type="hidden" id="OnTheFlyByPassLoginPin2" name="OnTheFlyByPassLoginPin2" value="">
		</form>
	</body>
</html>