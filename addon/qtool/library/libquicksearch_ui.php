<?php

class libquicksearch_ui{
	var $TargetValueTemplate;
	var $IntranetDB;
	
	function libquicksearch_ui($IntranetDB='')
	{
		$this->TargetValueTemplate = "<!--SearchValue-->";
		$this->IntranetDB = $IntranetDB;
	}
	
	function Get_Left_Menu_Structure_Array()
	{
		$IntranetDB = $this->IntranetDB;
		$MenuArr = array();	
		
		### Intranet User
		$MenuArr['IntranetUser']['Title'] = 'Intranet User';
		$MenuArr['IntranetUser']['SubMenuArr']['intranet_user']['Title'] = 'Intranet User';
		$MenuArr['IntranetUser']['SubMenuArr']['intranet_user']['InfoArr'][] = array(	'SearchTitle' => 'ID',
																						'jsSearchType' => 'user_id',
																						'jsSearchSQL' => "Select * From ".$IntranetDB.".INTRANET_USER Where UserID = '".$this->TargetValueTemplate."';"
																						);
		$MenuArr['IntranetUser']['SubMenuArr']['intranet_user']['InfoArr'][] = array(	'SearchTitle' => 'Login',
																						'jsSearchType' => 'user_login',
																						'jsSearchSQL' => "Select * From ".$IntranetDB.".INTRANET_USER Where UserLogin = '".$this->TargetValueTemplate."';"
																						);
		$MenuArr['IntranetUser']['SubMenuArr']['intranet_user']['InfoArr'][] = array(	'SearchTitle' => 'Name',
																						'jsSearchType' => 'user_name',
																						'jsSearchSQL' => "Select * From ".$IntranetDB.".INTRANET_USER Where ChineseName Like '%".$this->TargetValueTemplate."%' OR EnglishName Like '%".$this->TargetValueTemplate."%';"
																						);
		$MenuArr['IntranetUser']['SubMenuArr']['intranet_user']['InfoArr'][] = array(	'SearchTitle' => 'WebSAMS',
																						'jsSearchType' => 'user_websams',
																						'tbDefaultValue' => '#', 
																						'jsSearchSQL' => "Select * From ".$IntranetDB.".INTRANET_USER Where WebSAMSRegNo = '".$this->TargetValueTemplate."';"
																						);
		$MenuArr['IntranetUser']['SubMenuArr']['intranet_user']['InfoArr'][] = array(	'SearchTitle' => 'STRN',
																						'jsSearchType' => 'user_strn',
																						'jsSearchSQL' => "Select * From ".$IntranetDB.".INTRANET_USER Where STRN = '".$this->TargetValueTemplate."';"
																						);
		$MenuArr['IntranetUser']['SubMenuArr']['intranet_user']['InfoArr'][] = array(	'SearchTitle' => 'ClassName',
																						'jsSearchType' => 'user_classname',
																						'jsSearchSQL' => "Select * From ".$IntranetDB.".INTRANET_USER Where ClassName Like '%".$this->TargetValueTemplate."%';"
																						);
		
		### Parent Student
		$MenuArr['ParentStudent']['Title'] = 'Parent student';
		$MenuArr['ParentStudent']['SubMenuArr']['intranet_parentrelation']['Title'] = 'Parent student';
		$MenuArr['ParentStudent']['SubMenuArr']['intranet_parentrelation']['InfoArr'][] = array(	'SearchTitle' => 'ParentID',
																								'jsSearchType' => 'parent_user_id',
																								'jsSearchSQL' => "Select * From ".$IntranetDB.".INTRANET_PARENTRELATION Where ParentID = '".$this->TargetValueTemplate."';"
																						);
		$MenuArr['ParentStudent']['SubMenuArr']['intranet_parentrelation']['InfoArr'][] = array(	'SearchTitle' => 'StudentID',
																								'jsSearchType' => 'student_user_id',
																								'jsSearchSQL' => "Select * From ".$IntranetDB.".INTRANET_PARENTRELATION Where StudentID = '".$this->TargetValueTemplate."';"
																						);
		
		### Form Class
		$MenuArr['FormClass']['Title'] = 'Form Class';
		
		### Form Class > Form
		$MenuArr['FormClass']['SubMenuArr']['form']['Title'] = "Form";
		$MenuArr['FormClass']['SubMenuArr']['form']['InfoArr'][] = array(	'SearchTitle' => 'ID',
																			'jsSearchType' => 'formid',
																			'jsSearchSQL' => "Select * From ".$IntranetDB.".YEAR Where YearID = '".$this->TargetValueTemplate."';"
																			);
		$MenuArr['FormClass']['SubMenuArr']['form']['InfoArr'][] = array(	'SearchTitle' => 'Name',
																			'jsSearchType' => 'form_name',
																			'jsSearchSQL' => "Select * From ".$IntranetDB.".YEAR Where YearName Like '%".$this->TargetValueTemplate."%';"
																			);
		$MenuArr['FormClass']['SubMenuArr']['form']['InfoArr'][] = array(	'SearchTitle' => 'WebSAMS Code',
																			'jsSearchType' => 'form_websams_code',
																			'jsSearchSQL' => "Select * From ".$IntranetDB.".YEAR Where WEBSAMSCode = '".$this->TargetValueTemplate."';"
																			);
		
		### Form Class > Class
		$MenuArr['FormClass']['SubMenuArr']['class']['Title'] = "Class";
		$MenuArr['FormClass']['SubMenuArr']['class']['InfoArr'][] = array(	'SearchTitle' => 'ID',
																			'jsSearchType' => 'classid',
																			'jsSearchSQL' => "Select * From ".$IntranetDB.".YEAR_CLASS Where YearClassID = '".$this->TargetValueTemplate."';"
																			);
		$MenuArr['FormClass']['SubMenuArr']['class']['InfoArr'][] = array(	'SearchTitle' => 'Name',
																			'jsSearchType' => 'class_name',
																			'jsSearchSQL' => "Select * From ".$IntranetDB.".YEAR_CLASS Where ClassTitleEN Like '%".$this->TargetValueTemplate."%' Or ClassTitleB5 Like '%".$this->TargetValueTemplate."%';"
																			);
		$MenuArr['FormClass']['SubMenuArr']['class']['InfoArr'][] = array(	'SearchTitle' => 'WebSAMS Code',
																			'jsSearchType' => 'class_websams_code',
																			'jsSearchSQL' => "Select * From ".$IntranetDB.".YEAR_CLASS Where WEBSAMSCode = '".$this->TargetValueTemplate."';"
																			);
		$MenuArr['FormClass']['SubMenuArr']['class']['InfoArr'][] = array(	'SearchTitle' => 'YearID',
																			'jsSearchType' => 'class_yearid',
																			'jsSearchSQL' => "Select * From ".$IntranetDB.".YEAR_CLASS Where YearID = '".$this->TargetValueTemplate."';"
																			);
		$MenuArr['FormClass']['SubMenuArr']['class']['InfoArr'][] = array(	'SearchTitle' => 'AcademicYearID',
																			'jsSearchType' => 'class_academicyearid',
																			'jsSearchSQL' => "Select * From ".$IntranetDB.".YEAR_CLASS Where AcademicYearID = '".$this->TargetValueTemplate."';"
																			);
		$MenuArr['FormClass']['SubMenuArr']['class']['InfoArr'][] = array(	'SearchTitle' => 'GroupID',
																			'jsSearchType' => 'class_groupid',
																			'jsSearchSQL' => "Select * From ".$IntranetDB.".YEAR_CLASS Where GroupID = '".$this->TargetValueTemplate."';"
																			);
		
		
		### Subject
		$MenuArr['Subject']['Title'] = 'Subject';
		
		### Subject > Learning Category
		$MenuArr['Subject']['SubMenuArr']['learning_category']['Title'] = "Learning Category";
		$MenuArr['Subject']['SubMenuArr']['learning_category']['InfoArr'][] = array('SearchTitle' => 'ID',
																					'jsSearchType' => 'learningcategoryid',
																					'jsSearchSQL' => "Select * From ".$IntranetDB.".LEARNING_CATEGORY Where LearningCategoryID = '".$this->TargetValueTemplate."';"
																					);
		$MenuArr['Subject']['SubMenuArr']['learning_category']['InfoArr'][] = array('SearchTitle' => 'Name',
																					'jsSearchType' => 'learningcategory_name',
																					'jsSearchSQL' => "Select * From ".$IntranetDB.".LEARNING_CATEGORY Where NameEng Like '%".$this->TargetValueTemplate."%' Or NameChi Like '%".$this->TargetValueTemplate."%';"
																					);
		$MenuArr['Subject']['SubMenuArr']['learning_category']['InfoArr'][] = array('SearchTitle' => 'Code',
																					'jsSearchType' => 'learningcategory_code',
																					'jsSearchSQL' => "Select * From ".$IntranetDB.".LEARNING_CATEGORY Where Code = '".$this->TargetValueTemplate."';"
																					);
		
		### Subject > Subject
		$MenuArr['Subject']['SubMenuArr']['subject']['Title'] = "Subject";
		$MenuArr['Subject']['SubMenuArr']['subject']['InfoArr'][] = array(	'SearchTitle' => 'ID',
																			'jsSearchType' => 'subjectid',
																			'jsSearchSQL' => "Select * From ".$IntranetDB.".ASSESSMENT_SUBJECT Where RecordID = '".$this->TargetValueTemplate."';"
																			);
		$MenuArr['Subject']['SubMenuArr']['subject']['InfoArr'][] = array(	'SearchTitle' => 'Name',
																			'jsSearchType' => 'subject_name',
																			'jsSearchSQL' => "Select * From ".$IntranetDB.".ASSESSMENT_SUBJECT Where EN_SNAME Like '%".$this->TargetValueTemplate."%' Or CH_SNAME Like '%".$this->TargetValueTemplate."%' Or EN_DES Like '%".$this->TargetValueTemplate."%' Or CH_DES Like '%".$this->TargetValueTemplate."%' Or EN_ABBR Like '%".$this->TargetValueTemplate."%' Or CH_ABBR Like '%".$this->TargetValueTemplate."%';"
																			);
		$MenuArr['Subject']['SubMenuArr']['subject']['InfoArr'][] = array(	'SearchTitle' => 'Code',
																			'jsSearchType' => 'subject_code',
																			'jsSearchSQL' => "Select * From ".$IntranetDB.".ASSESSMENT_SUBJECT Where CODEID = '".$this->TargetValueTemplate."' OR CMP_CODEID = '".$this->TargetValueTemplate."';"
																			);
		$MenuArr['Subject']['SubMenuArr']['subject']['InfoArr'][] = array(	'SearchTitle' => 'LearningCategoryID',
																			'jsSearchType' => 'subject_learning_category',
																			'jsSearchSQL' => "Select * From ".$IntranetDB.".ASSESSMENT_SUBJECT Where LearningCategoryID = '".$this->TargetValueTemplate."';"
																			);
		
		### Subject > Subject Group
		$MenuArr['Subject']['SubMenuArr']['subject_group']['Title'] = "Subject Group";
		$MenuArr['Subject']['SubMenuArr']['subject_group']['InfoArr'][] = array('SearchTitle' => 'ID',
																				'jsSearchType' => 'subjectgroupid',
																				'jsSearchSQL' => "Select * From ".$IntranetDB.".SUBJECT_TERM_CLASS Where SubjectGroupID = '".$this->TargetValueTemplate."';"
																				);
		$MenuArr['Subject']['SubMenuArr']['subject_group']['InfoArr'][] = array('SearchTitle' => 'Name',
																				'jsSearchType' => 'subject_group_name',
																				'jsSearchSQL' => "Select * From ".$IntranetDB.".SUBJECT_TERM_CLASS Where ClassTitleEN Like '%".$this->TargetValueTemplate."%' Or ClassTitleB5 Like '%".$this->TargetValueTemplate."%';"
																				);
		$MenuArr['Subject']['SubMenuArr']['subject_group']['InfoArr'][] = array('SearchTitle' => 'Code',
																				'jsSearchType' => 'subject_group_code',
																				'jsSearchSQL' => "Select * From ".$IntranetDB.".SUBJECT_TERM_CLASS Where ClassCode = '".$this->TargetValueTemplate."';"
																				);
		$MenuArr['Subject']['SubMenuArr']['subject_group']['InfoArr'][] = array('SearchTitle' => 'Internal Code',
																				'jsSearchType' => 'subject_group_internal_code',
																				'jsSearchSQL' => "Select * From ".$IntranetDB.".SUBJECT_TERM_CLASS Where InternalClassCode = '".$this->TargetValueTemplate."';"
																				);
		
		
		### Location
		$MenuArr['Location']['Title'] = 'Location';
		
		### Location > Building
		$MenuArr['Location']['SubMenuArr']['building']['Title'] = "Building";
		$MenuArr['Location']['SubMenuArr']['building']['InfoArr'][] = array('SearchTitle' => 'ID',
																			'jsSearchType' => 'buildingid',
																			'jsSearchSQL' => "Select * From ".$IntranetDB.".INVENTORY_LOCATION_BUILDING Where BuildingID = '".$this->TargetValueTemplate."';"
																			);
		$MenuArr['Location']['SubMenuArr']['building']['InfoArr'][] = array('SearchTitle' => 'Name',
																			'jsSearchType' => 'building_name',
																			'jsSearchSQL' => "Select * From ".$IntranetDB.".INVENTORY_LOCATION_BUILDING Where NameEng Like '%".$this->TargetValueTemplate."%' Or NameChi Like '%".$this->TargetValueTemplate."%';"
																			);
		$MenuArr['Location']['SubMenuArr']['building']['InfoArr'][] = array('SearchTitle' => 'Code',
																			'jsSearchType' => 'building_code',
																			'jsSearchSQL' => "Select * From ".$IntranetDB.".INVENTORY_LOCATION_BUILDING Where Code = '".$this->TargetValueTemplate."';"
																			);
																			
		### Location > Floor
		$MenuArr['Location']['SubMenuArr']['floor']['Title'] = "Floor";
		$MenuArr['Location']['SubMenuArr']['floor']['InfoArr'][] = array(	'SearchTitle' => 'ID',
																			'jsSearchType' => 'floorid',
																			'jsSearchSQL' => "Select * From ".$IntranetDB.".INVENTORY_LOCATION_LEVEL Where LocationLevelID = '".$this->TargetValueTemplate."';"
																			);
		$MenuArr['Location']['SubMenuArr']['floor']['InfoArr'][] = array(	'SearchTitle' => 'Name',
																			'jsSearchType' => 'floor_name',
																			'jsSearchSQL' => "Select * From ".$IntranetDB.".INVENTORY_LOCATION_LEVEL Where NameEng Like '%".$this->TargetValueTemplate."%' Or NameChi Like '%".$this->TargetValueTemplate."%';"
																			);
		$MenuArr['Location']['SubMenuArr']['floor']['InfoArr'][] = array(	'SearchTitle' => 'Code',
																			'jsSearchType' => 'floor_code',
																			'jsSearchSQL' => "Select * From ".$IntranetDB.".INVENTORY_LOCATION_LEVEL Where Code = '".$this->TargetValueTemplate."';"
																			);
		$MenuArr['Location']['SubMenuArr']['floor']['InfoArr'][] = array(	'SearchTitle' => 'BuildingID',
																			'jsSearchType' => 'floor_buildingid',
																			'jsSearchSQL' => "Select * From ".$IntranetDB.".INVENTORY_LOCATION_LEVEL Where BuildingID = '".$this->TargetValueTemplate."';"
																			);
		
		### Location > Room 
		$MenuArr['Location']['SubMenuArr']['room']['Title'] = "Room";
		$MenuArr['Location']['SubMenuArr']['room']['InfoArr'][] = array('SearchTitle' => 'ID',
																		'jsSearchType' => 'roomid',
																		'jsSearchSQL' => "Select * From ".$IntranetDB.".INVENTORY_LOCATION Where LocationID = '".$this->TargetValueTemplate."';"
																		);
		$MenuArr['Location']['SubMenuArr']['room']['InfoArr'][] = array('SearchTitle' => 'Name',
																		'jsSearchType' => 'room_name',
																		'jsSearchSQL' => "Select * From ".$IntranetDB.".INVENTORY_LOCATION Where NameEng Like '%".$this->TargetValueTemplate."%' Or NameChi Like '%".$this->TargetValueTemplate."%';"
																		);
		$MenuArr['Location']['SubMenuArr']['room']['InfoArr'][] = array('SearchTitle' => 'Code',
																		'jsSearchType' => 'room_code',
																		'jsSearchSQL' => "Select * From ".$IntranetDB.".INVENTORY_LOCATION Where Code = '".$this->TargetValueTemplate."';"
																		);
		$MenuArr['Location']['SubMenuArr']['room']['InfoArr'][] = array('SearchTitle' => 'LocationLevelID',
																		'jsSearchType' => 'room_floorid',
																		'jsSearchSQL' => "Select * From ".$IntranetDB.".INVENTORY_LOCATION Where LocationLevelID = '".$this->TargetValueTemplate."';"
																		);
		
		
		### School Year
		$MenuArr['SchoolYear']['Title'] = 'School Year';
		
		### School Year > Academic Year 
		$MenuArr['SchoolYear']['SubMenuArr']['academic_year']['Title'] = "Academic Year";
		$MenuArr['SchoolYear']['SubMenuArr']['academic_year']['InfoArr'][] = array(	'SearchTitle' => 'ID',
																					'jsSearchType' => 'academicyearid',
																					'jsSearchSQL' => "Select * From ".$IntranetDB.".ACADEMIC_YEAR Where AcademicYearID = '".$this->TargetValueTemplate."';"
																					);
		$MenuArr['SchoolYear']['SubMenuArr']['academic_year']['InfoArr'][] = array(	'SearchTitle' => 'Name',
																					'jsSearchType' => 'academicyear_name',
																					'jsSearchSQL' => "Select * From ".$IntranetDB.".ACADEMIC_YEAR Where YearNameEN = '".$this->TargetValueTemplate."' Or YearNameB5 = '".$this->TargetValueTemplate."';"
																					);
		
		### School Year > Term 
		$MenuArr['SchoolYear']['SubMenuArr']['term']['Title'] = "Term";
		$MenuArr['SchoolYear']['SubMenuArr']['term']['InfoArr'][] = array(	'SearchTitle' => 'ID',
											'jsSearchType' => 'yeartermid',
											'jsSearchSQL' => "Select * From ".$IntranetDB.".ACADEMIC_YEAR_TERM Where YearTermID = '".$this->TargetValueTemplate."';"
		);
		$MenuArr['SchoolYear']['SubMenuArr']['term']['InfoArr'][] = array(	'SearchTitle' => 'Name',
											'jsSearchType' => 'yearterm_name',
											'jsSearchSQL' => "Select * From ".$IntranetDB.".ACADEMIC_YEAR_TERM Where YearTermNameEN Like '%".$this->TargetValueTemplate."%' Or YearTermNameB5 Like '%".$this->TargetValueTemplate."%';"
		);
		$MenuArr['SchoolYear']['SubMenuArr']['term']['InfoArr'][] = array(	'SearchTitle' => 'AcademicYearID',
											'jsSearchType' => 'yearterm_academicyearid',
											'jsSearchSQL' => "Select * From ".$IntranetDB.".ACADEMIC_YEAR_TERM Where AcademicYearID = '".$this->TargetValueTemplate."';"
		);
		
		
		### eEnrolment
		$MenuArr['eEnrolment']['Title'] = 'eEnrolment';
		
		### eEnrolment > Club
		$MenuArr['eEnrolment']['SubMenuArr']['enrolment_club']['Title'] = "Club";
		$MenuArr['eEnrolment']['SubMenuArr']['enrolment_club']['InfoArr'][] = array('SearchTitle' => 'EnrolGroupID',
																					'jsSearchType' => 'enrolgroupid',
																					'jsSearchSQL' => "Select ieg.*, ig.AcademicYearID From ".$IntranetDB.".INTRANET_GROUP as ig Inner Join INTRANET_ENROL_GROUPINFO as ieg On (ig.GroupID = ieg.GroupID) Where ieg.EnrolGroupID = '".$this->TargetValueTemplate."';"
																					);
		$MenuArr['eEnrolment']['SubMenuArr']['enrolment_club']['InfoArr'][] = array('SearchTitle' => 'GroupID',
																					'jsSearchType' => 'groupid',
																					'jsSearchSQL' => "Select * From ".$IntranetDB.".INTRANET_GROUP Where GroupID = '".$this->TargetValueTemplate."';"
																					);
		$MenuArr['eEnrolment']['SubMenuArr']['enrolment_club']['InfoArr'][] = array('SearchTitle' => 'Name',
																					'jsSearchType' => 'club_name',
																					'jsSearchSQL' => "Select * From ".$IntranetDB.".INTRANET_GROUP Where Title Like '%".$this->TargetValueTemplate."%' And RecordType = '5';"
																					);
		$MenuArr['eEnrolment']['SubMenuArr']['enrolment_club']['InfoArr'][] = array('SearchTitle' => 'AcademicYearID',
																					'jsSearchType' => 'club_academicyearid',
																					'jsSearchSQL' => "Select * From ".$IntranetDB.".INTRANET_GROUP Where AcademicYearID = '".$this->TargetValueTemplate."' And RecordType = '5';"
																					);
		
		### eEnrolment > Activity
		$MenuArr['eEnrolment']['SubMenuArr']['enrolment_activity']['Title'] = "Activity";
		$MenuArr['eEnrolment']['SubMenuArr']['enrolment_activity']['InfoArr'][] = array('SearchTitle' => 'EnrolEventID',
																						'jsSearchType' => 'enroleventid',
																						'jsSearchSQL' => "Select * From ".$IntranetDB.".INTRANET_ENROL_EVENTINFO Where EnrolEventID = '".$this->TargetValueTemplate."';"
																						);
		$MenuArr['eEnrolment']['SubMenuArr']['enrolment_activity']['InfoArr'][] = array('SearchTitle' => 'Name',
																						'jsSearchType' => 'activity_name',
																						'jsSearchSQL' => "Select * From ".$IntranetDB.".INTRANET_ENROL_EVENTINFO Where EventTitle Like '%".$this->TargetValueTemplate."%';"
																						);
		$MenuArr['eEnrolment']['SubMenuArr']['enrolment_activity']['InfoArr'][] = array('SearchTitle' => 'AcademicYearID',
																						'jsSearchType' => 'activity_academicyearid',
																						'jsSearchSQL' => "Select * From ".$IntranetDB.".INTRANET_ENROL_EVENTINFO Where AcademicYearID = '".$this->TargetValueTemplate."';"
																						);
		$MenuArr['eEnrolment']['SubMenuArr']['enrolment_activity']['InfoArr'][] = array('SearchTitle' => 'EnrolGroupID',
																						'jsSearchType' => 'activity_enrolgroupid',
																						'jsSearchSQL' => "Select * From ".$IntranetDB.".INTRANET_ENROL_EVENTINFO Where EnrolGroupID = '".$this->TargetValueTemplate."';"
																						);
		
		
		### eReportCard
		$MenuArr['eReportCard']['Title'] = 'eReportCard';
		
		### eReportCard > Report
		$MenuArr['eReportCard']['SubMenuArr']['erc_report']['Title'] = "Report";
		$MenuArr['eReportCard']['SubMenuArr']['erc_report']['InfoArr'][] = array(	'SearchTitle' => 'ReportID',
																					'jsSearchType' => 'reportid',
																					'jsSearchSQL' => "Select * From RC_REPORT_TEMPLATE Where ReportID = '".$this->TargetValueTemplate."';"
																					);
		$MenuArr['eReportCard']['SubMenuArr']['erc_report']['InfoArr'][] = array(	'SearchTitle' => 'Name',
																					'jsSearchType' => 'report_name',
																					'jsSearchSQL' => "Select * From RC_REPORT_TEMPLATE Where ReportTitle Like '%".$this->TargetValueTemplate."%';"
																					);
		$MenuArr['eReportCard']['SubMenuArr']['erc_report']['InfoArr'][] = array(	'SearchTitle' => 'Semester',
																					'jsSearchType' => 'report_termid',
																					'jsSearchSQL' => "Select * From RC_REPORT_TEMPLATE Where Semester = '".$this->TargetValueTemplate."';"
																					);
		$MenuArr['eReportCard']['SubMenuArr']['erc_report']['InfoArr'][] = array(	'SearchTitle' => 'ClassLevelID',
																					'jsSearchType' => 'report_classlevelid',
																					'jsSearchSQL' => "Select * From RC_REPORT_TEMPLATE Where ClassLevelID = '".$this->TargetValueTemplate."';"
																					);
		
		### eReportCard > Report Column
		$MenuArr['eReportCard']['SubMenuArr']['erc_report_column']['Title'] = "Report Column";
		$MenuArr['eReportCard']['SubMenuArr']['erc_report_column']['InfoArr'][] = array('SearchTitle' => 'ReportColumnID',
																						'jsSearchType' => 'reportcolumnid',
																						'jsSearchSQL' => "Select * From RC_REPORT_TEMPLATE_COLUMN Where ReportColumnID = '".$this->TargetValueTemplate."';"
																						);
		$MenuArr['eReportCard']['SubMenuArr']['erc_report_column']['InfoArr'][] = array('SearchTitle' => 'Name',
																						'jsSearchType' => 'reportcolumn_name',
																						'jsSearchSQL' => "Select * From RC_REPORT_TEMPLATE_COLUMN Where ColumnTitle Like '%".$this->TargetValueTemplate."%';"
																						);
		$MenuArr['eReportCard']['SubMenuArr']['erc_report_column']['InfoArr'][] = array('SearchTitle' => 'ReportID',
																						'jsSearchType' => 'reportid',
																						'jsSearchSQL' => "Select * From RC_REPORT_TEMPLATE_COLUMN Where ReportID = '".$this->TargetValueTemplate."';"
																						);
		$MenuArr['eReportCard']['SubMenuArr']['erc_report_column']['InfoArr'][] = array('SearchTitle' => 'Semester',
																						'jsSearchType' => 'reportcolumn_semester',
																						'jsSearchSQL' => "Select * From RC_REPORT_TEMPLATE_COLUMN Where SemesterNum = '".$this->TargetValueTemplate."';"
																						);
		
		
		### eReportCard (Rubrics)
		$MenuArr['eReportCardRubrics']['Title'] = 'eReportCard (Rubrics)';
		
		### eReportCard (Rubrics) > Module
		$MenuArr['eReportCardRubrics']['SubMenuArr']['erc_rubrics_module']['Title'] = "Module";
		$MenuArr['eReportCardRubrics']['SubMenuArr']['erc_rubrics_module']['InfoArr'][] = array('SearchTitle' => 'ID',
																								'jsSearchType' => 'module_id',
																								'jsSearchSQL' => "Select * From RC_MODULE Where ModuleID = '".$this->TargetValueTemplate."';"
																								);
		$MenuArr['eReportCardRubrics']['SubMenuArr']['erc_rubrics_module']['InfoArr'][] = array('SearchTitle' => 'Code',
																								'jsSearchType' => 'module_code',
																								'jsSearchSQL' => "Select * From RC_MODULE Where ModuleCode Like '%".$this->TargetValueTemplate."%';"
																								);
		$MenuArr['eReportCardRubrics']['SubMenuArr']['erc_rubrics_module']['InfoArr'][] = array('SearchTitle' => 'Name',
																								'jsSearchType' => 'module_name',
																								'jsSearchSQL' => "Select * From RC_MODULE Where ModuleNameEn Like '%".$this->TargetValueTemplate."%' Or ModuleNameCh Like '%".$this->TargetValueTemplate."%';"
																								);
		
		### eReportCard (Rubrics) > Topic
		$MenuArr['eReportCardRubrics']['SubMenuArr']['erc_rubrics_topic']['Title'] = "Topic";
		$MenuArr['eReportCardRubrics']['SubMenuArr']['erc_rubrics_topic']['InfoArr'][] = array(	'SearchTitle' => 'ID',
																								'jsSearchType' => 'topic_id',
																								'jsSearchSQL' => "Select * From RC_TOPIC Where TopicID = '".$this->TargetValueTemplate."';"
																								);
		$MenuArr['eReportCardRubrics']['SubMenuArr']['erc_rubrics_topic']['InfoArr'][] = array(	'SearchTitle' => 'Code',
																								'jsSearchType' => 'topic_code',
																								'jsSearchSQL' => "Select * From RC_TOPIC Where TopicCode Like '%".$this->TargetValueTemplate."%';"
																								);
		$MenuArr['eReportCardRubrics']['SubMenuArr']['erc_rubrics_topic']['InfoArr'][] = array(	'SearchTitle' => 'Name',
																								'jsSearchType' => 'topic_name',
																								'jsSearchSQL' => "Select * From RC_TOPIC Where TopicNameEn Like '%".$this->TargetValueTemplate."%' Or TopicNameCh Like '%".$this->TargetValueTemplate."%';"
																								);
																								
		### eReportCard (Rubrics) > Report
		$MenuArr['eReportCardRubrics']['SubMenuArr']['erc_rubrics_report']['Title'] = "Report";
		$MenuArr['eReportCardRubrics']['SubMenuArr']['erc_rubrics_report']['InfoArr'][] = array('SearchTitle' => 'ID',
																								'jsSearchType' => 'report_id',
																								'jsSearchSQL' => "Select * From RC_REPORT_TEMPLATE Where ReportID = '".$this->TargetValueTemplate."';"
																								);
		$MenuArr['eReportCardRubrics']['SubMenuArr']['erc_rubrics_report']['InfoArr'][] = array('SearchTitle' => 'Name',
																								'jsSearchType' => 'report_name',
																								'jsSearchSQL' => "Select * From RC_REPORT_TEMPLATE Where ReportTitleEn Like '%".$this->TargetValueTemplate."%' Or ReportTitleCh Like '%".$this->TargetValueTemplate."%';"
																								);
			
		return $MenuArr;
	}
	
	
	function Get_Search_Rows($SearchType, $SearchSubType)
	{
		global $PATH_WRT_ROOT;
		include_once($PATH_WRT_ROOT."library/lib_ui.php");
		include_once($PATH_WRT_ROOT."library/common_function.php");
		$libUI = new lib_ui();
		
		$MenuArr = $this->Get_Left_Menu_Structure_Array();
		$MenuTitle =  $MenuArr[$SearchType]['SubMenuArr'][$SearchSubType]['Title'];
		$MenuInfoArr = $MenuArr[$SearchType]['SubMenuArr'][$SearchSubType]['InfoArr'];
		$numOfRow = count($MenuInfoArr);
		
		$x = '';
		$x .= $this->Get_Header_Row($MenuTitle);
		
		for ($i=0; $i<$numOfRow; $i++)
		{
			$thisTitle 			= $MenuInfoArr[$i]['SearchTitle'];
			$thisJsSearchType 	= $MenuInfoArr[$i]['jsSearchType'];
			$thisTbDefaultValue = $MenuInfoArr[$i]['tbDefaultValue'];
			$thisJsSearchSQL 	= $MenuInfoArr[$i]['jsSearchSQL'];
			
			$thisFocus = ($i==0)? 1 : 0;
			$thisJsSearchTbID 	= $thisJsSearchType.'_tb';
			$thisJsSQLHiddenID = $thisJsSearchType.'_sql';
			
			$this_jsOnkeyup = "js_Check_Quick_Search_Submit(event, '".$thisJsSearchType."', '".$thisJsSearchTbID."')";
			$x .= '<tr>'."\n";
				$x .= '<td width="30%">'.$thisTitle.'</td>'."\n";
				$x .= '<td width="1%">:</td>'."\n";
				$x .= '<td>'."\n";
					$x .= $this->Get_Quick_Search_Textbox($thisJsSearchTbID, $thisTbDefaultValue, '', $this_jsOnkeyup, $thisFocus);
					$x .= '<input type="hidden" id="'.$thisJsSQLHiddenID.'" name="'.$thisJsSQLHiddenID.'" value="'.$thisJsSearchSQL.'" />'."\n";
				$x .= '</td>'."\n";
				$x .= '<td align="right">'."\n";
					$x .= $libUI->Get_Btn('Search', 'button', "js_Submit_Quick_Search('".$thisJsSearchType."', '".$thisJsSearchTbID."');");
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";
		}
		
		return $x;
	}
	
	function Get_Quick_Search_Textbox($Id, $defaultValue='', $Size='', $OnKeyUp='', $focusClass=0)
	{
		if ($Size == '')
			$Size = 20;
		
		$onkeyup_par = '';
		if ($OnKeyUp != '')
			$onkeyup_par = 'onkeyup="'.$OnKeyUp.'"';
			
		$class_focus = '';
		if ($focusClass)
			$class_focus = 'focus_tb';
			
		$x = "<input type='text' class='quick_search_textbox ".$class_focus."' id='".$Id."' name='".$Id."' value='".$defaultValue."' size='".$Size."' ".$onkeyup_par."/>\n"; 
		
		return $x;
	}
	
	function Get_Header_Row($Title)
	{
		$x = '';
		
		# Title
		$x .= '<tr>'."\n";
			$x .= '<td colspan="4" class="quick_search_title">'.$Title.'</td>'."\n";
		$x .= '</tr>'."\n";
		
		# Separator
		$x .= '<tr><td colspan="4"><hr class="quick_search_hr" /></td></tr>'."\n";
		
		return $x;
	}
}