<?php

class lib_ui{

	function lib_ui(){
	}

	function Get_Page_Header($Charset) {
		$header = '';
		$header .= '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">';
		$header .= '<html>'."\n";
			$header .= "\t".'<head>'."\n";
				$header .= "\t\t".'<meta http-equiv="cache-control" content="no-cache">'."\n";
				$header .= "\t\t".'<meta http-equiv="expires" content="0">'."\n";
				$header .= "\t\t".'<meta http-equiv="pragma" content="no-cache" />'."\n";
				$header .= "\t\t".'<meta http-equiv="content-type" content="text/html; charset='.$Charset.'" />'."\n";
				$header .= "\t\t".'<title>eClass SQL Browser - '.Get_School_Name().'</title>'."\n";
			$header .= "\t".'</head>'."\n\n";
			$header .= "\t".'<body>'."\n";
		
		return $header;
	}

	function Get_Page_Footer() {
				$footer = '';
				$footer .= "\t\t".'<div>';
					$footer .= "\t\t\t".'<table width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#999999">'."\n";
						$footer .= "\t\t\t\t".'<tbody>'."\n";
							$footer .= "\t\t\t\t\t".'<tr>'."\n";
					  			$footer .= "\t\t\t\t\t\t".'<td align="center">'."\n";
					  				$footer .= "\t\t\t\t\t\t\t".'<span style="color:#FFFFFF">'."\n";
					  					$footer .= "\t\t\t\t\t\t\t\t".'Copyright &copy; 2009-'.date('Y').' Ivan Ko at Broadlearning Education (Asia) Limited. All rights reserved.'."\n";
					  				$footer .= "\t\t\t\t\t\t\t".'</span>'."\n";
					  			$footer .= "\t\t\t\t\t\t".'</td>'."\n";
					  		$footer .= "\t\t\t\t\t".'</tr>'."\n";
						$footer .= "\t\t\t\t".'</tbody>'."\n";
					$footer .= "\t\t\t".'</table>'."\n";
				$footer .= "\t\t".'</div>'."\n";
			$footer .= "\t".'</body>'."\n";
		$footer .= '</html>'."\n";
		
		return $footer;
	}
	
	function Get_Selection_By_Asso_Array($data, $tags, $selected="", $all=0, $noFirst=0, $FirstTitle="") {
		if(getArrayDepth($data)==1) {
			$data = array(''=>(array)$data);
		}	
			
		global $button_select,$i_status_all,$i_general_NotSet;
		$x = "<SELECT $tags>\n";
		if ($noFirst == 0) {
			$empty_selected = ($selected == '' && trim(strlen($selected)==0))? "SELECTED":"";
			if($FirstTitle=="") {
				if ($all==0) {
					$title = "-- $button_select --";
				}
				else if ($all == 2) {
					$title = "$i_general_NotSet";
				}
				else {
					$title = "$i_status_all";
				}
				//echo $title;
			}
			else {
				$title = $FirstTitle;
			}
	
			$x .= "<OPTION value='' $empty_selected> $title </OPTION>\n";
		}
	
		while($element1 = each($data)) {
			$OptGroupName = $element1['key'];
			$OptionArr = $element1['value'];
			
			if(trim($OptGroupName)!=='')
				$x .= "<optgroup label=\"".$OptGroupName."\">\n";
				
			while($element2 = each($OptionArr)) {
				$tempKey = $element2['key'];
				$tempValue = $element2['value'];
				if(is_array($selected)) {
					$sel_str = (in_array($tempKey,$selected) && isset($selected)? "SELECTED":"");
				}
				else {
					$sel_str = ($selected == $tempKey && $selected!=="" ? "SELECTED":"");
				}
				
				$x .= "<OPTION value='".htmlspecialchars($tempKey,ENT_QUOTES)."' $sel_str $disabled>$tempValue</OPTION>\n";
			}
			
			if(trim($OptGroupName)!='') {
				$x .= "</optgroup>\n";
			}
		}
		$x .= "</SELECT>\n";
		
		return $x;
	}
		
	function Get_Database_Selection($Database, $InputIntranetDB, $InputeClassDB, $InputLmsDB)
	{
		global $PATH_WRT_ROOT, $DefaultSettingsArr, $libDB, $eclass_prefix;
		
		$sql = 'Show Databases';
		$databaseArr = $libDB->returnArray($sql);
		
		//sort($databaseArr);
		
		$numOfDatabase = count($databaseArr);
		$sortedSelectAssoAry = array();
		$eReportCardDbAry = array();
		$classRoomDbAry = array();
		$otherDbAry = array();
		for ($i=0; $i<$numOfDatabase; $i++)
		{
			$thisDB_Name = $databaseArr[$i]['Database'];
			
			# Do not show sales and CRM databases
			$thisSkip = false;
			$numOfForbiddenDB = count($DefaultSettingsArr['Security']['ForbiddenDB']);
			for ($j=0; $j<$numOfForbiddenDB; $j++) {
				# Skip Forbidden Databases
				if (strstr($thisDB_Name, $DefaultSettingsArr['Security']['ForbiddenDB'][$j])) {
					$thisSkip = true;
				}
			}
			if ($thisSkip) {
				continue;
			}
			
			### Check if Classroom DB
			$isClassRoomDb = false;
			if (substr($thisDB_Name, 0, strlen($eclass_prefix)) == $eclass_prefix) {
				$isClassRoomDb = true;
			}
			
			### Check if eReportCard DB and eReportCard(Rubrics) DB
			$reportCardDbPrefix = $InputIntranetDB."_DB_REPORT_CARD_";
			$reportCardRubricsDbPrefix = $InputIntranetDB."_DB_REPORT_CARD_RUBRICS_";
			$isReportCardDb = false;
			$isReportCardRubricsDb = false;
			if (substr($thisDB_Name, 0, strlen($reportCardDbPrefix)) == $reportCardDbPrefix) {
				if (substr($thisDB_Name, 0, strlen($reportCardRubricsDbPrefix)) == $reportCardRubricsDbPrefix) {
					$isReportCardRubricsDb = true;
				}
				else {
					$isReportCardDb = true;
				}
			}
			
			
			### Separate into different array for sorting
			if ($thisDB_Name == $InputIntranetDB) {
				$sortedSelectAssoAry['Basic'][$thisDB_Name] = '[intranet_db] '.$thisDB_Name;
			}
			else if ($thisDB_Name == $InputeClassDB) {
				$sortedSelectAssoAry['Basic'][$thisDB_Name] = '[eclass_db] '.$thisDB_Name;
			}
			else if ($thisDB_Name == $InputLmsDB) {
				$sortedSelectAssoAry['Basic'][$thisDB_Name] = '[lms] '.$thisDB_Name;
			}
			else if ($isReportCardDb) {
				$eReportCardDbAry['eReportCard'][$thisDB_Name] = $thisDB_Name;
			}
			else if ($isReportCardRubricsDb) {
				$eReportCardDbAry['eReportCard (Rubrics)'][$thisDB_Name] = $thisDB_Name;
			}
			else if ($isClassRoomDb) {
				$classRoomDbAry[] = $thisDB_Name;
			}
			else {
				$otherDbAry[] = $thisDB_Name;
			}
		}
		
		$sortedSelectAssoAry = array_merge($sortedSelectAssoAry, $eReportCardDbAry);
		
		natcasesort($classRoomDbAry);
		$classRoomDbAry = array_values($classRoomDbAry);
		$numOfClassRoomDb = count($classRoomDbAry);
		for ($i=0; $i<$numOfClassRoomDb; $i++) {
			$_dbName = $classRoomDbAry[$i];
			
			$sortedSelectAssoAry['Classroom'][$_dbName] = $_dbName;
		}
		
		natcasesort($otherDbAry);
		$otherDbAry = array_values($otherDbAry);
		$numOfOtherDb = count($otherDbAry);
		for ($i=0; $i<$numOfOtherDb; $i++) {
			$_dbName = $otherDbAry[$i];
			
			$sortedSelectAssoAry['Others'][$_dbName] = $_dbName;
		}
		
		return $this->Get_Selection_By_Asso_Array($sortedSelectAssoAry, 'id="Database" name="Database"', $Database, $all=0, $noFirst=1);
	}
	
	function Get_Charset_Selection($Charset='')
	{
		global $SettingArr;
		include_once('common_function.php');
		
		$Charset = ($Charset == '')? $SettingArr['Display']['Charset'] : $Charset;
		$Charset = strtolower($Charset);
		$CharsetArr = Get_Charset_Arr();
		
		$databaseSelection = $this->Get_Selection_By_Asso_Array($CharsetArr, 'id="Charset" name="Charset"', $Charset, $all=0, $noFirst=1);
		
		return $databaseSelection;
	}
	
	function Get_Settings_Table($CurSettingsArr)
	{
		global $PATH_WRT_ROOT, $DefaultSettingsArr;
		
		### Construct Table
		$settingsTable = '';
		$settingsTable .= "<table cellpadding='0' cellspacing='2' border='0' width='100%'>\n";
			$settingsTable .= "<tr>\n";
			$settingsTable .= "<td colspan='100%'><b>[".Get_School_Code()."] ".Get_School_Name()."</b></td>\n";
			$settingsTable .= "</tr>\n";
			
			$settingsTable .= "<tr style='vertical-align:middle'>\n";
				# IP Address
				$settingsTable .= "<td style='width:120px'>\n";
					$settingsTable .= "Your IP: ".Get_Current_IP_Address();
				$settingsTable .= "</td>\n";
				$settingsTable .= "<td style='width:10px'>&nbsp;</td>\n";
				
				# DB Selection
				$settingsTable .= "<td style='width:330px'>\n";
					$settingsTable .= "<label for='Database'>Database: </label>\n";
					$settingsTable .= $this->Get_Database_Selection($CurSettingsArr['Database'], $CurSettingsArr['Database_Intranet'], $CurSettingsArr['Database_eClass'], $CurSettingsArr['Database_LMS']);
				$settingsTable .= "</td>\n";
				$settingsTable .= "<td style='width:10px'>&nbsp;</td>\n";
				
				# Charset Selection
				$settingsTable .= "<td style='width:130px'>\n";
					$settingsTable .= "<label for='Charset'>Charset: </label>\n";
					$settingsTable .= $this->Get_Charset_Selection($CurSettingsArr['Charset']);
				$settingsTable .= "</td>\n";
				$settingsTable .= "<td style='width:10px'>&nbsp;</td>\n";
				
				# Others settings option & Quick-Search Btn
				$settingsTable .= "<td>\n";
					$settingsTable .= "<a class=\"tablelink\" href=\"javascript:js_ShowHide_Layer('DBInfoDiv', null, 'selectbox_layer');\">DB Info</a>\n";
					$settingsTable .= " | \n";
					$settingsTable .= "<a class=\"tablelink\" href=\"javascript:js_ShowHide_Layer('OtherSettingsDiv', null, 'selectbox_layer');\">Other Settings</a>\n";
					$settingsTable .= " | \n";
					$settingsTable .= "<a id='QuickSearchLink' class=\"tablelink\" href=\"src/quick_search/ajax_get_quick_search_layer.php?Database_Intranet=".$CurSettingsArr['Database_Intranet']."\" onclick=\"js_ShowHide_Layer('', 'hide', 'selectbox_layer');\" alt=\"Quick Search\">";
						$settingsTable .= "Quick-Search <span class='remark_grey'>(Ctrl+Alt+Q)</span>";
					$settingsTable .= "</a>\n";
				$settingsTable .= "</td>\n";
				
				# Version Info Btn
				$thisIcon = (date('Y-m-d') <= $DefaultSettingsArr['Display']['NewVerLastDate'])? 'information_new_icon.png' : 'information_icon.png';
				$settingsTable .= "<td align='right'>\n";
					$settingsTable .= "<a id='VersionInfoIcon' class=\"tablelink\" href=\"src/version_info/ajax_get_version_info_div.php\">";
						$settingsTable .= "<img width='20' src='".$PATH_WRT_ROOT."images/".$thisIcon."' alt=\"What's New?\" />";
					$settingsTable .= "</a>\n";
				$settingsTable .= "</td>\n";
			$settingsTable .= "</tr>\n";
		$settingsTable .= "</table>\n";
		
		return $settingsTable;
	}
	
	function Get_DB_Info_Layer($CurSettingsArr)
	{
		### Build layer
		$div = '';
		$div .= '<div id="DBInfoDiv" style="width:350px;float:right;position:absolute;left:630px;top:50px;z-index:99;" class="selectbox_layer">'."\n";
			$div .= "<table cellpadding='2' cellspacing='0' border='0' width='100%'>\n";
				# Close Btn
				$div .= "<tr>\n";
					$div .= "<td style='text-align:left' colspan='3'>\n";
						$div .= "<a class=\"tablelink\" href=\"javascript:js_ShowHide_Layer('DBInfoDiv', null, 'selectbox_layer');\">Close</a>\n";
					$div .= "</td>\n";
				$div .= "</tr>\n";
				
				# SQL User Name
				$div .= "<tr>\n";
					$div .= "<td style='width:30%;'>Username</td>\n";
					$div .= "<td style='width:1%;'>:</td>\n";
					$div .= "<td><input type='text' id='MySQL_UserName' name='MySQL_UserName' value='".$CurSettingsArr['MySQL_UserName']."' onkeyup='js_Check_DB_Info_Submit(event);' /></td>\n";
					$div .= "</td>\n";
				$div .= "</tr>\n";
				
				# SQL Password
				$div .= "<tr>\n";
					$div .= "<td>Password</td>\n";
					$div .= "<td>:</td>\n";
					$div .= "<td><input type='password' id='MySQL_Password' name='MySQL_Password' value='".$CurSettingsArr['MySQL_Password']."' onkeyup='js_Check_DB_Info_Submit(event);' /></td>\n";
					$div .= "</td>\n";
				$div .= "</tr>\n";
				
				# Host
				$div .= "<tr>\n";
					$div .= "<td>Host</td>\n";
					$div .= "<td>:</td>\n";
					$div .= "<td><input type='text' id='MySQL_Host' name='MySQL_Host' value='".$CurSettingsArr['MySQL_Host']."' onkeyup='js_Check_DB_Info_Submit(event);' /></td>\n";
					$div .= "</td>\n";
				$div .= "</tr>\n";
				
				# Intranet DB
				$div .= "<tr>\n";
					$div .= "<td>Intranet DB</td>\n";
					$div .= "<td>:</td>\n";
					$div .= "<td><input type='text' id='Database_Intranet' name='Database_Intranet' value='".$CurSettingsArr['Database_Intranet']."' onkeyup='js_Check_DB_Info_Submit(event);' /></td>\n";
					$div .= "</td>\n";
				$div .= "</tr>\n";
				
				# eClass DB
				$div .= "<tr>\n";
					$div .= "<td>eClass DB</td>\n";
					$div .= "<td>:</td>\n";
					$div .= "<td><input type='text' id='Database_eClass' name='Database_eClass' value='".$CurSettingsArr['Database_eClass']."' onkeyup='js_Check_DB_Info_Submit(event);' /></td>\n";
					$div .= "</td>\n";
				$div .= "</tr>\n";
				
				# LMS DB
				$div .= "<tr>\n";
					$div .= "<td>LMS DB</td>\n";
					$div .= "<td>:</td>\n";
					$div .= "<td><input type='text' id='Database_LMS' name='Database_LMS' value='".$CurSettingsArr['Database_LMS']."' onkeyup='js_Check_DB_Info_Submit(event);' /></td>\n";
					$div .= "</td>\n";
				$div .= "</tr>\n";
				
				$div .= '<tr>'."\n";
					$div .= '<td colspan="3" align="left" valign="top" class="dotline" height="1"><img src="images/10x10.gif" height="2"></td>'."\n";
				$div .= '</tr>'."\n";
				
				# Refresh Button
				$div .= '<tr>'."\n";
					$div .= '<td colspan="3" align="center" style="padding:5px 5px 5px 5px;">'."\n";
						$div .= $this->Get_Btn('Refresh', 'button', "formSubmit('', 0, 0, '');");
					$div .= '</td>'."\n";
				$div .= '</tr>'."\n";
				
			$div .= "</table>\n";
		$div .= '</div>'."\n";
		
		return $div;
	}
	
	function Get_Other_Settings_Layer($CurSettingsArr)
	{
		# keep query input
		$checkedRemainQuery = ($CurSettingsArr['KeepQuery'])? "checked" : "";
		$isRemainQueryHTML = "<input type='checkbox' id='KeepQuery' name='KeepQuery' value='1' $checkedRemainQuery />\n";
		
		# disable jQuery DataTable Plugin
		$checkedDisableDataTable = ($CurSettingsArr['DisableDataTable'])? "checked" : "";
		$disableDataTableOnClick = "onClick='js_Change_Object_Disable_Status(this.checked, \"RepeatHeaderTr\"); js_Change_Object_Disable_Status(this.checked, \"RepeatHeader\"); js_Change_Object_Disable_Status(this.checked, \"RepeatHeaderCount\");'";
		$disableDataTableHTML = "<input type='checkbox' id='DisableDataTable' name='DisableDataTable' value='1' $checkedDisableDataTable $disableDataTableOnClick />\n";
		
		# repeat header input
		$disableRepeatHeaderTr = ($CurSettingsArr['DisableDataTable'])? "" : "disabled='disabled'";
		$checkedRepeatHeader = ($CurSettingsArr['RepeatHeader'])? "checked" : "";
		$disabledRepeatHeaderCount = ($checkedRepeatHeader == '')? "disabled" : "";
		$isRepeatHeaderHTML = "<input type='checkbox' id='RepeatHeader' name='RepeatHeader' value='1' $disableRepeatHeaderTr onclick=\"js_Change_Object_Disable_Status(this.checked, 'RepeatHeaderCount');\" $checkedRepeatHeader />\n";
		$repeatHeaderCountHTML = "<input type='text' class='textboxnum' id='RepeatHeaderCount' name='RepeatHeaderCount' value='".$CurSettingsArr['RepeatHeaderCount']."' size='2' $disabledRepeatHeaderCount />\n"; 
		
		# auto disable jQuery DataTable Plugin
		$checkedAutoDisableDataTable = ($CurSettingsArr['AutoDisableDataTable'])? "checked" : "";
		$disabledAutoDisableDataTableCount = ($checkedAutoDisableDataTable == '')? "disabled" : "";
		$disableAutoDataTableHTML = "<input type='checkbox' id='AutoDisableDataTable' name='AutoDisableDataTable' value='1' onclick=\"js_Change_Object_Disable_Status(this.checked, 'AutoDisableDataTableCount');\" $checkedAutoDisableDataTable />\n";
		$autoDisableDataTableCountHTML = "<input type='text' class='textboxnum' id='AutoDisableDataTableCount' name='AutoDisableDataTableCount' value='".$CurSettingsArr['AutoDisableDataTableCount']."' size='3' $disabledAutoDisableDataTableCount />\n"; 
		
		
		### Build layer
		$div = '';
		$div .= '<div id="OtherSettingsDiv" style="width:350px;float:right;position:absolute;left:675px;top:50px;z-index:99;" class="selectbox_layer">'."\n";
			$div .= "<table cellpadding='2' cellspacing='0' border='0' width='100%'>\n";
				# Close Btn
				$div .= "<tr>\n";
					$div .= "<td style='text-align:left'>\n";
						$div .= "<a class=\"tablelink\" href=\"javascript:js_ShowHide_Layer('OtherSettingsDiv', null, 'selectbox_layer');\">Close</a>\n";
					$div .= "</td>\n";
				$div .= "</tr>\n";
				
				# Keep Query
				$div .= "<tr>\n";
					$div .= "<td>\n";
						$div .= $isRemainQueryHTML."<label for='KeepQuery'> Keep Query in Textarea</label>\n";
					$div .= "</td>\n";
				$div .= "</tr>\n";
				
				# Disable DataTable
				$div .= "<tr>\n";
					$div .= "<td>\n";
						$div .= $disableDataTableHTML."<label for='DisableDataTable'> Disable DataTable Plugin</label>\n";
					$div .= "</td>\n";
				$div .= "</tr>\n";
				
				# Repeat Header
				$div .= "<tr id='RepeatHeaderTr' $disableRepeatHeaderTr>\n";
					$div .= "<td>\n";
						$div .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
						$div .= $isRepeatHeaderHTML."<label for='RepeatHeader'> Repeat Header for each </label>\n";
						$div .= $repeatHeaderCountHTML."<label for='RepeatHeader'> row(s)</label>\n";
					$div .= "</td>\n";
				$div .= "</tr>\n";
				
				
				# Auto Disable DataTable
				$div .= "<tr>\n";
					$div .= "<td>\n";
						$div .= $disableAutoDataTableHTML."<label for='AutoDisableDataTable'> Auto disable DataTable if there are more than </label>\n";
						$div .= $autoDisableDataTableCountHTML."<label for='AutoDisableDataTable'> row(s)</label>\n";
					$div .= "</td>\n";
				$div .= "</tr>\n";
				
				$div .= "<tr><td>&nbsp;</td></tr>";
				
			$div .= "</table>\n";
		$div .= '</div>'."\n";
		
		return $div;
	}
	
	function Get_Short_Cut_Table($CurSettingsArr)
	{
		include_once('common_function.php');
		
		$shortCurArr = Get_Short_Cut_Arr($CurSettingsArr);
		$numOfShortCut = count($shortCurArr);
		
		$shortCutTable = '';
		$shortCutTable .= "<table cellpadding='0' cellspacing='2' border='0' align='center' class='BlueTable' style='width:90%;'>\n";
			$shortCutTable .= "<thead>\n";
				$shortCutTable .= "<tr>\n";
					$shortCutTable .= "<th colspan='2'>Short Cut</th>\n";
				$shortCutTable .= "</tr>\n";
			$shortCutTable .= "</thead>\n";
			
			$shortCutTable .= "<tbody align='center'>\n";
				for($i=0; $i<$numOfShortCut; $i++)
				{
					$thisRowNumDisplay = $i + 1;
					$thisDisplay = $shortCurArr[$i]['Display'];
					$thisJS = $shortCurArr[$i]['JS'];
					$thisTrCss = ($i % 2 == 0)? '' : 'class="odd"';
					
					$shortCutTable .= "<tr $thisTrCss>\n";
						$shortCutTable .= "<td class='counter_text'>\n".$thisRowNumDisplay."</td>\n";
						$shortCutTable .= "<td><a href=\"javascript: ".$thisJS."\">\n".$thisDisplay."</a></td>\n";
					$shortCutTable .= "</tr>\n";
				}
			$shortCutTable .= "</tbody>\n";
		$shortCutTable .= "</table>\n";
		
		return $shortCutTable;
	}
	
	function Get_SQL_Result_Table($Sql, $CurSettingsArr, $tableNum)
	{
		global $DefaultSettingsArr, $libDB;
		
		include_once('common_function.php');
		$action = Get_SQL_Action($Sql);
		
		if (Is_SQL_Update_Action($Sql))
		{
			### Check if the user can update the database 
			$CanUpdateDB = true;
			if ($CurSettingsArr['OnTheFlyCanUpdate']) {
				$CanUpdateDB = true;
			}
			else if ($CurSettingsArr['Security']['CheckPermission']['UpdateDB'] == 1 && !in_array(Get_Current_IP_Address(), (array)$DefaultSettingsArr['Security']['AuthorizedIP']))
			{
				$TableHTML .= "<b><font color='red'>Sorry. You are not allowed to update the database in this site.</font></b>";
				$CanUpdateDB = false;
			}
			
			if ($CanUpdateDB == true) {
				$resultSet = $libDB->db_db_query($Sql);
				$errorMsg = mysql_error();
				
				$TableHTML = '';
				if ($errorMsg != '') {
					$TableHTML .= $this->Get_SQL_Error_Display_Table($errorMsg);
				}
				else {
					$executionTime = $libDB->Get_Execution_Time();
					$TableHTML .= $this->Get_UpdateAction_Result_Table($action, $executionTime, mysql_info(), $libDB->db_affected_rows());
				}
			}
				
			$numOfResult = 0;
		}
		else
		{
			$resultSetArr = $libDB->returnAssoArray($Sql);
			$errorMsg = mysql_error();
			
			$TableHTML = '';
			if ($errorMsg != '')
			{
				$TableHTML .= $this->Get_SQL_Error_Display_Table($errorMsg);
				$numOfResult = 0;
			}
			else
			{
				$executionTime = $libDB->Get_Execution_Time();
				list($tmp_TableHTML, $numOfResult) = $this->Get_Result_Set_Table($resultSetArr, $action, $executionTime, $CurSettingsArr, $tableNum, $Sql);
				$TableHTML .= $tmp_TableHTML;
			}
		}
		
		return array($TableHTML, $numOfResult);
	}
	
	function Get_UpdateAction_Result_Table($Action, $ExecutionTime, $MySQL_Info, $AffectedRow)
	{
		if ($Action == 'use') {
			$thisDisplay = 'Database changed';
		}
		else {
			if ($Action == 'alter') {
				$thisDisplay = $MySQL_Info;
			}
			else {
				$thisDisplay = $AffectedRow.' rows affected';
			}
			$thisDisplay .= ' in '.$ExecutionTime.' sec';
		}
		
		$updateInfoTable = '';
		$updateInfoTable .= '<table class="ResultSetTable" cellpadding="2" cellspacing="2" border="1" width="100%" >'."\n";
			$updateInfoTable .= '<tr>';
				$updateInfoTable .= '<td style="width:160px">'.$thisDisplay.'</td>'."\n";
			$updateInfoTable .= '</tr>';
		$updateInfoTable .= '</table>';
		
		return $updateInfoTable;
	}
	
	function Get_SQL_Error_Display_Table($ErrorMsg)
	{
		$TableHTML = '';
		$TableHTML .= '<table cellpadding="2" cellspacing="2" border="1" class="ResultSetTable">'."\n";
			$TableHTML .= '<tr><td style="color:red; font-weight:bold;">'.$ErrorMsg.'</td></tr>'."\n";
		$TableHTML .= '</table>'."\n";
		
		return $TableHTML;
	}
	
	function Get_History_Table($Title, $HistoryArr)
	{
		$HistoryLink = '';
		$HistoryLink .= "<a id='DateHistoryLink' class=\"tablelink\" href=\"src/date_history/ajax_get_date_history_layer.php\" style='float:right;'>";
			$HistoryLink .= "(More...)";
		$HistoryLink .= "</a>\n";

		$numOfHistory = count($HistoryArr);
		$counter = 0;
		$x = '';
		
		$x .= "<table cellpadding='0' cellspacing='2' border='0' class='BlueTable' style='width:100%;'>\n";
			$x .= "<thead>\n";
				$x .= "<tr>\n";
					$x .= "<th colspan='2'>\n";
						$x .= '<span style="float:left;width:110px;">&nbsp;</span>'."\n";
						$x .= '<span style="float:left;">History</span>'."\n";
						$x .= '<span style="float:right;">'.$HistoryLink.'</span>'."\n";
					$x .= "</th>\n";
				$x .= "</tr>\n";
			$x .= "</thead>\n";
			
			$x .= "<tbody>\n";
				for ($i=0; $i<$numOfHistory; $i++)
				{
					$thisSQL = trim($HistoryArr[$i]);
					
					if ($thisSQL == "")
						continue;
						
					$tr_css = ($i % 2 == 0)? "" : "class='odd'";
						
					$x .= "<tr $tr_css>\n";
						$x .= "<td class='counter_text'><a title=\"Copy to SQL textarea\" href=\"javascript: copyHistoryToTextarea(".$i.");\">".++$counter."</a>&nbsp;</td>\n";
						$x .= '<td><a href="javascript: formSubmit(\'\', 0, '.$i.');">'. nl2br($thisSQL) .'</a></td>'."\n";
					$x .= "</tr>\n";
				}
			$x .= "</tbody>";
		$x .= "</table>";
		
		return $x;
	}
	
	// TargetDate = yyyy-mm-dd
	function Get_Date_History_Table($TargetDate)
	{
		global $PATH_WRT_ROOT, $DefaultSettingsArr;
				
		include_once($PATH_WRT_ROOT."library/libdb.php");
		include_once($PATH_WRT_ROOT."library/libimport.php");
		$libimport = new libimport();
		
		// use original settings charset as one csv file cannot cater both charset
		$Charset = $DefaultSettingsArr['Display']['Charset'];
		
		$TargetDate = str_replace('-', '', $TargetDate);
		$TargetCsvFile = $TargetDate.'.csv';
		$TargetLogFilePath = $DefaultSettingsArr['Log']['FilePath'].$TargetDate.'/'.$TargetCsvFile;
		
		
		### Get Sql Log and filter other users' Sql
		$HistorySqlArr = array();
		if (is_file($TargetLogFilePath))
		{
			if ($Charset == 'utf-8')
				$HistorySqlArr = $libimport->GET_IMPORT_TXT($TargetLogFilePath);
			else if ($Charset == 'big5')
				$HistorySqlArr = $libimport->GET_IMPORT_TXT_BIG5($TargetLogFilePath);
				
			$HeaderArr = array_shift($HistorySqlArr);
			$numOfHistory = count($HistorySqlArr);
			for ($i=0; $i<$numOfHistory; $i++)
			{
				$thisIP = $HistorySqlArr[$i][1];
				$thisSQL = trim($HistorySqlArr[$i][2]);
				
				if ($thisIP != Get_Current_IP_Address() || $thisSQL == '')
					unset($HistorySqlArr[$i]);
			}
			$HistorySqlArr = array_values($HistorySqlArr);
		}
		$numOfHistory = count($HistorySqlArr);
		$HistorySqlArr = array_reverse($HistorySqlArr);
		
		
		$x = '';
		$x .= '<table cellpadding="0" cellspacing="2" border="0" class="BlueTable" style="width:100%;">'."\n";
			$x .= '<thead>'."\n";
				$x .= '<tr>'."\n";
					$x .= '<th>#</th>'."\n";
					$x .= '<th style="text-align:left;">Time</th>'."\n";
					$x .= '<th style="text-align:left;">Database</th>'."\n";
					$x .= '<th style="text-align:left;">SQL</th>'."\n";
				$x .= '</tr>'."\n";
			$x .= '</thead>'."\n";
			
			$x .= '<tbody>'."\n";
				if ($numOfHistory == 0)
				{
					$x .= '<tr><td colspan="4" style="text-align:center;">There are no queries records for this date.</td></tr>'."\n";
				}
				else
				{
					for ($i=0; $i<$numOfHistory; $i++)
					{
						$thisTime = trim($HistorySqlArr[$i][0]);
						$thisDatabase = trim($HistorySqlArr[$i][2]);
						$thisSQL = trim($HistorySqlArr[$i][3]);
						
						$tr_css = ($i % 2 == 0)? "" : "class='odd'";
						
						$x .= '<tr '.$tr_css.'>'."\n";
							$x .= '<td class="counter_text">'.($i + 1).'&nbsp;</td>'."\n";
							$x .= '<td style="vertical-align:top">'.$thisTime.'</td>'."\n";
							$x .= '<td style="vertical-align:top">'.$thisDatabase.'</td>'."\n";
							$x .= '<td style="vertical-align:top"><a href="javascript: formSubmit(standardizeQuery(\''.str_replace("'", "\'", $thisSQL).'\'), 0, \'\', \''.$thisDatabase.'\');">'. nl2br(standardizeQuery($thisSQL)) .'</a></td>'."\n";
						$x .= '</tr>'."\n";
					}
				}
			$x .= '</tbody>'."\n";
		$x .= '</table>'."\n";
		
		return $x;
	}
	
	
	function Get_Result_Set_Table($ResultSetArr, $Action, $ExecutionTime, $CurSettingsArr, $tableNum, $TableSql)
	{
		global $PATH_WRT_ROOT;
		
		$resultInfoTable = '';
		$resultSetTable = '';
		$titleRow = '';
		$titleRowCopy = '';
		$rowCounter = 0;
		$numOfRows = count($ResultSetArr);
		$numOfRowsDigit = strlen($numOfRows);
		
		$disableDataTable = $CurSettingsArr['DisableDataTable'];
		
		$replaceFromAry = array("\n", "\r", "'", '"');
		$replaceToAry = array(" ", " ", "\'", urlencode('"'));
		
		
		# Display total number of records if there are any
		if ($numOfRows > 0)
		{
			$resultInfoTable .= '<table cellpadding="0" cellspacing="0" border="0" width="100%">'."\n";
				$resultInfoTable .= '<tr>';
					$resultInfoTable .= '<td style="width:160px">'.$numOfRows.' rows in set ('.$ExecutionTime.' sec)</td>'."\n";
					$resultInfoTable .= '<td><a class="tablelink" href="javascript:formSubmit(\''.str_replace($replaceFromAry, $replaceToAry, $TableSql).'\', 1, 0);">[Export]</a></td>'."\n";
				$resultInfoTable .= '</tr>';
			$resultInfoTable .= '</table>';
			$resultInfoTable .= '<br style="clear:both" />';
		}
		
		$resultSetTable .= '<table id="ResultSetTable_'.$tableNum.'" cellpadding="2" cellspacing="2" border="1" class="display ResultSetTable">'."\n";
		
		if ($numOfRows == 0)
		{
			$resultSetTable .= '<tr><td>No Matched Result</td></tr>'."\n";
		}
		else
		{
			if ($CurSettingsArr['AutoDisableDataTable'] && $numOfRows > $CurSettingsArr['AutoDisableDataTableCount']) {
				$disableDataTable = 1;
			}
			
			### Build Table Header
			$firstResultArr = $ResultSetArr[0];
			$HeaderTitleArr = array_keys($firstResultArr);
			
			$titleRow .= '<tr>'."\n";
			$titleRowCopy .= '<tr>'."\n";
				$titleRow .= '<th style="width:20px; text-align:center;">#</th>'."\n";
				$titleRowCopy .= '<th style="width:20px; text-align:center;">#</th>'."\n";
				
				$columnCount = 0;
				foreach ($HeaderTitleArr as $HeaderTitle) {
					$titleRow .= '<th>'."\n";
					$titleRowCopy .= '<th>'."\n";
						$titleRow .= trim($HeaderTitle);
						$titleRowCopy .= trim($HeaderTitle);
						
						//if ($disableDataTable == false && $numOfRows > 1)
						if ($disableDataTable == false)
						{
							$titleRow .= '<br />';
							$titleRow .= "<input type=\"text\" value=\"\" class=\"search_init\" />\n";
						}
						$titleRow .= '<br /><a href="javascript:void(0);" onclick="createListToTemp('.$tableNum.', '.$columnCount.');"><img width="10" border="0px" src="'.$PATH_WRT_ROOT.'images/comma.png" title="Create a list of this column and append to the Temp textarea" /></a>';
						
					$titleRow .= '</th>'."\n";
					$titleRowCopy .= '</th>'."\n";
					
					$columnCount++;
				}
				
				# An extra column to provide short cut to the user for select * / desc / show create table execution
				if ($Action == 'show tables')
				{
					$titleRow .= '<th>'.'Action'.'</th>'."\n";
					$titleRowCopy .= '<th>'.'Action'.'</th>'."\n";
				}
					
			$titleRow .= '</tr>'."\n";
			$titleRowCopy .= '</tr>'."\n";
			
			# Put the header in the result table
			$resultSetTable .= '<thead>'.$titleRow.'</thead>'."\n";
			
			
			### Build Table Content
			$rowCounter = 0;
			$resultSetTable .= '<tbody>'."\n";
			foreach ($ResultSetArr as $index => $RowDataArr)
			{
				$thisResultTableRow = '';
				$skipRow = 0;
				
				### Add left zeros for DataTable sorting
				if ($disableDataTable == false)
					$thisRowDisplay = str_pad($rowCounter + 1, $numOfRowsDigit, 0, STR_PAD_LEFT);
				else
					$thisRowDisplay = $rowCounter + 1;
		
				$thisResultTableRow .= '<td style="text-align:center"><b>'.$thisRowDisplay.'</b></td>'."\n";
				
				if ($Action == 'show tables')
				{
					foreach($RowDataArr as $key => $value){
						$thisTableName = trim($value);
						$thisOptionArr = array();
						
						# Select * Option
						$thisSelectAllCommand = "Select * From ".$thisTableName;
						$thisOptionArr[] = '<a href="javascript: formSubmit(\''.$thisSelectAllCommand.'\', 0);" title="'.$thisSelectAllCommand.'">'. 'Select * ' .'</a>'."\n";
						
						# Select * Limit 100 Option
						$thisSelectLimitCommand = "Select * From ".$thisTableName." Limit 100";
						$thisOptionArr[] = '<a href="javascript: formSubmit(\''.$thisSelectLimitCommand.'\', 0);" title="'.$thisSelectLimitCommand.'">'. 'Limit 100' .'</a>'."\n";
						
						# Select * Limit Desc 100 Option
						$thisSelectLimitCommand = "Select * From ".$thisTableName." Order By {{{PrimaryKey}}} Desc Limit 100";
						$thisOptionArr[] = '<a href="javascript: formSubmit(\''.$thisSelectLimitCommand.'\', 0);" title="'.$thisSelectLimitCommand.'">'. 'Recent 100' .'</a>'."\n";
						
						# Select Count(*) Option
						$thisSelectCountCommand = "Select Count(*) From ".$thisTableName;
						$thisOptionArr[] = '<a href="javascript: formSubmit(\''.$thisSelectCountCommand.'\', 0);" title="'.$thisSelectCountCommand.'">'. 'Count(*)' .'</a>'."\n";
						
						# Desc Option
						$thisDescCommand = "desc ".$thisTableName;
						$thisOptionArr[] = '<a href="javascript: formSubmit(\''.$thisDescCommand.'\', 0);" title="'.$thisDescCommand.'">'. 'Desc' .'</a>'."\n";
						
						# Show Create Table Option
						$thisShowCreateCommand = "Show Create Table ".$thisTableName;
						$thisOptionArr[] = '<a href="javascript: formSubmit(\''.$thisShowCreateCommand.'\', 0);" title="'.$thisShowCreateCommand.'">'. 'Show Create Table ' .'</a>'."\n";
						
						# Backup SQL
						$thisBackUpSql = 'Create Table '.$thisTableName.'_'.date('Ymd').' Select * From '.$thisTableName.';';
						$thisOptionArr[] = '<a href="javascript: js_Set_TextArea_Text(js_Get_SQL_Value() + standardizeQuery(\'<!--newLineWin-->'.$thisBackUpSql.'\'));" title="'.$thisBackUpSql.'">'. 'Backup SQL ' .'</a>'."\n";
						
						# Display
						$thisDisplayOption = implode(' <b>|</b> ', $thisOptionArr);
						
						//$thisResultTableRow .= '<td>'.$thisTableName.'&nbsp;</td>'."\n";
						//$thisResultTableRow .= '<td align="center">'.$thisDisplayOption.'&nbsp;</td>'."\n";
						$thisResultTableRow .= '<td><a href="javascript: js_Set_TextArea_Text(\''.$thisTableName.'\');" title="Copy table name to SQL textarea">'.$thisTableName.'</a></td>'."\n";
						$thisResultTableRow .= '<td align="center">'.$thisDisplayOption.'</td>'."\n";
					}
				}
				else if ($Action == 'show create table')
				{
					$counter = 0;
					foreach($RowDataArr as $key => $value){
						$counter++;
						
						if ($counter == 2)
						{
							# sql statement
							$thisSQL_Arr = explode("\n", $value);
							$numOfLine = count($thisSQL_Arr);
							
							$thisDisplay = '';
							for ($i=0; $i<$numOfLine; $i++)
							{
								$thisSQL_Line = $thisSQL_Arr[$i];
								
								## Add a tab for the middle lines
								if ( ($i != 0) && ($i != $numOfLine-1) )
									$thisSQL_Line = "&nbsp;&nbsp;&nbsp;&nbsp;".$thisSQL_Line;
									
								$thisDisplay .= $thisSQL_Line."<br />\n";
							}
							
							$thisDisplay = str_replace("`", "", $thisDisplay);
						}
						else
						{
							$thisDisplay = $value;
						}
						
						$thisResultTableRow .= '<td>'.$thisDisplay.'</td>'."\n";
					}
				}
				else if ($Action == 'show databases')
				{
					foreach($RowDataArr as $key => $value){
					
						# Do not show sales and CRM databases
						if (strstr($value, 'support') || strstr($value, 'sales'))
							$skipRow = 1;
						
						$value = ($value=="")? "&nbsp;" : trim($value);
						$thisResultTableRow .= '<td>'.$value.'</td>'."\n";
					}
				}
				else
				{
					foreach($RowDataArr as $key=>$value){
						if (is_null($value))
							$value = '<i>NULL</i>';
						else if ($value == "")
							$value = "&nbsp;";
						else
							$value = trim(intranet_htmlspecialchars($value));
							
						$thisResultTableRow .= '<td>'.$value.'</td>'."\n";
					}
				}
				
				if (!$skipRow)
				{
					$rowCounter++;
					$tr_css = ($rowCounter % 2 == 0)? "" : "class='odd'";
					$resultSetTable .= '<tr '.$tr_css.' style="vertical-align:top;">'."\n";
						$resultSetTable .= $thisResultTableRow;
					$resultSetTable .= '</tr>'."\n";
				}
				
				if ( ($rowCounter % $CurSettingsArr['RepeatHeaderCount'] == 0) && $CurSettingsArr['RepeatHeader'])
				{
					$resultSetTable .= $titleRowCopy;
				}
			}
			$resultSetTable .= '</tbody>'."\n";
			
			/*
			if ($disableDataTable == false)
			{
				### footer filters
				$resultSetTable .= '<tfoot>'."\n";
				
					$resultSetTable .= '<th>&nbsp;</th>'."\n";
					
					foreach ($HeaderTitleArr as $HeaderTitle)
							$resultSetTable .= '<th><input type="text" value="" class="search_init" /></th>'."\n";
							
					# An extra column to provide short cut to the user for select * / desc / show create table execution
					if ($Action == 'show tables')
						$resultSetTable .= '<th>&nbsp;</th>'."\n";
						
				$resultSetTable .= '</tfoot>'."\n";
			}
			*/
		}
		
		$resultSetTable .= '</table>'."\n";
		
		return array($resultInfoTable.$resultSetTable, $numOfRows);
	}
	
	function Get_Current_SQL_Info_Div($SQL, $lastSql)
	{
		$div = '';
		$div .= '<div id="sql_info_div" style="float:left;">';
			$div .= '<div id="sql_info_inner_div" style="float:left;">';
			
				$div .= '<b>Current SQL: </b><br /> '.$SQL.'<br />';
				$div .= '<br />';
				
				if ($lastSql != '')
				{
					$div .= '<b>Last SQL: </b>';
					$div .= '<br />';
					$div .= '<a href="javascript:formSubmit(\'\', 0, 1);">'.$lastSql.'</a>';
					$div .= '<br />';
					$div .= '<br />';
				}
				
			$div .= '</div>';
		$div .= '</div>';
		
		return $div;
	}
	
	function Get_SQL_TextArea($ID_Name, $CurSettingsArr, $Sql='', $IsTemp=0, $Rows='')
	{
		$thisDisplay = '';
		
		if ((isset($CurSettingsArr['KeepQuery']) && $CurSettingsArr['KeepQuery']) || $IsTemp)
			$thisDisplay = $Sql;
			
		$Cols = ($Cols=='')? '135' : $Cols;
		$Rows = ($Rows=='')? '8' : $Rows;
			
		$queryTextArea = "";
		$queryTextArea .= '<textarea rows="'.$Rows.'" name="'.$ID_Name.'" id="'.$ID_Name.'" style="width:100%;">'.$thisDisplay.'</textarea>'."\n";
		
		return $queryTextArea;
	}
	
	function Get_Back_To_Top_Table()
	{
		$backToTopTable = "";
		$backToTopTable .= "<table width='100%' cellpadding='0' cellspacing='2' border='0'>\n";
			$backToTopTable .= "<tr>\n";
				$backToTopTable .= "<td align='left'>\n";
					$backToTopTable .= "<a class='tablelink' href='#' onclick='backToTop();'>Back to Top <span class='remark_grey'>(Ctrl+Alt+T)</span></a>\n";
				$backToTopTable .= "</td>\n";
				$backToTopTable .= "<td align='right'>\n";
					$backToTopTable .= "<a class='tablelink' href='#' onclick='backToTop();'> <span class='remark_grey'>(Ctrl+Alt+T)</span> Back to Top</a>\n";
				$backToTopTable .= "</td>\n";
			$backToTopTable .= "</tr>\n";
		$backToTopTable .= "</table>\n";
		
		return $backToTopTable;
	}
	
	function Get_Btn($ParTitle, $ParType, $ParOnClick="", $ParName="", $ParOtherAttribute="")
	{
		$btnClick = ($ParOnClick!="") ? "onclick=\"$ParOnClick\"" : "";
		$ParName = ($ParName!="") ? "name=\"$ParName\" id=\"$ParName\"" : "";
		
		$rx = "<input type=\"$ParType\" class=\"formsmallbutton\" $btnClick $ParName value=\"$ParTitle\" ";
		$rx .= " $ParOtherAttribute ";
		$rx .= "onMouseOver=\"this.className='formsmallbuttonon'\" onMouseOut=\"this.className='formsmallbutton'\"/>";

		return $rx;
	}
	
	function GET_DATE_PICKER($ID_Name, $DefaultValue="", $OnDatePickSelectedFunction="") {
		global $Defined, $PATH_WRT_ROOT, $image_path, $LAYOUT_SKIN, $Lang;
		
		$DateFormat = ($DateFormat == "")? "yy-mm-dd":$DateFormat;
		$DefaultValue = ($DefaultValue == "")? date('Y-m-d'):$DefaultValue;
				
		$x = "";
		$x .= '<input type=text name="'.$ID_Name.'" id="'.$ID_Name.'" value="'.$DefaultValue.'" size=10 maxlength=10 class="textboxdate">';
		
		$x .= "\n";
		if ($Defined<1) {
			$x .= '<script type="text/javascript" src="js/jquery/jquery.datepick.js"></script>
						<link rel="stylesheet" href="css/jquery.datepick.css" type="text/css" />
						<script>
							$.datepick.setDefaults({
								showOn: \'both\', 
								buttonImageOnly: true, 
								buttonImage: \'images/icon_calendar_off.gif\', 
								buttonText: \'Calendar\'});
						</script>
						';
		}
		$x .= "<script>
						$('input#".$ID_Name."').ready(function(){ 
							$('input#".$ID_Name."').datepick({
									dateFormat: '".$DateFormat."',
									dayNamesMin: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
									changeFirstDay: false,
									firstDay: 0,
									onSelect: function() {
										".$OnDatePickSelectedFunction."
									}
								});
							});
					</script>
					";
		
		$Defined++;
		return $x;
	}
	
	function Get_Ajax_Loading_Image()
	{
		return '<img src="/images/indicator.gif"> <span class="tabletextremark">Loading...</span>';
	}
	
}