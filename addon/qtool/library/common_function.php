<?php

function intranet_undo_htmlspecialchars($string){
	$string=str_replace("&amp;", "&", $string);
	$string=str_replace("&quot;", "\"", $string);
	
	$string=str_replace("&lt;", "<", $string);
	$string=str_replace("&gt;", ">\n", $string);
	$string=str_replace("&#039;","'",$string);
	
	return $string;
}

function intranet_htmlspecialchars($string)
{
	global $CurSettingsArr;
	
	$charset = $CurSettingsArr['Charset'];
	if ($charset == 'big5') {
		$string = htmlspecialchars($string,ENT_COMPAT | ENT_HTML401, 'BIG5-HKSCS');
	}
	else {
		$string = htmlspecialchars($string);
	}
	
	$string = str_replace("&amp;#", "&#", $string);
	$string = str_replace("'","&#039;",$string);
	return $string;
}

## Debug messages
function debug_r($arr) {
        echo "<i>[debug]</i>\n<PRE>\n";
        print_r($arr);
        echo "\n</PRE>\n<br>\n";
}

function hdebug_r($arr) {
        echo "<!--\n[debug]\n";
        print_r($arr);
        echo "\n-->\n";
}

function debug_pr($arr) {
        echo "<i>[debug]</i>\n<PRE>\n";
        print_r($arr);
        echo "\n</PRE>\n<br>\n";
}

function hdebug_pr($arr) {
        echo "<!--\n[debug]\n";
        print_r($arr);
        echo "\n-->\n";
}

function Check_Permission($CurSettingsArr) {
	global $DefaultSettingsArr, $PHP_AUTH_USER, $PHP_AUTH_PW;
	
	$isPinValid = false;
	if ($CurSettingsArr['Security']['CheckPermission']['ByPassLoginPin1'] != '' && $CurSettingsArr['Security']['CheckPermission']['ByPassLoginPin2'] != '') {
		if (checkValidPin1($CurSettingsArr['Security']['CheckPermission']['ByPassLoginPin1']) && checkValidPin2($CurSettingsArr['Security']['CheckPermission']['ByPassLoginPin2'])) {
			$isPinValid = true;
		}
	}
	
	if (!$isPinValid && $CurSettingsArr['OnTheFlyByPassLogin']) {
		header('Location: src/pinLogin/list.php?able2View='.$CurSettingsArr['OnTheFlyCanView'].'&able2Update='.$CurSettingsArr['OnTheFlyCanUpdate']);
	}
	else if ($isPinValid || $CurSettingsArr['Security']['CheckPermission']['ByPassLogin']) {
		// do nth
	}
	else {
		# get the password from central server
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_REFERER, $_SERVER["SERVER_NAME"]);
		@curl_setopt($ch, CURLOPT_URL, "http://eclassupdate.broadlearning.com/api/blpswd.php?ptype=3");
					
		// grab URL and pass it to the browser
		$password_central = trim(curl_exec($ch));
		curl_close ($ch);
		
		if ($password_central!="" && strlen($password_central)>3) {
			if(!isset($_SERVER['PHP_AUTH_USER']) || $_SERVER['PHP_AUTH_USER']!="broadlearning" || md5("2012allschools".$_SERVER['PHP_AUTH_PW'])!=$password_central) {
				$realm = "eClass SQL Browser ( ".strftime("%X %Z",time())." )";
				
				Header("WWW-Authenticate: Basic realm=\"".$realm."\"");
				Header("HTTP/1.0 401 Unauthorized");
				echo "Unauthorized Access\n";
				exit;
			}
		}
		else {
			echo 'Failed to login central account.';
			die();
		}
	}
	
	if ($isPinValid || $CurSettingsArr['Security']['CheckPermission']['ByPassLogin']) {
		// skip IP address checking
	}
	else {
		if (in_array(Get_Current_IP_Address(), $DefaultSettingsArr['Security']['AuthorizedIP']) == false) {
			echo "Sorry. [".Get_Current_IP_Address()."] You are not registered.";
			die();
		}
	}
}

function Get_Current_IP_Address() {
	$ipAddress = '';
	if (isset($_SERVER["HTTP_X_REAL_IP"]) && !empty($_SERVER["HTTP_X_REAL_IP"])) {
		$ipAddress = $_SERVER["HTTP_X_REAL_IP"];
	}
	else {
		$ipAddress = $_SERVER["REMOTE_ADDR"];
	}
	
	return $ipAddress;
}

function Get_Charset_Arr()
{
	$CharsetArr = array("utf-8" => "UTF-8",
						"big5" => "Big5");
	return $CharsetArr;
}

function Get_Short_Cut_Arr($CurSettingsArr)
{
	$shortCurArr = array();
	
	$shortCurArr[] = array("Display" => "Show Databases", "JS" => "formSubmit('Show Databases', 0, '');");
	$shortCurArr[] = array("Display" => "Show Tables", "JS" => "formSubmit('Show Tables', 0, '');");
	$shortCurArr[] = array("Display" => "Show Tables Like", "JS" => "js_Set_TextArea_Text('Show Tables Like \'%%\'');");
	$shortCurArr[] = array("Display" => "Show Create Table", "JS" => "js_Set_TextArea_Text('Show Create Table TableName');");
	$shortCurArr[] = array("Display" => "Select *", "JS" => "js_Set_TextArea_Text('Select * From TableName');");
	$shortCurArr[] = array("Display" => "Insert Into", "JS" => "js_Set_TextArea_Text('INSERT INTO TableName (### Field List ###) Values (### Value List ###)');");
	$shortCurArr[] = array("Display" => "Update", "JS" => "js_Set_TextArea_Text('UPDATE TableName Set FieldName = \'Value\' Where FieldName = \'Value\' ');");
	$shortCurArr[] = array("Display" => "Delete", "JS" => "js_Set_TextArea_Text('DELETE FROM TableName WHERE FieldName = \'Value\' ');");
	$shortCurArr[] = array("Display" => "Show Full ProcessList", "JS" => "formSubmit('Show Full ProcessList', 0, '');");
	$shortCurArr[] = array("Display" => "eRC Student Subject Mark", "JS" => "js_Set_TextArea_Text('Select * From RC_REPORT_RESULT_SCORE Where ReportID=\'\' And SubjectID=\'\' And StudentID=\'\';');");
	$shortCurArr[] = array("Display" => "eRC Student Grand Mark", "JS" => "js_Set_TextArea_Text('Select * From RC_REPORT_RESULT Where ReportID=\'\' And StudentID=\'\';');");
	$shortCurArr[] = array("Display" => "Check Active & Archived User", "JS" => "formSubmit('Select iau.* From INTRANET_USER as iu Inner Join INTRANET_ARCHIVE_USER as iau On (iu.UserID = iau.UserID)', 0, '');");
	$shortCurArr[] = array("Display" => "iPortfolio Classroom", "JS" => "formSubmit('Select * From ".$CurSettingsArr['Database_eClass'].".course Where RoomType = 4', 0, '');");
	$shortCurArr[] = array("Display" => "App User Device", "JS" => "js_Set_TextArea_Text('Select * From ".$CurSettingsArr['Database_Intranet'].".APP_USER_DEVICE Where UserID=\'\' And RecordStatus = \'1\';');");
	$shortCurArr[] = array("Display" => "App Request Log", "JS" => "formSubmit('Select LogID, RequestText, ResponseText, DateInput, StartTime, EndTime, ProcessSecond, MemoryUsageText From ".$CurSettingsArr['Database_Intranet'].".APP_REQUEST_LOG Order By LogID desc Limit 100', 0, '');");
	$shortCurArr[] = array("Display" => "App Request Log (Today)", "JS" => "formSubmit('Select LogID, RequestText, ResponseText, DateInput, StartTime, EndTime, ProcessSecond, MemoryUsageText From ".$CurSettingsArr['Database_Intranet'].".APP_REQUEST_LOG Where DATE(DateInput) = \'".date('Y-m-d')."\' Order By LogID desc Limit 100', 0, '');");
	
	return $shortCurArr;
}

function Get_SQL_Action($Sql)
{
	$Sql = Standardize_SQL($Sql);
	
	$action = '';
	if (substr($Sql, 0, 6) == 'select')
		$action = 'select';
	else if (substr($Sql, 0, 6) == 'update')
		$action = 'update';
	else if (substr($Sql, 0, 6) == 'insert')
		$action = 'insert';
	else if (substr($Sql, 0, 6) == 'delete')
		$action = 'delete';
	else if (substr($Sql, 0, 5) == 'alter')
		$action = 'alter';
	else if (substr($Sql, 0, 4) == 'desc')
		$action = 'desc';
	else if (substr($Sql, 0, 6) == 'create')
		$action = 'create';
	else if (substr($Sql, 0, 4) == 'drop')
		$action = 'drop';
	else if (substr($Sql, 0, 14) == 'show databases')
		$action = 'show databases';
	else if (substr($Sql, 0, 11) == 'show tables')
		$action = 'show tables';
	else if (substr($Sql, 0, 17) == 'show table status')
		$action = 'show table status';
	else if (substr($Sql, 0, 17) == 'show create table')
		$action = 'show create table';
	else if (substr($Sql, 0, 20) == 'show create database')
		$action = 'show create database';
	else if (substr($Sql, 0, 3) == 'use')
		$action = 'use';
		
	return $action;
}

function Get_SQL_Update_Action_Array() {
	return array('update', 'insert', 'alter', 'delete', 'drop', 'create', 'use');
}

function Is_SQL_Update_Action($sql) {
	$UpdateActionArr = Get_SQL_Update_Action_Array();
	$action = Get_SQL_Action($sql);
	
	return in_array($action, $UpdateActionArr);
}

function Is_Update_Action($action) {
	$UpdateActionArr = Get_SQL_Update_Action_Array();
	
	return in_array($action, $UpdateActionArr);
}

function Standardize_SQL($Sql)
{
	return strtolower(trim($Sql));
}

function Get_Include_JS_CSS_HTML($disableDataTable, $platform)
{
	$includeHTML = '';
	
	$includeHTML .= '<link rel="stylesheet" href="css/skin/'.$platform.'.css" />'."\n";
	$includeHTML .= '<link rel="stylesheet" href="css/sql_query.css" />'."\n";
	$includeHTML .= '<link rel="stylesheet" href="css/jquery.fancybox-1.2.5.css" />'."\n";
	//$includeHTML .= '<link href="css/fg.menu.css" media="screen" rel="stylesheet" />'."\n";
	
	$includeHTML .= '<script type="text/javascript" src="js/jquery/jquery-1.3.2.min.js"></script>'."\n";
	$includeHTML .= '<script type="text/javascript" src="js/jquery/jquery.floatheader.min.js"></script>'."\n";
	$includeHTML .= '<script type="text/javascript" src="js/jquery/jquery.fancybox-1.2.5.js"></script>'."\n";
	//$includeHTML .= '<script type="text/javascript" src="js/jquery/fg.menu.js"></script>'."\n";
	//$includeHTML .= '<script type="text/javascript" src="js/jquery/jquery.cookie.js"></script>'."\n";
	$includeHTML .= '<script type="text/javascript" src="js/jquery/jquery.textarea.js"></script>'."\n";
	
	$includeHTML .= '<script type="text/javascript" src="js/jquery/jquery-ui-1.7.2.custom.min.js"></script>'."\n";
    $includeHTML .= '<link href="css/cupertino/jquery-ui-1.7.2.custom.css" media="screen" rel="stylesheet" />'."\n";
	
	$includeHTML .= '<script type="text/javascript" src="js/script.js"></script>'."\n";
	$includeHTML .= '<script type="text/javascript" src="js/quick_search.js"></script>'."\n";
	$includeHTML .= '<script type="text/javascript" src="js/date_history.js"></script>'."\n";	

	
	if ($disableDataTable == false)
	{
		$includeHTML .= '<link rel="stylesheet" href="css/demo_page.css" />'."\n";
		$includeHTML .= '<link rel="stylesheet" href="css/demo_table.css" />'."\n";
		//$includeHTML .= '<link rel="stylesheet" href="css/demos.css">'."\n";
	
		$includeHTML .= '<script language="JavaScript" src="js/jquery/jquery.dataTables.js"></script>'."\n";
	}
	
	return $includeHTML;
}

function Trim_SQL($sql)
{
	$sql = trim($sql);
	$sql = str_replace("\r\n", '<!--newLineWin-->', $sql);
	$sql = str_replace("\n", '<!--newLineUnix-->', $sql);
	$sql = str_replace("\r", '<!--newLineMac-->', $sql);
	$sql = str_replace(chr(13), '<!--ch13-->', $sql);
	$sql = str_replace("\t", '<!--tab-->', $sql);
	
	return $sql;
}

function standardizeQuery($parSql) {
	$parSql = str_replace('<!--newLineWin-->', "\r\n", $parSql);
	$parSql = str_replace('<!--newLineUnix-->', "\n", $parSql);
	$parSql = str_replace('<!--newLineMac-->', "\r", $parSql);
	$parSql = str_replace('<!--ch13-->', "\n", $parSql);
	$parSql = str_replace('<!--tab-->', "\t", $parSql);
	
	return $parSql;
}

function Convert_SQL_to_js_SQL($sql)
{
	$sql = Trim_SQL($sql);
	$sql = str_replace("\\", "\\\\", $sql);
	$sql = str_replace("'", "\'", $sql);
	
	return $sql;
}

function Convert_SQL_to_log_SQL($sql)
{
	$sql = Trim_SQL($sql);
	$sql = str_replace('"', '""', $sql);
	
	return $sql;
}

function Get_Current_Settings_Array($OnTheFlyCanView, $OnTheFlyCanUpdate, $OnTheFlyByPassLogin, $Delimiter, $OnTheFlyByPassLoginPin1, $OnTheFlyByPassLoginPin2) {
	global $DefaultSettingsArr;
	
	$CurSettingsArr = array();
	
	$CurSettingsArr['OnTheFlyCanView'] = ($OnTheFlyCanView)? 1 : 0;
	$CurSettingsArr['OnTheFlyCanUpdate'] = ($OnTheFlyCanUpdate)? 1 : 0;
	$CurSettingsArr['OnTheFlyByPassLogin'] = ($OnTheFlyByPassLogin)? 1 : 0;
	$CurSettingsArr['Delimiter'] = ($Delimiter)? $Delimiter : $DefaultSettingsArr['Functional']['Delimiter'];
	
	$CurSettingsArr['MySQL_UserName'] = (isset($_REQUEST['MySQL_UserName']))? $_REQUEST['MySQL_UserName'] : $DefaultSettingsArr['Database']['MySQL_UserName'];
	$CurSettingsArr['MySQL_Password'] = (isset($_REQUEST['MySQL_Password']))? $_REQUEST['MySQL_Password'] : $DefaultSettingsArr['Database']['MySQL_Password'];
	$CurSettingsArr['MySQL_Host'] = (isset($_REQUEST['MySQL_Host']))? $_REQUEST['MySQL_Host'] : $DefaultSettingsArr['Database']['MySQL_Host'];
	$CurSettingsArr['MySQL_Host'] = ($CurSettingsArr['MySQL_Host'] == '')? 'localhost' : $CurSettingsArr['MySQL_Host'];
	
	$CurSettingsArr['Database_Intranet'] = (isset($_REQUEST['Database_Intranet']))? $_REQUEST['Database_Intranet'] : $DefaultSettingsArr['Database']['Database_Intranet'];
	$CurSettingsArr['Database_eClass'] = (isset($_REQUEST['Database_eClass']))? $_REQUEST['Database_eClass'] : $DefaultSettingsArr['Database']['Database_eClass'];
	$CurSettingsArr['Database_LMS'] = (isset($_REQUEST['Database_LMS']))? $_REQUEST['Database_LMS'] : $DefaultSettingsArr['Database']['Database_LMS'];
	
	$CurSettingsArr['Database'] = (isset($_REQUEST['Database']))? $_REQUEST['Database'] : $DefaultSettingsArr['Database']['Default_Database'];
	$CurSettingsArr['Database'] = ($CurSettingsArr['Database']=='')? $CurSettingsArr['Database_Intranet'] : $CurSettingsArr['Database'];
	
	$CurSettingsArr['Charset'] = (isset($_REQUEST['Charset']))? $_REQUEST['Charset'] : $DefaultSettingsArr['Display']['Charset'];
	$CurSettingsArr['KeepQuery'] = (isset($_REQUEST['KeepQuery']))? $_REQUEST['KeepQuery'] : $DefaultSettingsArr['Functional']['KeepQuery'];
	$CurSettingsArr['RepeatHeader'] = (isset($_REQUEST['RepeatHeader']))? $_REQUEST['RepeatHeader'] : $DefaultSettingsArr['Functional']['RepeatHeader'];
	$CurSettingsArr['RepeatHeaderCount'] = (isset($_REQUEST['RepeatHeaderCount']))? $_REQUEST['RepeatHeaderCount'] : $DefaultSettingsArr['Functional']['RepeatHeaderCount'];
	$CurSettingsArr['DisableDataTable'] = (isset($_REQUEST['DisableDataTable']))? $_REQUEST['DisableDataTable'] : $DefaultSettingsArr['Functional']['DisableDataTable'];
	$CurSettingsArr['AutoDisableDataTable'] = (isset($_REQUEST['AutoDisableDataTable']))? $_REQUEST['AutoDisableDataTable'] : $DefaultSettingsArr['Functional']['AutoDisableDataTable'];
	$CurSettingsArr['AutoDisableDataTableCount'] = (isset($_REQUEST['AutoDisableDataTableCount']))? $_REQUEST['AutoDisableDataTableCount'] : $DefaultSettingsArr['Functional']['AutoDisableDataTableCount'];
	
	$CurSettingsArr['Security']['CheckPermission']['View'] = ($CurSettingsArr['OnTheFlyCanView'])? 0 : $DefaultSettingsArr['Security']['CheckPermission']['View'];
	$CurSettingsArr['Security']['CheckPermission']['UpdateDB'] = ($CurSettingsArr['OnTheFlyCanUpdate'])? 0 : $DefaultSettingsArr['Security']['CheckPermission']['UpdateDB'];
	$CurSettingsArr['Security']['CheckPermission']['ByPassLogin'] = ($CurSettingsArr['OnTheFlyByPassLogin'])? 1 : $DefaultSettingsArr['Security']['CheckPermission']['ByPassLogin'];
	$CurSettingsArr['Security']['CheckPermission']['ByPassLoginPin1'] = ($OnTheFlyByPassLoginPin1)? $OnTheFlyByPassLoginPin1 : '';
	$CurSettingsArr['Security']['CheckPermission']['ByPassLoginPin2'] = ($OnTheFlyByPassLoginPin2)? $OnTheFlyByPassLoginPin2 : '';
	
	
	return $CurSettingsArr;
}

function Get_jsHistory_Array($SqlHistoryArr)
{
	$numOfSQLHistory = count((array)$SqlHistoryArr);
	$lastSql = "";
	
	$x = '';
	$x .= '<script>'."\n";
	$x .= 'var jsHistoryArr = new Array('.$numOfSQLHistory.');'."\n";
	for ($i=0; $i<$numOfSQLHistory; $i++)
	{
		$thisSQL = trim($SqlHistoryArr[$i]);
			
		if ($thisSQL == "")
			continue;
		
		$jsThisSQL = Convert_SQL_to_js_SQL($thisSQL);
		$x .= 'jsHistoryArr['.$i.'] = \''.$jsThisSQL.'\';'."\n";
	}
	$x .= "</script>"."\n\n";
		
	return $x;
}

function Get_File_Content($file) {
		
		clearstatcache();
        if(file_exists($file) && is_file($file) && filesize($file)!=0){
                $x =  ($fd = fopen($file, "r")) ? fread($fd,filesize($file)) : "";
                if ($fd)
                    fclose ($fd);
        }
        
        return $x;
}

function Get_School_Name() {
	global $IntranetSettingsPathRoot;

	$school_data = explode("\n",Get_File_Content($IntranetSettingsPathRoot."file/school_data.txt"));
	$SchoolName = $school_data[0];

	return $SchoolName;
}

function Get_School_Code() {
	global $config_school_code;
	
	return $config_school_code;
}


function exporticon() {
	// dummy function for EJ lang files...
}

function Array_Remove_Empty($arr) {
    $narr = array();
    while(list($key, $val) = each($arr)){
        if (is_array($val)){
            $val = Array_Remove_Empty($val);
            if (count($val)!=0){
                $narr[$key] = $val;
            }
        }
        else {
            if (trim($val) != ""){
                $narr[$key] = $val;
            }
        }
    }
    unset($arr);
    return $narr;
}

function Array_Trim($arr) {
	$narr = array();
	while(list($key, $val) = each($arr)){
		if (is_array($val)){
			$val = Array_Trim($val);
			$narr[$key] = $val;
		}
		else
		{
			$narr[$key] = trim($val);
		}
	}
	unset($arr);
	return $narr;
}

function getCurPageURL($withQueryString=1, $withPageSuffix=1) {
	 $pageSuffix = ($withQueryString==1)? $_SERVER["REQUEST_URI"] : $_SERVER["SCRIPT_NAME"];
	 $pageURL = 'http';
	 if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
	 $pageURL .= "://";
	 if ($_SERVER["SERVER_PORT"] != "80") {
	  //$pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
	  $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"];
	 } else {
	  //$pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
	  $pageURL .= $_SERVER["SERVER_NAME"];
	 }
	 
	 if ($withPageSuffix) {
	 	$pageURL .= $pageSuffix;
	 }
	 
	 return $pageURL;
}

function getHtmlByUrl($url) {
   		// Set $_COOKIE string
		//$strCookie = 'PHPSESSID=' . $_COOKIE['PHPSESSID'] . '; path=/';
		
		$strCookie = '';
		foreach ((array)$_COOKIE as $_key => $_value) {
			$strCookie .= $_key.'='.$_value.';';
		}
		$strCookie .= '; path=/';
		
		session_write_close();
		$ch = curl_init();
		
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_HEADER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_COOKIE, $strCookie);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $_SESSION);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
		$html = curl_exec($ch);
		curl_close($ch);
		
		return str_replace("\n", "\r\n", trim($html));
   	}

function getSystemPlatform() {
	$url = getCurPageURL($withQueryString=false, $withPageSuffix=false);
	$version = getHtmlByUrl($url.'/includes/version.php');
	
	$posEj = strpos($version, 'Version: ej');
	$posIp = strpos($version, 'Version: ip');
	if ($posEj !== false) {
	    $platform = 'ej';
	}
	else if ($posIp !== false){
	    $platform = 'ip';
	}
	else {
		$platform = 'other';
	}
	
	return $platform;
}

function getArrayDepth($ary) {
	$hasArray = 0;
	$maxDepth = $depth = 0;
	
	foreach((array)$ary as $item) {
		if(is_array($item))	{
			$depth = max(getArrayDepth($item),$maxDepth);
			$hasArray = 1;
		}
	}
	if ($hasArray) {
		return $depth + 1;
	}
	else {
		return 1;
	}
}

/**
 * Function for Start Timer
 * @return boolean
 */
function StartTimer($variableName="") {
	$mtime = microtime();
    $mtime = explode(" ",$mtime);
    $mtime = $mtime[1] + $mtime[0];
    
    if ($variableName!="") {
    	global ${$variableName};
    	${$variableName} = $mtime;
    }
    else {
    	global $timestart;
    	$timestart = $mtime;
    }
            
    return true;
}

/**
 * Function for Stop Timer
 * @param number $precision Precision of output time
 * @return number Time
 */
function StopTimer($precision=5, $NoNumFormat=false, $variableName="") {
    if ($variableName!="") {
    	global ${$variableName};
    	$timestart = ${$variableName};
    }
    else {
    	global $timestart;
    }
    $mtime = microtime();
    $mtime = explode(" ",$mtime);
    $mtime = $mtime[1] + $mtime[0];
    $timeend = $mtime;
    $timetotal = $timeend-$timestart;
	return ($NoNumFormat==false) ? number_format($timetotal,$precision) : $timetotal;
}

function getPresetSqlSymbol($key) {
	$symbol = '';
	
	if ($key == 'primaryKey') {
		$symbol = '{{{PrimaryKey}}}';
	}
	
	return $symbol;
}

function generatePin1() {
	$serverName = $_SERVER["SERVER_NAME"];
	$firstChar = substr($serverName, 0, 1);
	$lastChar = substr($serverName, -1, 1);
	
	return $firstChar.date('md').date('D').$lastChar;
}

function checkValidPin1($parPin1) {
	return ($parPin1 == generatePin1())? true : false;
}

function generatePin2Key($parPin1) {
	return md5($parPin1);
}

function generatePin2() {
	$pin2Key = generatePin2Key(generatePin1());
	
	$first3Char = substr($pin2Key, 0, 3);
	$last3Char = substr($pin2Key, -3, 3);
	
	return $first3Char.$last3Char;
}

function checkValidPin2($parPin2) {
	return ($parPin2 == generatePin2())? true : false;
}
?>