function js_Reload_Date_History_Table()
{
	var jsTargetDate = Trim($('input#HistoryDate').val());
	
	$('div#HistoryDetailsDiv').html(js_Get_Ajax_Loading_Image()).load(
		"src/date_history/ajax_get_date_history_table.php", 
		{ TargetDate: jsTargetDate },
		function(ReturnData)
		{
			
		}
	);
}