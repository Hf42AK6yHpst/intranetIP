// Global Variable Here
var jsGlobalArr = new Array();
jsGlobalArr['AltPressed'] = false;
jsGlobalArr['CtrlPressed'] = false;
jsGlobalArr['LeftApplePressed'] = false;
jsGlobalArr['RightApplePressed'] = false;


function backToTop() {
    var x1 = x2 = x3 = 0;
    var y1 = y2 = y3 = 0;

    if (document.documentElement) {
        x1 = document.documentElement.scrollLeft || 0;
        y1 = document.documentElement.scrollTop || 0;
    }

    if (document.body) {
        x2 = document.body.scrollLeft || 0;
        y2 = document.body.scrollTop || 0;
    }

    x3 = window.scrollX || 0;
    y3 = window.scrollY || 0;

    var x = Math.max(x1, Math.max(x2, x3));
    var y = Math.max(y1, Math.max(y2, y3));

    window.scrollTo(Math.floor(x / 2), Math.floor(y / 2));

    if (x > 0 || y > 0) {
        window.setTimeout("backToTop()", 25);
    }
}

function LTrim(str){
        var whitespace = new String(" \t\n\r");
        var s = new String(str);
        if (whitespace.indexOf(s.charAt(0)) != -1) {
                var j=0, i = s.length;
                while (j < i && whitespace.indexOf(s.charAt(j)) != -1)
                        j++;
                s = s.substring(j, i);
        }
        return s;
}
function RTrim(str){
        var whitespace = new String(" \t\n\r");
        var s = new String(str);
        if (whitespace.indexOf(s.charAt(s.length-1)) != -1) {
                var i = s.length - 1;       // Get length of string
                while (i >= 0 && whitespace.indexOf(s.charAt(i)) != -1)
                        i--;
                s = s.substring(0, i+1);
        }
        return s;
}
function Trim(str){
        return RTrim(LTrim(str));
}


function js_Set_TextArea_Text(text)
{
	//document.getElementById("SQL").innerHTML = text;
	$('textarea#SQL').val(text).focus();
}


function js_Check_KeyDown(e)
{
	var keynum = Get_KeyNum(e);
	var keychar = String.fromCharCode(keynum);
	
	if (keynum == 17) {
		jsGlobalArr['CtrlPressed'] = true;
	}
	else if (keynum == 18) {
		jsGlobalArr['AltPressed'] = true;
	}
	else if (keynum == 91) {
		jsGlobalArr['LeftApplePressed'] = true;
	}
	else if (keynum == 93) {
		jsGlobalArr['RightApplePressed'] = true;
	}
	
	if ((jsGlobalArr['CtrlPressed']==true || jsGlobalArr['LeftApplePressed']==true || jsGlobalArr['RightApplePressed']==true) && keynum==13)	//Ctrl+Enter => Submit
	{
		formSubmit(js_Get_SQL_Value(), 0);
	}
	else if ((jsGlobalArr['CtrlPressed']==true || jsGlobalArr['LeftApplePressed']==true || jsGlobalArr['RightApplePressed']==true) && keynum==83)	//Ctrl+S => Submit
	{
		e.preventDefault();
		formSubmit(js_Get_SQL_Value(), 0);
	}
	else if (jsGlobalArr['CtrlPressed']==true && jsGlobalArr['AltPressed']==true && keynum==69)		// Ctrl+Alt+E => Explain Sql
	{
		formSubmit('Explain(' + js_Get_SQL_Value() + ')', 0);
	}
	else if (jsGlobalArr['CtrlPressed']==true && jsGlobalArr['AltPressed']==true && keynum==81)		// Ctrl+Alt+Q => Quick-Search
	{
		$('a#QuickSearchLink').click();
	}
	else if (jsGlobalArr['CtrlPressed']==true && jsGlobalArr['AltPressed']==true && keynum==84)		// Ctrl+Alt+T => Go to Top
	{
		backToTop();
	}
	else if (jsGlobalArr['CtrlPressed']==true && jsGlobalArr['AltPressed']==true && keynum==88)		// Ctrl+Alt+X => Export
	{
		js_Export_Result(js_Get_SQL_Value());
	}
}

function js_Check_KeyUp(e)
{
	var keynum;
	var keychar;
	
	if(window.event) // IE
	{
		e = window.event;
		keynum = e.keyCode;
	}
	else if(e.which) // Netscape/Firefox/Opera
	{
		keynum = e.which;
	}
	keychar = String.fromCharCode(keynum);
	
	if (keynum == 17) {
		jsGlobalArr['CtrlPressed'] = false;
	}
	else if (keynum == 18) {
		jsGlobalArr['AltPressed'] = false;
	}
	else if (keynum == 91) {
		jsGlobalArr['LeftApplePressed'] = false;
	}
	else if (keynum == 93) {
		jsGlobalArr['RightApplePressed'] = false;
	}
}

function js_Get_SQL_Value()
{
	return document.getElementById('SQL').value;
}

function js_ShowHide_Layer(DivID, Action, DivClass)
{
	$('.' + DivClass).slideUp();
	
	if (DivID != '')
	{
		if (Action == null)
		{
			if ($('#' + DivID).is (':visible') == false)
				Action = 'show';
			else
				Action = 'hide';
		}
		
		if (Action == 'show')
			$('#' + DivID).slideDown();
		else
			$('#' + DivID).slideUp();
	}
}

function js_Change_Object_Disable_Status(Checked, TargetObjectID)
{
	if (Checked == true)
		document.getElementById(TargetObjectID).disabled = false;
	else
		document.getElementById(TargetObjectID).disabled = true;
}

function js_Get_Ajax_Loading_Image()
{
	return '<img src="images/indicator.gif"> <span class="tabletextremark">Loading...</span>';
}

function js_Move_Div_To_Top(jsDivID, jsDivClass)
{
	var zmax = 0;
	$('div.' + jsDivClass).each(function() {
        var cur = parseInt($(this).css('zIndex'));
        zmax = cur > zmax ? cur : zmax;
    });
	
	$('div#' + jsDivID).css("zIndex", zmax + 5);
}

function Get_KeyNum(e)
{
	var keynum;
		
	if(window.event) // IE
	{
		e = window.event;
		keynum = e.keyCode;
	}
	else if(e.which) // Netscape/Firefox/Opera
		keynum = e.which;
		
	return keynum;
}

function js_Check_DB_Info_Submit(e)
{
	var keynum = Get_KeyNum(e);
	var keychar = String.fromCharCode(keynum);
	
	if (keynum==13)		//Enter => Submit
		formSubmit('', 0, 0, '');
}

function js_Get_Loading_Image(jsRootPath)
{
	var jsImg = '';
	jsRootPath = jsRootPath || '';
	
	jsImg += '<img src="' + jsRootPath + 'images/indicator.gif">';
	jsImg += ' <span class="tabletextremark">Loading...</span>';
	
	return jsImg;
}

function js_Export_Result(jsSQL)
{
	formSubmit(jsSQL, 1);
}

function createListToTemp(tableNum, columnNo) {
	var jsTempArr = new Array();
	
	$('table#ResultSetTable_' + tableNum + ' > tbody > tr').each( function () {
		var jsThisHtml = $(this).find('td').eq(columnNo + 1).html();
		if (jsThisHtml != null) {
			jsThisHtml = jsThisHtml.replace(/\\/g, "\\\\")
			jsThisHtml = jsThisHtml.replace(/'/g, "\\'")
			jsTempArr[jsTempArr.length] = jsThisHtml;
		}
	});
	var jsListText = "'" + jsTempArr.join("', '", jsTempArr) + "'";
	
	var jsOriTextareaValue = Trim($('textarea#SQL_Temp').val());
	if (jsOriTextareaValue != '') {
		jsListText = "\r\n" + jsListText;
	}
	$('textarea#SQL_Temp').val(jsOriTextareaValue + jsListText).focus();
}

function insertTextInTextarea(parText, parAppend) {
	parAppend = parAppend || false;
	
	if (parAppend) {
		var cursorPos = $('textarea#SQL').attr('selectionStart'),
	    v = $('textarea#SQL').val(),
	    textBefore = v.substring(0,  cursorPos ),
	    textAfter  = v.substring( cursorPos, v.length );
	    $('textarea#SQL').val( textBefore + parText + textAfter ).focus();
	}
	else {
		js_Set_TextArea_Text(js_Get_SQL_Value() + parText);
	}
}

function getTodayDateString() {
	var today = new Date();
	var dd = today.getDate();
	var mm = today.getMonth()+1; //January is 0!

	var yyyy = today.getFullYear();
	if(dd<10){
	    dd='0'+dd;
	} 
	if(mm<10){
	    mm='0'+mm;
	} 
	
	return  yyyy.toString() + mm.toString() + dd.toString();
}