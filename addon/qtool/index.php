<?php
ini_set('display_errors',1);
error_reporting(E_ALL ^ E_NOTICE); 

if($phpVersionSupport) {
	date_default_timezone_set("UTC");
}
else {
	putenv("TZ=UTC");
}

# include DB settings and common function
$PATH_WRT_ROOT = "";
include_once($PATH_WRT_ROOT."library/common_function.php");
include_once($PATH_WRT_ROOT."settings.php");
include_once($PATH_WRT_ROOT."library/libdb.php");
include_once($PATH_WRT_ROOT."library/lib_ui.php");

StartTimer();

### Variable Initialization
$AutoDisableDataTable = isset($_POST['AutoDisableDataTable'])? $_POST['AutoDisableDataTable'] : 1;
$AutoDisableDataTableCount = isset($_POST['AutoDisableDataTableCount'])? $_POST['AutoDisableDataTableCount'] : 100;
$OnTheFlyCanView = isset($_REQUEST['OnTheFlyCanView'])? $_REQUEST['OnTheFlyCanView'] : 0;
$OnTheFlyCanUpdate = isset($_REQUEST['OnTheFlyCanUpdate'])? $_REQUEST['OnTheFlyCanUpdate'] : 0;
$OnTheFlyByPassLogin = isset($_REQUEST['OnTheFlyByPassLogin'])? $_REQUEST['OnTheFlyByPassLogin'] : 0;
$OnTheFlyByPassLoginPin1 = isset($_POST['OnTheFlyByPassLoginPin1'])? $_POST['OnTheFlyByPassLoginPin1'] : 0;
$OnTheFlyByPassLoginPin2 = isset($_POST['OnTheFlyByPassLoginPin2'])? $_POST['OnTheFlyByPassLoginPin2'] : 0;
$Delimiter = isset($_POST['Delimiter'])? trim(stripslashes($_POST['Delimiter'])) : 0;
$CurSettingsArr = Get_Current_Settings_Array($OnTheFlyCanView, $OnTheFlyCanUpdate, $OnTheFlyByPassLogin, $Delimiter, $OnTheFlyByPassLoginPin1, $OnTheFlyByPassLoginPin2);

if (!isset($_POST['Platform'])) {
	$Platform = getSystemPlatform();
}
else {
	$Platform = $_POST['Platform'];
}
$CurSettingsArr['Platform'] = $Platform;

### Get Query
$SQL = empty($_POST['SQL'])? '' : trim(stripslashes($_POST['SQL']));
$SQL_Temp = empty($_POST['SQL_Temp'])? '' : trim(stripslashes($_POST['SQL_Temp']));

$sqlAry = explode($CurSettingsArr['Delimiter'], $SQL);
$sqlAry = array_values(Array_Trim(Array_Remove_Empty($sqlAry)));
$numOfSql = count($sqlAry);


### Check permission for intranet
Check_Permission($CurSettingsArr);


### Initialize Library
$libUI = new lib_ui();
$libDB = new libdb($CurSettingsArr['Database']);
$libDB->Open_DB($CurSettingsArr);
$sqlAry = $libDB->Get_Analyzed_Final_Sql($sqlAry);


### Settings Display
$settingsTable = $libUI->Get_Settings_Table($CurSettingsArr);
$dbInfoDiv = $libUI->Get_DB_Info_Layer($CurSettingsArr);
$otherSettingsDiv = $libUI->Get_Other_Settings_Layer($CurSettingsArr);


### Create History JS Array
$SQL_History = $_POST['SQL_History'];
$SQL_History = stripslashes(intranet_undo_htmlspecialchars($SQL_History));
$SQL_History = ($SQL.$DefaultSettingsArr['Database']['SqlHistorySeparator'].$SQL_History);
$SQL_HistoryArr = explode($DefaultSettingsArr['Database']['SqlHistorySeparator'], $SQL_History);
$jsHistoryArrHTML = Get_jsHistory_Array($SQL_HistoryArr);

### History Display
$sqlHistoryTable = $libUI->Get_History_Table('History', $SQL_HistoryArr);

### Current SQL Info Div
$CurrentSqlInfoDiv = $libUI->Get_Current_SQL_Info_Div($SQL_HistoryArr[0], $SQL_HistoryArr[1]);


## Short Cut Table
$shortCutTable = $libUI->Get_Short_Cut_Table($CurSettingsArr);


### Query input textarea
$primaryKeyShortCut = '<span class="tablelink" onclick="insertTextInTextarea(\''.getPresetSqlSymbol('primaryKey').'\', true);" style="cursor: pointer;">[Add Primary Key]</span>';
$convertToBackupSqlBtn = '<span class="tablelink" onclick="convertToBackupSql();" style="cursor: pointer;">[Convert to backup SQL]</span>';
$queryTextArea = $libUI->Get_SQL_TextArea('SQL', $CurSettingsArr, $SQL);
$queryTextAreaTemp = $libUI->Get_SQL_TextArea('SQL_Temp', '', $SQL_Temp, $IsTemp=1, '', 4);


### Result Table display
$resultSetTableAry = array();
$numOfResultSetAry = array();
if ($numOfSql > 1) {
	$CurSettingsArr['DisableDataTable'] = 1;
	$disableDataTable = 1;
}
for ($i=0; $i<$numOfSql; $i++) {
	$_sql = $sqlAry[$i];
	
	// Special Handling if use "Use" to change DB
	$_action = Get_SQL_Action($_sql);
	$_isActionUse = false;
	$_originalDatabase = $CurSettingsArr['Database'];
	if ($_action == 'use') {
		// e.g. "use intranet" to change the database
		$_isActionUse = true;
		
		$_sqlPieceAry = explode(' ', $_sql);
		$_targetDatabase = trim($_sqlPieceAry[1]);
		
		$CurSettingsArr['Database'] = $_targetDatabase;
	}
	
	$libDB = new libdb($CurSettingsArr['Database']);
	$libDB->Open_DB($CurSettingsArr);
	
	if ($_sql) {
		list($_resultSetTable, $_numOfResultSet) = $libUI->Get_SQL_Result_Table($_sql, $CurSettingsArr, $i);
		
		# Disable the DataTable automatically if there is no result returned
		# Disable the DataTable automatically if the user has checked the option
		if ($_numOfResultSet == 0 || ($AutoDisableDataTable == 1 && $_numOfResultSet > $AutoDisableDataTableCount)) {
			$disableDataTable = 1;
		}
		
		if ($numOfSql > 1) {
			$_resultSetTable = ($i + 1).'.<br />'.nl2br($_sql).'<br />'.$_resultSetTable;
		}
		$resultSetTableAry[] = $_resultSetTable;
		
		if ($_isActionUse && mysql_error() != '') {
			$CurSettingsArr['Database'] = $_originalDatabase;
		}
	}
	$libDB->Close_DB();
}
$resultSetTable = implode('<br /><hr class="hr_thin" /><br />', $resultSetTableAry);


if ($numOfSql > 0) {
	$resultSetTable .= '<br /><hr class="hr_thin" />';
	
	## back to top table at the bottom
	$backToTopTable = $libUI->Get_Back_To_Top_Table();
}


### Get Page Header and Footer
$headerHTML = $libUI->Get_Page_Header($CurSettingsArr['Charset']);
$footerHTML = $libUI->Get_Page_Footer();



### Get JS and CSS include files
$include_JS_CSS_HTML = Get_Include_JS_CSS_HTML($disableDataTable, $Platform);



echo $headerHTML."\n";
echo $include_JS_CSS_HTML."\n";
echo $jsHistoryArrHTML."\n";
?>
	
<script>
$(document).ready( function() {
	// Set default DB
	$('select#Database').val('<?=$CurSettingsArr['Database']?>');
	
	// Make the textarea support "tab" and resizable
	$('textarea#SQL, textarea#SQL_Temp').tabby().resizable({
		ghost: false	
	});
	$('textarea#SQL').focus();
	
	$('div.draggable_div').draggable().mousedown( function () {
		js_Move_Div_To_Top($(this).attr('id'), 'draggable_div');
	});
	
	// Version Info Layer Initialization
	$('a#VersionInfoIcon, a#QuickSearchLink, a#DateHistoryLink').fancybox({
		'hideOnContentClick': false
	});
	
	
	<?php if ($disableDataTable == false) { ?>
		var oTable;
		var asInitVals = new Array();
		
		oTable = $('.ResultSetTable').dataTable( {
			
		} );
		
		// Search keywords listener
		$("thead input").keyup( function () {
			/* Filter on the column (the index) of this element */
			oTable.fnFilter( this.value, $("thead input").index(this) + 1);
		} );
		
		/*
		 * Support functions to provide a little bit of 'user friendlyness' to the textboxes in 
		 * the footer
		 */
		$("thead input").each( function (i) {
			asInitVals[i] = this.value;
		} );
		
		$("thead input").focus( function () {
			if ( this.className == "search_init" )
			{
				this.className = "";
				this.value = "";
			}
		} );
		
		$("thead input").blur( function (i) {
			if ( this.value == "" )
			{
				this.className = "search_init";
				this.value = asInitVals[$("thead input").index(this)];
			}
		} );
		
		$('.ResultSetTable th').unbind('click.DT');
		
	<?php } else { ?>
	
		// Floating Table Header initialization
		jQuery('.ResultSetTable').floatHeader({
			fadeIn: 1000, 
			fadeOut: 1000
		});
		
	<?php } ?>
	
	// Show the DB Info layer if the DB info is not correct
	if ($('select#Database').val() == '' || $('select#Database').val() == null) {
		js_ShowHide_Layer('DBInfoDiv', null, 'selectbox_layer');
	}
	
	//$('.ResultSetTable').minEmoji();
});

function formSubmit(jsSql, jsIsExport, jsHistoryArrIndex, jsDatabase)
{
	if ($('#AutoDisableDataTable').attr('checked') == true) {
		$('#AutoDisableDataTable').val(1);
	}
	else {
		$('#AutoDisableDataTable').val(0);
		$('#AutoDisableDataTable').attr('checked', 'checked');
	}
	
	if ($('#RepeatHeaderCount').val() == '') {
		$('#RepeatHeaderCount').val('<?=$DefaultSettingsArr['Functional']['RepeatHeaderCount']?>');
	}
		
	if ($('#AutoDisableDataTableCount').val() == '') {
		$('#AutoDisableDataTableCount').val('<?=$DefaultSettingsArr['Functional']['AutoDisableDataTableCount']?>');
	}
		
	if (jsDatabase != null && jsDatabase != '') {
		$('select#Database').val(jsDatabase);
	}
		
	var obj = document.query_form;
	if (jsIsExport == 1) {
		if (jsSql != '') {
			var jsTempSQL = $('input#SQL_Export').val();
			$('input#SQL_Export').val(jsSql);
		}
		
		obj.action = 'src/export/export.php';
		obj.submit();
		
		if (jsSql != '') {
			$('input#SQL_Export').val(jsTempSQL);
		}
	}
	else {
		obj.action = 'index.php';
		if (Trim(jsSql) != '')
			document.getElementById("SQL").value = jsSql;
		else
			document.getElementById("SQL").value = getSqlFromHistoryAry(jsHistoryArrIndex);
			
		// Log SQL
		$.post(
			"src/log/ajax_log_sql.php", 
			{
				TargetSql: encodeURIComponent(document.getElementById("SQL").value),
				Charset: $('select#Charset').val(),
				Database: $('select#Database').val()
			},
			function(ReturnData)
			{
				// do nth
				//alert('success = ' + ReturnData);
				obj.submit();
			}
		);
	}
}

function copyHistoryToTextarea(historyIndex) {
	js_Set_TextArea_Text(getSqlFromHistoryAry(historyIndex));
}

function getSqlFromHistoryAry(historyIndex) {
	var historyText = (jsHistoryArr[historyIndex] == null || jsHistoryArr[historyIndex] == 'undefined')? '' : jsHistoryArr[historyIndex];
	
	return standardizeQuery(historyText);
}

function standardizeQuery(parSql) {
	parSql = parSql.replace(/<!--newLineWin-->/gi, "\r\n")
	parSql = parSql.replace(/<!--newLineUnix-->/gi, "\n");
	parSql = parSql.replace(/<!--newLineMac-->/gi, "\r");
	parSql = parSql.replace(/<!--ch13-->/gi, "\n");
	parSql = parSql.replace(/<!--tab-->/gi, "\t");
	
	return parSql;
}

function convertToBackupSql() {
	var tempText = $('textarea#SQL').val();
	var tableAry = tempText.split("\n");
	var numOfTable = tableAry.length;

	var i = 0;
	var allSqlText = '';
	for (i=0; i<numOfTable; i++) {
		var _tableName = tableAry[i];
		var _bakSql = 'Create Table ' + _tableName + '_' + getTodayDateString() + ' Select * From ' + _tableName + ';<!--newLineWin-->';

		allSqlText = allSqlText + _bakSql;
	}

	js_Set_TextArea_Text(standardizeQuery(allSqlText));
}

// add key checking for the whole page
document.onkeyup = js_Check_KeyUp;
document.onkeydown = js_Check_KeyDown;
</script>
	
<form id="query_form" name="query_form" method="POST">

	<!-- Hidden value in the top for export massive data (can export data even if the table is not fully loaded) -->
	<input type='hidden' id='SQL_History' name='SQL_History' value='<?=intranet_htmlspecialchars($SQL_History)?>' />
	<input type='hidden' id='SQL_Export' name='SQL_Export' value='<?=intranet_htmlspecialchars($SQL)?>' />
	
	<div class="container_div">
		<div class="sub_container_div">
			<?= $settingsTable ?>
			<?= $dbInfoDiv ?>
			<?= $otherSettingsDiv ?>
		</div>
		<br style="clear:both" />
		
		<div class="info_outer_div">
			<?= $CurrentSqlInfoDiv ?>
			
			<div id="history_div" style="float:left">
				<div id="history_inner_div">
					<?= $sqlHistoryTable?>
					<br />
				</div>
			</div>
			
			<div id="short_cut_div" style="float:left">
				<div id="history_inner_div">
					<?= $shortCutTable?>
					<br />
				</div>
			</div>
		</div>
		<br style="clear:both" />
		
		<div class="sub_container_div">
			<div id="sql_div" class="draggable_div" style="width:60%;">
				<table border="0" cellpadding="2" cellspacing="0" style="width:100%;">
					<tr style="vertical-align:bottom">
						<td>
							<div style="float:left;"><b>SQL</b>&nbsp;&nbsp;&nbsp;<?=$primaryKeyShortCut?> <?php echo $convertToBackupSqlBtn?></div>
							<div style="float:right;">Delimiter <input type="text" id="DelimiterTb" name="Delimiter" value="<?=intranet_htmlspecialchars($CurSettingsArr['Delimiter'])?>" />&nbsp;&nbsp;</div>
							<br style="clear:both;" />
							<div style="float:left;width:100%;"><?=$queryTextArea?></div>
						</td>
						<td style="width:2px;">&nbsp;</td>
						<td style="width:135px;">
							<div>
								<span><input type="button" value="Export " onclick="js_Export_Result(js_Get_SQL_Value());" /></span>
								<br style="clear:both;" />
								<span class="remark_grey" style="text-align:center">(Ctrl+Alt+X)</span>
							</div>
							<div>
								<span><input type="button" value="Explain" onclick="formSubmit('Explain(' + js_Get_SQL_Value() + ')', 0);" /></span>
								<br style="clear:both;" />
								<span class="remark_grey" style="text-align:center">(Ctrl+Alt+E)</span>
							</div>
							<div>
								<span><input type="button" value="Submit" onclick="formSubmit(js_Get_SQL_Value(), 0);" /></span>
								<br style="clear:both;" />
								<span class="remark_grey" style="text-align:center">(Ctrl+Enter)</span>
							</div>
							<div style="height:10px;"></div>
						</td>
					</tr>
				</table>
			</div>
			<div id="sql_temp_div" class="draggable_div" style="width:40%;">
				<table border="0" cellpadding="2" cellspacing="0" style="width:100%;">
					<tr style="vertical-align:bottom;">
						<td colspan="2">
							<div style="float:left;">
								Temp
							</div>
							<br style="clear:both;" />
							<div id="SQL_TempDiv" style="float:left;width:100%;">
								<?=$queryTextAreaTemp?>
							</div>
						</td>
					</tr>
				</table>
			</div>
		</div>
		
		<br style="clear:both" />
		<hr class="hr_thick" />
		
		<div id="container" class="sub_container_div">  
			<br style="clear:both" /> 
			<div id="demo">
				<?= $resultSetTable ?>
			</div>
		</div>
		<br style="clear:both" />
		
		<div class="sub_container_div">
			<?= $backToTopTable ?>
		</div>
		<br style="clear:both" />
	</div>
	
	<input type="hidden" id="OnTheFlyCanView" name="OnTheFlyCanView" value="<?=$OnTheFlyCanView?>" />
	<input type="hidden" id="OnTheFlyCanUpdate" name="OnTheFlyCanUpdate" value="<?=$OnTheFlyCanUpdate?>" />
	<input type="hidden" id="OnTheFlyByPassLogin" name="OnTheFlyByPassLogin" value="<?=$OnTheFlyByPassLogin?>" />
	<input type="hidden" id="OnTheFlyByPassLoginPin1" name="OnTheFlyByPassLoginPin1" value="<?=$OnTheFlyByPassLoginPin1?>" />
	<input type="hidden" id="OnTheFlyByPassLoginPin2" name="OnTheFlyByPassLoginPin2" value="<?=$OnTheFlyByPassLoginPin2?>" />
	<input type="hidden" id="Platform" name="Platform" value="<?=$Platform?>" />
</form>
<?= $footerHTML ?>
<?= '<!--Page loads time = '.StopTimer().'s -->' ?>