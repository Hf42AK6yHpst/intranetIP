<?php
#using: Paul
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/elib_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libIntranetModule.php");

intranet_opendb();
###########################################

$ModuleID = isset($_REQUEST['ModuleID'])? trim($_REQUEST['ModuleID']) : "";

$objModule = new libintranetmodule();
if($ModuleID ==""){
	echo "No Module ID is given";
	die();
}

$sql = "SELECT 
			ModuleLicenseID,
			NumberOfUnit,
			NumberOfStudents,
			NumberOfLicense,
			ExpiryDate,
			InputDate,
			if(md5(CONCAT(ModuleId,'_',NumberOfLicense,'_',InputDate)) = CheckKey,1,0) as LicenseIsValid
		FROM 
			INTRANET_MODULE_LICENSE
		WHERE 
			ModuleID = '".$ModuleID."' 
		ORDER BY 
			InputDate desc";
$licenseHistoryRS = $objModule->returnArray($sql) or die(mysql_error());

$sql = "SELECT 
			SUM(ml.NumberOfLicense) as \"NumberOfLicense\", m.Code as \"Code\",
			m.Description as \"Description\"
		FROM 
			INTRANET_MODULE_LICENSE as ml
		LEFT JOIN 
			INTRANET_MODULE as m
		ON
			ml.ModuleID = m.ModuleID
		WHERE 
			ml.ModuleID = '".$ModuleID."'
		GROUP BY
			ml.ModuleID";
			
$licenseRS 	= $objModule->returnArray($sql);

if(sizeof($licenseRS) == 1)
{
	$moduleTotalLicense = $licenseRS[0]["NumberOfLicense"];
	$moduleCode 		= $licenseRS[0]["Code"];
	$moduleDesc 		= $licenseRS[0]["Description"];
}
else
{
	//SUPPOSE  THERE SHOULD BE ONLY ONE MODULE ID FOR THE GROUPING
	echo "Invalid Module or Error Module Code , Program exit!!";
	exit();
}

$hasInvalidQuota = false;
####################################################

# Check the module is amBook or not
$sql = "SELECT Code FROM INTRANET_MODULE WHERE ModuleID = '".$ModuleID."'";
$Is_amBook = current($objModule->returnVector($sql))=='ambook'? true : false;
$Is_poemsandsongs = current($objModule->returnVector($sql))=='poemsandsongs'? true : false;
$Is_gvlistening = current($objModule->returnVector($sql))=='gvlistening'? true : false;
$Is_rsreading = current($objModule->returnVector($sql))=='rsreading1'? true : false;

# Generate UI
$rsCount	= count($licenseHistoryRS);
$BgColor 	= '#EEE'; 
$curColor 	= "#EEE";

$table_html  = "<table width=\"100%\" height=\"95%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">";

if($Is_amBook){
	$table_html .= "<col width=\"8%\"\>
					<col width=\"30%\"\>
					<col width=\"23%\"\>
					<col width=\"23%\"\>
					<col width=\"16%\"\>";	
}
else if($Is_poemsandsongs||$Is_gvlistening||$Is_rsreading){
	$table_html .= "<col width=\"8%\"\>
					<col width=\"30%\"\>
					<col width=\"30%\"\>
					<col width=\"30%\"\>";
}
else{
	$table_html .= "<col width=\"8%\"\>
					<col width=\"46%\"\>
					<col width=\"46%\"\>";
}

$table_html .= "	<tr style=\"font-weight:bold;background:#CCC;\" height=\"30px\">
						<td>&nbsp;</td>
						<td>InputDate</td>";

if($Is_amBook){
	$table_html .= "	<td>No. of Units</td>
						<td>No. of Students</td>";
}
else if($Is_poemsandsongs||$Is_gvlistening||$Is_rsreading){
	$table_html .= "	<td>Expiry Date</td>";
}

$table_html .= "		<td>Quota</td>
					</tr>";

$expiredLicense = 0;

for($i=0; $i<$rsCount; $i++){
	$curColor = ($curColor == "")? $BgColor : "";		
	$_licenseInputDate = $licenseHistoryRS[$i]["InputDate"];
	$_numberOfUnit     = $licenseHistoryRS[$i]["NumberOfUnit"];
	$_numberOfStudents = $licenseHistoryRS[$i]["NumberOfStudents"];
	$_numberOfLicense  = $licenseHistoryRS[$i]["NumberOfLicense"];
	$_expiryDate	   = $licenseHistoryRS[$i]["ExpiryDate"];
	$_licenseIsValid   = $licenseHistoryRS[$i]["LicenseIsValid"];
	$_IsValidStatus    = "";
	$_no = $i +1;
	
	if($_licenseIsValid == 1){
		$_IsValidStatus = "";
	}
	else{
		$hasInvalidQuota = true;
		$_IsValidStatus = "<font color = \"red\">&nbsp;*</font>";
		
		if($_numberOfLicense > 0){
			//ONLY MINUS THE LICENSE IF THE LICENSE QUOTA IS LARGER THAN 0
			$moduleTotalLicense = $moduleTotalLicense - $_numberOfLicense;
		}
	}
	
	# Calculate Expired
	if($Is_poemsandsongs||$Is_gvlistening||$Is_rsreading){
		if(!empty($_expiryDate)&&strtotime($_expiryDate)<time()){
			$expiredLicense += $_numberOfLicense;
		}
	}
	
	$table_html .= "<tr style=\"background:$curColor\" valign=\"top\">
						<td height=\"1\">$_no</td>
						<td>$_licenseInputDate</td>";
	
	if($Is_amBook){
		$table_html .= "<td>$_numberOfUnit</td>
						<td>$_numberOfStudents</td>";
	}
	else if($Is_poemsandsongs||$Is_gvlistening||$Is_rsreading){
		$table_html .= "<td>$_expiryDate</td>";
	}					
							
	$table_html .= "	<td>$_numberOfLicense $_IsValidStatus</td>
				   	</tr>";
}

$column_no   = $Is_amBook? 5 : ($Is_poemsandsongs||$Is_gvlistening||$Is_rsreading? 4:3);
$table_html .= "	<tr><td colspan=\"$column_no\">&nbsp;</td></tr>
					<tr><td colspan=\"$column_no\" height=\"20px\">&nbsp;</td></tr>		
					<tr height=\"30\" style=\"font-weight:bold;\" valign=\"bottom\">
						<td colspan=\"$column_no\"  align=\"center\" >
							Enabled Quota :&nbsp;$moduleTotalLicense";

if($Is_poemsandsongs||$Is_gvlistening||$Is_rsreading){
	$table_html .= "		&nbsp;&nbsp;&nbsp;&nbsp;Expired Quota :&nbsp;$expiredLicense";
	$table_html .= "		&nbsp;&nbsp;&nbsp;&nbsp;Quota Left :&nbsp;".($moduleTotalLicense - $expiredLicense);
}

$table_html .= "		</td>
					</tr>
					<tr height=\"30\"valign=\"bottom\">
						<td colspan=\"$column_no\" align=\"right\" >
							<HR />";

if($hasInvalidQuota){
	$table_html .= "		<font color='red'>Note: (*) Quota does not match original, it has been modified.</font>";
}

$table_html .= "		</td>
					</tr>		
				</table>";


####################################################

?>
<div style="height:100%;">
	<div style="height:10%;padding:0 0 0 10px;">
		<b>Module Code</b> : <?=$moduleCode?><BR />
		<b>Module Description</b> : <?=$moduleDesc?><BR />
		<br/>
	</div>
	<div id="display_content" style="height:80%;padding:0 0 0 10px;overflow-y:auto;">
		<?=$table_html?>
	</div>
	<div id="btn_panel" style="height:10%;background:#EEE;">
		<table width="100%"><tr><td valign="center" align="center">
		<input type="button" name="close_modulelist" id="close_modulelist" value="Close" onClick="window.parent.tb_remove();"/>
		</td></tr></table>
	</div>
</div>
<?php
###########################################
intranet_closedb();
exit();
?>