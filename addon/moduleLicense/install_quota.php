<?php
# using: Thomas
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libIntranetModule.php");

intranet_opendb();
###########################################

$libIntranetModule = new libIntranetModule();
$linterface 	= new interface_html("popup5.html");
$getModuleIDList 	= (isset($_REQUEST['getModuleIDList']))? trim($_REQUEST['getModuleIDList']) : "";
$strModuleList 	= (isset($_REQUEST['strModuleList']))? trim($_REQUEST['strModuleList']) : "";
$enable 		= (isset($_REQUEST['enable']))? trim($_REQUEST['enable']) : "";

$aryModuleList = array();
$x = "";


if(!empty($getModuleIDList) && $strModuleList){
	## Get List of modules to be updated
	$sql = "SELECT
			 	*, 
			 	CODE as code			 	
			FROM 
				INTRANET_MODULE
			WHERE 
				MODULEID in (".$strModuleList.")";
	$aryModuleList = $libIntranetModule->returnArray($sql);
}
	
	
		$x .= '<div style="height:490px;width:100%;background:white;">';		
		$x .= '		<div id="display_content" style="height:425px;overflow-y:auto;">';
		$x .= '			<table width="100%" cellpadding="0" cellspacing="0">';
		
		if($aryModuleList != array()){
			$x .= '			<col width="5%"/>';
			$x .= '			<col width="15%"/>';
			$x .= '			<col width="15%"/>';
			$x .= '			<col width="15%"/>';
			$x .= '			<col width="15%"/>';
			$x .= '			<col width="15%"/>';
			$x .= '			<col width="15%"/>';
			$x .= '			<tr height="30px" style="font-weight:bold;background:#CCC;">';
			$x .= '				<td>#</td>';
			$x .= '				<td>'.$Lang['ModuleLicense']['ModuleCode'].'</td>';
			$x .= '				<td>'.$Lang['ModuleLicense']['Title'].'</td>';
			$x .= '				<td>No. of Units</td>';
			$x .= '				<td>No. of Students</td>';
			$x .= '				<td>Expiry Date</td>';
			$x .= '				<td><input type="text" class="numeric" name="quota_all" id="quota_all" size="5"/><input type="button" name="apply_all" id="apply_all" value="Apply All" onClick="apply_all_quota();"/></td>';
			$x .= '			</tr>';
		
			foreach($aryModuleList as $b => $info){
				$x .= '		<tr height="30px">';
				$x .= '			<td>'.($b+1).'.</td>';
				$x .= '			<td>'.$info['code'].'</td>';
				$x .= '			<td>'.$info['Description'].'</td>';
				
				if($info['code']=='ambook'){
					$x .= '		<td>';
					$x .= '			<select name="NumberOfUnit['.$info['ModuleID'].']" onchange="calculate_amBook_quota('.$info['ModuleID'].')">';
					$x .= '				<option value="8">8</option>';
					$x .= '				<option value="16">16</option>';
					$x .= '				<option value="24">24</option>';
					$x .= '			</select>';
					$x .= '		</td>';
					$x .= '		<td>';
					$x .= '			<input type="text" class="numeric" name="NumberOfStudents['.$info['ModuleID'].']" size="3" onkeyup="calculate_amBook_quota('.$info['ModuleID'].')">';
					$x .= '		</td>';
				}
				else{
					$x .= '		<td>';
					$x .= '			N/A <input type="hidden" name="NumberOfUnit['.$info['ModuleID'].']" value="0"/>';
					$x .= '		</td>';
					$x .= '		<td>';
					$x .= '			N/A <input type="hidden" name="NumberOfStudents['.$info['ModuleID'].']" value="0"/>';
					$x .= '		</td>';
				}
				
				if(in_array($info['code'],$libIntranetModule->showExpiryDateModuleAry)){
					$x .= '		<td>';
					$x .= '			<script>';
					$x .= '				$(document).ready(function(){';
					$x .= '					$.datepick.setDefaults({showOn: \'both\', buttonImageOnly: true, buttonImage: \'/images/2009a/icon_calendar_off.gif\', buttonText: \'\', mandatory: true});';
					$x .= '					$(\'#ExpiryDate_'.$info['ModuleID'].'\').datepick({';
					$x .= '						dateFormat: \'yy-mm-dd\',';
					$x .= '						dayNamesMin: [\'S\', \'M\', \'T\', \'W\', \'T\', \'F\', \'S\'],';
					$x .= '						changeFirstDay: false,';
					$x .= '						firstDay: 0';
					$x .= '					});';
					$x .= '				});';
					$x .= '			</script>';
					$x .= '			<input type="text" id="ExpiryDate_'.$info['ModuleID'].'" name="ExpiryDate['.$info['ModuleID'].']" value="'.date('Y-m-d').'" size="10">';
					$x .= '		</td>';
				}
				else{
					$x .= '		<td>';
					$x .= '			N/A <input type="hidden" name="ExpiryDate['.$info['ModuleID'].']" value=""/>';
					$x .= '		</td>';	
				}
				
				$x .= '			<td>';
				$x .= '				<input type="text" class="'.($info['code']=='ambook'? '':'apply_all').' quota numeric" name="quota['.$info['ModuleID'].']" id="'.$info['ModuleID'].'" value="'.$info['NumberOfCopy'].'" size="5" '.($info['code']=='ambook'? 'readonly':'').'/>';
				$x .= '				<input type="hidden" name="code['.$info['ModuleID'].']" value="'.$info['code'].'"/>';
				$x .= '			</td>';
				$x .= '		</tr>';
			}
		}
		else{
			$x .= '			<tr><td><BR /><BR /><BR /><BR /><BR /><center>No modules selected</center</td></tr>';
		}
		
		$x .= '			</table>';
		$x .= '			</br>';
		$x .= '		</div>';
		$x .= '		<div id="btn_panel" style="height:80px;background:#EEE;text-align:center;">';
		$x .= '			<HR /><B>Total: '.count($aryModuleList).' Module(s) </b><BR /> <BR />';
		$x .= $aryModuleList != array()? '<input type="button" name="submit_modulelist" id="submit_modulelist" value="Confirm Update" onClick="confirm_update();"/>&nbsp;' : '';
		$x .= '			<input type="button" name="cancel_modulelist" id="cancel_modulelist" value="Cancel" onClick="window.parent.tb_remove();"/>';
		$x .= '		</div>';
		$x .= '</div>';
	


$linterface->LAYOUT_START();
?>

<!-- Date Picker JS and CSS -->
<script type="text/javascript" src="<?=$intranet_httppath?>/templates/jquery/jquery.datepick.js"></script>
<link rel="stylesheet" type="text/css" href="<?=$intranet_httppath?>/templates/jquery/jquery.datepick.css">

<script language="javascript">
	$(document).ready(function(){
				
		//Get ModuleID list
		if($("#getModuleIDList").val() == ""){
			var strModuleList = "";
			$("input[name=ModuleID[]][checked]", window.parent.document).each(function(){
				strModuleList += (strModuleList!="")? ",":"";
				strModuleList += $(this).val();
			});
			
			$("#strModuleList").val(strModuleList);
			$("#getModuleIDList").val(1);
			
			document.form1.submit();
		}
		
		$('.numeric').NumericOnly();
	});
	
	function get_quota_list(){
		var quota_list 			  = [];
		var ModuleID_list 		  = [];
		var NumberOfUnit_list 	  = [];
		var NumberOfStudents_list = [];
		var ExpiryDate			  = [];
		var Code_list			  = [];
		var index=0;
		
		$('.quota').each(function() {
			
			quota_list[index] 		 	  = $(this).val();			
			ModuleID_list[index] 	 	  = $(this).attr('id');
			NumberOfUnit_list[index] 	  = $('[name=NumberOfUnit[' + $(this).attr('id') + ']]').val();
			NumberOfStudents_list[index]  = $('[name=NumberOfStudents[' + $(this).attr('id') + ']]').val();
			ExpiryDate[index]			  = $('[name=ExpiryDate[' + $(this).attr('id') + ']]').val();
			Code_list[index]			  = $('[name=code[' + $(this).attr('id') + ']]').val();
			
			index++;
		});
		
		var result=[quota_list, ModuleID_list, NumberOfUnit_list, NumberOfStudents_list, ExpiryDate, Code_list];
		return result;
	}
	
	function confirm_update(){
		var quota_check = true;
		$("input.quota").each(function(){
			if(!isInteger(Trim($(this).val()))){
				alert("Please enter numeric value for quota");
				$("#quota_all").focus();
				quota_check = false;
				return false;
			}
		});
		if(!quota_check){
			return;
		}			
					
		var quota_moduleID 		  = get_quota_list();
		var quota_list 			  = quota_moduleID[0];
		var strModuleID 		  = quota_moduleID[1];
		var NumberOfUnit_list 	  = quota_moduleID[2];
		var NumberOfStudents_list = quota_moduleID[3];
		var ExpiryDate_list		  = quota_moduleID[4];
		var Code_list			  = quota_moduleID[5];

		$.post('ajax.php', 
			{
				action					: 'update_module_quota',
				'aryModuleID[]'			: strModuleID,
				'aryModuleQuota[]'		: quota_list,
				'aryNumberOfUnit[]'		: NumberOfUnit_list,
				'aryNumberOfStudnets[]'	: NumberOfStudents_list,
				'aryExpiryDate[]'		: ExpiryDate_list,
				'aryCode[]'				: Code_list
			},
			function(data){
				$('body').html(data);				
				//$("#display_content").html(data);
				//$("#btn_panel").html('<input type="button" name="close_modulelist" id="close_modulelist" value="close" onClick="close_and_refresh_parent();"/>');
			});
	}
	
	function close_and_refresh_parent(){
		window.parent.location.reload();
		window.parent.tb_remove();
	}
	
	function apply_all_quota(){
		var quote_amt = Trim($("#quota_all").val());
		if( quote_amt== "" || !isInteger(quote_amt)){
			alert("Please enter numeric value for quota");
			$("#quota_all").val("");
			$("#quota_all").focus();
			return;
		}
		
		if(confirm("Are you sure you want to apply quota to all modules?")){
			$("input.apply_all").val($("#quota_all").val());
		}		
	}
	
	function calculate_amBook_quota(ModuleID){
		var NumberOfUnit 	 = $('[name=NumberOfUnit['+ModuleID+']]').val();
		var NumberOfStudents = $('[name=NumberOfStudents['+ModuleID+']]').val();
		
		if(NumberOfUnit!='' && isInteger(NumberOfUnit) && NumberOfStudents!='' && isInteger(NumberOfStudents)){
			var quota = NumberOfUnit * NumberOfStudents; 
			$('[name=quota['+ModuleID+']]').val(quota);
		}
		else{
			$('[name=quota['+ModuleID+']]').val('');
		}
	}
	
	$.fn.NumericOnly = function() {
		return this.each(function(){
			$(this).keydown(function(e){
				var key = e.charCode || e.keyCode || 0; 
				// allow backspace, tab, delete, arrows, minus , numbers and keypad numbers ONLY 
				return (
						key == 8 ||
						key == 9 ||
						key == 46 ||
						key == 109 ||
						key == 189 ||
						(key >= 37 && key <= 40) ||
						(key >= 48 && key <= 57) ||
						(key >= 96 && key <= 105));
			})
		}) 
	};
</script>
<form name="form1" method="post" style="margin:0px;padding:0px">

<?= $x ?>

<input type="hidden" name="getModuleIDList" id="getModuleIDList" value="<?=$getModuleIDList?>" />
<input type="hidden" name="strModuleList" id="strModuleList" value="<?=$strModuleList?>" />

</form>

<?php

$linterface->LAYOUT_STOP();

###########################################
intranet_closedb();
?>