<?php
//modifying By 
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libIntranetModule.php");


//include_once($PATH_WRT_ROOT."includes/libportal.php");

intranet_auth();
intranet_opendb();

$mid = trim($mid);  // get the module Id
$quota = trim($quota); // user set quota value
$action =trim($action);
if(!is_numeric($mid))
{
	echo "Module format error [".$mid."]<br/>";
	exit();
}
if(!is_numeric($quota))
{
	echo "quotaEnable quota format error ";
	exit();
}

$objModule = new libintranetmodule($mid);

switch ($action){
	case "resetQuota":
		$objModule->resetTotalQuota($quota);
		break;
	case "resetInuseQuota":
		$objModule->resetQuotaInUse($quota);
		break;
	case "buyQuota":
		$objModule->registerQuota($quota);
		break;
	case "releaseQuota":
		$objModule->deregisterQuota($quota);
		break;
	case "updateTotalQuotaDirectly":
		$sql = "update INTRANET_MODULE set TotalQuota = {$quota} where ModuleID ={$mid}";
		$objModule->db_db_query($sql);
		break;		
	case "updateInUseQuotaDirectly":
		$sql = "update INTRANET_MODULE set QuotaInUse  = {$quota} where ModuleID ={$mid}";
		$objModule->db_db_query($sql);
		break;		
	case "operateQuota":
		$objModule->updateEnableQuota($quota);
		break;

}




include_once("templates/update_quota.tmpl.php");

intranet_closedb();
exit();
