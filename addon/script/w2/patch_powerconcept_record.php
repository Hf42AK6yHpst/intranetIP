<?php
// Editing by Siuwan
/*
 * S87239 - 香港浸會大學附屬學校王錦輝中小學 (中學部) - Cannot load the concept Map in some article
 */
$PATH_WRT_ROOT = '../../../';
include_once($PATH_WRT_ROOT.'includes/global.php');
include_once($PATH_WRT_ROOT.'includes/libdb.php');



intranet_opendb();
include_once($PATH_WRT_ROOT."includes/w2/w2Config.inc.php");

$li = new libdb();

$sql = "SELECT course_id FROM ".$eclass_db.".course WHERE RoomType = '".$w2_cfg['w2ClassRoomType']."'";
$w2_classRoomID = current($li->returnVector($sql));

if($w2_classRoomID>0){
	$w2_classRoomDB = $eclass_prefix.'c'.$w2_classRoomID;
	$sql = "
			SELECT 
				CONTENT_CODE,SCHEME_CODE
			FROM 
				".$eclass_db.".W2_DEFAULT_CONTENT 
			WHERE 
				SCHEME_CODE NOT LIKE '%\_%' AND SETTING_NAME = 'conceptMapContent' 
			ORDER BY 
				CONTENT_CODE , SCHEME_CODE";
	$records = $li->returnResultSet($sql);
	$record_count = count($records);
	echo '[Begin patch at '.date("Y-m-d H:i:s").']<br />'."\n";
	echo 'Total number of default powerconcept is '.$record_count."<br>\n";
	if($record_count>0){
		echo 'Patching default powerconcept in progress: <br />'."\n";
		for($i = 0, $i_max = $record_count;$i< $i_max; $i++){
			$contentCode = $records[$i]['CONTENT_CODE'];
			$schemeCode = $records[$i]['SCHEME_CODE'];
			$powerconceptName = 'W2_DEFAULT_'.strtoupper($contentCode).'_'.strtoupper($schemeCode);
			$_delete = 'delete from '.$w2_classRoomDB.'.powerconcept where Name = \''.$powerconceptName.'\';';
			$_insert = 'insert into '.$w2_classRoomDB.'.powerconcept(Name,DataContent,InputDate,ModifiedDate,user_id,group_id,assignment_id,plan_section_id,MarkedDataContent,MarkedDate) values (\''.$powerconceptName.'\',\'\',now(),now(),0,0,0,NULL,NULL,NULL);';
			$_update = 'update '.$w2_classRoomDB.'.powerconcept set DataContent = (SELECT SETTING_VALUE from '.$eclass_db.'.W2_DEFAULT_CONTENT where setting_name = \'conceptMapContent\' and content_code = \''.$contentCode.'\' and scheme_code = \''.$schemeCode.'\') where Name = \''.$powerconceptName.'\';';
			
			$runSQL[] = $_delete;
			$runSQL[] = $_insert;
			$runSQL[] = $_update;
		
		}
		$runResultWithError = 0;
		$runResultOK = 0;
		
		for($i = 0 , $i_max = count($runSQL); $i < $i_max; $i++){
			$_sql = $runSQL[$i];
			$_return=$li->db_db_query($_sql);
			echo $_sql.'<br/><br/>';
			if(!$_return){
				$_display = '<font color="Red">Fail</font>';
				echo 'Run : '.$_sql.' Return '.$_display.'<br/>';
			}
			
			
		}
	}

	$schoolCode = strtolower($config_school_code);
	$sql = "
			SELECT 
				r_contentCode,schemeNum
			FROM 
				".$intranet_db.".W2_CONTENT_DATA
			WHERE 
				schoolCode = '".$schoolCode."' AND deletedby IS NULL AND conceptType = 'powerconcept' AND powerConceptID IS NOT NULL AND status = 1
			ORDER BY 
				r_contentCode , schemeNum";
	$records = $li->returnResultSet($sql); //ip20b5alpha_scheme1050
	$record_count = count($records);
	if($record_count>0){
		$ori_ary = array();
		echo 'Patching content input powerconcept in progress: <br />'."\n";
		for($i = 0, $i_max = $record_count;$i< $i_max; $i++){
			$contentCode = $records[$i]['r_contentCode'];
			$schemeNum = $records[$i]['schemeNum'];
			$ori_ary[] = strtolower($w2_cfg["schoolCodeCurrent"])."_scheme".$schemeNum;
		}
		$sql = "
			SELECT 
				CONTENT_CODE,SCHEME_CODE
			FROM 
				".$eclass_db.".W2_DEFAULT_CONTENT 
			WHERE 
				SCHEME_CODE IN ('".implode("','",$ori_ary)."') AND SETTING_NAME = 'conceptMapContent'
			ORDER BY 
				CONTENT_CODE , SCHEME_CODE";
		$records = $li->returnResultSet($sql);
		$record_count = count($records);
		echo 'Total number of content input with wrong school code is '.$record_count."<br>\n";
		$runSQL = array();
		for($i = 0, $i_max = $record_count;$i< $i_max; $i++){
			$schemeCode = strtoupper($records[$i]['SCHEME_CODE']); //bl_w2_scheme17
			$contentCode = $records[$i]['CONTENT_CODE'];
			$correct_scheme_code = str_replace(strtolower($w2_cfg["schoolCodeCurrent"]),strtolower($schoolCode),strtolower($schemeCode)); //ip20b5alpha_scheme17
			$ori_pc_name = 'W2_DEFAULT_'.strtoupper($contentCode.'_'.$schemeCode);
			$correct_pc_name = 'W2_DEFAULT_'.strtoupper($contentCode.'_'.$correct_scheme_code);
			$_updateDefaultContent = "UPDATE ".$eclass_db.".W2_DEFAULT_CONTENT SET SCHEME_CODE = '".$correct_scheme_code."' WHERE SCHEME_CODE = '".$schemeCode."'";
			$_updateClassroomPC = "UPDATE ".$w2_classRoomDB.".powerconcept SET Name = '".$correct_pc_name."' WHERE Name = '".$ori_pc_name."'";
			$runSQL[] = $_updateDefaultContent;
			$runSQL[] = $_updateClassroomPC;
		}
		$runResultWithError = 0;
		$runResultOK = 0;
			
		for($i = 0 , $i_max = count($runSQL); $i < $i_max; $i++){
			$_sql = $runSQL[$i];
			echo $_sql.'<br/><br/>';
		
			$_return=$li->db_db_query($_sql);
			if(!$_return){
				$_display = '<font color="Red">Fail</font>';
				echo 'Run : '.$_sql.' Return '.$_display.'<br/>';
			}
			
		}
			
	}

		
	
	
	echo '[End patch at '.date("Y-m-d H:i:s").']<br />'."\n";

}else{
	echo 'No classroom found.<br />'."\n";
}
intranet_closedb();
?>