<?php
/**
 * This script is to count the times of student logged into classroom within period, e.g.
 *      Classroom 1: [studentA, studentB, studentC, studentA] (studentA login into classroom twice)
 *      Classroom 2: [studentA, studentC, studentE]
 *      Classroom 3: [studentD]
 *  => The result will return this object,
 *     {
 *        'cnt': 8, //students that login into classroom
 *        'result': (cnt is >= $_POST['student_count'])
 *     }
 *
 * $_POST['start_date']     : [string YYYY-MM-DD] The start date of the period
 * $_POST['end_date']       : [string YYYY-MM-DD] The end date of the period
 * $_POST['student_count']  : [int] The minimum number of student login to classroom within period
 * $_POST['timestamp']      : [int] time()
 * $_POST['details']        : [Optional bool] show the number of student that have login into classroom (group by classroom id)
 * $_POST['method']         : [char 'A' or 'B'] See $_POST['key'] for details
 *
 * $_POST['key'] (Method A): The encrypted key
 *      $key = encrypt_string(
 *          $json->encode( $data )
 *      )
 * $_POST['key'] (Method B): The encrypted key
 *      $key  = base64_encode(
 *      	aes_cbc_128_encrypt(
 *      		$json->encode( $data ),
 *      		SECRET
 *      	)
 *      );
 */
$PATH_WRT_ROOT = "../../";
include_once( $PATH_WRT_ROOT . "includes/global.php" );
include_once( $PATH_WRT_ROOT . "includes/libdb.php" );
include_once( $PATH_WRT_ROOT . "includes/json.php" );

intranet_opendb();

$json = new JSON_obj();
$db = new libdb();
define('SECRET', '4am4OYQpJa2jLNprnP1EtE8QYOM04xFF');

//// Check access right START ////
# validate token
if ( ! $_POST['key'] || $_POST['method'] != 'A' & $_POST['method'] != 'B') {
    header( "HTTP/1.1 401 Unauthorized" );
    echo 'ERR-401001: Unauthorized';
    exit;
}

# get POST data
$key = $_POST['key'];
$method = $_POST['method'];


# descrypt key
if($method == 'A')
{
    $decryptJson = decrypt_string($key);
}
else if($method == 'B')
{
    $decryptJson = aes_cbc_128_decrypt( base64_decode($_POST['key']), SECRET );              // It works if client site installed mcrypt library on php
}

# valid descrypted data
if ( ! $decryptJson ) {
    header( "HTTP/1.1 401 Unauthorized" );
    echo 'ERR-401002: Unauthorized';
    exit;
}
$keyArr = $json->decode( $decryptJson );

# validate POST parameters
$checkFieldArr = array(
    'start_date',
    'end_date',
    'student_count'
    // 	,'timestamp'
);

foreach ( $checkFieldArr as $checkField ) {
    if ( ! isset( $_POST[ $checkField ] ) || $_POST[ $checkField ] != $keyArr[ $checkField ] ) {
        header( "HTTP/1.1 401 Unauthorized" );
        echo 'ERR-401003: Invalid request';
        exit;
    }
}


# Get Data
$start_date     = (isset($_POST['start_date']) && $_POST['start_date'] != '') ? $_POST['start_date'] : '';
$end_date       = (isset($_POST['end_date']) && $_POST['end_date'] != '') ? $_POST['end_date'] : '';
$student_count  = (isset($_POST['student_count']) && $_POST['student_count'] != '') ? (int)$_POST['student_count'] : 0;


# validate the value of paramaters
if($start_date == '' || $end_date == '' || $student_count <= 0)
{
    header( "HTTP/1.1 400 Bad Request" );
    echo 'ERR-400001: Invalid request';
    exit;
}


# validate token lifetime - valid for 30 seconds
// if ( time() - IntegerSafe( $_POST['timestamp'] ) > 300 ) {
if ( time() - IntegerSafe( $keyArr['timestamp'] ) > 30 ) {
    header( "HTTP/1.1 410 Gone" );
    echo 'ERR-410001: Gone';
    exit;
}
//// Check access right END ////

/* */
if ( ! function_exists( "classNamingDB" ) ) {
    function classNamingDB( $cid ) {
        global $eclass_db, $eclass_prefix;
        // echo $cid." ".$eclass_db;
        if ( $cid == $eclass_db ) {
            return $eclass_db;
        } else {
            return $eclass_prefix . "c" . $cid;
        }
    }
}

//// Get all classroom START ////
$sql       = "SELECT course_id FROM {$eclass_db}.course where roomtype = 0";
$courseIds = $db->returnVector( $sql );
//// Get all classroom END ////

//// Get all student login in period START ////
$accessClassroomStudentIdMapping = array();
$accessClassroomStudentIdArr     = array();
if(!empty($courseIds)){
    foreach ( $courseIds as $courseId ) {
        $classDb = classNamingDB( $courseId );

        // consider all active and deleted users
        $sql = "SELECT 
            		U.intranet_user_id
            	FROM
            		{$classDb}.login_session LS
            	INNER JOIN
            		{$classDb}.usermaster U
            	ON
            		LS.user_id = U.user_id
            	WHERE
            		U.memberType = 'S'
            	AND
            		LS.inputdate BETWEEN '{$start_date} 00:00:00' AND '{$end_date} 23:59:59'
    	";
		$studentIdArr = $db->returnVector( $sql );

		$accessClassroomStudentIdMapping[ $courseId ] = count( $studentIdArr );
		$accessClassroomStudentIdArr                  = array_merge( $accessClassroomStudentIdArr, $studentIdArr );
    }
//     $accessClassroomStudentIdArr = array_unique( $accessClassroomStudentIdArr );
}
//// Get all student login in period START ////


## Return Response Data
if ( $_POST['details'] ) {
    echo $json->encode( $accessClassroomStudentIdMapping );
} else {
	echo $json->encode( array(
		'cnt' => count( $accessClassroomStudentIdArr),
		"result" => (count( $accessClassroomStudentIdArr) >= $student_count)
	));
}
