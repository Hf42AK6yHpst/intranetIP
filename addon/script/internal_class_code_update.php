<?
# using:  

$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include('../check.php');

intranet_opendb();

$x = '';
if ($flag==1)
{
	$x .= "internal_class_code_update.php .......... Start at ". date("Y-m-d H:i:s")."<br><br>\r\n";	

	$li = new libdb();

	############################################################
	# Internal Class Code Patch [Start]
	############################################################
	$x .= "===================================================================<br>\r\n";
	$x .= "Internal Class Code Patch [Start]</b><br>\r\n";
	$x .= "===================================================================<br><br>\r\n";
	
	$sql = "Select
					stc.SubjectGroupID, ayt.YearTermID, ayt.AcademicYearID, st.SubjectID, stc.InternalClassCode, stc.ClassTitleEn
			From
					SUBJECT_TERM_CLASS as stc
					Inner Join
					SUBJECT_TERM as st
					On (stc.SubjectGroupID = st.SubjectGroupID)
					Inner Join
					ACADEMIC_YEAR_TERM as ayt
					On (st.YearTermID = ayt.YearTermID)
			Where
					stc.InternalClassCode != ''
			";
	$SubjectGroupArr = $li->returnArray($sql);
	$numOfSubjectGroup = count($SubjectGroupArr);
	
	for ($i=0; $i<$numOfSubjectGroup; $i++)
	{
		$thisSubjectGroupID = $SubjectGroupArr[$i]['SubjectGroupID'];
		$thisYearTermID = $SubjectGroupArr[$i]['YearTermID'];
		$thisAcademicYearID = $SubjectGroupArr[$i]['AcademicYearID'];
		$thisSubjectID = $SubjectGroupArr[$i]['SubjectID'];
		$thisOldInternalClassCode = $SubjectGroupArr[$i]['InternalClassCode'];
		$thisClassTitleEn = $SubjectGroupArr[$i]['ClassTitleEn'];
		
		### Get the oldest student added in the class to determine the form class ID
		$sql = "Select
						min(stcu.DateInput), yc.YearClassID, yc.YearID, stcu.UserID
				From
						SUBJECT_TERM_CLASS_USER as stcu
						Inner Join
						YEAR_CLASS_USER as ytcu
						On (stcu.UserID = ytcu.UserID)
						Inner Join
						YEAR_CLASS as yc
						On (ytcu.YearClassID = yc.YearClassID)
				Where
						stcu.SubjectGroupID = '".$thisSubjectGroupID."'
						And
						yc.AcademicYearID = '".$thisAcademicYearID."'
				Group By
						stcu.SubjectGroupID
				";
		$StudentInfoArr = $li->returnArray($sql);
		
		
		$thisYearID = $StudentInfoArr[0]['YearID'];
		$thisYearClassID = $StudentInfoArr[0]['YearClassID'];
		$thisNewInternalClassCode = $thisAcademicYearID.'-'.$thisYearTermID.'-'.$thisSubjectID.'-'.$thisYearID.'-'.$thisYearClassID;
		
		$x .= "# ".($i + 1)."<br>\r\n";
		$x .= "Subject Group: ".$thisClassTitleEn."<br>\r\n";
		$x .= "SubjectGroupID: ".$thisSubjectGroupID."<br>\r\n";
		$x .= "Old InternalClassCode: ".$thisOldInternalClassCode."<br>\r\n";
		$x .= "New InternalClassCode: ".$thisNewInternalClassCode."<br>\r\n";
		
		$sql = "Update SUBJECT_TERM_CLASS Set InternalClassCode = '".$thisNewInternalClassCode."' Where SubjectGroupID = '".$thisSubjectGroupID."'";
		$SuccessArr[$thisSubjectGroupID] = $li->db_db_query($sql);
		
		$x .= $sql."<br>\r\n";;

		if ($SuccessArr[$thisSubjectGroupID])
			$x .= "<font color='blue'><b>Success</b></font><br><br>\r\n";
		else
			$x .= "<font color='red'><b>Failed</b></font><br><br>\r\n";
	}
	
	$x .= "===================================================================<br>\r\n";
	$x .= "Internal Class Code Patch [End]</b><br>\r\n";
	$x .= "===================================================================<br>\r\n";
	############################################################
	# Internal Class Code Patch [End]
	############################################################
	
	
	
	$x .= "<br>\r\n<br>\r\nEnd at ". date("Y-m-d H:i:s")."<br><br>";	
	echo $x;

	
	#  save log
	$lf = new libfilesystem();
	$log_folder = $PATH_WRT_ROOT."file/migratelog/internal_class_code_update";
	if(!file_exists($log_folder))
		mkdir($log_folder);
	$log_file = $log_folder."/data_patch_log_". date("YmdHis") .".html";
	$lf->file_write($x, $log_file);
	echo "<br>File Log: ". $log_file;
	

}
else
{ ?>
	<html>
	<body>
	<font color=red size=+1>[Please Make DB Backup First]</font><br><br>
	
	This page will update the Internal Class Code of the Subject Group if the group is created by "Quick-Create"<br />
	<br /><br />
		
	<a href=?flag=1>Click here to proceed.</a> <br><br>
	
	<br>
	</body></html>

	
	
<? }

intranet_closedb();
 ?>
	