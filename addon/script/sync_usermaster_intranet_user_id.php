<?php
/*
 * Editing by Siuwan
*
* ****************** Important Note *****************
* Aim: update usermaster and user_course intranet_user_id with INTRANET_USER intranet_user_id
* To run the script, use Putty/Command Line to run "php {path_to_script}/sync_usermaster_intranet_user_id.php --apply"
* ***************************************************
*/
set_time_limit(144000);
$PathRelative = '../../';
include_once($PathRelative.'includes/global.php');
include_once($PathRelative.'includes/libdb.php');

intranet_opendb();
$li = new libdb();
$li->db = $intranet_db;

$options = getopt('',array("apply"));
$applyMode = isset($options['apply']);
## Init
$targetCourseUsersAry = array();
$userEmailsAry = array();

## Preparation
# get course_id from course
$sql = "SELECT course_id FROM ".$eclass_db.".course WHERE RoomType = 0";
$courseIds = $li->returnVector($sql);
$courseIdsNum = count($courseIds);
# Main
if($courseIdsNum > 0){
    for($i=0 ; $i<$courseIdsNum ; $i++){
        # search for null intranet_user_id record in usermaster
        $li->db = $eclass_prefix."c".$courseIds[$i];
        if(mysql_select_db($li->db)){
            $sql = "SELECT user_id, user_email FROM usermaster WHERE status IS NULL AND intranet_user_id IS NULL OR intranet_user_id = ''";
            $userRecords = $li->returnArray($sql);
            foreach((array)$userRecords as $userRecord){
                $targetCourseUsersAry[$userRecord['user_email']][] = array("courseId"=>$courseIds[$i], "userId"=>$userRecord['user_id']);
                $userEmailsAry[] = $userRecord['user_email'];
            }
        }
    }

    # get intranet_user_id in INTRANET_USER
    $li->db = $intranet_db;
    $userEmailsStr = implode("','",$userEmailsAry);
    $sql = "SELECT UserID, UserEmail FROM ".$intranet_db.".INTRANET_USER WHERE UserEmail IN ('".$userEmailsStr."')";
    $intranetUserRecords = $li->returnArray($sql);
    $intranetUserRecordsNum = count($intranetUserRecords);
    echo "Number of intranet users to be updated : ".$intranetUserRecordsNum.PHP_EOL;;
    $numberOfUpdatedUsers = 0;
    for($i=0 ; $i<$intranetUserRecordsNum ; $i++){
        $IntranetUserID = $intranetUserRecords[$i]['UserID'];
        $UserEmail = $intranetUserRecords[$i]['UserEmail'];
        $userCourseRecords = $targetCourseUsersAry[$UserEmail];

        $j = 1;
        foreach((array)$userCourseRecords as $userCourseRecord){
            #update user_course
            $sql1 = "UPDATE ".$eclass_db.".user_course SET intranet_user_id = '".$IntranetUserID."' WHERE course_id = '".$userCourseRecord['courseId']."' AND user_id = '".$userCourseRecord['userId']."'";
            if($applyMode){
                $updateResult = $li->db_db_query($sql1);
            }
            #update usermaster
            $li->db = $eclass_prefix."c".$userCourseRecord['courseId'];
            $sql2 = "UPDATE usermaster SET intranet_user_id = '".$IntranetUserID."' WHERE user_id = '".$userCourseRecord['userId']."'";
            if($applyMode){
                $updateUserMasterResult = $li->db_db_query($sql2);
                $updateResult = $updateResult && $updateUserMasterResult;
            }
            $j++;
        }
        $numberOfUpdatedUsers += $updateResult ? 1 : 0;
        $li->db = $intranet_db;

    }
    if($applyMode){
        echo ($numberOfUpdatedUsers === $intranetUserRecordsNum ? "SUCCESS" : "FAIL").PHP_EOL;
    }
}

#########################################################################################################
intranet_closedb();
?>