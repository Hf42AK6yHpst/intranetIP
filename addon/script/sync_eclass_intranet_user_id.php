<?
/*
 * Editing by Pun 
 * 
 * ****************** Important Note *****************
 * Aim: update eclass intranet_user_id with INTRANET_USER UserID
 * 		- use user_email as key [tb: user_course]
 * ***************************************************
 */

$PathRelative = '../../';
include_once($PathRelative.'includes/global.php');
include_once($PathRelative.'includes/libdb.php');

intranet_auth();
intranet_opendb();
#############################################################################
## Tseting only
#############################################################################
$debug_mode = true;
$applyMode = ($_GET['applyMode'] == 1);
$li = new libdb();
$li->db = $intranet_db;

## Init
$intranetUserArr = array();
$intrnaetClassMap = array();
$intrnaetUIDMap = array();



## Preparation
# Get Intranet All User Info
$sql = "SELECT * FROM ".$intranet_db.".INTRANET_USER";
$userInfo = $li->returnArray($sql);

if(count($userInfo) > 0){
	for($i=0 ; $i<count($userInfo) ; $i++){
		# prepare Intranet UserID Mapping Array
		$intrnaetUIDMap[$userInfo[$i]['UserEmail']] = $userInfo[$i];
	}
}

# Get eClass All user_course records which do not have intranet_user_id
$sql = "select uc.* from ".$eclass_db.".user_course uc INNER JOIN ".$intranet_db.".INTRANET_USER iu ON uc.user_email = iu.UserEmail where uc.intranet_user_id IS NULL OR uc.intranet_user_id = 0 ORDER BY uc.course_id";
$usercourseArr = $li->returnArray($sql);
//debug_r($sql);

# Main
$x  = '';
$x .= '<div style="padding:10px;">';
$x .= '<table cellpadding="4" cellspacing="0" border="1" width="100%">';
$x .= '<tr>';
$x .= '<th>&nbsp;</th>';
$x .= '<th>&nbsp;</th>';
$x .= '<th colspan="3">Intranet</th>';
$x .= '<th style="background-color:gray;width:10px;">&nbsp;</th>';
$x .= '<th colspan="3">eClass</th>';
$x .= '<th>&nbsp;</th>';
if($applyMode){
    $x .= '<th>&nbsp;</th>';
}
$x .= '</tr>';
$x .= '<tr>';
$x .= '<th>#</th>';
$x .= '<th>CourseID</th>';
$x .= '<th>UserInfo</th>';
$x .= '<th>Class No.</th>';
$x .= '<th>Email</th>';
$x .= '<th style="background-color:gray;width:10px;">&nbsp;</th>';
$x .= '<th>name</th>';
$x .= '<th>class_number</th>';
$x .= '<th>user_email</th>';
$x .= '<th>SQL</th>';
if($applyMode){
    $x .= '<th>Apply Result</th>';
}
$x .= '</tr>';

$j = 1;
if(count($usercourseArr) > 0){
	for($i=0 ; $i<count($usercourseArr) ; $i++){
		
		$user_course_id 	= $usercourseArr[$i]['user_course_id'];
		$course_id 			= $usercourseArr[$i]['course_id'];
		$e_intranet_user_id	= $usercourseArr[$i]['intranet_user_id'];
		$e_user_id 			= $usercourseArr[$i]['user_id'];
		$e_class_number 	= $usercourseArr[$i]['class_number'];
		$e_fiirtname 		= $usercourseArr[$i]['firstname'];
		$e_chinesename		= $usercourseArr[$i]['chinesename'];
		$e_user_email		= $usercourseArr[$i]['user_email'];
		$e_dob				= $usercourseArr[$i]['dob'];
		# Intranet Info
		if(true){
			$i_userid 		= $intrnaetUIDMap[$e_user_email]['UserID'];
			$i_englishname 	= $intrnaetUIDMap[$e_user_email]['EnglishName'];
			$i_chinesename 	= $intrnaetUIDMap[$e_user_email]['ChineseName'];
			$i_useremail 	= $intrnaetUIDMap[$e_user_email]['UserEmail'];
			$i_classname 	= $intrnaetUIDMap[$e_user_email]['ClassName'];
			$i_classno 		= $intrnaetUIDMap[$e_user_email]['ClassNumber'];
			$i_dob 			= $intrnaetUIDMap[$e_user_email]['DateOfBirth'];
		}

		# update user_course
		$sql1 = "update ".$eclass_db.".user_course SET 
					intranet_user_id = '".$i_userid."'
				WHERE user_course_id = '".$user_course_id."'";
		if($applyMode){
			$sql1_result = $li->db_db_query($sql1);
		}

		$x .= '<tr>';
		$x .= '<td>'.$j.'</td>';
		$x .= '<td>'.$course_id.'</td>';
		$x .= '<td>'.$i_englishname.'<br>'.$i_chinesename.'<br>'.$i_userid.'<br>'.$i_dob.'</td>';
		$x .= '<td>'.$i_classname.' - '.$i_classno.'</td>';
		$x .= '<td>'.$i_useremail.'</td>';
		$x .= '<td style="background-color:gray;width:10px;">&nbsp;</td>';
		$x .= '<td>'.(($e_fiirtname != '') ? $e_fiirtname : 'NULL').'<br>'.
					 (($e_chinesename != '') ? $e_chinesename : 'NULL').'<br>'.
					 (($e_intranet_user_id != '') ? $e_intranet_user_id : 'NULL').'<br>'.
					 (($e_dob != '') ? $e_dob : 'NULL').
			  '</td>';
		$x .= '<td>'.$e_class_number.'</td>';
		$x .= '<td>'.$e_user_email.'</td>';
		$x .= '<td>'.$sql1.'</td>';
		if($applyMode){
			$x .= '<td>'.(($sql1_result)?'OK':'Failed').'</td>';
		} else {
			//$x .= '<td>--</td>';
		}
		$x .= '</tr>';
		$j++;
	}
		
}


$x .= '</table>';
$x .= '</div>';

#########################################################################################################
intranet_closedb();
?>
<style> 
html, body, table, select, input, textarea{ font-size:12px; }
</style>

<?php if($applyMode){ ?>
	<a href="<?=basename(__FILE__)?>?applyMode=0">$applyMode = 0</a>
<?php }else{ ?>
	<a href="<?=basename(__FILE__)?>?applyMode=1">$applyMode = 1</a>
<?php } ?>
<?=$x?>