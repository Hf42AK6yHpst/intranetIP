<?php
// take away this in order to run the script.
die();

// using 
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

#################################################################
##################### Script Config [START] #####################
#################################################################
$debugMode = true;
$adminStaffGroupId = 3;
$teachingStaffGroupId = 1;
$parentGroupId = 4;
$studentGroupID = 2;
#################################################################
###################### Script Config [END] ######################
#################################################################


if (!$flag) {
	echo '<html><br><a href=?flag=1>Click here to proceed</a> <br><br><br></body></html>';
	die();
}

intranet_opendb();

$db = new libdb();
$successAry = array();

$db->Start_Trans();


// Admin Staff
$sql = "SELECT UserID FROM INTRANET_USER WHERE RecordType=1 AND Teaching = 0";
$allTargetUserIdAry = $db->returnVector($sql);
$sql = "SELECT UserID FROM INTRANET_USERGROUP WHERE GroupID = '".$adminStaffGroupId."'";
$inGroupUserId = $db->returnVector($sql);
$missedUserIdAry = array_values(array_diff($allTargetUserIdAry, $inGroupUserId));
$numOfMissedUser = count($missedUserIdAry);
for ($i=0; $i<$numOfMissedUser; $i++) {
	$sql = "Insert Into INTRANET_USERGROUP (GroupID, UserID, DateInput, DateModified) Values ('".$adminStaffGroupId."', '".$missedUserIdAry[$i]."', now(), now())";
	if ($debugMode) {
		debug_pr($sql);
	}
	else {
		$successAry['nonTeachingStaff'][] = $db->db_db_query($sql);
	}	
}

// Teaching Staff
$sql = "SELECT UserID FROM INTRANET_USER WHERE RecordType=1 AND Teaching = 1";
$allTargetUserIdAry = $db->returnVector($sql);
$sql = "SELECT UserID FROM INTRANET_USERGROUP WHERE GroupID = '".$teachingStaffGroupId."'";
$inGroupUserId = $db->returnVector($sql);
$missedUserIdAry = array_values(array_diff($allTargetUserIdAry, $inGroupUserId));
$numOfMissedUser = count($missedUserIdAry);
for ($i=0; $i<$numOfMissedUser; $i++) {
	$sql = "Insert Into INTRANET_USERGROUP (GroupID, UserID, DateInput, DateModified) Values ('".$teachingStaffGroupId."', '".$missedUserIdAry[$i]."', now(), now())";
	if ($debugMode) {
		debug_pr($sql);
	}
	else {
		$successAry['teachingStaff'][] = $db->db_db_query($sql);
	}	
}

// Parent
$sql = "SELECT UserID FROM INTRANET_USER WHERE RecordType=3";
$allTargetUserIdAry = $db->returnVector($sql);
$sql = "SELECT UserID FROM INTRANET_USERGROUP WHERE GroupID = '".$parentGroupId."'";
$inGroupUserId = $db->returnVector($sql);
$missedUserIdAry = array_values(array_diff($allTargetUserIdAry, $inGroupUserId));
$numOfMissedUser = count($missedUserIdAry);
for ($i=0; $i<$numOfMissedUser; $i++) {
	$sql = "Insert Into INTRANET_USERGROUP (GroupID, UserID, DateInput, DateModified) Values ('".$parentGroupId."', '".$missedUserIdAry[$i]."', now(), now())";
	if ($debugMode) {
		debug_pr($sql);
	}
	else {
		$successAry['parent'][] = $db->db_db_query($sql);
	}	
}

// Student
$sql = "SELECT UserID FROM INTRANET_USER WHERE RecordType=2";
$allStudentUserIdAry = $db->returnVector($sql);
$sql = "SELECT UserID FROM INTRANET_USERGROUP WHERE GroupID = '".$studentGroupID."'";
$inGroupStudentUserId = $db->returnVector($sql);
$missedUserIdAry = array_values(array_diff($allStudentUserIdAry, $inGroupStudentUserId));
$numOfMissedUser = count($missedUserIdAry);
for ($i=0; $i<$numOfMissedUser; $i++) {
	$sql = "Insert Into INTRANET_USERGROUP (GroupID, UserID, DateInput, DateModified) Values ('".$studentGroupID."', '".$missedUserIdAry[$i]."', now(), now())";
	if ($debugMode) {
		debug_pr($sql);
	}
	else {
		$successAry['student'][] = $db->db_db_query($sql);
	}	
}

$db->UpdateRole_UserGroup();

debug_pr($successAry);
if($debugMode || in_multi_array(false,$successAry)) {
	echo "Add missed user to identity group FAILED";
	$db->RollBack_Trans();
}
else {
	echo "Add missed user to identity group Success";
	$db->Commit_Trans();
}

intranet_closedb();
?>