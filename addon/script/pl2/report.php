<?php
$PathRelative = '../../../';
include_once($PathRelative.'includes/global.php');
include_once($PathRelative.'includes/libdb.php');
if($plugin['power_lesson_2']){
# get the password from central server
$ch = curl_init();
curl_setopt($ch, CURLOPT_HEADER, 0);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_REFERER, $_SERVER["SERVER_NAME"]);
curl_setopt($ch, CURLOPT_URL, "http://eclassupdate.broadlearning.com/api/blpswd.php?ptype=3");

$password_central = trim(curl_exec($ch));
curl_close ($ch);

if ($password_central!="" && strlen($password_central)>3) {
    if(!isset($_SERVER['PHP_AUTH_USER']) || $_SERVER['PHP_AUTH_USER']!="broadlearning" || md5("2012allschools".$_SERVER['PHP_AUTH_PW'])!=$password_central) {
        $realm = "PowerLesson 2.0 Debugger ( ".strftime("%X %Z",time())." )";
        
        Header("WWW-Authenticate: Basic realm=\"".$realm."\"");
        Header("HTTP/1.0 401 Unauthorized");
        echo "Unauthorized Access\n";
        exit;
    }
}
    
intranet_opendb();
$li = new libdb();
$li->db = $intranet_db;
$startDate = isset($_POST['startDate']) && $_POST['startDate'] !== null ? $_POST['startDate'] : '' ;
$endDate = isset($_POST['endDate']) && $_POST['endDate'] !== null ? $_POST['endDate'] : '' ;
$searchDate = isset($_POST['searchDate']) && $_POST['searchDate'] !== null ? $_POST['searchDate'] : 'actual_lesson_date' ;

## Preparation
# get course_id from course
$sql = "SELECT 
            course_id, 
            course_name 
        FROM 
            ".$eclass_db.".course 
        WHERE 
            RoomType = 0 
        ORDER BY
            course_name";  
$courses = $li->returnArray($sql);
$courseNum = count($courses);

$lessonStatisticRecordsOfClassrooms = array();
if($courseNum > 0){
    for($i = 0 ; $i < $courseNum ; $i++){
        $li->db = $eclass_prefix."c".$courses[$i]['course_id'];
        if(mysql_select_db($li->db)){
            $lessons = array(
                'total' => 0,
                'delivered' => array(),
                'withPreLessons' => 0,
                'withInLessons' => 0,
                'withPostLessons' => 0,
                'preLessonActivities' => 0,
                'inLessonActivities' => 0,
                'postLessonActivities' => 0
            );
            
            # get phase, activity of lessons in classroom
            $sql = "
                SELECT 
                    l.lesson_id, 
                    l.progress_state,
                    p.phase_type, 
                    count(*) cnt 
                FROM
                    powerlesson_activities a 
                INNER JOIN 
                    powerlesson_phases p ON a.phase_id = p.phase_id 
                INNER JOIN 
                    powerlesson_lessons l ON p.lesson_id = l.lesson_id";
            if($startDate !== ''){
                $sql .= "
                WHERE
                   l.".$searchDate." >= '".$startDate." 00:00:00'
            ";
            }
            if($endDate !== ''){
                $sql .= "
                ".($startDate !== '' ? " AND " : " WHERE ")."
                   l.".$searchDate." <= '".$endDate." 23:59:59'
            ";
            }
            $sql .= "    
                GROUP BY 
                    l.lesson_id, l.progress_state, p.phase_type";
            $lessonRecords = $li->returnArray($sql);
            $lessonIds = array();
            if($lessonRecords){
                foreach((array)$lessonRecords as $lessonRecord){
                    if(!in_array($lessonRecord['lesson_id'], $lessonIds)){
                        $lessonIds[] = $lessonRecord['lesson_id'];
                    }
                    if($lessonRecord['progress_state'] === 'FINISHED'){
                        if(!in_array($lessonRecord['lesson_id'], $lessons['delivered'])){
                            $lessons['delivered'][] = $lessonRecord['lesson_id'];
                        }
                    }
                    switch($lessonRecord['phase_type']){
                        case 'PRE' :
                            $lessons['withPreLessons'] ++;
                            $lessons['preLessonActivities'] += $lessonRecord['cnt'];
                            break;
                        case 'IN' :
                            $lessons['withInLessons'] ++;
                            $lessons['inLessonActivities'] += $lessonRecord['cnt'];
                            break;
                        case 'POST' :
                            $lessons['withPostLessons'] ++;
                            $lessons['postLessonActivities'] += $lessonRecord['cnt'];
                            break;
                    }
                }
                
                $lessons['total'] = count($lessonIds);
            }
            
            $lessonStatisticRecordsOfClassrooms[$courses[$i]['course_id']] = $lessons;
        }
    }
}
#########################################################################################################
intranet_closedb();
?>

<!DOCTYPE html>
<html>
    <head>
        <style>
            
            table {
                font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
                border-collapse: collapse;
            }
            
            td, th {
                padding: 8px;
            }
            
            select, input[type=text] {
                padding: 4px;
                width: 150px;
            }
            
            #result td, #result th {
                border: 1px solid #ddd;
            }
            
            #result tr:hover {background-color: #ddd;}
            
            th {
                padding-top: 12px;
                padding-bottom: 12px;
                text-align: left;
                width : 180px;
            }
            
            .classroomTotal { font-weight:bold }
            .schoolTotal { font-weight:bold }
        </style>
    </head>
    <body>
    	<form method="POST">
        	<table border="0">
        		<tr>
        			<td>Start date:</td>
        			<td colspan="2"><input type="text" id="startDate" name="startDate" value="<?php echo $startDate; ?>" maxlength="10"> (YYYY-MM-DD)</td>
        		</tr>
        		<tr>
        			<td>End date:</td>
        			<td colspan="2"><input type="text" id="endDate" name="endDate" value="<?php echo $endDate; ?>" maxlength="10"> (YYYY-MM-DD)</td>
        		</tr>
        		<tr>
        			<td>Search by:</td>
        			<td>
        				<select name="searchDate" id="searchDate">
        					<option value="create_date">Creation Date</option>
        					<option value="actual_lesson_date">Delivery Date</option>
        					<option value="expected_lesson_date">Planned Date</option>
        				</select>
        			</td>
        			<td><input type="submit" value="Submit"></td>
        		</tr>
        	</table>
    	</form> 
    	<br />
        <table id="result">
          <tr>
            <th style="width : 100px;">&nbsp;</th>
            <th>已建立課堂</th>
            <th>已教授課堂</th>
            <th>有課前預習活動的課堂</th>
            <th>有課中互動活動的課堂</th>
            <th>有課後延伸活動的課堂</th>
            <th>課前預習活動</th>
            <th>課中互動活動</th>
            <th>課後延伸活動</th>
          </tr>
          <?php 

            $schoolTotals = array(
                'total' => array(),
                'delivered' => array(),
                'withPreLessons' => array(),
                'withInLessons' => array(),
                'withPostLessons' => array(),
                'preLessonActivities' => array(),
                'inLessonActivities' => array(),
                'postLessonActivities' => array()
            );
            $schoolTotals['total'] = 0;
            $schoolTotals['delivered'] = 0;
            $schoolTotals['withPreLessons'] = 0;
            $schoolTotals['withInLessons'] = 0;
            $schoolTotals['withPostLessons'] = 0;
            $schoolTotals['preLessonActivities'] = 0;
            $schoolTotals['inLessonActivities'] = 0;
            $schoolTotals['postLessonActivities'] = 0;
            
            for($i = 0 ; $i < $courseNum ; $i++){
                    $lessons = $lessonStatisticRecordsOfClassrooms[$courses[$i]['course_id']];
                    $schoolTotals['total'] += $lessons['total'];
                    $schoolTotals['delivered'] += count($lessons['delivered']);
                    $schoolTotals['withPreLessons'] += $lessons['withPreLessons'];
                    $schoolTotals['withInLessons'] += $lessons['withInLessons'];
                    $schoolTotals['withPostLessons'] += $lessons['withPostLessons'];
                    $schoolTotals['preLessonActivities'] += $lessons['preLessonActivities'];
                    $schoolTotals['inLessonActivities'] += $lessons['inLessonActivities'];
                    $schoolTotals['postLessonActivities'] += $lessons['postLessonActivities'];
                    echo "
                      <tr>
                        <td style='font-weight:bold;'>".$courses[$i]['course_name']."</td>
                        <td>".$lessons['total']."</td>
                        <td>".count($lessons['delivered'])."</td>
                        <td>".$lessons['withPreLessons']."</td>
                        <td>".$lessons['withInLessons']."</td>
                        <td>".$lessons['withPostLessons']."</td>
                        <td>".$lessons['preLessonActivities']."</td>
                        <td>".$lessons['inLessonActivities']."</td>
                        <td>".$lessons['postLessonActivities']."</td>
                      </tr>
                    ";
                }
            echo "
              <tr class='schoolTotal'>
                <td align='right'>全校總計</td>
                <td>".$schoolTotals['total']."</td>
                <td>".$schoolTotals['delivered']."</td>
                <td>".$schoolTotals['withPreLessons']."</td>
                <td>".$schoolTotals['withInLessons']."</td>
                <td>".$schoolTotals['withPostLessons']."</td>
                <td>".$schoolTotals['preLessonActivities']."</td>
                <td>".$schoolTotals['inLessonActivities']."</td>
                <td>".$schoolTotals['postLessonActivities']."</td>
              </tr>
            ";
          ?>
        </table>
    </body>
</html>
<?php }?>
