<?php
// modifying : 

###########################################################################
#																		  #
# Synchronize the released AP record in eDisv12 (DISCIPLINE_MERIT_RECORD) # 
# 		to Student Profile (PROFILE_STUDENT_MERIT) 						  #
#																		  #
# (all academic year)													  #
#																		  #
# /addon/ip25/index.php will call this file								  #
#																		  #
###########################################################################

$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
/*
intranet_auth();
intranet_opendb();
*/
$ldiscipline = new libdisciplinev12();

$allYearInfo = GetAllAcademicYearInfo(); 

for($a=0, $a_max=sizeof($allYearInfo); $a<$a_max; $a++) {
	list($yid, $yearEN, $yearB5) = $allYearInfo[$a];
	
	//$currentAcademicYearID = Get_Current_Academic_Year_ID();
	//$currentYearName = $ldiscipline->getAcademicYearNameByYearID($currentAcademicYearID);
	
	$currentAcademicYearID = $yid;
	
	$sql = "SELECT YearNameEN, YearNameB5 FROM ACADEMIC_YEAR WHERE AcademicYearID=$currentAcademicYearID";
	$yearData = $ldiscipline->returnArray($sql,2);
	$yearEN = $yearData[0][0];
	$yearB5 = $yearData[0][1];
	
	
	# remove records of current academic year in Student Profile 
	$sql = "DELETE FROM PROFILE_STUDENT_MERIT WHERE AcademicYearID='$currentAcademicYearID'";
	$ldiscipline->db_db_query($sql);
	
	# remove linkage between AP records & Student Profile
	//$sql = "UPDATE DISCIPLINE_MERIT_RECORD SET ProfileMeritID=NULL where AcademicYearID='$currentAcademicYearID' AND ReleaseStatus=".DISCIPLINE_STATUS_RELEASED." AND RecordStatus=".DISCIPLINE_STATUS_APPROVED;
	$sql = "UPDATE DISCIPLINE_MERIT_RECORD SET ProfileMeritID=NULL where AcademicYearID='$currentAcademicYearID'";
	$ldiscipline->db_db_query($sql);
	
	$ldiscipline->Start_Trans();
	
	$insertCount = 0;
	$successCount = 0;
	
	# select AP records (Approved & Released ONLY)
	$sql = "SELECT RecordID, RecordDate, AcademicYearID, YearTermID, StudentID, ItemID, ItemText, PICID, ProfileMeritCount, ProfileMeritType, Remark FROM DISCIPLINE_MERIT_RECORD WHERE AcademicYearID='$currentAcademicYearID' AND ReleaseStatus=".DISCIPLINE_STATUS_RELEASED." AND RecordStatus=".DISCIPLINE_STATUS_APPROVED." ORDER BY RecordDate";
	$result = $ldiscipline->returnArray($sql, 10);
	
	
	for($i=0; $i<sizeof($result); $i++) {
		
		list($recID, $recDate, $yearID, $termID, $studentID, $itemID, $itemText, $pic, $meritCount, $meritType, $remark) = $result[$i];
		$termName = $ldiscipline->getTermNameByTermID($termID);
		$stdInfo = $ldiscipline->getStudentNameByID($studentID);
		$clsName = $stdInfo[0]['ClassName'];
		$clsNo = $stdInfo[0]['ClassNumber'];
		
		$sql = "INSERT INTO PROFILE_STUDENT_MERIT (UserID, Year, Semester, MeritDate, NumberOfUnit, Reason, PersonInCharge, RecordType, RecordStatus, Remark, ClassName, ClassNumber, DateInput, DateModified, AcademicYearID, YearTermID)
					VALUES
				('$studentID', '$yearEN', '$termName', '$recDate', '$meritCount', '".$ldiscipline->Get_Safe_Sql_Query($itemText)."', '$pic', '$meritType', '1', '".$ldiscipline->Get_Safe_Sql_Query($remark)."', '$clsName', '$clsNo', NOW(), NOW(), '$currentAcademicYearID', '$termID')
		";
		$res[0] = $ldiscipline->db_db_query($sql);
		$insert_id = $ldiscipline->db_insert_id();
		if($insert_id) $insertCount++;
		//echo $sql.'<br>';
		
		$sql = "UPDATE DISCIPLINE_MERIT_RECORD SET ProfileMeritID=$insert_id WHERE RecordID=$recID";
		$res[1] = $ldiscipline->db_db_query($sql);
		//echo $sql.'<br>';
	
		$final_res = (count($res) == count(array_filter($res)));
		if($final_res)
		{
			$ldiscipline->Commit_Trans();
		}
		else
		{
			$ldiscipline->RollBack_Trans();
			echo "Fail to add record to Student Profile (AP RecordID : $recID)<br>";
		}
	}
	
	if(sizeof($result)==0)
	{
		$ldiscipline->RollBack_Trans();
	}
	echo "<br>Total AP record(s) in <font color=red>$yearEN</font> (AcademicYearID = $currentAcademicYearID) : ".sizeof($result);
	echo "<br>Success insert into student profile : ".$insertCount."<br>";
}



//intranet_closedb();
?>