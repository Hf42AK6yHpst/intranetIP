<?php
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/liblibrarymgmt.php");

intranet_auth();
intranet_opendb();

$libdb = new liblms();


if($_POST['Flag']!=1) {
	echo "<form method='post'><input value='run script' type='submit'><input type='hidden' name='Flag' value=1></form>";
}


# case 1: the book is available but the borrow log is not updated!

echo "<h2>1) Check for duplicated borrow records. </h2>";
$sql = 'Select log.BorrowLogID, log.BookID, log.UniqueID, log.UserID, log.BorrowTime, log.DueDate, log.recordstatus as log_status, log.datemodified, unq.recordstatus as book_status, unq.barcode From LIBMS_BORROW_LOG AS log,  LIBMS_BOOK_UNIQUE as unq  where log.recordstatus="BORROWED" and unq.recordstatus<>"BORROWED" AND log.uniqueid=unq.uniqueID order by log.borrowtime asc';


$LogArray = $libdb->returnResultSet($sql);

for ($i=0; $i<sizeof($LogArray); $i++)
{
	$LogObj = $LogArray[$i];
	
	$sql = "Select * From LIBMS_BORROW_LOG AS log where duedate='".$LogObj['DueDate']."' and userid='".$LogObj['UserID']."' and UniqueID='".$LogObj['UniqueID']."' order by datemodified asc";
	$SuspectedArray = $libdb->returnResultSet($sql);

	# if duplicated by 2 and the latest modified is returned
	if ( sizeof($SuspectedArray) >= 2 && $SuspectedArray[sizeof($SuspectedArray)-1]["RecordStatus"]=="RETURNED")
	{
		# remove the first duplicated record
		echo "<b>BorrowTime: ". $LogObj['BorrowTime']. " and number of records: ".sizeof($SuspectedArray)."</b>";
		debug($sql);
	
		for ($j=0; $j<sizeof($SuspectedArray)-1; $j++)
		{
			$BorrowLogID = $SuspectedArray[$j]['BorrowLogID'];
			$sql = "DELETE From LIBMS_BORROW_LOG WHERE BorrowLogID='".$BorrowLogID."' AND RecordStatus<>'RETURNED'";
			if($_POST['Flag'])
			{
				$libdb->db_db_query($sql);
				debug("<font color='blue'>Execute: " . $sql."</font>");
			}
		}
	} else
	{
		echo "<p><font color='red'>Plese check if this record(s) need follow-up!</font></p>";
		debug_r($SuspectedArray);
	}
}


# case 2: overdue

echo "<h2>2) Check for problematic log which has 2 or more borrowed records in log for the same book!</h2>";

$sql = 'Select log.UniqueID From LIBMS_BORROW_LOG AS log where log.recordstatus="BORROWED"  order by log.borrowtime asc';

$LogArray = $libdb->returnResultSet($sql);

for ($i=0; $i<sizeof($LogArray); $i++)
{
	$LogObj = $LogArray[$i];
	
	$sql = "Select * From LIBMS_BORROW_LOG AS log where recordstatus='BORROWED' and UniqueID='".$LogObj['UniqueID']."' order by datemodified asc";
	$SuspectedArray = $libdb->returnResultSet($sql);

	# if duplicated by 2 and the latest modified is returned
	if ( sizeof($SuspectedArray) >= 2 )
	{
		# remove the first duplicated record
		echo "<b>BorrowTime: ". $LogObj['BorrowTime']. " and number of records: ".sizeof($SuspectedArray)."</b>";
		debug($sql);
	
		for ($j=0; $j<sizeof($SuspectedArray)-1; $j++)
		{
			$BorrowLogID = $SuspectedArray[$j]['BorrowLogID'];
			$sql = "UPDATE LIBMS_BORROW_LOG SET RecordStatus='RETURNED', ReturnedTime=CONCAT(DueDate, ' 10:15:23') WHERE BorrowLogID='".$BorrowLogID."' AND RecordStatus<>'RETURNED'";
			if($_POST['Flag'])
			{
				$libdb->db_db_query($sql);
				debug("<font color='blue'>Execute: " . $sql."</font>");
			}
		}
	} else
	{
		//echo "<p><font color='red'>Plese check if this record(s) need follow-up!</font></p>";
		//debug_r($SuspectedArray);
	}
}


intranet_closedb();
?>