<?php
/*
 * Aim: patch the acno log table  
 * 		
 * 
 */
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/liblibrarymgmt.php");

intranet_auth();
intranet_opendb();
#########################################################################################################


## Use Library
$libdb = new liblms();


## Init 
$result = array();
############## Apply Patch ##############
$isApply = true;
#########################################
$noOfAvailCopyArr = array();
$noOfCopyArr = array();



## Get Data
if($_POST['Flag']!=1) {
	echo "<form method='post'><input value='run script' type='submit'><input type='hidden' name='Flag' value=1></form>";
}



$sql = "select BookID, BookTitle from LIBMS_BOOK order by BookID";
$bookArr = $libdb->returnArray($sql);

# get no of available copy if any into BOOKID 
$sql = "SELECT BookID, COUNT(*) as cnt FROM  LIBMS_BOOK_UNIQUE
		WHERE RecordStatus IN ('NORMAL', 'SHELVING', 'RESERVED') 
		group by BookID"; 
$temp1 = $libdb->returnArray($sql);
if(count($temp1) > 0){
	for ($i=0; $i<count($temp1); $i++){
		$noOfAvailCopyArr[$temp1[$i]['BookID']] = $temp1[$i]['cnt'];
	}
}

# get no of copy if any into BOOKID 
$sql = "SELECT BookID, COUNT(*) as cnt FROM  LIBMS_BOOK_UNIQUE
		WHERE RecordStatus NOT IN ('LOST',  'WRITEOFF',  'SUSPECTEDMISSING', 'DELETE') 
		group by BookID"; 
$temp2 = $libdb->returnArray($sql);
if(count($temp2) > 0){
	for ($i=0; $i<count($temp2); $i++){
		$noOfCopyArr[$temp2[$i]['BookID']] = $temp2[$i]['cnt'];
	}
}


## Main
for ($i=0; $i<count($bookArr); $i++)
{
	
	## handle the first copy of each Book
	$BookID = $bookArr[$i]['BookID'];
	$BookTitle = $bookArr[$i]['BookTitle'];
	
	$noOfCopyAvailable = (isset($noOfAvailCopyArr[$BookID])) ? $noOfAvailCopyArr[$BookID] : 0;
	$noOfCopy = (isset($noOfCopyArr[$BookID])) ? $noOfCopyArr[$BookID] : 0;
	
	$sql = "update LIBMS_BOOK set 
				NoOfCopyAvailable = '".$noOfCopyAvailable."', NoOfCopy= '".$noOfCopy."' 
			where BookID = '".$BookID."' ";
	echo $sql.'<br>';
	if($isApply){
		$result['update_book_copy_info_'.$BookID] = $libdb->db_db_query($sql);
	}
}



echo '<style> 
html, body, table, select, input, textarea{ font-size:12px; }
</style>';

debug_r($result);


	
#########################################################################################################
intranet_closedb();
?>