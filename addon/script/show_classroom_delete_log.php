<?php

$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

include('../check.php');

intranet_opendb();
##########################################################################################

## Library
$db = new libdb();


## Init
$x = '';
$cnt = 0;
$error = array();


## SQL
/*
 * RecordDetail - breakdown
	 * 	CourseID
	 *  CourseCode
	 * 	CourseName
	 * 	RoomType
	 * 	InputDate
	 * 	CalID
	 * 	SubjectGroupID
	 * 	UrlRequestFrom
 */
$sql = "select 
			l.*, u.ChineseName, u.EnglishName, u.UserEmail
		from MODULE_RECORD_DELETE_LOG as l 
		left join INTRANET_USER as u on u.UserID = l.LogBy  
		where l.Module = 'eClass' 
		order by l.LogDate desc ";
$logArr = $db->returnArray($sql);


## MAIN
if(count($logArr) > 0){
	for($i=0 ; $i<count($logArr) ; $i++){
		$logID = $logArr[$i]['LogID'];
		$detail = $logArr[$i]['RecordDetail'];
		$logDate = $logArr[$i]['LogDate'];
		$logBy = $logArr[$i]['LogBy'];
		$chineseName = $logArr[$i]['ChineseName'];
		$englishName = $logArr[$i]['EnglishName'];
		
		$itemDataArr[$cnt]['LogDate'] = $logDate;
		$itemDataArr[$cnt]['LogBy'] = $logBy;
		$itemDataArr[$cnt]['ChineseName'] = $chineseName;
		$itemDataArr[$cnt]['EnglishName'] = $englishName;
		
		$itemArr = explode("<br>", $detail);
		if(count($itemArr) > 0){
			for($j=0 ; $j<count($itemArr) ; $j++){
				$itemData = explode(": ", $itemArr[$j]);
				
				$itemName = $itemData[0];
				$itemDataArr[$cnt][$itemName] = $itemData[1];
			}
			$cnt++;
		} else {
			$error[] = $logID;
		}
		
		$x = '<td></td>';
	}
}
$x .= '<table cellpadding="5" cellspacing="0" border="1" width="98%">';
$x .= '<tr>';
$x .= '<th>CourseID</th>';
$x .= '<th>CourseCode</th>';
$x .= '<th>CourseName</th>';
$x .= '<th>RoomType</th>';
$x .= '<th>InputDate</th>';
$x .= '<th>CalID</th>';
$x .= '<th>SubjectGroupID</th>';
$x .= '<th>UrlRequestFrom</th>';
$x .= '<th>LogDate</th>';
$x .= '<th>LogBy</th>';
$x .= '</tr>';

if(count($itemDataArr) > 0){
	for($i=0 ; $i<count($itemDataArr) ; $i++){
//		$deletedBy = $itemDataArr[$i]['EnglishName'].' ('.$itemDataArr[$i]['ChineseName'].')';
//		$deletedBy = $itemDataArr[$i]['EnglishName'];
		$deletedBy = $itemDataArr[$i]['ChineseName'];
//		$deletedBy = $itemDataArr[$i]['EnglishName'].' <br>'.$itemDataArr[$i]['ChineseName'];
		
		$x .= '<tr>';
		$x .= '<td>'.$itemDataArr[$i]['CourseID'].'</td>';
		$x .= '<td>'.$itemDataArr[$i]['CourseCode'].'</td>';
		$x .= '<td>'.$itemDataArr[$i]['CourseName'].'</td>';
		$x .= '<td>'.$itemDataArr[$i]['RoomType'].'</td>';
		$x .= '<td>'.$itemDataArr[$i]['InputDate'].'</td>';
		$x .= '<td>'.$itemDataArr[$i]['CalID'].'</td>';
		$x .= '<td>'.$itemDataArr[$i]['SubjectGroupID'].'</td>';
		$x .= '<td>'.$itemDataArr[$i]['UrlRequestFrom'].'</td>';
		$x .= '<td>'.$itemDataArr[$i]['LogDate'].'</td>';
		$x .= '<td>'.$deletedBy.'</td>';
		$x .= '</tr>';
	}
}
$x .= '</table>';


echo '<style> 
html, body, table, select, input, textarea{ font-size:12px; }
</style>';
echo $x;

intranet_closedb();
?>