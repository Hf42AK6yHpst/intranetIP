<?php
// editing by 
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libebookreader.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");

intranet_opendb();

//header("Content-type: text/html; charset=utf-8");
$lgs = new libgeneralsettings();

$ModuleName = "InitSetting";
$GSary = $lgs->Get_General_Setting($ModuleName);
if(!$GSary['eBookPageInfoPatch_20170418']){
$libebookreader = new libebookreader();
$sql = "SELECT BookID FROM INTRANET_ELIB_BOOK WHERE SubCategory = 'English Classics'";
$bidList = $libebookreader->returnVector($sql);
$jsBidList = "var jsBidList=[";
$x = "English Classics: ";
foreach($bidList as $bid){
	$x .= '<a href="javascript:void(0)" onclick="setFrame('.$bid.')">'.$bid.'</a> | ';
	$jsBidList .= $bid.",";
}
$x .= "<br>";
$sql = "SELECT BookID FROM INTRANET_ELIB_BOOK WHERE SubCategory = 'Young Readers'";
$bidList = $libebookreader->returnVector($sql);
$x .= "Young Readers: ";
foreach($bidList as $bid){
	$x .= '<a href="javascript:void(0)" onclick="setFrame('.$bid.')">'.$bid.'</a> | ';
	$jsBidList .= $bid.",";
}
$jsBidList = rtrim($jsBidList,",");
$jsBidList .= "];";
$x .= "<br>";
$y = '<iframe id="PreProcessFrame" width="1px" height="1px" src=""></iframe>';
$x .= $y;
$x .= "<br>";
/*
if(isset($_REQUEST['bid'])){
	$ebook_root_path = $intranet_root."/file/eBook/".$bid."/";
	$success = $libebookreader->Parse_EPUB_XML($ebook_root_path);
	if($success){
		
	}
}
*/
//echo $x;
echo "===================================================================<br>\r\n";
echo "Patching eBook Page Info data [Start]<br>\r\n";
echo "===================================================================<br>\r\n";
echo $y;

?>
<div id="book_page_update_status">
Updating book page info...<span id="numberOfBooks"></span>
</div>
<script src="../../templates/jquery/jquery-1.8.3.min.js"></script>
<script>
<?=$jsBidList?>
var index = 0;
function detector(){
	if($('#PreProcessFrame').contents().find("body").html().search('Done')=="-1"){
		if($('#toend').length){
			$('#toend').css('display','none');
		}
	}else{
		if(jsBidList.length==index+1){
			clearInterval(timefunc);
			$('#book_page_update_status').html('Done');
			$('#endOfBookPageUpdate').css('display','block');
			if($('#toend').length){
				$('#toend').css('display','block');
			}
		}else{
			index++;
			setFrame(jsBidList[index]);
		}
	}
}
function setFrame(book){
	document.getElementById('PreProcessFrame').src = "/home/eLearning/ebook_manage/page_count_checking.php?bid="+book;
	$('#numberOfBooks').html((index+1)+"/"+jsBidList.length);
}
setFrame(jsBidList[0]);
var timefunc = setInterval(function(){detector()}, 1000);
</script>
<div id="endOfBookPageUpdate" style="display:none">
===================================================================<br>
Patching eBook Page Info data [End]<br>
===================================================================<br>
</div>
<?php
	$sql = "insert ignore into GENERAL_SETTING 
					(Module, SettingName, SettingValue, DateInput) 
				values 
					('$ModuleName', 'eBookPageInfoPatch_20170418', 1, now())";
		$lgs->db_db_query($sql);
}else{
	echo "===================================================================<br>\r\n";
	echo "Patching eBook Page Info data [Start]<br>\r\n";
	echo "===================================================================<br>\r\n";
	echo "===================================================================<br>\r\n";
	echo "Patching eBook Page Info data [End]<br>\r\n";
	echo "===================================================================<br>\r\n";
}

?>