<?php
##############
## This Data Patch is to install worksheets questions for client.
## Run Path: http://kis-test.eclass.hk/addon/script/kis/worksheets/update_sql.php
##############
/**
 * Change Log:
 * 2018-03-09 Pun [123349] [ip.2.5.9.3.1]
 *  - Added pbk.eclass.hk
 */
// ini_set('display_errors',1); error_reporting(E_ALL ^ E_NOTICE);
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libeclass40.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/icalendar.php");

//include_once($PATH_WRT_ROOT."includes/chiuchunkg.eclass.hk/settings.php");
## Prompt login
if(!$CallFromSameDomain){
	include_once("../../../check.php");
}

eclass_opendb();


$clientDomain[] = "aogkg.eclass.hk";
$clientDomain[] = "chiuchunkg.eclass.hk";
$clientDomain[] = "hartspreschool.eclass.hk";
$clientDomain[] = "hk5skg.eclass.hk";
$clientDomain[] = "hke-icms.eclass.hk";
$clientDomain[] = "hywkk.eclass.hk";
$clientDomain[] = "icms.eclass.hk";
$clientDomain[] = "kis-training.eclass.hk";
$clientDomain[] = "mingwai.eclass.hk";
$clientDomain[] = "mosgraceful.eclass.hk";
$clientDomain[] = "pbk.eclass.hk";
$clientDomain[] = "pe-mingwai.eclass.hk";
$clientDomain[] = "rmkg.eclass.hk";
$clientDomain[] = "sihmwts.eclass.hk";
$clientDomain[] = "sunkids.eclass.hk";
$clientDomain[] = "taksunkg.eclass.hk";
$clientDomain[] = "tbcc.eclass.hk";
$clientDomain[] = "tbcek.eclass.hk";
$clientDomain[] = "tbcgk.eclass.hk";
$clientDomain[] = "tbck.eclass.hk";
$clientDomain[] = "tbcpk.eclass.hk";
$clientDomain[] = "tsuenwanbckg.eclass.hk";
$clientDomain[] = "ylsyk.eclass.hk";

$course_code = 'kisworksheets';
$db_table = array('assessment','phase','task','question','quiz','quiz_question');
//$update_time = '2017-02-23 00:00:00';
//$update_time = '2017-03-21 00:00:00';
$update_time = '2017-04-20 00:00:00';

$lo = new libeclass();

$sql = "select course_id from ".$eclass_db.".course where course_code='".$course_code."'";
$count_obj = $lo->returnVector($sql);
$course_id = $count_obj[0];
$lo->RoomType = 7;
	echo "<strong> KIS Worksheets Database Update</strong>";
if($course_id!=''){
	$targetDbName = $eclass_prefix."c".$course_id;
	echo "<br/><br/><strong>Source Database Name : ".$targetDbName."</strong><br/>";
}

$quizIDString = '';
$sqlArray = array();
foreach ($db_table as $tableName){
	if($tableName == 'quiz_question'){
		$sql = "select * from {$targetDbName}.{$tableName} where quiz_id in ({$quizIDString})";
	}else{
		$sql = "select * from {$targetDbName}.{$tableName} where modified > '$update_time'";
	}
	
	$returnAry = $lo->returnArray($sql,null,1);
	
	if($tableName == 'quiz'){
		$quizID = array();
	}
	
	foreach ($returnAry as $data){
		$fieldList = array();
		$valueList = array();

		foreach ($data as $key=>$value){
			$fieldList[] = "$key";
			$valueList[] = "'".mysql_real_escape_string($value)."'";
			
			if($key == 'quiz_id'){
				$quizID[] = $value;
			}
			
		}
		$fieldListString = implode($fieldList,',');
		$valueListString = implode($valueList,',');
		
		$sqlArray[] = "{$tableName} ({$fieldListString}) Values ({$valueListString})";
	}
	
	if($tableName == 'quiz'){
		$quizIDString = implode($quizID,',');;
	}
}

//-----Start installation-----

echo "<br/><h3>----- Start installation-----</h3><br/>";

foreach($clientDomain as $domain){
	include_once($PATH_WRT_ROOT."includes/$domain/settings.php");
	
	$sql = "select course_id from ".$eclass_db.".course where course_code='".$course_code."'";
	$count_obj = $lo->returnVector($sql);
	$course_id = $count_obj[0];
	$lo->RoomType = 7;
	
	if($course_id!=''){
		$targetDbName = $eclass_prefix."c".$course_id;
	}else{
		echo "<br/>-----Warning-----<br/>'$domain' has not install KIS Worksheets<br/>-----Warning-----<br/>";
		continue;
	}
	
	$errorSql = false;
	echo "<br/><br/>----- Start insert {$domain}-----<br/>";
	
	foreach($sqlArray as $sql){
		
		$insertSql = "Insert into {$targetDbName}.{$sql}";
		//echo "<br/>".$insertSql."<br/>";
		$result = $lo->db_db_query($insertSql);
		if(!$result){
			echo "<br/>Invalid query: {$insertSql}<br/>";
			$errorSql = true;
		}

	}
	if(!$errorSql){
		echo "Success!";
	}
	echo "<br/>----- End insert {$domain}-----<br/><br/>";
}

echo "<br/><h3>----- End installation-----</h3>";

//-----End installation-----
eclass_closedb();
?>