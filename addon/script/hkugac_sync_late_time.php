<?php
// Editing by 
/*
 * 2017-11-14 (Carlos): Created for $sys_custom['StudentAttendance']['HKUGAC_LateAbsentEmailNotification'] to copy late time to late profile record.
 */
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_opendb();

$li = new libdb();

echo "[BEGIN][".date("Y-m-d")."]<br />\n";

$result = array();

$sql = "ALTER TABLE CARD_STUDENT_PROFILE_RECORD_REASON ADD COLUMN LateTime time";
$result[$sql] = $li->db_db_query($sql);

$sql = "SHOW TABLES LIKE 'CARD_STUDENT_DAILY_LOG_%_%'";
$dailylog_tables = $li->returnVector($sql);

for($i=0;$i<count($dailylog_tables);$i++){
	
	$dailylog_table = $dailylog_tables[$i];
	if(preg_match('/^CARD_STUDENT_DAILY_LOG_(\d\d\d\d)_(\d\d)$/',$dailylog_table,$matches)){
		$year = $matches[1];
		$month = $matches[2];
		 
		$sql = "SELECT UserID,DATE_FORMAT(CONCAT('$year-$month-',DayNumber),'%Y-%m-%d') as RecordDate,InSchoolTime,AMStatus FROM $dailylog_table WHERE AMStatus='2' AND InSchoolTime IS NOT NULL AND InSchoolTime<>''";
		$records = $li->returnResultSet($sql);
		$record_size = count($records);
		
		for($j=0;$j<$record_size;$j++){
			$sql = "UPDATE CARD_STUDENT_PROFILE_RECORD_REASON SET LateTime='".$records[$j]['InSchoolTime']."' WHERE StudentID='".$records[$j]['UserID']."' AND RecordDate='".$records[$j]['RecordDate']."' AND DayType=2 AND RecordType='2'";
			$result[$sql] = $li->db_db_query($sql);
		}
	}
}

debug_pr($result);

echo "[END][".date("Y-m-d")."]<br />\n";

intranet_closedb();
?>