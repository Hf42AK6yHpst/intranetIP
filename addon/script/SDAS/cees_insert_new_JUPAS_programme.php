<?php 

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
// include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
// include_once($PATH_WRT_ROOT."includes/libportfolio.php");
intranet_opendb();
$db = new libdb();

//  CITY U
$sql = "INSERT IGNORE INTO JUPAS_PROGRAMME (JupasCode,ProgrammeFullName,Institution,ProgrammeType) 
VALUES 
('JS1000','BSc Computational Finance and Financial Technology','CITY','D'),
('JS1001','BBA Global Business','CITY','D'),
('JS1002','BBA Accountancy','CITY','D'),
('JS1003','Department of Economics and Finance (options: BBA Business Economics, BBA Finance)','CITY','D'),
('JS1004','Department of Information Systems (options: BBA Global Business Systems Management, BBA Information Management)','CITY','D'),
('JS1005','BBA Management','CITY','D'),
('JS1006','Department of Management Sciences (options: BBA Business Analysis, BBA Business Operations Management)','CITY','D'),
('JS1007','BBA Marketing','CITY','D'),
('JS1041','School of Creative Media (options: BA Creative Media, BSc Creative Media, BAS New Media)','CITY','D'),
('JS1042','BA Creative Media','CITY','D'),
('JS1043','BSc Creative Media','CITY','D'),
('JS1044','BAS New Media','CITY','D'),
('JS1051','School of Energy and Environment (options: BEng Energy Science and Engineering, BEng Environmental Science and Engineering)','CITY','D'),
('JS1061','Bachelor of Laws','CITY','D'),
('JS1071','School of Data Science (options: BSc Data Science, BEng Data and Systems Analytics)','CITY','D'),
('JS1072','BSc Data Science','CITY','D'),
('JS1073','BEng Data and Systems Analytics','CITY','D'),
('JS1091','Division of Building Science and Technology (options: ASc Building Services Engineering, ASc Construction Engineering and Management, ASc Surveying)','CITY','SUB-D'),
('JS1093','ASc Architectural Studies','CITY','SUB-D'),
('JS1101','Department of Social and Behavioural Sciences (options: BSocSc Criminology and Sociology, BSocSc Psychology, BSocSc Social Work)','CITY','D'),
('JS1102','BSocSc Asian and International Studies','CITY','D'),
('JS1103','BA Chinese and History','CITY','D'),
('JS1104','BA English','CITY','D'),
('JS1105','Department of Linguistics and Translation (options: BA Linguistics and Language Applications, BA Translation and Interpretation)','CITY','D'),
('JS1106','Department of Media and Communication (options: BA Digital Television and Broadcasting, BA Media and Communication)','CITY','D'),
('JS1108','BSocSc Public Policy and Politics','CITY','D'),
('JS1201','Department of Architecture and Civil Engineering (options: BEng Architectural Engineering, BEng Civil Engineering, BSc Surveying)','CITY','D'),
('JS1202','BSc Chemistry','CITY','D'),
('JS1203','Department of Biomedical Sciences (options: BSc Biological Sciences, BSc Biomedical Sciences)','CITY','D'),
('JS1204','BSc Computer Science','CITY','D'),
('JS1205','Department of Electronic Engineering (options:BEng Computer&Data Engineering, BEng Electronic&Communication Engineering, BEng Information Engineering)','CITY','D'),
('JS1206','BSc Computing Mathematics','CITY','D'),
('JS1207','Department of Mechanical Engineering (options: BEng Mechanical Engineering,BEng Nuclear & Risk Engineering)','CITY','D'),
('JS1208','BSc Applied Physics','CITY','D'),
('JS1210','BEng Materials Science and Engineering','CITY','D'),
('JS1211','BEng Biomedical Engineering','CITY','D'),
('JS1801','Bachelor of Veterinary Medicine','CITY','D')
";
$db->db_db_query($sql);

###############HKBU ###############
$sql = "INSERT IGNORE INTO JUPAS_PROGRAMME (JupasCode,ProgrammeFullName,Institution,ProgrammeType)
VALUES
('JS2020','Bachelor of Arts','BU','D'),
('JS2030','Bachelor of Arts in Music','BU','D'),
('JS2040','English Language & Literature and English Language Teaching (Double Degree)','BU','U'),
('JS2050','Bachelor of Music in Creative IndustriesNEW','BU','D'),
('JS2110','Bachelor of Business Administration - Accounting Concentration','BU','D'),
('JS2120','Bachelor of Business Administration','BU','D'),
('JS2130','Bachelor of Business Administration - Applied Economics Concentration','BU','D'),
('JS2140','Bachelor of Business Administration - Entrepreneurship Concentration','BU','D'),
('JS2150','Bachelor of Business Administration - Finance Concentration','BU','D'),
('JS2160','Bachelor of Business Administration - Human Resources Management Concentration','BU','D'),
('JS2170','Bachelor of Business Administration - Information Systems and e-Business Management Concentration','BU','D'),
('JS2180','Bachelor of Business Administration - Marketing Concentration','BU','D'),
('JS2310','Bachelor of Communication','BU','D'),
('JS2320','Bachelor of Communication - Film Major - Animation and Media Arts Concentration','BU','D'),
('JS2330','Bachelor of Communication - Film Major - Film and Television Concentration','BU','D'),
('JS2410','Bachelor of Chinese Medicine and Bachelor of Science in Biomedical Science','BU','D'),
('JS2420','Bachelor of Pharmacy in Chinese Medicine','BU','D'),
('JS2510','Bachelor of Science','BU','D'),
('JS2610','Bachelor of Arts/ Bachelor of Social Sciences (Geography/ Government & International Studies/ History/ Sociology)','BU','D'),
('JS2620','Bachelor of Arts in Physical Education and Recreation Management','BU','D'),
('JS2630','Bachelor of Social Sciences in European Studies - French Stream','BU','D'),
('JS2640','Bachelor of Social Sciences in European Studies - German Stream','BU','D'),
('JS2650','Bachelor of Social Sciences in China Studies (Economics/ Geography/ History/ Sociology)','BU','D'),
('JS2660','Bachelor of Social Work','BU','D'),
('JS2680','Geography/ History/ Sociology and Liberal Studies Teaching (Double Degree)','BU','U'),
('JS2690','Geography/ History/ Sociology and Personal, Social and Humanities Education Teaching (Double Degree)NEW','BU','U'),
('JS2810','Bachelor of Arts in Visual Arts','BU','D'),
('JS2910','Bachelor of Science in Business Computing and Data AnalyticsNEW','BU','D')
";
$db->db_db_query($sql);

###############LINGNAN ###############
$sql = "INSERT IGNORE INTO JUPAS_PROGRAMME (JupasCode,ProgrammeFullName,Institution,ProgrammeType)
VALUES
('JS7100','Bachelor of Arts (Honours)','LU','D'),
('JS7101','Bachelor of Arts (Honours) in Chinese','LU','D'),
('JS7123','Bachelor of Arts (Honours) in Global Liberal ArtsNEW','LU','D'),
('JS7200','Bachelor of Business Administration (Honours)','LU','D'),
('JS7204','Bachelor of Arts (Honours) in Translation','LU','D'),
('JS7216','Bachelor of Business Administration (Honours) - Risk and Insurance Management','LU','D'),
('JS7225','Bachelor of Science (Honours) in Data Science','LU','D'),
('JS7300','Bachelor of Social Sciences (Honours)','LU','D'),
('JS7503','Bachelor of Arts (Honours) in Contemporary English Studies','LU','D'),
('JS7606','Bachelor of Arts (Honours) in Cultural Studies','LU','D'),
('JS7709','Bachelor of Arts (Honours) in History','LU','D'),
('JS7802','Bachelor of Arts (Honours) in Philosophy','LU','D'),
('JS7905','Bachelor of Arts (Honours) in Visual Studies','LU','D')    
";
$db->db_db_query($sql);


###############CUHK###############
$sql = "INSERT IGNORE INTO JUPAS_PROGRAMME (JupasCode,ProgrammeFullName,Institution,ProgrammeType)
VALUES
('JS4006','Anthropology','CU','U'),
('JS4018','Chinese Language and Literature','CU','U'),
('JS4020','Cultural Studies','CU','U'),
('JS4022','Cultural Management','CU','U'),
('JS4032','English','CU','U'),
('JS4044','Fine Arts','CU','U'),
('JS4056','History','CU','U'),
('JS4068','Japanese Studies','CU','U'),
('JS4070','Linguistics','CU','U'),
('JS4082','Music','CU','U'),
('JS4094','Philosophy','CU','U'),
('JS4109','Religious Studies','CU','U'),
('JS4111','Theology','CU','U'),
('JS4123','Translation','CU','U'),
('JS4136','Chinese Studies','CU','U'),
('JS4202','Integrated Bachelor of Business Administration Programme','CU','U'),
('JS4214','Global Business Studies','CU','U'),
('JS4226','Hospitality and Real Estate','CU','U'),
('JS4238','Insurance, Financial and Actuarial Analysis','CU','U'),
('JS4240','Professional Accountancy','CU','U'),
('JS4252','Quantitative Finance','CU','U'),
('JS4254','Interdisciplinary Major Programme in Global Economics and Finance','CU','U'),
('JS4264','Bachelor of Business Administration (Integrated BBA Programme) and Juris Doctor Double Degree Programme','CU','D'),
('JS4276','Quantitative Finance and Risk Management Science','CU','U'),
('JS4288','International Business and Chinese Enterprise','CU','U'),
('JS4329','Physical Education, Exercise Science and Health','CU','U'),
('JS4331','B.A. (Chinese Language Studies) and B.Ed. (Chinese Language Education)','CU','D'),
('JS4343','B.A. (English Studies) and B.Ed. (English Language Education)','CU','D'),
('JS4361','B.Ed. in Mathematics and Mathematics Education','CU','D'),
('JS4372','B.Ed. in Early Childhood Education','CU','D'),
('JS4401','Engineering','CU','U'),
('JS4428','BEng in Financial Technology','CU','D'),
('JS4434','BEng in Electronic Engineering','CU','D'),
('JS4460','BEng in Biomedical Engineering','CU','D'),
('JS4462','BEng in Energy and Environmental Engineering','CU','D'),
('JS4468','BEng in Artificial Intelligence: Systems and TechnologiesNEW','CU','D'),
('JS4501','Medicine (MBChB) Programme','CU','D'),
('JS4502','Medicine (MBChB) Programme Global Physician-Leadership Stream (GPS)','CU','D'),
('JS4513','Nursing','CU','U'),
('JS4525','Pharmacy','CU','U'),
('JS4537','Public Health','CU','U'),
('JS4542','Chinese Medicine','CU','U'),
('JS4550','Biomedical Sciences','CU','U'),
('JS4601','Science','CU','U'),
('JS4633','Earth System Science (Atmospheric Science / Geophysics)','CU','U'),
('JS4682','Enrichment Mathematics','CU','U'),
('JS4690','Enrichment Stream in Theoretical Physics','CU','U'),
('JS4719','Risk Management Science','CU','U'),
('JS4801','Social Science','CU','U'),
('JS4812','Architectural Studies','CU','U'),
('JS4824','Economics','CU','U'),
('JS4836','Geography and Resource Management','CU','U'),
('JS4838','Urban Studies','CU','U'),
('JS4848','Government and Public Administration','CU','U'),
('JS4850','Journalism and Communication','CU','U'),
('JS4858','Global Communication','CU','U'),
('JS4862','Psychology','CU','U'),
('JS4874','Social Work','CU','U'),
('JS4886','Sociology','CU','U'),
('JS4892','Global Studies','CU','U'),
('JS4893','Data Science and Policy Studies','CU','U'),
('JS4903','Bachelor of Laws','CU','D')    
";
$db->db_db_query($sql);


##############EDUHK ###############
$sql = "INSERT IGNORE INTO JUPAS_PROGRAMME (JupasCode,ProgrammeFullName,Institution,ProgrammeType)
VALUES
('JS8105','Bachelor of Education (Honours) (Chinese Language)','IED','D'),
('JS8222','Bachelor of Education (Honours) (English Language) - Primary','IED','D'),
('JS8234','Bachelor of Education (Honours) (Primary) - General Studies','IED','D'),
('JS8246','Bachelor of Education (Honours) (Primary) - Mathematics','IED','D'),
('JS8325','Bachelor of Education (Honours) (Physical Education)','IED','D'),
('JS8361','Bachelor of Education (Honours) (Secondary) - Information and Communication Technology','IED','D'),
('JS8371','Bachelor of Education (Honours) (Business, Accounting and Financial Studies)','IED','D'),
('JS8391','Bachelor of Education (Honours) (Secondary) in Mathematics','IED','D'),
('JS8404','Bachelor of Education (Honours) (Early Childhood Education)','IED','D'),
('JS8416','Bachelor of Education (Honours) (Chinese History)','IED','D'),
('JS8428','Bachelor of Education (Honours) (Geography)','IED','D'),
('JS8430','Bachelor of Education (Honours) (Science)','IED','D'),
('JS8507','Higher Diploma in Early Childhood Education','IED','SUB-D'),
('JS8600','Bachelor of Arts (Honours) in Language Studies (Chinese Major)','IED','D'),
('JS8612','Bachelor of Arts (Honours) in Language Studies (English Major)','IED','D'),
('JS8624','Bachelor of Social Sciences (Honours) in Global and Environmental Studies','IED','D'),
('JS8636','Bachelor of Arts (Honours) in Creative Arts and Culture (Music)','IED','D'),
('JS8648','Bachelor of Arts (Honours) in Creative Arts and Culture (Visual Arts)','IED','D'),
('JS8651','Bachelor of Social Sciences (Honours) in Psychology','IED','D'),
('JS8663','Bachelor of Arts (Honours) in Special Education','IED','D'),
('JS8801','Bachelor of Arts (Honours) in Creative Arts and Culture and Bachelor of Education (Honours) (Music) Co-terminal Double Degree Programme','IED','D'),
('JS8813','Bachelor of Arts (Honours) in Creative Arts and Culture and Bachelor of Education (Honours) (Visual Arts) Co-terminal Double Degree Programme','IED','D'),
('JS8825','Bachelor of Arts (Honours) in Language Studies and Bachelor of Education (Honours) (English Language) Co-terminal Double Degree Programme','IED','D')  
";
$db->db_db_query($sql);


###############POLYU ###############
$sql = "INSERT IGNORE INTO JUPAS_PROGRAMME (JupasCode,ProgrammeFullName,Institution,ProgrammeType)
VALUES
('JS3014','HD in Applied Physics','PU','SUB-D'),
('JS3026','HD in Building Services Engineering','PU','SUB-D'),
('JS3038','HD in Building Technology & Management (Engineering)','PU','SUB-D'),
('JS3040','HD in Chemical Technology','PU','SUB-D'),
('JS3052','HD in Civil Engineering','PU','SUB-D'),
('JS3064','HD in Electrical Engineering','PU','SUB-D'),
('JS3076','HD in Electronic & Information Engineering','PU','SUB-D'),
('JS3105','HD in Land Surveying and Geo-Informatics','PU','SUB-D'),
('JS3117','HD in Industrial & Systems Engineering','PU','SUB-D'),
('JS3284','HD in Building Technology & Management (Surveying)','PU','SUB-D'),
('JS3337','BSc (Hons) in Mental Health Nursing','PU','D'),
('JS3349','BSc (Hons) in Food Safety and Technology','PU','D'),
('JS3351','BEng (Hons) in Transportation Systems Engineering','PU','D'),
('JS3375','BEng (Hons) in Environmental Engineering & Sustainable Development','PU','D'),
('JS3387','BSc (Hons) in Land Surveying and Geo-Informatics','PU','D'),
('JS3442','BSc (Hons) in Property Management','PU','D'),
('JS3466','BBA (Hons) in Accounting & Finance','PU','D'),
('JS3478','BSc (Hons) in Medical Laboratory Science','PU','D'),
('JS3480','Broad Discipline of Language, Culture & Communication','PU','D'),
('JS3492','BA (Hons) Scheme in Fashion and Textiles','PU','D'),
('JS3507','BEng (Hons) in Aviation Engineering','PU','D'),
('JS3519','BSc (Hons) in Internet & Multimedia Technologies','PU','D'),
('JS3533','BBA (Hons) in Global Supply Chain Management','PU','D'),
('JS3557','BEng (Hons) Scheme in Product and Industrial Engineering','PU','D'),
('JS3569','BA (Hons) Scheme in Design','PU','D'),
('JS3571','BSc (Hons) Scheme in Logistics and Enterprise Engineering','PU','D'),
('JS3583','BBA (Hons) in Management','PU','D'),
('JS3595','BBA (Hons) in Financial Services','PU','D'),
('JS3600','BSc (Hons) in Biomedical Engineering','PU','D'),
('JS3612','BSc (Hons) in Radiography','PU','D'),
('JS3624','BSc (Hons) in Occupational Therapy','PU','D'),
('JS3636','BSc (Hons) in Physiotherapy','PU','D'),
('JS3648','BSc (Hons) in Nursing','PU','D'),
('JS3650','BSc (Hons) in Optometry','PU','D'),
('JS3662','BA (Hons) in Social Work','PU','D'),
('JS3674','BBA (Hons) in International Shipping & Transport Logistics','PU','D'),
('JS3703','BEng (Hons) in Electronic & Information Engineering','PU','D'),
('JS3715','BEng (Hons) in Electrical Engineering','PU','D'),
('JS3739','BEng (Hons) in Civil Engineering','PU','D'),
('JS3741','BEng (Hons) Scheme in Mechanical Engineering','PU','D'),
('JS3753','BEng (Hons) in Building Services Engineering','PU','D'),
('JS3765','BA (Hons) in Social Policy & Social Entrepreneurship','PU','D'),
('JS3777','BEng (Hons) in Sustainable Structural and Fire Engineering','PU','D'),
('JS3789','BSc (Hons) in Surveying','PU','D'),
('JS3791','BSc (Hons) in Building Engineering and Management','PU','D'),
('JS3806','BSc (Hons) in Investment Science and Finance Analytics','PU','D'),
('JS3820','BSc (Hons) in Tourism & Events Management','PU','D'),
('JS3868','Broad Discipline of Computing','PU','D'),
('JS3882','BSc (Hons) in Hotel Management','PU','D'),
('JS3894','BBA (Hons) in Marketing','PU','D'),
('JS3911','BBA (Hons) in Accountancy','PU','D'),
('JS3923','BSc (Hons) in Applied Biology with Biotechnology','PU','D'),
('JS3985','BSc (Hons) in Engineering Physics','PU','D'),
('JS3997','BSc (Hons) in Chemical Technology','PU','D')   
";
$db->db_db_query($sql);


###############HKUST ###############
$sql = "INSERT IGNORE INTO JUPAS_PROGRAMME (JupasCode,ProgrammeFullName,Institution,ProgrammeType)
VALUES
('JS5100','Science','UST','U'),
('JS5101','International Research Enrichment','UST','U'),
('JS5200','Engineering','UST','U'),
('JS5211','BSc Integrative Systems and Design','UST','D'),
('JS5300','Business and Management','UST','U'),
('JS5311','BBA Economics','UST','D'),
('JS5312','BBA Finance','UST','D'),
('JS5313','BBA Global Business','UST','D'),
('JS5314','BBA Information Systems','UST','D'),
('JS5315','BBA Management','UST','D'),
('JS5316','BBA Marketing','UST','D'),
('JS5317','BBA Operations Management','UST','D'),
('JS5318','BBA Professional Accounting','UST','D'),
('JS5331','BSc Economics and Finance','UST','D'),
('JS5332','BSc Quantitative Finance','UST','D'),
('JS5411','BSc Global China Studies','UST','D'),
('JS5412','BSc Quantitative Social Analysis','UST','D'),
('JS5811','BSc Biotechnology and Business','UST','D'),
('JS5812','BSc Environmental Management and Technology','UST','D'),
('JS5813','BSc Mathematics and Economics','UST','D'),
('JS5814','BSc Risk Management and Business Intelligence','UST','D'),
('JS5901','BEng/BSc & BBA Dual Degree Program in Technology and Management','UST','D')    
";
$db->db_db_query($sql);


###############HKU###############
$sql = "INSERT IGNORE INTO JUPAS_PROGRAMME (JupasCode,ProgrammeFullName,Institution,ProgrammeType)
VALUES
('JS6004','Bachelor of Arts in Architectural Studies','HKU','D'),
('JS6016','Bachelor of Science in Surveying','HKU','D'),
('JS6028','Bachelor of Arts in Landscape Studies','HKU','D'),
('JS6030','Bachelor of Arts in Conservation','HKU','D'),
('JS6042','Bachelor of Arts in Urban Studies','HKU','D'),
('JS6054','Bachelor of Arts','HKU','D'),
('JS6066','Bachelor of Arts and Bachelor of Education in Language Education - English (double degree)','HKU','D'),
('JS6078','Bachelor of Arts and Bachelor of Laws (double degree)','HKU','D'),
('JS6080','Bachelor of Arts and Bachelor of Education in Language Education - Chinese (double degree)','HKU','D'),
('JS6092','Bachelor of Education in Early Childhood Education and Special Education','HKU','D'),
('JS6107','Bachelor of Dental Surgery','HKU','D'),
('JS6119','Bachelor of Education and Bachelor of Science (double degree)','HKU','D'),
('JS6157','Bachelor of Science in Speech and Hearing Sciences','HKU','D'),
('JS6195','Bachelor of Education and Bachelor of Social Sciences (double degree)','HKU','D'),
('JS6212','Bachelor of Arts and Sciences','HKU','D'),
('JS6224','Bachelor of Arts and Sciences in Applied Artificial Intelligence','HKU','D'),
('JS6236','Bachelor of Arts and Sciences in Design+','HKU','D'),
('JS6248','Bachelor of Arts and Sciences in Financial Technology','HKU','D'),
('JS6250','Bachelor of Arts and Sciences in Global Health and Development','HKU','D'),
('JS6406','Bachelor of Laws','HKU','D'),
('JS6456','Bachelor of Medicine and Bachelor of Surgery','HKU','D'),
('JS6468','Bachelor of Nursing','HKU','D'),
('JS6482','Bachelor of Chinese Medicine','HKU','D'),
('JS6494','Bachelor of Pharmacy','HKU','D'),
('JS6717','Bachelor of Social Sciences','HKU','D'),
('JS6729','Bachelor of Science in Actuarial Science','HKU','D'),
('JS6731','Bachelor of Social Work','HKU','D'),
('JS6767','Bachelor of Economics / Bachelor of Economics and Finance','HKU','D'),
('JS6781','Bachelor of Business Administration / Bachelor of Business Administration in Accounting and Finance','HKU','D'),
('JS6793','Bachelor of Business Administration (Information Systems)','HKU','D'),
('JS6808','Bachelor of Business Administration (Law) and Bachelor of Laws (double degree)','HKU','D'),
('JS6810','Bachelor of Social Sciences (Government and Laws) and Bachelor of Laws (double degree)','HKU','D'),
('JS6822','Bachelor of Journalism','HKU','D'),
('JS6860','Bachelor of Finance in Asset Management and Private Banking','HKU','D'),
('JS6884','Bachelor of Science in Quantitative Finance','HKU','D'),
('JS6896','Bachelor of Business Administration in International Business and Global Management','HKU','D'),
('JS6901','Bachelor of Science','HKU','D'),
('JS6925','Bachelor of Engineering in Biomedical Engineering','HKU','D'),
('JS6949','Bachelor of Biomedical Sciences','HKU','D'),
('JS6951','Bachelor of Engineering in Engineering Science','HKU','D'),
('JS6963','Bachelor of Engineering','HKU','D')    
";
$db->db_db_query($sql);


###############OUHK ###############
$sql = "INSERT IGNORE INTO JUPAS_PROGRAMME (JupasCode,ProgrammeFullName,Institution,ProgrammeType)
VALUES
('JS9001','Bachelor of Social Sciences with Honours in Psychology','OU','D'),
('JS9003','Bachelor of Social Sciences with Honours in Politics and Public Administration','OU','D'),
('JS9004','Bachelor of Social Sciences with Honours in Applied Social Studies','OU','D'),
('JS9005','Bachelor of Social Sciences with Honours in Global and China Studies','OU','D'),
('JS9006','Bachelor of Social Sciences with Honours in Ageing Society and Services Studies','OU','D'),
('JS9011','Bachelor of Arts with Honours in Chinese','OU','D'),
('JS9013','Bachelor of Arts with Honours in Language Studies and Translation','OU','D'),
('JS9016','Bachelor of Arts with Honours in Creative Advertising and Media Design','OU','D'),
('JS9017','Bachelor of Arts with Honours in English and Comparative Literature','OU','D'),
('JS9220','Bachelor of Business Administration with Honours in Professional Accounting','OU','D'),
('JS9222','Bachelor of Business Administration with Honours in Accounting and Taxation','OU','D'),
('JS9230','Bachelor of Business Administration with Honours in Business Management','OU','D'),
('JS9240','Bachelor of Business Administration with Honours in Global Business and Marketing','OU','D'),
('JS9250','Bachelor of Business Administration with Honours in Corporate Governance','OU','D'),
('JS9275','Bachelor of Business Administration with Honours in Finance and Real Estate','OU','D'),
('JS9280','Bachelor of Applied Psychology with Honours, Bachelor of Business Management with Honours','OU','D'),
('JS9291','Bachelor of Business Administration with Honours in Hotel and Sustainable Tourism Management','OU','D'),
('JS9294','Bachelor of Business Administration with Honours in Sports and eSports Management','OU','D'),
('JS9530','Bachelor of Education with Honours in English Language Teaching and Bachelor of English Language Studies with Honours','OU','D'),
('JS9540','Bachelor of English Language Studies with Honours','OU','D'),
('JS9550','Bachelor of Language Studies with Honours (Applied Chinese Language Studies)','OU','D'),
('JS9560','Bachelor of Education with Honours (Chinese Language Teaching) and Bachelor of Language Studies with Honours (Applied Chinese Language Studies)','OU','D'),
('JS9570','Bachelor of Language Studies with Honours (Bilingual Communication), Bachelor of Global Business with Honours','OU','D'),
('JS9580','Bachelor of Education with Honours in Early Childhood Education (Leadership and Special Educational Needs)','OU','D'),
('JS9720','Bachelor of Engineering with Honours in Electronic and Computer Engineering','OU','D'),
('JS9768','Bachelor of Science with Honours in Testing Science and Certification','OU','D')    
";
$db->db_db_query($sql);


###############SSSDP ###############
$sql = "INSERT IGNORE INTO JUPAS_PROGRAMME (JupasCode,ProgrammeFullName,Institution,ProgrammeType)
VALUES
 ('JSSA01','Offered by CIHE: Bachelor of Nursing (Honours)','SSSDP','D'),
('JSSA02','Offered by CIHE: Bachelor of Science (Honours) in Digital Entertainment','SSSDP','D'),
('JSSC02','Offered by CHC: Bachelor of Science (Honours) in Architecture','SSSDP','D'),
('JSSC03','Offered by CHC: Bachelor of Science (Honours) in Computer Science','SSSDP','D'),
('JSSH01','Offered by HSUHK: Bachelor of Business Administration (Honours) in Supply Chain Management','SSSDP','D'),
('JSSH02','Offered by HSUHK: Bachelor of Science (Honours) in Actuarial Studies and Insurance','SSSDP','D'),
('JSSH03','Offered by HSUHK: Bachelor of Arts (Honours) in Applied and Human-Centred Computing','SSSDP','D'),
('JSSH04','Offered by HSUHK: Bachelor of Science (Honours) in Data Science and Business Intelligence','SSSDP','D'),
('JSSH05','Offered by HSUHK: Bachelor of Management Science and Information Management (Honours)','SSSDP','D'),
('JSST01','Offered by TWC: Bachelor of Health Science (Honours) in Nursing','SSSDP','D'),
('JSST02','Offered by TWC: Bachelor of Science (Honours) in Medical Laboratory Science','SSSDP','D'),
('JSST03','Offered by TWC: Bachelor of Science (Honours) in Radiation Therapy','SSSDP','D'),
('JSST04','Offered by TWC: Bachelor of Science (Honours) in Occupational Therapy','SSSDP','D'),
('JSST05','Offered by TWC: Bachelor of Science (Honours) in Physiotherapy','SSSDP','D'),
('JSSU12','Offered by OUHK: Bachelor of Arts with Honours in Creative Writing and Film Arts','SSSDP','D'),
('JSSU14','Offered by OUHK: Bachelor of Fine Arts with Honours in Animation and Visual Effects','SSSDP','D'),
('JSSU15','Offered by OUHK: Bachelor of Fine Arts with Honours in Cinematic Design and Photographic Digital Art','SSSDP','D'),
('JSSU40','Offered by OUHK: Bachelor of Nursing with Honours in General Health Care','SSSDP','D'),
('JSSU50','Offered by OUHK: Bachelor of Nursing with Honours in Mental Health Care','SSSDP','D'),
('JSSU65','Offered by OUHK: Bachelor of Engineering with Honours in Testing and Certification','SSSDP','D'),
('JSSU71','Offered by OUHK: Bachelor of Computing with Honours in Internet Technology','SSSDP','D'),
('JSSU90','Offered by OUHK: Bachelor of International Hospitality and Attractions Management with Honours','SSSDP','D'),
('JSSU92','Offered by OUHK: Bachelor of Business Administration with Honours in Business Intelligence and Analytics','SSSDP','D'),
('JSSU95','Offered by OUHK: Bachelor of Sports and Recreation Management with Honours','SSSDP','D'),
('JSSU96','Offered by OUHK: Bachelor of Business Administration with Honours in Financial Technology and Innovation','SSSDP','D'),
('JSSU97','Offered by OUHK: Bachelor of Business Administration with Honours in Global Marketing and Supply Chain Management','SSSDP','D'),
('JSSV01','Offered by VTC-THEi: Bachelor of Arts (Honours) in Fashion Design','SSSDP','D'),
('JSSV02','Offered by VTC-THEi: Bachelor of Arts (Honours) in Product Design','SSSDP','D'),
('JSSV03','Offered by VTC-THEi: Bachelor of Arts (Honours) in Landscape Architecture','SSSDP','D'),
('JSSV04','Offered by VTC-THEi: Bachelor of Arts (Honours) in Culinary Arts and Management','SSSDP','D'),
('JSSV05','Offered by VTC-THEi: Bachelor of Engineering (Honours) in Civil Engineering','SSSDP','D'),
('JSSV06','Offered by VTC-THEi: Bachelor of Engineering (Honours) in Environmental Engineering and Management','SSSDP','D'),
('JSSV07','Offered by VTC-THEi: Bachelor of Arts (Honours) in Horticulture and Landscape Management','SSSDP','D'),
('JSSV08','Offered by VTC-THEi: Bachelor of Science (Honours) in Surveying','SSSDP','D'),
('JSSV09','Offered by VTC-THEi: Bachelor of Social Sciences (Honours) in Sports and Recreation Management','SSSDP','D'),
('JSSV10','Offered by VTC-THEi: Bachelor of Engineering (Honours) in Building Services Engineering','SSSDP','D'),
('JSSV11','Offered by VTC-THEi: Bachelor of Science (Honours) in Multimedia Technology and Innovation','SSSDP','D'),
('JSSV12','Offered by VTC-THEi: Bachelor of Science (Honours) in Information and Communications Technology','SSSDP','D')   
";
$db->db_db_query($sql);
?>