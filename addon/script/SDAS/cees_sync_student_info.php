<?php 
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/cees/libcees.php");
include_once($PATH_WRT_ROOT."includes/cust/student_data_analysis_system/libSDAS.php");
$lcees = new libcees();
$objSDAS = new libSDAS();

intranet_opendb();
define("success", 1);
define("failed", 0);

$sql = 'Select Distinct AcademicYearID from EXAM_DSE_STUDENT_SCORE';
$rs = $objSDAS->returnResultSet($sql);
$AcademicYearArr = Get_Array_By_Key($rs, 'AcademicYearID');
// $UserID = $_SESSION['UserID'];
foreach ((array)$AcademicYearArr as $AcademicYearID){
	$logInfoArr = $lcees-> getCeesSyncLog($Cond='',$OrderBy='DateInput DESC Limit 1',$LogID='','2',$UserID='',$AcademicYearID,$RecordStatus='1');
	
	if(!empty($logInfoArr)){
		$sql = 'Select Distinct StudentID from EXAM_DSE_STUDENT_SCORE WHERE AcademicYearID ='.$AcademicYearID;
		$StudentIDArr = Get_Array_By_Key($objSDAS->returnResultSet($sql), 'StudentID');
		$StudentIDArr_sql = implode(',',(array)$StudentIDArr);
		$sql = 'Select WebSAMSRegNo, EnglishName, ChineseName  FROM INTRANET_USER WHERE UserID IN ('.$StudentIDArr_sql.')';
		$StudentInfoArr = $objSDAS->returnResultSet($sql);
	
		if(!empty($StudentInfoArr)){
			$success = $lcees->syncStudentInfo($AcademicYearID,'DSEStudentInfo',$StudentInfoArr);
			//sync Log
			$log = 'success:'.$success['rda']['success'].', error:'.$success['rda']['error'].', '.$additionLogContent;
			if($dataHash == '' || $success['rda']['checksum'] == ''){
				$status = failed;
			}else if( empty($success['rda']['error']) && (  $exam == EXAMID_JUPAS || $dataHash==$success['rda']['checksum'])){
				// EXAMID_JUPAS checksum logic fail ... don't know why after adding $dataArrTemp['Prog'] checksum return from CEES is different
				$status = success;
			}else{
				$status = failed;
			}
			echo "#####Successfully sync Data (".$AcademicYearID.")#####";
		}else{
			echo "#####Fail to sync Data (".$AcademicYearID.") Reason: No Student Record Found#####";
		}
	}else{
		echo "#####Fail to sync Data (".$AcademicYearID.") Reason: Not yet submitted#####";
	}
	echo "<br>";
}
intranet_closedb();
?>