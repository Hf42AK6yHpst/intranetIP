<?php
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
//include_once($PATH_WRT_ROOT."includes/lib.php");

intranet_opendb();

$ldb = new libdb();


if($flag1==1) {
	####################################################################
	##### This is for the update of WebSAMSRegno if "#" is missing #####
	#####				update STUDENT only						   #####
	####################################################################
	
	$sql = "SELECT UserID, UserLogin, WebSAMSRegNo FROM INTRANET_USER WHERE REcordType=2 AND WebSAMSRegNo NOT LIKE '#%' AND WebSAMSRegNo IS NOT NULL AND WebSAMSRegNo != ''";
	$UserIdAry = $ldb->returnVector($sql);
	
	# update 
	$sql = "UPDATE INTRANET_USER SET WebSAMSRegNO = CONCAT('#', WebSAMSRegNo) WHERE UserID IN (".implode(',', $UserIdAry).")";
	$ldb->db_db_query($sql);
	
	intranet_closedb();
	
	echo "###### UPDATE WebSAMS RegNo (add \"#\") #####<br>";
	echo sizeof($UserIdAry)." record(s) updated.";
	echo "<br>";
	echo "<br>";
	echo "<br>";
	echo "<br>";
} 

if($flag2==1) {

	#########################################################################
	##### 	This is for synchronize the WebSAMSRegno 				   	#####
	#####		from ipf to student account						   		#####
	#####	if WebSAMSRegno exist in ipf, but not in student account  	#####
	#########################################################################
	$NoOfRecord = synWSRegnoToIntranetUser();

	echo "###### Synchronize WebSAMS RegNo from ipf to student account #####<br>";
	echo "<font color='red'>".$NoOfRecord." records updated.</font>";
	echo "<br>";
	echo "<br>";
	echo "<br>";
	echo "<br>";
}
?>

<form name="form1" method="get" action="">
Update of WebSAMSRegno if "#" is missing (student account only) 
<br>
<input type="button" name="flag1" id="flag1" value='Update "#"' onClick="self.location.href='update_websams_regno.php?flag1=1'">

<br>
<br>

Synchronize WebSAMSRegno from iPortfolio to student account (if WebSASMSRegno exists in ipf, but not in Student Account)
<br>
<input type="button" name="flag2" id="flag2" value='Synchronize with ipf' onClick="self.location.href='update_websams_regno.php?flag2=1'">

</form>


<?

?>