<?php
/*
 * Aim: patch data to break "stock-take and write-off" into "stock-take" and "write-off" permission for eLib+   
 * 
 * 	2016-04-25 [Cameron] create this file		
 * 
 */
 

$path = ($FromCentralUpdate) ? $intranet_root."/" : "../../";	

include_once($path."includes/global.php");
include_once($path."includes/libdb.php");
include_once($path."includes/liblibrarymgmt.php");

if (!$FromCentralUpdate) {
	intranet_auth();
	intranet_opendb();
}

#########################################################################################################


## Use Library
$libdb = new liblms();


## Init 
$result = array();
############## Apply Patch ##############

#########################################
$insertValuesArr = array();
$groupRightIDArr = array();

## Get Data
if(($_POST['Flag']!=1) && !$FromCentralUpdate) {
	$isApply = false;
	echo "<form method='post'><input value='run script' type='submit'><input type='hidden' name='Flag' value=1></form>";
}
else {
	$isApply = true;
}


$sql = "SELECT GroupRightID, GroupID, IsAllow, DateModified, LastModifiedBy 
		FROM LIBMS_GROUP_RIGHT 
		WHERE `Section`='admin' 
		AND `Function`='stock-take and write-off' 
		AND IsAllow=1 order by GroupRightID";
$groupRightArr = $libdb->returnResultSet($sql);
$nrRs = count($groupRightArr);
if($nrRs > 0){
	for ($i=0; $i<$nrRs; $i++){
		$rs = $groupRightArr[$i];
		$groupRightIDArr[] = $rs['GroupRightID'];
		$insertValuesArr[] = "('".$rs['GroupID']."','admin','write-off',1,now(),'".$_SESSION['UserID']."')";
	}
	
	## main
	if (count($groupRightIDArr) > 0) {
		$sql = "UPDATE LIBMS_GROUP_RIGHT SET `Function`='stock-take' WHERE GroupRightID IN ('".implode("','",$groupRightIDArr)."')";
		echo $sql.'<br>';
		if($isApply){
			$result['stock-take'] = $libdb->db_db_query($sql);
		}
	}
	
	if (count($insertValuesArr) > 0) {
		$sql = "INSERT INTO LIBMS_GROUP_RIGHT (GroupID, Section, Function, IsAllow, DateModified, LastModifiedBy) VALUES ";
		$sql .= implode(",",$insertValuesArr);
		echo $sql.'<br>';
		if($isApply){
			$result['write-off'] = $libdb->db_db_query($sql);
		}
	}
}


if ($FromCentralUpdate) {
	if ($result['stock-take']) {
		$x .= "update stock-take right Success<br>\r\n";
	}
	if ($result['write-off']) {
		$x .= "add write-off right Success<br>\r\n";
	}
}
else {
	echo '<style> 
	html, body, table, select, input, textarea{ font-size:12px; }
	</style>';
	if ($_POST['Flag']) {
		debug_r($result);
	}
	intranet_closedb();	
}

	
#########################################################################################################

?>