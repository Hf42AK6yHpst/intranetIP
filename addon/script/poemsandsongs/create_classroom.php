<?php

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libeclass40.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/icalendar.php");
include_once($eclass40_filepath."/src/plugin/poemsandsongs/config.php");

## Prompt login
include_once("../../check.php");

eclass_opendb();

$course_code = 'poemsandsongs';
$course_name = 'Poems and Songs';
$course_desc = 'Poems and Songs Classroom';
$max_user = "NULL";
$max_storage = "NULL";

$lo = new libeclass();

$sql = "select course_id from ".$eclass_db.".course where course_code='".$course_code."'";
$count_obj = $lo->returnVector($sql);
$course_id = $count_obj[0];
$lo->RoomType = 7;
if($course_id=='')
{
	$course_id = $lo->eClassAdd($course_code, $course_name, $course_desc, $max_user, $max_storage);
	$lo->eClassSubjectUpdate($subj_id, $course_id);
	echo 'Poems and Songs Classroom created!<br/>';
}
else
{
	echo 'Poems and Songs Classroom already created!<br/>';
}
# check if there's any notes previously input
$sql = "SELECT count(*) from ".$eclass_prefix."c".$course_id.".notes";
$notes_obj = $lo->returnVector($sql);

if($notes_obj[0]>0)
die('Notes exist in Poems and Songs Classroom!');

$notes_result = array();
$chapter_ct = 1;

foreach($poemsandsongs_cfg['chapter'] as $chapter_key=>$chapter_ary)
{
	for($a=0;$a<=sizeof($chapter_ary['Notes']['en']);$a++)
	{
		$title = ($a==0)?$chapter_ary['ChapterName']['en']:$chapter_ary['Notes']['en'][$a-1];
		
		$sql = "INSERT INTO
					".$eclass_prefix."c".$course_id.".notes 
					(Title,url,a_no,b_no,status,inputdate,modified)
				VALUES
					('".addslashes($title)."','".$chapter_key."',".$chapter_ct.",".$a.",1,now(),now())";
		$notes_result[] = $lo->db_db_query($sql);
		
		if($a!=0)
		{
			$url = "src/itextbook/poemsandsongs/?Mode=action&Task=view&Unit=".$chapter_ary['Unit']."&Page=".$a;
					
			$notes_id = $lo->db_insert_id();
			$sql = "INSERT INTO
						".$eclass_prefix."c".$course_id.".notes_section
						(notes_id,notes_section_type,url,inputdate,modified)
					VALUES
						(".$notes_id.",14,'".$url."',now(),now())";
			$notes_result[] = $lo->db_db_query($sql);
		}
	}
	$chapter_ct++;
}

if(in_array(false,$notes_result))
	echo 'Error occured when createing Poems and Songs Notes!';
else
	echo 'Poems and Songs Notes created!';

# Create Poems and Songs table in special room DB
$lo->db = $eclass_prefix."c".$course_id;

$sql = "CREATE TABLE IF NOT EXISTS `poemsandsongs_exercise` (
			`record_id` int(8) NOT NULL auto_increment,
			`class` varchar(255) default NULL,
			`class_number` int(8) default NULL,
			`student_name` varchar(255) default NULL,
			`user_id` int(8) NOT NULL,
			`unit_id` int(8) NOT NULL,
			`page_id` int(8) NOT NULL,
			`section_id` varchar(5) default NULL,
			`inputdate` datetime default NULL,
			`content` text,
			`score` float default NULL,
			`academic_year_id` int(8) default NULL,
			PRIMARY KEY  (`record_id`)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8";
echo $lo->db_db_query($sql)? "<br/>Table - 'poemsandsongs_exercise' created!":"<br/>Table - 'poemsandsongs_exercise' failed to create!";
		
$sql = "CREATE TABLE IF NOT EXISTS `poemsandsongs_setting` (
			`record_id` int(8) NOT NULL auto_increment,
			`year_id` int(8) default NULL,
			`academic_year_id` int(8) default NULL,
			`unit_id` int(8) default NULL,
			`start_date` datetime default NULL,
			`end_date` datetime default NULL,
			`last_modified_by` int(8) default NULL,
			`last_modified_time` datetime default NULL,			
			PRIMARY KEY  (`record_id`)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8";
echo $lo->db_db_query($sql)? "<br/>Table - 'poemsandsongs_setting' created!":"<br/>Table - 'poemsandsongs_setting' failed to create!";

$sql = "CREATE TABLE IF NOT EXISTS `poemsandsongs_writing` (
			`record_id` int(8) NOT NULL auto_increment,
			`class` varchar(255) default NULL,
			`class_number` int(8) default NULL,
			`student_name` varchar(255) default NULL,
			`user_id` int(8) NOT NULL,
			`unit_id` int(8) NOT NULL,
			`page_id` int(8) NOT NULL,
			`topic_id` int(8) NOT NULL,
			`inputdate` datetime default NULL,
			`modifydate` datetime default NULL,
			`grade` varchar(20) default NULL,
			`status` varchar(20) default NULL,
			`is_display_board` char(1) NOT NULL default 'F',
			`is_ePost` char(1) NOT NULL default 'F',
			`content` longtext,
			`comment` text,
			`attachment` text,
			`parent_record_id` int(8) default NULL,
			`academic_year_id` int(8) default NULL,
			PRIMARY KEY  (`record_id`)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8";
echo $lo->db_db_query($sql)? "<br/>Table - 'poemsandsongs_writing' created!":"<br/>Table - 'poemsandsongs_writing' failed to create!";

# Create iTextbook Record
$sql = "SELECT COUNT(*) FROM ".$intranet_db.".ITEXTBOOK_BOOK WHERE BookType = 'poemsandsongs' AND Status = 0";
$NoOfBook = current($lo->returnVector($sql));

if($NoOfBook==0){
	$sql = "SELECT UserID FROM ".$intranet_db.".INTRANET_USER WHERE UserLogin = 'broadlearning'";
	$UserID = current($lo->returnVector($sql));
	
	$sql = "INSERT INTO
				".$intranet_db.".ITEXTBOOK_BOOK
				(BookType, BookName, BookLang, Chapters, Status, DateInput, DateModified, ModifiedBy)
			VALUES
				('poemsandsongs', 'Poems and Songs', 'en', '1', 0, NOW(), NOW(), '$UserID')";
	echo $lo->db_db_query($sql)? "<br/>Poems and Songs iTextbook Record Created!":"<br/>Failed to create Poems and Songs iTextbook Record";
}

eclass_closedb();

?>