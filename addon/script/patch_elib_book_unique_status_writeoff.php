<?php
/*
 * 	Purpose: update RecordStatus to 'WRITEOFF' in LIBMS_BOOK_UNIQUE if corresponding book item in LIBMS_WRITEOFF_LOG is WRITEOFF [case #L123983]
 * 
 *  2017-09-06 [Cameron] create this file
 */

$path = ($FromCentralUpdate) ? $intranet_root."/" : "../../";
$PATH_WRT_ROOT = $path;
include_once($path."includes/global.php");
include_once($path."includes/libdb.php");
include_once($path."includes/liblibrarymgmt.php");
include_once($path."includes/libelibrary.php");

if (!$FromCentralUpdate) {
	intranet_auth();
	intranet_opendb();
}

$libms = new liblms();
$libel = new elibrary();

if(($_POST['Flag']!=1) && !$FromCentralUpdate) {
	$isApply = false;
	echo("<form method='post'><input value='run script' type='submit'><input type='hidden' name='Flag' value=1></form>");
}
else {
	$isApply = true;
}


## 1. get BookID and UniqueID from LIBMS_BOOK_UNIQUE which RecordStatus='NORMAL' but LIBMS_WRITEOFF_LOG.Result='WRITEOFF'
	$bookStatus = " AND lbu.RecordStatus='NORMAL'";  
 	$sql = "SELECT 
					lbu.ACNO,
					lbu.BarCode,
					lb.BookTitle,
					lb.BookID, 
					lbu.UniqueID, 
					lwl.WriteOffID,
					lwl.StocktakeSchemeID
			FROM 
					LIBMS_BOOK_UNIQUE AS lbu
					INNER JOIN LIBMS_BOOK lb ON lb.BookID = lbu.BookID
					LEFT JOIN LIBMS_WRITEOFF_LOG lwl ON (lwl.BookUniqueID = lbu.UniqueID AND lwl.BookID = lbu.BookID)
			WHERE 
					lbu.RecordStatus NOT LIKE 'DELETE'
			AND 	lwl.Result='WRITEOFF'
			AND 	lwl.StocktakeSchemeID IS NOT NULL
					{$bookStatus}
			GROUP BY lbu.UniqueID
			ORDER BY lb.BookID";
//debug_r($sql);
$rs = $libms->returnResultSet($sql);
$nrRec = count($rs);
//debug_pr($rs);


$error = array();

if ($nrRec>0) {
	for($i=0;$i<$nrRec;$i++) {
		$r = $rs[$i];
		print '<span style="font-weight:bold">Book Title = '.$r['BookTitle'].'</span> ACNO = '.$r['ACNO'].'<br>';
		
		if ($isApply) {
			$libms->Start_Trans();

			$bookUniqueID = $r['UniqueID'];
			$bookID = $r['BookID'];
			$stocktakeSchemeID = $r['StocktakeSchemeID'];

			$result = array();
			$result['UpdateUniqueBookSuccess'] = $libms->UPDATE_UNIQUE_BOOK($bookUniqueID,'WRITEOFF');
			$result['UpdateBookCopyInfo'] = $libms->UPDATE_BOOK_COPY_INFO($bookID);
			
		
			if ($stocktakeSchemeID) {
				$sql = "SELECT StocktakeLogID FROM LIBMS_STOCKTAKE_LOG WHERE StocktakeSchemeID ='$stocktakeSchemeID' AND BookID='$bookID' AND BookUniqueID='$bookUniqueID'";
				$st_rs = $libms->returnResultSet($sql);
				if (count($st_rs) == 1) {
					$stocktakeLogID = $st_rs[0]['StocktakeLogID'];
					$result['RemoveStocktakeLogSuccess'] = $libms->DELETE_STOCKTAKE_LOG_RECORD($stocktakeLogID);	
				}
			}
				
			if (!in_array(false,$result)) {
				$libms->Commit_Trans();
								
				//syn the front end of the book info
				$libel->SYNC_PHYSICAL_BOOK_TO_ELIB($bookID);
				echo "<span style=\"color:green; font-weight:bold\">success</span><br>";
			}
			else {
				$libms->RollBack_Trans();
				echo "<span style=\"color:red; font-weight:bold\">fail</span><br>";
			}
				
		}			
		
	}	// end for
	
}
else {
	print 'No record found<br>';
}


if (!$FromCentralUpdate) {
	intranet_closedb();
}
?>