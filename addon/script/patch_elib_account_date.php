<?php
/*
 * 	Purpose: sync min(LIBMS_BOOK_UNIQUE.CreationDate) group by BookID to INTRANET_ELIB_BOOK.AccountDate
 * 
 *  2017-01-25 [Cameron] create this file
 */

$PATH_WRT_ROOT = "../../"; 
$path = ($FromCentralUpdate) ? $intranet_root."/" : "../../";
include_once($path."includes/global.php");
include_once($path."includes/libdb.php");
include_once($path."includes/liblibrarymgmt.php");
include_once($path."includes/libelibrary.php");

if (!$FromCentralUpdate) {
	intranet_auth();
	intranet_opendb();
}

$liblms = new liblms();
$elib = new elibrary();

if(($_POST['Flag']!=1) && !$FromCentralUpdate) {
	$isApply = false;
	echo("<form method='post'><input value='run script' type='submit'><input type='hidden' name='Flag' value=1></form>");
}
else {
	$isApply = true;
}

$error = array();
$limit_upper = 1000;
$sql = "SELECT COUNT(DISTINCT BookID) AS CNT from LIBMS_BOOK_UNIQUE WHERE RecordStatus<>'DELETE'";
$rs = $liblms->returnResultSet($sql);
if (count($rs) > 0) {
	$no_of_rec = $rs[0]['CNT'];
	$maxLoop = ceil($no_of_rec / $limit_upper);
}
else {
	$maxLoop = 10000;		
}
$limit_i = 0;
$retrieve_done = false;
$rows = array();

$sql_books = "SELECT MIN(CreationDate) AS AccountDate, BookID 
				FROM LIBMS_BOOK_UNIQUE 
				WHERE RecordStatus<>'DELETE' 
				AND CreationDate IS NOT NULL 
				AND CreationDate <>'0000-00-00 00:00:00' 
				GROUP BY BookID ORDER BY BookID";

if ($isApply){
	while (!$retrieve_done && $limit_i<$maxLoop)
	{
		$sql_limit_start = $limit_i * $limit_upper;
		$sql = $sql_books . " limit {$sql_limit_start}, {$limit_upper}";
		$rows = $liblms->returnResultSet($sql);
	
		$limit_i++;
		$retrieve_done = (sizeof($rows)<$limit_upper);	
	
		for ($i=0, $iMax=count($rows); $i<$iMax; $i++)
		{
			$inetBookID = $elib->physical_book_init_value + $rows[$i]["BookID"];
			$check_sql = "SELECT BookID FROM INTRANET_ELIB_BOOK WHERE BookID='".$inetBookID."'";
			$rs = $elib->returnResultSet($check_sql);
			if (count($rs) > 0) {
				$inet_sql = "UPDATE INTRANET_ELIB_BOOK SET AccountDate = '".addslashes($rows[$i]['AccountDate'])."' WHERE BookID='".$inetBookID."'";
				$error[] = $elib->db_db_query($inet_sql);
			}
			else {
				$elib->SYNC_PHYSICAL_BOOK_TO_ELIB($inetBookID);
			}
		}
		unset($rows);
	}
}

if (!$FromCentralUpdate) {
	echo '<META http-equiv="Content-Type"  content="text/html" Charset="UTF-8"/>';
}
 
if($isApply){
	$result = in_array(false,$error) ? "Fail to sync Account Date!<br>" : "<span style=\"color:green; font-weight:bold\">Sync Account Date success!</span><br>";
	if ($FromCentralUpdate) {
		$x .= $result."\r\n";
	}
	else {
		echo $result;				
	}
}
if (!$FromCentralUpdate) {
	intranet_closedb();
}

?>