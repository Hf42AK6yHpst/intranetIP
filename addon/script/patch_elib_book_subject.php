<?php
/*
 * 	Purpose: synchronization of book subject from eLib+ LIBMS_BOOK to ip INTRANET_ELIB_BOOK
 * 
 *  2016-07-08 [Cameron] create this file
 */

$path = ($FromCentralUpdate) ? $intranet_root."/" : "../../";
include_once($path."includes/global.php");
include_once($path."includes/libdb.php");
include_once($path."includes/liblibrarymgmt.php");
include_once($path."includes/libelibrary.php");

if (!$FromCentralUpdate) {
	intranet_auth();
	intranet_opendb();
}

$liblms = new liblms();
$elib = new elibrary();

if(($_POST['Flag']!=1) && !$FromCentralUpdate) {
	$isApply = false;
	echo("<form method='post'><input value='run script' type='submit'><input type='hidden' name='Flag' value=1></form>");
}
else {
	$isApply = true;
}

$offset = $elib->physical_book_init_value;
$offset = $offset ? $offset : 10000000;

$sql = "SELECT 	BookID+$offset as BookID, Subject
		FROM  	LIBMS_BOOK
		WHERE 	Subject is not null and Subject<>'' and	(RecordStatus<>'DELETE' or RecordStatus is null)
		ORDER BY Subject, BookID";

$rs = $liblms->returnResultSet($sql);
$nrRec = count($rs);

//debug_pr($sql);

$error = array();
$subject_array = array();
if ($nrRec > 0) {
	foreach((array)$rs as $k=>$v) {
		$subject = $v['Subject'];
		$bookID = $v['BookID'];
		$subject_array[$subject][] = $bookID;	
	}
} 
//debug_r($subject_array);

foreach($subject_array as $k=>$v){
	$book_ids = implode(",",$v);
	$sql = "UPDATE INTRANET_ELIB_BOOK set RelevantSubject='".$elib->Get_Safe_Sql_Query($k)."' WHERE BookID IN (".$book_ids.") AND BookID>$offset";
	if($isApply){
		$error[] = $elib->db_db_query($sql);
	}
	debug_pr($sql);
}

$sql = "select count(BookID) as NrRec from LIBMS_BOOK where Subject is not null and Subject<>'' and (RecordStatus<>'DELETE' or RecordStatus is null)";
$rs = $liblms->returnResultSet($sql);
if (count($rs)) {
	$result = "Number of record in LIBMS_BOOK with subject = ".$rs[0]['NrRec']."<br>"; 
	$x .=  $result."\r\n";
	echo $result; 	
}

$sql = "select count(BookID) as NrRec from INTRANET_ELIB_BOOK where RelevantSubject is not null and RelevantSubject<>'' and BookID>$offset";
$rs = $elib->returnResultSet($sql);
if (count($rs)) {
	$result = "Number of record in INTRANET_ELIB_BOOK with RelevantSubject = ".$rs[0]['NrRec']."<br>"; 
	$x .=  $result."\r\n";
	echo $result; 	
}

if($isApply){
	$result = in_array(false,$error) ? "Fail to sync subject!<br>" : "<span style=\"color:green; font-weight:bold\">Sync subject success!</span><br>";
	if ($FromCentralUpdate) {
		$x .= $result."\r\n";
	}
	else {
		echo $result;				
	}
}
if (!$FromCentralUpdate) {
	intranet_closedb();
}
?>