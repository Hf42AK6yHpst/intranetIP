<?php
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."addon/check.php");
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/liblibrarymgmt.php");

?>
<META http-equiv="Content-Type"  content="text/html" Charset="UTF-8"  />
<?php
//intranet_auth();
intranet_opendb();

$liblms = new liblms();

//if($_POST['Flag']!=1) {
//	die("<form method='post'><input value='run script' type='submit'><input type='hidden' name='Flag' value=1></form>");
//}
//echo $_POST['Flag'];

if($_POST['Flag'] > 0 ) {
	## all records
	$sql = "select u.UserID, u.EnglishName, u.ClassName, u.ClassNumber, u.Balance, 
	  b.BookTitle,
	  bu.RecordStatus,
	  bl.BorrowTime, bl.DueDate, bl.ReturnedTime,
	  o.OverDueLogID, o.Payment, o.PaymentReceived
	  from LIBMS_BOOK b
	  inner join LIBMS_BOOK_UNIQUE bu on bu.BookID=b.BookID
	  inner join LIBMS_BORROW_LOG bl on bl.UniqueID=bu.UniqueID
	  inner join LIBMS_USER u on u.UserID=bl.UserID
	  inner join LIBMS_OVERDUE_LOG o on o.BorrowLogID=bl.BorrowLogID
	where o.PaymentReceived > o.Payment
		and o.Payment > 0
		order by u.EnglishName,
		    bl.BorrowTime
	";
debug_pr('all record sql');	
debug_pr($sql);	
	$rs = $liblms->returnArray($sql,null,1);
	$nrRec = count($rs);
//debug_pr($rs);
	if ($nrRec > 0) {
		print "<h2>overall records</h2><br>";
		print "<table border=1 cellspacing=1 cellpadding=1>";
		print "<tr>";
		foreach($rs[0] as $k=>$v) {
			print "<th>$k</th>";
		}
		print "</tr>";
		
		for ( $i= 0; $i < $nrRec; $i++ ) {
			$currRs = $rs[$i];
			print "<tr>";			
			foreach($currRs as $k=>$v) {
				print "<td>$v</td>";
			}
			print "</tr>";	
		}
		print "</table>";
	}
	
	
	## get all user whose payment received item > outstanding
	$sql = "select distinct u.UserID
	  from LIBMS_BOOK b
	  inner join LIBMS_BOOK_UNIQUE bu on bu.BookID=b.BookID
	  inner join LIBMS_BORROW_LOG bl on bl.UniqueID=bu.UniqueID
	  inner join LIBMS_USER u on u.UserID=bl.UserID
	  inner join LIBMS_OVERDUE_LOG o on o.BorrowLogID=bl.BorrowLogID
	where o.PaymentReceived > o.Payment
		and o.Payment > 0
		order by u.UserID
	";
	$rs = $liblms->returnArray($sql,null,1);
	$nrRec = count($rs);
	
	## loop for each user, get all overdue log where payment received item > outstanding of the user
	print "<h2>loop for each user</h2><br>";	
	for ( $i= 0; $i < $nrRec; $i++ ) {
		$userID = $rs[$i]['UserID'];
		$sql = "select u.UserID, u.EnglishName, u.ClassName, u.ClassNumber, u.Balance, 
		  b.BookTitle,
		  bu.RecordStatus,
		  bl.BorrowTime, bl.DueDate, bl.ReturnedTime,
		  o.OverDueLogID, o.Payment, o.PaymentReceived
		  from LIBMS_BOOK b
		  inner join LIBMS_BOOK_UNIQUE bu on bu.BookID=b.BookID
		  inner join LIBMS_BORROW_LOG bl on bl.UniqueID=bu.UniqueID
		  inner join LIBMS_USER u on u.UserID=bl.UserID
		  inner join LIBMS_OVERDUE_LOG o on o.BorrowLogID=bl.BorrowLogID
		where o.PaymentReceived > o.Payment
			and o.Payment > 0
			and u.UserID='$userID'
			order by bl.BorrowTime
		";
debug_pr($sql);	
		$payment_rs = $liblms->returnArray($sql,null,1);
		$nrPayment = count($payment_rs);
debug_pr('payment details for user '. ($nrPayment > 0 ? $payment_rs[0]['EnglishName'] : $userID));		
debug_pr($payment_rs);	
		for ( $j= 0; $j < $nrPayment; $j++ ) {
			$overDueLogID = $payment_rs[$j]['OverDueLogID'];
			$payment 			= $payment_rs[$j]['Payment'];
			$paymentReceived 	= $payment_rs[$j]['PaymentReceived'];
			
			$update_sql = "UPDATE LIBMS_OVERDUE_LOG SET PaymentReceived=Payment where OverDueLogID='$overDueLogID'";
debug_pr($update_sql);
			if ($_POST['Flag'] == 1) {
				$liblms->db_db_query($update_sql);				
			}
		}
		
		$sql = "select sum(ifnull(o.PaymentReceived,0)-o.Payment) as Balance FROM LIBMS_OVERDUE_LOG o 
					inner join LIBMS_BORROW_LOG bl on bl.BorrowLogID=o.BorrowLogID
				where bl.UserID='$userID'
				and o.RecordStatus<>'WAIVED'
				and o.Payment>0";
debug_pr('new balance sql for user '. ($nrPayment > 0 ? $payment_rs[0]['EnglishName'] : $userID));		
debug_pr($sql);			
		$balance_rs = $liblms->returnArray($sql,null,1);
debug_pr($balance_rs);	
		if (count($balance_rs) > 0) {
			$new_balance = $balance_rs[0]['Balance'];
			if ($new_balance > 0) {
				print "<h3 style='color:red'>Warning: Balance still negative! Name: ".$payment_rs[0]['EnglishName']." --> NewBalance: $new_balance</h3><br>";
			}
			$update_sql = "update LIBMS_USER set Balance='$new_balance' where UserID='$userID'";
debug_pr($update_sql);		
			if ($_POST['Flag'] == 1) {	
				$liblms->db_db_query($update_sql);
			}		
		}		
	}	// end loop for each user
}	// $_POST['Flat'] > 0

intranet_closedb();

?>
 
<html>
<head>
<script language="javascript">
	function run_script(type) {
		document.f1.Flag.value = type;
		document.f1.submit();
	}
</script>
</head>
<body>
	<form name="f1" method="post" action="<?=$PATH_WRT_ROOT?>addon/script/patch_elibp_payment_balance.php">
		<input type="button" name="check_record" value="check record only" onClick="run_script(2)"><br><br>
		<input type="button" name="do_patching" value="do data patch" onClick="run_script(1)"><br>
		<input type="hidden" name="Flag" value="0">
	</form>
</body>
</html>
