<?php
/*
 * 	Purpose: synchronization of book category from eLib+ to eBook according to setting of $sys_custom['eLibraryPlus']['BookCategorySyncTwoLang']
 * 
 * 	2016-06-24 [Cameron] fix bug: should group by BookCategoryCode and BookCategoryType but not BookCategoryCode only
 *  
 *  2015-09-15 [Cameron] create this file
 */
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/liblibrarymgmt.php");
include_once($PATH_WRT_ROOT."includes/libelibrary.php");

intranet_auth();
intranet_opendb();

$liblms = new liblms();
$elib = new elibrary();

if($_POST['Flag']!=1) {
	die("<form method='post'><input value='run script' type='submit'><input type='hidden' name='Flag' value=1></form>");
}

$sql = "select count(BookCategoryCode) as NumRec from LIBMS_BOOK_CATEGORY where DescriptionEn like '% & %' or DescriptionChi like '% & %' ";
$check = $liblms->returnArray($sql);
if (count($check) > 0) {
	if ($check[0]['NumRec'] > 0) {
		$sql = "update LIBMS_BOOK_CATEGORY set DescriptionEn=replace(DescriptionEn,' & ',' &amp; '), DescriptionChi=replace(DescriptionChi,' & ',' &amp; ')";
		$error[] = $liblms->db_db_query($sql);
	}
} 

$sql = "select 
	c.BookCategoryCode,
	c.DescriptionEn,
	c.DescriptionChi,
	c.BookCategoryType
from 
	LIBMS_BOOK_CATEGORY c
inner join 
	LIBMS_BOOK b on b.BookCategoryCode=c.BookCategoryCode
where c.BookCategoryCode<>''	
group by 
	c.BookCategoryCode,
	c.BookCategoryType
order by 
	c.BookCategoryCode";
$rs = $liblms->returnArray($sql);

debug_pr($rs);
$error = array();

for ($i=0,$iMax=count($rs); $i<$iMax; $i++)
{
	$ret = $elib->SYNC_BOOK_CATEGORY_TO_ELIB($rs[$i]);
	if (!$ret) {
		debug_pr("Error when sync BookCategoryCode = " . $rs[$i]['BookCategoryCode'] . " BookCategory = " . $rs[$i]['DescriptionEn']. " BookCategoryType = " . $rs[$i]['BookCategoryType']);
	}
	$error[] = $ret;
}

echo in_array(false,$error) ? "Fail!<br>" : "<span style=\"color:green; font-weight:bold\">done & successful!</span><br>";

intranet_closedb();
?>