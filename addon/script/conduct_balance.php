<?
# modifying : 

#################################################################################
# make conduct score consistent between merit record (DISCIPLINE_MERIT_RECORD)	#
# and conduct score balance (DISCIPLINE_STUDENT_CONDUCT_BALANCE)				#
#																				#
# conduct score inconsistency due to the time gap of following change :			#
#		originally : conduct score changes when the AP record is approved		#
#		new : conduct score changes when the AP record is released				#
# 																				#
#			!!!! Re-Calculate the conduct Score of students	!!!!				#
#																				#
# /addon/ip25/index.php will call this file								  		#
#################################################################################

# !!!!!!!!!!!! ATTENTION !!!!!!!!!!!!!!!! #
# ***** this is Year Base calculation *** #


$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
/*
intranet_auth();
intranet_opendb();
*/
$ldiscipline = new libdisciplinev12();

$AcademicYearID = Get_Current_Academic_Year_ID();
$yearName = $ldiscipline->getAcademicYearNameByYearID($AcademicYearID);
$YearTermID = getCurrentSemesterID();
$updatedStudentAry = array();

$termData = getSemesters($AcademicYearID);
$termOfCurrentYear = array();
foreach($termData as $id=>$name) {
	$termOfCurrentYear[] = $id;
}

$baseMark = $ldiscipline->getConductMarkRule('baseMark');
$newConductScore = $baseMark;

# students who have revised conduct mark
$sql = "SELECT DISTINCT StudentID FROM DISCIPLINE_CONDUCT_SCORE_CHANGE_LOG WHERE AcademicYearID='$AcademicYearID'";
$studentIDAry = $ldiscipline->returnVector($sql);

# select year term
$sql = "SELECT DISTINCT YearTermID FROM DISCIPLINE_CONDUCT_SCORE_CHANGE_LOG WHERE AcademicYearID='$AcademicYearID'";
$termIDAry = $ldiscipline->returnVector($sql);

# for every year term
foreach($termIDAry as $termID) {
	$termName = $ldiscipline->getTermNameByTermID($termID);
	$baseMark = $newConductScore;

	
	$sql = "select StudentID, SUM(ConductScoreChange) from DISCIPLINE_MERIT_RECORD where RecordStatus=".DISCIPLINE_STATUS_APPROVED." AND ReleaseStatus=".DISCIPLINE_STATUS_RELEASED." AND AcademicYearID=$AcademicYearID AND YearTermID=$termID GROUP BY StudentID";
	$conductScoreAry = $ldiscipline->returnArray($sql, 2);
	$count = 0;
	$revised = 0;
	
	for($i=0; $i<sizeof($conductScoreAry); $i++) {
		
		list($studentid, $score) = $conductScoreAry[$i];
		$baseMark = (isset($newBaseMark[$studentid])) ? $newBaseMark[$studentid] : $baseMark;
		
		$newConductScore = $baseMark + $score;
	
		$sql = "UPDATE DISCIPLINE_STUDENT_CONDUCT_BALANCE SET ConductScore=$newConductScore WHERE StudentID=$studentid AND AcademicYearID=$AcademicYearID AND YearTermID=$termID";
		$ldiscipline->db_db_query($sql);
		
		$sql = "SELECT ToScore FROM DISCIPLINE_CONDUCT_SCORE_CHANGE_LOG WHERE StudentID=$studentid AND AcademicYearID=$AcademicYearID AND YearTermID=$termID ORDER BY RecordID DESC LIMIT 1";
		$result = $ldiscipline->returnVector($sql);
		$toScore = $result[0];
	
		$sql = "INSERT INTO DISCIPLINE_CONDUCT_SCORE_CHANGE_LOG 
					(StudentID, Year, Semester, FromScore, ToScore, ChangeUserID, RecordType, DateInput, DateModified, AcademicYearID, YearTermID)
				VALUES
					('$studentid', '$yearName', '$termName', $toScore, '$newConductScore', '$UserID', '99', NOW(), NOW(), '$AcademicYearID', '$termID')	
		";
		
		$ldiscipline->db_db_query($sql);
		$newBaseMark[$studentid] = $newConductScore;
		$count++;

		//$studentIDAry = remove_item_by_value($studentIDAry, $studentid);
		$key = array_search($studentid, $studentIDAry);
		unset($studentIDAry[$key]);
		$studentIDAry = array_values($studentIDAry);
		
	}
	
	# students have no AP record
	for($i=0; $i<sizeof($studentIDAry); $i++) {
		$key = array_search($termID, $termOfCurrentYear);
		if($key!=0) {	# not the 1st term of year
			$sql = "SELECT ConductScore FROM DISCIPLINE_STUDENT_CONDUCT_BALANCE WHERE AcademicYearID='$AcademicYearID' AND YearTermID='".$termOfCurrentYear[0]."'";
			$temp = $ldiscipline->returnVector($sql);
			$newConductScore = $temp[0];
		} else {
			$newConductScore = $baseMark;	
		}
		
		$sql = "UPDATE DISCIPLINE_STUDENT_CONDUCT_BALANCE SET ConductScore='$newConductScore' WHERE StudentID=".$studentIDAry[$i]." AND AcademicYearID=$AcademicYearID AND YearTermID=$termID";
		$ldiscipline->db_db_query($sql);
		
		$sql = "SELECT ToScore FROM DISCIPLINE_CONDUCT_SCORE_CHANGE_LOG WHERE StudentID=".$studentIDAry[$i]." AND AcademicYearID=$AcademicYearID AND YearTermID=$termID ORDER BY RecordID DESC LIMIT 1";
		$result = $ldiscipline->returnVector($sql);
		$toScore = $result[0];

		$sql = "INSERT INTO DISCIPLINE_CONDUCT_SCORE_CHANGE_LOG 
					(StudentID, Year, Semester, FromScore, ToScore, ChangeUserID, RecordType, DateInput, DateModified, AcademicYearID, YearTermID)
				VALUES
					('".$studentIDAry[$i]."', '$yearName', '$termName', $toScore, '$newConductScore', '$UserID', '99', NOW(), NOW(), '$AcademicYearID', '$termID')	
		";
		$ldiscipline->db_db_query($sql);
		$revised++;
			
	}
	
	echo "$termName : <br>$count record(s) changed.<br>\n$revised record(s) revised.<br>\n";
}

echo "Process of \"Remove Duplication of Conduct Score Change\"<br>\n";


//intranet_closedb();
?>