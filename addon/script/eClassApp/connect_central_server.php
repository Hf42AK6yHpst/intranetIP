<?php
ini_set('display_errors',1);
error_reporting(E_ALL ^ E_NOTICE); 

$PATH_WRT_ROOT = "../../../";
include_once ($PATH_WRT_ROOT."addon/check.php");
include_once ($PATH_WRT_ROOT."includes/global.php");
include_once ($PATH_WRT_ROOT."includes/libdb.php");
include_once ($PATH_WRT_ROOT."includes/eClassApp/libeClassApp.php");

intranet_opendb();

$leClassApp = new libeClassApp();

$centralServerUrl = $leClassApp->getCurCentralServerUrl();
$checkConnectionApi = str_replace('index.php', 'checkConnect.php', $centralServerUrl);

session_write_close();
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $checkConnectionApi);
curl_setopt($ch, CURLOPT_HEADER, false);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
$responseString = curl_exec($ch);
curl_close($ch);

intranet_closedb();

if ($responseString == 'ACK') {
	$statusDisplay = '<span style="color:green;">SUCCESS</span>';
}
else {
	$statusDisplay = '<span style="color:red; font-weight:bold;">FAILED</span>';
}

echo "Target central server:<br />".$centralServerUrl."<br /><br />";
echo "Status:<br />".$statusDisplay."<br />";
?>