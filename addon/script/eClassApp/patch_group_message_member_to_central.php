<?php
ini_set('display_errors',1);
error_reporting(E_ALL ^ E_NOTICE); 

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp.php");
include_once($PATH_WRT_ROOT."includes/eClassApp/groupMessage/libeClassApp_groupMessage.php");

intranet_opendb();

$lgroupMessage = new libeClassApp_groupMessage();

$groupAry = $lgroupMessage->getGroupData();
$numOfGroup = count($groupAry);

for ($i=0; $i<$numOfGroup; $i++) {
	$_groupId = $groupAry[$i]['GroupID'];
	
	$successAry[$_groupId] = $lgroupMessage->saveGroupMemberInCloud($_groupId);
}

debug_pr($successAry);
?>