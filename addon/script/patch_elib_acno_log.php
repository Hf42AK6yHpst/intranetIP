<?php
/*
 * Aim: patch the acno log table  
 * 		
 * 
 */
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/liblibrarymgmt.php");

intranet_auth();
intranet_opendb();
#########################################################################################################


## Use Library
$libdb = new liblms();


## Init 
$result = array();
############## Apply Patch ##############
$isApply = true;
#########################################


## Get Data
if($_POST['Flag']!=1) {
	echo "<form method='post'><input value='run script' type='submit'><input type='hidden' name='Flag' value=1></form>";
}


# case 1: the book is available but the borrow log is not updated!

echo "<h2>1) Patch eLibrary ACNO LOG table. </h2>";

$sql = "select `Key` as ACNO_Key, Next_Number from LIBMS_ACNO_LOG order by `Key` ";
$tempArr = $libdb->returnArray($sql);
$acnoKeyArr = array();


$x = '<table border="1" cellpadding="4" cellspacing="0" width="90%">';
$x .= '<tr>';
$x .= '<th>ACNO Prefix</th><th>Next Number</th><td style="background:gray;width:2px;">&nbsp;</td>';
$x .= '<th>ACNO Prefix</th><th>Next Number</th><td style="background:gray;width:2px;">&nbsp;</td>';
$x .= '<th>ACNO Prefix</th><th>Next Number</th><td style="background:gray;width:2px;">&nbsp;</td>';
$x .= '<th>ACNO Prefix</th><th>Next Number</th>';
$x .= '</tr>';
if(count($tempArr) > 0){
	for ($i=0; $i<count($tempArr); $i++){
		$acnoKeyArr[$tempArr[$i]['ACNO_Key']] = $tempArr[$i]['Next_Number'];
		
		$x .= ($i % 4 == 0) ? '<tr>' : '';
		$x .= '<td width="80" align="center">'.$tempArr[$i]['ACNO_Key'].'</td>';
		$x .= '<td width="100" align="center">'.$tempArr[$i]['Next_Number'].'</td>';
		$x .= ($i % 4 == 3) ? '</tr>' : '<td style="background:gray;width:2px;">&nbsp;</td>';
	}
}
$x .= '</table><br>';
echo $x;

$sql = "select 
			ACNO_Prefix, max(ACNO_Num * 1) as max_ACNO_Num 
		from LIBMS_BOOK_UNIQUE 
		WHERE RecordStatus <> 'DELETE' 
		group by ACNO_Prefix 
		order by ACNO_Prefix ";
$itemArr = $libdb->returnArray($sql);

for ($i=0; $i<count($itemArr); $i++)
{
	
	## handle the first copy of each Book
	$ACNO_Prefix = $itemArr[$i]['ACNO_Prefix'];
	$max_ACNO_Num = $itemArr[$i]['max_ACNO_Num'];
	
	if($ACNO_Prefix != ''){
		$orig_len = strlen($max_ACNO_Num);
		$next_number = (int)$max_ACNO_Num + 1;
		if(strlen($next_number) < $orig_len){
			$next_number = str_pad($next_number, $orig_len, "0", STR_PAD_LEFT);
		}
		
		if(!isset($acnoKeyArr[$ACNO_Prefix])){
			$sql = "insert into LIBMS_ACNO_LOG (`Key`, Next_Number) values ('".$ACNO_Prefix."', '".$next_number."') ";
		} else {
			$sql = "update LIBMS_ACNO_LOG set Next_Number = '".$next_number."' where `Key` = '".$ACNO_Prefix."' ";
		}
		if($isApply){
			$result['prefix_'.$ACNO_Prefix] = $libdb->db_db_query($sql);
		}
		echo $sql.'<br>';
	}
}



echo '<style> 
html, body, table, select, input, textarea{ font-size:12px; }
</style>';

debug_r($result);

#########################################################################################################
intranet_closedb();
?>