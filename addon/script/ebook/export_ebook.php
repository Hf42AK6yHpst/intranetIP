<?php
if (
	! isset( $_POST['password'] ) &&
	$_POST['password'] != 'SQYVhnlLgyC2ftEKXShJbA9gDoRJefse'
) {
	?>
    <form method="POST">
        <input type="password" name="password"/>
        <input type="submit"/>
    </form>
	<?php
	exit;
}

$PATH_WRT_ROOT = "{$_SERVER['DOCUMENT_ROOT']}/";
include_once( $PATH_WRT_ROOT . "includes/global.php" );
include_once( $PATH_WRT_ROOT . "includes/libdb.php" );
include_once($PATH_WRT_ROOT.'includes/libfilesystem.php');

header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");

intranet_opendb();
$li = new libdb();
$fm = new libfilesystem();

$sql = "SELECT * FROM INTRANET_ELIB_BOOK WHERE BookID > 800 AND BookID < 10000000 AND Publish=1";
$rs  = $li->returnResultSet( $sql );


//// Book Cover START ////
$folderToDownload = $intranet_root."/file/temp/ebook_export/";
$fm->folder_remove_recursive($folderToDownload);
$fm->createFolder($folderToDownload);
foreach($rs as $book){
	$source = "{$intranet_root}/file/elibrary/content/{$book['BookID']}/image/cover.jpg";
	if(!file_exists($source)){
		continue;
	}
	$fm->file_copy($source, "{$folderToDownload}{$book['BookID']}.jpg");
}

//Pack the ebooks and sql files
$ZipFileName = "ebook_cover_photo.zip";
$fm->file_remove($folderToDownload.$ZipFileName);//remove old file
$fm->file_zip('.', $ZipFileName, $folderToDownload);
echo "<a href='".$intranet_rel_path."file/temp/ebook_export/".$ZipFileName."' target='_blank'>Download book cover zip</a>";
//// Book Cover END ////

?>
<style>
    .hide{
        display: none;
    }
</style>
<hr />
<a href="#" onclick="downloadBooks()">Download book list excel (Total: <?= count($rs)?>)</a>
<script src="https://unpkg.com/xlsx/dist/xlsx.full.min.js"></script>
<div>
    <label>
        <input type="checkbox" onclick="toggleHideBookList()"/>
        Show book list
    </label>
</div>
<table id="bookList" border="1" class="hide">
    <colgroup>
        <col style="width: 65px;">
        <col style="width: 200px;">
        <col style="width: 100px;">
        <col style="width: 100px;">
        <col style="width: 100px;">
        <col style="width: 150px;">
        <col style="width: 100px;">
        <col style="width: 150px;">
        <col style="width: 50px;">
        <col>
    </colgroup>
    <thead>
    <tr>
        <th>Book ID</th>
        <th>Title</th>
        <th>Author</th>
        <th>Editor</th>
        <th>Adapter</th>
        <th>Publisher</th>
        <th>Publish year</th>
        <th>ISBN</th>
        <th>Tags</th>
        <th>Description</th>
    </tr>
    </thead>
    <tbody>
	<?php foreach ( $rs as $book ): ?>
        <tr>
            <td><?= $book['BookID'] ?></td>
            <td><?= $book['Title'] ?></td>
            <td><?= $book['Author'] ?></td>
            <td></td>
            <td></td>
            <td><?= $book['Publisher'] ?></td>
            <td></td>
            <td><?= $book['ISBN'] ?></td>
            <td></td>
            <td><?= $book['Preface'] ?></td>
        </tr>
	<?php endforeach; ?>
    </tbody>
</table>
<script>
	function downloadBooks() {
      var workbook = XLSX.utils.book_new();
      var ws1 = XLSX.utils.table_to_sheet(document.getElementById('bookList'));
      XLSX.utils.book_append_sheet(workbook, ws1, "Sheet1");
      XLSX.writeFile(workbook, 'eBooks.xlsx');
    }

    function toggleHideBookList(){
      var element = document.getElementById("bookList");
      element.classList.toggle("hide");
    }
</script>
