<?php
/*
 * 	Purpose: update INTRANET_ELIB_BOOK.BookCategoryType (sync from LIBMS db) [case #F130026]
 * 
 *  2017-12-12 [Cameron] create this file
 */

$path = ($FromCentralUpdate) ? $intranet_root."/" : "../../";
$PATH_WRT_ROOT = $path;
include_once($path."includes/global.php");
include_once($path."includes/libdb.php");
include_once($path."includes/liblibrarymgmt.php");
include_once($path."includes/libelibrary.php");

if (!$FromCentralUpdate) {
	intranet_auth();
	intranet_opendb();
}

$libms = new liblms();
$libel = new elibrary();

if(($_POST['Flag']!=1) && !$FromCentralUpdate) {
	$isApply = false;
	echo("<form method='post'><input value='run script' type='submit'><input type='hidden' name='Flag' value=1></form>");
}
else {
	$isApply = true;
}


## 1. get BookID and UniqueID from LIBMS_BOOK_UNIQUE which RecordStatus='NORMAL' but LIBMS_WRITEOFF_LOG.Result='WRITEOFF'
	$sql = "SELECT 
						lb.BookID + 10000000 AS BookID,
						lbc.BookCategoryType  
			FROM 
						LIBMS_BOOK AS lb 
			LEFT JOIN 
						LIBMS_BOOK_CATEGORY AS lbc on lbc.BookCategoryCode=lb.BookCategoryCode 
							AND lbc.BookCategoryType IN (IFNULL((SELECT lbl2.BookCategoryType FROM LIBMS_BOOK_LANGUAGE lbl2 WHERE lbl2.BookLanguageCode = lb.Language),1))
			WHERE
						(lb.RecordStatus<>'DELETE' OR lb.RecordStatus is null) 
			ORDER BY 	
						lb.BookID "; 

$rs = $libms->returnResultSet($sql);
$bookCategoryTypeAry = BuildMultiKeyAssoc($rs, 'BookCategoryType', 'BookID', $SingleValue=1, $BuildNumericArray=1);
$result = array();

$perTime = 250;	// max number of BookID to update per time 

if (count($bookCategoryTypeAry)) {
	if ($isApply) {
		$libel->Start_Trans();
	}
	foreach((array)$bookCategoryTypeAry as $cat=>$bookIDAry) {
//		debug_pr($cat);
//		debug_pr($bookIDAry);
		if (count($bookIDAry)) {
			$chunkBookID = array_chunk($bookIDAry,$perTime);
			foreach((array)$chunkBookID as $bookID) {
				$bookIDs = implode(",",$bookID);
				if (!empty($bookIDs)) {
					$sql = "UPDATE INTRANET_ELIB_BOOK SET BookCategoryType='".$cat."' WHERE BookID IN (".$bookIDs.")";
//debug_pr($sql);					
					if ($isApply) {
						$result[] = $libel->db_db_query($sql);
					}
				}
				
			}
		}
	}
	
	if ($isApply) {
		if (!in_array(false,$result)) {
			$libel->Commit_Trans();
			echo "<span style=\"color:green; font-weight:bold\">success</span><br>";
		}
		else {
			$libel->RollBack_Trans();
			echo "<span style=\"color:red; font-weight:bold\">fail</span><br>";
		}
	}			
}
else {
	print 'No record found<br>';
}


if (!$FromCentralUpdate) {
	intranet_closedb();
}
?>