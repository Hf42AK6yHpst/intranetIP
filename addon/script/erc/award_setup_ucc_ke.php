<?php
ini_set('display_errors',1);
error_reporting(E_ALL ^ E_NOTICE); 

// editing by 
@SET_TIME_LIMIT(0);
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_award.php");

intranet_opendb();

$lgs = new libgeneralsettings();
$lreportcard = new libreportcardcustom();
$lreportcard_award = new libreportcard_award();

$ModuleName = "eRCDataPatch";
$SettingName = "AwardItemSetup_UCCKE";
$GSary = $lgs->Get_General_Setting($ModuleName);

$LogOutput = '';
if ($GSary[$SettingName] != 1 && $plugin['ReportCard2008'] && $ReportCardCustomSchoolName == "ucc_ke") {
	$LogOutput .= "eReportCard - Award Setup for UCCKE [Start] ".date("Y-m-d H:i:s")."<br><br>\r\n\r\n";
	
	$ReportID = 0;	// default report settings
	$AwardID = 0;
	$DisplayOrder = 0;
	
	
	### First Class Honour
	++$AwardID;
	$DataArr = array('AwardID'=>$AwardID, 'AwardCode'=>'first_class_honour', 'AwardNameEn'=>'First Class Honour', 'AwardNameCh'=>'學行優異生', 'ApplicableReportType'=>'T', 'AwardType'=>'OVERALL', 'SchoolCode'=>$ReportCardCustomSchoolName, 'DisplayOrder'=>++$DisplayOrder);
	$Success[$AwardID]['InsertAward'] = $lreportcard_award->Insert_Award_Record($DataArr);
	$DataArr = array();
	$DataArr[] = array('YearID'=>1, 'Quota'=>0);
	$DataArr[] = array('YearID'=>2, 'Quota'=>0);
	$DataArr[] = array('YearID'=>3, 'Quota'=>0);
	$DataArr[] = array('YearID'=>4, 'Quota'=>0);
	$DataArr[] = array('YearID'=>6, 'Quota'=>0);
	$DataArr[] = array('YearID'=>7, 'Quota'=>0);
	$Success[$AwardID]['UpdateApplicableFormInfo'] = $lreportcard_award->Update_Award_Applicable_Form_Info($AwardID, $ReportID, $DataArr);
	
	### Second Class Honour (Division 1)
	++$AwardID;
	$DataArr = array('AwardID'=>$AwardID, 'AwardCode'=>'second_class_honour_1', 'AwardNameEn'=>' Second Class Honour (Division 1)', 'AwardNameCh'=>'學行良好生（甲等）', 'ApplicableReportType'=>'T', 'AwardType'=>'OVERALL', 'SchoolCode'=>$ReportCardCustomSchoolName, 'DisplayOrder'=>++$DisplayOrder);
	$Success[$AwardID]['InsertAward'] = $lreportcard_award->Insert_Award_Record($DataArr);
	$DataArr = array();
	$DataArr[] = array('YearID'=>1, 'Quota'=>0);
	$DataArr[] = array('YearID'=>2, 'Quota'=>0);
	$DataArr[] = array('YearID'=>3, 'Quota'=>0);
	$DataArr[] = array('YearID'=>4, 'Quota'=>0);
	$DataArr[] = array('YearID'=>6, 'Quota'=>0);
	$DataArr[] = array('YearID'=>7, 'Quota'=>0);
	$Success[$AwardID]['UpdateApplicableFormInfo'] = $lreportcard_award->Update_Award_Applicable_Form_Info($AwardID, $ReportID, $DataArr);
	
	### Second Class Honour (Division 2)
	++$AwardID;
	$DataArr = array('AwardID'=>$AwardID, 'AwardCode'=>'second_class_honour_2', 'AwardNameEn'=>' Second Class Honour (Division 2)', 'AwardNameCh'=>'學行良好生（乙等）', 'ApplicableReportType'=>'T', 'AwardType'=>'OVERALL', 'SchoolCode'=>$ReportCardCustomSchoolName, 'DisplayOrder'=>++$DisplayOrder);
	$Success[$AwardID]['InsertAward'] = $lreportcard_award->Insert_Award_Record($DataArr);
	$DataArr = array();
	$DataArr[] = array('YearID'=>1, 'Quota'=>0);
	$DataArr[] = array('YearID'=>2, 'Quota'=>0);
	$DataArr[] = array('YearID'=>3, 'Quota'=>0);
	$DataArr[] = array('YearID'=>4, 'Quota'=>0);
	$DataArr[] = array('YearID'=>6, 'Quota'=>0);
	$DataArr[] = array('YearID'=>7, 'Quota'=>0);
	$Success[$AwardID]['UpdateApplicableFormInfo'] = $lreportcard_award->Update_Award_Applicable_Form_Info($AwardID, $ReportID, $DataArr);
	
	### Form Subject Prize
	++$AwardID;
	$DataArr = array('AwardID'=>$AwardID, 'AwardCode'=>'form_subject_prize_1', 'AwardNameEn'=>'Form Subject Prize (First Position) in', 'AwardNameCh'=>'Form Subject Prize (First Position) in', 'ApplicableReportType'=>'T', 'AwardType'=>'SUBJECT', 'SchoolCode'=>$ReportCardCustomSchoolName, 'DisplayOrder'=>++$DisplayOrder);
	$Success[$AwardID]['InsertAward'] = $lreportcard_award->Insert_Award_Record($DataArr);
	$DataArr = array();
	$DataArr[] = array('YearID'=>1, 'Quota'=>0);
	$DataArr[] = array('YearID'=>2, 'Quota'=>0);
	$DataArr[] = array('YearID'=>3, 'Quota'=>0);
	$DataArr[] = array('YearID'=>4, 'Quota'=>0);
	$DataArr[] = array('YearID'=>6, 'Quota'=>0);
	$DataArr[] = array('YearID'=>7, 'Quota'=>0);
	$Success[$AwardID]['UpdateApplicableFormInfo'] = $lreportcard_award->Update_Award_Applicable_Form_Info($AwardID, $ReportID, $DataArr);
	$DataArr = array();
	$CriteriaOrder = 0;
	$DataArr[] = array('CriteriaType'=>'RANK', 'CriteriaOrder'=>++$CriteriaOrder, 'RankField'=>'OrderMeritForm', 'FromRank'=>1, 'ToRank'=>1);
	$Success[$AwardID]['UpdateCriteria'] = $lreportcard_award->Update_Award_Criteria($AwardID, $ReportID, $DataArr);
	
	### Form Subject Prize
	++$AwardID;
	$DataArr = array('AwardID'=>$AwardID, 'AwardCode'=>'form_subject_prize_2', 'AwardNameEn'=>'Form Subject Prize (Second Position) in', 'AwardNameCh'=>'Form Subject Prize (Second Position) in', 'ApplicableReportType'=>'T', 'AwardType'=>'SUBJECT', 'SchoolCode'=>$ReportCardCustomSchoolName, 'DisplayOrder'=>++$DisplayOrder);
	$Success[$AwardID]['InsertAward'] = $lreportcard_award->Insert_Award_Record($DataArr);
	$DataArr = array();
	$DataArr[] = array('YearID'=>1, 'Quota'=>0);
	$DataArr[] = array('YearID'=>2, 'Quota'=>0);
	$DataArr[] = array('YearID'=>3, 'Quota'=>0);
	$DataArr[] = array('YearID'=>4, 'Quota'=>0);
	$DataArr[] = array('YearID'=>6, 'Quota'=>0);
	$DataArr[] = array('YearID'=>7, 'Quota'=>0);
	$Success[$AwardID]['UpdateApplicableFormInfo'] = $lreportcard_award->Update_Award_Applicable_Form_Info($AwardID, $ReportID, $DataArr);
	$DataArr = array();
	$CriteriaOrder = 0;
	$DataArr[] = array('CriteriaType'=>'RANK', 'CriteriaOrder'=>++$CriteriaOrder, 'RankField'=>'OrderMeritForm', 'FromRank'=>2, 'ToRank'=>2);
	$Success[$AwardID]['UpdateCriteria'] = $lreportcard_award->Update_Award_Criteria($AwardID, $ReportID, $DataArr);
	
	### Form Subject Prize
	++$AwardID;
	$DataArr = array('AwardID'=>$AwardID, 'AwardCode'=>'form_subject_prize_3', 'AwardNameEn'=>'Form Subject Prize (Third Position) in', 'AwardNameCh'=>'Form Subject Prize (Third Position) in', 'ApplicableReportType'=>'T', 'AwardType'=>'SUBJECT', 'SchoolCode'=>$ReportCardCustomSchoolName, 'DisplayOrder'=>++$DisplayOrder);
	$Success[$AwardID]['InsertAward'] = $lreportcard_award->Insert_Award_Record($DataArr);
	$DataArr = array();
	$DataArr[] = array('YearID'=>1, 'Quota'=>0);
	$DataArr[] = array('YearID'=>2, 'Quota'=>0);
	$DataArr[] = array('YearID'=>3, 'Quota'=>0);
	$DataArr[] = array('YearID'=>4, 'Quota'=>0);
	$DataArr[] = array('YearID'=>6, 'Quota'=>0);
	$DataArr[] = array('YearID'=>7, 'Quota'=>0);
	$Success[$AwardID]['UpdateApplicableFormInfo'] = $lreportcard_award->Update_Award_Applicable_Form_Info($AwardID, $ReportID, $DataArr);
	$DataArr = array();
	$CriteriaOrder = 0;
	$DataArr[] = array('CriteriaType'=>'RANK', 'CriteriaOrder'=>++$CriteriaOrder, 'RankField'=>'OrderMeritForm', 'FromRank'=>3, 'ToRank'=>3);
	$Success[$AwardID]['UpdateCriteria'] = $lreportcard_award->Update_Award_Criteria($AwardID, $ReportID, $DataArr);
	
	### Class Subject Prize
	++$AwardID;
	$DataArr = array('AwardID'=>$AwardID, 'AwardCode'=>'class_subject_prize', 'AwardNameEn'=>'Class Subject Prize in', 'AwardNameCh'=>'Class Subject Prize in', 'ApplicableReportType'=>'T', 'AwardType'=>'SUBJECT', 'SchoolCode'=>$ReportCardCustomSchoolName, 'DisplayOrder'=>++$DisplayOrder);
	$Success[$AwardID]['InsertAward'] = $lreportcard_award->Insert_Award_Record($DataArr);
	$DataArr = array();
	$DataArr[] = array('YearID'=>1, 'Quota'=>0);
	$DataArr[] = array('YearID'=>2, 'Quota'=>0);
	$DataArr[] = array('YearID'=>3, 'Quota'=>0);
	$DataArr[] = array('YearID'=>4, 'Quota'=>0);
	$DataArr[] = array('YearID'=>6, 'Quota'=>0);
	$DataArr[] = array('YearID'=>7, 'Quota'=>0);
	$Success[$AwardID]['UpdateApplicableFormInfo'] = $lreportcard_award->Update_Award_Applicable_Form_Info($AwardID, $ReportID, $DataArr);
	$DataArr = array();
	$CriteriaOrder = 0;
	$DataArr[] = array('CriteriaType'=>'RANK', 'CriteriaOrder'=>++$CriteriaOrder, 'RankField'=>'OrderMeritClass', 'FromRank'=>1, 'ToRank'=>1);
	$Success[$AwardID]['UpdateCriteria'] = $lreportcard_award->Update_Award_Criteria($AwardID, $ReportID, $DataArr);
	
	### First Position in class
	++$AwardID;
	$DataArr = array('AwardID'=>$AwardID, 'AwardCode'=>'position_in_class_1', 'AwardNameEn'=>'First Position in class', 'AwardNameCh'=>'First Position in class', 'ApplicableReportType'=>'T', 'AwardType'=>'OVERALL', 'SchoolCode'=>$ReportCardCustomSchoolName, 'DisplayOrder'=>++$DisplayOrder);
	$Success[$AwardID]['InsertAward'] = $lreportcard_award->Insert_Award_Record($DataArr);
	$DataArr = array();
	$DataArr[] = array('YearID'=>1, 'Quota'=>0);
	$DataArr[] = array('YearID'=>2, 'Quota'=>0);
	$DataArr[] = array('YearID'=>3, 'Quota'=>0);
	$DataArr[] = array('YearID'=>4, 'Quota'=>0);
	$DataArr[] = array('YearID'=>6, 'Quota'=>0);
	$DataArr[] = array('YearID'=>7, 'Quota'=>0);
	$Success[$AwardID]['UpdateApplicableFormInfo'] = $lreportcard_award->Update_Award_Applicable_Form_Info($AwardID, $ReportID, $DataArr);
	$DataArr = array();
	$CriteriaOrder = 0;
	$DataArr[] = array('CriteriaType'=>'RANK', 'CriteriaOrder'=>++$CriteriaOrder, 'RankField'=>'OrderMeritClass', 'FromRank'=>1, 'ToRank'=>1);
	$Success[$AwardID]['UpdateCriteria'] = $lreportcard_award->Update_Award_Criteria($AwardID, $ReportID, $DataArr);
	
	### Second Position in class
	++$AwardID;
	$DataArr = array('AwardID'=>$AwardID, 'AwardCode'=>'position_in_class_2', 'AwardNameEn'=>'Second Position in class', 'AwardNameCh'=>'Second Position in class', 'ApplicableReportType'=>'T', 'AwardType'=>'OVERALL', 'SchoolCode'=>$ReportCardCustomSchoolName, 'DisplayOrder'=>++$DisplayOrder);
	$Success[$AwardID]['InsertAward'] = $lreportcard_award->Insert_Award_Record($DataArr);
	$DataArr = array();
	$DataArr[] = array('YearID'=>1, 'Quota'=>0);
	$DataArr[] = array('YearID'=>2, 'Quota'=>0);
	$DataArr[] = array('YearID'=>3, 'Quota'=>0);
	$DataArr[] = array('YearID'=>4, 'Quota'=>0);
	$DataArr[] = array('YearID'=>6, 'Quota'=>0);
	$DataArr[] = array('YearID'=>7, 'Quota'=>0);
	$Success[$AwardID]['UpdateApplicableFormInfo'] = $lreportcard_award->Update_Award_Applicable_Form_Info($AwardID, $ReportID, $DataArr);
	$DataArr = array();
	$CriteriaOrder = 0;
	$DataArr[] = array('CriteriaType'=>'RANK', 'CriteriaOrder'=>++$CriteriaOrder, 'RankField'=>'OrderMeritClass', 'FromRank'=>2, 'ToRank'=>2);
	$Success[$AwardID]['UpdateCriteria'] = $lreportcard_award->Update_Award_Criteria($AwardID, $ReportID, $DataArr);
	
	### Third Position in class
	++$AwardID;
	$DataArr = array('AwardID'=>$AwardID, 'AwardCode'=>'position_in_class_3', 'AwardNameEn'=>'Third Position in class', 'AwardNameCh'=>'Third Position in class', 'ApplicableReportType'=>'T', 'AwardType'=>'OVERALL', 'SchoolCode'=>$ReportCardCustomSchoolName, 'DisplayOrder'=>++$DisplayOrder);
	$Success[$AwardID]['InsertAward'] = $lreportcard_award->Insert_Award_Record($DataArr);
	$DataArr = array();
	$DataArr[] = array('YearID'=>1, 'Quota'=>0);
	$DataArr[] = array('YearID'=>2, 'Quota'=>0);
	$DataArr[] = array('YearID'=>3, 'Quota'=>0);
	$DataArr[] = array('YearID'=>4, 'Quota'=>0);
	$DataArr[] = array('YearID'=>6, 'Quota'=>0);
	$DataArr[] = array('YearID'=>7, 'Quota'=>0);
	$Success[$AwardID]['UpdateApplicableFormInfo'] = $lreportcard_award->Update_Award_Applicable_Form_Info($AwardID, $ReportID, $DataArr);
	$DataArr = array();
	$CriteriaOrder = 0;
	$DataArr[] = array('CriteriaType'=>'RANK', 'CriteriaOrder'=>++$CriteriaOrder, 'RankField'=>'OrderMeritClass', 'FromRank'=>3, 'ToRank'=>3);
	$Success[$AwardID]['UpdateCriteria'] = $lreportcard_award->Update_Award_Criteria($AwardID, $ReportID, $DataArr);
	
	
	debug_pr($Success);
	
	
	# update General Settings - markd the script is executed
	$sql = "insert ignore into GENERAL_SETTING 
						(Module, SettingName, SettingValue, DateInput) 
					values 
						('$ModuleName', '$SettingName', 1, now())";
	$SuccessArr['UpdateGeneralSettings'] = $lgs->db_db_query($sql);
	
	$LogOutput .= "eReportCard - Award Setup for UCCKE [End] ".date("Y-m-d H:i:s")."<br>";
	
	# show log
	echo $LogOutput;
	
	# save log
	$log_folder = $PATH_WRT_ROOT."file/reportcard2008/script_log";
	if(!file_exists($log_folder))
		mkdir($log_folder);
	$log_file = $log_folder."/award_setup_uccke_". date("YmdHis") .".log";
	$lf = new libfilesystem();
	$lf->file_write(strip_tags($LogOutput), $log_file);
}
?>