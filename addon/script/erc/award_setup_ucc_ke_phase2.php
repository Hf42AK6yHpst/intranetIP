<?php
ini_set('display_errors',1);
error_reporting(E_ALL ^ E_NOTICE); 

// editing by 
@SET_TIME_LIMIT(0);
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_award.php");

intranet_opendb();

$lgs = new libgeneralsettings();
$lreportcard = new libreportcardcustom();
$lreportcard_award = new libreportcard_award();

$ModuleName = "eRCDataPatch";
$SettingName = "AwardItemSetup_UCCKE_phase2";
$GSary = $lgs->Get_General_Setting($ModuleName);


$LogOutput = '';
if ($GSary[$SettingName] != 1 && $plugin['ReportCard2008'] && $ReportCardCustomSchoolName == "ucc_ke") {
	$LogOutput .= "eReportCard - Award Setup for UCCKE [Start] ".date("Y-m-d H:i:s")."<br><br>\r\n\r\n";
	
	$ReportID = 0;	// default report settings
	$AwardID = 10;
	$DisplayOrder = 10;
	
	
	### Conduct Award
	++$AwardID;
	$DataArr = array('AwardID'=>$AwardID, 'AwardCode'=>'conduct_award', 'AwardNameEn'=>'Conduct Award', 'AwardNameCh'=>'Conduct Award', 'ApplicableReportType'=>'T', 'AwardType'=>'OVERALL', 'SchoolCode'=>$ReportCardCustomSchoolName, 'DisplayOrder'=>++$DisplayOrder);
	$Success[$AwardID]['InsertAward'] = $lreportcard_award->Insert_Award_Record($DataArr);
	$DataArr = array();
	$DataArr[] = array('YearID'=>1, 'Quota'=>0);
	$DataArr[] = array('YearID'=>2, 'Quota'=>0);
	$DataArr[] = array('YearID'=>3, 'Quota'=>0);
	$DataArr[] = array('YearID'=>4, 'Quota'=>0);
	$DataArr[] = array('YearID'=>6, 'Quota'=>0);
	$DataArr[] = array('YearID'=>7, 'Quota'=>0);
	$Success[$AwardID]['UpdateApplicableFormInfo'] = $lreportcard_award->Update_Award_Applicable_Form_Info($AwardID, $ReportID, $DataArr);
	
	
	### Conduct Progress Award
	++$AwardID;
	$DataArr = array('AwardID'=>$AwardID, 'AwardCode'=>'conduct_progress_award', 'AwardNameEn'=>'Conduct Progress Award', 'AwardNameCh'=>'Conduct Progress Award', 'ApplicableReportType'=>'T', 'AwardType'=>'OVERALL', 'SchoolCode'=>$ReportCardCustomSchoolName, 'DisplayOrder'=>++$DisplayOrder);
	$Success[$AwardID]['InsertAward'] = $lreportcard_award->Insert_Award_Record($DataArr);
	$DataArr = array();
	$DataArr[] = array('YearID'=>1, 'Quota'=>0);
	$DataArr[] = array('YearID'=>2, 'Quota'=>0);
	$DataArr[] = array('YearID'=>3, 'Quota'=>0);
	$DataArr[] = array('YearID'=>4, 'Quota'=>0);
	$DataArr[] = array('YearID'=>6, 'Quota'=>0);
	$DataArr[] = array('YearID'=>7, 'Quota'=>0);
	$Success[$AwardID]['UpdateApplicableFormInfo'] = $lreportcard_award->Update_Award_Applicable_Form_Info($AwardID, $ReportID, $DataArr);
	
	
	### Student Showing Academic Progress
	++$AwardID;
	$DataArr = array('AwardID'=>$AwardID, 'AwardCode'=>'academic_progress', 'AwardNameEn'=>'Student Showing Academic Progress', 'AwardNameCh'=>'Student Showing Academic Progress', 'ApplicableReportType'=>'T', 'AwardType'=>'OVERALL', 'SchoolCode'=>$ReportCardCustomSchoolName, 'DisplayOrder'=>++$DisplayOrder);
	$Success[$AwardID]['InsertAward'] = $lreportcard_award->Insert_Award_Record($DataArr);
	$DataArr = array();
	$DataArr[] = array('YearID'=>1, 'Quota'=>0);
	$DataArr[] = array('YearID'=>2, 'Quota'=>0);
	$DataArr[] = array('YearID'=>3, 'Quota'=>0);
	$DataArr[] = array('YearID'=>4, 'Quota'=>0);
	$DataArr[] = array('YearID'=>6, 'Quota'=>0);
	$DataArr[] = array('YearID'=>7, 'Quota'=>0);
	$Success[$AwardID]['UpdateApplicableFormInfo'] = $lreportcard_award->Update_Award_Applicable_Form_Info($AwardID, $ReportID, $DataArr);
	
	
	### PTA Progress Award
	++$AwardID;
	$DataArr = array('AwardID'=>$AwardID, 'AwardCode'=>'pta_progress', 'AwardNameEn'=>'PTA Progress Award', 'AwardNameCh'=>'PTA Progress Award', 'ApplicableReportType'=>'T', 'AwardType'=>'OVERALL', 'SchoolCode'=>$ReportCardCustomSchoolName, 'DisplayOrder'=>++$DisplayOrder);
	$Success[$AwardID]['InsertAward'] = $lreportcard_award->Insert_Award_Record($DataArr);
	$DataArr = array();
	$DataArr[] = array('YearID'=>1, 'Quota'=>0);
	$DataArr[] = array('YearID'=>2, 'Quota'=>0);
	$DataArr[] = array('YearID'=>3, 'Quota'=>0);
	$DataArr[] = array('YearID'=>4, 'Quota'=>0);
	$DataArr[] = array('YearID'=>6, 'Quota'=>0);
	$DataArr[] = array('YearID'=>7, 'Quota'=>0);
	$Success[$AwardID]['UpdateApplicableFormInfo'] = $lreportcard_award->Update_Award_Applicable_Form_Info($AwardID, $ReportID, $DataArr);
	
	
	### Top Library Book Borrower Award
	++$AwardID;
	$DataArr = array('AwardID'=>$AwardID, 'AwardCode'=>'top_book_borrower', 'AwardNameEn'=>'Top Library Book Borrower Award', 'AwardNameCh'=>'Top Library Book Borrower Award', 'ApplicableReportType'=>'T', 'AwardType'=>'OVERALL', 'SchoolCode'=>$ReportCardCustomSchoolName, 'DisplayOrder'=>++$DisplayOrder);
	$Success[$AwardID]['InsertAward'] = $lreportcard_award->Insert_Award_Record($DataArr);
	$DataArr = array();
	$DataArr[] = array('YearID'=>1, 'Quota'=>0);
	$DataArr[] = array('YearID'=>2, 'Quota'=>0);
	$DataArr[] = array('YearID'=>3, 'Quota'=>0);
	$DataArr[] = array('YearID'=>4, 'Quota'=>0);
	$DataArr[] = array('YearID'=>6, 'Quota'=>0);
	$DataArr[] = array('YearID'=>7, 'Quota'=>0);
	$Success[$AwardID]['UpdateApplicableFormInfo'] = $lreportcard_award->Update_Award_Applicable_Form_Info($AwardID, $ReportID, $DataArr);
	
	
	### Reading Scheme Award (Silver Prize)
	++$AwardID;
	$DataArr = array('AwardID'=>$AwardID, 'AwardCode'=>'reading_scheme_silver', 'AwardNameEn'=>'Reading Scheme Award (Silver Prize)', 'AwardNameCh'=>'Reading Scheme Award (Silver Prize)', 'ApplicableReportType'=>'T', 'AwardType'=>'OVERALL', 'SchoolCode'=>$ReportCardCustomSchoolName, 'DisplayOrder'=>++$DisplayOrder);
	$Success[$AwardID]['InsertAward'] = $lreportcard_award->Insert_Award_Record($DataArr);
	$DataArr = array();
	$DataArr[] = array('YearID'=>1, 'Quota'=>0);
	$DataArr[] = array('YearID'=>2, 'Quota'=>0);
	$DataArr[] = array('YearID'=>3, 'Quota'=>0);
	$DataArr[] = array('YearID'=>4, 'Quota'=>0);
	$DataArr[] = array('YearID'=>6, 'Quota'=>0);
	$DataArr[] = array('YearID'=>7, 'Quota'=>0);
	$Success[$AwardID]['UpdateApplicableFormInfo'] = $lreportcard_award->Update_Award_Applicable_Form_Info($AwardID, $ReportID, $DataArr);
	
	
	### Disciple Award
	++$AwardID;
	$DataArr = array('AwardID'=>$AwardID, 'AwardCode'=>'disciple_award', 'AwardNameEn'=>'Disciple Award', 'AwardNameCh'=>'Disciple Award', 'ApplicableReportType'=>'T', 'AwardType'=>'OVERALL', 'SchoolCode'=>$ReportCardCustomSchoolName, 'DisplayOrder'=>++$DisplayOrder);
	$Success[$AwardID]['InsertAward'] = $lreportcard_award->Insert_Award_Record($DataArr);
	$DataArr = array();
	$DataArr[] = array('YearID'=>1, 'Quota'=>0);
	$DataArr[] = array('YearID'=>2, 'Quota'=>0);
	$DataArr[] = array('YearID'=>3, 'Quota'=>0);
	$DataArr[] = array('YearID'=>4, 'Quota'=>0);
	$DataArr[] = array('YearID'=>6, 'Quota'=>0);
	$DataArr[] = array('YearID'=>7, 'Quota'=>0);
	$Success[$AwardID]['UpdateApplicableFormInfo'] = $lreportcard_award->Update_Award_Applicable_Form_Info($AwardID, $ReportID, $DataArr);
	
	
	### Service Award
	++$AwardID;
	$DataArr = array('AwardID'=>$AwardID, 'AwardCode'=>'service_award', 'AwardNameEn'=>'Service Award', 'AwardNameCh'=>'Service Award', 'ApplicableReportType'=>'T', 'AwardType'=>'OVERALL', 'SchoolCode'=>$ReportCardCustomSchoolName, 'DisplayOrder'=>++$DisplayOrder);
	$Success[$AwardID]['InsertAward'] = $lreportcard_award->Insert_Award_Record($DataArr);
	$DataArr = array();
	$DataArr[] = array('YearID'=>1, 'Quota'=>0);
	$DataArr[] = array('YearID'=>2, 'Quota'=>0);
	$DataArr[] = array('YearID'=>3, 'Quota'=>0);
	$DataArr[] = array('YearID'=>4, 'Quota'=>0);
	$DataArr[] = array('YearID'=>6, 'Quota'=>0);
	$DataArr[] = array('YearID'=>7, 'Quota'=>0);
	$Success[$AwardID]['UpdateApplicableFormInfo'] = $lreportcard_award->Update_Award_Applicable_Form_Info($AwardID, $ReportID, $DataArr);
	
	
	### Highest Distinction in Academic Achievement
	++$AwardID;
	$DataArr = array('AwardID'=>$AwardID, 'AwardCode'=>'highest_distinction_in_academic', 'AwardNameEn'=>'Highest Distinction in Academic Achievement', 'AwardNameCh'=>'Highest Distinction in Academic Achievement', 'ApplicableReportType'=>'T', 'AwardType'=>'OVERALL', 'SchoolCode'=>$ReportCardCustomSchoolName, 'DisplayOrder'=>++$DisplayOrder);
	$Success[$AwardID]['InsertAward'] = $lreportcard_award->Insert_Award_Record($DataArr);
	$DataArr = array();
	$DataArr[] = array('YearID'=>1, 'Quota'=>0);
	$DataArr[] = array('YearID'=>2, 'Quota'=>0);
	$DataArr[] = array('YearID'=>3, 'Quota'=>0);
	$DataArr[] = array('YearID'=>4, 'Quota'=>0);
	$DataArr[] = array('YearID'=>6, 'Quota'=>0);
	$DataArr[] = array('YearID'=>7, 'Quota'=>0);
	$Success[$AwardID]['UpdateApplicableFormInfo'] = $lreportcard_award->Update_Award_Applicable_Form_Info($AwardID, $ReportID, $DataArr);
	
	
	### Student of the Form
	++$AwardID;
	$DataArr = array('AwardID'=>$AwardID, 'AwardCode'=>'student_of_the_form', 'AwardNameEn'=>'Student of the Form', 'AwardNameCh'=>'Student of the Form', 'ApplicableReportType'=>'T', 'AwardType'=>'OVERALL', 'SchoolCode'=>$ReportCardCustomSchoolName, 'DisplayOrder'=>++$DisplayOrder);
	$Success[$AwardID]['InsertAward'] = $lreportcard_award->Insert_Award_Record($DataArr);
	$DataArr = array();
	$DataArr[] = array('YearID'=>1, 'Quota'=>0);
	$DataArr[] = array('YearID'=>2, 'Quota'=>0);
	$DataArr[] = array('YearID'=>3, 'Quota'=>0);
	$DataArr[] = array('YearID'=>4, 'Quota'=>0);
	$DataArr[] = array('YearID'=>6, 'Quota'=>0);
	$DataArr[] = array('YearID'=>7, 'Quota'=>0);
	$Success[$AwardID]['UpdateApplicableFormInfo'] = $lreportcard_award->Update_Award_Applicable_Form_Info($AwardID, $ReportID, $DataArr);
	
	
	### Student of Sports
	++$AwardID;
	$DataArr = array('AwardID'=>$AwardID, 'AwardCode'=>'student_of_sports', 'AwardNameEn'=>'Student of Sports', 'AwardNameCh'=>'Student of Sports', 'ApplicableReportType'=>'T', 'AwardType'=>'OVERALL', 'SchoolCode'=>$ReportCardCustomSchoolName, 'DisplayOrder'=>++$DisplayOrder);
	$Success[$AwardID]['InsertAward'] = $lreportcard_award->Insert_Award_Record($DataArr);
	$DataArr = array();
	$DataArr[] = array('YearID'=>1, 'Quota'=>0);
	$DataArr[] = array('YearID'=>2, 'Quota'=>0);
	$DataArr[] = array('YearID'=>3, 'Quota'=>0);
	$DataArr[] = array('YearID'=>4, 'Quota'=>0);
	$DataArr[] = array('YearID'=>6, 'Quota'=>0);
	$DataArr[] = array('YearID'=>7, 'Quota'=>0);
	$Success[$AwardID]['UpdateApplicableFormInfo'] = $lreportcard_award->Update_Award_Applicable_Form_Info($AwardID, $ReportID, $DataArr);
	
	
	### Student of Arts
	++$AwardID;
	$DataArr = array('AwardID'=>$AwardID, 'AwardCode'=>'student_of_arts', 'AwardNameEn'=>'Student of Arts', 'AwardNameCh'=>'Student of Arts', 'ApplicableReportType'=>'T', 'AwardType'=>'OVERALL', 'SchoolCode'=>$ReportCardCustomSchoolName, 'DisplayOrder'=>++$DisplayOrder);
	$Success[$AwardID]['InsertAward'] = $lreportcard_award->Insert_Award_Record($DataArr);
	$DataArr = array();
	$DataArr[] = array('YearID'=>1, 'Quota'=>0);
	$DataArr[] = array('YearID'=>2, 'Quota'=>0);
	$DataArr[] = array('YearID'=>3, 'Quota'=>0);
	$DataArr[] = array('YearID'=>4, 'Quota'=>0);
	$DataArr[] = array('YearID'=>6, 'Quota'=>0);
	$DataArr[] = array('YearID'=>7, 'Quota'=>0);
	$Success[$AwardID]['UpdateApplicableFormInfo'] = $lreportcard_award->Update_Award_Applicable_Form_Info($AwardID, $ReportID, $DataArr);
	
		
	debug_pr($Success);
	
	
	# update General Settings - markd the script is executed
	$sql = "insert ignore into GENERAL_SETTING 
						(Module, SettingName, SettingValue, DateInput) 
					values 
						('$ModuleName', '$SettingName', 1, now())";
	$SuccessArr['UpdateGeneralSettings'] = $lgs->db_db_query($sql);
	
	$LogOutput .= "eReportCard - Award Setup for UCCKE [End] ".date("Y-m-d H:i:s")."<br>";
	
	# show log
	echo $LogOutput;
	
	# save log
	$log_folder = $PATH_WRT_ROOT."file/reportcard2008/script_log";
	if(!file_exists($log_folder))
		mkdir($log_folder);
	$log_file = $log_folder."/award_setup_uccke_". date("YmdHis") .".log";
	$lf = new libfilesystem();
	$lf->file_write(strip_tags($LogOutput), $log_file);
}
?>