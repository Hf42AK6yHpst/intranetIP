<?php
ini_set('display_errors',1);
error_reporting(E_ALL ^ E_NOTICE); 

// editing by 
@SET_TIME_LIMIT(0);
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_award.php");

intranet_opendb();

$lgs = new libgeneralsettings();
$lreportcard = new libreportcardcustom();
$lreportcard_award = new libreportcard_award();

//$ModuleName = "eRCDataPatch";
//$SettingName = "AwardItemSetup_ShatinMethodist";
//$GSary = $lgs->Get_General_Setting($ModuleName);

$LogOutput = '';
if ($GSary[$SettingName] != 1 && $plugin['ReportCard2008'] && $ReportCardCustomSchoolName == "sha_tin_methodist") {
	$LogOutput .= "eReportCard - Award Setup for Shatin Methodist [Start] ".date("Y-m-d H:i:s")."<br><br>\r\n\r\n";
	
	$ReportID = 0;	// default report settings
	$AwardID = 0;
	$DisplayOrder = 0;
	
	### First in Form
	++$AwardID;
	$DataArr = array('AwardID'=>$AwardID, 'AwardCode'=>'first_in_form', 'AwardNameEn'=>'First in Form', 'AwardNameCh'=>'First in Form', 'ApplicableReportType'=>'T', 'AwardType'=>'OVERALL', 'SchoolCode'=>$ReportCardCustomSchoolName, 'DisplayOrder'=>++$DisplayOrder);
	$Success[$AwardID]['InsertAward'] = $lreportcard_award->Insert_Award_Record($DataArr);
	$DataArr = array();
	$DataArr[] = array('YearID'=>30, 'Quota'=>0);
	$DataArr[] = array('YearID'=>45, 'Quota'=>0);
	$DataArr[] = array('YearID'=>50, 'Quota'=>0);
	$DataArr[] = array('YearID'=>39, 'Quota'=>0);
	$DataArr[] = array('YearID'=>57, 'Quota'=>0);
	$DataArr[] = array('YearID'=>58, 'Quota'=>0);
	$Success[$AwardID]['UpdateApplicableFormInfo'] = $lreportcard_award->Update_Award_Applicable_Form_Info($AwardID, $ReportID, $DataArr);
	$DataArr = array();
	$CriteriaOrder = 0;
	$DataArr[] = array('CriteriaType'=>'RANK', 'CriteriaOrder'=>++$CriteriaOrder, 'RankField'=>'OrderMeritForm', 'FromRank'=>1, 'ToRank'=>1);
	$Success[$AwardID]['UpdateCriteria'] = $lreportcard_award->Update_Award_Criteria($AwardID, $ReportID, $DataArr);
	
	### Second in Form
	++$AwardID;
	$DataArr = array('AwardID'=>$AwardID, 'AwardCode'=>'second_in_form', 'AwardNameEn'=>'Second in Form', 'AwardNameCh'=>'Second in Form', 'ApplicableReportType'=>'T', 'AwardType'=>'OVERALL', 'SchoolCode'=>$ReportCardCustomSchoolName, 'DisplayOrder'=>++$DisplayOrder);
	$Success[$AwardID]['InsertAward'] = $lreportcard_award->Insert_Award_Record($DataArr);
	$DataArr = array();
	$DataArr[] = array('YearID'=>30, 'Quota'=>0);
	$DataArr[] = array('YearID'=>45, 'Quota'=>0);
	$DataArr[] = array('YearID'=>50, 'Quota'=>0);
	$DataArr[] = array('YearID'=>39, 'Quota'=>0);
	$DataArr[] = array('YearID'=>57, 'Quota'=>0);
	$DataArr[] = array('YearID'=>58, 'Quota'=>0);
	$Success[$AwardID]['UpdateApplicableFormInfo'] = $lreportcard_award->Update_Award_Applicable_Form_Info($AwardID, $ReportID, $DataArr);
	$DataArr = array();
	$CriteriaOrder = 0;
	$DataArr[] = array('CriteriaType'=>'RANK', 'CriteriaOrder'=>++$CriteriaOrder, 'RankField'=>'OrderMeritForm', 'FromRank'=>2, 'ToRank'=>2);
	$Success[$AwardID]['UpdateCriteria'] = $lreportcard_award->Update_Award_Criteria($AwardID, $ReportID, $DataArr);
	
	### Third in Form
	++$AwardID;
	$DataArr = array('AwardID'=>$AwardID, 'AwardCode'=>'third_in_form', 'AwardNameEn'=>'Third in Form', 'AwardNameCh'=>'Third in Form', 'ApplicableReportType'=>'T', 'AwardType'=>'OVERALL', 'SchoolCode'=>$ReportCardCustomSchoolName, 'DisplayOrder'=>++$DisplayOrder);
	$Success[$AwardID]['InsertAward'] = $lreportcard_award->Insert_Award_Record($DataArr);
	$DataArr = array();
	$DataArr[] = array('YearID'=>30, 'Quota'=>0);
	$DataArr[] = array('YearID'=>45, 'Quota'=>0);
	$DataArr[] = array('YearID'=>50, 'Quota'=>0);
	$DataArr[] = array('YearID'=>39, 'Quota'=>0);
	$DataArr[] = array('YearID'=>57, 'Quota'=>0);
	$DataArr[] = array('YearID'=>58, 'Quota'=>0);
	$Success[$AwardID]['UpdateApplicableFormInfo'] = $lreportcard_award->Update_Award_Applicable_Form_Info($AwardID, $ReportID, $DataArr);
	$DataArr = array();
	$CriteriaOrder = 0;
	$DataArr[] = array('CriteriaType'=>'RANK', 'CriteriaOrder'=>++$CriteriaOrder, 'RankField'=>'OrderMeritForm', 'FromRank'=>3, 'ToRank'=>3);
	$Success[$AwardID]['UpdateCriteria'] = $lreportcard_award->Update_Award_Criteria($AwardID, $ReportID, $DataArr);
	
	### Outstanding Academic Award
	++$AwardID;
	$DataArr = array('AwardID'=>$AwardID, 'AwardCode'=>'outstanding_academic_award', 'AwardNameEn'=>'Outstanding Academic Award', 'AwardNameCh'=>'Outstanding Academic Award', 'ApplicableReportType'=>'T', 'AwardType'=>'OVERALL', 'SchoolCode'=>$ReportCardCustomSchoolName, 'DisplayOrder'=>++$DisplayOrder);
	$Success[$AwardID]['InsertAward'] = $lreportcard_award->Insert_Award_Record($DataArr);
	$DataArr = array();
	$DataArr[] = array('YearID'=>30, 'Quota'=>0);
	$DataArr[] = array('YearID'=>45, 'Quota'=>0);
	$DataArr[] = array('YearID'=>50, 'Quota'=>0);
	$DataArr[] = array('YearID'=>39, 'Quota'=>0);
	$DataArr[] = array('YearID'=>57, 'Quota'=>0);
	$DataArr[] = array('YearID'=>58, 'Quota'=>0);
	$Success[$AwardID]['UpdateApplicableFormInfo'] = $lreportcard_award->Update_Award_Applicable_Form_Info($AwardID, $ReportID, $DataArr);
	$DataArr = array();
	$CriteriaOrder = 0;
	$DataArr[] = array('CriteriaType'=>'RANK', 'CriteriaOrder'=>++$CriteriaOrder, 'RankField'=>'OrderMeritForm', 'FromRank'=>4, 'ToRank'=>10);
	$Success[$AwardID]['UpdateCriteria'] = $lreportcard_award->Update_Award_Criteria($AwardID, $ReportID, $DataArr);
	
	### Best in (Subject)
	++$AwardID;
	$DataArr = array('AwardID'=>$AwardID, 'AwardCode'=>'best_in_subject', 'AwardNameEn'=>'Best in', 'AwardNameCh'=>'Best in', 'ApplicableReportType'=>'T', 'AwardType'=>'SUBJECT', 'SchoolCode'=>$ReportCardCustomSchoolName, 'DisplayOrder'=>++$DisplayOrder);
	$Success[$AwardID]['InsertAward'] = $lreportcard_award->Insert_Award_Record($DataArr);
	$DataArr = array();
	$DataArr[] = array('YearID'=>30, 'Quota'=>0);
	$DataArr[] = array('YearID'=>45, 'Quota'=>0);
	$DataArr[] = array('YearID'=>50, 'Quota'=>0);
	$DataArr[] = array('YearID'=>39, 'Quota'=>0);
	$DataArr[] = array('YearID'=>57, 'Quota'=>0);
	$DataArr[] = array('YearID'=>58, 'Quota'=>0);
	$Success[$AwardID]['UpdateApplicableFormInfo'] = $lreportcard_award->Update_Award_Applicable_Form_Info($AwardID, $ReportID, $DataArr);
	$DataArr = array();
	$CriteriaOrder = 0;
	$DataArr[] = array('CriteriaType'=>'RANK', 'CriteriaOrder'=>++$CriteriaOrder, 'RankField'=>'OrderMeritForm', 'FromRank'=>1, 'ToRank'=>3);
	$Success[$AwardID]['UpdateCriteria'] = $lreportcard_award->Update_Award_Criteria($AwardID, $ReportID, $DataArr);
	
	### First in (Subject)
	++$AwardID;
	$DataArr = array('AwardID'=>$AwardID, 'AwardCode'=>'first_in_subject', 'AwardNameEn'=>'First in', 'AwardNameCh'=>'First in', 'ApplicableReportType'=>'T', 'AwardType'=>'SUBJECT', 'SchoolCode'=>$ReportCardCustomSchoolName, 'DisplayOrder'=>++$DisplayOrder);
	$Success[$AwardID]['InsertAward'] = $lreportcard_award->Insert_Award_Record($DataArr);
	$DataArr = array();
	$DataArr[] = array('YearID'=>30, 'Quota'=>0);
	$DataArr[] = array('YearID'=>45, 'Quota'=>0);
	$DataArr[] = array('YearID'=>50, 'Quota'=>0);
	$DataArr[] = array('YearID'=>39, 'Quota'=>0);
	$DataArr[] = array('YearID'=>57, 'Quota'=>0);
	$DataArr[] = array('YearID'=>58, 'Quota'=>0);
	$Success[$AwardID]['UpdateApplicableFormInfo'] = $lreportcard_award->Update_Award_Applicable_Form_Info($AwardID, $ReportID, $DataArr);
	$DataArr = array();
	$CriteriaOrder = 0;
	$DataArr[] = array('CriteriaType'=>'RANK', 'CriteriaOrder'=>++$CriteriaOrder, 'RankField'=>'OrderMeritForm', 'FromRank'=>1, 'ToRank'=>1);
	$Success[$AwardID]['UpdateCriteria'] = $lreportcard_award->Update_Award_Criteria($AwardID, $ReportID, $DataArr);
	
	
	
	### First in Form Scholarship
	++$AwardID;
	$DataArr = array('AwardID'=>$AwardID, 'AwardCode'=>'first_in_form_scholarship', 'AwardNameEn'=>'First in Form Scholarship', 'AwardNameCh'=>'First in Form Scholarship', 'ApplicableReportType'=>'W', 'AwardType'=>'OVERALL', 'SchoolCode'=>$ReportCardCustomSchoolName, 'DisplayOrder'=>++$DisplayOrder);
	$Success[$AwardID]['InsertAward'] = $lreportcard_award->Insert_Award_Record($DataArr);
	$DataArr = array();
	$DataArr[] = array('YearID'=>30, 'Quota'=>0);
	$DataArr[] = array('YearID'=>45, 'Quota'=>0);
	$DataArr[] = array('YearID'=>50, 'Quota'=>0);
	$DataArr[] = array('YearID'=>39, 'Quota'=>0);
	$DataArr[] = array('YearID'=>57, 'Quota'=>0);
	$DataArr[] = array('YearID'=>58, 'Quota'=>0);
	$Success[$AwardID]['UpdateApplicableFormInfo'] = $lreportcard_award->Update_Award_Applicable_Form_Info($AwardID, $ReportID, $DataArr);
	$DataArr = array();
	$CriteriaOrder = 0;
	$DataArr[] = array('CriteriaType'=>'RANK', 'CriteriaOrder'=>++$CriteriaOrder, 'RankField'=>'OrderMeritForm', 'FromRank'=>1, 'ToRank'=>1);
	$Success[$AwardID]['UpdateCriteria'] = $lreportcard_award->Update_Award_Criteria($AwardID, $ReportID, $DataArr);
	
	### Second in Form Scholarship
	++$AwardID;
	$DataArr = array('AwardID'=>$AwardID, 'AwardCode'=>'second_in_form_scholarship', 'AwardNameEn'=>'Second in Form Scholarship', 'AwardNameCh'=>'Second in Form Scholarship', 'ApplicableReportType'=>'W', 'AwardType'=>'OVERALL', 'SchoolCode'=>$ReportCardCustomSchoolName, 'DisplayOrder'=>++$DisplayOrder);
	$Success[$AwardID]['InsertAward'] = $lreportcard_award->Insert_Award_Record($DataArr);
	$DataArr = array();
	$DataArr[] = array('YearID'=>30, 'Quota'=>0);
	$DataArr[] = array('YearID'=>45, 'Quota'=>0);
	$DataArr[] = array('YearID'=>50, 'Quota'=>0);
	$DataArr[] = array('YearID'=>39, 'Quota'=>0);
	$DataArr[] = array('YearID'=>57, 'Quota'=>0);
	$DataArr[] = array('YearID'=>58, 'Quota'=>0);
	$Success[$AwardID]['UpdateApplicableFormInfo'] = $lreportcard_award->Update_Award_Applicable_Form_Info($AwardID, $ReportID, $DataArr);
	$DataArr = array();
	$CriteriaOrder = 0;
	$DataArr[] = array('CriteriaType'=>'RANK', 'CriteriaOrder'=>++$CriteriaOrder, 'RankField'=>'OrderMeritForm', 'FromRank'=>2, 'ToRank'=>2);
	$Success[$AwardID]['UpdateCriteria'] = $lreportcard_award->Update_Award_Criteria($AwardID, $ReportID, $DataArr);
	
	### Third in Form Scholarship
	++$AwardID;
	$DataArr = array('AwardID'=>$AwardID, 'AwardCode'=>'third_in_form_scholarship', 'AwardNameEn'=>'Third in Form Scholarship', 'AwardNameCh'=>'Third in Form Scholarship', 'ApplicableReportType'=>'W', 'AwardType'=>'OVERALL', 'SchoolCode'=>$ReportCardCustomSchoolName, 'DisplayOrder'=>++$DisplayOrder);
	$Success[$AwardID]['InsertAward'] = $lreportcard_award->Insert_Award_Record($DataArr);
	$DataArr = array();
	$DataArr[] = array('YearID'=>30, 'Quota'=>0);
	$DataArr[] = array('YearID'=>45, 'Quota'=>0);
	$DataArr[] = array('YearID'=>50, 'Quota'=>0);
	$DataArr[] = array('YearID'=>39, 'Quota'=>0);
	$DataArr[] = array('YearID'=>57, 'Quota'=>0);
	$DataArr[] = array('YearID'=>58, 'Quota'=>0);
	$Success[$AwardID]['UpdateApplicableFormInfo'] = $lreportcard_award->Update_Award_Applicable_Form_Info($AwardID, $ReportID, $DataArr);
	$DataArr = array();
	$CriteriaOrder = 0;
	$DataArr[] = array('CriteriaType'=>'RANK', 'CriteriaOrder'=>++$CriteriaOrder, 'RankField'=>'OrderMeritForm', 'FromRank'=>3, 'ToRank'=>3);
	$Success[$AwardID]['UpdateCriteria'] = $lreportcard_award->Update_Award_Criteria($AwardID, $ReportID, $DataArr);
	
	### Scholarship of Outstanding Academic Performance
	++$AwardID;
	$DataArr = array('AwardID'=>$AwardID, 'AwardCode'=>'scholarship_of_outstanding_academic_performance', 'AwardNameEn'=>'Scholarship of Outstanding Academic Performance', 'AwardNameCh'=>'Scholarship of Outstanding Academic Performance', 'ApplicableReportType'=>'W', 'AwardType'=>'OVERALL', 'SchoolCode'=>$ReportCardCustomSchoolName, 'DisplayOrder'=>++$DisplayOrder);
	$Success[$AwardID]['InsertAward'] = $lreportcard_award->Insert_Award_Record($DataArr);
	$DataArr = array();
	$DataArr[] = array('YearID'=>30, 'Quota'=>0);
	$DataArr[] = array('YearID'=>45, 'Quota'=>0);
	$DataArr[] = array('YearID'=>50, 'Quota'=>0);
	$DataArr[] = array('YearID'=>39, 'Quota'=>0);
	$DataArr[] = array('YearID'=>57, 'Quota'=>0);
	$DataArr[] = array('YearID'=>58, 'Quota'=>0);
	$Success[$AwardID]['UpdateApplicableFormInfo'] = $lreportcard_award->Update_Award_Applicable_Form_Info($AwardID, $ReportID, $DataArr);
	$DataArr = array();
	$CriteriaOrder = 0;
	$DataArr[] = array('CriteriaType'=>'RANK', 'CriteriaOrder'=>++$CriteriaOrder, 'RankField'=>'OrderMeritForm', 'FromRank'=>4, 'ToRank'=>10);
	$Success[$AwardID]['UpdateCriteria'] = $lreportcard_award->Update_Award_Criteria($AwardID, $ReportID, $DataArr);
	
	### Scholarship for Best in (Subject)
	++$AwardID;
	$DataArr = array('AwardID'=>$AwardID, 'AwardCode'=>'scholarship_for_best_in_subject', 'AwardNameEn'=>'Scholarship for Best in', 'AwardNameCh'=>'Scholarship for Best in', 'ApplicableReportType'=>'W', 'AwardType'=>'SUBJECT', 'SchoolCode'=>$ReportCardCustomSchoolName, 'DisplayOrder'=>++$DisplayOrder);
	$Success[$AwardID]['InsertAward'] = $lreportcard_award->Insert_Award_Record($DataArr);
	$DataArr = array();
	$DataArr[] = array('YearID'=>30, 'Quota'=>0);
	$DataArr[] = array('YearID'=>45, 'Quota'=>0);
	$DataArr[] = array('YearID'=>50, 'Quota'=>0);
	$DataArr[] = array('YearID'=>39, 'Quota'=>0);
	$DataArr[] = array('YearID'=>57, 'Quota'=>0);
	$DataArr[] = array('YearID'=>58, 'Quota'=>0);
	$Success[$AwardID]['UpdateApplicableFormInfo'] = $lreportcard_award->Update_Award_Applicable_Form_Info($AwardID, $ReportID, $DataArr);
	$DataArr = array();
	$CriteriaOrder = 0;
	$DataArr[] = array('CriteriaType'=>'RANK', 'CriteriaOrder'=>++$CriteriaOrder, 'RankField'=>'OrderMeritForm', 'FromRank'=>1, 'ToRank'=>3);
	$Success[$AwardID]['UpdateCriteria'] = $lreportcard_award->Update_Award_Criteria($AwardID, $ReportID, $DataArr);
	
	### Scholarship for First in (Subject)
	++$AwardID;
	$DataArr = array('AwardID'=>$AwardID, 'AwardCode'=>'scholarship_for_first_in_subject', 'AwardNameEn'=>'Scholarship for First in', 'AwardNameCh'=>'Scholarship for First in', 'ApplicableReportType'=>'W', 'AwardType'=>'SUBJECT', 'SchoolCode'=>$ReportCardCustomSchoolName, 'DisplayOrder'=>++$DisplayOrder);
	$Success[$AwardID]['InsertAward'] = $lreportcard_award->Insert_Award_Record($DataArr);
	$DataArr = array();
	$DataArr[] = array('YearID'=>30, 'Quota'=>0);
	$DataArr[] = array('YearID'=>45, 'Quota'=>0);
	$DataArr[] = array('YearID'=>50, 'Quota'=>0);
	$DataArr[] = array('YearID'=>39, 'Quota'=>0);
	$DataArr[] = array('YearID'=>57, 'Quota'=>0);
	$DataArr[] = array('YearID'=>58, 'Quota'=>0);
	$Success[$AwardID]['UpdateApplicableFormInfo'] = $lreportcard_award->Update_Award_Applicable_Form_Info($AwardID, $ReportID, $DataArr);
	$DataArr = array();
	$CriteriaOrder = 0;
	$DataArr[] = array('CriteriaType'=>'RANK', 'CriteriaOrder'=>++$CriteriaOrder, 'RankField'=>'OrderMeritForm', 'FromRank'=>1, 'ToRank'=>1);
	$Success[$AwardID]['UpdateCriteria'] = $lreportcard_award->Update_Award_Criteria($AwardID, $ReportID, $DataArr);
	
	
	### Award for Exemplary Personal Qualities
	++$AwardID;
	$DataArr = array('AwardID'=>$AwardID, 'AwardCode'=>'award_for_exemplary_personal_qualities', 'AwardNameEn'=>'Award for Exemplary Personal Qualities', 'AwardNameCh'=>'Award for Exemplary Personal Qualities', 'ApplicableReportType'=>'ALL', 'AwardType'=>'OVERALL', 'SchoolCode'=>$ReportCardCustomSchoolName, 'DisplayOrder'=>++$DisplayOrder);
	$Success[$AwardID]['InsertAward'] = $lreportcard_award->Insert_Award_Record($DataArr);
	$DataArr = array();
	$DataArr[] = array('YearID'=>30, 'Quota'=>0);
	$DataArr[] = array('YearID'=>45, 'Quota'=>0);
	$DataArr[] = array('YearID'=>50, 'Quota'=>0);
	$DataArr[] = array('YearID'=>39, 'Quota'=>0);
	$DataArr[] = array('YearID'=>57, 'Quota'=>0);
	$DataArr[] = array('YearID'=>58, 'Quota'=>0);
	$Success[$AwardID]['UpdateApplicableFormInfo'] = $lreportcard_award->Update_Award_Applicable_Form_Info($AwardID, $ReportID, $DataArr);
	$DataArr = array();
	$CriteriaOrder = 0;
	$DataArr[] = array('CriteriaType'=>'PERSONAL_CHAR', 'CriteriaOrder'=>++$CriteriaOrder, 'MinPersonalCharCode'=>'40');
	$Success[$AwardID]['UpdateCriteria'] = $lreportcard_award->Update_Award_Criteria($AwardID, $ReportID, $DataArr);
	
	### Best-all-round Student Award
	++$AwardID;
	$DataArr = array('AwardID'=>$AwardID, 'AwardCode'=>'best_all_round_student_award', 'AwardNameEn'=>'Best-all-round Student Award', 'AwardNameCh'=>'Best-all-round Student Award', 'ApplicableReportType'=>'ALL', 'AwardType'=>'OVERALL', 'SchoolCode'=>$ReportCardCustomSchoolName, 'DisplayOrder'=>++$DisplayOrder);
	$Success[$AwardID]['InsertAward'] = $lreportcard_award->Insert_Award_Record($DataArr);
	$DataArr = array();
	$DataArr[] = array('YearID'=>30, 'Quota'=>0);
	$DataArr[] = array('YearID'=>45, 'Quota'=>0);
	$DataArr[] = array('YearID'=>50, 'Quota'=>0);
	$DataArr[] = array('YearID'=>39, 'Quota'=>0);
	$DataArr[] = array('YearID'=>57, 'Quota'=>0);
	$DataArr[] = array('YearID'=>58, 'Quota'=>0);
	$Success[$AwardID]['UpdateApplicableFormInfo'] = $lreportcard_award->Update_Award_Applicable_Form_Info($AwardID, $ReportID, $DataArr);
	$DataArr = array();
	$CriteriaOrder = 0;
	$DataArr[] = array('CriteriaType'=>'RANK', 'CriteriaOrder'=>++$CriteriaOrder, 'RankField'=>'OrderMeritForm', 'FromRank'=>1, 'ToRank'=>10);
	$DataArr[] = array('CriteriaType'=>'PERSONAL_CHAR', 'CriteriaOrder'=>++$CriteriaOrder, 'MinPersonalCharCode'=>'40', 'PreviousCriteriaRelation'=>'AND');
	$Success[$AwardID]['UpdateCriteria'] = $lreportcard_award->Update_Award_Criteria($AwardID, $ReportID, $DataArr);
	
	
	### Award for improvement in (Subject)
	++$AwardID;
	$DataArr = array('AwardID'=>$AwardID, 'AwardCode'=>'award_for_improvement_in_subject', 'AwardNameEn'=>'Award for improvement in', 'AwardNameCh'=>'Award for improvement in', 'ApplicableReportType'=>'T', 'AwardType'=>'SUBJECT', 'SchoolCode'=>$ReportCardCustomSchoolName, 'DisplayOrder'=>++$DisplayOrder);
	$Success[$AwardID]['InsertAward'] = $lreportcard_award->Insert_Award_Record($DataArr);
	$DataArr = array();
	$DataArr[] = array('YearID'=>30, 'Quota'=>0);
	$DataArr[] = array('YearID'=>45, 'Quota'=>0);
	$DataArr[] = array('YearID'=>50, 'Quota'=>0);
	$DataArr[] = array('YearID'=>39, 'Quota'=>0);
	$DataArr[] = array('YearID'=>57, 'Quota'=>0);
	$DataArr[] = array('YearID'=>58, 'Quota'=>0);
	$Success[$AwardID]['UpdateApplicableFormInfo'] = $lreportcard_award->Update_Award_Applicable_Form_Info($AwardID, $ReportID, $DataArr);
	$DataArr = array();
	$CriteriaOrder = 0;
	$DataArr[] = array('CriteriaType'=>'IMPROVEMENT', 'CriteriaOrder'=>++$CriteriaOrder, 'ImprovementField'=>'SDScore', 'MinImprovement'=>'1.3');
	$Success[$AwardID]['UpdateCriteria'] = $lreportcard_award->Update_Award_Criteria($AwardID, $ReportID, $DataArr);
	
	### Scholarship for improvement in (Subject)
	++$AwardID;
	$DataArr = array('AwardID'=>$AwardID, 'AwardCode'=>'scholarship_for_improvement_in_subject', 'AwardNameEn'=>'Scholarship for improvement in', 'AwardNameCh'=>'Scholarship for improvement in', 'ApplicableReportType'=>'T', 'AwardType'=>'SUBJECT', 'SchoolCode'=>$ReportCardCustomSchoolName, 'DisplayOrder'=>++$DisplayOrder);
	$Success[$AwardID]['InsertAward'] = $lreportcard_award->Insert_Award_Record($DataArr);
	$DataArr = array();
	$DataArr[] = array('YearID'=>30, 'Quota'=>0);
	$DataArr[] = array('YearID'=>45, 'Quota'=>0);
	$DataArr[] = array('YearID'=>50, 'Quota'=>0);
	$DataArr[] = array('YearID'=>39, 'Quota'=>0);
	$DataArr[] = array('YearID'=>57, 'Quota'=>0);
	$DataArr[] = array('YearID'=>58, 'Quota'=>0);
	$Success[$AwardID]['UpdateApplicableFormInfo'] = $lreportcard_award->Update_Award_Applicable_Form_Info($AwardID, $ReportID, $DataArr);
	$DataArr = array();
	$CriteriaOrder = 0;
	$DataArr[] = array('CriteriaType'=>'AWARD_LINKAGE', 'CriteriaOrder'=>++$CriteriaOrder, 'FromRank'=>1, 'ToRank'=>1, 'LinkedAwardID'=>($AwardID-1), 'ExcludeOriginalAward'=>1);
	$Success[$AwardID]['UpdateCriteria'] = $lreportcard_award->Update_Award_Criteria($AwardID, $ReportID, $DataArr);
	
	### Improvement Scholarship (Whole Year)
	++$AwardID;
	$DataArr = array('AwardID'=>$AwardID, 'AwardCode'=>'improvement_scholarship_whole_year', 'AwardNameEn'=>'Improvement Scholarship (Whole Year)', 'AwardNameCh'=>'Improvement Scholarship (Whole Year)', 'ApplicableReportType'=>'W', 'AwardType'=>'OVERALL', 'SchoolCode'=>$ReportCardCustomSchoolName, 'DisplayOrder'=>++$DisplayOrder);
	$Success[$AwardID]['InsertAward'] = $lreportcard_award->Insert_Award_Record($DataArr);
	$DataArr = array();
	$DataArr[] = array('YearID'=>30, 'Quota'=>0);
	$DataArr[] = array('YearID'=>45, 'Quota'=>0);
	$DataArr[] = array('YearID'=>50, 'Quota'=>0);
	$DataArr[] = array('YearID'=>39, 'Quota'=>0);
	$DataArr[] = array('YearID'=>57, 'Quota'=>0);
	$DataArr[] = array('YearID'=>58, 'Quota'=>0);
	$Success[$AwardID]['UpdateApplicableFormInfo'] = $lreportcard_award->Update_Award_Applicable_Form_Info($AwardID, $ReportID, $DataArr);
	$DataArr = array();
	$CriteriaOrder = 0;
	$DataArr[] = array('CriteriaType'=>'IMPROVEMENT', 'CriteriaOrder'=>++$CriteriaOrder, 'ImprovementField'=>'GrandSDScore', 'MinImprovement'=>'0');
	$Success[$AwardID]['UpdateCriteria'] = $lreportcard_award->Update_Award_Criteria($AwardID, $ReportID, $DataArr);
	
	
	debug_pr($Success);
	
	
	# update General Settings - markd the script is executed
	//$sql = "insert ignore into GENERAL_SETTING 
	//					(Module, SettingName, SettingValue, DateInput) 
	//				values 
	//					('$ModuleName', '$SettingName', 1, now())";
	//$SuccessArr['UpdateGeneralSettings'] = $li->db_db_query($sql);
	
	$LogOutput .= "eReportCard - Award Setup for Shatin Methodist [End] ".date("Y-m-d H:i:s")."<br>";
	
	# show log
	echo $LogOutput;
	
	# save log
	$log_folder = $PATH_WRT_ROOT."file/reportcard2008/script_log";
	if(!file_exists($log_folder))
		mkdir($log_folder);
	$log_file = $log_folder."/award_setup_shatin_methodist_". date("YmdHis") .".log";
	$lf = new libfilesystem();
	//$lf->file_write(strip_tags($LogOutput), $log_file);
}
?>