<?php
/*
 * 	Purpose: update TransAmountBefore and TransAmountAfter for LIBMS_BALANCE_LOG [case #T139412]
 * 
 *  2018-10-02 [Cameron] create this file
 */

$path = ($FromCentralUpdate) ? $intranet_root."/" : "../../";
$PATH_WRT_ROOT = $path;
include_once($path."includes/global.php");
include_once($path."includes/libdb.php");
include_once($path."includes/liblibrarymgmt.php");

if (!$FromCentralUpdate) {
	intranet_auth();
	intranet_opendb();
}

$libms = new liblms();

if(($_POST['Flag']!=1) && !$FromCentralUpdate) {
	$isApply = false;
	echo("<form method='post'><input value='run script' type='submit'><input type='hidden' name='Flag' value=1></form>");
}
else {
	$isApply = true;
}


## 1. get BookID and UniqueID from LIBMS_BOOK_UNIQUE which RecordStatus='NORMAL' but LIBMS_WRITEOFF_LOG.Result='WRITEOFF'
	$sql = "SELECT      lbl.TransRefID, 
                        lbl.LogID, 
                        lbl.TransAmount, 
                        lol.Payment, 
                        lol.PaymentReceived,
                        lbl.TransDesc
            FROM 
                        LIBMS_BALANCE_LOG lbl
            INNER JOIN 
                        LIBMS_OVERDUE_LOG lol ON lol.OverDueLogID=lbl.TransRefID
            WHERE 
                        lbl.TransType='OverdueLog' 
            AND 
                        lbl.TransDesc IN ('pay overdue','waive overdue') 
            ORDER BY 
                        lbl.TransRefID, lbl.LogID"; 

    $logAry = $libms->returnResultSet($sql);
    $logAssoc = BuildMultiKeyAssoc($logAry, array('TransRefID','LogID'));
    unset($logAry);
    $result = array();
//debug_pr($sql);
//debug_pr($logAssoc);

    if (count($logAssoc)) {
    	if ($isApply) {
    	    $libms->Start_Trans();
    	}
    	foreach((array)$logAssoc as $_transRefID=>$_logAry) {

    	    if (count($_logAry)) {
    	        $owe = 0;
    			foreach((array)$_logAry as $__logID=>$__logAry) {
     				$transDesc = $__logAry['TransDesc'];
    				$transAmountBefore = $owe == 0 ? $__logAry['Payment'] : $owe;
    				$transAmountAfter = $transAmountBefore - $__logAry['TransAmount'];
    				$owe = $transAmountAfter;
    				
    				if (!empty($__logID)) {
    				    $sql = "UPDATE LIBMS_BALANCE_LOG SET TransAmountBefore='".$transAmountBefore."', TransAmountAfter='".$transAmountAfter ."' WHERE LogID='".$__logID."'";
//debug_pr($sql);					
    					if ($isApply) {
    					    $result['updateTransAmountBeforeAndAfter'] = $libms->db_db_query($sql);
    					}
    				}
    			}				
			}
		}
	
		$sql = "UPDATE LIBMS_BALANCE_LOG SET TransAmountAfter=0-TransAmount WHERE TransDesc='cancel overdue'";
		if ($isApply) {
		    $result['updateTransAmountAfterForCancelOverdue'] = $libms->db_db_query($sql);
		}
//debug_pr($result);

    	if ($isApply) {
    		if (!in_array(false,$result)) {
    		    $libms->Commit_Trans();
    			echo "<span style=\"color:green; font-weight:bold\">success</span><br>";
    		}
    		else {
    		    $libms->RollBack_Trans();
    			echo "<span style=\"color:red; font-weight:bold\">fail</span><br>";
    		}
    	}			
    }
    else {
    	print 'No record found<br>';
    }


if (!$FromCentralUpdate) {
	intranet_closedb();
}
?>