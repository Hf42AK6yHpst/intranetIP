<?php
// take away this in order to run the script.


// using 
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");

#################################################################
##################### Script Config [START] #####################
#################################################################
$debugMode = false;
#################################################################
###################### Script Config [END] ######################
#################################################################


if (!$flag) {
	echo '<html><br><a href=?flag=1>Click here to proceed</a> <br><br><br></body></html>';
	die();
}

intranet_opendb();

$db = new libdb();
$successAry = array();

$db->Start_Trans();

//iPorfolio record - table eclassdb.AWARD_STUDENT
$sql = "SELECT RecordID, UserID, AcademicYearID, ClassName, ClassNumber FROM $eclass_db.AWARD_STUDENT";
$result = $db->ReturnResultSet($sql);

$iPOtotalrecord = count($result);
debug_pr('$iPOtotalrecord='.$iPOtotalrecord);

$iPOcountUpdatedRecord = 0;
$iPOcountNoNeedUpdatedRecord = 0;
foreach($result as $UserInfoAry){
	$_academicYearID = '';
	$_userID = '';
	$_recordID = '';
	$_classname = '';
	$_classnumber = '';
	
	$_academicYearID = $UserInfoAry['AcademicYearID'];
	$_userID = $UserInfoAry['UserID'];
	$_recordID = $UserInfoAry['RecordID'];
	$_classname = $UserInfoAry['ClassName'];
	$_classnumber = $UserInfoAry['ClassNumber'];
	
	$libuser = new libuser($_userID);
	$classinfoAry = $libuser->getStudentInfoByAcademicYear($_academicYearID);
	
	if( $_classname == $classinfoAry[0]['ClassNameEn'] && $_classnumber == $classinfoAry[0]['ClassNumber']){
		$iPOcountNoNeedUpdatedRecord++;
		continue;
	}
	else{
		$_classname = $classinfoAry[0]['ClassNameEn'];
		$_classnumber = $classinfoAry[0]['ClassNumber'];
		$sql = " UPDATE $eclass_db.AWARD_STUDENT SET ClassName = '".$db->Get_Safe_Sql_Query($_classname)."' , ClassNumber = '".$_classnumber."'  WHERE RecordID = '".$_recordID."' ";
		$iPOcountUpdatedRecord++;
		if($debugMode){
			$successAry[] = 0;
		}
		else{
			$successAry[] = $db->db_db_query($sql);
		}
		
		if($_userID == 1971){
			debug_pr($sql);	
		}
		
	}
}
debug_pr('$iPOcountUpdatedRecord='.$iPOcountUpdatedRecord);
debug_pr('$iPOcountNoNeedUpdatedRecord='.$iPOcountNoNeedUpdatedRecord);


echo '------------------------------------------------------------------------------------------------------<br>';


//Student Profile record - table intranet.PROFILE_STUDENT_AWARD
$sql = "SELECT AcademicYearID, YearNameEN FROM ACADEMIC_YEAR";
$academicYearAry = $db->ReturnResultSet($sql);
$academicYearAssoAry = BuildMultiKeyAssoc($academicYearAry,'YearNameEN',array('AcademicYearID'),1);
unset($academicYearAry);

$sql = "SELECT StudentAwardID, UserID, Year, ClassName, ClassNumber FROM $intranet_db.PROFILE_STUDENT_AWARD";
$result = $db->ReturnResultSet($sql);

$SPtotalrecord = count($result);
debug_pr('$SPtotalrecord='.$SPtotalrecord);

$SPcountUpdatedRecord = 0;
$SPcountNoNeedUpdatedRecord = 0;
foreach($result as $UserInfoAry){
	$_academicYearID = '';
	$_userID = '';
	$_recordID = '';
	$_classname = '';
	$_classnumber = '';
	
	$_academicYearID = $academicYearAssoAry[$UserInfoAry['Year']];
	$_userID = $UserInfoAry['UserID'];
	$_recordID = $UserInfoAry['StudentAwardID'];
	$_classname = $UserInfoAry['ClassName'];
	$_classnumber = $UserInfoAry['ClassNumber'];
	
	$libuser = new libuser($_userID);
	$classinfoAry = $libuser->getStudentInfoByAcademicYear($_academicYearID);
	
	if( $_classname == $classinfoAry[0]['ClassNameEn'] && $_classnumber == $classinfoAry[0]['ClassNumber']){
		$SPcountNoNeedUpdatedRecord++;
		continue;
	}
	else{
		$_classname = $classinfoAry[0]['ClassNameEn'];
		$_classnumber = $classinfoAry[0]['ClassNumber'];
		$sql = " UPDATE $intranet_db.PROFILE_STUDENT_AWARD SET ClassName = '".$db->Get_Safe_Sql_Query($_classname)."' , ClassNumber = '".$_classnumber."'  WHERE StudentAwardID = '".$_recordID."' ";
		$SPcountUpdatedRecord++;
		if($debugMode){
			$successAry[] = 0;
		}
		else{
			$successAry[] = $db->db_db_query($sql);
		}
		
		if($_userID == 1971){
			debug_pr($sql);	
		}
	}
	
}
debug_pr('$SPcountUpdatedRecord='.$SPcountUpdatedRecord);
debug_pr('$SPcountNoNeedUpdatedRecord='.$SPcountNoNeedUpdatedRecord);
 
echo '------------------------------------------------------------------------------------------------------<br>';

debug_pr($successAry);

if($debugMode || in_array(false,$successAry)) { 
	echo "Reload class FAILED";
	$db->RollBack_Trans();
}
else {
	echo "Reload class Success";
	$db->Commit_Trans();
}

intranet_closedb();
?>