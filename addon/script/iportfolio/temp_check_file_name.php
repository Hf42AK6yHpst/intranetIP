<?php

function get_file_basename($file){
	$returnStr = '';
	if($file != ''){
		$tmp = explode('/', $file);
		$returnStr = trim($tmp[count($tmp)-1]);
	}
	return $returnStr;
}

$template_folder = "/home/web/eclass40/eclass30/files/eclass40_c35/notes/dc89d16f73c223bd50a6d837511589a0_1248079492TEMPLATES/TEMPLATES/";

$handle = opendir($template_folder);

while ($file = readdir($handle))
{
  if($file == "." || $file == "..") continue;

  $tmp_file_path = $template_folder.$file;

  if(date("Y-m-d", filemtime($tmp_file_path)) == date("Y-m-d"))
  {
    $lastDotPos = strrpos(basename($tmp_file_path), ".");
    $file_name = substr(basename($tmp_file_path), 0, $lastDotPos);
    $file_ext = substr(basename($tmp_file_path), $lastDotPos+1);
    $mod_path = substr($tmp_file_path, 0, strrpos($tmp_file_path, "/")) . "/" . $file_name . "_iPt{orig_id}." . $file_ext;
  
    $today_file_old_arr[] = array(
                          $tmp_file_path,
                          $file_name,
                          $file_ext,
                          $mod_path,
                          date("Y-m-d H:i:s", filemtime($tmp_file_path))
                        );
                        
		$filename_part_arr = explode(".", get_file_basename($tmp_file_path));
		$file_ext = array_pop($filename_part_arr);
		$file_name = implode(".", $filename_part_arr);
		$mod_path = substr($tmp_file_path, 0, strrpos($tmp_file_path, "/")) . "/" . $file_name . "_iPt{orig_id}." . $file_ext;
		
    $today_file_new_arr[] = array(
                      $tmp_file_path,
                      $file_name,
                      $file_ext,
                      $mod_path,
                      date("Y-m-d H:i:s", filemtime($tmp_file_path))
                    );
  }
}

$x = "<html>";
$x .= "<head>";
$x .= "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"/>";
$x .= "<style type=\"text/css\">";
$x .= "table {border-collapse:collapse;}";
$x .= "table, td, th {border:1px solid black;}";
$x .= "</style>";
$x .= "</head>";
$x .= "<body>";

$x .= "<table cellpadding=\"3\" cellspacing=\"0\">";
$x .= "<caption>Using Old Mechanism</caption>";
$x .= "<tr><th>File Full Path</th><th>File Name</th><th>File Ext</th><th>Path to student</th><th>Last Modified</th></tr>";
for($i=0; $i<count($today_file_old_arr); $i++)
{
  $x .= "<tr><td style=\"font-size:13px;\">".$today_file_old_arr[$i][0]."</td><td nowrap>".$today_file_old_arr[$i][1]."</td><td nowrap>".$today_file_old_arr[$i][2]."</td><td style=\"font-size:13px;\">".$today_file_old_arr[$i][3]."</td><td nowrap>".$today_file_old_arr[$i][4]."</td></tr>";
}
$x .= "</table>";

####################################
$x .= "<br /><br />";
####################################

$x .= "<table cellpadding=\"3\" cellspacing=\"0\">";
$x .= "<caption>Using New Mechanism</caption>";
$x .= "<tr><th>File Full Path</th><th>File Name</th><th>File Ext</th><th>Path to student</th><th>Last Modified</th></tr>";
for($i=0; $i<count($today_file_new_arr); $i++)
{
  $x .= "<tr><td style=\"font-size:13px;\">".$today_file_new_arr[$i][0]."</td><td nowrap>".$today_file_new_arr[$i][1]."</td><td nowrap>".$today_file_new_arr[$i][2]."</td><td style=\"font-size:13px;\">".$today_file_new_arr[$i][3]."</td><td nowrap>".$today_file_new_arr[$i][4]."</td></tr>";
}
$x .= "</table>"; 
 
$x .= "</body>";
$x .= "</html>";

$fp = fopen('testing_log/ipf_'.date("Ymd_His").'.html', 'w');
fwrite($fp, $x);
fclose($fp);

echo $x;

?>
