<?
//Update : fai
@SET_TIME_LIMIT(800);
include_once("../../../includes/global.php");
include_once("../../check.php");
//include_once($eclass_filepath.'/addon/check.php');
include_once($eclass_filepath.'/src/includes/php/lib-filesystem.php');
include_once($eclass_filepath.'/src/includes/php/lib-db.php');
include_once("../../../includes/libeclass.php");
//include_once("../../../includes/portfolio25/iPortfolioConfig.inc.php");
include_once("function.php");
?>
<link href="css/ipf.css" rel="stylesheet" type="text/css">
<?
intranet_opendb();
$currentTime = date("d/m/y : H:i:s", time());

$objDB = new libdb();
$objDB->db = $intranet_db;


$dbListValue = trim($dbListValue);
$lastAccessTime = trim($lastAccessTime);
$lastTableRecord = $tableRecord;
$lastAccessDB = trim($lastAccessDB);

echo "eclass_filepath ".$eclass_filepath."<br/>";
echo "eclass_prefix ".$eclass_prefix."<br/>";
$related_eclass_filepath = $eclass_filepath."/files/".$dbListValue;
echo "related_eclass_filepath ".$related_eclass_filepath."<Br/>";






//debug_r($lastTableRecord);
$showDatabaseList = databaseList($dbListValue);

$displayNumberOfRecord = "";
if($dbListValue != "")
{
	$totalRecordResult = getTotalRecordResult($dbListValue);	
	$displayNumberOfRecord = displayTotalRecordResult($totalRecordResult,$dbListValue);
	if($dbListValue == $lastAccessDB)
	{

		$displayCompareRecord = "<br/>Compare Table : ".$dbListValue.". Below is the details<br/>";
		$displayCompareRecord .= displayCompareRecord($totalRecordResult,$lastTableRecord);
		$displayCompareRecord .= "<br/>";

		$showUpdateFileList = updateFileList($related_eclass_filepath, 1);

	}


}

intranet_closedb();

?>
<font color = "blue"><b>Current Time : <?= $currentTime?></b></font><br/>
<hr/>
<form name = "form1" action="checkDBRecord.php" method="post">

<table><tr><td>Selected Database: </td><td><?=$showDatabaseList?>&nbsp;<input type = "submit" value="Show Record"></td></table>
<?=$showUpdateFileList?>
<?=$displayCompareRecord?>
<br/><?=$displayNumberOfRecord?><br/>
<input type = "hidden" name="lastAccessTime" value="<?= $currentTime?>">
<input type = "hidden" name="lastAccessDB" value = "<?=  $dbListValue?>">
</form>
<?
function databaseList($selectedValue)
{
	global $objDB;
	global $eclass_db;
	global $intranet_db;
	global $ipf_cfg;

	$sql = "show databases";
	$result = $objDB->returnArray($sql);

	$totalNo = sizeof($result);

	$html = "<select name=\"dbListValue\">";
	for($i = 0; $i < $totalNo;$i++)
	{
		$selected = "";
		$tableName = $result[$i]["Database"];
		if($tableName != "information_schema")
		{
			$selected = $tableName == $selectedValue ? "selected":"" ;
			$html .= "<option value=\"".$tableName."\" ".$selected.">".$tableName."</option>\n";
		}
	}
	$html .= "</select>";
	return $html;

}

function getTotalRecordResult ($dbName = null)
{
	global $objDB;
	global $eclass_db;
	global $intranet_db;
	global $ipf_cfg;

	//$sql = "SELECT table_name, table_rows FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = '".$dbName."'";
//	$sql = "show tables from ".$dbName;
	$sql = "show tables from ".$dbName." like 'IES%'";

	$result = $objDB->returnArray($sql);

	$totalCountResult = array();
	for($i = 0; $i < sizeof($result);$i++)
	{
//		$_tableName = $result[$i]["Tables_in_".$dbName];
		$_tableName = $result[$i]["Tables_in_IP25 (IES%)"];

		$sql_count = "select count(*) as total from ".$dbName.".".$_tableName;
		$_count = $objDB->returnArray($sql_count);
		$_noOfRecord = $_count[0]["total"];
		$_rowData["table_name"] = $_tableName;
		$_rowData["table_rows"] = $_noOfRecord;
		$totalCountResult[] = $_rowData;
	}
	return $totalCountResult;
}
function displayTotalRecordResult ($resultSet,$dbName)
{
	global $objDB;
	global $eclass_db;
	global $intranet_db;
	global $ipf_cfg;

	$htmlResult = "Table Name : ".$dbName."<br>";
	$htmlResult .= generateSepcialHTMLTableResult($resultSet);

	return $htmlResult;
}
function generateSepcialHTMLTableResult($resultSet)
{
	$noOfResult = sizeof($resultSet);
	$HeaderTitleArr  = array();
	$titleStack = array();
	$htmlTitleRow  = "";
	$noOfField  = 0;
	$htmlData  = "";
	if($noOfResult != 0)
	{
		$HeaderTitleArr = array_keys($resultSet[0]);

		$htmlTitleRow .= "<tr>";
		foreach ($HeaderTitleArr as $HeaderTitle)
		{

			if(is_int($HeaderTitle)>0)
			{
				
			}
			else
			{
				$htmlTitleRow .= '<th>'."\n";
				$htmlTitleRow .= trim($HeaderTitle);				
				$htmlTitleRow .= '</th>'."\n";

				$titleStack[] = $HeaderTitle;
			}

		}
		$htmlTitleRow .= "</tr>";
	
		for($i  = 0; $i <$noOfResult;$i++)
		{
			//DISPLAY EACH ROW
			$htmlData  .= "<tr>";
			$tableName = $resultSet[$i]["table_name"];
			$tableRow = $resultSet[$i]["table_rows"];
			$hiddenValue = $tableName.":".$tableRow;
			$htmlData .= "<td>".$tableName."&nbsp;</td><td>".$tableRow."&nbsp;<input type = \"hidden\" name=\"tableRecord[]\" value=\"".$hiddenValue."\"></td>\n";

			$htmlData  .= "</tr>";	
		}

		$htmlSummary = "<tr><td colspan = \"".$noOfField."\">Total Record : ".$noOfResult."</td></tr>";
	}
	
	$htmlResult = "<table border = \"1\">";
	$htmlResult .=  $htmlTitleRow;
	$htmlResult .=  $htmlData;
	$htmlResult .=  $htmlSummary;
	$htmlResult .=  "</table>";
	return $htmlResult;
}
function displayCompareRecord($newResultSet,$lastResultSet)
{
	// a variable to store the old dataset come from $lastResultSet with format $compareOldSet["LINK"] = 0 , $compareOldSet["FILE"] = 12
	$compareOldSet = array(); 

	for($i =0 ; $i < sizeof($lastResultSet);$i++)
	{
		$_data = $lastResultSet[$i];
		$_tempData = split(":",$_data);
		$_lastTableName = $_tempData[0];
		$_lastNoOFRecord = $_tempData[1];
		$compareOldSet[$_lastTableName] = $_lastNoOFRecord;
	}
	
	$comparedResult  = array(); //store the compared result
	for($i = 0; $i < sizeof ($newResultSet);$i++)
	{
		$_tableName = $newResultSet[$i]["table_name"];
		$_numOfData = $newResultSet[$i]["table_rows"];
		$_lastNumOfData = $compareOldSet[$_tableName];
		if($_lastNumOfData != $_numOfData)
		{
			$resultData["Table Name"] = $_tableName;
			$resultData["Pre Num of Record"] = $_lastNumOfData;
			$resultData["Current Num of Record"] = $_numOfData;
			$different = intval($_numOfData - $_lastNumOfData);
			$color = "green";
			if($different > 0)
			{
				$color = "blue";
			}elseif($different < 0)
			{
					$color = "red";
			}


			$resultData["Different"] = "<b><font color = \"".$color."\">".$different."</font></b>";
			$comparedResult[] = $resultData;
		}
	}

	if(sizeof($comparedResult) == 0)
	{
		$htmlCompareResult = "Number of record is the same.";
	}else
	{
		$htmlCompareResult = generateHTMLTableResult($comparedResult);
	}
	
	return $htmlCompareResult;
}
function updateFileList($related_eclass_filepath, $updateMin = 1)
{
	if(!file_exists($related_eclass_filepath))
	{
		$htmlResult = $related_eclass_filepath." does not exist!<br/>";
	}
	else
	{
		$cmd = "find ".$related_eclass_filepath."/. -name \"*.*\" -mmin -".$updateMin." -exec ls -l {} \\;";
		$cmd = OsCommandSafe($cmd);
		$returnMsg = exec($cmd,$_tmpListResult);

		
		$listResult  = array();
		if(sizeof($_tmpListResult) > 0)
		{
			for($i=0;$i< sizeof($_tmpListResult); $i++)
			{
				$fileName["File Name"] = $_tmpListResult[$i];
				$listResult[] = $fileName;
			}
			$htmlResult = "File List under \"".$related_eclass_filepath."\" update within  ".$updateMin." mins.";
			$htmlResult .= generateHTMLTableResult($listResult);	
		}
		else
		{
			$htmlResult = "There is no new file under ".$related_eclass_filepath." within ".$updateMin." mins. <br/>";
		}

	}
	return "<br/><br/>".$htmlResult."<br/>";
}
?>

