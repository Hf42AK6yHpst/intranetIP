<?php
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."addon/check.php");
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");

intranet_opendb();

$lf = new libfilesystem();
$libscm = new subject_class_mapping();


### Backup DB
$ASSESSMENT_STUDENT_SUBJECT_RECORD = $eclass_db.'.ASSESSMENT_STUDENT_SUBJECT_RECORD';
$ASSESSMENT_STUDENT_SUBJECT_RECORD_BAK = $eclass_db.'.ASSESSMENT_STUDENT_SUBJECT_RECORD_CMPID_PATCH_'.date("Ymd_His");
$sql = "Create Table $ASSESSMENT_STUDENT_SUBJECT_RECORD_BAK Select * From $ASSESSMENT_STUDENT_SUBJECT_RECORD";
$BackupSuccess = $libscm->db_db_query($sql);

if (!$BackupSuccess) {
	echo "Backup Database Failed...";
	die();
}


### Build Component Subject Info Arr
$SubjectInfoArr = $libscm->Get_Subject_List_With_Component();
$numOfSubject = count($SubjectInfoArr);

// $SubjectComponentAssoArr[$ParentSubjectID] = array($ComponentSubjectID, $ComponentSubjectID);
$SubjectComponentAssoArr = array();
for ($i=0; $i<$numOfSubject; $i++) {
	$thisSubjectID = $SubjectInfoArr[$i]['SubjectID'];
	$thisIsComponent = $SubjectInfoArr[$i]['IsComponent'];
	$thisParentSubjectID = $SubjectInfoArr[$i]['ParentSubjectID'];
	
	if (!$thisIsComponent) {
		continue;
	}
	
	(array)$SubjectComponentAssoArr[$thisParentSubjectID][] = $thisSubjectID;
}
$ExistingSubjectIDArr = Get_Array_By_Key($SubjectInfoArr, 'SubjectID');


### Get Assessment Result
$sql = "Select
				RecordID, SubjectID, SubjectComponentID
		From
				$ASSESSMENT_STUDENT_SUBJECT_RECORD
		Where
				SubjectID Is Not Null And SubjectID != '' And SubjectID != 0
				And SubjectComponentID Is Not Null And SubjectComponentID != '' And SubjectComponentID != 0
		";
$AssessmentInfoArr = $libscm->returnArray($sql);
$numOfAssessment = count($AssessmentInfoArr);


$numOfWrongRecord = 0;
$numOfSuccess = 0;
$numOfFailed = 0;
$logContent = '';
$logContent .= 'RecordID,SubjectID,OldSubjectComponentID,Status'."\r\n";

for ($i=0; $i<$numOfAssessment; $i++) {
	$thisRecordID = $AssessmentInfoArr[$i]['RecordID'];
	$thisSubjectID = $AssessmentInfoArr[$i]['SubjectID'];
	$thisSubjectComponentID = $AssessmentInfoArr[$i]['SubjectComponentID'];
	
	if (!in_array($thisSubjectID, (array)$ExistingSubjectIDArr)) {
		// Subject Deleted => cannot check => skip
		continue;
	}
	
	if (in_array($thisSubjectComponentID, (array)$SubjectComponentAssoArr[$thisSubjectID])) {
		$thisWrongComponentID = false;
	}
	else {
		$thisWrongComponentID = true;
	}
	
	if ($thisWrongComponentID) {
		$numOfWrongRecord++;
		
		$sql = "Update $ASSESSMENT_STUDENT_SUBJECT_RECORD Set SubjectComponentID = Null Where RecordID = '".$thisRecordID."'";
		$thisSuccess = $libscm->db_db_query($sql);
		
		if ($thisSuccess) {
			$numOfSuccess++;
			$thisStatus = 'Success';
		}
		else {
			$numOfFailed++;
			$thisStatus = '<font color="red">FAILED</font>';
		}
		$logContent .= $thisRecordID.','.$thisSubjectID.','.$thisSubjectComponentID.','.$thisStatus."\r\n";
	}
}


$logContent .= "\r\n";
$logContent .= 'Total number of Wrong Subject Component records: '.$numOfWrongRecord."\r\n";
$logContent .= 'Number of success update records: '.$numOfSuccess."\r\n";
$logContent .= 'Number of falied update records: '.$numOfFailed."\r\n";

$log_filepath = $PATH_WRT_ROOT."file/iportfolio/log/remove_academic_wrong_subject_component_id_".date("Ymd_His").".txt";
$lf->file_write($logContent, $log_filepath);

echo "Logged at $log_filepath";

intranet_closedb();
?>