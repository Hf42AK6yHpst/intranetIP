<?php

include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
intranet_opendb();

if(isset($operation))
{
  function writeDataLog($x)
  {
    global $intranet_root, $operation;
  
    $path = $intranet_root.'/addon/script/iportfolio/lp_data_log/op_'.$operation.'_'.date("Ymd_His").'.dat';
  
    $fp = fopen($path, 'w');
    fwrite($fp, $x);
    fclose($fp);
    
    return $path;
  }
  
  function getData()
  {
    $li = new libdb();
    
    $sql = "SELECT FileID, User_id, Title, Location, VirPath FROM eclass40_c35.eclass_file WHERE Location LIKE '%student_u2_wp35%' and title LIKE '%ipt%'";
    return $li->returnArray($sql);
  }
  
  function data2string($db_arr)
  {
    return serialize($db_arr);
  }
  
  $data_arr = getData();
  $data_str = data2string($data_arr);
  $file_path = writeDataLog($data_str);
  
  echo "Completed!!<br />";
  echo "Path: ".$file_path;
}
else
{
  function getStageSelect()
  {
  	$html = "<select name=\"operation\">";
  	$html .= "<option value=\"init\">Initialization</option>\n";
  	$html .= "<option value=\"reinit\">Re-initialization</option>\n";
  	$html .= "<option value=\"reset\">Template Reset</option>\n";
  	$html .= "</select>";
  	return $html;
  }

?>

<html>
<head>
<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv='content-type' content='text/html; charset=<?=$html_charset?>' />
<link href="css/ipf.css" rel="stylesheet" type="text/css">
</head>
<body>
<form method="POST">
  <?=getStageSelect()?>
  <input type="submit" value="Write Log">
</form>
</body>
</html>

<?php
}
intranet_closedb();
?>