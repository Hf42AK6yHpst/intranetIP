<?php
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."addon/check.php");
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_opendb();

$libdb = new libdb();
$libdb->db = $eclass_db;

$sql = "Select RecordID, UserID From SELF_ACCOUNT_STUDENT Where DefaultSA = 'SLP' Order By ModifiedDate desc";
$selfAccountAssoAry = BuildMultiKeyAssoc($libdb->returnResultSet($sql), 'UserID', 'RecordID', $SingleValue=1, $BuildNumericArray=1);

$successAry = array();
foreach ((array)$selfAccountAssoAry as $_studentId => $_selfAccountAry) {
	$_numOfSelfAccount = count((array)$_selfAccountAry);
	
	if ($_numOfSelfAccount > 1) {
		array_shift($_selfAccountAry);
		$sql = "Update SELF_ACCOUNT_STUDENT Set DefaultSA = null Where RecordID IN ('".implode("','", (array)$_selfAccountAry)."') ";
		$successAry[$_studentId] = $libdb->db_db_query($sql);
	}
}

if (in_array(false, $successAry)) {
	echo '<font color="red">FAILED</font>';
}
else {
	echo 'Success';
}

intranet_closedb();
?>