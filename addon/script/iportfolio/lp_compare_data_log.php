<?php

include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
intranet_opendb();

DEFINE("log_path", $intranet_root."/addon/script/iportfolio/lp_data_log");
$skip_compare_field = array("FileID");

$currentTime = date("d/m/y : H:i:s", time());
if(isset($first_file) && isset($second_file))
{
  function loadData($file_name)
  {
    $filepath = log_path.'/'.$file_name;
  
    $fp = fopen($filepath, 'r');
    $contents = fread($fp, filesize($filepath));
    fclose($fp);
    
    $returnArr = unserialize($contents);
    
    return $returnArr;
  }
  
  function getHeader($data_arr)
  {
    $header_arr = array();
    
    if(is_array($data_arr))
    {
      $first_data = current($data_arr);
    
      if(is_array($first_data))
      {
        foreach($first_data AS $colname => $val)
        {
          if(!is_numeric($colname))
          {
            $header_arr[] = $colname;
          }
        }
      }
    }
    
    return $header_arr;
  }
  
  function genHeaderRow($first_header_arr, $sec_header_arr)
  {
    $x = "<tr>";
    
    if(count($first_header_arr) == 0)
    {
      $x .= "<td>&nbsp;</td>";
    }
    else
    {
      for($i=0; $i<count($first_header_arr); $i++)
      {
        $x .= "<td>".$first_header_arr[$i]."</td>";
      }
    }
    
    if(count($sec_header_arr) == 0)
    {
      $x .= "<td>&nbsp;</td>";
    }
    else
    {
      for($i=0; $i<count($sec_header_arr); $i++)
      {
        $x .= "<td>".$sec_header_arr[$i]."</td>";
      }
    }
    
    $x .= "<td>Pass / Fail</td>";
    $x .= "</tr>";
    
    return $x;
  }
  
  function compareHeader($first_header_arr, $sec_header_arr)
  {
    $max_colNo = MAX(count($first_header_arr), count($sec_header_arr));
    
    $result = true;
    for($i=0; $i<$max_colNo; $i++)
    {
      if($first_header_arr[$i] != $sec_header_arr[$i])
      {
        $result = false;
        break;
      }
    }
    
    return $result;
  }
  
  function genCompareTableContent($first_header_arr, $first_file_data, $sec_header_arr, $sec_file_data)
  {
    global $skip_compare_field;
  
    if(!compareHeader($first_header_arr, $sec_header_arr))
    {
      $x = "<tr><td colspan=\"".(count($first_header_arr)+count($sec_header_arr))."\" style=\"color: red\">Header Not Match!!</td></tr>";
    }
    else
    {
      $unify_header = $first_header_arr;
      
      $max_rowNo = max(count($first_file_data), count($sec_file_data));
      
      $x = "";
      for($i=0; $i<$max_rowNo; $i++)
      {
        $x .= "<tr>";
      
        for($j=0; $j<count($unify_header); $j++)
        {
          $y[] = "<td>".$first_file_data[$i][$unify_header[$j]]."</td>";
          $z[] = "<td>".$sec_file_data[$i][$unify_header[$j]]."</td>";
        
          $match[$j] = (in_array($unify_header[$j], $skip_compare_field) || $first_file_data[$i][$unify_header[$j]] == $sec_file_data[$i][$unify_header[$j]]);
        }
        
        if(is_array($y))
        {
          $x .= implode("\n", $y);
        }
        if(is_array($z))
        {
          $x .= implode("\n", $z);
        }
        $x .= (is_array($match) && count($unify_header) == count(array_filter($match))) ? "<td style=\"color: green\">Pass</td>" : "<td style=\"color: red\">Fail</td>";
        
        $x .= "</tr>";
        
        unset($y);
        unset($z);
        unset($match);
      }
    }
    
    return $x;
  }
  
  $first_file_data = loadData($first_file);
  $sec_file_data = loadData($second_file);
  
  $first_header_arr = getHeader($first_file_data);
  $first_colNo = count($first_header_arr);
  $sec_header_arr = getHeader($sec_file_data);
  $sec_colNo = count($sec_header_arr);
  
  $header_row_html = genHeaderRow($first_header_arr, $sec_header_arr);
  $main_table_html = genCompareTableContent($first_header_arr, $first_file_data, $sec_header_arr, $sec_file_data);
}


function getFileList()
{
  global $intranet_root;

  $handle = opendir(log_path);
  
  $file_list = array();
  while ($file = readdir($handle))
  {
    if($file == "." || $file == "..") continue;
    if(substr($file, -3) != "log") continue;
    
    $file_list[] = $file;
  }
  rsort($file_list);

  return $file_list;
}

function genFileSelection($file_arr, $select_name, $default_select)
{
  $t_file_arr = array();
  for($i=0; $i<count($file_arr); $i++)
  {
    $t_file_arr[] = array($file_arr[$i], $file_arr[$i]);
  }

  return getSelectByArray($t_file_arr, "name=\"$select_name\"", $default_select, 0, 0, "", 2);
}

$first_file_arr = getFileList();
$first_file_selection = genFileSelection($first_file_arr, "first_file", $first_file);

$sec_file_arr = getFileList();
$sec_file_selection = genFileSelection($sec_file_arr, "second_file", $second_file);

?>

<html>
<head>
<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv='content-type' content='text/html; charset=<?=$html_charset?>' />
<link href="css/ipf.css" rel="stylesheet" type="text/css">
</head>
<body>
<font color = "blue"><b>Current Time : <?= $currentTime?></b></font>&nbsp;&nbsp;[<a href = "<?=$_SERVER["PHP_SELF"]?>">Reset Page</a>]<br/> 
<form method="POST">
<table width="100%" border="1" cellpadding="3" cellspacing="0">
  <tr>
    <td width="45%" colspan="<?=$first_colNo?>"><?=$first_file_selection?></td>
    <td width="45%" colspan="<?=$sec_colNo?>"><?=$sec_file_selection?></td>
    <td width="10%"><input type="submit" value="Compare" /></td>
  </tr>
  <?=$header_row_html?>
  <?=$main_table_html?>
</table>
</form>

</body>
</html>