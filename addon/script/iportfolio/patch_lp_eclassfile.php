<?php
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."addon/check.php");
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
intranet_opendb();

$libdb = new libdb();

$sql = "Select * From $eclass_db.course Where RoomType = 4";
$dataAry = $libdb->returnResultSet($sql);
$courseId = $dataAry[0]['course_id'];
$ipf_db = $eclass_prefix."c".$courseId;
$sql = "
		SELECT 
			COUNT(*) 
		FROM 
			".$ipf_db.".eclass_file f INNER JOIN ".$ipf_db.".usermaster u ON f.User_id = u.user_id 
		WHERE 
			f.Category = 7 
		AND f.IsDir = 1 
		AND u.memberType = 'S' 
		AND f.memberType != u.membertype
		AND f.UserEmail != u.user_email
		AND f.Title != '' 
		AND f.UserEmail IS NOT NULL 
		AND f.UserEmail != ''
";
$studentFileCnt = current($libdb->returnVector($sql));

$sql = "
		SELECT 
			COUNT(*) 
		FROM 
			".$ipf_db.".eclass_file f INNER JOIN ".$ipf_db.".usermaster u ON f.User_id = u.user_id 
		WHERE 
			f.Category = 7 
		AND f.IsDir = 1 
		AND f.memberType = 'T' 
		AND f.memberType != u.membertype
		AND f.UserEmail != u.user_email
		AND f.Title != '' 
		AND f.UserEmail IS NOT NULL 
		AND f.UserEmail != ''
";
$teacherFileCnt = current($libdb->returnVector($sql));

if($studentFileCnt!=$teacherFileCnt){
	$successAry['matchFileCntResult'] = false;
}else{

$sql = "UPDATE 
				".$ipf_db.".eclass_file f INNER JOIN ".$ipf_db.".usermaster u ON f.User_id = u.user_id 
		SET
			f.UserEmail = u.user_email,
			f.memberType = u.membertype 
		WHERE 
			f.Category = 7 
		AND f.IsDir = 1 
		AND u.memberType = 'S' 
		AND f.memberType != u.membertype
		AND f.UserEmail != u.user_email
		AND f.Title != ''
		AND f.UserEmail IS NOT NULL 
		AND f.UserEmail != ''
";
$successAry[] = $libdb->db_db_query($sql);
}

if (in_array(false, (array)$successAry)) {
	echo "FAILED";
}
else {
	echo "Success";
}

intranet_closedb();
?>