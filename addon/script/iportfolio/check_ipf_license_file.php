<?php

include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/portfolio25/libpf-accountmanager.php");
include_once("../../../includes/libpf-sturec.php");
intranet_opendb();

$objDB = new libdb();
$objIPF = new libpf_accountmanager(); 
$li_pf = new libpf_sturec();

$objIPF->SET_LICENSE_KEYS();
$free_license = $objIPF->GET_FREE_LICENSE();

debug_pr('free licenses:');
debug_pr($free_license);


$file_name = ($_GET['filename']=='')? 'keys.dat' : $_GET['filename'];
$keys = $li_pf->GET_KEY_LICENSE($li_pf->file_path."/".$file_name);

debug_pr('Key File = '.$file_name);
debug_pr('all licenses:');
debug_pr($keys);

intranet_closedb();
?>