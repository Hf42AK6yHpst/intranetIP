<?php

include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../..//includes/libportfolio.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("../../../lang/iportfolio_lang.$intranet_session_language.php");
include_once("../../check.php");

intranet_opendb();

global $Lang, $PATH_WRT_ROOT, $LAYOUT_SKIN;

$liport = new libportfolio();
$liport->ACCESS_CONTROL("student_info");

$result = $liport->returnStudentLicenseSummary();

$x = '<p style="font-size: 20px;">'.$ec_iPortfolio['Activated_Liense_Summary'].'</p>';
$x .= '<table width="60%" border="1px" cellspacing="0" cellpadding="4">';
	$x .= '<tr>';
		$x .= '<td width="45%" style="border-right-width: 0px;">'.$ec_iPortfolio['Activated_Liense_Summary_current'].'</td>';
		$x .= '<td style="border-width: 1px 1px 1px 0px; border-style: solid;"> <span style="width:0px;"></span> </td>';
	$x .= '</tr>';
	$x .= '<tr>';
		$x .= '<td >'.$ec_iPortfolio['Active'].' : </td><td ><a href="check_license_student_details.php?target=1">'.$result[0].'</a></td>';
	$x .= '</tr>';
	$x .= '<tr>';
		$x .= '<td >'.$ec_iPortfolio['Activated_Liense_Summary_inactive'].' : </td><td><a href="check_license_student_details.php?target=2">'.$result[1].'</a></td>';
	$x .= '</tr>';
	$x .= '<tr>';
		$x .= '<td  style="border-width:1px 1px 2px 1px; border-style: solid;">'.$ec_iPortfolio['Activated_Liense_Summary_total_amount'].' : </td><td style="border-bottom-width:2px; border-style: solid;">'.($result[0]+$result[1]).'</td>';
	$x .= '</tr>';
	$x .= '<tr height="30px"> </tr>';
	$x .= '<tr>';
		$x .= '<td style="border-width: 2px 0px 1px 1px; border-style: solid;">'.$ec_iPortfolio['Activated_Liense_Summary_without_class'].'</td>';
		$x .= '<td style="border-width: 2px 1px 1px 0px; border-style: solid;"><span style="width:0px;"></span></td>';
	$x .= '</tr>';
	$x .= '<tr>';
		$x .= '<td cnowrap="nowrap">'.$ec_iPortfolio['Active'].' : </td><td cnowrap="nowrap"><a href="check_license_student_details.php?target=3">'.$result[2].'</a></td>';
	$x .= '</tr>';
	$x .= '<tr>';
		$x .= '<td>'.$ec_iPortfolio['Activated_Liense_Summary_inactive'].' : </td><td class="row_off tabletext" nowrap="nowrap"><a href="check_license_student_details.php?target=4">'.$result[3].'</a></td>';
	$x .= '</tr>';
	$x .= '<tr>';
		$x .= '<td style="border-bottom-width:2px; border-style: solid;">'.$ec_iPortfolio['Activated_Liense_Summary_total_amount'].' : </td><td nowrap="nowrap" style="border-bottom-width:2px; border-style: solid;">'.($result[2]+$result[3]).'</td>';
	$x .= '</tr>';
	$x .= '<tr height="30px"></tr>';
	$x .= '<tr>';
		$x .= '<td nowrap="nowrap" style="border-width: 2px 0px 1px 1px; border-style: solid;">'.$ec_iPortfolio['Activated_Liense_Summary_alumni'].'</td>';
		$x .= '<td nowrap="nowrap" style="border-width: 2px 1px 1px 0px; border-style: solid;"><span style="width:0px;"></span></td>';
	$x .= '</tr>';
	$x .= '<tr>';
		$x .= '<td nowrap="nowrap">'.$ec_iPortfolio['Activated_Liense_Summary_total_amount'].' : </td><td class="row_off tabletext nowrap="nowrap"><a href="check_license_student_details.php?target=5">'.$result[4].'</a></td>';
	$x .= '</tr>';
$x .= '</table>';										

echo $x;

intranet_closedb();
?>