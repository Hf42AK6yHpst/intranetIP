<?php
// using 
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($eclass_filepath.'/addon/check.php');
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libelibrary_install.php");

intranet_opendb();

$li = new libdb();

/*
There is some data have to be KEPT and transfered from original version including:
1.) Notes (in db table: INTRANET_ELIB_USER_MYNOTES)  
2.) Reading Progress (in db table: INTRANET_ELIB_USER_PROGRESS)
3.) Reviews (in db tables: INTRANET_ELIB_BOOK_REVIEW & INTRANET_ELIB_BOOK_REVIEW_HELPFUL)
4.) Recommend (in db tables: INTRANET_ELIB_BOOK_RECOMMEND)
5.) Other statistics (not sure which tables..)
My Favorite

NOT KEPT:
1.) Bookmarks (in db tables: INTRANET_ELIB_USER_BOOKMARK)
2.) Highlight (in db tables: INTRANET_ELIB_USER_FORMAT)

+--------------------------------------+
| Tables_in_emeeting_intranet (%ELIB%) |
+--------------------------------------+
| INTRANET_ELIB_BOOK                   |
| INTRANET_ELIB_BOOK_AUTHOR            |
| INTRANET_ELIB_BOOK_CHAPTER           |
| INTRANET_ELIB_BOOK_HISTORY           |
| INTRANET_ELIB_BOOK_MY_FAVOURITES     |
| INTRANET_ELIB_BOOK_PAGE              |
| INTRANET_ELIB_BOOK_PARAGRAPH         |
| INTRANET_ELIB_BOOK_RECOMMEND         |
| INTRANET_ELIB_BOOK_REVIEW            |
| INTRANET_ELIB_BOOK_REVIEW_HELPFUL    |
| INTRANET_ELIB_BOOK_SETTINGS          |
| INTRANET_ELIB_USER_BOOKMARK          |
| INTRANET_ELIB_USER_FORMAT            |
| INTRANET_ELIB_USER_MYNOTES           |
| INTRANET_ELIB_USER_PROGRESS          |
+--------------------------------------+
*
*
**/


# book title, old book ID, new book ID)
$BooksArr[] = array("A love for life",667,746);
$BooksArr[] = array("A matter of chance",648,728);
$BooksArr[] = array("A puzzle for logan",642,722);
$BooksArr[] = array("A Tangled Web",665,744);
$BooksArr[] = array("All I want",677,592);
$BooksArr[] = array("Different Worlds",598,683);
$BooksArr[] = array("Dolphin Music",600,685);
$BooksArr[] = array("Don't Stop Now",601,686);
$BooksArr[] = array("Emergency murder",603,688);
$BooksArr[] = array("Eye of the storm",604,689);
$BooksArr[] = array("Frozen Pizza and other slices",605,690);
$BooksArr[] = array("He knows too much",668,747);
$BooksArr[] = array("Help!",606,691);
$BooksArr[] = array("Hotel Casanova",607,695);
$BooksArr[] = array("How I met myself",643,723);
$BooksArr[] = array("In the shadow of the mountain",651,731);
$BooksArr[] = array("Jungle love",610,698);
$BooksArr[] = array("Just good friends",660,739);
$BooksArr[] = array("Just like a movie",611,699);
$BooksArr[] = array("Murder maker",613,701);
$BooksArr[] = array("Next Door To Love",614,702);
$BooksArr[] = array("Staying together",617,705);
$BooksArr[] = array("SuperBird",618,706);
$BooksArr[] = array("The ironing man",624,712);
$BooksArr[] = array("The Lahti File",646,726);
$BooksArr[] = array("The way home",627,715);
$BooksArr[] = array("Trumpet Voluntary",671,754);
$BooksArr[] = array("Two Lives",647,727);
$BooksArr[] = array("When Summer comes",650,730);
$BooksArr[] = array("Withn High Fences",659,738);

//debug_r($BooksArr);


# step check if new book exists (otherwise, terminate)
//$sql = "SELECT count(*) FROM INTRANET_ELIB_BOOK WHERE BOOKID IN (667, 600)";  // for development
$sql = "SELECT count(*) FROM INTRANET_ELIB_BOOK WHERE BOOKID IN (688, 754)";  // real

$BooksDB = $li->returnVector($sql);
if (sizeof($BooksDB)==1 && $BooksDB[0]==2)
{
	## Init Library
	$objElib = new elibrary_install();

	for ($i=0; $i<sizeof($BooksArr); $i++)
	{
		list($BkTitle, $BkOldID, $BkNewID) = $BooksArr[$i];

		if ($plugin['eLib_license']==1)
		{
			# check if the book is enabled (if not, nothing to update)
			if(trim($objElib->check_book_enabled($BkOldID))!="")
			{
				$objElib->enable_site_book_license($BkNewID, -1);
				# disable old book
				$objElib->disable_site_book_license($BkOldID);
			}
			
			
			# SET OLD BOOK TO UNPUBLISHED
			$sql = "UPDATE INTRANET_ELIB_BOOK SET Publish=0 WHERE BookID={$BkOldID} ";
			$li->db_db_query($sql);
			
			#remove from INTRANET_ELIB_USER_BOOKMARK
			$sql = "UPDATE INTRANET_ELIB_USER_BOOKMARK SET BookID={$BkNewID} WHERE BookID={$BkOldID} ";
			$li->db_db_query($sql);
			
			# update INTRANET_ELIB_BOOK_REVIEW , INTRANET_ELIB_BOOK_REVIEW_HELPFUL ,  INTRANET_ELIB_USER_MYNOTES, INTRANET_ELIB_USER_PROGRESS
			# INTRANET_ELIB_BOOK_MY_FAVOURITES, INTRANET_ELIB_BOOK_RECOMMEND, INTRANET_ELIB_BOOK_HISTORY
			$sql = "UPDATE INTRANET_ELIB_BOOK_REVIEW SET BookID={$BkNewID} WHERE BookID={$BkOldID} ";
			$li->db_db_query($sql);
			
			$sql = "UPDATE INTRANET_ELIB_BOOK_REVIEW_HELPFUL SET BookID={$BkNewID} WHERE BookID={$BkOldID} ";
			$li->db_db_query($sql);
			
			$sql = "UPDATE INTRANET_ELIB_USER_MYNOTES SET BookID={$BkNewID} WHERE BookID={$BkOldID} ";
			$li->db_db_query($sql);
			
			# if the note is marked to a page which is no longer exist (i.e. greater than last page number of new book)
			# change it to book note
			$sql2 = "SELECT MAX(pageid) FROM INTRANET_ELIB_BOOK_PAGE WHERE pagetype='page' AND BookID={$BkOldID}"; # tOt CHANGE!!!
			$PageMax = $li->returnVector($sql2);
			
			//$sql2 = "SELECT NoteID FROM INTRANET_ELIB_USER_MYNOTES";
			
			$sql = "UPDATE INTRANET_ELIB_USER_MYNOTES SET pageid=0, notetype='na' WHERE BookID={$BkNewID} AND pageid>".$PageMax[0]." AND ";
			$li->db_db_query($sql);
			
			$sql = "UPDATE INTRANET_ELIB_USER_PROGRESS SET BookID={$BkNewID} WHERE BookID={$BkOldID} ";
			$li->db_db_query($sql);
			
			$sql = "UPDATE INTRANET_ELIB_BOOK_MY_FAVOURITES SET BookID={$BkNewID} WHERE BookID={$BkOldID} ";
			$li->db_db_query($sql);
			
			$sql = "UPDATE INTRANET_ELIB_BOOK_RECOMMEND SET BookID={$BkNewID} WHERE BookID={$BkOldID} ";
			$li->db_db_query($sql);
			
			$sql = "UPDATE INTRANET_ELIB_BOOK_HISTORY SET BookID={$BkNewID} WHERE BookID={$BkOldID} ";
			$li->db_db_query($sql);
		} else
		{
			# no need to do as there is no such selling done
			# update INTRANET_ELIB_BOOK_STUDENT 
		}
		# update user record to new books
		
		# for notes, update to book note if the page no is greater than new page number
		
		echo ($i+1).". <b>$BkTitle</b> is updated!<br />";
	}
} else
{
	echo "There is no revised books of CUP! Please ask solution to copy.";
}


intranet_closedb();
?>