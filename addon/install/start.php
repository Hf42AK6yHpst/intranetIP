<?php
include("../../includes/global.php");
include("../../includes/libdb.php");
include("../../includes/libfilesystem.php");
include("../../includes/SQLintranet.php");
include("../../includes/libaccount.php");
include("../check.php");
intranet_opendb();
$li = new libdb();

function build_category($thisTmp){
     # Create Sample Data
     $thisTmp->db_db_query("DELETE FROM INTRANET_GROUP");
     $thisTmp->db_db_query("DELETE FROM INTRANET_ROLE");
     $thisTmp->db_db_query("DELETE FROM INTRANET_BOOKING_PERIOD");
     $thisTmp->db_db_query("DELETE FROM INTRANET_GROUP_CATEGORY");
     $sql  = "INSERT INTO INTRANET_GROUP (Title, Description, RecordType, RecordStatus, DateInput, DateModified) VALUES
                    ('Teacher', '教師', 0, 1, now(), now()),
                    ('Student', '學生', 0, 1, now(), now()),
                    ('Admin Staff', '職員', 0, 1, now(), now()),
                    ('Parent', '家長', 0, 1, now(), now())";
     $thisTmp->db_db_query($sql);
     echo "<p>".$sql."\n";
     $sql = "INSERT INTO INTRANET_GROUP_CATEGORY (CategoryName,RecordType,DateInput,DateModified)
                    VALUES ('Identity 基本組別',1,now(),now())
                    ,('Admin 行政',1,now(),now())
                    ,('Academic 學術',1,now(),now())
                    ,('Class 班別',1,now(),now())
                    ,('House 社級',1,now(),now())
                    ,('ECA 課外活動',1,now(),now())
                    ,('Miscellaneous 其他',0,now(),now())
            ";
     $thisTmp->db_db_query($sql);
     echo "<p>".$sql."\n";

     # Change the ID to be compatible with fixed type (old version)
     $sql = "UPDATE INTRANET_GROUP_CATEGORY SET GroupCategoryID = GroupCategoryID - 1";
     $thisTmp->db_db_query($sql);
     echo "<p>".$sql."\n";

     $sql  = "INSERT INTO INTRANET_ROLE (Title, Description, RecordType, RecordStatus, DateInput, DateModified) VALUES
                    ('Principal', '校長', 0, 0, now(), now()),
                    ('Vice Principal', '副校長', 0, 0, now(), now()),
                    ('Master', '主任', 0, 0, now(), now()),
                    ('Teacher', '教師', 0, 0, now(), now()),
                    ('Secretary', '書記', 0, 0, now(), now()),
                    ('Union Head', '學生會會長', 0, 0, now(), now()),
                    ('Union Head', '家長會會長', 0, 0, now(), now()),
                    ('Member', '會員', 0, 1, now(), now()),
                    ('Secretary', '書記', 1, 1, now(), now()),
                    ('Panel Head', '科主任', 2, 0, now(), now()),
                    ('Convener', '聯絡人', 2, 0, now(), now()),
                    ('Member', '會員', 2, 1, now(), now()),
                    ('Form Master', '班主任', 3, 0, now(), now()),
                    ('Asso. Form Master', '副班主任', 3, 0, now(), now()),
                    ('Monitor', '班長', 3, 0, now(), now()),
                    ('Student', '學生', 3, 1, now(), now()),
                    ('House Counselor', '社監', 4, 0, now(), now()),
                    ('House Captain', '社長', 4, 0, now(), now()),
                    ('Member', '會員', 4, 1, now(), now()),
                    ('Chairman', '會長', 5, 0, now(), now()),
                    ('Vice Chairman', '副會長', 5, 0, now(), now()),
                    ('Member', '會員', 5, 1, now(), now()),
                    ('Member', '會員', 6, 1, now(), now())
               ";
     $thisTmp->db_db_query($sql);
     echo "<p>".$sql."\n";
     /*
     $sql  = "INSERT INTO INTRANET_BOOKING_PERIOD (TimeStart, TimeEnd, Title) VALUES (28800, 32400, 'Period 1'), (32400, 36000, 'Period 2'), (36000, 39600, 'Period 3'), (39600, 43200, 'Period 4'), (43200, 46800, 'Period 5'), (46800, 50400, 'Period 6'), (50400, 54000, 'Period 7'), (54000, 57600, 'Period 8'), (57600, 61200, 'Period 9'), (61200, 64800, 'Period 10'), (64800, 68400, 'Period 11'), (68400, 72000, 'Period 12')";
     $thisTmp->db_db_query($sql);
     echo "<p>".$sql."\n";
*/
     $thisTmp->db_db_query("DELETE FROM INTRANET_SLOT");
     $thisTmp->db_db_query("DELETE FROM INTRANET_SLOT_BATCH");
     $sql = "INSERT INTO INTRANET_SLOT_BATCH (Title, RecordStatus) VALUES ('Standard', '1')";
     $thisTmp->db_db_query($sql);
     echo "<p>".$sql."\n";
     $sql = "INSERT INTO INTRANET_SLOT (BatchID, Title, TimeRange) VALUES ('1','Period 1','08:00 - 09:00'),('1','Period 2','09:00 - 10:00'),('1','Period 3','10:00 - 11:00'),('1','Period 4','11:00 - 12:00'),('1','Period 5','12:00 - 13:00'),('1','Period 6','13:00 - 14:00'),('1','Period 7','14:00 - 15:00'),('1','Period 8','15:00 - 16:00'),('1','Period 9','16:00 - 17:00'),('1','Period 10','17:00 - 18:00'),('1','Period 11','18:00 - 19:00'),('1','Period 12','19:00 - 20:00')";
     $thisTmp->db_db_query($sql);
     echo "<p>".$sql."\n";
     $sql = "UPDATE INTRANET_SLOT SET SlotSeq = SlotID WHERE BatchID = 1";
     $thisTmp->db_db_query($sql);
     echo "<p>".$sql."\n";


     # Staff Attendance
     $sql = "INSERT INTO CARD_STAFF_ATTENDANCE_ADMIN (Username,Password) VALUES ('admin','eclassstaff')";
     $thisTmp->db_db_query($sql);


     # Sports System
     $sql = "INSERT INTO SPORTS_SYSTEM_SETTING () VALUES ()";
     $thisTmp->db_db_query($sql);

     $sql = "INSERT INTO SPORTS_EVENT_TYPE_NAME (EventTypeID, EnglishName, ChineseName, DateInput, DateModified)
                    VALUES (1, 'Track','徑賽',now(),now()),
                           (2, 'Field','田賽',now(),now()),
                           (3, 'House Relay','社際接力',now(),now()),
						   (4, 'Class Relay','班際接力',now(),now())
                           ";
     $thisTmp->db_db_query($sql);
	 
	 # Swimming Gala
     $sql = "INSERT INTO WATERGALA_EVENT_TYPE_NAME (EventTypeID, EnglishName, ChineseName, DateInput, DateModified)
                    VALUES (1, 'Track','徑賽',now(),now()),
                           (2, 'Field','田賽',now(),now()),
                           (3, 'House Relay','社際接力',now(),now()),
						   (4, 'Class Relay','班際接力',now(),now())
                           ";
     $thisTmp->db_db_query($sql);


}
?>
<html><head><title>
script : create intranet database
</title></head><body>
<h1>script : create intranet database</h1>

<?php
# Build Category
if($category==1){
     build_category($li);
}


# Create Database

if ($li->db_create_db($intranet_db)){
     print ("Database created successfully\n");
     for($i=0; $i<sizeof($sql_table); $i++){
          $sql = $sql_table[$i];
          $li->db_db_query($sql);
          echo "<p>".$sql."\n";
     }
     # Create Sample Data
     build_category($li);
} else {
     echo ("<p>Error creating database: %s\n" . mysql_error ());
     # update new tables
     for($i=0; $i<sizeof($sql_table); $i++){
          $sql = $sql_table[$i];
          if($li->db_db_query($sql)){
               echo "<p>Update new tables: ".$sql."\n";
          }
     }
}

# script : create file directory if folder does not exist
$lo = new libfilesystem();
$lo->folder_new($intranet_root."/file/timetable");
$lo->folder_new($intranet_root."/file/resource");
$lo->folder_new($intranet_root."/file/photo");
$lo->folder_new($intranet_root."/file/photo/personal");
$lo->folder_new($intranet_root."/file/mail");
$lo->folder_new($intranet_root."/file/group");
$lo->folder_new($intranet_root."/file/user");
$lo->folder_new($intranet_root."/file/export");
$lo->folder_new($intranet_root."/file/announcement");
$lo->folder_new($intranet_root."/file/qb");
$lo->folder_new($intranet_root."/file/studentfiles");
$lo->folder_new($intranet_root."/file/templates");

# script : create intranet htpasswd & htgroup
$lu = new libaccount();
$lu->make_htpasswd();
$lu->make_htgroup();
?>
<br><br>
============ Completed =============

</body>
</html>
<?php intranet_closedb(); ?>