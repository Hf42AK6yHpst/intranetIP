<?php
// using : 
/*
 * 	Log
 * 	
 * 	2016-09-15 Cameron
 * 		- set memory to unlimit so that all books can be synchronized
 */
@SET_TIME_LIMIT(216000);
@ini_set('memory_limit', -1);

$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libelibrary.php");

intranet_opendb();


$lbelib = new elibrary();

$lbelib->SYNC_PHYSICAL_BOOK_TO_ELIB("", true);

intranet_closedb();
?>