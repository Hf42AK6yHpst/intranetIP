<?php
	$PATH_WRT_ROOT = '../../';
	include_once($PATH_WRT_ROOT.'includes/global.php');
	include_once($PATH_WRT_ROOT.'includes/libdb.php');
	include_once($PATH_WRT_ROOT.'includes/pagamo/libpagamo_install.php');
	include_once("../check.php");
	
	intranet_opendb();
	$libpagamoinstall = new pagamo_install();
?>
<!DOCTYPE html>
<html>
<head>
<style>
	body{
		background-color: grey;
	}
		
	.import_table{
		width:600px; height:auto; margin: 0 auto;	
		padding: 20px;	
		background-color: white;
		border-style:solid;
		border-width:1px;
	}
	
	#import_form{
		width:600px;
  		 display: inline-block;		
	}
	
	#quotation_table{
  		
	}
	
	input, textarea {   
	    padding: 9px;  
	    border: solid 1px #E5E5E5;  
	    outline: 0;  
	    font: normal 13px/100% Verdana, Tahoma, sans-serif;  
	    width: 200px;  
	    background: #FFFFFF;  
	    }  	 
	  
	textarea {   
	    width: 400px;  
	    max-width: 400px;  
	    height: 150px;  
	    line-height: 150%;  
	    }  
	  
	input:hover, textarea:hover,  
	input:focus, textarea:focus {   
	    border-color: #C9C9C9;   
	    }  
	  
	.form label {   
	    margin-left: 10px;   
	    color: #999999;   
	    }  
	  
	.submit input {  
	    width: auto;  
	    padding: 9px 15px;  
	    background: #617798;  
	    border: 0;  
	    font-size: 14px;  
	    color: #FFFFFF;  
    }  

	div label{
		padding-right: 40px;
	}
	
	#quotation_table{
		border-collapse: separate;
		border-spacing: 0px;
		font-size: 16px;
		text-align: center;
		font-family:Arial,Helvetica,sans-serif;
	}
	#quotation_table tr th {
		margin: 0px;
		padding: 3px;
		padding-top: 5px;
		padding-bottom: 5px;
		background-color: #A6A6A6;
		font-weight: normal;
		color: #FFFFFF;
		border-bottom: 1px solid #CCCCCC;
		border-right: 1px solid #CCCCCC;
	}
	
	#quotation_table td {
		padding-top: 5px;
		padding-bottom: 5px;
	}
</style>
<script src="jquery.min.js"></script>
<script>
	$("document").ready(function(){		
		$("#installation_type").change(function(){
			if($(this)[0].selectedIndex == 0){
				$(".quotation_book_count").css("display", "none");
				$("#quotation_book_count").val("");
			}else{
				$(".quotation_book_count").css("display", "");
			}
		});		
		$(".delete_btn").live("click", function(){
			if(confirm("Delete this quotation ?")){				
				var quotation_id = $(this).attr('value');
				$(".quotation_id").val(quotation_id);
				//$.post('ebook_uninstall.php', { quotation_id: quotation_id });
				$("#quotation_form").submit();
			}
		});
		
		$("#period_setting").live("change", function(){
			$(".license_period").html($("#license_period_template" +$(this).val() ).html());
		});
		
	});

	function formCheck(){
		if($('#quotation_number').val()==""){
			alert('please input a quotation number');
			return false;
		}
		<?php 
		if($sys_custom['non_eclass_PaGamO']==true){
		?>
		if($('#school_code').val()==""){
			alert('please input a proper school code');
			return false;
		}
		if($('#school_name').val()==""){
			alert('please input a proper schoo name');
			return false;
		}
		<?php 
		}
		?>	
		if($('#period_from').val()==""){
			alert('please input a proper start date');
			return false;
		}
		if($('#period_to').val()==""){
			alert('please input a proper end date');
			return false;
		}
		if(new Date($('#period_from').val()).getTime() > new Date($('#period_to').val()).getTime()){
			alert('End date should be later than start date');
			return false;
		}
		return true;
	}
</script>
</head>
<body>
	<div class="import_table" style="">			
		<form id="import_form" class="form" action="pagamo_install.php" enctype="multipart/form-data" accept-charset="UTF-8" method="post" onsubmit="return formCheck()">
			<h3 style="width: auto;   background-color: #eeeeee;   margin: 5px;">New PaGamO Quotation</h3>
			<br/>
			<div class="quotation">
				<label for="quotation_number">Quotation #</label>
					<span style=" margin-left: 34px;"><input type="text" name="quotation_number" id="quotation_number" /></span>
			</div>
			
			<br/>
			<?php 
			if($sys_custom['non_eclass_PaGamO']==true){
			?>		
			<div class="schoolCode">
				<label for="school_code">School Code</label>
					<span style=" margin-left: 34px;"><input type="text" name="school_code" id="school_code" /></span>
			</div>
			
			<br/>
			<div class="schoolName">
				<label for="school_name">School Name <br> (中文全寫)</label>
					<span style=" margin-left: 34px;"><input type="text" name="school_name" id="school_name"/></span>
			</div>
			
			<br/>
			<?php 
			}
			?>
						
			<div class="classLevel">
				<label for="class_level">Class Level</label> 
				<select style=" margin-left: 25px;" class="class_level" id="class_level" name="class_level">
					<?=$libpagamoinstall->getClassLevelOptions();?>
				</select>		
			</div>
			<br/>		
			
			<div class="std_quota">
				<label for="pagamo_std_quota"># of Student Quota</label>
					<span style=" margin-left: 35px;">
						<input min="1"  type="number" name="pagamo_std_quota" id="pagamo_std_quota" value="120"/>
					</span>
			</div>
			<br/>
					
			<div class="tch_quota">
				<label for="pagamo_tch_quota"># of Teacher Quota</label>
					<span style=" margin-left: 35px;">
						<input min="1"  type="number" name="pagamo_tch_quota" id="pagamo_tch_quota" value="10"/>
					</span>
			</div>
			<br/>
			
			<div class="license_period">
				<label for="period_from">License period </label> 
				<span style=" margin-left: 16px;">from <input type="date" name="period_from" id="period_from" value="<?=date('Y-m-d')?>"/> <br/></span>	
				<span style=" margin-left: 158px;">to <input type="date" name="period_to" id="period_to" value="<?=date('Y-m-d', strtotime('+1 year -1 day'))?>" /></span>				
			</div>
			<br/>
			
			<!--<div class="purchase_date">
				<label for="purchase_date">Purchase Date </label> 
				<span style=" margin-left: 16px;">in <input type="date" name="purchase_date" id="purchase_date" /> <br/></span>	
			</div>-->
			<br/>
				
			<div class="submit">
				<input type="submit" value="Send" />
			</div>
		
		</form>		
		
		<div style="display:none" id="license_period_template1">
				<label >License period </label> 
				<span  style=" margin-left: 35px;">--</span>	
		</div>	
			
		<div style="display:none" id="license_period_template2">
			<label for="period_from">License period </label> 
				<span style=" margin-left: 16px;">from <input type="date" name="period_from" id="period_from" /> <br/></span>	
				<span style=" margin-left: 158px;">to <input type="date" name="period_to" id="period_to" /></span>							
		</div>	
		
			<br/>
			<br/>
			<br/>	
		<form id="quotation_form" action="pagamo_uninstall.php" enctype="multipart/form-data" accept-charset="UTF-8" method="post">
			<input type="hidden"  class="quotation_id" name="quotation_id" />
		</form>			
		<span>-----------------------------------------------------------------------------------------</span><br/>
		<h3>Current quotation applied</h3><br/>
		<text style="color:red"># in red:</text> Quotation that has user.
		<table style="width: 600px;" id="quotation_table">
			<thead>
				<th>#</th>
				<th style="width:20%">Quotation #</th>
				<th>Class Level</th>
				<th># of Std Quota</th>
				<th># of Tch Quota</th>
				<th style="width:15%">From</th>
				<th style="width:15%">To</th>
				<th style="width:15%">Install Date</th>
				<th>Delete</th>				
			</thead>
			<tbody>
				<?= $libpagamoinstall->retrieve_quotation_install_info(); ?>		
			</tbody>
		</table>			
	</div>
</body>
</html>

<?php
	intranet_closedb();
?>