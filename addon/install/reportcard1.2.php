<?php
# Editing by 
@SET_TIME_LIMIT(1000);
@ini_set('memory_limit', -1);

$eRC_Addon_PATH_WRT_ROOT = ($PATH_WRT_ROOT!="") ? $PATH_WRT_ROOT : "../../";
include_once($eRC_Addon_PATH_WRT_ROOT."includes/global.php");
include_once($eRC_Addon_PATH_WRT_ROOT."includes/libdb.php");
include_once($eRC_Addon_PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($eRC_Addon_PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($eRC_Addon_PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
include_once($eRC_Addon_PATH_WRT_ROOT."includes/libaccount.php");
if (!$CallFromSameDomain)
{
	include_once("../check.php");
}


function updateSchema_eRC($updateMode, $lastSchemaUpdateDate, $schemaInputDate, $schemaDesc, $schemaSql) {
	global $li;
	
	if ($updateMode==2 && strtotime($lastSchemaUpdateDate) > strtotime($schemaInputDate)) {
		
	}
	else {
		echo "<b>[Date: ".$schemaInputDate." deployment]</b><br>";
		echo "<b>[Function: ".$schemaDesc."]</b><br>";
		if (!$li->db_db_query($schemaSql)){
			echo "warning: ".$schemaSql." <br><br>";
		}
		else {
			echo "Ok<br><br>";
		}
	}
}


if ($plugin['ReportCard2008'])
{
	if ($_REQUEST['direct_run'] == 1)
		intranet_opendb();
		
	$li = new libdb();
	$lreportcard = new libreportcardcustom();

	# script : create file directory if folder does not exist
	$lo = new libfilesystem();
	$lo->folder_new($intranet_root."/file/reportcard2008/");
	$lo->folder_new($intranet_root."/file/reportcard2008/templates/");
	
	# create a text file storing current year used for database naming
	if (!file_exists($intranet_root."/file/reportcard2008/db_year.txt")) {
		$eRC_AcademicYear = $lreportcard->GET_ACADEMIC_YEAR();
		$eRC_AcademicYear = ($eRC_AcademicYear=='')? '2016' : $eRC_AcademicYear;
		$lo->file_write($eRC_AcademicYear, $intranet_root."/file/reportcard2008/db_year.txt");
		$lreportcard = new libreportcardcustom();
	}
	else {
		$eRC_AcademicYear = trim(get_file_content($intranet_root."/file/reportcard2008/db_year.txt"));
		if ($eRC_AcademicYear=='' || $eRC_AcademicYear==0 || $eRC_AcademicYear=='-1') {
			$eRC_AcademicYear = $lreportcard->GET_ACADEMIC_YEAR();
			$eRC_AcademicYear = ($eRC_AcademicYear=='')? '2016' : $eRC_AcademicYear;
			$lo->file_write($eRC_AcademicYear, $intranet_root."/file/reportcard2008/db_year.txt");
			$lreportcard = new libreportcardcustom();
		}
	}
	
	?>
	<body>
	<h1>Script : Create eReportCard 1.2 Database & Tables</h1>
	<?php
	# The Suffix should be change according to  different academic year, 
	# e.g. DB_REPORT_CARD_2008, DB_REPORT_CARD_2009
	
	$reportcard_db = $lreportcard->DBName;
	
	# Create Database
	if ($li->db_create_db($reportcard_db)){
		print("<p>Initial Database created successfully</p>\n");
	} else {
		print("<p>Initial Database exist already: ".mysql_error()."</p>\n");
	}
	
	$sql = "CREATE TABLE IF NOT EXISTS RC_LOG (
				LogID int(8) NOT NULL auto_increment,
				Functionality varchar(128) default null,
				UserID int(8) default null,
				LogContent text default null,
				InputDate datetime default null,
				PRIMARY KEY (LogID),
				KEY Functionality (Functionality)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8
			";
	$li->db_db_query($sql);
	
	$sql = "SHOW DATABASES LIKE '%".$intranet_db."_DB_REPORT_CARD_%'";
	$DatabaseAry = $li->returnArray($sql);
	
	for($a=0; $a<sizeof($DatabaseAry);$a++)
	{
		$sql_table = array();
		$sql_alter = array();
		
		$reportcard_db = $DatabaseAry[$a][0];
		$activeYearOfCurrentDB = substr($DatabaseAry[$a][0], -4);
		
		// ignore backup DB like "intranet_DB_REPORT_CARD_2007_UTF82"
		$tmpDBNameArr = explode('_', $reportcard_db);
		$numOfDBNamePart = count($tmpDBNameArr);
		$targetCheckingText = $tmpDBNameArr[$numOfDBNamePart - 1];
		if (strtolower(substr($targetCheckingText, 0, 4)) == 'utf8')
			continue;
		if (strpos($reportcard_db, '_RUBRICS_') !== false)
			continue;	
		
	
		print("<p>Creating tables for ".$DatabaseAry[$a][0]."...</p>\n");
		
		
		$sql_table[] = 
		"CREATE TABLE IF NOT EXISTS $reportcard_db.RC_CLASS_COMMENT_PROGRESS (
		  ClassCommentProgressID int(8) NOT NULL auto_increment,
		  ReportID int(11) default NULL,
		  ClassID int(11) default NULL,
		  TeacherID int(11) default NULL,
		  IsCompleted tinyint(4) default NULL,
		  DateInput datetime default NULL,
		  DateModified datetime default NULL,
		  PRIMARY KEY  (ClassCommentProgressID),
		  KEY ReportID (ReportID),
		  KEY ClassID (ClassID)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8";
		
		$sql_table[] = 
		"CREATE TABLE IF NOT EXISTS $reportcard_db.RC_COMMENT_BANK (
		  CommentID int(11) NOT NULL auto_increment,
		  CommentCode varchar(16) default NULL,
		  CommentCategory varchar(64) default NULL,
		  IsSubjectComment tinyint(4) default NULL,
		  CommentEng varchar(255) default NULL,
		  Comment varchar(255) default NULL,
		  DateInput datetime default NULL,
		  DateModified datetime default NULL,
		  PRIMARY KEY  (CommentID)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8";
		
		$sql_table[] = 
		"CREATE TABLE IF NOT EXISTS $reportcard_db.RC_GRADING_SCHEME (
		  SchemeID int(8) NOT NULL auto_increment,
		  SchemeTitle varchar(128) default NULL,
		  SchemeType char(2) default NULL,
		  FullMark float default NULL,
		  PassMark float default NULL,
		  TopPercentage tinyint(4) default NULL,
		  Pass varchar(16) default NULL,
		  Fail varchar(16) default NULL,
		  DateInput datetime default NULL,
		  DateModified datetime default NULL,
		  PRIMARY KEY  (SchemeID)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8";
		
		$sql_table[] = 
		"CREATE TABLE IF NOT EXISTS $reportcard_db.RC_GRADING_SCHEME_RANGE (
		  GradingSchemeRangeID int(8) NOT NULL auto_increment,
		  SchemeID int(8) default NULL,
		  Nature char(2) default NULL,
		  LowerLimit float default NULL,
		  UpperLimit float default NULL,
		  Grade varchar(4) default NULL,
		  GradePoint float default NULL,
		  DateInput datetime default NULL,
		  DateModified datetime default NULL,
		  PRIMARY KEY  (GradingSchemeRangeID),
		  KEY SchemeID (SchemeID)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8";
		
		$sql_table[] = 
		"CREATE TABLE IF NOT EXISTS $reportcard_db.RC_MARKSHEET_COMMENT (
		  CommentID int(8) NOT NULL auto_increment,
		  StudentID int(11) default NULL,
		  SubjectID int(8) default NULL,
		  ReportID int(11) default NULL,
		  Comment text,
		  TeacherID int(11) default NULL,
		  DateInput datetime default NULL,
		  DateModified datetime default NULL,
		  PRIMARY KEY  (CommentID),
		  KEY StudentID (StudentID),
		  KEY SubjectID (SubjectID)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8";
		
		$sql_table[] = 
		"CREATE TABLE IF NOT EXISTS $reportcard_db.RC_MARKSHEET_FEEDBACK (
		  FeedbackID int(8) NOT NULL auto_increment,
		  SubjectID int(8) default NULL,
		  ReportID int(11) default NULL,
		  StudentID int(11) default NULL,
		  Comment text,
		  IsTeacherRead tinyint(4) default '0',
		  DateInput datetime default NULL,
		  DateModified datetime default NULL,
		  PRIMARY KEY  (FeedbackID),
		  KEY SubjectID (SubjectID),
		  KEY ReportID (ReportID)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8";
		
		$sql_table[] = 
		"CREATE TABLE IF NOT EXISTS $reportcard_db.RC_MARKSHEET_OVERALL_SCORE (
		  MarksheetOverallScoreID int(11) NOT NULL auto_increment,
		  StudentID int(11) default NULL,
		  SubjectID int(11) default NULL,
		  ReportID int(11) default NULL,
		  SchemeID int(11) default NULL,
		  MarkType char(2) default NULL,
		  MarkNonNum varchar(11) default NULL,
		  LastModifiedUserID int(11) default NULL,
		  DateInput datetime default NULL,
		  DateModified datetime default NULL,
		  PRIMARY KEY  (MarksheetOverallScoreID),
		  UNIQUE KEY RC_MARKSHEET_OVERALL_SCORE_UK1 (ReportID,SubjectID,StudentID),
		  KEY StudentID (StudentID),
		  KEY SubjectID (SubjectID),
		  KEY SchemeID (SchemeID)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8";
		
		$sql_table[] = 
		"CREATE TABLE IF NOT EXISTS $reportcard_db.RC_MARKSHEET_SCORE (
		  MarksheetScoreID int(11) NOT NULL auto_increment,
		  StudentID int(11) default NULL,
		  SubjectID int(11) default NULL,
		  ReportColumnID int(11) default NULL,
		  SchemeID int(11) default NULL,
		  MarkType char(2) default NULL,
		  MarkRaw float default '-1',
		  MarkNonNum varchar(11) default NULL,
		  LastModifiedUserID int(11) default NULL,
		  DateInput datetime default NULL,
		  DateModified datetime default NULL,
		  PRIMARY KEY  (MarksheetScoreID),
		  UNIQUE KEY RC_MARKSHEET_SCORE_UK1 (ReportColumnID,SubjectID,StudentID),
		  KEY StudentID (StudentID),
		  KEY SubjectID (SubjectID),
		  KEY SchemeID (SchemeID)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8";
		
		$sql_table[] = 
		"CREATE TABLE IF NOT EXISTS $reportcard_db.RC_MARKSHEET_SUBMISSION_PROGRESS (
		  MarksheetSubmissionProgressID int(11) NOT NULL auto_increment,
		  TeacherID int(11) default NULL,
		  SubjectID int(11) default NULL,
		  ClassID int(11) default NULL,
		  IsCompleted tinyint(4) default NULL,
		  ReportID int(11) default NULL,
		  DateInput datetime default NULL,
		  DateModified datetime default NULL,
		  PRIMARY KEY  (MarksheetSubmissionProgressID),
		  KEY TeacherID (TeacherID),
		  KEY SubjectID (SubjectID)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8";
		
		$sql_table[] = 
		"CREATE TABLE IF NOT EXISTS $reportcard_db.RC_MARKSHEET_VERIFICATION_PROGRESS (
		  VerificationProgressID int(8) NOT NULL auto_increment,
		  SubjectID int(8) default NULL,
		  ReportID int(11) default NULL,
		  StudentID int(11) default NULL,
		  ClassLevelID int(11) default NULL,
		  DateInput datetime default NULL,
		  DateModified datetime default NULL,
		  PRIMARY KEY  (VerificationProgressID),
		  KEY SubjectID (SubjectID),
		  KEY ReportID (ReportID)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8";
		
		$sql_table[] = 
		"CREATE TABLE IF NOT EXISTS $reportcard_db.RC_OTHER_STUDENT_INFO (
		  OtherStudentInfoID int(8) NOT NULL auto_increment,
		  StudentID int(11) default NULL,
		  ReportID int(11) default NULL,
		  CIPHours float default NULL,
		  Attendance tinyint(4) default NULL,
		  Conduct tinyint(4) default NULL,
		  CCA text,
		  NAPFA tinyint(4) default NULL,
		  TeacherID int(11) default NULL,
		  DateInput datetime default NULL,
		  DateModified datetime default NULL,
		  PRIMARY KEY  (OtherStudentInfoID),
		  UNIQUE KEY RC_OTHER_STUDENT_INFO_UK1 (StudentID,ReportID),
		  KEY StudentID (StudentID),
		  KEY ReportID (ReportID)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8";
		
		$sql_table[] = 
		"CREATE TABLE IF NOT EXISTS $reportcard_db.RC_REPORT_RESULT (
		  ResultID int(8) NOT NULL auto_increment,
		  StudentID int(11) default NULL,
		  ReportID int(8) default NULL,
		  ReportColumnID int(11) default NULL,
		  GrandTotal float default NULL,
		  GrandAverage float default NULL,
		  GPA float default NULL,
		  OrderMeritClass int(11) default NULL,
		  OrderMeritForm int(11) default NULL,
		  Promotion tinyint(4) default NULL,
		  Passed tinyint(4) default NULL,
		  NewClassName varchar(100) default NULL,
		  Semester char(2) default NULL,
		  AdjustedBy int(11) default NULL,
		  DateInput datetime default NULL,
		  DateModified datetime default NULL,
		  PRIMARY KEY  (ResultID),
		  KEY StudentID (StudentID),
		  KEY ReportID (ReportID),
		  KEY ReportColumnID (ReportColumnID)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8";
		
		$sql_table[] = 
		"CREATE TABLE IF NOT EXISTS $reportcard_db.RC_REPORT_RESULT_ARCHIVE (
		  ResultID int(8) NOT NULL auto_increment,
		  StudentID int(11) default NULL,
		  ReportID int(8) default NULL,
		  ReportColumnID int(11) default NULL,
		  GrandTotal float default NULL,
		  GrandAverage float default NULL,
		  GPA float default NULL,
		  OrderMeritClass int(11) default NULL,
		  OrderMeritForm int(11) default NULL,
		  Promotion tinyint(4) default NULL,
		  Passed tinyint(4) default NULL,
		  NewClassName varchar(100) default NULL,
		  Semester char(2) default NULL,
		  AdjustedBy int(11) default NULL,
		  DateInput datetime default NULL,
		  DateModified datetime default NULL,
		  PRIMARY KEY  (ResultID),
		  KEY StudentID (StudentID),
		  KEY ReportID (ReportID),
		  KEY ReportColumnID (ReportColumnID)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8";
		
		$sql_table[] = 
		"CREATE TABLE IF NOT EXISTS $reportcard_db.RC_REPORT_RESULT_FULLMARK (
		  ReportResultFullmarkID int(11) NOT NULL auto_increment,
		  StudentID int(11) default NULL,
		  ReportID int(11) default NULL,
		  ReportColumnID int(11) default NULL,
		  SubjectID int(11) default NULL,
		  FullMark float default NULL,
		  Semester char(2) default NULL,
		  DateInput datetime default NULL,
		  DateModified datetime default NULL,
		  PRIMARY KEY  (ReportResultFullmarkID),
		  KEY StudentID (StudentID),
		  KEY SubjectID (SubjectID),
		  KEY ReportID (ReportID),
		  KEY ReportColumnID (ReportColumnID)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8";
		
		$sql_table[] = 
		"CREATE TABLE IF NOT EXISTS $reportcard_db.RC_REPORT_RESULT_SCORE (
		  ReportResultScoreID int(11) NOT NULL auto_increment,
		  StudentID int(11) default NULL,
		  SubjectID int(11) default NULL,
		  ReportID int(11) default NULL,
		  ReportColumnID int(11) default NULL,
		  Mark float default NULL,
		  Grade varchar(8) default NULL,
		  IsOverall tinyint(4) default '0',
		  Semester char(2) default NULL,
		  OrderMeritClass int(11) default NULL,
		  OrderMeritForm int(11) default NULL,
		  DateInput datetime default NULL,
		  DateModified datetime default NULL,
		  PRIMARY KEY  (ReportResultScoreID),
		  KEY StudentID (StudentID),
		  KEY SubjectID (SubjectID),
		  KEY ReportID (ReportID)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8";
		
		$sql_table[] = 
		"CREATE TABLE IF NOT EXISTS $reportcard_db.RC_REPORT_RESULT_SCORE_ARCHIVE (
		  MarksheetConsolidatedID int(11) NOT NULL auto_increment,
		  StudentID int(11) default NULL,
		  SubjectID int(11) default NULL,
		  ReportID int(11) default NULL,
		  ReportColumnID int(11) default NULL,
		  MarkWeighted float default NULL,
		  Grade varchar(8) default NULL,
		  IsOverall tinyint(4) default '0',
		  Semester char(2) default NULL,
		  OrderMeritClass int(11) default NULL,
		  OrderMeritForm int(11) default NULL,
		  DateInput datetime default NULL,
		  DateModified datetime default NULL,
		  PRIMARY KEY  (MarksheetConsolidatedID),
		  KEY StudentID (StudentID),
		  KEY SubjectID (SubjectID),
		  KEY ReportID (ReportID)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8";
		
		$sql_table[] = 
		"CREATE TABLE IF NOT EXISTS $reportcard_db.RC_REPORT_TEMPLATE (
		  ReportID int(11) NOT NULL auto_increment,
		  ReportTitle varchar(255) default NULL,
		  Semester char(2) default NULL,
		  Description varchar(255) default NULL,
		  ClassLevelID int(11) default NULL,
		  HeaderHeight int(2) default NULL,
		  Footer varchar(255) default NULL,
		  LineHeight int(2) default NULL,
		  AllowClassTeacherComment tinyint(1) default NULL,
		  CommentLengthClassTeacher int(2) default NULL,
		  AllowSubjectTeacherComment tinyint(1) default NULL,
		  CommentLengthSubjectTeacher int(2) default NULL,
		  ShowOverallPositionClass tinyint(1) default NULL,
		  ShowOverallPositionForm tinyint(1) default NULL,
		  OverallPositionRangeClass varchar(10) default NULL,
		  OverallPositionRangeForm varchar(10) default NULL,
		  HideNotEnrolled tinyint(1) default NULL,
		  DisplaySettings longblob,
		  PercentageOnColumnWeight tinyint(1) default NULL,
		  MarksheetSubmissionStart datetime default NULL,
		  MarksheetSubmissionEnd datetime default NULL,
		  MarksheetVerificationStart datetime default NULL,
		  MarksheetVerificationEnd datetime default NULL,
		  ShowSubjectFullMark tinyint(1) default NULL,
		  ShowSubjectOverall tinyint(1) default NULL,
		  Issued date default NULL,
		  LastPrinted datetime default NULL,
		  LastAdjusted datetime default NULL,
		  LastGenerated datetime default NULL,
		  LastMarksheetInput datetime default NULL,
		  DateInput datetime default NULL,
		  DateModified datetime default NULL,
		  PRIMARY KEY  (ReportID),
		  KEY ClassLevelID (ClassLevelID)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8";
		
		$sql_table[] = 
		"CREATE TABLE IF NOT EXISTS $reportcard_db.RC_REPORT_TEMPLATE_COLUMN (
		  ReportColumnID int(11) NOT NULL auto_increment,
		  ReportID int(8) default NULL,
		  ColumnTitle varchar(128) default NULL,
		  DefaultWeight float default NULL,
		  DisplayOrder int(2) default NULL,
		  ShowPosition tinyint(4) default NULL,
		  PositionRange varchar(10) default NULL,
		  SemesterNum int(2) default NULL,
		  IsDetails tinyint(1) default NULL,
		  DateInput datetime default NULL,
		  DateModified datetime default NULL,
		  PRIMARY KEY  (ReportColumnID),
		  KEY ReportID (ReportID)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8";
		
		$sql_table[] = 
		"CREATE TABLE IF NOT EXISTS $reportcard_db.RC_REPORT_TEMPLATE_SUBJECT_WEIGHT (
		  ReportColumnSubjectID int(11) NOT NULL auto_increment,
		  ReportID int(8) default NULL,
		  ReportColumnID int(11) default NULL,
		  SubjectID int(11) default NULL,
		  IsDisplay tinyint(4) default NULL,
		  Weight float default NULL,
		  DateInput datetime default NULL,
		  DateModified datetime default NULL,
		  PRIMARY KEY  (ReportColumnSubjectID),
		  KEY ReportID (ReportID),
		  KEY ReportColumnID (ReportColumnID)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8";
		
		$sql_table[] = 
		"CREATE TABLE IF NOT EXISTS $reportcard_db.RC_SETTINGS (
		  SettingID int(8) NOT NULL auto_increment,
		  SettingCategory varchar(32) default NULL,
		  SettingKey varchar(32) default NULL,
		  SettingValue varchar(128) default NULL,
		  DateInput datetime default NULL,
		  DateModified datetime default NULL,
		  PRIMARY KEY  (SettingID)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8";
		
		$sql_table[] = 
		"CREATE TABLE IF NOT EXISTS $reportcard_db.RC_SUBJECT_FORM_GRADING (
		  SubjectFormGradingID int(11) NOT NULL auto_increment,
		  SubjectID int(11) default NULL,
		  ClassLevelID int(11) default NULL,
		  DisplayOrder int(2) default NULL,
		  SchemeID int(8) default NULL,
		  ScaleInput char(2) default NULL,
		  ScaleDisplay char(2) default NULL,
		  DateInput datetime default NULL,
		  DateModified datetime default NULL,
		  PRIMARY KEY  (SubjectFormGradingID),
		  KEY SubjectID (SubjectID)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8";
		
		$sql_table[] = 
		"CREATE TABLE IF NOT EXISTS $reportcard_db.RC_SUB_MARKSHEET_SCORE (
		  ColumnID int(11) default NULL,
		  StudentID int(11) default NULL,
		  MarkRaw float default NULL,
		  TeacherID int(11) default NULL,
		  DateInput datetime default NULL,
		  DateModified datetime default NULL,
		  UNIQUE KEY RC_SUB_MARKSHEET_STD (ColumnID,StudentID),
		  KEY ColumnID (ColumnID),
		  KEY StudentID (StudentID)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8";
		
		$sql_table[] = 
		"CREATE TABLE IF NOT EXISTS $reportcard_db.RC_SUB_MARKSHEET_COLUMN (
		 ColumnID int(11) NOT NULL auto_increment,
		 SubjectID int(8),
		 ReportColumnID int(8),
		 ColumnTitle varchar(128),
		 Ratio float,
		 DateInput datetime,
		 DateModified datetime,
		 PRIMARY KEY (ColumnID),
		 INDEX ReportColumnID (ReportColumnID),
		 INDEX SubjectID (SubjectID),
		 INDEX ColumnTitle (ColumnTitle)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8";
		
		
		##################################
		# Tables for St. Stephen - START (20080715/YatWoon)
		##################################
		$sql_table[] = 
		"CREATE TABLE IF NOT EXISTS $reportcard_db.`RC_CURRICULUM_EXPECTATION` 
		(
		  `CurExpID` int(8) NOT NULL auto_increment,
		  `ClassLevelID` int(11) default NULL,
		  `SubjectID` int(11) default NULL,
		  `Content` text default NULL,
		  `DateInput` datetime default NULL,
		  `DateModified` datetime default NULL,
		  PRIMARY KEY  (`CurExpID`)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8";
		
		$sql_table[] = 
		"CREATE TABLE IF NOT EXISTS $reportcard_db.`RC_PERSONAL_CHARACTERISTICS` 
		(
		  `CharID` int(8) NOT NULL auto_increment,
		  `Title_EN` varchar (255) default NULL,  
		  `Title_CH` varchar (255) default NULL,  
		  `DateInput` datetime default NULL,
		  `DateModified` datetime default NULL,
		  PRIMARY KEY  (`CharID`)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8";
		
		$sql_table[] = 
		"CREATE TABLE IF NOT EXISTS $reportcard_db.`RC_PERSONAL_CHARACTERISTICS_DATA` 
		(
			`CharRecordID` int(8) NOT NULL auto_increment,
			`StudentID` int(11) default NULL,
			`SubjectID` int(8) default NULL,
			`ReportID` int(11) default NULL,
			`CharData` text,
			`TeacherID` int(11) default NULL,
			`DateInput` datetime default NULL,
			`DateModified` datetime default NULL,
			PRIMARY KEY  (`CharRecordID`),
			KEY `StudentID` (`StudentID`),
			KEY `SubjectID` (`SubjectID`)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8";
		
		$sql_table[] = 
		"CREATE TABLE IF NOT EXISTS $reportcard_db.`RC_LEVEL_SUBJECT_PERSONAL_CHARACTERISTICS` 
		(
		  `LSCharID` int(8) NOT NULL auto_increment,
		  `ClassLevelID` int(11) default NULL,
		  `SubjectID` int(11) default NULL,
		  `CharID` int(11) default NULL,
		  `DateModified` datetime default NULL,
		  PRIMARY KEY  (`LSCharID`)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8";
		
		##################################
		# Tables for St. Stephen - END
		##################################
		
		
		##################################
		# Added on 20080829, Table for UCC (KE)
		# Intented for storing "Effort", which is an additional info to subject
		##################################
		$sql_table[] = 
		"CREATE TABLE IF NOT EXISTS $reportcard_db.RC_EXTRA_SUBJECT_INFO (
		  ExtraInfoID int(8) NOT NULL auto_increment,
		  StudentID int(11) default NULL,
		  SubjectID int(8) default NULL,
		  ReportID int(11) default NULL,
		  Info varchar(255) default NULL,
		  TeacherID int(11) default NULL,
		  DateInput datetime default NULL,
		  DateModified datetime default NULL,
		  PRIMARY KEY  (ExtraInfoID),
		  KEY StudentID (StudentID),
		  KEY SubjectID (SubjectID)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8";
		
		
		## Manual Adjustment
		$sql_table[] = 
		"CREATE TABLE IF NOT EXISTS $reportcard_db.RC_MANUAL_ADJUSTMENT (
			ManualAdjustmentID int(11) NOT NULL auto_increment,
			ReportID int(11),
			ReportColumnID int(11),
			StudentID int(8),
			SubjectID int(11),
			OtherInfoName varchar(255),
			AdjustedValue varchar(255),
			DateInput datetime,
			LastModified datetime,
			LastModifiedBy int(8),
			PRIMARY KEY (ManualAdjustmentID),
			INDEX ReportID (ReportID),
			INDEX ReportColumnID (ReportColumnID),
			INDEX StudentID (StudentID),
			INDEX SubjectID (SubjectID),
			UNIQUE ReportSubjectAdjustedValue (ReportID, ReportColumnID, StudentID, SubjectID, OtherInfoName)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8";
		
		
		## Report Archive
		$sql_table[] = 
		"CREATE TABLE IF NOT EXISTS $reportcard_db.RC_REPORT_CARD_ARCHIVE (
			ReportCardArchiveID int(11) NOT NULL auto_increment,
			ReportID int(11) default NULL,
			Semester char(8) default NULL,
			YearID int(8) default NULL,
			YearClassID int(8) default NULL,
			StudentID int(8) default NULL,
			ReportCardHTML text default NULL,
			DateInput datetime default NULL,
			LastModifiedBy int(8) default NULL,
			PRIMARY KEY (ReportCardArchiveID),
			INDEX ReportID (ReportID),
			INDEX Semester (Semester),
			INDEX YearID (YearID),
			INDEX YearClassID (YearClassID),
			INDEX StudentID (StudentID),
			UNIQUE ArchiveReportCardKey (ReportID, StudentID)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8";
		
		##################################
		# Added on 20091117, Table for HKUGA Primary School
		# Store Extended Learning Activity (ELA) Information
		##################################
		$sql_table[] = 
		"CREATE TABLE IF NOT EXISTS $reportcard_db.RC_EXTENDED_LEARNING_ACTIVITY (
		  ELAID int(11) NOT NULL auto_increment,
		  SubjectID int(11) default NULL,
		  IsModule tinyint(2) default 0,
		  Remark text default NULL,
		  LastModifiedBy int(11) default NULL,
		  DateInput datetime default NULL,
		  DateModified datetime default NULL,
		  PRIMARY KEY (ELAID),
		  UNIQUE KEY SubjectID (SubjectID)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8";
		##################################
		# Table for HKUGA Primary School - END
		##################################
		
		
		##################################
		# Added on 20091130, Table for Munsang College
		# Store Promotion Criteria Info
		##################################
		$sql_table[] = 
		"CREATE TABLE IF NOT EXISTS $reportcard_db.RC_PROMOTION_CRITERIA (
			PromotionCriteriaID int(11) NOT NULL auto_increment,
			ClassLevelID int(11) default NULL,
			PassGrandAverage tinyint(1) default 0,
			MaxFailNumOfSubject1 int(3) default NULL,
			SubjectList1 text default NULL,
			MaxFailNumOfSubject2 int(3) default NULL,
			SubjectList2 text default NULL,
			MaxFailNumOfSubject3 int(3) default NULL,
			SubjectList3 text default NULL,
			NonSubmissionAssignment tinyint(1) default 0,
			MaxAssignment int(3),
			DateInput datetime default NULL,
			DateModified datetime default NULL,
			LastModifiedBy int(11) default NULL,
			PRIMARY KEY (PromotionCriteriaID),
			KEY ClassLevelID (ClassLevelID)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8";
		##################################
		# Table for Munsang College - END
		##################################
		
		
		##################################
		# Added on 20091230, Table for Pooi Tun
		# Store Subject Ranking Display Settings
		##################################
		$sql_table[] = 
		"CREATE TABLE IF NOT EXISTS $reportcard_db.RC_POSITION_DISPLAY_SETTING (
			PositionDisplaySettingID int(11) NOT NULL auto_increment,
			ReportID int(11) default NULL,
			SubjectID int(11) default NULL,
			RangeType tinyint(3) default NULL,
			UpperLimit tinyint(3) default NULL,
			GreaterThanAverage tinyint(3) default NULL,
			RecordType varchar(255) default NULL,
			DateInput datetime default NULL,
			DateModified datetime default NULL,
			LastModifiedBy int(11) default NULL,
			PRIMARY KEY (PositionDisplaySettingID),
			UNIQUE KEY ReportSubjectPositionType(ReportID, SubjectID, RecordType)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8";
		##################################
		# Table for Pooi Tun - END
		##################################
		
		
		##################################
		# Added on 20100513, Table for Holy Trinity
		# Store Student Academic Progress Info
		##################################
		$sql_table[] = 
		"CREATE TABLE IF NOT EXISTS $reportcard_db.RC_STUDENT_ACADEMIC_PROGRESS (
			AcademicProgressID int(11) NOT NULL auto_increment,
			ReportID int(11) default NULL,
			SubjectID int(11) default NULL,
			StudentID int(11) default NULL,
			FromReportID int(11) default NULL,
			ToReportID int(11) default NULL,
			FromScore int(11) default NULL,
			ToScore int(11) default NULL,
			ScoreDifference int(11) default NULL,
			OrderClassScoreDifference int(11) default NULL,
			OrderFormScoreDifference int(11) default NULL,
			FromRank int(11) default NULL,
			ToRank int(11) default NULL,
			RankDifference int(11) default NULL,
			OrderClassRankDifference int(11) default NULL,
			OrderFormRankDifference int(11) default NULL,
			DateModified datetime default NULL,
			LastModifiedBy int(11) default NULL,
			PRIMARY KEY (AcademicProgressID),
			UNIQUE KEY StudentSubjectProgress(ReportID, SubjectID, StudentID),
			KEY OrderClassScoreDifference (OrderClassScoreDifference),
			KEY OrderFormScoreDifference (OrderFormScoreDifference),
			KEY OrderClassRankDifference (OrderClassRankDifference),
			KEY OrderFormRankDifference (OrderFormRankDifference)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8";
		##################################
		# Table for Holy Trinity - END
		##################################
		
		##################################
		# Added on 20100618, Table for Master Report
		##################################
		$sql_table[] = 
		"CREATE TABLE IF NOT EXISTS $reportcard_db.RC_MASTER_REPORT_PRESET (
		     PresetID int(8) NOT NULL auto_increment,
		     PresetName varchar(100) default NULL,
		     PresetValue text,
		     DateInput datetime default NULL,
		     DateModified datetime default NULL,
		     PRIMARY KEY (PresetID)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8";
		##################################
		# Table for Master Report - END
		##################################
		
		##################################
		# Added on 20110519, Table for Award Generation (Shatin Methodist)
		##################################
		$sql_table[] = 
		"CREATE TABLE IF NOT EXISTS $reportcard_db.RC_AWARD (
			AwardID int(8) NOT NULL auto_increment,
			AwardCode varchar(64) default NULL,
			AwardNameEn varchar(256) default NULL,
			AwardNameCh varchar(256) default NULL,
			AwardType varchar(64) default NULL COMMENT 'SUBJECT: Subject Prize; OVERALL: Overall Prize;',
			ApplicableReportType varchar(4) default NULL COMMENT 'T: Term Report; W: Whole Year Report; ALL: All Report Types;',
			SchoolCode varchar(128) default NULL,
			DisplayOrder int(4) default NULL,
			InputDate datetime default NULL,
			InputBy int(11) default NULL,
			LastModifiedDate datetime default NULL,
			LastModifiedBy int(11) default NULL,
			PRIMARY KEY (AwardID),
			KEY SchoolCodeApplicableReportType (SchoolCode, ApplicableReportType)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8";
		
		$sql_table[] = 
		"CREATE TABLE IF NOT EXISTS $reportcard_db.RC_AWARD_APPLICABLE_FORM_INFO (
			RecordID int(8) NOT NULL auto_increment,
			ReportID int(8) default 0,
			AwardID int(8) default NULL,
			YearID int(8) default NULL,
			Quota int(4) default NULL,
			PRIMARY KEY (RecordID),
			UNIQUE KEY FormReportAward (YearID, ReportID, AwardID),
			KEY ReportAward (ReportID, AwardID),
			KEY AwardID (AwardID)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8";
		
		$sql_table[] = 
		"CREATE TABLE IF NOT EXISTS $reportcard_db.RC_AWARD_CRITERIA (
			CriteriaID int(8) NOT NULL auto_increment,
			ReportID int(8) default 0,
			AwardID int(8) default NULL,
			CriteriaType varchar(64) default NULL COMMENT 'RANK, PERSONAL_CHAR, IMPROVEMENT, AWARD_LINKAGE',
			CriteriaOrder int(4) default NULL,
			PreviousCriteriaRelation varchar(8) default NULL COMMENT 'AND, OR',
			RankField varchar(64) default NULL COMMENT 'For Ranking: OrderMeritClass, OrderMeritForm, OrderMeritSubjectGroup',
			FromRank int(4) default NULL COMMENT 'For RANK / AWARD_LINKAGE',
			ToRank int(4) default NULL COMMENT 'For RANK / AWARD_LINKAGE',
			MinPersonalCharCode varchar(64) default NULL COMMENT 'For PERSONAL_CHAR',
			ImprovementField varchar(64) default NULL COMMENT 'For IMPROVEMENT: Mark, GrandAverage, GrandTotal, GrandGPA, GrandSDScore, OrderMeritClass, OrderMeritForm, OrderMeritSubjectGroup',
			MinImprovement float default 0 COMMENT 'For IMPROVEMENT',
			FromReportID int(8) default NULL COMMENT 'For IMPROVEMENT',
			FromReportColumnID int(8) default NULL COMMENT 'For IMPROVEMENT',
			FromMustBePass tinyint(2) default 0 COMMENT 'For IMPROVEMENT',
			ToReportID int(8) default NULL COMMENT 'For IMPROVEMENT',
			ToReportColumnID int(8) default NULL COMMENT 'For IMPROVEMENT',
			ToMustBePass tinyint(2) default 1 COMMENT 'For IMPROVEMENT',
			LinkedAwardID int(8) default NULL COMMENT 'For AWARD_LINKAGE',
			ExcludeOriginalAward tinyint(2) default NULL COMMENT 'For AWARD_LINKAGE',
			PRIMARY KEY (CriteriaID),
			KEY ReportAward (ReportID, AwardID),
			KEY LinkedAwardID (LinkedAwardID)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8";
		
		$sql_table[] = 
		"CREATE TABLE IF NOT EXISTS $reportcard_db.RC_AWARD_GENERATED_STUDENT_RECORD (
			RecordID int(8) NOT NULL auto_increment,
			ReportID int(8) default NULL,
			StudentID int(8) default NULL,
			AwardID int(8) default NULL,
			SubjectID int(8) default NULL,
			AwardRank int(4) default NULL,			
			DetermineValue float default NULL,
			DetermineText text default NULL,
			RankField varchar(64) default NULL COMMENT 'For Ranking: OrderMeritClass, OrderMeritForm, OrderMeritSubjectGroup',			
			ImprovementValue float default NULL,
			ImprovementField varchar(64) default NULL COMMENT 'For Improvement: Mark, GrandAverage, GrandTotal, GrandGPA, GrandSDScore, OrderMeritClass, OrderMeritForm, OrderMeritSubjectGroup',
			FromValue float default NULL,
			FromNature varchar(64) default NULL,
			ToValue float default NULL,
			ToNature varchar(64) default NULL,
			InputBy int(8) default NULL,
			InputDate datetime default NULL,
			PRIMARY KEY (RecordID),
			KEY ReportStudentAward (ReportID, StudentID, AwardID),
			KEY ReportAward (ReportID, AwardID),
			KEY StudentID (StudentID)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8";
		
		$sql_table[] = 
		"CREATE TABLE IF NOT EXISTS $reportcard_db.RC_AWARD_STUDENT_RECORD (
			RecordID int(8) NOT NULL auto_increment,
			ReportID int(8) default NULL,
			StudentID int(8) default NULL,
			AwardText text default NULL,
			InputBy int(8) default NULL,
			InputDate datetime default NULL,
			LastModifiedBy int(8) default NULL,
			LastModifiedDate datetime default NULL,
			PRIMARY KEY (RecordID),
			KEY ReportStudent (ReportID, StudentID),
			KEY StudentID (StudentID)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8";
		
		$sql_table[] = 
		"CREATE TABLE IF NOT EXISTS $reportcard_db.RC_TEMP_IMPORT_AWARD (
			TempID int(11) NOT NULL auto_increment,
			ImportUserID int(11) default NULL,
			RowNumber int(8) default NULL,
			ClassName varchar(255) default NULL,
			ClassNumber varchar(255) default NULL,
			UserLogin varchar(255) default NULL,
			WebSAMSRegNo varchar(100) default NULL,
			StudentID int(8) default NULL,
			StudentName varchar(255) default NULL,
			AwardText text default NULL,
			DateInput datetime default NULL,
			PRIMARY KEY (TempID),
			KEY ImportUserIDAndRowNumber (ImportUserID, RowNumber)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8";

		##################################
		# Table for Award Generation (Shatin Methodist) - END
		##################################
		
		$sql_table[] = 
		"CREATE TABLE IF NOT EXISTS $reportcard_db.RC_OTHER_INFO_STUDENT_RECORD (
			RecordID int(11) NOT NULL auto_increment,
			TermID int(11) NOT NULL,
			UploadType varchar(128) Default Null,
			StudentID int(11) NOT NULL,
			ItemCode varchar(128) Default Null,
			Information text Default Null,
			DateInput datetime Default Null,
			InputBy int(11) Default Null,
			PRIMARY KEY (RecordID),
			KEY TermID (TermID),
			KEY StudentID (StudentID),
			KEY ItemCode (ItemCode),
			KEY UploadType (UploadType)
		)ENGINE=InnoDB DEFAULT CHARSET=utf8
		";
		
		$sql_table[] = 
		"CREATE TABLE IF NOT EXISTS $reportcard_db.RC_PERSONAL_CHARACTERISTICS_SCALE (
		     ScaleID int(8) NOT NULL auto_increment,
		     Code varchar(64) default NULL,
		     NameEn varchar(255) default NULL,
		     NameCh varchar(255) default NULL,
		     IsDefault tinyint(1) default 0,
		     DisplayOrder int(3) default NULL,
		     DateInput datetime default NULL,
		     InputBy int(8) default NULL,
		     DateModified datetime default NULL,
		     ModifiedBy int(8) default NULL,
		     PRIMARY KEY (ScaleID),
		     UNIQUE KEY Code (Code)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8
		";
		
		$sql_table[] = 
		"CREATE TABLE IF NOT EXISTS $reportcard_db.RC_PERSONAL_CHARACTERISTICS_SCALE_MGMT_USER (
		     ScaleMgmtUserID int(11) NOT NULL auto_increment,
		     ScaleID int(8) NOT NULL,
			 UserID int(8) NOT NULL,
			 DateInput datetime default NULL,
		     InputBy int(8) default NULL,
		     DateModified datetime default NULL,
		     ModifiedBy int(8) default NULL,
		     PRIMARY KEY (ScaleMgmtUserID),
		     UNIQUE KEY ScaleUser (ScaleID, UserID),
			 KEY UserID (UserID)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8
		";
		
		$sql_table[] = 
		"CREATE TABLE $reportcard_db.RC_REPORT_TEMPLATE_SUBJECT_VERTICAL_WEIGHT (
		     VerticalWeightID int(11) NOT NULL auto_increment,
		     ReportID int(8) default NULL,
		     ReportColumnID int(11) default NULL,
		     SubjectID int(11) default NULL,
		     Weight float default NULL,
		     DateInput datetime default NULL,
		     InputBy int(8) default NULL,
		     DateModified datetime default NULL,
		     ModifiedBy int(8) default NULL,
		     PRIMARY KEY (VerticalWeightID),
		     KEY ReportColumnSubject (ReportID, ReportColumnID, SubjectID),
			 KEY ReportColumnID (ReportColumnID),
			 KEY SubjectID (SubjectID)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8
		";

		$sql_table[] = 
		"CREATE TABLE IF NOT EXISTS $reportcard_db.RC_CONDUCT (
		  ConductID int(11) NOT NULL auto_increment,
		  Conduct varchar(64) default NULL,
		  DateInput datetime default NULL,
		  InputBy int(8) Default Null,
		  DateModified datetime default NULL,
		  LastModifiedBy int(8) Default Null,
		  PRIMARY KEY  (ConductID)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8";
		
		$sql_table[] = 
		"CREATE TABLE IF NOT EXISTS $reportcard_db.RC_REPORT_STUDENT_EXTRA_INFO (
		  StudentExtraInfoID int(11) NOT NULL auto_increment,
		  StudentID int(8) NOT NULL,
		  ReportID int(11) NOT NULL,
		  ConductID int(11) default NULL,
		  DateInput datetime default NULL,
		  InputBy int(8) Default Null,
		  DateModified datetime default NULL,
		  LastModifiedBy int(8) Default Null,
		  PRIMARY KEY  (StudentExtraInfoID),
		  UNIQUE KEY StudentReportID (StudentID, ReportID)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8";
		
		$sql_table[] = 
		"CREATE TABLE IF NOT EXISTS $reportcard_db.RC_COMMENT_MAX_LENGTH (
		     SettingsID int(8) NOT NULL auto_increment,
		     ReportID int(11) default Null,
		     SubjectID int(11) default Null,
		     MaxLength int(8) default -1,
		     InputDate datetime default NULL,
		     InputBy int(11) default NULL,
		     ModifiedDate datetime default NULL,
		     ModifiedBy int(11) default NULL,
		     PRIMARY KEY (SettingsID),
		     UNIQUE KEY ReportSubjectSettings (ReportID, SubjectID)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8";
		
		// 2012-03-06 [Please follow this format afterwards]
//		$sql_table[] = array(
//			"2012-03-06",
//			"Create test eRC DB schema table",
//			"CREATE TABLE IF NOT EXISTS $reportcard_db.RC_TEMP (
//			     TempID int(8) NOT NULL auto_increment,
//			     PRIMARY KEY (TempID)
//			) ENGINE=InnoDB DEFAULT CHARSET=utf8"
//		);

		$sql_table[] = array(
			"2012-07-26",
			"Create Teacher Extra Info Table",
			"CREATE TABLE IF NOT EXISTS $reportcard_db.RC_TEACHER_EXTRA_INFO (
			     RecordID int(11) NOT NULL auto_increment,
			     UserID int(11) NOT NULL,
			     SignaturePath text default NULL,
				 SchoolChopPath text default NULL,
				 TeacherTitle varchar(255) default NULL,
			     DateInput datetime default NULL,
			     InputBy int(11) default NULL,
				 DateModified datetime default NULL,
			     ModifiedBy int(11) default NULL,
			     PRIMARY KEY (RecordID),
			     UNIQUE KEY UserID (UserID)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8"
		);
		
		$sql_table[] = array(
			"2012-09-03",
			"Create table RC_MARKSHEET_SPECIFIC_USER_SUBMISSION_PERIOD to store specific marksheet submission period for specific users",
			"CREATE TABLE IF NOT EXISTS $reportcard_db.RC_MARKSHEET_SPECIFIC_USER_SUBMISSION_PERIOD (
			     RecordID int(11) NOT NULL auto_increment,
				 ReportID int(11) NOT NULL,
			     UserID int(11) NOT NULL,
			     MarksheetSubmissionStart datetime NOT NULL,
				 MarksheetSubmissionEnd datetime NOT NULL,
			     DateInput datetime default NULL,
			     InputBy int(11) default NULL,
				 DateModified datetime default NULL,
			     ModifiedBy int(11) default NULL,
			     PRIMARY KEY (RecordID),
			     UNIQUE KEY ReportIDUserID (ReportID, UserID)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8"
		);
		
		$sql_table[] = array(
			"2012-12-13",
			"Create table RC_LOG to log different user actions",
			"CREATE TABLE IF NOT EXISTS $reportcard_db.RC_LOG (
			     LogID int(8) NOT NULL auto_increment,
			     Functionality varchar(128) default NULL,
			     UserID int(8) default NULL,
			     LogContent text,
			     InputDate datetime default NULL,
			     PRIMARY KEY (LogID),
			     KEY Functionality (Functionality)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8"
		);
		
		$sql_table[] = array(
			"2013-07-05",
			"Create table RC_REPORT_TEMPLATE_FILE to store different file path of a template",
			"CREATE TABLE IF NOT EXISTS $reportcard_db.RC_REPORT_TEMPLATE_FILE (
			     FileID int(8) NOT NULL auto_increment,
			     ReportID int(11) NOT NULL,
				 FileType varchar(128) NOT NULL,
			     FilePath text NOT NULL,
			     InputDate datetime default NULL,
				 InputBy int(11) NOT NULL,
				 ModifiedDate datetime default NULL,
				 ModifiedBy int(11) NOT NULL,
			     PRIMARY KEY (FileID),
			     KEY ReportIDFileType (ReportID, FileType)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8"
		);
		
		$sql_table[] = array(
			"2013-07-16",
			"Create table RC_TEMP_IMPORT_CLASS_TEACHER_COMMENT to store temp record for validation of student identity",
			"CREATE TABLE IF NOT EXISTS $reportcard_db.RC_TEMP_IMPORT_CLASS_TEACHER_COMMENT (
				TempID int(11) NOT NULL auto_increment,
				ImportUserID int(11) default NULL,
				RowNumber int(8) default NULL,
				ClassName varchar(255) default NULL,
				ClassNumber varchar(255) default NULL,
				WebSAMSRegNo varchar(100) default NULL,
				StudentID int(8) default NULL,
				StudentName varchar(255) default NULL,
				CommentContent text default NULL,
				AdditionalComment text default NULL,
				Conduct varchar(255) default NULL,
				PRIMARY KEY (TempID),
				KEY ImportUserIDAndRowNumber (ImportUserID, RowNumber)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8"
		);
		
		$sql_table[] = array(
			"2014-01-22",
			"Create table RC_EXCLUDE_RANKING_STUDENT to store exclude ranking student",
			"CREATE TABLE IF NOT EXISTS $reportcard_db.RC_EXCLUDE_RANKING_STUDENT (
				RecordID int(11) NOT NULL auto_increment,
				YearTermID int(11) default NULL,
				StudentID int(11) default NULL,
				InputDate datetime default NULL,
				InputBy int(11) NOT NULL,
				ModifiedDate datetime default NULL,
				ModifiedBy int(11) NOT NULL,
				PRIMARY KEY (RecordID),
				KEY TermStudent (YearTermID, StudentID)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8"
		);
		
		$sql_table[] = array(
			"2015-04-23",
			"Create table RC_SUBJECT_TOPIC to store subject topic info",
			"CREATE TABLE IF NOT EXISTS $reportcard_db.RC_SUBJECT_TOPIC (
			     SubjectTopicID int(11) NOT NULL auto_increment,
			     SubjectID int(11) NOT NULL,
				 YearID int(11) NOT NULL,
				 Code varchar(128) NOT NULL,
			     NameEn varchar(255) NOT NULL,
			     NameCh varchar(255) NOT NULL,
			     Level tinyint(3) NOT NULL,
			     DisplayOrder tinyint(3) NOT NULL,
			     PreviousLevelTopicID int(11) NOT NULL,
			     ParentTopicID int(11) NOT NULL,
			     RecordStatus tinyint(3) default '1',
			     DateInput datetime NOT NULL,
			     InputBy int(11) NOT NULL,
			     DateModified datetime NOT NULL,
			     LastModifiedBy int(11) NOT NULL,
			     PRIMARY KEY (SubjectTopicID),
			     KEY ActiveFormSubject (RecordStatus, YearID, SubjectID),
				 KEY ActiveCode (RecordStatus, Code)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8"
		);
		
		$sql_table[] = array(
			"2015-05-04",
			"Create table RC_MARKSHEET_TOPIC_SCORE to store marksheet input score of subject topic",
			"CREATE TABLE IF NOT EXISTS $reportcard_db.RC_MARKSHEET_TOPIC_SCORE (
			     MarksheetTopicScoreID int(11) NOT NULL auto_increment,
			     StudentID int(11) default NULL,
			     SubjectID int(11) default NULL,
			     SubjectTopicID int(11) default NULL,
			     ReportColumnID int(11) default NULL,
			     SchemeID int(11) default NULL,
			     MarkType char(2) default NULL,
			     MarkRaw float default '-1',
			     MarkNonNum varchar(11) default NULL,
			     LastModifiedUserID int(11) default NULL,
			     DateInput datetime default NULL,
			     DateModified datetime default NULL,
			     PRIMARY KEY (MarksheetTopicScoreID),
			     UNIQUE KEY RC_MARKSHEET_TOPIC_SCORE_UK1 (ReportColumnID,SubjectID,SubjectTopicID,StudentID),
			     KEY StudentID (StudentID),
			     KEY SubjectID (SubjectID),
			     KEY SubjectTopicID (SubjectTopicID),
			     KEY SchemeID (SchemeID)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8"
		);
		
		$sql_table[] = array(
			"2015-05-04",
			"Create table TEMP_SUBJECT_TOPIC_IMPORT to store import subject topic temp data",
			"CREATE TABLE IF NOT EXISTS $reportcard_db.TEMP_SUBJECT_TOPIC_IMPORT (
			     TempID int(11) NOT NULL auto_increment,
			     UserID int(11) NOT NULL,
			     RowNumber int(11) default NULL,
			     YearID int(11) NOT NULL,
				 YearName varchar(255) NOT NULL,
			     SubjectID int(11) NOT NULL,
			     SubjectCode varchar(255) NOT NULL,
				 SubjectName varchar(255) NOT NULL,
			     SubjectTopicID int(11) NOT NULL,
			     SubjectTopicCode varchar(255) NOT NULL,
			     SubjectTopicNameEn varchar(255) NOT NULL,
				 SubjectTopicNameCh varchar(255) NOT NULL,
			     DateInput datetime default NULL,
			     PRIMARY KEY (TempID),
			     KEY UserID (UserID),
			     KEY RowNumber (RowNumber)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8"
		);
		
		$sql_table[] = array(
			"2015-10-08",
			"Create table RC_TEMP_IMPORT_SUBJECT_SCORE to store temp data of import subject score",
			"CREATE TABLE IF NOT EXISTS $reportcard_db.RC_TEMP_IMPORT_SUBJECT_SCORE (
			     TempID int(11) NOT NULL auto_increment,
			     UserID int(11) NOT NULL,
			     RowNumber int(11) default NULL,
			     ReportID int(11) default NULL,
				 StudentID int(11) default NULL,
			     SubjectID int(11) default NULL,
			     ReportColumnID int(11) default NULL,
			     SchemeID int(11) default NULL,
			     MarkType char(2) default NULL,
			     MarkRaw float default '-1',
			     MarkNonNum varchar(11) default NULL,
			     IsEstimated tinyint(2) default '0',
			     DateInput datetime default NULL,
			     PRIMARY KEY (TempID),
			     KEY UserID (UserID),
			     KEY RowNumber (RowNumber),
			     KEY ReportID (ReportID)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8"
		);
		
		$sql_table[] = array(
			"2015-10-27",
			"Create table RC_REPORT_SETTING to store report-based settings",
			"CREATE TABLE IF NOT EXISTS $reportcard_db.RC_REPORT_SETTING (
			     SettingsID int(11) NOT NULL auto_increment,
				 ReportID int(11) NOT NULL,
			     SettingName varchar(100) NOT NULL,
			     SettingValue text,
			     DateInput datetime default NULL,
			     InputBy int(11) default NULL,
			     DateModified datetime default NULL,
			     ModifiedBy int(11) default NULL,
			     PRIMARY KEY (SettingsID),
			     UNIQUE KEY ReportSettingName (ReportID, SettingName)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8"
		);
		
		$sql_table[] = array(
			"2015-12-16",
			"Create table RC_TEMP_IMPORT_EXTRA_INFO to store temp data for extra info import",
			"CREATE TABLE IF NOT EXISTS $reportcard_db.RC_TEMP_IMPORT_EXTRA_INFO (
			     TempID int(11) NOT NULL auto_increment,
			     UserID int(11) NOT NULL,
			     RowNumber int(11) default NULL,
			     ReportID int(11) default NULL,
				 WebSAMSRegNo varchar(100) default NULL,
				 ClassName varchar(255) default NULL,
				 ClassNumber int(8) default NULL,
				 StudentID int(11) default NULL,
				 SubjectID int(11) default NULL,
				 SubjectGroupCode varchar(128) default NULL,
			     SubjectGroupName varchar(255) default NULL,
			     SubjectGroupID int(11) default NULL,
			     Info varchar(64) default NULL,
			     DateInput datetime default NULL,
			     PRIMARY KEY (TempID),
			     KEY UserID (UserID),
			     KEY RowNumber (RowNumber),
			     KEY ReportID (ReportID)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8"
		);
		
		$sql_table[] = array(
			"2015-12-28",
			"Create Table RC_FORM_MAIN_SUBJECT for storing main subjects of each class level",
			"CREATE TABLE IF NOT EXISTS $reportcard_db.RC_FORM_MAIN_SUBJECT (
				RecordID int(11) NOT NULL auto_increment,
				ClassLevelID int(11) NOT NULL,
				SubjectID int(11) NOT NULL,
				DisplayStatus tinyint(2) default 0,
				DateInput datetime default NULL,
				InputBy int(8) default Null,
				DateModified datetime default NULL,
				LastModifiedBy int(8) default Null,
				PRIMARY KEY (RecordID),
				UNIQUE KEY FormSubject (ClassLevelID, SubjectID)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8"
		);
		
		$sql_table[] = array(
			"2016-06-15",
			"Create Table RC_TEMP_IMPORT_ALL_SCHOOL_AWARD to store temp data of import awards",
			"CREATE TABLE IF NOT EXISTS $reportcard_db.RC_TEMP_IMPORT_ALL_SCHOOL_AWARD (
			     TempID int(11) NOT NULL auto_increment,
			     ImportUserID int(11) NOT NULL,
			     RowNumber int(11) default NULL,
			     AwardID int(8) default NULL,
			     AwardCode varchar(255) default NULL,
			     AwardName varchar(255) default NULL,
			     AwardAppliedForm text,
			     DateInput datetime default NULL,
			     PRIMARY KEY (TempID),
			     KEY ImportUserID (ImportUserID),
			     KEY RowNumber (RowNumber),
			     KEY AwardID (AwardID)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8"
		);
		
		$sql_table[] = array(
			"2016-06-22",
			"Create Table RC_PROMOTION_FAILED_SUBJECT for storing failed subjects linked to promotion students",
			"CREATE TABLE $reportcard_db.RC_PROMOTION_FAILED_SUBJECT (
			     RecordID int(11) NOT NULL auto_increment,
			     StudentID int(11) NOT NULL,
			     ReportID int(11) NOT NULL,
			     SubjectID int(11) NOT NULL,
			     Mark float default NULL,
			     Grade varchar(255) default NULL,
			     InputBy int(11) NOT NULL,
			     DateInput datetime NOT NULL,
			     ModifiedBy int(11) NOT NULL,
			     DateModified datetime NOT NULL,
			     PRIMARY KEY (RecordID),
			     UNIQUE KEY StudentReportSubjectKey (StudentID, ReportID, SubjectID)
			)"
		);
		
		$sql_table[] = array(
			"2016-12-15",
			"Create Table RC_ABILITY_INDEX_CATEGORY for categories of ability index",
			"CREATE TABLE $reportcard_db.RC_ABILITY_INDEX_CATEGORY (
			     CatID int(11) NOT NULL auto_increment,
			     Code varchar(255) NOT NULL,
			     Name varchar(255) NOT NULL,
			     Type tinyint(1) NOT NULL,
			     Level int(8) NOT NULL,
			     UpperCat int(11) default NULL,
			     DateInput datetime NOT NULL,
			     DateModified datetime NOT NULL,
			     PRIMARY KEY (CatID),
			     UNIQUE KEY Code (Code)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8"
		);
		
		$sql_table[] = array(
			"2016-12-15",
			"Create Table RC_ABILITY_INDEX_ITEM for items of ability index",
			"CREATE TABLE $reportcard_db.RC_ABILITY_INDEX_ITEM (
			     ItemID int(11) NOT NULL auto_increment,
			     Code varchar(255) NOT NULL,
			     Name varchar(255) NOT NULL,
			     Type tinyint(1) NOT NULL,
			     Level int(8) NOT NULL,
			     UpperCat int(11) default NULL,
			     DateInput datetime NOT NULL,
			     DateModified datetime NOT NULL,
			     PRIMARY KEY (ItemID),
			     UNIQUE KEY Code (Code)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8"
		);
		
		$sql_table[] = array(
			"2016-12-16",
			"Create Table RC_ABILITY_CATEGORY_INDEX_MAPPING for mapping between categories of ability index",
			"CREATE TABLE $reportcard_db.RC_ABILITY_CATEGORY_INDEX_MAPPING (
			     MappingID int(11) NOT NULL auto_increment,
				 TWItemID int(11) NOT NULL,
			     TWItemCode varchar(255) NOT NULL,
				 MOItemID int(11) NOT NULL,
			     MOItemCode varchar(255) NOT NULL,
			     DateInput datetime NOT NULL,
			     DateModified datetime NOT NULL,
			     PRIMARY KEY (MappingID),
			     KEY TWItemCode (TWItemCode),
			     KEY MOItemCode (MOItemCode)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8"
		);
		
		$sql_table[] = array(
			"2017-03-03",
			"Create table RC_PROMOTION_RETENTION_FAILED_SUBJECT for storing failed subjects linked to promotion students",
			"CREATE TABLE IF NOT EXISTS $reportcard_db.RC_PROMOTION_RETENTION_FAILED_SUBJECT (
				RecordID int(11) auto_increment NOT NULL,
				StudentID int(11) NOT NULL,
				ReportID int(11) NOT NULL,
				SubjectID int(11) NOT NULL,
				Mark float DEFAULT NULL,
				Grade varchar(255) DEFAULT NULL,
				SubjectType tinyint NOT NULL DEFAULT '1' COMMENT '1:Current Year; 2:Previous Year',
				Year int(8) DEFAULT NULL,
				InputBy int(11) NOT NULL,
				DateInput datetime NOT NULL,
				ModifiedBy int(11) NOT NULL,
				DateModified datetime NOT NULL,
				PRIMARY KEY (RecordID),
				UNIQUE KEY StudentReportSubjectKey(StudentID, ReportID, SubjectID, SubjectType, Year) 
			) ENGINE=InnoDB DEFAULT CHARSET=utf8"
		);
		
		$sql_table[] = array(
			"2017-04-13",
			"Create table RC_STUDENT_PROMOTION_RETENTION_STATUS for storing student promotion status",
			"CREATE TABLE IF NOT EXISTS $reportcard_db.RC_STUDENT_PROMOTION_RETENTION_STATUS (
				RecordID int(11) auto_increment NOT NULL,
				StudentID int(11) NOT NULL,
				ReportID int(11) NOT NULL,
				ClassLevelID int(11) NOT NULL,
				SchoolType varchar(8) DEFAULT NULL COMMENT 'P: Primary; S:Secondary',
				FormNumber int(8) DEFAULT NULL,
				Year int(8) DEFAULT NULL,
				PromotionStatus int(8) DEFAULT NULL,
				StatusReason varchar(255) DEFAULT NULL,
				IsNeedMarkupExam tinyint(4) default '0',
				InputBy int(11) NOT NULL,
				DateInput datetime NOT NULL,
				ModifiedBy int(11) NOT NULL,
				DateModified datetime NOT NULL,
				PRIMARY KEY (RecordID),
				UNIQUE KEY StudentPromotionStatusKey(StudentID, ClassLevelID, Year) 
			) ENGINE=InnoDB DEFAULT CHARSET=utf8"
		);
		
		$sql_table[] = array(
			"2017-05-15",
			"Create table RC_GROUP_MEMBER for storing group members",
			"CREATE TABLE IF NOT EXISTS $reportcard_db.RC_GROUP_MEMBER (
				UserID int(11) NOT NULL,
     			GroupType varchar(255) default NULL,
     			UNIQUE KEY UserID (UserID)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8"
		);
		
		$sql_table[] = array(
		    "2018-06-15",
		    "Create Table RC_SUBJECT_FORM_CREDIT for credit setting",
		    "CREATE TABLE IF NOT EXISTS $reportcard_db.RC_SUBJECT_FORM_CREDIT(
                FormCreditID int(11) NOT NULL AUTO_INCREMENT,
                ClassLevelID int(11) NOT NULL,
                ReportID     int(11) NOT NULL,
                SubjectID    int(11) NOT NULL,
                Type         varchar(1) NOT NULL,
                Credit       float NULL,
                LowerLimit   float NULL,
                DateInput    datetime NULL,
                InputBy      int(11) NULL,
                DateModified datetime NULL,
                ModifiedBy   int(11) NULL,
                PRIMARY KEY(FormCreditID),
			    KEY ClassLevelID (ClassLevelID),
			    KEY SubjectID (SubjectID),
			    KEY Type (Type)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8"
		);
		
		$sql_table[] = array(
		    "2019-06-28",
		    "Create Table RC_GRADE_DESCRIPTOR for grade setting",
		    "CREATE TABLE IF NOT EXISTS $reportcard_db.RC_GRADE_DESCRIPTOR(
                DescriptorID int(11) NOT NULL AUTO_INCREMENT,
                Grade varchar(255) NOT NULL,
                Descriptor_ch TEXT NULL,
                Descriptor_en TEXT NULL,
                DateInput    datetime NULL,
                InputBy      int(11) NULL,
                DateModified datetime NULL,
                ModifiedBy   int(11) NULL,
                PRIMARY KEY(DescriptorID),
                KEY Grade (Grade)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8"
		);
		
		$sql_table[] = array(
			"2019-12-18",
			"Create Table RC_STUDENT_SUBJECT_STREAM for subject stream setting",
			"CREATE TABLE IF NOT EXISTS $reportcard_db.RC_STUDENT_SUBJECT_STREAM(
				RecordID int(11) NOT NULL AUTO_INCREMENT,
				StudentID int(11) NOT NULL,
				SubjectStream varchar(1) NOT NULL,
				DateInput DATETIME NULL,
				InputBy int(11) NULL,
				DateModified DATETIME NULL,
				ModifiedBy int(11) NULL,
				PRIMARY KEY(RecordID),
				KEY(StudentID),
				KEY(SubjectStream)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8"
		);

        $sql_table[] = array(
            "2020-04-27",
            "Create Table RC_STUDENT_CONODUCT_GRADE for generated conduct grade",
            "CREATE TABLE IF NOT EXISTS $reportcard_db.RC_STUDENT_CONODUCT_GRADE(
				RecordID int(11) NOT NULL AUTO_INCREMENT,
				ReportID int(11) NOT NULL,
				StudentID int(11) NOT NULL,
				ConductScore int(11) NOT NULL,
				ConductGrade varchar(255) DEFAULT NULL,
				ModifiedGrade varchar(255) DEFAULT NULL,
				DateInput datetime DEFAULT NULL,
				InputBy int(11) DEFAULT NULL,
				DateModified datetime DEFAULT NULL,
				ModifiedBy int(11) DEFAULT NULL,
				PRIMARY KEY(RecordID),
				KEY(ReportID),
				KEY(StudentID)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8"
        );

        $sql_table[] = array(
            "2020-04-27",
            "Create Table RC_PROMOTION_STATUS_REMARKS for remarks of different promotion status",
            "CREATE TABLE IF NOT EXISTS $reportcard_db.RC_PROMOTION_STATUS_REMARKS(
				RecordID int(11) NOT NULL AUTO_INCREMENT,
				ClassLevelID int(11) NOT NULL,
				SubjectStream varchar(1) DEFAULT NULL,
				PromotionType varchar(255) NOT NULL,
                Remarks text DEFAULT NULL,
				DateInput datetime DEFAULT NULL,
				InputBy int(11) DEFAULT NULL,
				DateModified datetime DEFAULT NULL,
				ModifiedBy int(11) DEFAULT NULL,
				PRIMARY KEY(RecordID),
				KEY(ClassLevelID),
				KEY(SubjectStream),
				KEY(PromotionType)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8"
        );

        $sql_table[] = array(
            '2020-04-27',
            'CREATE TABLE RC_GROUP_MEMBER_RIGHT for assigned admin right of specific pages',
            "CREATE TABLE IF NOT EXISTS $reportcard_db.RC_GROUP_MEMBER_RIGHT (
                 GroupMemberRightID int(11) NOT NULL AUTO_INCREMENT,
				 UserID int(11) NOT NULL,
                 AdminGroupRightName varchar(128) DEFAULT NULL,
                 DateInput datetime DEFAULT NULL,
                 InputBy int(11) DEFAULT NULL,
                 DateModified datetime DEFAULT NULL,
                 LastModifiedBy int(11) DEFAULT NULL,
                 PRIMARY KEY (GroupMemberRightID),
                 KEY UserID (UserID),
                 KEY AdminGroupRightName (AdminGroupRightName)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8"
        );

        $sql_table[] = array(
            '2020-06-21',
            'CREATE TABLE RC_REPORT_PRESET_SETTING for report preset settings of different reports',
            "CREATE TABLE IF NOT EXISTS $reportcard_db.RC_REPORT_PRESET_SETTING (
                 PresetID int(8) NOT NULL auto_increment,
                 PresetReportType varchar(100) DEFAULT NULL,
                 PresetName varchar(100) DEFAULT NULL,
                 PresetValue mediumtext,
                 DateInput datetime DEFAULT NULL,
                 InputBy int(11) DEFAULT NULL,
                 DateModified datetime DEFAULT NULL,
                 ModifiedBy int(11) DEFAULT NULL,
                 PRIMARY KEY (PresetID),
                 KEY PresetReportType (PresetReportType),
                 KEY PresetName (PresetName)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8"
        );

        $sql_table[] = array(
            '2020-10-22',
            'CREATE TABLE RC_SUBJECT_GROUP_DESCRIPTION to store description for each subject group',
            "CREATE TABLE IF NOT EXISTS $reportcard_db.RC_SUBJECT_GROUP_DESCRIPTION (
                RecordID int(11) NOT NULL auto_increment,
                RecordCode varchar(255) DEFAULT NULL,
                ReportID int(11) DEFAULT NULL,
                SubjectID int(11) DEFAULT NULL,
                SubjectGroupID int(11) DEFAULT NULL,
                Description text,
                DescriptionChi text,
                DateInput datetime DEFAULT NULL,
                InputBy int(11) DEFAULT NULL,
                DateModified datetime DEFAULT NULL,
                ModifiedBy int(11) DEFAULT NULL,
                PRIMARY KEY (RecordID),
                KEY RecordCode (RecordCode),
                KEY ReportID (ReportID),
                KEY SubjectID (SubjectID),
                KEY SubjectGroupID (SubjectGroupID)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8"
        );

		# create new tables
		$numOfCreateTable = count((array)$sql_table);
		for($i=0; $i<$numOfCreateTable; $i++){
			$sql = $sql_table[$i];
			
			if (is_array($sql)) {
				// new logic => have input date, desc, and the sql statement
				updateSchema_eRC($flag, $last_schema_date_updated, $sql[0], $sql[1], $sql[2]);
			}
			else {
				// old logic => only have the sql statement
				if ($flag==2) {
					// will not include old schema update for "update new schema only"
				}
				else {
					if($li->db_db_query($sql)){
						echo "<p>Created (or skip creating) a new table:<br />".$sql."</p>\n";
					} else {
						echo "<p>Failed to create table:<br />".$sql."<br />Error: ".mysql_error()."</p>\n";
					}
				}
			}
		}
		
		
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_COMMENT_BANK DROP COLUMN IsSubjectComment;";
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_COMMENT_BANK ADD COLUMN SubjectID INT(11) AFTER CommentCategory;";
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_COMMENT_BANK ADD INDEX SubjectID (SubjectID);";
		
		// 2008-07-02 Andy
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_REPORT_RESULT_SCORE ADD RawMark FLOAT AFTER Grade;";
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_REPORT_RESULT_SCORE_ARCHIVE ADD RawMark FLOAT AFTER Grade;";
		
		// 2008-07-04 Yat Woon
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_REPORT_TEMPLATE ADD COLUMN ShowGrandTotal tinyint AFTER OverallPositionRangeForm;";
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_REPORT_TEMPLATE ADD COLUMN ShowGrandAvg tinyint AFTER ShowGrandTotal;";
		
		// 2008-07-15 Andy
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_REPORT_RESULT_SCORE_ARCHIVE DROP COLUMN MarkWeighted;";
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_REPORT_RESULT_SCORE_ARCHIVE ADD COLUMN Mark FLOAT DEFAULT NULL AFTER ReportColumnID;";
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_REPORT_RESULT_SCORE_ARCHIVE ADD COLUMN RawMark FLOAT DEFAULT NULL AFTER Grade;";
		
		// 2008-07-16 Andy
		/* disabled by Yuen on 2008-11-25
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_SUB_MARKSHEET_SCORE CHANGE ColumnID ReportColumnID INT(11) DEFAULT NULL;";
		*/
			
		// 2008-07-25 Andy
		$sql_alter[] = "ALTER TABLE $intranet_db.INTRANET_USER ADD HKID varchar(10) default NULL;";
		
		// 2008-07-29 Andy
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_REPORT_RESULT ADD COLUMN ClassNoOfStudent INT(4) DEFAULT NULL AFTER OrderMeritClass";
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_REPORT_RESULT ADD COLUMN FormNoOfStudent INT(4) DEFAULT NULL AFTER OrderMeritForm";
		
		// 2008-08-11 Andy
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_REPORT_RESULT_SCORE ADD COLUMN ClassNoOfStudent INT(4) DEFAULT NULL AFTER OrderMeritClass";
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_REPORT_RESULT_SCORE ADD COLUMN FormNoOfStudent INT(4) DEFAULT NULL AFTER OrderMeritForm";
		
		// 2008-11-25 Yuen
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_SUB_MARKSHEET_SCORE CHANGE ReportColumnID ColumnID INT(11) DEFAULT NULL;";
		
		// 2008-12-01 Ivan
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_REPORT_TEMPLATE ADD ShowNumOfStudentClass tinyint(1) DEFAULT NULL;";
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_REPORT_TEMPLATE ADD ShowNumOfStudentForm tinyint(1) DEFAULT NULL;";
		
		// 2009-04-27 Ivan
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_SUB_MARKSHEET_COLUMN ADD Calculation char(255) DEFAULT NULL AFTER Ratio;";
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_OTHER_STUDENT_INFO MODIFY Conduct varchar(32);";
		
		// 2009-05-20 Ivan
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_REPORT_TEMPLATE ADD LastDateToiPortfolio datetime DEFAULT NULL;";
		
		// 2009-07-23 Ivan
		$sql_alter[] = "ALTER Table $reportcard_db.RC_MARKSHEET_SUBMISSION_PROGRESS Add SubjectGroupID int(11) default NULL After ReportID;";
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_MARKSHEET_SUBMISSION_PROGRESS ADD INDEX SubjectGroupID (SubjectGroupID);";
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_MARKSHEET_SUBMISSION_PROGRESS ADD INDEX ReportID (ReportID);";
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_MARKSHEET_SUBMISSION_PROGRESS ADD INDEX ClassID (ClassID);";
		
		// 2009-07-24 Ivan
		$sql_alter[] = "ALTER Table $reportcard_db.RC_COMMENT_BANK Modify Comment text Default NULL;";
		$sql_alter[] = "ALTER Table $reportcard_db.RC_COMMENT_BANK Modify CommentEng text Default NULL;";
		
		// 2009-08-11 Ivan (For Marksheet Verification)
		$sql_alter[] = "ALTER Table $reportcard_db.RC_MARKSHEET_FEEDBACK Add LastModifiedBy int(11) default NULL;";
		$sql_alter[] = "ALTER Table $reportcard_db.RC_MARKSHEET_FEEDBACK Add RecordStatus tinyint(1) default NULL;";
		
		// 2009-08-31 Marcus 
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_GRADING_SCHEME_RANGE ADD AdditionalCriteria float DEFAULT 0";
		
		// 2009-09-02 Marcus 
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_MARKSHEET_FEEDBACK MODIFY RecordStatus VARCHAR(25) DEFAULT NULL";
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_MARKSHEET_FEEDBACK MODIFY Comment TEXT DEFAULT NULL";
		
		// 2009-09-14 Ivan 
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_REPORT_TEMPLATE MODIFY Semester char(8) DEFAULT NULL";
		
		// 2009-09-15 Ivan (For Archive Report Card)
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_REPORT_TEMPLATE Add LastArchived datetime DEFAULT NULL";
		
		// 2009-10-30 Ivan (SubMS - transfer overall mark to marksheet)
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_SUB_MARKSHEET_COLUMN ADD TransferToMS tinyint(2) DEFAULT 0 AFTER Calculation;";
		
		// 2009-11-17 Ivan (for HKUGA Primary School Grade - Very Long Grade Name)
		$sql_alter[] = "ALTER Table $reportcard_db.RC_GRADING_SCHEME_RANGE Modify Grade varchar(255) Default NULL;";
		$sql_alter[] = "ALTER Table $reportcard_db.RC_REPORT_RESULT_SCORE Modify Grade varchar(255) Default NULL;";

		// 2009-11-17 Ivan (for HKUGA Primary School Grade - Subjects in different Term has different Course Description)
		$sql_alter[] = "Alter Table $reportcard_db.RC_CURRICULUM_EXPECTATION Add COLUMN TermID INT(11) Default 0 AFTER SubjectID;";
		// 2009-11-17 Ivan (for HKUGA Primary School - Display Subjects in different languages)
		$sql_alter[] = "Alter Table $reportcard_db.RC_SUBJECT_FORM_GRADING Add COLUMN LangDisplay varchar(128) Default Null After ScaleDisplay;";
		
		// 2009-11-27 Ivan (for Kiangsu-Chekiang College - Stream Position)
		$sql_alter[] = "Alter Table $reportcard_db.RC_REPORT_RESULT Add COLUMN OrderMeritStream int(4) Default Null After FormNoOfStudent;";
		$sql_alter[] = "Alter Table $reportcard_db.RC_REPORT_RESULT Add COLUMN StreamNoOfStudent int(4) Default Null After OrderMeritStream;";
		$sql_alter[] = "Alter Table $reportcard_db.RC_REPORT_RESULT_SCORE Add COLUMN OrderMeritStream int(4) Default Null After FormNoOfStudent;";
		$sql_alter[] = "Alter Table $reportcard_db.RC_REPORT_RESULT_SCORE Add COLUMN StreamNoOfStudent int(4) Default Null After OrderMeritStream;";
		
		// 2009-11-27 Ivan (for UCCKE - Subject Group Position)
		$sql_alter[] = "Alter Table $reportcard_db.RC_REPORT_RESULT_SCORE Add COLUMN OrderMeritSubjectGroup int(4) Default Null After StreamNoOfStudent;";
		$sql_alter[] = "Alter Table $reportcard_db.RC_REPORT_RESULT_SCORE Add COLUMN SubjectGroupNoOfStudent int(4) Default Null After OrderMeritSubjectGroup;";
		
		// 2009-12-18 Marcus (for Display Subject Group Option)
		$sql_alter[] = "alter table $reportcard_db.RC_SUBJECT_FORM_GRADING add column DisplaySubjectGroup varchar(128) AFTER LangDisplay";
		
		// 2009-12-21 Ivan (for Multiple Term Report)
		$sql_alter[] = "alter table $reportcard_db.RC_REPORT_TEMPLATE add column isMainReport tinyint(2) Default 1";
		
		// 2009-12-22 Ivan (for Position Display - Display position only if the mark is greater than the Average for some marks)
		$sql_alter[] = "alter table $reportcard_db.RC_REPORT_TEMPLATE add column GreaterThanAverageClass tinyint(3) Default 0";
		$sql_alter[] = "alter table $reportcard_db.RC_REPORT_TEMPLATE add column GreaterThanAverageForm tinyint(3) Default 0";
		
		// 2010-01-08 Marcus (for Report Template Settings - to block teacher from modifying mark sheet again)
		$sql_alter[] = "alter table $reportcard_db.RC_REPORT_TEMPLATE_COLUMN add column BlockMarksheet varchar(11) Default NULL AFTER PositionRange ";
		
		// 2010-01-14 Ivan (for Report Template Settings Step 4 - Display Subject Component or not)
		$sql_alter[] = "alter table $reportcard_db.RC_REPORT_TEMPLATE add column ShowSubjectComponent tinyint(1) Default 1";
		
		// 2010-02-11 Marcus (for Carmel Alison to show Score in GrandMS and Rank Ordering)
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_REPORT_RESULT ADD COLUMN GrandSDScore FLOAT DEFAULT NULL AFTER AdjustedBy";
		
		// 2010-02-11 Marcus (for Carmel Alison to show Score in GrandMS and Rank Ordering)
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_REPORT_RESULT_SCORE ADD COLUMN SDScore FLOAT DEFAULT NULL AFTER SubjectGroupNoOfStudent";
		
		// 2010-02-19 Ivan (Add index to Grading Scheme Tables)
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_SUBJECT_FORM_GRADING Add INDEX ClassLevelID (ClassLevelID)";
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_SUBJECT_FORM_GRADING Add INDEX SchemeID (SchemeID)";
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_GRADING_SCHEME_RANGE Add INDEX Nature (Nature)";
		
		// 2010-03-01 Ivan (Add Display Order in Personal Characteristics)
		$sql_alter[] = "Alter Table $reportcard_db.RC_PERSONAL_CHARACTERISTICS Add DisplayOrder int(3) Default Null After Title_CH";
		$sql_alter[] = "Alter Table $reportcard_db.RC_PERSONAL_CHARACTERISTICS Add INDEX DisplayOrder (DisplayOrder)";

		// 2010-03-02 Marcus (Add LastPromotionUpdate to store promotion generation date)
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_REPORT_TEMPLATE ADD COLUMN LastPromotionUpdate DATETIME DEFAULT NULL";
		
		// 2010-03-31 Ivan (Add customized Term Start & End Date for Munsang College F5 & F7 - retrieve data from different modules)
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_REPORT_TEMPLATE ADD COLUMN TermStartDate DATE DEFAULT NULL";
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_REPORT_TEMPLATE ADD COLUMN TermEndDate DATE DEFAULT NULL";
		
		// 2010-04-07 Marcus (Add FullMark to RC_SUB_MARKSHEET_COLUMN for munsang )
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_SUB_MARKSHEET_COLUMN ADD COLUMN FullMark float Default NULL AFTER TransferToMS";
		
		// 2010-05-12 Ivan (Add LastGeneratedAcademicProgress to store academic progress generation date)
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_REPORT_TEMPLATE ADD COLUMN LastGeneratedAcademicProgress DATETIME DEFAULT NULL";
		
		// 2010-05-13 Ivan (Add index)
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_REPORT_RESULT_SCORE Add INDEX ReportColumnID (ReportColumnID)";
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_REPORT_RESULT_SCORE Add INDEX Mark (Mark)";
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_REPORT_RESULT Add INDEX GrandTotal (GrandTotal)";
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_REPORT_RESULT Add INDEX GrandAverage (GrandAverage)";
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_REPORT_RESULT Add INDEX GPA (GPA)";
		
		// 2010-05-20 Ivan (Add RawSDScore)
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_REPORT_RESULT_SCORE ADD COLUMN RawSDScore float Default NULL AFTER SDScore";
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_REPORT_RESULT ADD COLUMN RawGrandSDScore float Default NULL AFTER GrandSDScore";

		// 2010-05-24 Marcus (Set Default Value of Promotion to 1)
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_REPORT_RESULT ALTER COLUMN Promotion SET DEFAULT 1";
		
		// 2010-05-25 Ivan (Change the Academic Progress Records as Float)
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_STUDENT_ACADEMIC_PROGRESS CHANGE FromScore FromScore float DEFAULT NULL";
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_STUDENT_ACADEMIC_PROGRESS CHANGE ToScore ToScore float DEFAULT NULL";
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_STUDENT_ACADEMIC_PROGRESS CHANGE ScoreDifference ScoreDifference float DEFAULT NULL";
		
		// 2010-06-04 Ivan (Add Unique Key to prevent duplicated Subject-GradingScheme Relationship records)
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_SUBJECT_FORM_GRADING Add Unique Index SubjectFormScheme (SubjectID, ClassLevelID, SchemeID)";
		
		// 2010-06-08 Ivan (Add AcademicProgressPrize field to record if the student get this prize)
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_STUDENT_ACADEMIC_PROGRESS Add AcademicProgressPrize tinyint(1) default 0 After OrderFormRankDifference";
		
		// 2010-06-28 Ivan (Add Unique Key to prevent duplicated Subject Mark records)
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_REPORT_RESULT_SCORE Add Unique Index Report_Column_Subject_Student (ReportID , ReportColumnID , SubjectID, StudentID)";
		
		// 2010-07-06 Marcus (Add ShowPosition to control Subject Position Display)
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_POSITION_DISPLAY_SETTING ADD COLUMN ShowPosition tinyint(1) DEFAULT 1 AFTER RecordType";
		
		// 2010-08-09 Marcus (change Footer data type from varchar(255) to text to store longer string)
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_REPORT_TEMPLATE MODIFY Footer TEXT";

		// 2010-10-26 Marcus (Drop Unique Key to cater template based grading scheme)
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_SUBJECT_FORM_GRADING DROP Index SubjectFormScheme";
		
		// 2010-11-05 Ivan (Add Subject Group Info in Personal Characteristics and Add related indice)
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_PERSONAL_CHARACTERISTICS_DATA Add Column SubjectGroupID int(8) default 0 After SubjectID,
																					Add Index SubjectGroupID (SubjectGroupID),
																					Add Index ReportID (ReportID),
																					Add Index TeacherID (TeacherID)
						";

		// 2010-11-15 Ivan (Add AttendanceDays to store the Attendance Days for Wah Yan College Kowloon)
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_REPORT_TEMPLATE ADD COLUMN AttendanceDays int(5) default 0";
		
		// 2011-05-12 Ivan ("text" is not big enough to store hkuga_college html => change to "mediumtext")
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_REPORT_CARD_ARCHIVE MODIFY ReportCardHTML MEDIUMTEXT DEFAULT NULL";
		
		// 2011-05-23 Ivan (Add LastAwardGenerate)
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_REPORT_TEMPLATE ADD COLUMN LastGeneratedAward datetime Default Null AFTER LastGeneratedAcademicProgress;";

		// 2011-05-30 Marcus (Add Submission Date for personal characteristic)
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_REPORT_TEMPLATE ADD COLUMN PCSubmissionStart datetime default NULL AFTER MarksheetVerificationEnd";
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_REPORT_TEMPLATE ADD COLUMN PCSubmissionEnd datetime default NULL AFTER PCSubmissionStart";
		
		// 2011-05-31 Ivan (Award Generation)
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_TEMP_IMPORT_AWARD ADD COLUMN ReportID int(8) default NULL AFTER RowNumber";
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_TEMP_IMPORT_AWARD DROP Index ImportUserIDAndRowNumber";
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_TEMP_IMPORT_AWARD ADD Index ImportUserAndReportAndRowNumber (ImportUserID, ReportID, RowNumber)";
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_AWARD_CRITERIA MODIFY ImprovementField varchar(64) default NULL COMMENT 'For IMPROVEMENT: Mark, GrandAverage, GrandTotal, GPA, GrandSDScore, OrderMeritClass, OrderMeritForm, OrderMeritSubjectGroup'";
		
		// 2011-06-09 Marcus
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_GRADING_SCHEME_RANGE ADD COLUMN Description VARCHAR(255) default NULL AFTER AdditionalCriteria";
		
		// 2011-07-26 Marcus
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_PERSONAL_CHARACTERISTICS_DATA ADD COLUMN Comment TEXT DEFAULT NULL AFTER CharData";
		
		// 2011-08-15 Ivan
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_MARKSHEET_COMMENT ADD COLUMN AdditionalComment mediumtext DEFAULT NULL AFTER Comment";
		
		// 2011-08-24 Marcus
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_SUB_MARKSHEET_COLUMN DROP Index ColumnTitle";
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_SUB_MARKSHEET_COLUMN MODIFY ColumnTitle TEXT DEFAULT NULL";
		
		// 2011-08-29 Ivan
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_MARKSHEET_SCORE Add Column IsEstimated tinyint(2) Default 0";
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_MARKSHEET_OVERALL_SCORE Add Column IsEstimated tinyint(2) Default 0";
		
		// 2011-09-08 Ivan
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_REPORT_TEMPLATE Add Column LastGeneratedBy int(8) Default Null";
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_COMMENT_BANK Add Column IsAdditional tinyint(2) Default 0, Add Index IsAdditional (IsAdditional)";
		
		// 2011-10-07 Ivan (Award Generation)
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_TEMP_IMPORT_AWARD ADD COLUMN AwardName varchar(255) default NULL AFTER AwardText,
																		ADD COLUMN AwardCode varchar(255) default NULL AFTER AwardName,
																		ADD COLUMN AwardID int(8) default NULL AFTER AwardCode,
																		ADD COLUMN SubjectName varchar(255) default NULL AFTER AwardID,
																		ADD COLUMN SubjectWebSAMSCode varchar(255) default NULL AFTER SubjectName,
																		ADD COLUMN SubjectID int(8) default NULL AFTER SubjectWebSAMSCode
						";		
		
		// 2011-10-13 Ivan
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_AWARD_GENERATED_STUDENT_RECORD Add Column NumOfEstimatedScore int(4) Default 0 After ToNature";
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_AWARD Add Column ShowInReportCard tinyint(2) Default 1 After ApplicableReportType";
		
		// 2011-11-23 Marcus
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_OTHER_INFO_STUDENT_RECORD Add Column ClassID int(11) NOT NULL After ItemCode, Add Index ClassID (ClassID)";
		
		// 2012-01-13 Marcus
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_CONDUCT ADD UNIQUE INDEX Conduct (Conduct)";
		
		// 2012-01-18 Ivan
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_REPORT_TEMPLATE Add Column SpecialPercentage tinyint(2) Default 0 After AttendanceDays";
		
		
		// 2012-03-06 [Please follow this format afterwards]
//		$sql_alter[] = array(
//			"2012-03-06",
//			"Drop test eRC DB schema table",
//			"Drop Table $reportcard_db.RC_TEMP"
//		);

		// 2012-03-16
		$sql_alter[] = array(
			"2012-03-16",
			"Alter Table RC_POSITION_DISPLAY_SETTING Add GreaterThanMark ",
			"ALTER TABLE $reportcard_db.RC_POSITION_DISPLAY_SETTING Add Column GreaterThanMark tinyint(3) default NULL AFTER GreaterThanAverage"
		);
		
		$sql_alter[] = array(
			"2012-06-22",
			"Alter Table RC_REPORT_TEMPLATE Add DisplayOption ",
			"ALTER TABLE $reportcard_db.RC_REPORT_TEMPLATE Add Column DisplayOption tinyint(3) default NULL"
		);
		
		$sql_alter[] = array(
			"2012-09-03",
			"Alter Table RC_REPORT_TEMPLATE Add ApplySpecificMarksheetSubmission ",
			"ALTER TABLE $reportcard_db.RC_REPORT_TEMPLATE Add Column ApplySpecificMarksheetSubmission tinyint(1) default 0"
		);
		
		$sql_alter[] = array(
			"2012-12-14",
			"Alter Table RC_SUBJECT_FORM_GRADING Add ModifiedBy ",
			"ALTER TABLE $reportcard_db.RC_SUBJECT_FORM_GRADING Add Column ModifiedBy int(11) default null"
		);
		
		$sql_alter[] = array(
			"2013-02-06",
			"Alter Table RC_REPORT_RESULT Add ActualAverage",
			"ALTER TABLE $reportcard_db.RC_REPORT_RESULT Add Column ActualAverage float(6,2) default null after GrandAverage"
		);
		
		$sql_alter[] = array(
			"2013-06-18",
			"Alter Table RC_REPORT_TEMPLATE_COLUMN add column TermReportColumnID",
			"Alter Table $reportcard_db.RC_REPORT_TEMPLATE_COLUMN Add COLUMN TermReportColumnID int(11) Default Null;"
		);
		
		$sql_alter[] = array(
			"2013-07-16",
			"Alter Table RC_TEMP_IMPORT_CLASS_TEACHER_COMMENT add column ReportID and InputDate",
			"ALTER Table $reportcard_db.RC_TEMP_IMPORT_CLASS_TEACHER_COMMENT Add COLUMN ReportID int(8) Default NULL,
																Add COLUMN InputDate datetime Default NULL"
		);
		
		$sql_alter[] = array(
			"2014-08-04",
			"Alter Table RC_REPORT_RESULT add column RawGrandAverage",
			"ALTER Table $reportcard_db.RC_REPORT_RESULT Add COLUMN RawGrandAverage float default NULL"
		);
		
		$sql_alter[] = array(
			"2014-10-23",
			"Alter Table RC_SUBJECT_FORM_GRADING add column ReportID",
			"ALTER TABLE $reportcard_db.RC_SUBJECT_FORM_GRADING ADD COLUMN ReportID INT(11) DEFAULT 0 NOT NULL,  ADD UNIQUE KEY SubjectReportScheme (SubjectID,ClassLevelID,ReportID)"
		);
		
		$sql_alter[] = array(
			"2015-01-15",
			"Alter Table RC_REPORT_TEMPLATE add column PrincipalName",
			"ALTER TABLE $reportcard_db.RC_REPORT_TEMPLATE ADD COLUMN PrincipalName varchar(255) Default NULL"
		);
		
		$sql_alter[] = array(
			"2015-02-13",
			"Alter Table RC_SUB_MARKSHEET_COLUMN add column PassMark",
			"ALTER TABLE $reportcard_db.RC_SUB_MARKSHEET_COLUMN ADD COLUMN PassMark float Default NULL AFTER FullMark"
		);
		
		$sql_alter[] = array(
			"2015-03-13",
			"Alter Table RC_REPORT_TEMPLATE add column LastDateExportToWebSAMS",
			"ALTER TABLE $reportcard_db.RC_REPORT_TEMPLATE ADD COLUMN LastDateExportToWebSAMS datetime default NULL"
		);
		
		$sql_alter[] = array(
			"2015-05-14",
			"Alter Table TEMP_SUBJECT_TOPIC_IMPORT add column SubjectCmpCode and SubjectCmpName",
			"ALTER TABLE $reportcard_db.TEMP_SUBJECT_TOPIC_IMPORT 	ADD COLUMN SubjectCmpCode varchar(255) NOT NULL after SubjectName, 
																	ADD COLUMN SubjectCmpName varchar(255) NOT NULL after SubjectCmpCode"
		);
		$sql_alter[] = array(
			"2015-05-14",
			"Alter Table TEMP_SUBJECT_TOPIC_IMPORT add column SubjectCmpID",
			"ALTER TABLE $reportcard_db.TEMP_SUBJECT_TOPIC_IMPORT 	ADD COLUMN SubjectCmpID int(11) NOT NULL after SubjectName"
		);
		
		$sql_alter[] = array(
			"2015-07-17",
			"Alter Table RC_REPORT_TEMPLATE add column ReportSequence",
			"ALTER TABLE $reportcard_db.RC_REPORT_TEMPLATE ADD COLUMN ReportSequence int(11) Default NULL"
		);
		
		$sql_alter[] = array(
			"2015-12-21",
			"Alter Table RC_MARKSHEET_SPECIFIC_USER_SUBMISSION_PERIOD add column SubjectID",
			"ALTER TABLE $reportcard_db.RC_MARKSHEET_SPECIFIC_USER_SUBMISSION_PERIOD ADD COLUMN SubjectID int(8) Default 0 after UserID"
		);
		
		$sql_alter[] = array(
			"2015-12-23",
			"Alter table RC_MARKSHEET_SPECIFIC_USER_SUBMISSION_PERIOD drop unique key ReportIDUserID",
			"ALTER TABLE $reportcard_db.RC_MARKSHEET_SPECIFIC_USER_SUBMISSION_PERIOD drop key ReportIDUserID"
		);
		$sql_alter[] = array(
			"2015-12-23",
			"Alter table RC_MARKSHEET_SPECIFIC_USER_SUBMISSION_PERIOD add unique key ReportUserSubject",
			"ALTER TABLE $reportcard_db.RC_MARKSHEET_SPECIFIC_USER_SUBMISSION_PERIOD add unique key ReportUserSubject (ReportID, UserID, SubjectID)"
		);
		
		$sql_alter[] = array(
			"2016-02-16",
			"Alter table RC_REPORT_RESULT_SCORE add column to store SD score",
			"ALTER TABLE $reportcard_db.RC_REPORT_RESULT_SCORE ADD COLUMN OrderMeritClassBySD int(11) default NULL,
															   ADD COLUMN OrderMeritFormBySD int(11) default NULL,
															   ADD COLUMN OrderMeritStreamBySD int(4) default NULL,
															   ADD COLUMN OrderMeritSubjectGroupBySD int(4) default NULL"
		);
		$sql_alter[] = array(
			"2016-02-16",
			"Alter table RC_REPORT_RESULT add column to store SD score",
			"ALTER TABLE $reportcard_db.RC_REPORT_RESULT ADD COLUMN OrderMeritClassBySD int(11) default NULL,
														 ADD COLUMN OrderMeritFormBySD int(11) default NULL,
													     ADD COLUMN OrderMeritStreamBySD int(4) default NULL"
		);
		
		$sql_alter[] = array(
			"2016-06-14",
			"Alter table RC_AWARD add column to store internal award code",
			"ALTER TABLE $reportcard_db.RC_AWARD ADD COLUMN InternalAwardCode varchar(64) default NULL"
		);
		
		$sql_alter[] = array(
			"2016-10-06",
			"Alter Table RC_MARKSHEET_SPECIFIC_USER_SUBMISSION_PERIOD add column SubjectID",
			"ALTER TABLE $reportcard_db.RC_MARKSHEET_SPECIFIC_USER_SUBMISSION_PERIOD ADD COLUMN SubjectGroupID int(8) Default 0 after SubjectID"
		);
		
		$sql_alter[] = array(
			"2016-10-06",
			"Alter table RC_MARKSHEET_SPECIFIC_USER_SUBMISSION_PERIOD drop unique key ReportIDUserID",
			"ALTER TABLE $reportcard_db.RC_MARKSHEET_SPECIFIC_USER_SUBMISSION_PERIOD drop key ReportUserSubject"
		);
		$sql_alter[] = array(
			"2016-10-06",
			"Alter table RC_MARKSHEET_SPECIFIC_USER_SUBMISSION_PERIOD add unique key ReportUserSubject",
			"ALTER TABLE $reportcard_db.RC_MARKSHEET_SPECIFIC_USER_SUBMISSION_PERIOD add unique key ReportUserSubjectGroup (ReportID, UserID, SubjectID, SubjectGroupID)"
		);
			
		$sql_alter[] = array(
			"2016-11-17",
			"Alter table RC_REPORT_TEMPLATE add column CTCSubmissionStart, CTCSubmissionEnd",
			"ALTER TABLE $reportcard_db.RC_REPORT_TEMPLATE ADD COLUMN CTCSubmissionStart datetime DEFAULT NULL,
														   ADD COLUMN CTCSubmissionEnd datetime DEFAULT NULL"
		);
		
		$sql_alter[] = array(
				"2017-01-04",
				"Alter table RC_SUB_MARKSHEET_COLUMN add column SubLevel",
				"ALTER TABLE $reportcard_db.RC_SUB_MARKSHEET_COLUMN ADD COLUMN SubLevel int(8) default 0"
		);
		$sql_alter[] = array(
			"2017-01-04",
			"Alter table RC_SUB_MARKSHEET_COLUMN add column fromColumnID",
			"ALTER TABLE $reportcard_db.RC_SUB_MARKSHEET_COLUMN ADD COLUMN fromColumnID int(8) default NULL"
		);
		
		$sql_alter[] = array(
			"2017-01-04",
			"Alter table RC_SUB_MARKSHEET_SCORE_SCHEME add column Grade",
			"ALTER TABLE $reportcard_db.RC_SUB_MARKSHEET_SCORE_SCHEME ADD COLUMN Grade varchar(255) default NULL AFTER LowerLimit"
		);
		
		$sql_alter[] = array(
			"2017-03-22",
			"Alter table RC_REPORT_TEMPLATE add column PublishSubmissionStart, PublishSubmissionEnd",
			"ALTER TABLE $reportcard_db.RC_REPORT_TEMPLATE ADD COLUMN PublishSubmissionStart datetime DEFAULT NULL,
														   ADD COLUMN PublishSubmissionEnd datetime DEFAULT NULL"
		);
		
		$sql_alter[] = array(
			"2017-04-12",
			"Alter table RC_PROMOTION_RETENTION_FAILED_SUBJECT",
			"ALTER TABLE $reportcard_db.RC_PROMOTION_RETENTION_FAILED_SUBJECT MODIFY COLUMN SubjectType tinyint(4) NOT NULL default '1' COMMENT '1: Failed, 2: In Subject Retention, 3: Ended Subject Retention'"
		);
		
		$sql_alter[] = array(
			"2017-04-12",
			"Alter table RC_PROMOTION_RETENTION_FAILED_SUBJECT add column ClassLevelID",
			"ALTER TABLE $reportcard_db.RC_PROMOTION_RETENTION_FAILED_SUBJECT ADD COLUMN ClassLevelID int(11) NOT NULL AFTER ReportID"
		);
		
		$sql_alter[] = array(
			"2017-05-02",
			"Alter table RC_REPORT_RESULT_SCORE_ARCHIVE add column InputBy, ModifiedBy",
			"ALTER TABLE $reportcard_db.RC_REPORT_RESULT_SCORE_ARCHIVE ADD COLUMN InputBy int(11) default NULL,
																		 ADD COLUMN ModifiedBy int(11) default NULL"
		);
		
		$sql_alter[] = array(
			"2017-09-22",
			"Alter table RC_GROUP_MEMBER add column InputBy, InputDate",
			"ALTER TABLE $reportcard_db.RC_GROUP_MEMBER ADD COLUMN InputBy int(11) default NULL,
														 ADD COLUMN InputDate datetime default NULL"
		);
		
		$sql_alter[] = array(
			"2018-01-05",
			"Alter Table RC_SUBJECT_TOPIC to increase storage size of subject topic",
			"ALTER TABLE $reportcard_db.RC_SUBJECT_TOPIC MODIFY NameEn varchar(512) NOT NULL,
															MODIFY NameCh varchar(512) NOT NULL"
		);
		
		$sql_alter[] = array(
			"2018-01-08",
			"Alter Table TEMP_SUBJECT_TOPIC_IMPORT to increase temp storage size of subject topic when import",
			"ALTER TABLE $reportcard_db.TEMP_SUBJECT_TOPIC_IMPORT MODIFY SubjectTopicNameEn varchar(512) NOT NULL,
																MODIFY SubjectTopicNameCh varchar(512) NOT NULL"
		);
		
		$sql_alter[] = array(
			"2018-02-05",
			"Alter Table RC_REPORT_TEMPLATE to store additional report settings",
			"ALTER TABLE $reportcard_db.RC_REPORT_TEMPLATE ADD COLUMN DisplayCommentSection tinyint(1) DEFAULT 1,
															ADD COLUMN DisplayAwardSection tinyint(1) DEFAULT 1,
															ADD COLUMN MaxActivityCount int(5) DEFAULT '0',
															ADD COLUMN DisplayActivitySection tinyint(1) DEFAULT 1"
		);
		
		$sql_alter[] = array(
		    "2018-06-19",
		    "Alter Table RC_SUBJECT_FORM_CREDIT",
		    "ALTER TABLE $reportcard_db.RC_SUBJECT_FORM_CREDIT ADD COLUMN Credit float NULL"
		);
		
		$sql_alter[] = array(
		    "2019-02-28",
		    "Alter Table RC_COMMENT_BANK to add related grade of comment",
		    "ALTER TABLE $reportcard_db.RC_COMMENT_BANK ADD COLUMN RelatedGrade varchar(255) DEFAULT NULL"
		);
		
		$sql_alter[] = array(
		    "2019-03-01",
		    "Alter Table RC_MARKSHEET_COMMENT to add field to store CommentID",
		    "ALTER TABLE $reportcard_db.RC_MARKSHEET_COMMENT ADD COLUMN CommentIDList mediumtext DEFAULT NULL"
		);

        $sql_alter[] = array(
            "2020-10-20",
            "Alter Table RC_PERSONAL_CHARACTERISTICS to add field to store Description",
            "ALTER TABLE $reportcard_db.RC_PERSONAL_CHARACTERISTICS ADD COLUMN Description text DEFAULT NULL AFTER Title_CH"
        );

        $sql_alter[] = array(
            "2020-11-02",
            "Alter Table RC_SUBJECT_TOPIC to add field to store term setting",
            "ALTER TABLE $reportcard_db.RC_SUBJECT_TOPIC ADD COLUMN TargetTermSeq int(4) DEFAULT '0'"
        );

        $sql_alter[] = array(
            "2020-11-02",
            "Alter Table TEMP_SUBJECT_TOPIC_IMPORT to add field to store term setting",
            "ALTER TABLE $reportcard_db.TEMP_SUBJECT_TOPIC_IMPORT ADD COLUMN TargetTermSeq int(4) DEFAULT '0'"
        );
        
		# alter tables
//		for($i=0; $i<sizeof($sql_alter); $i++){
//			$sql = $sql_alter[$i];
//			if($li->db_db_query($sql)){
//				echo "<p>Altered table: ".$sql."</p>\n";
//			} else {
//				echo "<p>Failed to alter table:<br />".$sql."<br />Error: ".mysql_error()."</p>\n";
//			}
//		}
		$numOfAlterSql = count((array)$sql_alter);
		for($i=0; $i<$numOfAlterSql; $i++){
			$sql = $sql_alter[$i];
			
			if (is_array($sql)) {
				// new logic => have input date, desc, and the sql statement
				updateSchema_eRC($flag, $last_schema_date_updated, $sql[0], $sql[1], $sql[2]);
			}
			else {
				// old logic => only have the sql statement
				if ($flag==2) {
					// will not include old schema update for "update new schema only"
				}
				else {
					if($li->db_db_query($sql)){
						echo "<p>Altered table: ".$sql."</p>\n";
					} else {
						echo "<p>Failed to alter table:<br />".$sql."<br />Error: ".mysql_error()."</p>\n";
					}
				}
			}
		}
		
		
		### Personal Characteristics Display Order Patch
		$sql_update_personal_char_display_order = "Update $reportcard_db.RC_PERSONAL_CHARACTERISTICS set DisplayOrder = CharID Where DisplayOrder is Null";
		$li->db_db_query($sql_update_personal_char_display_order);
		
		
		if ($_REQUEST['direct_run']==1 || $flag2==1) {
			# Run data patch 
			include_once($eRC_Addon_PATH_WRT_ROOT."includes/libgeneralsettings.php");
			$lgs = new libgeneralsettings();
			$ModuleName = "eRCDataPatch";
			$GSary = $lgs->Get_General_Setting($ModuleName);
	
			$eRC_x = '';
			$eRC_x .= '<p>Run data patch start............</p><br>';
			
//			###################################################
//			### Remove Duplicate Grading Scheme [Start]
//			###################################################
//			/*******************************************************************************************
//			 * !!!NOTE: DO NOT RE-EXECUTE THIS PART after $GSary['TemplateBasedGradingSchemeScript_'.$reportcard_db] run,
//			 * 			Otherwise, some template based grading scheme will be LOST.
//			 ******************************************************************************************/
//			$sql = "DESC $reportcard_db.RC_SUBJECT_FORM_GRADING ";
//			$tmp = $li->returnArray($sql);
//	 		$field = Get_Array_By_Key($tmp,"Field");
//	 		
//	 		// if ReportID exist, don't need to run these 2 script
//			if (!in_array("ReportID", $field) && !$GSary['RemoveDuplicateGradingScheme_'.$reportcard_db] && !$GSary['TemplateBasedGradingSchemeScript_'.$reportcard_db])
//			{
//				# backup RC_SUBJECT_FORM_GRADING for precaution
//				$sql = "CREATE TABLE IF NOT EXISTS $reportcard_db.RC_SUBJECT_FORM_GRADING_".date("YmdHis")." SELECT * FROM $reportcard_db.RC_SUBJECT_FORM_GRADING";
//				$li->db_db_query($sql);
//				
//				$eRC_x .= "===================================================================<br>\r\n";
//				$eRC_x .= "Remove Duplicate Grading Scheme  [Start]</b><br>\r\n";
//				$eRC_x .= "===================================================================<br>\r\n";
//				
//				$sql = "
//					SELECT  
//						ClassLevelID, 
//						SubjectID, 
//						MAX(SubjectFormGradingID)  
//					FROM 
//						$reportcard_db.RC_SUBJECT_FORM_GRADING 
//					GROUP BY 
//						ClassLevelID, 
//						SubjectID 
//					HAVING 
//						COUNT(*)>1  
//				";
//				$DuplicateRecord = $li->returnArray($sql);
//				
//				foreach((array)$DuplicateRecord as $thisRecord)
//				{
//					list($thisClassLevelID,$thisSubjectID, $keepSubjectFormGrading) = $thisRecord;
//					$sql = " 
//						DELETE FROM 
//							$reportcard_db.RC_SUBJECT_FORM_GRADING 
//						WHERE 
//							ClassLevelID = $thisClassLevelID
//							AND SubjectID = $thisSubjectID
//							AND SubjectFormGradingID <> $keepSubjectFormGrading
//					";
//					$li->db_db_query($sql);
//				}
//				
//				$success = $li->db_db_query($sql)?1:0;
//				
//				if($success)
//					$eRC_x .= "Remove Duplicate Grading Scheme  <font color='blue'>Success</font>.<br><br>\r\n";
//				else
//				{
//					$eRC_x .= $sql."<br>".mysql_error()."<br>";
//					$eRC_x .= "Remove Duplicate Grading Scheme  <font color='red'>Fail</font>.<br><br>\r\n";
//				}
//				# update General Settings - markd the script is executed
//				$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName','RemoveDuplicateGradingScheme_$reportcard_db', 1, now()) ";
//				$li->db_db_query($sql) or debug_pr($sql."\n".mysql_error());
//						
//				$eRC_x .= "===================================================================<br>\r\n";
//				$eRC_x .= "Remove Duplicate Grading Scheme  [End]</b><br>\r\n";
//				$eRC_x .= "===================================================================<br>\r\n";
//			}
//			###################################################
//			### Remove Duplicate Grading Scheme [End]
//			###################################################		
//			
//			###################################################
//			### Copy grading scheme to cater template based scheme [Start]
//			###################################################
//			if (!in_array("ReportID", $field) && !$GSary['TemplateBasedGradingSchemeScript_'.$reportcard_db])
//			{
//				// 2010-09-24 Marcus (alter RC_SUBJECT_FORM_GRADING add ReportID, cater template based Grading Scheme Settings)
//				$sql = "ALTER TABLE $reportcard_db.RC_SUBJECT_FORM_GRADING ADD COLUMN ReportID INT(11) DEFAULT 0 NOT NULL,  ADD UNIQUE KEY SubjectReportScheme (SubjectID,ClassLevelID,ReportID) ";
//				if(!$li->db_db_query($sql))
//					$eRC_x .=$sql."<br>".mysql_error()."<br>";
//				
//				$sql = "
//					INSERT INTO
//						$reportcard_db.RC_SUBJECT_FORM_GRADING
//						(
//							ReportID,
//							SubjectID,
//							ClassLevelID,
//							DisplayOrder,
//							SchemeID,
//							ScaleInput,
//							ScaleDisplay,
//							LangDisplay,
//							DisplaySubjectGroup,
//							DateInput,
//							DateModified
//						)
//					SELECT 
//						rt.ReportID,
//						sfg.SubjectID,
//						sfg.ClassLevelID,
//						sfg.DisplayOrder,
//						sfg.SchemeID,
//						sfg.ScaleInput,
//						sfg.ScaleDisplay,
//						sfg.LangDisplay,
//						sfg.DisplaySubjectGroup,
//						NOW() as DateInput,
//						NOW() as DateModified
//					FROM 
//						$reportcard_db.RC_REPORT_TEMPLATE rt 
//						LEFT OUTER JOIN $reportcard_db.RC_SUBJECT_FORM_GRADING sfg ON rt.ClassLevelID = sfg.ClassLevelID
//					WHERE 
//						sfg.ReportID = 0
//				";
//				$success = $li->db_db_query($sql)?1:0;
//				
//				if($success)
//					$eRC_x .= "Copy grading scheme to cater template based scheme <font color='blue'>Success</font>.<br><br>\r\n";
//				else
//				{
//					$eRC_x .= $sql."<br>".mysql_error()."<br>";
//					$eRC_x .= "Copy grading scheme to cater template based scheme <font color='red'>Fail</font>.<br><br>\r\n";
//				}
//				# update General Settings - markd the script is executed
//				$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'TemplateBasedGradingSchemeScript_$reportcard_db', 1, now()) ";
//				$li->db_db_query($sql) or debug_pr($sql."\n".mysql_error());
//						
//				$eRC_x .= "===================================================================<br>\r\n";
//				$eRC_x .= "Copy grading scheme to cater template based scheme [End]</b><br>\r\n";
//				$eRC_x .= "===================================================================<br>\r\n";
//			}
//			###################################################
//			### Copy grading scheme to cater template based scheme [End]
//			###################################################
//			
//			
//			###################################################
//			### Migrate Personal Characteristics from customized library constructor to DB [Start]
//			###################################################
//			// $ScaleArr[$ScaleCode] = $LangKey
//			$ScaleArr = $lreportcard->PersonalCharGradeArr;
//			if (!$GSary['MigratePersonalCharScaleToDbScript_'.$reportcard_db] && $sys_custom['eRC']['Settings']['PersonalCharacteristics'] && is_array($ScaleArr))
//			//if ($reportcard_db == 'IP25_DB_REPORT_CARD_2009')
//			{
//				$eRC_x .= "===================================================================<br>\r\n";
//				$eRC_x .= "Migrate Personal Characteristics from customized library constructor to DB [Start]</b><br>\r\n";
//				$eRC_x .= "===================================================================<br>\r\n";
//				
//				$numOfScale = count((array)$ScaleArr);
//				$DefaultScaleCode = $lreportcard->DefaultPersonalCharGrade;
//				$DisplayCharItemInReverseOrder = $lreportcard->DisplayCharItemInReverseOrder;
//				
//				$ScaleCounter = 0;
//				$thisPatchSuccessArr = array();
//				foreach((array)$ScaleArr as $thisScaleCode => $thisLangKey) {
//					$ScaleCounter++;
//					
//					$thisDisplayEn = $eReportCard[$thisLangKey.'_en'];
//					$thisDisplayCh = $eReportCard[$thisLangKey.'_b5'];
//					$thisIsDefault = ($thisScaleCode==$DefaultScaleCode)? 1 : 0;
//					$thisDisplayOrder = ($DisplayCharItemInReverseOrder)? $numOfScale - $ScaleCounter + 1 : $ScaleCounter;
//					
//					
//					$sql = "Insert Into	$reportcard_db.RC_PERSONAL_CHARACTERISTICS_SCALE
//									(Code, NameEn, NameCh, IsDefault, DisplayOrder)
//							Values
//									('".$li->Get_Safe_Sql_Query($thisScaleCode)."', '".$li->Get_Safe_Sql_Query($thisDisplayEn)."', '".$li->Get_Safe_Sql_Query($thisDisplayCh)."', '".$thisIsDefault."', '".$thisDisplayOrder."')
//							";
//					$thisPatchSuccessArr[$thisScaleCode] = $li->db_db_query($sql)? 1 : 0;
//				}
//				
//				if(in_array(false, $thisPatchSuccessArr)) {
//					$eRC_x .= $sql."<br>".mysql_error()."<br>";
//					$eRC_x .= "Migrate Personal Characteristics Scale To Db <font color='red'>Fail</font>.<br><br>\r\n";
//				}
//				else {
//					$eRC_x .= "Migrate Personal Characteristics Scale To Db <font color='blue'>Success</font>.<br><br>\r\n";
//					
//					// back up data table
//					$sql = "CREATE TABLE IF NOT EXISTS $reportcard_db.RC_PERSONAL_CHARACTERISTICS_DATA_".date("YmdHis")." SELECT * FROM $reportcard_db.RC_PERSONAL_CHARACTERISTICS_DATA";
//					$thisPatchSuccessArr['BackupPersonalCharData'] = $li->db_db_query($sql);
//					
//					// get scale code and scale id mapping
//					$sql = "Select ScaleID, Code From $reportcard_db.RC_PERSONAL_CHARACTERISTICS_SCALE";
//					$ScaleInfoArr = $li->returnResultSet($sql);
//					$ScaleAssoArr = BuildMultiKeyAssoc($ScaleInfoArr, "Code", "ScaleID", $SingleValue=1);
//					
//					// get current personal char data
//					$sql = "Select CharRecordID, CharData From $reportcard_db.RC_PERSONAL_CHARACTERISTICS_DATA";
//					$PersonalCharDataArr = $li->returnResultSet($sql);
//					
//					$numOfData = count($PersonalCharDataArr);
//					for ($i=0; $i<$numOfData; $i++) {
//						$thisCharRecordID = $PersonalCharDataArr[$i]['CharRecordID'];
//						$thisCharData = $PersonalCharDataArr[$i]['CharData'];	// 2:10,3:10,4:40,5:60,6:50,1:50
//						
//						$thisCharDataArr = explode(',', $thisCharData);
//						$thisNumOfChar = count($thisCharDataArr);
//						
//						$thisNewCharDataArr = array();
//						for ($j=0; $j<$thisNumOfChar; $j++) {
//							$thisCharItemData = $thisCharDataArr[$j];
//							$thisCharItemDataArr = explode(':', $thisCharItemData);
//							
//							$thisCharID = $thisCharItemDataArr[0];
//							$thisScaleCode = $thisCharItemDataArr[1];
//							$thisScaleID = ($ScaleAssoArr[$thisScaleCode])? $ScaleAssoArr[$thisScaleCode] : $thisScaleCode;
//							
//							$thisNewCharDataArr[] = $thisCharID.':'.$thisScaleID;
//						}
//						$thisNewCharData = implode(',', $thisNewCharDataArr);
//						
//						$sql = "Update $reportcard_db.RC_PERSONAL_CHARACTERISTICS_DATA set CharData = '$thisNewCharData' where CharRecordID = '$thisCharRecordID'";
//						$thisPatchSuccessArr['UpdateNewPersonalCharData'][$thisCharRecordID] = $li->db_db_query($sql);
//					}
//					
//					if(in_array(false, $thisPatchSuccessArr)) {
//						$eRC_x .= $sql."<br>".mysql_error()."<br>";
//						$eRC_x .= "Personal Characteristics Data Patch From Scale Code to ScaleID <font color='red'>Fail</font>.<br><br>\r\n";
//					}
//					else {
//						$eRC_x .= "Personal Characteristics Data Patch From Scale Code to ScaleID <font color='blue'>Success</font>.<br><br>\r\n";
//					}
//				}
//				
//				# update General Settings - mark the script is executed
//				$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'MigratePersonalCharScaleToDbScript_$reportcard_db', 1, now()) ";
//				$li->db_db_query($sql) or debug_pr($sql."\n".mysql_error());
//						
//				$eRC_x .= "===================================================================<br>\r\n";
//				$eRC_x .= "Migrate Personal Characteristics from customized library constructor to DB [End]</b><br>\r\n";
//				$eRC_x .= "===================================================================<br>\r\n";
//			}
//			###################################################
//			### Migrate Personal Characteristics from customized library constructor to DB [End]
//			###################################################
//	
			###################################################
			### Migrate OtherInfo CSV Data Into DB [Start]
			###################################################
			$SettingName = 'MigrateOtherInfoCSVData_'.$reportcard_db;
			if (!$GSary[$SettingName])
			{
				$eRC_x .= "===================================================================<br>\r\n";
				$eRC_x .= "Migrate OtherInfo CSV Data Into DB [Start]</b><br>\r\n";
				$eRC_x .= "===================================================================<br>\r\n";
				
				# Active Year
				include_once($eRC_Addon_PATH_WRT_ROOT."includes/form_class_manage.php");
				$lib_AcademicYear = new academic_year();
				$AcademicYearInfoArr = $lib_AcademicYear->Get_Academic_Year_Info_By_YearName($activeYearOfCurrentDB);
				$activeYearID = $AcademicYearInfoArr[0]['AcademicYearID'];
	
				$lreportcard->schoolYear = $activeYearOfCurrentDB;
				$lreportcard->schoolYearID = $activeYearID;
				$lreportcard->dataFilesPath = $intranet_root."/file/reportcard2008/".$lreportcard->schoolYear."/";
				$lreportcard->DBName = $lreportcard->GET_DATABASE_NAME($lreportcard->schoolYear);
				
				# Term List
				$lib_AcademicYear->AcademicYearID = $activeYearID;
				$TermList = $lib_AcademicYear->Get_Term_List(0);
				$numOfTerm = count($TermList);
				$TermList[$numOfTerm][0] = 0;	// whole year
				
				# Upload Type
				$configFilesType = array("summary","award","merit","eca","remark","interschool","schoolservice","attendance","dailyPerformance","demerit","OLE","assessment","post","subjectweight","others","additional_comments","position","skills","themeForInvestigation","TFIPerformance","awardproject", "projectinfo");
				
				# Class 
				$fcm = new form_class_manage();
				$ClassList = $fcm->Get_Class_List_By_Academic_Year($activeYearID);
				
				$PATH_WRT_ROOT = $eRC_Addon_PATH_WRT_ROOT;
				
				$lreportcard->Start_Trans();
				
				foreach((array)$configFilesType as $UploadType)
				{
					foreach((array)$TermList as $TermInfo)
					{
						$Term = $TermInfo[0];
						$NewOtherInfoArr = array();
						foreach((array)$ClassList as $ClassInfo)
						{
							$ClassID = $ClassInfo['YearClassID'];
							$OldOtherInfoArr = $lreportcard->getOtherInfoData_old($UploadType, $Term, $ClassID, '', 1);
							
							if($OldOtherInfoArr)
							{
								foreach((array)$OldOtherInfoArr as $StudentID => $thisOtherInfoArr)
								{
									foreach((array)$thisOtherInfoArr as $ItemCode => $OtherInfo)
									{
										if(is_array($OtherInfo))
											$thisNewOtherInfo = implode("\n",$OtherInfo);
										else
											$thisNewOtherInfo = $OtherInfo;
											
										$NewOtherInfoArr[$ClassID][$StudentID][$ItemCode] = $thisNewOtherInfo;
									}	
								}
							}
						}
						
						$Success = true;
						if(!empty($NewOtherInfoArr)) {
							$Success = $lreportcard->Insert_Student_OtherInfo($Term, $UploadType, $NewOtherInfoArr);
						}	
					}
				}
				
				if($Success)
				{
					# update General Settings - mark the script is executed
					$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', '$SettingName', 1, now()) ";
					$li->db_db_query($sql) or debug_pr($sql."\n".mysql_error());
					$lreportcard->Commit_Trans();
					$eRC_x .= '<div style="color:green">Success!!</div>';
				}
				else
				{
					$lreportcard->RollBack_Trans();
					$eRC_x .= '<div style="color:red">FAILED!!</div>';
				}
			
				// restore Active year of the obj
				$lreportcard->schoolYear = $lreportcard->GET_ACTIVE_YEAR();
				$lreportcard->schoolYearID = $lreportcard->GET_ACTIVE_YEAR_ID();
				$lreportcard->dataFilesPath = $intranet_root."/file/reportcard2008/".$lreportcard->schoolYear."/";
				$lreportcard->DBName = $lreportcard->GET_DATABASE_NAME($lreportcard->schoolYear);
						
				$eRC_x .= "===================================================================<br>\r\n";
				$eRC_x .= "Migrate OtherInfo CSV Data Into DB [End]</b><br>\r\n";
				$eRC_x .= "===================================================================<br>\r\n";
			}
			###################################################
			### Migrate OtherInfo CSV Data Into DB [End]
			###################################################
			
			
			###################################################
			### Delete Duplicated Comment Data [Start]
			###################################################
			$SettingName = 'DeleteDuplicatedCommentData_'.$reportcard_db;
			if (!$GSary[$SettingName])
			{
				$eRC_x .= "===================================================================<br>\r\n";
				$eRC_x .= "Delete Duplicated Comment Data [Start]</b><br>\r\n";
				$eRC_x .= "===================================================================<br>\r\n";
				
				$thisPatchSuccessArr = array();
				$lreportcard->Start_Trans();
				
				// back up data table
				$sql = "CREATE TABLE IF NOT EXISTS $reportcard_db.RC_MARKSHEET_COMMENT_".date("YmdHis")." SELECT * FROM $reportcard_db.RC_MARKSHEET_COMMENT";
				$thisPatchSuccessArr['BackupCommentData'] = $li->db_db_query($sql);
				
				$sql = "Select CommentID, StudentID, SubjectID, ReportID From $reportcard_db.RC_MARKSHEET_COMMENT Order By DateInput Desc";
				$commentInfoArr = $li->returnArray($sql);
				$numOfComment = count($commentInfoArr);
				$commentExistedArr = array();
				$SuccessDeleteDuplicatedCommentDataArr = array();
				for ($i=0; $i<$numOfComment; $i++) {
					$thisCommentID = $commentInfoArr[$i]['CommentID'];
					$thisStudentID = $commentInfoArr[$i]['StudentID'];
					$thisSubjectID = $commentInfoArr[$i]['SubjectID'];
					$thisReportID = $commentInfoArr[$i]['ReportID'];
					
					if ($commentExistedArr[$thisReportID][$thisSubjectID][$thisStudentID]) {
						$sql = "Delete From $reportcard_db.RC_MARKSHEET_COMMENT Where CommentID = '".$thisCommentID."'";
						$thisPatchSuccessArr[$thisCommentID] = $li->db_db_query($sql);
					}
					else {
						$commentExistedArr[$thisReportID][$thisSubjectID][$thisStudentID] = true;
					}
				}
				
				if(!in_array(false, $SuccessDeleteDuplicatedCommentDataArr))
				{
					# update General Settings - mark the script is executed
					$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', '$SettingName', 1, now()) ";
					$li->db_db_query($sql) or debug_pr($sql."\n".mysql_error());
					$lreportcard->Commit_Trans();
					$eRC_x .= '<div style="color:green">Success!! '.(count($thisPatchSuccessArr) - 1).' record(s) were deleted.</div>';
				}
				else
				{
					$lreportcard->RollBack_Trans();
					$eRC_x .= '<div style="color:red">FAILED!!</div>';
				}
			
				$eRC_x .= "===================================================================<br>\r\n";
				$eRC_x .= "Delete Duplicated Comment Data [End]</b><br>\r\n";
				$eRC_x .= "===================================================================<br>\r\n";
			}
			###################################################
			### Delete Duplicated Comment Data [End]
			###################################################
			
				
			###################################################
			### Update Schedule End Time from 00:00:00 to 23:59:59 [Start]
			###################################################
			$SettingName = 'UpdateScheduleEndTime_'.$reportcard_db;
			if (!$GSary[$SettingName])
			{
				$eRC_x .= "===================================================================<br>\r\n";
				$eRC_x .= "Update Schedule End Time from 00:00:00 to 23:59:59 [Start]</b><br>\r\n";
				$eRC_x .= "===================================================================<br>\r\n";
				
				$sql = "
					UPDATE
						$reportcard_db.RC_REPORT_TEMPLATE
					SET
						MarksheetSubmissionEnd = CONCAT(DATE(MarksheetSubmissionEnd),' 23:59:59'),
						MarksheetVerificationEnd = CONCAT(DATE(MarksheetVerificationEnd),' 23:59:59'),
						PCSubmissionEnd = CONCAT(DATE(PCSubmissionEnd),' 23:59:59')												
				";
				$Success = $li->db_db_query($sql);
				
				if($Success)
				{
					# update General Settings - mark the script is executed
					$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', '$SettingName', 1, now()) ";
					$li->db_db_query($sql) or debug_pr($sql."\n".mysql_error());
					$eRC_x .= '<div style="color:green">Success!! </div>';
				}
				else
				{
					$eRC_x .= '<div style="color:red">FAILED!!</div>';
				}
			
				$eRC_x .= "===================================================================<br>\r\n";
				$eRC_x .= "Update Schedule End Time from 00:00:00 to 23:59:59 [End]</b><br>\r\n";
				$eRC_x .= "===================================================================<br>\r\n";
			}
			###################################################
			### Update Schedule End Time from 00:00:00 to 23:59:59 [End]
			###################################################
			
			###################################################
			### Update default settings EnableVerificationPeriod to 1 [Start]
			###################################################
			$SettingName = 'UpdateDefaultEnableVerificationPeriodValue_'.$reportcard_db;
			if (!$GSary[$SettingName])
			{
				$eRC_x .= "===================================================================<br>\r\n";
				$eRC_x .= "Update default settings EnableVerificationPeriod to 1 [Start]</b><br>\r\n";
				$eRC_x .= "===================================================================<br>\r\n";
				
				$sql = "Select * From $reportcard_db.RC_SETTINGS Where SettingCategory = 'AccessSettings' and SettingKey = 'EnableVerificationPeriod'";
				$resultAry = $li->returnResultSet($sql);
				
				if (count($resultAry) == 0) {
					$sql = "Insert Into $reportcard_db.RC_SETTINGS (SettingCategory, SettingKey, SettingValue, DateInput) Values ('AccessSettings', 'EnableVerificationPeriod', 1, now())";
					$Success = $li->db_db_query($sql);
					
					if($Success) {
						# update General Settings - mark the script is executed
						$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', '$SettingName', 1, now()) ";
						$li->db_db_query($sql) or debug_pr($sql."\n".mysql_error());
						$eRC_x .= '<div style="color:green">Success!! </div>';
					}
					else {
						$eRC_x .= '<div style="color:red">FAILED!!</div>';
					}
				}
				else {
					$eRC_x .= 'Have settings already.'."<br>\r\n";
				}
			
				$eRC_x .= "===================================================================<br>\r\n";
				$eRC_x .= "Update default settings EnableVerificationPeriod to 1 [End]</b><br>\r\n";
				$eRC_x .= "===================================================================<br>\r\n";
			}
			###################################################
			### Update default settings EnableVerificationPeriod to 1 [End]
			###################################################
			
			
			###################################################
			### Exclude ranking student data from txt to database [Start]
			###################################################
			$SettingName = 'ExcludeRankingStudentDataFromTxtToDb_'.$reportcard_db;
			if (!$GSary[$SettingName])
			{
				$eRC_x .= "===================================================================<br>\r\n";
				$eRC_x .= "Exclude ranking student data from txt to database [Start]</b><br>\r\n";
				$eRC_x .= "===================================================================<br>\r\n";
				
				$excludeStudents = trim($lo->file_read($intranet_root."/file/reportcard2008/exclude_order_students.txt"));
				$excludeStudentsArray = array();
				if(!empty($excludeStudents)) {
					$excludeStudentsArray = explode(",", $excludeStudents);
				}
				$excludeStudentsArray = array_values(array_unique(array_remove_empty($excludeStudentsArray)));
				$numOfExcludeStudent = count($excludeStudentsArray);
				
				
				$lreportcard->DBName = $lreportcard->GET_DATABASE_NAME($lreportcard->schoolYear);
				$sql = "Select YearTermID From {$intranet_db}.ACADEMIC_YEAR_TERM Where AcademicYearID = '".$lreportcard->schoolYearID."'";
				$termIdAry = $li->returnVector($sql);
				$numOfTerm = count($termIdAry);
				
				$tmpSuccessAry = array();
				for ($i=0; $i<$numOfExcludeStudent; $i++) {
					$_studentId = $excludeStudentsArray[$i];
					
					for ($j=0; $j<$numOfTerm; $j++) {
						$__termId = $termIdAry[$j];
						
						$sql = "INSERT INTO $reportcard_db.RC_EXCLUDE_RANKING_STUDENT (YearTermID, StudentID, InputDate, ModifiedDate) Value ('".$__termId."', '".$_studentId."', now(), now())";
						$tmpSuccessAry[] = $li->db_db_query($sql);
					}
					
					$sql = "INSERT INTO $reportcard_db.RC_EXCLUDE_RANKING_STUDENT (YearTermID, StudentID, InputDate, ModifiedDate) Value ('0', '".$_studentId."', now(), now())";
					$tmpSuccessAry[] = $li->db_db_query($sql);
				}
				
				if (!in_array(false, (array)$tmpSuccessAry)) {
					# update General Settings - mark the script is executed
					$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', '$SettingName', 1, now()) ";
					$li->db_db_query($sql) or debug_pr($sql."\n".mysql_error());
					$eRC_x .= '<div style="color:green">Success!! </div>';
				}
				else {
					$eRC_x .= '<div style="color:red">FAILED!!</div>';
				}
				
				$eRC_x .= "===================================================================<br>\r\n";
				$eRC_x .= "Exclude ranking student data from txt to database [End]</b><br>\r\n";
				$eRC_x .= "===================================================================<br>\r\n";
			}
			###################################################
			### Exclude ranking student data from txt to database [End]
			###################################################
			
	
			
			$eRC_x .= '<p>Run data patch end............</p><br>';	
			
	//		$eRC_x .= '<p>other info database setup start............</p><br>';	
			###################################################
			### setup other info database data from csv  [Start]
			###################################################
			
			###################################################
			### setup other info database data from csv  [End]
			###################################################
	//		$eRC_x .= '<p>other info database setup end............</p><br>';
	
			echo $eRC_x;
		}
		
				
		
			
		print("<p>=============================== Finish Creating tables for ".$DatabaseAry[$a][0]."... =============================== </p><br /><br />\n");
		
	}
	
	
	// General Patch (not specific year)
	###################################################
	### Add .htaccess for csv files [Start]
	###################################################
	$SettingName = 'Add_.htaccess_file';
	if (!$GSary[$SettingName])
	{
		$eRC_x .= "===================================================================<br>\r\n";
		$eRC_x .= "Add .htaccess for csv files [Start]</b><br>\r\n";
		$eRC_x .= "===================================================================<br>\r\n";
		
		$fileContent = '<FilesMatch ".(csv)$">
 Order Allow,Deny
 Deny from all
</FilesMatch>';
		
		$Success = $lo->file_write($fileContent, $intranet_root."/file/reportcard2008/.htaccess");
		
		if($Success)
		{
			# update General Settings - mark the script is executed
			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', '$SettingName', 1, now()) ";
			$li->db_db_query($sql) or debug_pr($sql."\n".mysql_error());
			$eRC_x .= '<div style="color:green">Success!! </div>';
		}
		else
		{
			$eRC_x .= '<div style="color:red">FAILED!!</div>';
		}
	
		$eRC_x .= "===================================================================<br>\r\n";
		$eRC_x .= "Add .htaccess for csv files [End]</b><br>\r\n";
		$eRC_x .= "===================================================================<br>\r\n";
	}
	###################################################
	### Add .htaccess for csv files [End]
	###################################################
	
	?>
	</body>
	<? 
		if ($_REQUEST['direct_run'] == 1) 
			intranet_closedb(); 
	?>
<? } else { ?>
	<html>
	<body>
		The client has not purchased eRC.
	</body>
	</html>
<? } ?>