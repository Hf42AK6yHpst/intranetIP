<?php
	$PATH_WRT_ROOT = '../../';
	include_once($PATH_WRT_ROOT.'includes/global.php');
	include_once($PATH_WRT_ROOT.'includes/libdb.php');
	include_once($PATH_WRT_ROOT.'includes/pagamo/libpagamo_install.php');
	
	intranet_opendb();
	$libpagamoinstall= new pagamo_install();
	
	$hasError = false;
	
	if($sys_custom['non_eclass_PaGamO']){
		if(!$school_code){
			$hasError = true;
			$message = "Missing School Info";
		}
	}else{
		$school_code = $config_school_code;
		$school_name = GET_SCHOOL_NAME();
	}

	if(!$quotation_number){
		$hasError = true;
		$message = "Missing Quotation Number";
	}
	
	if($libpagamoinstall->get_quotation_exists_by_quotation_number($quotation_number)){
		$hasError = true;
		$message = "Quotation Number has been used";
	}
	
	if($hasError == false){
		$result = $libpagamoinstall->install_new_quotation($quotation_number, $school_code, $school_name, $class_level, $pagamo_std_quota, $pagamo_tch_quota, $period_from." 00:00:00", $period_to." 23:59:59");
		if($result){
			$message = "Quotation # ".$quotation_number." Installation Completed.";
		}else{
			$message = "Quotation # ".$quotation_number." Installation Failed.";
		}
	}
	
	/*
	if($_FILES["file"]["tmp_name"] && $QuotationNumber && $InstallationType){		
		$message = $libelibinstall->install_new_quotation($_FILES["file"]["tmp_name"], $QuotationNumber,$InstallationType, $PeriodFrom, $PeriodTo, $QuotationBookCount, "NOW()" );
	}else{
		if($_FILES["file"]["tmp_name"] == "")
			$message .= " CSV Files ; ";
		if($QuotationNumber == "")
			$message .= " Quotation Number ; ";
		if($InstallationType == "")
			$message .= " Installation Type ; ";
		$message = " The data filled are incomplete : ".$message;
	}*/
	intranet_closedb();
?>

<!DOCTYPE html>
<html>
<head>
<style>
	body{
		background-color: grey;
	}
		
	#import_form{
		width:500px;  margin: 0 auto;	
		padding: 20px;	
		background-color: white;
		border-style:solid;
		border-width:1px;
	}
	
	input, textarea {   
	    padding: 9px;  
	    border: solid 1px #E5E5E5;  
	    outline: 0;  
	    font: normal 13px/100% Verdana, Tahoma, sans-serif;  
	    width: 200px;  
	    background: #FFFFFF;  
	    }  	 
	  
	textarea {   
	    width: 400px;  
	    max-width: 400px;  
	    height: 150px;  
	    line-height: 150%;  
	    }  
	  
	input:hover, textarea:hover,  
	input:focus, textarea:focus {   
	    border-color: #C9C9C9;   
	    }  
	  
	.form label {   
	    margin-left: 10px;   
	    color: #999999;   
	    }  
	  
	.submit input {  
	    width: auto;  
	    padding: 9px 15px;  
	    background: #617798;  
	    border: 0;  
	    font-size: 14px;  
	    color: #FFFFFF;  
    }  

	div label{
		padding-right: 40px;
	}
</style>
</head>
<body>
	<div id="import_form" style="">
				
		<form class="form" action="pagamo.php" enctype="multipart/form-data" accept-charset="UTF-8" method="post">
			<div class="file">        
            		<span style="margin-left: 40px;"><?=$message?></span>
            </div>	
            <input style=" margin-left: 150px; margin-top: 20px;" type="submit" value="Home" />
		</form>				
					
	</div>
</body>
</html>