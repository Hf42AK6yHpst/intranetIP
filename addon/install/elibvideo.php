<?php
	$PATH_WRT_ROOT = '../../';
	include_once($PATH_WRT_ROOT.'includes/global.php');
	include_once($intranet_root.'/includes/libdb.php');
	include_once($intranet_root.'/includes/libelibvideo_install.php');
	include_once($intranet_root."/addon/check.php");
	
	intranet_opendb();
	$libelibinstall = new elibvideo_install();
?>
<!DOCTYPE html>
<html>
<head>
<style>
	body{
		background-color: grey;
	}
		
	.import_table{
		width:600px; height:auto; margin: 0 auto;	
		padding: 20px;	
		background-color: white;
		border-style:solid;
		border-width:1px;
	}
	
	#import_form{
		width:600px;
  		 display: inline-block;		
	}
	
	#quotation_table{
  		
	}
	
	input, textarea {   
	    padding: 9px;  
	    border: solid 1px #E5E5E5;  
	    outline: 0;  
	    font: normal 13px/100% Verdana, Tahoma, sans-serif;  
	    width: 200px;  
	    background: #FFFFFF;  
	    }  	 
	  
	textarea {   
	    width: 400px;  
	    max-width: 400px;  
	    height: 150px;  
	    line-height: 150%;  
	    }  
	  
	input:hover, textarea:hover,  
	input:focus, textarea:focus {   
	    border-color: #C9C9C9;   
	    }  
	  
	.form label {   
	    margin-left: 10px;   
	    color: #999999;   
	    }  
	  
	.submit input {  
	    width: auto;  
	    padding: 9px 15px;  
	    background: #617798;  
	    border: 0;  
	    font-size: 14px;  
	    color: #FFFFFF;  
    }  

	div label{
		padding-right: 40px;
	}
	
	#quotation_table{
		border-collapse: separate;
		border-spacing: 0px;
		font-size: 16px;
		text-align: center;
		font-family:Arial,Helvetica,sans-serif;
	}
	#quotation_table tr th {
		margin: 0px;
		padding: 3px;
		padding-top: 5px;
		padding-bottom: 5px;
		background-color: #A6A6A6;
		font-weight: normal;
		color: #FFFFFF;
		border-bottom: 1px solid #CCCCCC;
		border-right: 1px solid #CCCCCC;
	}
	
	#quotation_table td {
		padding-top: 5px;
		padding-bottom: 5px;
	}
</style>
<script src="jquery.min.js"></script>
<script>
	$("document").ready(function(){				
		$(".delete_btn").live("click", function(){
			if(confirm("Delete this quotation ?")){				
				var quotation_id = $(this).attr('value');
				$(".quotation_id").val(quotation_id);
				$("#quotation_form").submit();
			}
		});
		
		$("#period_setting").live("change", function(){
			$(".license_period").html($("#license_period_template" +$(this).val() ).html());
		});
	});
</script>
</head>
<body>
	<div class="import_table" style="">			
		<form id="import_form" class="form" action="elibvideo_install.php" enctype="multipart/form-data" accept-charset="UTF-8" method="post">
			<h3 style="width: auto;   background-color: #eeeeee;   margin: 5px;">eLibvideo school control</h3>
			
			<div class="quotation">
				<label for="school_code">School code</label>
					<span style=" margin-left: 34px;"><input type="text" name="school_code" id="school_code" /></span>
			</div>
			<br/>		
			
			<div class="quotation">
				<label for="school_name">School name</label>
					<span style=" margin-left: 34px;"><input type="text" name="school_name" id="school_name" /></span>
			</div>
			<br/>	
			
			<div class="quotation">
				<label for="package">Package</label>
					<span style=" margin-left: 34px;"><input type="text" name="package" id="package" /></span>
			</div>
			<br/>	
			
			<div class="license_setting">
				<label for="period_setting">License Setting </label> 
				<select style=" margin-left: 25px;" class="period_setting" id="period_setting">
					<option value="2">Set Period</option>
					<option value="1">One-Off</option>
				</select>		
			</div>
			<br/>
						
			<div class="license_period">
				<label for="period_from">License period </label> 
				<span style=" margin-left: 16px;">from <input type="date" name="period_from" id="period_from" /> <br/></span>	
				<span style=" margin-left: 158px;">to <input type="date" name="period_to" id="period_to" /></span>				
			</div>
			<br/>
							
			<div class="submit">
				<input type="submit" value="Send" />
			</div>
		
		</form>		
			<br/>
			<br/>
			<br/>	
		<form id="quotation_form" action="elibvideo_uninstall.php" enctype="multipart/form-data" accept-charset="UTF-8" method="post">
			<input type="hidden"  class="quotation_id" name="quotation_id" />
		</form>			
		<span>-----------------------------------------------------------------------------------------</span><br/>
		<h3>Current quotation applied</h3><br/>
		<div style="display:none" id="license_period_template1">
				<label >License period </label> 
				<span  style=" margin-left: 35px;">--</span>	
		</div>	
			
		<div style="display:none" id="license_period_template2">
			<label for="period_from">License period </label> 
				<span style=" margin-left: 16px;">from <input type="date" name="period_from" id="period_from" /> <br/></span>	
				<span style=" margin-left: 158px;">to <input type="date" name="period_to" id="period_to" /></span>							
		</div>	
		<table style="width: 600px;" id="quotation_table">
			<thead>
				<th>SchoolCode</th>
				<th>SchoolName</th><th>Package</th>
				<th>StartDate</th>
				<th>EndDate</th><th>Delete</th><tbody>	
				
				<?= $libelibinstall->retrive_evideo_license_html(); ?>	
				</tbody>		
			</thead>
		</table>			
	</div>
</body>
</html>

<?php
	intranet_closedb();
?>