<?php

/*
 * 2019-10-17 Paul - Update to prevent allow update group only
 * 2019-08-06 Paul - Update to prevent non-SSO group memeber being added as unmapped user on Google Side
 * 2018-09-12 Paul - Update to add flags for handling different situation
 * 2016-12-28 Henry HM - Create File.
 */

//error_reporting(E_ALL);
//ini_set('display_errors', 1);

$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libauth.php");
include_once($PATH_WRT_ROOT."includes/libaccountmgmt.php");
include_once($PATH_WRT_ROOT."includes/libeclass40.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");

include_once($PATH_WRT_ROOT."includes/google_api/libgoogleapi.php");
include_once($PATH_WRT_ROOT."includes/sso/libSSO_db.php");

intranet_opendb();

/**** Helper function START ****/
define('CHUNK_SIZE', 10);

// https://developers.google.com/admin-sdk/directory/v1/limits
global $waitCounter;
$waitCounter = 0;
function waitGoogle(){
    global $waitCounter;
    
    $millisecond = pow(2, $waitCounter++) * 1000;
    $millisecond += rand(1000, 2000);
    usleep( $millisecond * 1000 );
    
    if($waitCounter == 6){
        $waitCounter = 1;
    }
}
/**** Helper function END ****/
if($_GET['run']==1){
    echo 'Script Started.' . "<br/>\n";flush();
    
    if($_GET['with_group']==1){
        $ssoservice["Google"]['mode']['user_and_group'] = true;
    }
    
    if($_SESSION['CurrentSchoolYearInfo']['AcademicYearID']==""){
        echo 'Script Required Current Academic Year' . "<br/>\n";flush();
        echo 'Script Ended.' . "<br/>\n";flush();
        die();
    }
    $lu = new libuser();
    $google_api = new libGoogleAPI(0);
    $libSSO_db = new libSSO_db();
    //prepare password
    include_once($PATH_WRT_ROOT."includes/libpwm.php");
    $libpwm = new libpwm();
    
    if(!$ssoservice["Google"]["Valid"]){
        echo 'GSuite flag $ssoservice["Google"]["Valid"] does not switched on.' . "<br/>\n";
        echo 'Script Aborted.' . "<br/>\n";
        exit();
    }
    
    if(!isset($ssoservice["Google"]['api'][0]['application_name'])){
        echo 'GSuite setting $ssoservice["Google"][\'api\'][0] is not defined.' . "<br/>\n";
        echo 'Script Aborted.' . "<br/>\n";
        exit();
    }
    
    if(!$ssoservice["Google"]['mode']['user_and_group']){
        echo 'GSuite flag $ssoservice["Google"][\'mode\'][\'user_and_group\'] does not switched on, sync for users only...' . "<br/>\n";
    }
    
    
    echo 'Gathering data for users...';
    //users
    $sql='SELECT * FROM `INTRANET_USER` WHERE `RecordStatus`=\'1\';';
    $rows_user = $lu->returnArray($sql);
    
    $array_user_parameter = array();
    foreach((array)$rows_user as $row_user){
        $account_identifier = $row_user['UserLogin'] . '@' . $google_api->getDomain();
        
        $password_for_google_account_creation='';
        $rows_password = $libpwm->getData(array($row_user['UserID']));
        foreach((array)$rows_password as $key=>$row_password){
            $password_for_google_account_creation = $row_password;
        }
        if($row_user['ChineseName']==""){
            $row_user['ChineseName'] = $row_user['EnglishName'];
        }
        $user_parameter=array(
            'first_name'=>$row_user['EnglishName'],
            'last_name'=>$row_user['ChineseName'],
            'email'=>$account_identifier,
            'password'=>$password_for_google_account_creation,
            'uid'=>$row_user['UserID']
        );
        array_push($array_user_parameter,$user_parameter);
    }
    echo ' Finished' . "<br/>\n";flush();
    
    if($ssoservice["Google"]['mode']['user_and_group']){
        echo 'Gathering data for groups...';
        // get current acadmeic year english name
        $sql = 'SELECT YearNameEN FROM ACADEMIC_YEAR WHERE AcademicYearID="'.IntegerSafe($_SESSION['CurrentSchoolYearInfo']['AcademicYearID']).'"';
        $yearName = current($lu->returnVector($sql));
        
        //groups
        $sql='SELECT * FROM `INTRANET_GROUP` WHERE (AcademicYearID="'.IntegerSafe($_SESSION['CurrentSchoolYearInfo']['AcademicYearID']).'" OR AcademicYearID IS NULL);';
        $rows_group = $lu->returnArray($sql);
        
        $array_group_parameter = array();
        foreach((array)$rows_group as $row_group){
            $group_email = 'g'.($row_group['GroupID']+288).'@'.$google_api->getDomain();
            
            $group_parameter = array(
                'email'=>$group_email,
                'name'=>$row_group['Title']." (".$yearName.")",
                'group_id'=>$row_group['GroupID']
            );
            array_push($array_group_parameter,$group_parameter);
        }
        echo ' Finished' . "<br/>\n";flush();
        
        
        echo 'Gathering data for group members...';
        //members
        $sql='Select g.GroupID, g.Title, u.UserLogin From INTRANET_USERGROUP ug, INTRANET_USER u, INTRANET_GROUP g WHERE ug.GroupID=g.GroupID AND ug.UserID=u.UserID AND u.RecordStatus=\'1\' AND (g.AcademicYearID="'.IntegerSafe($_SESSION['CurrentSchoolYearInfo']['AcademicYearID']).'"  OR g.AcademicYearID IS NULL)';
        $rows_member = $lu->returnArray($sql);
        
        $array_member_parameter = array();
        foreach((array)$rows_member as $row_member){
            $group_email = 'g'.($row_member['GroupID']+288).'@'.$google_api->getDomain();
            $account_identifier = $row_member['UserLogin'] . '@' . $google_api->getDomain();
            
            $member_parameter = array(
                'email'=>$account_identifier,
                'group_email'=>$group_email
            );
            array_push($array_member_parameter,$member_parameter);
        }
        echo ' Finished' . "<br/>\n";flush();
    }
    
    echo 'Sync to Google...Start<br />';
    
    if($_GET['update_group_only']){
        /**** Sync group START ****/
        if ($ssoservice["Google"]['mode']['user_and_group']) {
            echo 'Updating DB for groups...';
            
            $groupChunkArr = array_chunk($array_group_parameter, CHUNK_SIZE);
            foreach ($groupChunkArr as $array_group_parameter) {
                waitGoogle();
                $google_api->startBatch();
                $array_batch_number_group = $google_api->newGoogleGroupByBatch($array_group_parameter);
                $array_result = $google_api->sendBatch();
                
                //groups
                for ($i = $array_batch_number_group['count_begin']; $i <= $array_batch_number_group['count_end']; $i++) {
                    foreach ((array)$array_result['array_result'] as $key => $result) {
                        if ($key == 'response-' . $i) {
                            $do_create_db_record = false;
                            if ($google_api->isError($result, '409')) {//duplicate record
                                $do_create_db_record = true;
                            } elseif ($google_api->isError($result)) {
                                $do_create_db_record = false;
                                echo 'ERROR: ' . $result->getMessage() . '<br />';
                            } else {
                                //no errors
                                $do_create_db_record = true;
                            }
                            if ($do_create_db_record) {
                                $parameter = $array_batch_number_group['parameters'][$i];
                                $libSSO_db->addSSOGroupIdentifier($parameter['group_id'], $parameter['email'], $parameter['name']);
                            }
                        }
                    }
                }
            }
            echo ' Finished' . "<br/>\n";
            flush();
        }
        /**** Sync group END ****/
    }else {
        
        if ($_GET['skip_user_update']) {
            // skip the process
        } else {
            /**** Sync user START ****/
            $waitCounter = 0;
            echo 'Updating DB for users...';
            
            $userChunkArr = array_chunk($array_user_parameter, CHUNK_SIZE);
            
            foreach ($userChunkArr as $array_user_parameter) {
                waitGoogle();
                $google_api->startBatch();
                $array_batch_number_user = $google_api->newGoogleAccountByBatch($array_user_parameter);
                $array_result = $google_api->sendBatch();
                
                //users
                for ($i = $array_batch_number_user['count_begin']; $i <= $array_batch_number_user['count_end']; $i++) {
                    foreach ((array)$array_result['array_result'] as $key => $result) {
                        if ($key == 'response-' . $i) {
                            $do_create_db_record = false;
                            if ($google_api->isError($result, '409')) {//duplicate record
                                $do_create_db_record = true;
                            } elseif ($google_api->isError($result, '412')) {//account full
                                $do_create_db_record = false;
                                echo 'ERROR: ' . $result->getMessage() . '<br />';
                            } elseif ($google_api->isError($result)) {
                                $do_create_db_record = false;
                                echo 'ERROR: ' . $result->getMessage() . '<br />';
                            } else {
                                //no errors
                                $do_create_db_record = true;
                            }
                            if ($do_create_db_record) {
                                $parameter = $array_batch_number_user['parameters'][$i];
                                
                                $array_sql_result = $libSSO_db->isUserHaveSSOIdentifiersAndReturnWithStatus($parameter['uid'], $google_api);
                                $is_account_record_exist = $array_sql_result['is_account_record_exist'];
                                $account_status = $array_sql_result['account_status'];
                                
                                if (!$is_account_record_exist) {
                                    $libSSO_db->addSSOIdentifier($parameter['uid'], $parameter['PrimaryEmail']);
                                }
                            }
                        }
                    }
                }
            }
            echo ' Finished' . "<br/>\n";
            flush();
            /**** Sync user END ****/
        }
        
        waitGoogle();
        /**** Sync group START ****/
        if ($ssoservice["Google"]['mode']['user_and_group']) {
            echo 'Updating DB for groups...';
            
            $groupChunkArr = array_chunk($array_group_parameter, CHUNK_SIZE);
            foreach ($groupChunkArr as $array_group_parameter) {
                waitGoogle();
                $google_api->startBatch();
                $array_batch_number_group = $google_api->newGoogleGroupByBatch($array_group_parameter);
                $array_result = $google_api->sendBatch();
                
                //groups
                for ($i = $array_batch_number_group['count_begin']; $i <= $array_batch_number_group['count_end']; $i++) {
                    foreach ((array)$array_result['array_result'] as $key => $result) {
                        if ($key == 'response-' . $i) {
                            $do_create_db_record = false;
                            if ($google_api->isError($result, '409')) {//duplicate record
                                $do_create_db_record = true;
                            } elseif ($google_api->isError($result)) {
                                $do_create_db_record = false;
                                echo 'ERROR: ' . $result->getMessage() . '<br />';
                            } else {
                                //no errors
                                $do_create_db_record = true;
                            }
                            if ($do_create_db_record) {
                                $parameter = $array_batch_number_group['parameters'][$i];
                                
                                $array_sql_result = $libSSO_db->isGroupHaveSSOIdentifiersAndReturnWithStatus($parameter['group_id'], $google_api);
                                $is_group_record_exist = $array_sql_result['is_group_record_exist'];
                                $group_status = $array_sql_result['group_status'];
                                
                                if (!$is_group_record_exist) {
                                    $libSSO_db->addSSOGroupIdentifier($parameter['group_id'], $parameter['email'], $parameter['name']);
                                }
                            }
                        }
                    }
                }
            }
            echo ' Finished' . "<br/>\n";
            flush();
        }
        /**** Sync group END ****/
        
        waitGoogle();
        
        
        /**** Sync group-user START ****/
        if ($ssoservice["Google"]['mode']['user_and_group']) {
            echo 'Updating DB for members...';
            
            $memberChunkArr = array_chunk($array_member_parameter, CHUNK_SIZE);
            foreach ($memberChunkArr as $array_member_parameter) {
                waitGoogle();
                foreach ($array_member_parameter as $member_key => $member_value) {
                    $user = $google_api->getGoogleAccountByEmail($member_value['email']);
                    if ($user == null) {
                        unset($array_member_parameter[$member_key]);
                    }
                }
                $google_api->startBatch();
                $array_batch_number_member = $google_api->addMemberToGroupByBatch($array_member_parameter);
                $array_result = $google_api->sendBatch();
                
                for ($i = $array_batch_number_member['count_begin']; $i <= $array_batch_number_member['count_end']; $i++) {
                    foreach ((array)$array_result['array_result'] as $key => $result) {
                        if ($key == 'response-' . $i) {
                            $do_create_db_record = false;
                            if ($google_api->isError($result, '409')) {//duplicate record
                                $do_create_db_record = true;
                            } elseif ($google_api->isError($result)) {
                                $do_create_db_record = false;
                                echo 'ERROR: ' . $result->getMessage() . '<br />';
                            } else {
                                //no errors
                                $do_create_db_record = true;
                            }
                            
                            if ($do_create_db_record) {
                                $parameter = $array_batch_number_member['parameters'][$i];
                            }
                        }
                    }
                }
            }
            echo ' Finished' . "<br/>\n";
            flush();
        }
        /**** Sync group-user END ****/
    }
    
    echo 'Sync to Google...Finished' . "<br/>\n";flush();
    echo 'Script Ended.' . "<br/>\n";flush();
    
    exit;
    
    
    
    ###################################################################################################################################
    ###################################################################################################################################
    ###################################################################################################################################
    ###################################################################################################################################
    ###################################################################################################################################
    //
    //
    //echo 'Sync to Google...';
    ////save to google
    //$google_api->startBatch();
    //$array_batch_number_user = $google_api->newGoogleAccountByBatch($array_user_parameter);
    //$array_batch_number_group = $google_api->newGoogleGroupByBatch($array_group_parameter);
    //$array_batch_number_member = $google_api->addMemberToGroupByBatch($array_member_parameter);
    //$array_result = $google_api->sendBatch();
    //echo ' Finished' . "<br/>\n";flush();
    //
    //
    //echo 'Updating DB for users...';
    ////users
    //for($i=$array_batch_number_user['count_begin'];$i<=$array_batch_number_user['count_end'];$i++){
    //	foreach ((array)$array_result['array_result'] as $key => $result) {
    //		if($key == 'response-'.$i){
    //			$do_create_db_record=false;
    //			if($google_api->isError($result,'409')){//duplicate record
    //				$do_create_db_record=true;
    //			}elseif($google_api->isError($result,'412')){//account full
    //				$do_create_db_record=false;
    //				echo 'ERROR: ' . $result->getMessage();
    //			}elseif($google_api->isError($result)){
    //				$do_create_db_record=false;
    //				echo 'ERROR: ' . $result->getMessage();
    //			}else{
    //				//no errors
    //				$do_create_db_record=true;
    //			}
    //			if($do_create_db_record){
    //				$parameter = $array_batch_number_user['parameters'][$i];
    //
    //				$array_sql_result = $libSSO_db->isUserHaveSSOIdentifiersAndReturnWithStatus($parameter['uid'], $google_api);
    //				$is_account_record_exist = $array_sql_result['is_account_record_exist'];
    //				$account_status = $array_sql_result['account_status'];
    //
    //				if(!$is_account_record_exist){
    //					$libSSO_db->addSSOIdentifier($parameter['uid'], $parameter['PrimaryEmail']);
    //				}
    //			}
    //		}
    //	}
    //}
    //echo ' Finished' . "<br/>\n";flush();
    //
    //if($ssoservice["Google"]['mode']['user_and_group']){
    //	echo 'Updating DB for groups...';
    //	//groups
    //	for($i=$array_batch_number_group['count_begin'];$i<=$array_batch_number_group['count_end'];$i++){
    //		foreach ((array)$array_result['array_result'] as $key => $result) {
    //			if($key == 'response-'.$i){
    //				$do_create_db_record=false;
    //				if($google_api->isError($result,'409')){//duplicate record
    //					$do_create_db_record=true;
    //				}elseif($google_api->isError($result)){
    //					$do_create_db_record=false;
    //					echo 'ERROR: ' . $result->getMessage();
    //				}else{
    //					//no errors
    //					$do_create_db_record=true;
    //				}
    //				if($do_create_db_record){
    //					$parameter = $array_batch_number_group['parameters'][$i];
    //
    //					$array_sql_result = $libSSO_db->isGroupHaveSSOIdentifiersAndReturnWithStatus($parameter['group_id'], $google_api);
    //					$is_group_record_exist = $array_sql_result['is_group_record_exist'];
    //					$group_status = $array_sql_result['group_status'];
    //
    //					if(!$is_group_record_exist){
    //						$libSSO_db->addSSOGroupIdentifier($parameter['group_id'], $parameter['email']);
    //					}
    //				}
    //			}
    //		}
    //	}
    //	echo ' Finished' . "<br/>\n";flush();
    //
    //
    //	echo 'Updating DB for members...';
    //	//members
    //	for($i=$array_batch_number_member['count_begin'];$i<=$array_batch_number_member['count_end'];$i++){
    //		foreach ((array)$array_result['array_result'] as $key => $result) {
    //			if($key == 'response-'.$i){
    //				$do_create_db_record=false;
    //				if($google_api->isError($result,'409')){//duplicate record
    //					$do_create_db_record=true;
    //				}elseif($google_api->isError($result)){
    //					$do_create_db_record=false;
    //					echo 'ERROR: ' . $result->getMessage();
    //				}else{
    //					//no errors
    //					$do_create_db_record=true;
    //				}
    //
    //				if($do_create_db_record){
    //					$parameter = $array_batch_number_member['parameters'][$i];
    //				}
    //			}
    //		}
    //	}
    //	echo ' Finished' . "<br/>\n";flush();
    //}
    //echo 'Script Ended.' . "<br/>\n";flush();
}else{
    ?>
<html>
    <body>
        <div class="container-fluid">
            <h2>Google SSO Script</h2><hr />
            <h4>Caution: please notice that the script is NOT for update. Please do not use it for update data</h4><hr />
            <div><a href="google_sso_batch_create.php?run=1" class="btn btn-primary btn-block">Import all to Google</a></div>
            <div><a href="google_sso_batch_create.php?run=1&with_group=1&skip_user_update=1" class="btn btn-primary btn-block">Import Only Group & Group Member</a></div>
            <div><a href="google_sso_batch_create.php?run=1&update_group_only=1" class="btn btn-primary btn-block">Update only Group</a></div>
        </div>
    </body>
</html>
<?php
}
?>