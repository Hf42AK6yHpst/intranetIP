<?php
	//$File = isset($_REQUEST['file']) && $_REQUEST['file']!=''? $_REQUEST['file'] : "";
	$QuotationNumber = isset($_REQUEST['quotation_number']) && $_REQUEST['quotation_number']!=''? $_REQUEST['quotation_number'] : "";
	$InstallationType = isset($_REQUEST['installation_type']) && $_REQUEST['installation_type']!=''? $_REQUEST['installation_type'] : "";
	$QuotationBookCount = isset($_REQUEST['quotation_book_count']) && $_REQUEST['quotation_book_count']!=''? $_REQUEST['quotation_book_count'] : "";
	$PeriodFrom = isset($_REQUEST['period_from']) && $_REQUEST['period_from']!=''? $_REQUEST['period_from'] : "";
	$PeriodTo = isset($_REQUEST['period_to']) && $_REQUEST['period_to']!=''? $_REQUEST['period_to'] : "";
	//$InputDate = isset($_REQUEST['purchase_date']) && $_REQUEST['purchase_date']!=''? $_REQUEST['purchase_date'] : "";

	$PATH_WRT_ROOT = '../../';
	include_once($PATH_WRT_ROOT.'includes/global.php');
	include_once($PATH_WRT_ROOT.'includes/libdb.php');
	include_once($PATH_WRT_ROOT.'includes/libelibrary_install.php');
	
	intranet_opendb();
	$libelibinstall = new elibrary_install();
	if($_FILES["file"]["tmp_name"] && $QuotationNumber && $InstallationType){		
		$message = $libelibinstall->install_new_quotation($_FILES["file"]["tmp_name"], $QuotationNumber,$InstallationType, $PeriodFrom, $PeriodTo, $QuotationBookCount, "NOW()" );
	}else{
		if($_FILES["file"]["tmp_name"] == "")
			$message .= " CSV Files ; ";
		if($QuotationNumber == "")
			$message .= " Quotation Number ; ";
		if($InstallationType == "")
			$message .= " Installation Type ; ";
		$message = " The data filled are incomplete : ".$message;
	}
	include_once('ebook_install_finish.php');
	intranet_closedb();
?>