<?php
	$PATH_WRT_ROOT = '../../';
	include_once($PATH_WRT_ROOT.'includes/global.php');
	include_once($PATH_WRT_ROOT.'includes/libdb.php');
	include_once($PATH_WRT_ROOT.'includes/libelibrary_install.php');
	include_once("../check.php");
	
	intranet_opendb();
	$libelibinstall = new elibrary_install();
?>
<!DOCTYPE html>
<html>
<head>
<style>
	body{
		background-color: grey;
	}
		
	.import_table{
		width:600px; height:auto; margin: 0 auto;	
		padding: 20px;	
		background-color: white;
		border-style:solid;
		border-width:1px;
	}
	
	#import_form{
		width:600px;
  		 display: inline-block;		
	}
	
	#quotation_table{
  		
	}
	
	input, textarea {   
	    padding: 9px;  
	    border: solid 1px #E5E5E5;  
	    outline: 0;  
	    font: normal 13px/100% Verdana, Tahoma, sans-serif;  
	    width: 200px;  
	    background: #FFFFFF;  
	    }  	 
	  
	textarea {   
	    width: 400px;  
	    max-width: 400px;  
	    height: 150px;  
	    line-height: 150%;  
	    }  
	  
	input:hover, textarea:hover,  
	input:focus, textarea:focus {   
	    border-color: #C9C9C9;   
	    }  
	  
	.form label {   
	    margin-left: 10px;   
	    color: #999999;   
	    }  
	  
	.submit input {  
	    width: auto;  
	    padding: 9px 15px;  
	    background: #617798;  
	    border: 0;  
	    font-size: 14px;  
	    color: #FFFFFF;  
    }  

	div label{
		padding-right: 40px;
	}
	
	#quotation_table{
		border-collapse: separate;
		border-spacing: 0px;
		font-size: 16px;
		text-align: center;
		font-family:Arial,Helvetica,sans-serif;
	}
	#quotation_table tr th {
		margin: 0px;
		padding: 3px;
		padding-top: 5px;
		padding-bottom: 5px;
		background-color: #A6A6A6;
		font-weight: normal;
		color: #FFFFFF;
		border-bottom: 1px solid #CCCCCC;
		border-right: 1px solid #CCCCCC;
	}
	
	#quotation_table td {
		padding-top: 5px;
		padding-bottom: 5px;
	}
</style>
<script src="jquery.min.js"></script>
<script>
	$("document").ready(function(){		
		$("#installation_type").change(function(){
			if($(this)[0].selectedIndex == 0){
				$(".quotation_book_count").css("display", "none");
				$("#quotation_book_count").val("");
			}else{
				$(".quotation_book_count").css("display", "");
			}
		});		
		$(".delete_btn").live("click", function(){
			if(confirm("Delete this quotation ?")){				
				var quotation_id = $(this).attr('value');
				$(".quotation_id").val(quotation_id);
				//$.post('ebook_uninstall.php', { quotation_id: quotation_id });
				$("#quotation_form").submit();
			}
		});
		
		$("#period_setting").live("change", function(){
			$(".license_period").html($("#license_period_template" +$(this).val() ).html());
		});
		
	});
</script>
</head>
<body>
	<div class="import_table" style="">			
		<form id="import_form" class="form" action="ebook_install.php" enctype="multipart/form-data" accept-charset="UTF-8" method="post">
			<h3 style="width: auto;   background-color: #eeeeee;   margin: 5px;">Import ebooks form</h3>
			<div class="file">                     	
				<label for="file">Import File</label>	
            		<span style="margin-left: 40px;"><input  type="file" name="file" id="file"/></span>
            </div>
            
            <br/>
			 <text style="font-size: 11px;">CSV format: Activated by System - (BookID , IsEnable [1 | Leave it blank])
            <br/>    Selection by school - (BookID , IsCompulsory [1 | Leave it blank])</text>
            <br/>
            <br/>
			<div class="quotation">
				<label for="quotation_number">Quotation #</label>
					<span style=" margin-left: 34px;"><input type="text" name="quotation_number" id="quotation_number" /></span>
			</div>
			
			<br/>		
			
			<div class="license_setting">
				<label for="period_setting">License Setting </label> 
				<select style=" margin-left: 25px;" class="period_setting" id="period_setting">
					<option value="2">Set Period</option>
					<option value="1">One-Off</option>
				</select>		
			</div>
			<br/>		
			
			<div class="installation_type">
				<label for="installation_type">Installation Type</label>
					<span style=" margin-left: 5px;"><select name="installation_type" id="installation_type" >
					  <option value="1">Activated by system</option>
					  <option value="2">Selection by school</option>
					</select></span>
			</div>
			<br/>
			
			<div style="display:none" class="quotation_book_count">
				<label for="quotation_book_count"># of books</label>
					<span style=" margin-left: 35px;">
						<input min="1"  type="number" name="quotation_book_count" id="quotation_book_count"/>
					</span>
			</div>
			<br/>
			
			<div class="license_period">
				<label for="period_from">License period </label> 
				<span style=" margin-left: 16px;">from <input type="date" name="period_from" id="period_from" /> <br/></span>	
				<span style=" margin-left: 158px;">to <input type="date" name="period_to" id="period_to" /></span>				
			</div>
			<br/>
			
			<!--<div class="purchase_date">
				<label for="purchase_date">Purchase Date </label> 
				<span style=" margin-left: 16px;">in <input type="date" name="purchase_date" id="purchase_date" /> <br/></span>	
			</div>-->
			<br/>
				
			<div class="submit">
				<input type="submit" value="Send" />
			</div>
		
		</form>		
		
		<div style="display:none" id="license_period_template1">
				<label >License period </label> 
				<span  style=" margin-left: 35px;">--</span>	
		</div>	
			
		<div style="display:none" id="license_period_template2">
			<label for="period_from">License period </label> 
				<span style=" margin-left: 16px;">from <input type="date" name="period_from" id="period_from" /> <br/></span>	
				<span style=" margin-left: 158px;">to <input type="date" name="period_to" id="period_to" /></span>							
		</div>	
		
			<br/>
			<br/>
			<br/>	
		<form id="quotation_form" action="ebook_uninstall.php" enctype="multipart/form-data" accept-charset="UTF-8" method="post">
			<input type="hidden"  class="quotation_id" name="quotation_id" />
		</form>			
		<span>-----------------------------------------------------------------------------------------</span><br/>
		<h3>Current quotation applied</h3><br/>
		<text style="color:red"># in red:</text> Quotation has been activated.
		<table style="width: 600px;" id="quotation_table">
			<thead>
				<th>#</th>
				<th># of books</th>
				<th>From</th>
				<th>To</th>
				<th>Install Date</th><th>Delete</th><tbody>		
				</tbody>		
				<?= $libelibinstall->retrieve_quotation_info_importside(); ?>
			</thead>
		</table>			
	</div>
</body>
</html>

<?php
	intranet_closedb();
?>