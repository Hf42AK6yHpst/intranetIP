<?php
# Editing by 
 
@SET_TIME_LIMIT(1000);

$POWER_PORTFOLIO_PATH_WRT_ROOT = ($PATH_WRT_ROOT!="") ? $PATH_WRT_ROOT : "../../";
include_once($POWER_PORTFOLIO_PATH_WRT_ROOT."includes/global.php");
include_once($POWER_PORTFOLIO_PATH_WRT_ROOT."includes/libdb.php");
include_once($POWER_PORTFOLIO_PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($POWER_PORTFOLIO_PATH_WRT_ROOT."includes/PowerPortfolio/config.inc.php");
include_once($POWER_PORTFOLIO_PATH_WRT_ROOT."includes/PowerPortfolio/libpowerportfolio.php");

if (!$CallFromSameDomain)
{
	include_once("../check.php");
}

if ($_REQUEST['direct_run'] == 1)
{
	intranet_opendb();
}
	
function updateSchema_PowerPortfolio($updateMode, $lastSchemaUpdateDate, $schemaInputDate, $schemaDesc, $schemaSql)
{
	global $li;
	
	if ($updateMode==2 && strtotime($lastSchemaUpdateDate) > strtotime($schemaInputDate)) {
		
	}
	else {
		echo "<b>[Date: ".$schemaInputDate." deployment]</b><br>";
		echo "<b>[Function: ".$schemaDesc."]</b><br>";
		if (!$li->db_db_query($schemaSql)){
			echo "warning: ".$schemaSql." <br><br>";
		}
		else {
			echo "Ok<br><br>";
		}
	}
}

$li = new libdb();
$lpower_portfolio = new libpowerportfolio();

if ($plugin['PowerPortfolio'])
{
	$SuccessArr = array();
	
	# Script : Create file directory if folder does not exist
	$lo = new libfilesystem();
	$lo->folder_new($intranet_root."/file/power_portfolio/");
	$lo->folder_new($intranet_root."/file/power_portfolio/templates/");
	
	# Set default active academic year
	$ActiveAcademicYearID = $lpower_portfolio->Get_Active_AcademicYearID();
	if ($ActiveAcademicYearID == '')
	{
		$SuccessArr['Set_Default_Active_AcademicYear'] = $lpower_portfolio->Update_Active_AcademicYearID(Get_Current_Academic_Year_ID());
		$lpower_portfolio->AcademicYearID = $lpower_portfolio->Get_Active_AcademicYearID();
		$lpower_portfolio->DBName = $lpower_portfolio->Get_Database_Name($lpower_portfolio->AcademicYearID);
	}
	?>
	
	<body>
	<h1>Script : Create Power Portfolio Database & Tables</h1>
	
	<?php
	# Create Database
	$reportcard_db = $lpower_portfolio->DBName;
	if ($li->db_create_db($reportcard_db)){
		print("<p>Initial Database created successfully</p>\n");
	}
	else {
		print("<p>Initial Database exist already: ".mysql_error()."</p>\n");
	}
	
	# Create log table
	$sql = "CREATE TABLE IF NOT EXISTS RC_KINDERGARTEN_LOG (
				LogID int(8) NOT NULL auto_increment,
				Functionality varchar(128) default null,
				UserID int(8) default null,
				LogContent text default null,
				InputDate datetime default null,
				PRIMARY KEY (LogID),
				KEY Functionality (Functionality)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8
			";
	$li->db_db_query($sql);
	
	# Loop Databases
	$sql = "SHOW DATABASES LIKE '%".$intranet_db."_DB_POWER_PORTFOLIO_%'";
	$DatabaseAry = $li->returnArray($sql);
	for($a=0; $a<sizeof($DatabaseAry);$a++)
	{
		$sql_table = array();
		$sql_alter = array();
		
		$reportcard_db = $DatabaseAry[$a][0];
		
		// Ignore backup DB like "intranet_DB_REPORT_CARD_KINDERGARTEN_2007_UTF82"
		$tmpDBNameArr = explode('_', $reportcard_db);
		$numOfDBNamePart = count($tmpDBNameArr);
		$targetCheckingText = $tmpDBNameArr[$numOfDBNamePart - 1];
		if (strtolower(substr($targetCheckingText, 0, 4)) == 'utf8')
			continue;
		
		// 2012-03-06 [Please follow this format afterwards]
//		$sql_table[] = array(
//			"2012-03-06",
//			"Create test eRC DB schema table",
//			"CREATE TABLE IF NOT EXISTS $reportcard_db.RC_TEMP (
//			     TempID int(8) NOT NULL auto_increment,
//			     PRIMARY KEY (TempID)
//			) ENGINE=InnoDB DEFAULT CHARSET=utf8"
//		);

        /*
		$sql_table[] = array(
			"2017-01-20",
			"Create Table RC_ABILITY_INDEX_CATEGORY for categories of ability index",
			"CREATE TABLE $reportcard_db.RC_ABILITY_INDEX_CATEGORY (
			     CatID int(11) NOT NULL auto_increment,
			     Code varchar(255) NOT NULL,
			     Name varchar(255) NOT NULL,
			     Type tinyint(1) NOT NULL,
			     Level int(8) NOT NULL,
			     UpperCat int(11) default NULL,
			     DateInput datetime NOT NULL,
			     DateModified datetime NOT NULL,
			     PRIMARY KEY (CatID),
			     UNIQUE KEY Code (Code)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8"
		);
		
		$sql_table[] = array(
			"2017-01-20",
			"Create Table RC_ABILITY_INDEX_ITEM for items of ability index",
			"CREATE TABLE $reportcard_db.RC_ABILITY_INDEX_ITEM (
			     ItemID int(11) NOT NULL auto_increment,
				 RubricSettingID int(11) NOT NULL,
			     Code varchar(255) NOT NULL,
			     Name varchar(255) NOT NULL,
			     Type tinyint(1) NOT NULL,
			     Level int(8) NOT NULL,
			     UpperCat varchar(20) default NULL,
			     DateInput datetime NOT NULL,
			     DateModified datetime NOT NULL,
			     PRIMARY KEY (ItemID),
				 KEY (RubricSettingID)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8"
		);
		
		$sql_table[] = array(
			"2017-01-20",
			"Create Table RC_ABILITY_CATEGORY_INDEX_MAPPING for mapping between categories of ability index",
			"CREATE TABLE $reportcard_db.RC_ABILITY_CATEGORY_INDEX_MAPPING (
			     MappingID int(11) NOT NULL auto_increment,
				 TWItemID int(11) NOT NULL,
			     TWItemCode varchar(255) NOT NULL,
				 MOItemID int(11) NOT NULL,
			     MOItemCode varchar(255) NOT NULL,
			     DateInput datetime NOT NULL,
			     DateModified datetime NOT NULL,
			     PRIMARY KEY (MappingID),
			     KEY TWItemCode (TWItemCode),
			     KEY MOItemCode (MOItemCode)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8"
		);
		
		$sql_table[] = array(
			"2017-01-25",
			"Create Table RC_FORM_TOPICS for form topics storage",
			"CREATE TABLE $reportcard_db.RC_FORM_TOPICS (
			     TopicID int(11) NOT NULL auto_increment,
			     TopicCode varchar(255) NOT NULL,
				 TopicNameB5 varchar(255) NOT NULL,
			     TopicNameEN varchar(255) NOT NULL,
				 Remark text default NULL,
				 YearID int(11) default NULL,
     			 IsDeleted int(1) default NULL,
				 InputBy int(11) default NULL,
			     DateInput datetime NOT NULL,
				 ModifiedBy int(11) default NULL,
			     DateModified datetime NOT NULL,
			     PRIMARY KEY (TopicID),
			     KEY TopicCode (TopicCode),
			     KEY YearID (YearID)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8"
		);
		
		$sql_table[] = array(
			"2017-01-25",
			"Create Table RC_LEARNING_ZONE for learning zone storage",
			"CREATE TABLE $reportcard_db.RC_LEARNING_ZONE (
			     ZoneID int(11) NOT NULL auto_increment,
			     ZoneCode varchar(255) NOT NULL,
				 ZoneNameB5 varchar(255) NOT NULL,
			     ZoneNameEN varchar(255) NOT NULL,
				 ZoneQuota int(11) default NULL,
     			 PictureType int(11) default NULL,
				 Remark text default NULL,
				 YearID int(11) default NULL,
     			 IsDeleted int(1) default NULL,
				 InputBy int(11) default NULL,
			     DateInput datetime NOT NULL,
				 ModifiedBy int(11) default NULL,
			     DateModified datetime NOT NULL,
			     PRIMARY KEY (ZoneID),
			     KEY ZoneCode (ZoneCode),
			     KEY YearID (YearID)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8"
		);
		
		$sql_table[] = array(
			"2017-02-07",
			"Create Table RC_STUDENT_SCORE for student score input storage",
			"CREATE TABLE $reportcard_db.RC_STUDENT_SCORE (
			     InputScoreID int(11) NOT NULL auto_increment,
				 StudentID int(11) NOT NULL,
			     ClassLevelID int(11) NOT NULL,
				 TimeTableID int(11) NOT NULL,
			     TopicID int(11) NOT NULL,
				 ZoneID int(11) NOT NULL,
			     ToolID int(11) NOT NULL,
				 Score float default '-1',
     			 ScoreGrade varchar(11) default NULL,
				 InputBy int(11) default NULL,
			     DateInput datetime NOT NULL,
				 ModifiedBy int(11) default NULL,
			     DateModified datetime NOT NULL,
			     PRIMARY KEY (InputScoreID),
			     KEY StudentID (StudentID),
			     KEY ClassLevelID (ClassLevelID),
			     KEY TimeTableID (TimeTableID),
			     KEY TopicID (TopicID),
			     KEY ZoneID (ZoneID),
				 KEY ToolID (ToolID)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8"
		);
		
		$sql_table[] = array(
			"2017-02-07",
			"Create Table STUDENT_ACCESS_LOG for student access log",
			"CREATE TABLE $reportcard_db.STUDENT_ACCESS_LOG (
			     AccessLogID int(11) NOT NULL auto_increment,
				 StudentID int(11) NOT NULL,
			     ClassLevelID int(11) NOT NULL,
				 TimeTableID int(11) NOT NULL,
			     TopicID int(11) NOT NULL,
				 ZoneID int(11) NOT NULL,
			     ToolID int(11) default NULL,
				 StartTime datetime default NULL,
     			 EndTime datetime default NULL,
			     DateInput datetime NOT NULL,
			     DateModified datetime NOT NULL,
			     PRIMARY KEY (AccessLogID),
			     KEY StudentID (StudentID),
			     KEY ClassLevelID (ClassLevelID),
			     KEY TimeTableID (TimeTableID),
			     KEY TopicID (TopicID),
			     KEY ZoneID (ZoneID),
				 KEY ToolID (ToolID)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8"
		);
		
		$sql_table[] = array(
			"2017-02-09",
			"CREATE TABLE RC_EQUIPMENT_CATA_MAPPING",
			"CREATE TABLE $reportcard_db.RC_EQUIPMENT_CATA_MAPPING (
			     MappingID int(11) NOT NULL auto_increment,
			     CodeID int(11) NOT NULL,
     			 AbilityCatID int(11) NOT NULL,
			     InputBy int(11) default NULL,
			     DateInput datetime NOT NULL,
			     ModifiedBy int(11) default NULL,
			     DateModified datetime NOT NULL,
			     IsDeleted int(1) default NULL,
			     PRIMARY KEY (MappingID),
			     KEY CodeID (CodeID),
			     KEY AbilityCatID (AbilityCatID)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8"	
		);
		
		$sql_table[] = array(
				"2017-02-09",
				"CREATE TABLE RC_EQUIPMENT",
				"CREATE TABLE $reportcard_db.RC_EQUIPMENT (
				     CodeID int(11) NOT NULL auto_increment,
				     Code varchar(25) NOT NULL,
				     EN_Name varchar(40) NOT NULL,
				     CH_Name varchar(40) NOT NULL,
				     YearID int(10) default NULL,
				     PhotoPath varchar(100) default NULL,
				     Remarks varchar(255) default NULL,
				     DateInput datetime default NULL,
				     InputBy int(11) default NULL,
				     DateModified datetime NOT NULL,
				     ModifiedBy int(11) default NULL,
				     IsDeleted int(1) default NULL,
				     PRIMARY KEY (CodeID),
				     UNIQUE KEY Code (CodeID,Code)
				) ENGINE=InnoDB DEFAULT CHARSET=utf8"
		);
		
		$sql_table[] = array(
				"2017-02-09",
				"CREATE TABLE RC_TOPICTIMETABLE",
				"CREATE TABLE $reportcard_db.RC_TOPICTIMETABLE (
				     TopicTimeTableID int(11) NOT NULL auto_increment,
				     TimeTableCode varchar(25) NOT NULL,
				     EN_Name varchar(40) NOT NULL,
				     CH_Name varchar(40) NOT NULL,
				     YearID int(10) default NULL,
				     StartDate datetime default NULL,
				     EndDate datetime default NULL,
				     DateInput datetime default NULL,
				     InputBy int(11) default NULL,
				     DateModified datetime NOT NULL,
				     ModifiedBy int(11) default NULL,
				     IsDeleted int(1) default NULL,
				     PRIMARY KEY (TopicTimeTableID),
				     UNIQUE KEY Code (TopicTimeTableID)
				) ENGINE=InnoDB DEFAULT CHARSET=utf8"
		);
		
		$sql_table[] = array(
				"2017-02-09",
				"CREATE TABLE RC_TIMETABLE_TOPIC_MAPPING",
				"CREATE TABLE $reportcard_db.RC_TIMETABLE_TOPIC_MAPPING (
				     MappingID int(11)  NOT NULL auto_increment,
				     TopicTimeTableID int(11) NOT NULL,
				     TopicID int(11) NOT NULL,
				     DateInput datetime default NULL,
				     InputBy int(11) default NULL,
				     DateModified datetime NOT NULL,
				     ModifiedBy int(11) default NULL,
				     IsDeleted int(1) default NULL,
				     PRIMARY KEY (MappingID),
				     UNIQUE KEY Code (MappingID)
				) ENGINE=InnoDB DEFAULT CHARSET=utf8"
		);
		
		$sql_table[] = array(
				"2017-02-09",
				"CREATE TABLE RC_TIMETABLE_ZONE_MAPPING",
				"CREATE TABLE $reportcard_db.RC_TIMETABLE_ZONE_MAPPING (
				     MappingID int(11)  NOT NULL auto_increment,
				     TopicTimeTableID int(11) NOT NULL,
				     TopicID int(11) NOT NULL,
				     ZoneID int(11) NOT NULL,
				     DateInput datetime default NULL,
				     InputBy int(11) default NULL,
				     DateModified datetime NOT NULL,
				     ModifiedBy int(11) default NULL,
				     IsDeleted int(1) default NULL,
				     PRIMARY KEY (MappingID),
				     UNIQUE KEY Code (MappingID)
				) ENGINE=InnoDB DEFAULT CHARSET=utf8"
		);
		
		$sql_table[] = array(
				"2017-02-09",
				"CREATE TABLE RC_TIMETABLE_TOOL_MAPPING",
				"CREATE TABLE $reportcard_db.RC_TIMETABLE_TOOL_MAPPING (
				     MappingID int(11)  NOT NULL auto_increment,
				     TopicTimeTableID int(11) NOT NULL,
				     TopicID int(11) NOT NULL,
				     ZoneID int(11) NOT NULL,
				     ToolCodeID	int(11) NOT NULL,
				     DateInput datetime default NULL,
				     InputBy int(11) default NULL,
				     DateModified datetime NOT NULL,
				     ModifiedBy int(11) default NULL,
				     IsDeleted int(1) default NULL,
				     PRIMARY KEY (MappingID),
				     UNIQUE KEY Code (MappingID)
				) ENGINE=InnoDB DEFAULT CHARSET=utf8"
		);
		
		$sql_table[] = array(
				"2017-09-22",
				"CREATE TABLE RC_ABILITY_GRADING_RANGE",
				"CREATE TABLE $reportcard_db.RC_ABILITY_GRADING_RANGE (
				     GradingRangeID int(8) NOT NULL auto_increment,
     				 SchemeID int(8) default NULL,
				     Nature char(2) default NULL,
				     LowerLimit float default NULL,
				     UpperLimit float default NULL,
				     Grade varchar(255) default NULL,
				     Description varchar(255) default NULL,
				     DateInput datetime default NULL,
				     InputBy int(11) default NULL,
				     DateModified datetime default NULL,
				     ModifiedBy int(11) default NULL,
     				 PRIMARY KEY (GradingRangeID),
				     KEY SchemeID (SchemeID)
				) ENGINE=InnoDB DEFAULT CHARSET=utf8"
		);
		
		$sql_table[] = array(
				"2017-09-22",
				"CREATE TABLE RC_ABILITY_GRADE_REMARKS",
				"CREATE TABLE $reportcard_db.RC_ABILITY_GRADE_REMARKS (
				     AbilityRemarkID int(8) NOT NULL auto_increment,
     				 CatID int(8) NOT NULL,
				     GradingRangeID int(8) NOT NULL,
				     Remarks mediumtext,
				     DateInput datetime default NULL,
				     InputBy int(11) default NULL,
				     DateModified datetime default NULL,
				     ModifiedBy int(11) default NULL,
     				 PRIMARY KEY (AbilityRemarkID),
				     KEY CatID (CatID),
				     KEY GradingRangeID (GradingRangeID)
				) ENGINE=InnoDB DEFAULT CHARSET=utf8"
		);
		
		$sql_table[] = array(
				"2017-10-17",
				"CREATE TABLE RC_EQUIPMENT_CATEGORY",
				"CREATE TABLE $reportcard_db.RC_EQUIPMENT_CATEGORY (
				     CatID int(11) NOT NULL auto_increment,
				     Code varchar(25) NOT NULL,
				     EN_Name varchar(40) NOT NULL,
				     CH_Name varchar(40) NOT NULL,
				     YearID int(10) default NULL,
				     Remarks varchar(255) default NULL,
				     DateInput datetime default NULL,
				     InputBy int(11) default NULL,
				     DateModified datetime NOT NULL,
				     ModifiedBy int(11) default NULL,
				     IsDeleted int(1) default NULL,
				     PRIMARY KEY (CatID),
				     UNIQUE KEY Code (CatID,Code)
				) ENGINE=InnoDB DEFAULT CHARSET=utf8"
		);
		
		$sql_table[] = array(
				"2017-12-12",
				"CREATE TABLE RC_TOPIC",
				"CREATE TABLE $reportcard_db.RC_TOPIC (
				     TopicID int(11) NOT NULL auto_increment,
					 CatID int(11) NOT NULL,
					 YearID int(11) NOT NULL,
				     Code varchar(128) NOT NULL,
				     NameEn varchar(255) NOT NULL,
				     NameCh varchar(255) NOT NULL,
				     DisplayOrder tinyint(3) NOT NULL,
				     DateInput datetime NOT NULL,
     				 InputBy int(11) NOT NULL,
				     DateModified datetime NOT NULL,
				     LastModifiedBy int(11) NOT NULL,
				     IsDeleted int(1) default NULL,
				     PRIMARY KEY (TopicID),
				     UNIQUE KEY Code (TopicID, Code)
				) ENGINE=InnoDB DEFAULT CHARSET=utf8"
		);
		
		$sql_table[] = array(
				"2017-12-12",
				"CREATE TABLE RC_TOPIC_CATEGORY",
				"CREATE TABLE $reportcard_db.RC_TOPIC_CATEGORY (
				     TopicCatID int(11) NOT NULL auto_increment,
				     CatType tinyint(3) NOT NULL,
				     Code varchar(128) NOT NULL,
				     NameEn varchar(255) NOT NULL,
				     NameCh varchar(255) NOT NULL,
				     DateInput datetime NOT NULL,
     				 InputBy int(11) NOT NULL,
				     DateModified datetime NOT NULL,
				     LastModifiedBy int(11) NOT NULL,
				     IsDeleted int(1) default NULL,
				     PRIMARY KEY (TopicCatID),
				     UNIQUE KEY Code (TopicCatID, Code)
				) ENGINE=InnoDB DEFAULT CHARSET=utf8"
		);
		
		$sql_table[] = array(
			"2017-12-14",
			"Create Table RC_TOPIC_SCORE for student topic score storage",
			"CREATE TABLE $reportcard_db.RC_TOPIC_SCORE (
			     TopicScoreID int(11) NOT NULL auto_increment,
				 StudentID int(11) NOT NULL,
			     ClassLevelID int(11) NOT NULL,
				 TopicID int(11) NOT NULL,
				 TermID int(11) NOT NULL,
				 Score float default '-1',
     			 ScoreGrade varchar(11) default NULL,
				 isDeleted tinyint(1) default '0',
				 InputBy int(11) default NULL,
			     DateInput datetime NOT NULL,
				 ModifiedBy int(11) default NULL,
			     DateModified datetime NOT NULL,
			     PRIMARY KEY (TopicScoreID),
			     KEY StudentID (StudentID),
			     KEY ClassLevelID (ClassLevelID),
			     KEY TopicID (TopicID),
			     KEY TermID (TermID)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8"
		);
		
		$sql_table[] = array(
		    "2018-03-22",
		    "CREATE TABLE RC_TIMETABLE_CLASS_ZONE_QUOTA_MAPPING",
		    "CREATE TABLE $reportcard_db.RC_TIMETABLE_CLASS_ZONE_QUOTA_MAPPING (
    		    MappingID int(11) NOT NULL auto_increment,
    		    TopicTimeTableID int(11) NOT NULL,
    		    ZoneID int(11) NOT NULL,
    		    YearClassID int(11) NOT NULL,
				ClassZoneQuota int(11) default NULL,
    		    DateInput datetime default NULL,
    		    InputBy int(11) default NULL,
    		    DateModified datetime NOT NULL,
    		    ModifiedBy int(11) default NULL,
    		    IsDeleted int(1) default NULL,
    		    PRIMARY KEY (MappingID),
    		    UNIQUE KEY Code (MappingID)
    		) ENGINE=InnoDB DEFAULT CHARSET=utf8"
		);
		
		$sql_table[] = array(
		    '2018-06-11',
		    'CREATE TABLE RC_ABILITY_GRADE_REMARKS_ALL',
		    "CREATE TABLE $reportcard_db.RC_ABILITY_GRADE_REMARKS_ALL (
                AbilityRemarkID int(8) NOT NULL AUTO_INCREMENT,
                CatID int(8) NOT NULL,
                GradingRangeID int(8) NOT NULL,
                Remarks mediumtext,
                DateInput datetime DEFAULT NULL,
                InputBy int(11) DEFAULT NULL,
                DateModified datetime DEFAULT NULL,
                ModifiedBy int(11) DEFAULT NULL,
                PRIMARY KEY (AbilityRemarkID),
                KEY CatID (CatID),
                KEY GradingRangeID (GradingRangeID)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8"
		);
		
		$sql_table[] = array(
		    '2018-06-11',
		    'CREATE TABLE RC_ABILITY_GRADE_REMARKS_CUSTOM',
		    "CREATE TABLE $reportcard_db.RC_ABILITY_GRADE_REMARKS_CUSTOM (
                CustomRemarkID int(8) NOT NULL AUTO_INCREMENT,
                AbilityRemarkID int(8) NOT NULL,
                CatID int(8) NOT NULL,
                GradingRangeID int(8) NOT NULL,
                YearID int(8) DEFAULT NULL,
                YearTermID int(8) DEFAULT NULL,
                Remarks mediumtext,
                DateInput datetime DEFAULT NULL,
                InputBy int(11) DEFAULT NULL,
                DateModified datetime DEFAULT NULL,
                ModifiedBy int(11) DEFAULT NULL,
                PRIMARY KEY (CustomRemarkID),
                KEY AbilityRemarkID (AbilityRemarkID),
                KEY CatID (CatID),
                KEY GradingRangeID (GradingRangeID),
                KEY YearID (YearID),
                KEY YearTermID (YearTermID)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8"
		);
		
		$sql_table[] = array(
		    '2018-07-25',
		    'CREATE TABLE RC_SUBJECT',
		    "CREATE TABLE $reportcard_db.RC_SUBJECT (
                CodeID int(11) NOT NULL AUTO_INCREMENT,
                YearID int(11) NOT NULL,
                SubjectID int(11) NOT NULL,
                TermID int(11) DEFAULT NULL,
                TopicID int(11) DEFAULT NULL,
                isTerm tinyint(1) DEFAULT '0',
                DateInput datetime DEFAULT NULL,
                InputBy int(11) DEFAULT NULL,
                DateModified datetime DEFAULT NULL,
                ModifiedBy int(11) DEFAULT NULL,
                IsDeleted int(1) DEFAULT NULL,
                PRIMARY KEY (CodeID),
                KEY YearID (YearID),
                KEY SubjectID (SubjectID),
                KEY TermID (TermID),
                KEY TopicID (TopicID),
                KEY isTerm (isTerm)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8"
		);
		
		$sql_table[] = array(
		    '2018-07-25',
		    'CREATE TABLE RC_SUBJECT_CATA_MAPPING',
		    "CREATE TABLE $reportcard_db.RC_SUBJECT_CATA_MAPPING (
                MappingID int(11) NOT NULL AUTO_INCREMENT,
                CodeID int(11) NOT NULL,
                AbilityCatID int(11) NOT NULL,
                DateInput datetime DEFAULT NULL,
                InputBy int(11) DEFAULT NULL,
                DateModified datetime DEFAULT NULL,
                ModifiedBy int(11) DEFAULT NULL,
                IsDeleted int(1) DEFAULT NULL,
                PRIMARY KEY (MappingID),
                KEY CodeID (CodeID),
                KEY AbilityCatID (AbilityCatID)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8"
		);
		
		$sql_table[] = array(
    		'2018-08-09',
    		'CREATE TABLE RC_SUBJECT_TOPIC_SCORE',
    		"CREATE TABLE $reportcard_db.RC_SUBJECT_TOPIC_SCORE (
        		TopicScoreID int(11) NOT NULL AUTO_INCREMENT,
        		StudentID int(11) NOT NULL,
                CodeID int(11) NOT NULL,
        		ClassLevelID int(11) NOT NULL,
        		ClassID int(11) NOT NULL,
                SubjectID int(11) NOT NULL,
                TWItemID int(11) NOT NULL,
				Score float default '-1',
     			ScoreGrade varchar(11) default NULL,
				isDeleted tinyint(1) default '0',
        		DateInput datetime DEFAULT NULL,
        		InputBy int(11) DEFAULT NULL,
        		DateModified datetime DEFAULT NULL,
        		ModifiedBy int(11) DEFAULT NULL,
        		PRIMARY KEY (TopicScoreID),
        		KEY StudentID (StudentID),
        		KEY CodeID (CodeID),
        		KEY ClassLevelID (ClassLevelID),
        		KEY ClassID (ClassID),
        		KEY SubjectID (SubjectID),
                KEY TWItemID (TWItemID)
    		) ENGINE=InnoDB DEFAULT CHARSET=utf8"
		);
		
		$sql_table[] = array(
		    '2018-11-23',
		    'CREATE TABLE RC_ABILITY_GRADE_REMARKS_TARGET_CAT',
		    "CREATE TABLE $reportcard_db.RC_ABILITY_GRADE_REMARKS_TARGET_CAT (
    		    RecordID int(11) NOT NULL AUTO_INCREMENT,
    		    CatID int(8) NOT NULL,
                YearTermID int(8) DEFAULT '0',
    		    ClassLevelID int(8) DEFAULT '0',
    		    TargetMOCat varchar(5) DEFAULT NULL,
    		    isDeleted tinyint(1) DEFAULT '0',
    		    DateInput datetime DEFAULT NULL,
    		    InputBy int(11) DEFAULT NULL,
    		    DateModified datetime DEFAULT NULL,
    		    ModifiedBy int(11) DEFAULT NULL,
    		    PRIMARY KEY (RecordID),
    		    KEY CatID (CatID),
    		    KEY YearTermID (YearTermID),
    		    KEY ClassLevelID (ClassLevelID),
    		    KEY TargetMOCat (TargetMOCat)
    		) ENGINE=InnoDB DEFAULT CHARSET=utf8"
		);
		
		$sql_table[] = array(
		    '2019-02-01',
		    'CREATE TABLE RC_INPUT_SCORE_PERIOD_SETTING',
		    "CREATE TABLE $reportcard_db.RC_INPUT_SCORE_PERIOD_SETTING (
    		    SettingID int(11) NOT NULL AUTO_INCREMENT,
                YearTermID int(8) DEFAULT '0',
                StartDate datetime default NULL,
			    EndDate datetime default NULL,
    		    DateInput datetime DEFAULT NULL,
    		    InputBy int(11) DEFAULT NULL,
    		    DateModified datetime DEFAULT NULL,
    		    ModifiedBy int(11) DEFAULT NULL,
    		    PRIMARY KEY (SettingID),
    		    KEY YearTermID (YearTermID)
    		) ENGINE=InnoDB DEFAULT CHARSET=utf8"
		);
		
		$sql_table[] = array(
		    '2019-02-01',
		    'CREATE TABLE RC_VIEW_REPORT_PERIOD_SETTING',
		    "CREATE TABLE $reportcard_db.RC_VIEW_REPORT_PERIOD_SETTING (
    		    SettingID int(11) NOT NULL AUTO_INCREMENT,
                YearTermID int(8) DEFAULT '0',
                StartDate datetime default NULL,
			    EndDate datetime default NULL,
    		    DateInput datetime DEFAULT NULL,
    		    InputBy int(11) DEFAULT NULL,
    		    DateModified datetime DEFAULT NULL,
    		    ModifiedBy int(11) DEFAULT NULL,
    		    PRIMARY KEY (SettingID),
    		    KEY YearTermID (YearTermID)
    		) ENGINE=InnoDB DEFAULT CHARSET=utf8"
		);
		
		$sql_table[] = array(
		    '2019-02-25',
		    'CREATE TABLE RC_ADMIN_GROUP',
		    "CREATE TABLE $reportcard_db.RC_ADMIN_GROUP (
    		     AdminGroupID int(11) NOT NULL AUTO_INCREMENT,
                 AdminGroupCode varchar(128) DEFAULT NULL,
                 AdminGroupNameEn varchar(128) DEFAULT NULL,
                 AdminGroupNameCh varchar(128) DEFAULT NULL,
                 DisplayOrder int(11) DEFAULT NULL,
                 RecordStatus tinyint(3) DEFAULT '1',
                 DateInput datetime DEFAULT NULL,
                 InputBy int(11) DEFAULT NULL,
                 DateModified datetime DEFAULT NULL,
                 LastModifiedBy int(11) DEFAULT NULL,
                 PRIMARY KEY (AdminGroupID),
                 KEY RecordStatus (RecordStatus),
                 KEY AdminGroupCode (AdminGroupCode)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8"
		);
		
		$sql_table[] = array(
		    '2019-02-25',
		    'CREATE TABLE RC_ADMIN_GROUP_USER',
		    "CREATE TABLE $reportcard_db.RC_ADMIN_GROUP_USER (
    		     AdminGroupUserID int(11) NOT NULL AUTO_INCREMENT,
                 AdminGroupID int(11) DEFAULT NULL,
                 UserID int(11) DEFAULT NULL,
                 DateInput datetime DEFAULT NULL,
                 InputBy int(11) DEFAULT NULL,
                 DateModified datetime DEFAULT NULL,
                 LastModifiedBy int(11) DEFAULT NULL,
                 PRIMARY KEY (AdminGroupUserID),
                 KEY AdminGroupUser (AdminGroupID,UserID),
                 KEY UserID (UserID)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8"
		);
		
		$sql_table[] = array(
		    '2019-02-25',
		    'CREATE TABLE RC_ADMIN_GROUP_RIGHT',
		    "CREATE TABLE $reportcard_db.RC_ADMIN_GROUP_RIGHT (
                 AdminGroupRightID int(11) NOT NULL AUTO_INCREMENT,
                 AdminGroupID int(11) DEFAULT NULL,
                 AdminGroupRightName varchar(128) DEFAULT NULL,
                 DateInput datetime DEFAULT NULL,
                 InputBy int(11) DEFAULT NULL,
                 DateModified datetime DEFAULT NULL,
                 LastModifiedBy int(11) DEFAULT NULL,
                 PRIMARY KEY (AdminGroupRightID),
                 KEY AdminGroupRight (AdminGroupID,AdminGroupRightName),
                 KEY AdminGroupRightName (AdminGroupRightName)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8"
		);

        $sql_table[] = array(
            '2019-10-10',
            'CREATE TABLE RC_ARCHIVE_REPORT_CARD',
            "CREATE TABLE $reportcard_db.RC_ARCHIVE_REPORT_CARD (
                 ArchiveID int(11) NOT NULL AUTO_INCREMENT,
                 YearID int(8) NOT NULL,
                 YearClassID int(8) NOT NULL,
                 YearTermID int(8) DEFAULT NULL,
                 StudentID int(8) NOT NULL,
                 DateInput datetime DEFAULT NULL,
                 InputBy int(11) DEFAULT NULL,
                 DateModified datetime DEFAULT NULL,
                 LastModifiedBy int(11) DEFAULT NULL,
                 PRIMARY KEY (ArchiveID),
                 KEY YearID (YearID),
                 KEY YearClassID (YearClassID),
                 KEY YearTermID (YearTermID),
                 KEY StudentID (StudentID)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8"
        );
        */

        $sql_table[] = array(
            "2020-08-27",
            "Create Table RC_ABILITY_INDEX_ITEM for items of ability index",
            "CREATE TABLE $reportcard_db.RC_ABILITY_INDEX_ITEM (
			     ItemID int(11) NOT NULL auto_increment,
				 RubricSettingID int(11) NOT NULL,
			     Code varchar(255) NOT NULL,
			     Name varchar(255) NOT NULL,
			     Type tinyint(1) NOT NULL,
			     Level int(8) NOT NULL,
			     UpperCat varchar(20) default NULL,
			     DateInput datetime NOT NULL,
			     DateModified datetime NOT NULL,
			     PRIMARY KEY (ItemID),
				 KEY (RubricSettingID)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8"
        );

        $sql_table[] = array(
        		'2020-08-27',
        		'CREATE TABLE RC_RUBRIC_SETTING',
        		"CREATE TABLE $reportcard_db.RC_RUBRIC_SETTING(
					RubricSettingID int(11) NOT NULL AUTO_INCREMENT,
					Name varchar(255) NOT NULL,
					YearID  int(11) NOT NULL,
					LevelNum tinyint(1) NOT NULL,
					GradeNum tinyint(1) NOT NULL,
					ExamGradeNum tinyint(1) NOT NULL,
					InputBy int(11) NOT NULL,
					DateInput DATETIME NOT NULL,
					ModifiedBy int(11) NOT NULL,
					DateModified DATETIME NOT NULL,
					PRIMARY KEY(RubricSettingID)
				) ENGINE=InnoDB DEFAULT CHARSET=utf8;"
        );
        $sql_table[] = array(
        		'2020-08-27',
        		'CREATE TABLE RC_RUBRIC_SETTING_ITEM',
        		"CREATE TABLE $reportcard_db.RC_RUBRIC_SETTING_ITEM(
					ItemID int(11) NOT NULL AUTO_INCREMENT,
					RubricSettingID int(11) NOT NULL,
					Type varchar(25) NOT NULL,
					Level int(2) NOT NULL,
					Content varchar(255) NOT NULL,
					PRIMARY KEY(ItemID),
					KEY(RubricSettingID)
				) ENGINE=InnoDB DEFAULT CHARSET=utf8;"
        );
        
        $sql_table[] = array(
        		"2020-09-03",
        		"Create Table RC_ABILITY_INDEX_MAPPING for mapping between categories of ability index",
        		"CREATE TABLE $reportcard_db.RC_ABILITY_INDEX_MAPPING (
	        		MappingID int(11) NOT NULL auto_increment,
	        		RubricSettingID int(11) NOT NULL,
					ItemID int(11) NOT NULL,
	        		DateInput datetime NOT NULL,
	        		DateModified datetime NOT NULL,
					PRIMARY KEY (MappingID),
	        		KEY (RubricSettingID)
        		) ENGINE=InnoDB DEFAULT CHARSET=utf8;"
        );
        
        $sql_table[] = array(
        		"2020-09-10",
        		"Create Table RC_TOPIC_SETTING",
        		"CREATE TABLE $reportcard_db.RC_TOPIC_SETTING(
					TopicSettingID int(11) AUTO_INCREMENT,
					Name varchar(255) NOT NULL,
					YearID int(11) NOT NULL,
					RubricSettingID int(11) NOT NULL,
					ExamPeriodStart DATE NOT NULL,
					ExamPeriodEnd DATE NOT NULL,
					MarkPeriodStart DATE NOT NULL,
					MarkPeriodEnd DATE NOT NULL,
					RubricList TEXT NOT NULL,
					DateInput DATETIME NOT NULL,
					InputBy int(11) NOT NULL,
					DateModified DATETIME NOT NULL,
					ModifiedBy int(11) NOT NULL,
					PRIMARY KEY(TopicSettingID),
					KEY(RubricSettingID)
				) ENGINE=InnoDB DEFAULT CHARSET=utf8;"
        );
        
        $sql_table[] = array(
        		"2020-09-14",
        		"Create Table RC_ACTIVITY_SETTING",
        		"CREATE TABLE $reportcard_db.RC_ACTIVITY_SETTING(
					ActivitySettingID int(11) NOT NULL AUTO_INCREMENT,
					ReportName varchar(100) NOT NULL,
					ApplyTopic tinyint(1) NOT NULL,
					TopicSettingID int(11) NULL,
					RubricCode varchar(50) NULL,
					ActivityTitle varchar(100) DEFAULT '',
					ActivityName varchar(100) NOT NULL,
					YearID int(11) NOT NULL,
					ActivityDate DATE NOT NULL,
					ActivityTime int(4) DEFAULT 0,
					Location varchar(100) NOT NULL,
					ContentType varchar(50) NOT NULL,
					DateInput DATETIME NOT NULL,
					InputBy int(11) NOT NULL,
					DateModified DATETIME NOT NULL,
					ModifiedBy int(11) NOT NULL,
					PRIMARY KEY(ActivitySettingID)
				) ENGINE=InnoDB DEFAULT CHARSET=utf8;"
        );
        
        $sql_table[] = array(
        		"2020-09-14",
        		"Create Table RC_ZONE",
        		"CREATE TABLE $reportcard_db.RC_ZONE (
        			ZoneID int(11) NOT NULL auto_increment,
	        		Name varchar(255) NOT NULL,
	        		Quota int(11) default NULL,
	        		PictureType int(11) default NULL,
					TopicSettingID int(11) NOT NULL,
	        		InputBy int(11) default NULL,
	        		DateInput datetime NOT NULL,
	        		ModifiedBy int(11) default NULL,
	        		DateModified datetime NOT NULL,
	        		PRIMARY KEY (ZoneID)
		        ) ENGINE=InnoDB DEFAULT CHARSET=utf8"
        );
        
        $sql_table[] = array(
        		"2020-09-15",
        		"Create Table RC_TOOL",
        		"CREATE TABLE $reportcard_db.RC_TOOL (
        			ToolID int(11) NOT NULL auto_increment,
	        		Name varchar(255) NOT NULL,
					YearID int(11) NOT NULL,
	        		TopicSettingID int(11) NOT NULL,
					ZoneID int(11) NOT NULL,
					RubricCode TEXT DEFAULT '',
					PhotoSRC TEXT DEFAULT '',
	        		InputBy int(11) default NULL,
	        		DateInput datetime NOT NULL,
	        		ModifiedBy int(11) default NULL,
	        		DateModified datetime NOT NULL,
	        		PRIMARY KEY (ToolID)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8"
        );

        $sql_table[] = array(
            "2020-09-15",
            "Create Table RC_REPORT",
            "CREATE TABLE $reportcard_db.RC_REPORT (
        			ReportID int(11) NOT NULL auto_increment,
	        		Name varchar(255) NOT NULL,
					YearID int(11) NOT NULL,
					StartDate DATE NOT NULL,
					EndDate DATE NOT NULL,
	        		CoverIncludeInfo tinyint(1) NOT NULL,
	        		CoverTitle varchar(255) DEFAULT NULL,
	        		CoverSubTitle varchar(255) DEFAULT NULL,
	        		CAReportTitle varchar(255) DEFAULT NULL,
	        		TopicID varchar(255) NOT NULL,
					ActivityID varchar(255) NOT NULL,
					TermAssessmentIncluded tinyint(1) NOT NULL,
	        		TermAssessmentTitle varchar(255) DEFAULT NULL,
					TermCommentIncluded tinyint(1) NOT NULL,
	        		TermCommentTitle varchar(255) DEFAULT NULL,
					PageLastPageIncludeInfo tinyint(1) NOT NULL,
	        		InputBy int(11) default NULL,
	        		DateInput datetime NOT NULL,
	        		ModifiedBy int(11) default NULL,
	        		DateModified datetime NOT NULL,
	        		PRIMARY KEY (ReportID),
    		        KEY YearID (YearID)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8"
        );
        
        $sql_table[] = array(
        		"2020-09-17",
        		"Create Table RC_COMMENT",
        		"CREATE TABLE $reportcard_db.RC_COMMENT (
			        CommentID int(11) NOT NULL AUTO_INCREMENT,
					Content TEXT NOT NULL,
					CatID int(11) NULL,
					InputBy int(11) default NULL,
					DateInput datetime NOT NULL,
					ModifiedBy int(11) default NULL,
					DateModified datetime NOT NULL,
					PRIMARY KEY(CommentID)
				) ENGINE=InnoDB DEFAULT CHARSET=utf8"
        );
        
        $sql_table[] = array(
        		"2020-10-29",
        		"Create Table RC_STUDENT_SCORE",
        		"CREATE TABLE $reportcard_db.RC_STUDENT_SCORE (
				     InputScoreID int(11) NOT NULL auto_increment,
				     StudentID int(11) NOT NULL,
				     TopicSettingID int(11) NOT NULL,
				     RubricCode varchar(255) NOT NULL,
				     isNA tinyint(1) DEFAULT '0',
				     Score int(2) DEFAULT '0',
				     InputBy int(11) default NULL,
				     DateInput datetime NOT NULL,
				     ModifiedBy int(11) default NULL,
				     DateModified datetime NOT NULL,
				     PRIMARY KEY (InputScoreID),
				     KEY StudentID (StudentID),
				     KEY TopicSettingID (TopicSettingID)
				) ENGINE=InnoDB DEFAULT CHARSET=utf8"
        );
        
        $sql_table[] = array(
        		"2020-11-03",
        		"Create Table RC_ACTIVITY",
        		"CREATE TABLE $reportcard_db.RC_ACTIVITY (
        			RecordID int(11) NOT NULL AUTO_INCREMENT,
					StudentID int(11) NOT NULL,
					ActivitySettingID int(11) NOT NULL,
					ActivityDate DATE NOT NULL,
					Duration int(4) DEFAULT NULL,
					Location varchar(255) DEFAULT NULL,
					imgSrc TEXT DEFAULT NULL,
					Description TEXT DEFAULT NULL,
				    InputBy int(11) default NULL,
				    DateInput datetime NOT NULL,
				    ModifiedBy int(11) default NULL,
				    DateModified datetime NOT NULL,
					PRIMARY KEY(RecordID),
					KEY StudentID(StudentID),
					KEY ActivitySettingID(ActivitySettingID)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8"
        );

		print("<p>Creating tables for ".$DatabaseAry[$a][0]."...</p>\n");
		
		# Create new db tables
		$numOfCreateTable = count((array)$sql_table);
		for($i=0; $i<$numOfCreateTable; $i++)
		{
			$sql = $sql_table[$i];
			
			if (is_array($sql)) {
				// new logic => have input date, desc, and the sql statement
				updateSchema_PowerPortfolio($flag, $last_schema_date_updated, $sql[0], $sql[1], $sql[2]);
			}
			else {
				// old logic => only have the sql statement
				if ($flag==2) {
					// will not include old schema update for "update new schema only"
				}
				else {
					if($li->db_db_query($sql)){
						echo "<p>Created (or skip creating) a new table:<br />".$sql."</p>\n";
					}
					else {
						echo "<p>Failed to create table:<br />".$sql."<br />Error: ".mysql_error()."</p>\n";
					}
				}
			}
		}
		
		// 2012-03-06 [Please follow this format afterwards]
//		$sql_alter[] = array(
//			"2012-03-06",
//			"Drop test eRC DB schema table",
//			"Drop Table $reportcard_db.RC_TEMP"
//		);

        /*
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_STUDENT_SCORE ADD COLUMN isDeleted tinyint(1) default '0' AFTER ScoreGrade";
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_EQUIPMENT ADD COLUMN CategoryID int(11) default '0'";
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_TIMETABLE_ZONE_MAPPING ADD COLUMN ZoneQuota int(11) default NULL";
		$sql_alter[] = "ALTER TABLE $reportcard_db.STUDENT_ACCESS_LOG MODIFY ToolID varchar(255) default NULL";
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_TOPIC ADD COLUMN TermID int(11) default '0' AFTER YearID";
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_STUDENT_SCORE ADD COLUMN isFromMgmt tinyint(1) default '0' AFTER ScoreGrade";
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_TOPIC_CATEGORY ADD COLUMN DisplayOrder tinyint(3) NOT NULL AFTER NameCh";
// 		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_SUBJECT_CATA_MAPPING ADD KEY(CodeID)";
// 		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_SUBJECT_CATA_MAPPING ADD KEY(AbilityCatID)";
// 		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_SUBJECT ADD KEY(SubjectID)";
// 		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_SUBJECT ADD KEY(YearID)";
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_EQUIPMENT ADD COLUMN ZoneID int(11) default '0'";
// 		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_EQUIPMENT ADD KEY(ZoneID)";
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_EQUIPMENT ADD COLUMN Chapter varchar(255) default NULL";
		*/

		# Update db tables
		$numOfAlterSql = count((array)$sql_alter);
		for($i=0; $i<$numOfAlterSql; $i++)
		{
			$sql = $sql_alter[$i];
			
			if (is_array($sql)) {
				// new logic => have input date, desc, and the sql statement
				updateSchema_PowerPortfolio($flag, $last_schema_date_updated, $sql[0], $sql[1], $sql[2]);
			}
			else {
				// old logic => only have the sql statement
				if ($flag==2) {
					// will not include old schema update for "update new schema only"
				}
				else {
					if($li->db_db_query($sql)){
						echo "<p>Altered table: ".$sql."</p>\n";
					}
					else {
						echo "<p>Failed to alter table:<br />".$sql."<br />Error: ".mysql_error()."</p>\n";
					}
				}
			}
		}
		
		print("<p>=============================== Finish Creating tables for ".$DatabaseAry[$a][0]."... =============================== </p><br /><br />\n");
	}
	?>
	</body>
	<? 
	if ($_REQUEST['direct_run'] == 1) 
		intranet_closedb(); 
}
else { ?>
	<html>
	<body>
		The client has not purchased Power Portfolio.
	</body>
	</html>
<? } ?>