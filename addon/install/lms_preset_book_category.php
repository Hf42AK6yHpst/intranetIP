<?php
// using : 
@SET_TIME_LIMIT(2000);

$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/liblibrarymgmt.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.b5.php");

intranet_opendb();


$liblib = new liblms();


if (is_array($PRESET_BOOK_CATEGORY[$CatCode]))
{
	$book_cats = $PRESET_BOOK_CATEGORY[$CatCode];
	for ($i=0; $i<sizeof($book_cats); $i++)
	{
		$catObj = $book_cats[$i];
		
		if ($ToUpdate)
		{
			$sql = "UPDATE LIBMS_BOOK_CATEGORY set DescriptionChi='".addslashes(trim($catObj[1]))."', DescriptionEn='".addslashes(trim($catObj[2]))."' WHERE BookCategoryCode='".addslashes($catObj[0])."' ";
		} else
		{
			$sql = "INSERT INTO LIBMS_BOOK_CATEGORY (BookCategoryCode, DescriptionChi, DescriptionEn, DateModified, LastModifiedBy) " .
					"VALUES ('".addslashes($catObj[0])."', '".addslashes(trim($catObj[1]))."', '".addslashes(trim($catObj[2]))."', now(), 1) ";
		}
		$liblib->db_db_query($sql);
		debug($sql);
	}
}

intranet_closedb();
?>