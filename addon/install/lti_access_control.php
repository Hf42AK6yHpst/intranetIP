<?php
	$PATH_WRT_ROOT = '../../';
	include_once($PATH_WRT_ROOT.'includes/global.php');
	include_once($PATH_WRT_ROOT.'includes/libdb.php');
	include_once($PATH_WRT_ROOT.'includes/liblti.php');
	include_once("../check.php");
	
	intranet_opendb();
	$liblti = new liblti();
?>
<!DOCTYPE html>
<html>
<head>
<style>
	body{
		background-color: grey;
	}
		
	.import_table{
		width:700px; height:auto; margin: 0 auto;	
		padding: 20px;	
		background-color: white;
		border-style:solid;
		border-width:1px;
	}
	
	#import_form{
		width:600px;
  		 display: inline-block;		
	}
	
	#quotation_table{
  		
	}
	
	input, textarea {   
	    padding: 9px;  
	    border: solid 1px #E5E5E5;  
	    outline: 0;  
	    font: normal 13px/100% Verdana, Tahoma, sans-serif;  
	    width: 200px;  
	    background: #FFFFFF;  
	    }  	 
	  
	textarea {   
	    width: 400px;  
	    max-width: 400px;  
	    height: 150px;  
	    line-height: 150%;  
	    }  
	  
	input:hover, textarea:hover,  
	input:focus, textarea:focus {   
	    border-color: #C9C9C9;   
	    }  
	  
	.form label {   
	    margin-left: 10px;   
	    color: #999999;   
	    }  
	  
	.submit input {  
	    width: auto;  
	    padding: 9px 15px;  
	    background: #617798;  
	    border: 0;  
	    font-size: 14px;  
	    color: #FFFFFF;  
    }  

	div label{
		padding-right: 40px;
	}
	
	#quotation_table{
		border-collapse: separate;
		border-spacing: 0px;
		font-size: 16px;
		text-align: center;
		font-family:Arial,Helvetica,sans-serif;
	}
	#quotation_table tr th {
		margin: 0px;
		padding: 3px;
		padding-top: 5px;
		padding-bottom: 5px;
		background-color: #A6A6A6;
		font-weight: normal;
		color: #FFFFFF;
		border-bottom: 1px solid #CCCCCC;
		border-right: 1px solid #CCCCCC;
	}
	
	#quotation_table td {
		padding-top: 5px;
		padding-bottom: 5px;
	}
</style>
<script src="jquery.min.js"></script>
<script>
	$("document").ready(function(){
		$(".delete_btn").live("click", function(){
			if(confirm("Delete this quotation ?")){				
				var quotation_id = $(this).attr('value');
				$(".app_record_id").val(quotation_id);
				$("#quotation_form").submit();
			}
		});
		$("#app_name").change(function(){
			var value = $("#app_name").val();
			if(value=="PaGamO"){
				$('#launch_url').val('https://www.pagamo.org/lti2_provider/messages/blti');
			}else{
				$('#launch_url').val('');
			}
		});
	});

	function formCheck(){
		if($('#app_name').val()==""){
			alert('please input a proper app name');
			return false;
		}
		if($('#school_code').val()==""){
			alert('please input a proper school code');
			return false;
		}
		if($('#launch_url').val()==""){
			alert('please input a proper launch url');
			return false;
		}
		if($('#launch_key').val()==""){
			alert('please input a proper launch key');
			return false;
		}
		if($('#launch_secret').val()==""){
			alert('please input a proper launch secret');
			return false;
		}
		return true;
	}
</script>
</head>
<body>
	<div class="import_table" style="">			
		<form id="import_form" class="form" action="lti_record_mgmt.php" enctype="multipart/form-data" accept-charset="UTF-8" method="post" onsubmit="return formCheck()">
			<h3 style="width: auto;   background-color: #eeeeee;   margin: 5px;">New LTI App Setting</h3>
			<br/>
			<div class="app">
				<label for="app_name">App Name</label>
					<span style=" margin-left: 34px;">
						<select name="app_name" id="app_name">
							<option value="">----Select----</option>
							<option value="PaGamO">PaGamO</option>
						</select>
					</span>
			</div>
			
			<br/>
			<div class="schoolCode">
				<label for="school_code">School Code</label>
					<span style=" margin-left: 34px;"><input type="text" name="school_code" id="school_code" value="<?=$config_school_code?>"/></span>
			</div>
			
			<br/>
			
			<div class="launchURL">
				<label for="launch_url">Launch URL</label> 
				<span style=" margin-left: 34px;"><input type="text" name="launch_url" id="launch_url"/></span>	
			</div>
			<br/>
			
			<div class="launchKey">
				<label for="launch_key">Launch Key</label> 
				<span style=" margin-left: 34px;"><input type="text" name="launch_key" id="launch_key"/></span>	
			</div>
			<br/>
			
			<div class="launchSecret">
				<label for="launch_secret">Launch Secret</label> 
				<span style=" margin-left: 34px;"><input type="text" name="launch_secret" id="launch_secret"/></span>		
			</div>
			<br/>
				
			<div class="submit">
				<input type="hidden"  class="add_action" name="action" value="add"/>
				<input type="submit" value="Send" />
			</div>
		
		</form>		
		
		
			<br/>
			<br/>	
		<form id="quotation_form" action="lti_record_mgmt.php" enctype="multipart/form-data" accept-charset="UTF-8" method="post">
			<input type="hidden"  class="app_record_id" name="app_record_id" />
			<input type="hidden"  class="remove_action" name="action" value="remove"/>
		</form>			
		<span>-----------------------------------------------------------------------------------------</span><br/>
		<h3>Current Settings</h3><br/>
		<table style="width: 600px;" id="quotation_table">
			<thead>
				<th>#</th>
				<th style="width:20%">School Code</th>
				<th>App Name</th>
				<th>Launch URL</th>
				<th>Key</th>
				<th style="width:15%">Secret</th>
				<th>Delete</th>				
			</thead>
			<tbody>
				<?= $liblti->getFullAppList(); ?>		
			</tbody>
		</table>			
	</div>
</body>
</html>

<?php
	intranet_closedb();
?>