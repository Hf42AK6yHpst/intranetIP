<?php
// using : 
@SET_TIME_LIMIT(1000);

$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libcal.php");
include_once($PATH_WRT_ROOT."includes/libcalevent.php");
include_once($PATH_WRT_ROOT."includes/libcalevent2007a.php");
include_once("../check.php");

intranet_opendb();

$libdb = new libdb();
$luser = new libuser();
$lcalevent = new libcalevent2007();

$successAry = array();
$successAry['synUser'] = $luser->synUserDataToModules('', array('library_management_system'));
$successAry['synHoliday'] = $lcalevent->synEventToModules('', array('library_management_system'));

debug_pr($successAry);


###################################################################
####################### Verify Data [Start] #######################
###################################################################
function LMS_Insert_Result_Cross_Check () {
	global $PATH_WRT_ROOT, $plugin, $intranet_db;
           $successAry = array();
          
           ### Syn data to LMS - INTRANET DATA
           include_once($PATH_WRT_ROOT."includes/liblibrarymgmt.php");
           $liblms = new liblms();
           $LIBMS_USER = $liblms->db.'.LIBMS_USER';
           $INTRANET_USER = $intranet_db.'.INTRANET_USER';
           $sql = "Select
                               UserID,
                               UserLogin,
                               UserEmail,
                               EnglishName,
                               ChineseName,
                               RecordType,
                               Teaching,
                               ClassNumber,
                               ClassName,
                               HomeTelNo,
                               MobileTelNo,
                               PersonalPhotoLink,
                               DateModified
                       From
                               $INTRANET_USER as iu
                       Where
                               RecordStatus = 1
               	   ";
           $intranetUserDataAry = $liblms->returnResultSet($sql);
           $numOfUser = count($intranetUserDataAry);
                      
           $intranetUserDataAssoAry = array();
           $intranetUserDataAssoAry = BuildMultiKeyAssoc($intranetUserDataAry, 'UserID');
               
           # LIBMS DATA
           $sql = "Select UserID, UserLogin, EnglishName, ChineseName, UserEmail, ClassNumber, ClassName, HomeTelNo, MobileTelNo, DateModified, UserPhoto, UserType From $LIBMS_USER";
		   $lmsUserDataAry = $liblms->returnResultSet($sql);              
           $lmsUserDataAssoAry = BuildMultiKeyAssoc($lmsUserDataAry, 'UserID');
   
           
//         unset($lmsUserDataAry);
//		   $fieldMappingAry[LmsField] = IntranetUserField  
           $fieldMappingAry = array();
           $fieldMappingAry['UserID'] = 'UserID';
           $fieldMappingAry['UserLogin'] = 'UserLogin';
           $fieldMappingAry['UserEmail'] = 'UserEmail';
           $fieldMappingAry['EnglishName'] = 'EnglishName';
           $fieldMappingAry['ChineseName'] = 'ChineseName';
           $fieldMappingAry['ClassNumber'] = 'ClassNumber';
           $fieldMappingAry['ClassName'] = 'ClassName';
           $fieldMappingAry['HomeTelNo'] = 'HomeTelNo';
           $fieldMappingAry['MobileTelNo'] = 'MobileTelNo';
           $fieldMappingAry['UserPhoto'] = 'PersonalPhotoLink';
           $fieldMappingAry['DateModified'] = 'DateModified'; 
                                
	       // loop student  
	       $array = array();         
	       $total = 0;       
	       $style = "color:red;";
	          
           foreach ($lmsUserDataAssoAry as $_userId => $_userIdDataArray)
           {
           		$total ++;  
       		       		
               	// loop each field of each student
               	foreach ($fieldMappingAry as $__lmsField => $__intranetUserField) {
               		$array[$__lmsField]['NotOk'] = 0;
               		$array[$__lmsField]['OK'] = 0;
               		$array[$__lmsField]['NotOkDisplay'] = "N/A";
               		$array['userType']['Ok'] = 0;
               		$array['userType']['NotOk'] = 0;
               		$array['userType']['NotOkDisplay'] = "N/A";
             		$__lmsData = $lmsUserDataAssoAry[$_userId][$__lmsField];
              		$__intranetUserData = $intranetUserDataAssoAry[$_userId][$__intranetUserField];
              			
              		if ($__lmsData == $__intranetUserData) {
	                   $array[$__lmsField]['OK']++; 
           
	                }
	                else {
	                   $array[$__lmsField]['NotOk']++;     
	                   $array[$__lmsField]['NotOkDisplay'] = $__lmsData . "  vs  " . $__intranetUserData . "<br />";
	                }         

               		if($array[$__lmsField]['NotOk'] > 0){             			
               		   $array['status'] = 'Pass';
               		}
               		else{
               		   $array['status'] = 'Fail';
               		}
              		           		
                } // end foreach ($fieldMappingAry as $__lmsField => $__intranetUserField)
               
               	// UserType checking
                $_recordType = $_userIdDataArray['RecordType'];
	            $_isTeaching = $_userIdDataArray['Teaching'];                           
	           
	            switch ($_recordType) {
	           		case USERTYPE_STAFF:
	             		$_userType = ($_isTeaching==1)? 'T' : 'NT';
	                    break;
	                case USERTYPE_STUDENT:
	                    $_userType = 'S';
	                    break;
	                case USERTYPE_PARENT:
	                    $_userType = 'P';
	                    break;
	                case USERTYPE_ALUMNI:
	                    $_userType = 'A';
	                    break;
	                default:
	                    $_userType = 'N/A';
	                    break;
	            }
               	
	            if ($_userType ==  $lmsUserDataAssoAry[$_userId]['UserType']) {
	               $array['userType']['Ok']++;
	            }
	            else {
	               $array['userType']['NotOk']++;
	               $array['userType']['NotOkDisplay'] =  $lmsUserDataAssoAry[$_userId]['UserType'] . "  vs  " . $_userType . "<br />";  
	                 
	            }

                                 
		  }	// end foreach ($lmsUserDataAssoAry as $_userId => $_userIdDataArray)


	### Start to show result ###        
    $array['result'] = '';
    $array['result'] .= "<table width='500px' border='1' cellpadding='0' cellspacing='0'>";
    $array['result'] .= "<tr>";
    $array['result'] .= "<td>Total Check Records:</td><td>" . $total .  "</td>" ;
    $array['result'] .= "</tr>";
    $array['result'] .= "<tr><td><br /></td></tr>";
             
    // loop each field
    foreach ($fieldMappingAry as $__lmsField => $__intranetUserField) {
    	$array['result'] .= "<tr>";
		$array['result'] .= "<td>" . $__lmsField ." OK Records:</td><td>". $array[$__lmsField]['OK'] . "</td>";
		$array['result'] .= "</tr>"; 
		
		if  ($array[$__lmsField]['NotOk'] > 0){
			$array['result'] .= "<tr>";
			$array['result'] .= "<td>"  . $__lmsField . " Not OK Records:</td><td style='" . $style . "'>" . $array[$__lmsField]['NotOk'] . "</td>";
			$array['result'] .= "</tr>";
		}
		else{
		    $array['result'] .= "<tr>";
			$array['result'] .= "<td>" . $__lmsField . " Not OK Records:</td><td>" . $array[$__lmsField]['NotOk'] . "</td>";
			$array['result'] .= "</tr>";
		}
	
		$array['result'] .= "<tr>";
		$array['result'] .= "<td>" . $__lmsField . " Not OK Records Display:</td><td>" .  $array[$__lmsField]['NotOkDisplay']. "</td>";
		$array['result'] .= "</tr>";
	    $array['result'] .= "<tr><td><br /></td></tr>";
		//$array['result'] .= $array['UserEmail'][0] . "<br />";
        } 
            
            
    // UserType
    $array['result'] .= "<tr>";
	$array['result'] .= "<td>UserType OK Records:</td><td>". $array['userType']['Ok'] . "</td>";
	$array['result'] .= "</tr>"; 
		    
	if  ($array['userType']['NotOk'] > 0){
		$array['result'] .= "<tr>";
		$array['result'] .= "<td>UserType Not OK Records:</td><td style='" . $style . "'>" . $array['userType']['NotOk'] . "</td>";
		$array['result'] .= "</tr>";
	}
	else{
		$array['result'] .= "<tr>";
		$array['result'] .= "<td>UserType Not OK Records:</td><td>" . $array['userType']['NotOk'] . "</td>";
		$array['result'] .= "</tr>";
	}
	$array['result'] .= "<tr>";
	$array['result'] .= "<td>UserType Not OK Records Display:</td><td>" .  $array['userType']['NotOkDisplay']. "</td>";
	$array['result'] .= "</tr>";
	$array['result'] .= "<tr><td><br /></td></tr>";


	//Overall result
    $array['result'] .= "<tr>";
    if($array['status'] = 'Fail'){	
	   $array['result'] .= "<td>Status:</td><td style='" . $style . "'>" . $array['status'] . "</td>"; 
	}
	else{ 
	   $array['result'] .= "<td>Status:</td><td>" . $array['status'] . "</td>";
	}
	$array['result'] .= "</tr>";
	$array['result'] .= "</table>";
		
return $array;
}

//For preseting the label format
function LMS_Insert_Label_Sample () {
	global $PATH_WRT_ROOT, $plugin, $intranet_db;
           $labelSample = array();
          
           ### Syn data to LMS - INTRANET DATA
           include_once($PATH_WRT_ROOT."includes/liblibrarymgmt.php");
           $liblms = new liblms();
           $LIBMS_USER = $liblms->db.'.LIBMS_LABEL_FORMAT';
           
	$labelSample[] = 'INSERT IGNORE INTO '.$LIBMS_USER.' (
						name,
						NX,
						NY,
						`font-size`,
						`paper-size-width`,
						`paper-size-height`,
						SpaceX,
						SpaceY,
						width,
						height,
						printing_margin_h,
						printing_margin_v,
						max_barcode_width,
						max_barcode_height,
						lMargin,
						tMargin,
						lineHeight
					) values (
						"Spine Label (8x20)",
						"8",
						"20",
						"8",
						"210",
						"297",
						"3",
						"2",
						"22",
						"12",
						"3",
						"3",
						"38",
						"12",
						"5",
						"9",
						"3"
					);';
	$labelSample[] = 'INSERT IGNORE INTO '.$LIBMS_USER.' (
						name,
						NX,
						NY,
						`font-size`,
						`paper-size-width`,
						`paper-size-height`,
						SpaceX,
						SpaceY,
						width,
						height,
						printing_margin_h,
						printing_margin_v,
						max_barcode_width,
						max_barcode_height,
						lMargin,
						tMargin,
						lineHeight
					) values (
						"Barcode Label Herma (4 x 14)",
						"4",
						"14",
						"8",
						"210",
						"297",
						"0",
						"0",
						"52.5",
						"21.2",
						"2",
						"2",
						"45",
						"12",
						"0.18",
						"0",
						"2.2"
					);';
	$labelSample[] = 'INSERT IGNORE INTO '.$LIBMS_USER.' (
						name,
						NX,
						NY,
						`font-size`,
						`paper-size-width`,
						`paper-size-height`,
						SpaceX,
						SpaceY,
						width,
						height,
						printing_margin_h,
						printing_margin_v,
						max_barcode_width,
						max_barcode_height,
						lMargin,
						tMargin,
						lineHeight
					) values (
						"Spine Label (6X12)",
						"6",
						"12",
						"12",
						"210",
						"297",
						"4",
						"0",
						"31",
						"23",
						"2",
						"2",
						"38",
						"12",
						"8",
						"9",
						"6"
					);';
	$labelSample[] = 'INSERT IGNORE INTO '.$LIBMS_USER.' (
						name,
						NX,
						NY,
						`font-size`,
						`paper-size-width`,
						`paper-size-height`,
						SpaceX,
						SpaceY,
						width,
						height,
						printing_margin_h,
						printing_margin_v,
						max_barcode_width,
						max_barcode_height,
						lMargin,
						tMargin,
						lineHeight
					) values (
						"Barcode Label (4X9)",
						"4",
						"9",
						"8",
						"210",
						"297",
						"3",
						"3",
						"50",
						"30",
						"2",
						"2",
						"38",
						"12",
						"3",
						"3",
						"3"
					);';
	
	$return_result = true;
	foreach($labelSample as $sql){
		$result = $liblms->db_db_query($sql);
		if($result <= 0)
			$return_result = false;
	}
	
	if($return_result)
		$return_result = "The label sample is preseted!<br/>";
	else
		$return_result = "The label sample is not preseted!<br/>";
		
	return $return_result;	
	
}

//$array = LMS_Insert_Result_Cross_Check();      
//echo $array['result'];

echo LMS_Insert_Label_Sample();

###################################################################
######################## Verify Data [End] ########################
###################################################################

intranet_closedb();
?>