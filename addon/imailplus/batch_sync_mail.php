<?php
// editing by 
/****************************************** Change log **********************************************
 * Created on 2011-07-06
 * Synchronize iMail Plus mails from mail server to local databases
 */
@SET_TIME_LIMIT(0);
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libwebmail.php");
include_once($PATH_WRT_ROOT."includes/imap_gamma.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include("../check.php");

if(!isset($plugin['imail_gamma']) || $plugin['imail_gamma']!=true){
	echo 'The system is not using iMail Plus, please upgrade and switch on the flag $plugin[\'imail_gamma\'] to enable it.';
	exit;
}

intranet_opendb();

$li = new libdb();
$linterface = new interface_html();

$DebugMode = false;// overwrite global var
$is_log = true; // log output to file for reference

echo "<html>\n";
echo "<head>\n";
echo "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">";
echo "</head>\n";
echo "<body>\n";

$sql = "CREATE TABLE IF NOT EXISTS TEMP_MAIL_CACHED_MAILS_STATUS(
			UserEmail varchar(255) NOT NULL,
			SyncStatus char(1) NOT NULL,
			DateInput datetime,
			PRIMARY KEY(UserEmail),
			INDEX ISyncStatus(SyncStatus)  
		) ENGINE=InnoDB DEFAULT CHARSET=utf8 ";
/* 
 * SyncStatus: C - Completed, P - Partially sync, N - Not synced yet
 */
$li->db_db_query($sql);

$sql = "SELECT 
			u.ImapUserEmail as Email,u.UserPassword as Password,t.SyncStatus 
		FROM INTRANET_USER as u 
		LEFT JOIN TEMP_MAIL_CACHED_MAILS_STATUS as t ON t.UserEmail = u.ImapUserEmail  
		WHERE u.RecordStatus = 1 
			AND u.ImapUserEmail IS NOT NULL 
			AND u.ImapUserEmail <> '' 
		UNION 
		(SELECT 
			s.MailBoxName as Email,s.MailBoxPassword as Password,t.SyncStatus 
		FROM MAIL_SHARED_MAILBOX as s 
		LEFT JOIN TEMP_MAIL_CACHED_MAILS_STATUS as t ON t.UserEmail = s.MailBoxName )";
/*
$sql = "SELECT 
			u.ImapUserEmail as Email,u.UserPassword as Password  
		FROM INTRANET_USER as u 
		WHERE u.RecordStatus = 1 
			AND u.ImapUserEmail IS NOT NULL 
			AND u.ImapUserEmail <> '' 
		UNION 
		(SELECT 
			s.MailBoxName as Email,s.MailBoxPassword as Password  
		FROM MAIL_SHARED_MAILBOX as s)";
*/
$user_data = $li->returnArray($sql);

$numOfUser = count($user_data);
$numOfCompleted = 0;
//$numOfPartialSync = 0;
$numOfNotSync = 0;
$completedSyncUser = array();
//$partialSyncUser = array();
$notSyncUser = array();
$newCompletedSyncUser = array();
$failSyncUser = array();
for($i=0;$i<$numOfUser;$i++){
	//list($uid,$email,$password,$status) = $user_data[$i];
	if($user_data[$i]['SyncStatus'] == 'C'){
		$completedSyncUser[] = $user_data[$i];
	}
	//else if($user_data[$i]['SyncStatus'] == 'P'){
	//	$partialSyncUser[] = $user_data[$i];
	//}
	else{
		$notSyncUser[] = $user_data[$i];
	}
}



$x  =  "<div style='border:1px solid blue'>\n";
$x .=  "<b>Lastest mail synchronization status summary:</b><br>\n";
$x .=  "Total no. of active users (that have mail account): $numOfUser<br>\n";
$x .=  "No. of users completed synchronized: ".sizeof($completedSyncUser)."<br>\n";
//$x .=  "No. of users partially synchronized: ".sizeof($partialSyncUser)."<br>";
$x .=  "No. of users not sychronized: ".sizeof($notSyncUser)."<br>\n";
$x .=  "</div>\n";

echo $x;

if(isset($submit1) && $SYS_CONFIG['Mail']['CacheMail']==1)
{
$start_time = time();

if($is_log){
	$log_time = date("YmdHis", time());
	$log_path = $file_path."/file/gamma_mail_sync_log";
	if(!file_exists($log_path) || !is_dir($log_path)){
		mkdir($log_path,0777);
	}
	$log_file = $log_path."/"."mail_sync_log_".$log_time.".txt";
	$fLog = fopen($log_file,"a+");
	fwrite($fLog,$x);
	fclose($fLog);
}

$x = "Starting synchronization process......<br><br>\n\n";
$x.= sprintf("Current PHP process ID is %d<br><br>\n\n",getmypid());

echo $x;
if($is_log && $log_file != ''){
	$fLog = fopen($log_file,"a+");
	fwrite($fLog,$x);
	fclose($fLog);
}
ob_flush();
flush();

for($i=0;$i<count($notSyncUser);$i++)
{
	list($email,$password,$status) = $notSyncUser[$i];
	//if($email == 'marcusleung@broadlearning.com' /* || $email == 'carlos_s1@broadlearning.com' */) continue;
	$x = "Synchronizing user <b>".htmlspecialchars($email)."</b>......";
	echo $x;
	if($is_log && $log_file != ''){
		$fLog = fopen($log_file,"a+");
		fwrite($fLog,$x);
		fclose($fLog);
	}
	ob_flush();
	flush();
	
	$_SESSION['SSV_EMAIL_LOGIN'] = $email;
	$_SESSION['SSV_EMAIL_PASSWORD'] = $password;
	$_SESSION['SSV_LOGIN_EMAIL'] = $email;
	$IMap = new imap_gamma();
	if($IMap->ConnectionStatus != true)
	{ 
		$failSyncUser[] = $notSyncUser[$i];
		$x = "[Connection failure]<br><br>\n";
		echo $x;
		if($is_log && $log_file != ''){
			$fLog = fopen($log_file,"a+");
			fwrite($fLog,$x);
			fclose($fLog);
		}
		ob_flush();
		flush();
		continue;
	}
	/*
	// debug section
	$IMap->getImapCacheAgent();
	$FolderList = $IMap->getAllFolderList();
	debug_pr($FolderList);
	for($j=0;$j<sizeof($FolderList);$j++){
		debug_pr($FolderList[$j]);
		//$last_uid = $IMap->Get_Last_Msg_UID($FolderList[$i]);
		if($IMap->Go_To_Folder($FolderList[$j])){
			debug_pr("Can change folder to ".$FolderList[$j]);
			if($IMap->IMapCache->Go_To_Folder($FolderList[$j])){
				debug_pr("Socket change folder to ".$FolderList[$j]);
			}else{
				debug_pr("Socket fail change folder to ".$FolderList[$j]);
			}
		}else{
			debug_pr("Fail to change folder to ".$FolderList[$j]);
		}
		//debug_pr($last_uid);
	}
	
	// end debug section
	*/
	
	
	$IMap->Start_Timer();
	$sql = "DELETE FROM MAIL_CACHED_MAILS WHERE UserEmail = '".$IMap->CurUserAddress."' ";
	$li->db_db_query($sql);
	$FolderList = $IMap->getAllFolderList();
	//$SkipFolders = array();
	//$SkipFolders[] = $IMap->reportSpamFolder;
	//$SkipFolders[] = $IMap->reportNonSpamFolder;
	//$SkipFolders[] = $IMap->HiddenFolder;
	//$Folder = array();
	//for($j=0;$j<sizeof($FolderList);$j++){
	//	if(in_array($FolderList[$j],$SkipFolders)) continue;
	//	$Folder[] = $FolderList[$j];
	//}
	
	$Result = $IMap->Check_New_Mails_For_Cached_Mails($FolderList);
	$MailsToAdd = array();
	$SyncResult = true;
	if(sizeof($Result)>0){
		foreach($Result as $folder => $uid_arr){
			$server_max_uid = $uid_arr['ServerMaxUID'];
			$client_max_uid = !isset($uid_arr['ClientMaxUID'])?0:$uid_arr['ClientMaxUID'];
			if($server_max_uid > $client_max_uid){
				// sync new mails to db
				$MailsToAdd[$folder] = array('StartUID'=>$client_max_uid,'EndUID'=>$server_max_uid);
			}
		}
	}
	
	// this process may takes long time ...
	if(sizeof($MailsToAdd)>0){
		$SyncResult = $IMap->Add_Cached_Mails($MailsToAdd,'.');
	}
	
	$newCompletedSyncUser[] = $notSyncUser[$i];
	$sql = "INSERT INTO TEMP_MAIL_CACHED_MAILS_STATUS (UserEmail,SyncStatus,DateInput) VALUES ('$email','C',NOW())";
	$li->db_db_query($sql);
	
	$single_process_time = $IMap->Stop_Timer();
	
	$x = "[Completed][".$single_process_time." sec]<br>\n";
	$x.= "No. of new completed/Total no. of ready sync user: <b>[".sizeof($newCompletedSyncUser)."/".sizeof($notSyncUser)."]</b><br>\n";
	$x.= "Overall status(No. of completed/Total no. of user): <b>[".(sizeof($completedSyncUser)+sizeof($newCompletedSyncUser))."/".$numOfUser."]</b><br>\n";
	$x.= "<br>\n\n";
	echo $x;
	if($is_log && $log_file != ''){
		$fLog = fopen($log_file,"a+");
		fwrite($fLog,$x);
		fclose($fLog);
	}
	ob_flush();
	flush();
}

$x = "<br>End of synchronization.<br>\n";

$end_time = time();
$x.= sprintf("Overall process time is %d seconds.<br>\n",($end_time - $start_time));

$x.= "<div style='border:1px solid blue'>\n";
$x.= "<b>Mail synchronization result summary:</b><br>\n";
$x.= "Total no. of active users (that have mail account): $numOfUser<br>\n";
$x.= "No. of users completed synchronized: ".(sizeof($completedSyncUser)+sizeof($newCompletedSyncUser))."<br>\n";
$x.= "No. of users failed to synchronize: ".sizeof($failSyncUser)."<br>\n";
$x.= "No. of users still not synchronized: ".($numOfUser - sizeof($completedSyncUser) - sizeof($newCompletedSyncUser))."<br>\n";
$x.= "</div>\n";
if($is_log){
	$x .= "Output is logged to file $log_file <br>\n";
}

echo $x;
ob_flush();
flush();
if($is_log && $log_file != ''){
	$fLog = fopen($log_file,"a+");
	fwrite($fLog,$x);
	fclose($fLog);
}

}else 
{

echo "<form id=\"form1\" name=\"form1\" action=\"batch_sync_mail.php\" method=\"GET\">\n";
echo "This page will synchronize all users mails from mail server to local database. <br>";
echo "Please caution this process may take very long time.<br>";
if(!isset($SYS_CONFIG['Mail']['CacheMail']) || $SYS_CONFIG['Mail']['CacheMail'] != true){
	echo 'Please switch on flag $SYS_CONFIG[\'Mail\'][\'CacheMail\'] at /plugins/gamma_mail_conf.php to continue.<br>';
}else{
	echo "Press the [Start] button to start synchronization.<br>";
	echo $linterface->GET_ACTION_BTN("Start", "submit", "", "submit1", $ParOtherAttribute="", $ParDisabled=0, $ParClass="");
}
echo "</form>\n";

}

echo "</body>";
echo "</html>";
intranet_closedb();
?>