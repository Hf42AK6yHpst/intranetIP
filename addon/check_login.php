<?php
/*
 * 	Purpose: check if given UserLogin and UserPassword match that of system
 * 
 * 	Log:
 * 	
 */
 
 @session_start();
 
 function validateAdminLogin($UserLogin, $UserPassword) {
	$ret = false;
	if (strtolower($UserLogin)=="broadlearning" ) {
  		# get the password from central server
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_REFERER, $_SERVER["SERVER_NAME"]);
		@curl_setopt($ch, CURLOPT_URL, "http://eclassupdate.broadlearning.com/api/blpswd.php?ptype=3");
		
		// grab URL and pass it to the browser
		$password_central = trim(curl_exec($ch));

		if ($password_central!="" && strlen($password_central)>3)
		{
			if (md5("2012allschools".$UserPassword)==$password_central)
			{
				$_SESSION['BE_AdminLogin'] = $UserLogin;
				$_SESSION['BE_LoginTime'] = time();
				$ret = true;
			}
		}  		
  	}
	return $ret;	
}	// end function validateAdminLogin
 
$UserLogin 		= $_POST["UserLogin"];
$UserPassword 	= $_POST["UserPassword"];
$DirectLink 	= $_POST["DirectLink"];

if (($UserLogin !="") && ($UserPassword!="")) {
	$pass = validateAdminLogin($UserLogin,$UserPassword);	
}

if ($pass) {
//	print "<br>sec url in check_login->".$DirectLink."<br>";
	header("Location: " . $DirectLink);
}
else {
	header("Location: " . "login.php?err=1");
}
 
?>