<?php
/*
 * 	using:
 * 	purpose:	show login interface for admin user
 * 	Log:
 * 	
 */

@session_start();

if(isset($_SESSION['BE_REDIRECT_URL']) && $_SESSION['BE_REDIRECT_URL'] != ''){
	$DirectLink = $_SESSION['BE_REDIRECT_URL'];
}

?>
<style type="text/css">
<!--
html {}
body { margin:0; padding:0; min-height:600px; background: fixed center;  font-family: Verdana; font-size:12px;}
#wrapper { display:block; width:670px; margin:0 auto;}
.main_container {position:absolute; top:0; height:320px; left: 10; margin-top:10px; width:670px;/* negative half of the height */}
.error_msg {color:#FF0000;}
-->
</style>

<script language="JavaScript" src="/templates/jquery/jquery-1.4.4.min.js"></script>
<script language="javascript">
function checkLoginForm(){
	$( "#login_btn" ).attr( "disabled", true );
	var obj = document.form1;
	var pass=1;
	if ($.trim($("#UserLogin").val()) == '') {
		alert("Please input user login!");
		$("#UserLogin").val('');
		$("#UserLogin").focus();
		pass  = 0;
	}
	else if($.trim($("#UserPassword").val()) == '') {
		alert("Please input password!");
		$("#UserPassword").val('');
		$("#UserPassword").focus();
		pass  = 0;
	} 
	
	if(pass)	
	{
		return true;
	}
	$( "#login_btn" ).attr( "disabled", false );
	return false;
}

</script>    
</head>

<body>
<form name="form1" action="check_login.php" method="post" onSubmit="return checkLoginForm();">

<div id="wrapper">
	<div class="main_container">		
     	<div class="login_page">     		     		  
        	<div class="login_form">
        		<table border=0>
        			<tr>
        				<td>Login ID</td>
        				<td><input class="" type="text" name="UserLogin" id="UserLogin"/></td>
        			</tr>
        			<tr>
        				<td>Password</td>
        				<td><input class="" type="password" name="UserPassword" id="UserPassword"/></td>
        			</tr>
        			<tr>
        				<td colspan="2" align="right"><input name="submit" type="submit" class="" id="login_btn" value="Login" /></td>
        			</tr>
        		</table>
	           	<?php if($err==1) { ?><div class="error_msg">Invalid LoginID/Password.</div><?php } ?>
		   </div>
		</div>
	</div>
</div>

<input type="hidden" name="url" value="/addon/login.php?err=1&DirectLink=<?=rawurlencode($DirectLink)?>">
<input type="hidden" name="DirectLink" value="<?=$DirectLink?>">
</form>
</body>

<SCRIPT language=javascript>
	document.getElementById('UserLogin').focus();
</script>

</html>

