<?
# using:  kenneth chung
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_opendb();

# $TestOnly = 1;		# Will display the form and disabled the submit button

# load history file
$history_file = $PATH_WRT_ROOT."file/ip20_to_ip25_script_update_history.txt";
$history_content = trim(get_file_content($history_file));
if ($history_content!="")
{
	# find if schema should be updated
	$tmp_arr = split("\n", $history_content);
	list($last_schema_date, $last_update, $last_update_ip) = split(",", $tmp_arr[sizeof($tmp_arr)-1]);
	
	if ($sql_eClassIP_update[sizeof($sql_eClassIP_update)-1][0]>$last_schema_date)
	{
		$is_new_available = true;
	}
	$new_update = ($is_new_available) ? "Yes" : "No";
} 
else
{
	$new_update = "-";
	$last_update = "-";
	$last_update_ip = "-";
}



?>


<html>
	<head>
		<meta http-equiv='pragma' content='no-cache' />
		<meta http-equiv='content-type' content='text/html; charset=utf-8' />
		<title>IP20 to IP25 Setup Page</title>
	</head>
	
	<body>
	<script language="javascript">
	<!--
	function check_form()
	{
		with(document.form1)
		{
			if(AcademicYearInfo.value == "")
			{
				alert("Please enter School Year Info.");
				AcademicYearInfo.focus();
				return false;
			}
			
			if(AcademicYearTermInfo.value == "")
			{
				alert("Please enter Semester Info.");
				AcademicYearTermInfo.focus();
				return false;
			}
			
			if(!confirm("Are you sure all data in eClass and Intranet databases has been converted to UTF-8 encoding already?"))
			{
				return false;
			}
			if(!confirm("Are you sure you want to continue the setup?"))
			{
				return false;
			}
			
			return true;
		}	
	}
	//-->
	</script>
	
	<font color=red size=+1>[Please Make DB Backup First]</font>
	<br>
	<font color=red size=+1>[Please make sure all data in eClass and Intranet databases has been converted to UTF-8 encoding already first]</font>
	<br>
	<br>
	This page will update the eClass IP 2.0 data compatible with eClass IP 2.5<br />
	Updates included in this script:<br />
	1. Setup an academic year and the corresponding terms<br />
	2. Copy data of forms, classes, class teachers and class students<br />
	3. Update data of the following modules:<br />
		<ul>
			<li>eAttendance (Student)</li>
			<li>eCircular</li>
			<li>eHomework</li>
			<li>eInventory</li>
			<li>eNotice</li>
			<li>ePayment</li>
			<li>Campus Link</li>
			<li>System Settings > Group</li>
			<li>System Settings > Location</li>
			<li>Preset Wordings == Convert the file content to utf-8</li>
			<li>Campus TV schedule == Convert the file content to utf-8</li>
			<li>School Calendar > Holiday & Event</li>
			<li>School Name/Infp == Convert the file content to utf-8</li>
			<li>iCalendar == Alter table to suit for new requirement</li>
			<li>iCalendar == Create Calendar for already created group and course</li>
			<li>iCalendar == Transfer course event to iCalendar table</li>
			<li>eEnrolment: Copy the settings from txt file to database</li>
			<li>eAttendance: Copy the settings from txt file to database</li>
		</ul>
	
	<br />
	
	<? if ($last_update != '-' && !$TestOnly) { ?>
		Last run: <font color="blue"><b><?=$last_update?></b></font> by <font color="blue"><b><?=$last_update_ip?></b></font> <font color="red">[This script can be run ONCE only]</font>
	<? } else { 
		# School Year Setting in admin console
		$o_academic_yr = get_file_content("$intranet_root/file/academic_yr.txt");
		# Year data in eDiscipline
		/*
		if($plugin['Disciplinev12'])
		{
			$li = new libdb();
			$tempSql = "select distinct(Year) from DISCIPLINE_MERIT_RECORD";
			$tempRs = $li->returnArray($tempSql);
		}
		debug_pr($tempRs);
		*/
		$li = new libdb();
		# PROFILE_STUDENT_ACTIVITY
		$tempSql = "select distinct(Year) from PROFILE_STUDENT_ACTIVITY";
		$tempRs1 = $li->returnArray($tempSql);
		
		# PROFILE_STUDENT_ATTENDANCE
		$tempSql = "select distinct(Year) from PROFILE_STUDENT_ATTENDANCE";
		$tempRs2 = $li->returnArray($tempSql);

		# PROFILE_STUDENT_AWARD
		$tempSql = "select distinct(Year) from PROFILE_STUDENT_AWARD";
		$tempRs3 = $li->returnArray($tempSql);
		
		# PROFILE_STUDENT_MERIT
		$tempSql = "select distinct(Year) from PROFILE_STUDENT_MERIT";
		$tempRs4 = $li->returnArray($tempSql);
		
		# PROFILE_STUDENT_MERIT
		$tempSql = "select distinct(Year) from PROFILE_STUDENT_MERIT";
		$tempRs5 = $li->returnArray($tempSql);
		
		$tempRsTmp = array_merge((array)$tempRs1, (array)$tempRs2, (array)$tempRs3, (array)$tempRs4, (array)$tempRs5);
		$tempRs = array();
		foreach($tempRsTmp as $k=>$d)
		{
			if(!in_array($d['0'], $tempRs) && $d['0'])
				$tempRs[] = $d['0'];
		}
		
		# Semester setting in admin console
		$o_sem = "";
		$semester_data = split("\n",get_file_content("$intranet_root/file/semester.txt"));
		for($i=0;$i<sizeof($semester_data);$i++)
		{
			list($SemName, $CurSem, $StartDate, $EndDate) = split("::", $semester_data[$i]);
			$o_sem .= $SemName;
			if($StartDate)
			{
				$o_sem .= ",".$StartDate.",".$EndDate;
			}
			$o_sem .= "<br />";
		}
	?>
	
		<form name="form1" method="post" action="ip20_to_ip25_confirm.php" onSubmit="return check_form();">
			<b>School Year Info:</b><br>
			<b>Please enter the school year info of the <font color="red">CURRENT</font> data in the eClass IP 2.0 System</b><br />
			Format: English Title, Chinese Title (use a "Comma", without space in between two records, to separate records)<br>
			<font color="red">* The comma should be in english format, i.e. use "," instead of "，"</font><br />
			<font color="red">* There should be no "," in the titles</font><br />
			
			<? if($o_academic_yr || $tempRs) { ?>
				<font color="#777777"><u>Client original setting (reference only):</u> <br /> 
				<? if($o_academic_yr) { ?>
					In admin console setting: <br />
					<?=$o_academic_yr?><br />
				<? } ?>
				<? if($tempRs) {?>
					In Student Profile Record: <br />
					<? foreach($tempRs as $k=>$d)
					{
						echo $d ."<br />";
					}
					?>
				<? } ?>
				</font>
			<? } ?>
			Example: <br>
			<font color="blue">
			2008-2009,二零零八至二零零九
			</font>
			<br />
			
			<input name="AcademicYearInfo" id="AcademicYearInfo" size="40" value="" /><br>
			
			<br><b>Semester Info:</b><br>
			Format: English Title, Chinese Title, Start Date, End Date (use a "Comma", without space in between two records, to separate and a "Enter" for entering next record)<br>
			<font color="red">* The comma should be in english format, i.e. use "," instead of "，"</font><br />
			<font color="red">* There should be no "," in the titles</font><br />
			
			<? if($o_sem) { ?>
				<font color="#777777"><u>Client original setting (reference only):</u> <br /> 
				In admin console setting: <br />
				<?=$o_sem?><br />
				</font>
			<? } ?>
			Example: <br>
			<font color="blue">
			1st Term,上學期,2008-09-01,2009-01-31<br>
			2nd Term,下學期,2009-02-01,2009-08-31<br>
			</font>
			<textarea name="AcademicYearTermInfo" id="AcademicYearTermInfo" cols="150" rows="8"></textarea><br>

			<input type="submit" value="Submit" <?=($TestOnly?"Disabled":"")?>>
		</form>
	
	<? } ?>
	
	


	</body>
</html>



<?
intranet_closedb();

?>