<?
# using: 
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/lib.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_opendb();

# Get Data
$AcademicYearInfo = stripslashes($_POST['AcademicYearInfo']);
$AcademicYearTermInfo = stripslashes(nl2br($_POST['AcademicYearTermInfo']));

# Get Year Info
$AcademicYearInfoArr = explode(',' ,$AcademicYearInfo);
$AcademicYearEn = $AcademicYearInfoArr[0];
$AcademicYearCh = $AcademicYearInfoArr[1];

# Get Term Info
$AcademicYearTermInfoArr = explode('<br />' ,$AcademicYearTermInfo);
$numOfInputYearTerm = count($AcademicYearTermInfoArr);
$YearTermArr = array();
for ($i=0; $i<$numOfInputYearTerm; $i++)
{
	$thisYearTermInfoText = $AcademicYearTermInfoArr[$i];
	
	if (trim($thisYearTermInfoText) == '')
		continue;
	
	$thisYearTermInfoArr = explode(',', $thisYearTermInfoText);
	$YearTermArr[$i]['YearTermEn'] = trim($thisYearTermInfoArr[0]);
	$YearTermArr[$i]['YearTermCh'] = $thisYearTermInfoArr[1];
	$YearTermArr[$i]['StartDate'] = $thisYearTermInfoArr[2];
	$YearTermArr[$i]['EndDate'] = $thisYearTermInfoArr[3];
}

# Build Term table Row
$numOfYearTerm = count($YearTermArr);
$termTableRow = '';
for ($i=0; $i<$numOfYearTerm; $i++)
{
	$termTableRow .= '<tr>';
		$termTableRow .= '<td>'.$YearTermArr[$i]['YearTermEn'].'</td>';
		$termTableRow .= '<td>'.$YearTermArr[$i]['YearTermCh'].'</td>';
		$termTableRow .= '<td>'.$YearTermArr[$i]['StartDate'].'</td>';
		$termTableRow .= '<td>'.$YearTermArr[$i]['EndDate'].'</td>';
	$termTableRow .= '</tr>';
}

?>


<html>
	<head>
		<meta http-equiv='pragma' content='no-cache' />
		<meta http-equiv='content-type' content='text/html; charset=utf-8' />
		<title>IP20 to IP25 Setup Page Confimation Page</title>
	</head>
	
	<body>
	<script language="javascript">
	<!--
	function check_form()
	{
		with(document.form1)
		{
			if(!confirm("The system setup is not reversible. Are you sure the displayed information is correct and proceed to the setup?"))
			{
				return false;
			}
			
			return true;
		}	
	}
	//-->
	</script>
	
	
	<br />
	
	<font color=red size=+1>[Please Make DB Backup First]</font><br /><br />
	<font size='+1'>Since the setup can be executed <font color="red">ONCE</font> only, please confirm the submitted data as follows:</font>
	<br />
	<br />
	
	<b>School Year Info</b><br />
	<table border="1">
		<tr>
			<td><b>English Title</b></td>
			<td><b>Chinese Title</b></td>
		</tr>
		<tr>
			<td><?=$AcademicYearEn?></td>
			<td><?=$AcademicYearCh?></td>
		</tr>
	</table>
	<br />
	<br />
	
	<b>Term Info</b><br />
	<table border="1">
		<tr>
			<td><b>English Title</b></td>
			<td><b>Chinese Title</b></td>
			<td><b>Start Date</b></td>
			<td><b>End Date</b></td>
		</tr>
		<?=$termTableRow?>
	</table>
	
	<form name="form1" method="post" action="ip20_to_ip25_script_update.php" onSubmit="return check_form();">
		<input type="submit" value="Confirm">
		
		<input type="hidden" id="AcademicYearEn" name="AcademicYearEn" value="<?=intranet_htmlspecialchars($AcademicYearEn)?>" />
		<input type="hidden" id="AcademicYearCh" name="AcademicYearCh" value="<?=intranet_htmlspecialchars($AcademicYearCh)?>" />
		
		<input type="hidden" id="YearTermArr" name="YearTermArr" value="<?=rawurlencode(serialize($YearTermArr));?>">
	</form>
	


	</body>
</html>



<?
intranet_closedb();

?>