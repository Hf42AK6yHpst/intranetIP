<?php
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT.'includes/global.php');
include_once($PATH_WRT_ROOT.'includes/icalendar.php');
include_once($PATH_WRT_ROOT.'includes/libeclass.php'); 
intranet_opendb();
	$iCal = new icalendar();
	$libeclass = new libeclass();
	# Alter table
	
	$sql ="alter table CALENDAR_REMINDER Add Column CalType tinyint(1)";
	$sqlResult["Alter_table_1"] = $iCal->db_db_query($sql);
	
	$sql ="alter table {$eclass_db}.course Add Column CalID int(8)";
	$sqlResult["Alter_table_2"] = $iCal->db_db_query($sql);
	
	$sql ="alter table CALENDAR_EVENT_PERSONAL_NOTE Drop Key EventID";
	$sqlResult["Alter_table_3"] = $iCal->db_db_query($sql);
	
	$sql ="alter table CALENDAR_EVENT_PERSONAL_NOTE Drop Key UserID";
	$sqlResult["Alter_table_4"] = $iCal->db_db_query($sql);
	
	$sql ="alter table CALENDAR_EVENT_PERSONAL_NOTE Add Column PersonalNoteID int auto_increment primary key";
	$sqlResult["Alter_table_5"] = $iCal->db_db_query($sql);
	
	# Insert Group Calendar
	$sql = "Select * from INTRANET_GROUP where CalID is NULL";
	$result = $iCal->returnArray($sql);
	$subsql = "";
	$eclassDb = $eclass_db;
	
	if (count($result)>0){
		foreach ($result as $r){
			$calID = $iCal->createSystemCalendar($r["Title"], 2, 'P',$r['Description']);
			$sqlResult["Create_ical_$calID"] = $calID;
			$sql = "update INTRANET_GROUP set CalID = $calID 
					where GroupID = ".$r["GroupID"];
			$sqlResult["combine_ical_$calID"] = $iCal->db_db_query($sql);
			$sql = "insert into CALENDAR_CALENDAR_VIEWER 
					(CalID,UserID,Access,Color,GroupID,GroupType) 
					select '$calID', UserID,
					Case When RecordType = 'A' and 
					(AdminAccessRight is NULL OR AdminAccessRight & 16 > 0)
					Then 'W'
					Else 'R'
					End,
					'2f75e9',
					'".$r["GroupID"]."',
					'E'
					From INTRANET_USERGROUP
					where GroupID = ".$r["GroupID"];
			$subsql = $r["GroupID"].",";
			$sqlResult["insert_group_viewer_$calID"] = $iCal->db_db_query($sql);
		}
		$subsql=rtrim($subsql,",");
		#class group
		$sql = "insert into CALENDAR_CALENDAR_VIEWER 
				(CalID,UserID,Access,Color,GroupID,GroupType) 
				select '$calID', t.UserID,
				'W','2f75e9',c.GroupID,'T'
				From
				YEAR_CLASS as c inner join YEAR_CLASS_TEACHER as t on
				c.YearClassID = t.YearClassID
				where c.GroupID in ($subsql)";
	
		$sqlResult["insert_viewer_teacher"] = $iCal->db_db_query($sql);
	}
	
	#insert class calendar
	$sql ="Select course_id, course_name, course_desc from {$eclassDb}.course where 
		CalID is null and RoomType=0"; 
	$result = $iCal->returnArray($sql);
	if (count($result)>0){
		foreach ($result as $r){
			$calID = $iCal->createSystemCalendar($r["course_name"],3,"P",$r["course_desc"]);
			$sqlResult["insert_class_Cal_$calID"] = $calID;
			$dbName = $libeclass->db_prefix."c".$r["course_id"];
			
			$sql2 = "update {$eclass_db}.course set CalID = $calID 
				where course_id = ".$r["course_id"];
			$sqlResult["update_course_$calID"] = $iCal->db_db_query($sql2); 
			// $sql = "SELECT SCHEMA_NAME FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME = '$dbName'";
			// $existDB = $iCal->db_db_query($sql);
			$sql = "SELECT table_name FROM information_schema.tables WHERE table_schema = '$dbName' AND table_name = 'usermaster'";
			$existDB = $iCal->returnArray($sql);
			if (!empty($existDB)){
			$sql = "
				insert into CALENDAR_CALENDAR_VIEWER 
				(CalID,UserID,Access,Color,GroupID,GroupType)
				select '$calID', i.UserID, 'W', '2f75e9','".$r["course_id"]."','C'
				From {$dbName}.usermaster as u inner join INTRANET_USER as i on
				u.user_email = i.UserEmail
				where u.status is null
				";
				$sqlResult["insert_course_viewer_$calID"] = $iCal->db_db_query($sql);
			}
		}
	}
	#check the event creator when he has no default calendar
	$sql = "select distinct u.UserID
		from {$eclass_db}.event as e 
			inner join {$eclass_db}.course as c on
				e.course_id = c.course_id 
			inner join {$eclass_db}.user_course as uc on
				c.course_id = uc.course_id and
				e.user_id = uc.user_id
			inner join INTRANET_USER as u on
				uc.user_email = u.UserEmail
		where u.UserID not in (select ca.Owner from CALENDAR_CALENDAR as ca inner join 
		CALENDAR_CALENDAR_VIEWER as v on ca.CalID = v.CalID where v.Access='A')
		";
	$eventCreator = $iCal->returnVector($sql);
	if (count($eventCreator)>0){
		foreach($eventCreator as $user){ 
			//$UserID = $user;
			$iCal->insertMyCalendar();
		} 
	}
	#insert original eclass event
	$sql = "select e.title,e.notes,
			e.share_type,
			c.CalID,
			e.is_important,e.is_allday,e.inputdate,
			e.durMi,e.durHr,e.share_type,e.eventdate,
			e.last_modified, e.link, u.UserID
			from {$eclass_db}.event as e 
			inner join {$eclass_db}.course as c on
				e.course_id = c.course_id 
			inner join {$eclass_db}.user_course as uc on
				c.course_id = uc.course_id and
				e.user_id = uc.user_id
			inner join INTRANET_USER as u on
				uc.user_email = u.UserEmail
			";
	$result = $iCal->returnArray($sql);
	
	if (count($result) > 0){
		$calIDset ="";	
		$sql = "insert into CALENDAR_EVENT_ENTRY 
		(UserID,CalID,EventDate,InputDate,ModifiedDate,Duration,IsImportant,IsAllDay
		,Access,Title,Description,Url) values ";
		/*$toEcho = true;
		foreach($result as $r){
			$calID = $r["CalID"];
			if ($r['share_type']==0){
				//$UserID = $r["UserID"];
				$calID = $iCal->returnOwnerDefaultCalendar();
			}
			$duration = $r["is_allday"]?1440:$r["durMi"]+$r["durHr"]*60;
			$sql .="('".$r["user_id"]."','".$calID."','".$r["eventdate"]."',
					'".$r["inputdate"]."','".$r["last_modified"]."','$duration',
					'".$r["is_important"]."','".$r["is_allday"]."','P',
					'".addslashes($r["title"])."','".addslashes($r["notes"])."','".$r["link"]."'
					), ";
			$calIDset .= $calID.",";
			if ($toEcho)
			{
				debug($sql);
				$toEcho = false;
			}
		}	
		$sql = rtrim($sql,", ");

		$sqlResult["insert_Event"] =$iCal->db_db_query($sql);
		//debug($sqlResult["insert_Event"]); 
		$calIDset = rtrim($calIDset,", ");*/
		$calIDset =Array();
		$hsql = "insert into CALENDAR_EVENT_ENTRY 
		(UserID,CalID,EventDate,InputDate,ModifiedDate,Duration,IsImportant,IsAllDay
		,Access,Title,Description,Url) values ";
		$i = 0;
		foreach($result as $r){
			$calID = $r["CalID"];
			if ($r['share_type']==0){
				//$UserID = $r["UserID"];
				$calID = $iCal->returnOwnerDefaultCalendar();
			}
			$duration = $r["is_allday"]?1440:$r["durMi"]+$r["durHr"]*60;
			$sql = $hsql."('".$r["UserID"]."','".$calID."','".$r["eventdate"]."',
					'".$r["inputdate"]."','".$r["last_modified"]."','$duration',
					'".$r["is_important"]."','".$r["is_allday"]."','P',
					'".addslashes($r["title"])."','".addslashes($r["notes"])."','".$r["link"]."'
					)";
			$sqlResult["insert_Event_".$i] = $iCal->db_db_query($sql);			
			$calIDset[] = $calID;
			if ($calID == '' || empty($calID)){
				echo $sql;
				debug_r($r);
			}
			$i++;
		}
		$calIDset = array_unique($calIDset);
		$calIDset = implode("','",$calIDset);
	$sql = "update CALENDAR_EVENT_ENTRY set 
			UID = concat(UserID,'-',CalID,'-',EventID,'@eclass')
			where CalID in ('$calIDset') and UID is NULL;
			";
	$sqlResult["update_Event"] =$iCal->db_db_query($sql);
	}

intranet_closedb();

?>