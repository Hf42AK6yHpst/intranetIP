<?php
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT.'includes/global.php');
include_once($PATH_WRT_ROOT.'includes/icalendar.php');
intranet_opendb();
	$iCal = new icalendar();
# Insert Group Calendar
	$sql = "Select * from INTRANET_GROUP where CalID is NULL";
	$result = $iCal->returnArray($sql);
	$subsql = "";
	$eclassDb = $eclass_db;
	
	if (count($result)>0){
		foreach ($result as $r){
			$calID = $iCal->createSystemCalendar($r["Title"], 2, 'P',$r['Description']);
			$sqlResult["Create_ical_$calID"] = $calID;
			$sql = "update INTRANET_GROUP set CalID = $calID 
					where GroupID = ".$r["GroupID"];
			$sqlResult["combine_ical_$calID"] = $iCal->db_db_query($sql);
			$sql = "insert into CALENDAR_CALENDAR_VIEWER 
					(CalID,UserID,Access,Color,GroupID,GroupType) 
					select '$calID', UserID,
					Case When RecordType = 'A' and 
					(AdminAccessRight is NULL OR AdminAccessRight & 16 > 0)
					Then 'W'
					Else 'R'
					End,
					'2f75e9',
					'".$r["GroupID"]."',
					'E'
					From INTRANET_USERGROUP
					where GroupID = ".$r["GroupID"];
			$subsql = $r["GroupID"].",";
			$sqlResult["insert_group_viewer_$calID"] = $iCal->db_db_query($sql);
		}
		$subsql=rtrim($subsql,",");
		#class group
		$sql = "insert into CALENDAR_CALENDAR_VIEWER 
				(CalID,UserID,Access,Color,GroupID,GroupType) 
				select '$calID', t.UserID,
				'W','2f75e9',c.GroupID,'T'
				From
				YEAR_CLASS as c inner join YEAR_CLASS_TEACHER as t on
				c.YearClassID = t.YearClassID
				where c.GroupID in ($subsql)";
	
		$sqlResult["insert_viewer_teacher"] = $iCal->db_db_query($sql);
	}
	debug_r($sqlResult);
intranet_closedb();
?>