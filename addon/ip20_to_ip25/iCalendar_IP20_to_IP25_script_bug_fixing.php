<?php
 # This script is only for running the wrong icalendar script;
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/lib.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT.'includes/icalendar.php');
include_once($PATH_WRT_ROOT.'includes/libeclass.php');
intranet_opendb();
$iCal = new icalendar();
$libeclass = new libeclass();
$iCal->Start_Trans();
## bug of admin right in group
$sql = "Select CalID, GroupID from INTRANET_GROUP";
$result = $iCal->returnArray($sql);
$subsql = "";
if (count($result)>0){
	foreach ($result as $r){
		$sql = "update CALENDAR_CALENDAR_VIEWER set Access = 'R'
				where 
				GroupType = 'E' AND
				CalID = ".$r['CalID']." AND
				UserID in (
					select UserID from INTRANET_USERGROUP where
					RecordType = 'A' and 
					(AdminAccessRight is not NULL and AdminAccessRight & 16 = 0) and
					GroupID = ".$r["GroupID"]."
				)
			";
		$sqlResult["update_group_viewer_$calID"] = $iCal->db_db_query($sql);
		$sqlResult["update_group_viewer_{$calID}_sql"] =$sql;
	}
 }

  
## eclass CalID missing bug
$sql = "select distinct GroupID, CalID from CALENDAR_CALENDAR_VIEWER where GroupType = 'C' and CalID <> 0";
$resultSet = $iCal->returnArray($sql);
if (count($resultSet)>0){ # link the course with calendar
	foreach($resultSet as $result){
		$sql = "update {$eclass_db}.course 
				set CalID = ".$result['CalID']."
				where course_id = ".$result['GroupID']."
		";		
		$sqlResult["link_with_ical_".$result['CalID']] = $iCal->db_db_query($sql);
		$sqlResult["link_with_ical_".$result['CalID'].'_sql'] = $sql;
	}
}
#check for missing course
$sql ="Select course_id, course_name, course_desc from {$eclass_db}.course 
		where 
			CalID is null and RoomType=0"; 
$result = $iCal->returnArray($sql);
if (count($result)>0){ # link the course with calendar
	#insert class calendar for missing course
	foreach ($result as $r){
		$calID = $iCal->createSystemCalendar($r["course_name"],3,"P",$r["course_desc"]);
		$sqlResult["insert_class_Cal_$calID"] = $calID;
		
		$sql2 = "update {$eclass_db}.course set CalID = $calID 
				where course_id = ".$r["course_id"];
		$sqlResult["update_course_$calID"] = $iCal->db_db_query($sql2);
		
		$dbName = $libeclass->db_prefix."c".$r["course_id"];
		$sql = "SELECT table_name FROM information_schema.tables WHERE table_schema = '$dbName' AND table_name = 'usermaster'";
		$existDB = $iCal->returnArray($sql);
		if (!empty($existDB)){
		$sql = "
			insert into CALENDAR_CALENDAR_VIEWER 
			(CalID,UserID,Access,Color,GroupID,GroupType)
			select '$calID', i.UserID, 'W', '2f75e9','".$r["course_id"]."','C'
			From {$dbName}.usermaster as u inner join INTRANET_USER as i on
			u.user_email = i.UserEmail
			where u.status is null
			";
			$sqlResult["insert_course_viewer_$calID"] = $iCal->db_db_query($sql);
		}
		/*#check the event creator when he has no default calendar
		$sql = "select u.UserID
				from {$eclass_db}.event as e inner join INTRANET_USER as u on
					e.user_email = u.UserEmail
				where u.UserID not in (select Owner from CALENDAR_CALENDAR)
				";
		$eventCreator = $iCal->returnVector($sql);
		if (count($eventCreator)==0){
			foreach($eventCreator as $user){
				//$UserID = $user;
				$iCal->insertMyCalendar();
			}
		}
		
		#insert original eclass event 
		$sql = "select e.title,e.notes,
				e.share_type,
				c.CalID,
				e.is_important,e.is_allday,e.inputdate,
				e.durMi,e.durHr,e.share_type,e.eventdate,
				e.last_modified, e.link, u.UserID
				from {$eclass_db}.event as e 
				inner join {$eclass_db}.course as c on
					e.course_id = c.course_id 
				inner join INTRANET_USER as u on
					e.user_email = u.UserEmail
				where c.course_id = ".$r["course_id"]." and e.share_type <>0
				";
		$result = $iCal->returnArray($sql);

		if (count($result) > 0){
			$calIDset ="";
			$sql = "insert into CALENDAR_EVENT_ENTRY 
			(UserID,CalID,EventDate,InputDate,ModifiedDate,Duration,IsImportant,IsAllDay
			,Access,Title,Description,Url) values ";
			foreach($result as $r){
				foreach($result as $r){
				$calID = $r["CalID"];
				if ($r['share_type']==0){
					//$UserID = $r["UserID"];
					$calID = $iCal->returnOwnerDefaultCalendar();
				}
				$duration = $r["is_allday"]?1440:$r["durMi"]+$r["durHr"]*60;
				$sql .="('".$r["user_id"]."','".$calID."','".$r["eventdate"]."',
						'".$r["inputdate"]."','".$r["last_modified"]."','$duration',
						'".$r["is_important"]."','".$r["is_allday"]."','P',
						'".$r["title"]."','".$r["notes"]."','".$r["link"]."'
						), ";
				$calIDset .= $calID.",";
			}	
			$sql = rtrim($sql,", ");
			$sqlResult["insert_Event"] =$iCal->db_db_query($sql);
			$calIDset = rtrim($calIDset,", ");
			$sql = "update CALENDAR_EVENT_ENTRY set 
					UID = concat(UserID,'-',CalID,'-',EventID,'@eclass')
					where CalID in ($calIDset) and UID is NULL;
					";
			$sqlResult["update_Event"] =$iCal->db_db_query($sql);
		}*/
		
	}
}

# check for missed user
$sql ="Select course_id, course_name, course_desc, CalID from {$eclass_db}.course 	
		where 
			CalID is not null and RoomType=0"; 
$result = $iCal->returnArray($sql);
if (count($result)>0){
	foreach ($result as $r){
		$dbName = $libeclass->db_prefix."c".$r["course_id"];
		$sql = "SELECT table_name FROM information_schema.tables WHERE table_schema = '$dbName' AND table_name = 'usermaster'";
			$existDB = $iCal->returnArray($sql);
		if (count($existDB)>0){
			$sql = "insert into CALENDAR_CALENDAR_VIEWER 
					(CalID,UserID,Access,Color,GroupID,GroupType) 
					select '".$r['CalID']."', i.UserID, 'W', '2f75e9','".$r["course_id"]."','C'
					from {$dbName}.usermaster as u inner join INTRANET_USER as i on
					u.user_email = i.UserEmail
					where i.UserID not in (
						select UserID from CALENDAR_CALENDAR_VIEWER
							where GroupType='C' and GroupID = ".$r['course_id']."
					)";
			$sqlResult['add_missed_user_'.$r['CalID']]= $iCal->db_db_query($sql);
			$sqlResult['add_missed_user_'.$r['CalID'].'_sql']= $sql;
		}
	}
}

#check for deleted course
$sql ="select distinct CalID from CALENDAR_CALENDAR_VIEWER 
		where GroupID not in (select course_id from {$eclass_db}.course) 
		and GroupType='C'
		";
$result = $iCal->returnArray($sql);
if (count($result)>0){
	foreach($result as $CalID){
		$sqlResult['remove_calendar_'.$CalID] = $iCal->removeCalendar($CalID);
	}
}


# check removed user in course
$sql = "select course_id,CalID from {$eclass_db}.course 
		where CalID is not null and RoomType=0";
$resultSet = $iCal->returnArray($sql);
foreach($resultSet as $r){
	$dbName = $libeclass->db_prefix."c".$r['course_id'];
	$sql = "select i.UserID from {$dbName}.usermaster as u 
			inner join INTRANET_USER as i on 
			u.user_email = i.UserEmail
			where u.status = 'deleted'
			and u.user_email not in (
				select user_email from {$dbName}.usermaster 
				where status is null
			)
		";
	$deleteUser = $iCal->returnVector($sql);
	if (count($deleteUser)>0){
		$sql = "delete from CALENDAR_CALENDAR_VIEWER 
				where CalID = ".$r['CalID']." and
				UserID in ('".implode("','",$deleteUser)."')
				
		";
		$sqlResult['delete_removed_user_'.$r['CalID']] = $iCal->db_db_query($sql);
		$sqlResult['delete_removed_user_'.$r['CalID'].'_sql'] = $sql;
	}
}

/* # remove duplicate
$sql = "select c.EventID from CALENDAR_EVENT_ENTRY as c, (select count(*) as cnt, e.UserID,e.CalID,e.EventDate,e.Duration,e.InputDate from CALENDAR_EVENT_ENTRY as e inner join CALENDAR_CALENDAR_VIEWER as v on e.CalID = v.CalID and e.UserID = v.UserID and v.GroupType = 'C' group by e.CalID,e.EventDate,e.Duration,e.InputDate) as a where a.cnt>1 and c.UserID=a.UserID and a.CalID=c.CalID and a.EventDate=c.EventDate and a.Duration=c.Duration and a.InputDate=c.InputDate";
$deplicateID =$iCal->returnVector($sql);
$sql = "delete from CALENDAR_EVENT_ENTRY where EventID in ('".implode("','",$deplicateID )."')";
$sqlResult["remove_duplicate"] =$iCal->db_db_query($sql);

# add unique key
$sql = "alter table CALENDAR_EVENT_ENTRY add unique key (UserID,EventDate,InputDate,Title,CalID)";
$sqlResult["add_unique_key"] =$iCal->db_db_query($sql);*/

#check the event creator when he has no default calendar
$sql = "select u.UserID
		from {$eclass_db}.event as e inner join INTRANET_USER as u on
			e.user_email = u.UserEmail
		where u.UserID not in (select Owner from CALENDAR_CALENDAR)
		";
$eventCreator = $iCal->returnVector($sql);
if (count($eventCreator)==0){
	foreach($eventCreator as $user){
		//$UserID = $user;
		$iCal->insertMyCalendar();
	}
}

$sql = "select max(inputdate) from {$eclass_db}.event
		inner join {$eclass_db}.course as c on
			e.course_id = c.course_id
		";
$maxInputdate = $iCal->returnVector($sql);

# delete group event
$sql = "delete from CALENDAR_EVENT_ENTRY where InputDate<= '".$maxInputdate[0]."' 
		and CalID in (
			select distinct CalID from CALENDAR_CALENDAR_VIEWER where GroupType ='C'
			and GroupID in (select course_id from {$eclass_db}.course)
		)";
$sqlResult["delete_old_group_Event"] =$iCal->db_db_query($sql);
$sqlResult["delete_old_group_Event_Sql"]=$sql;

#check for personal event
$sql = "select e.user_id,e.inputdate,
		e.eventdate,e.title
		{$eclass_db}.event as e 
		inner join {$eclass_db}.course as c on
			e.course_id = c.course_id 
		inner join INTRANET_USER as u on
			e.user_email = u.UserEmail
		where e.share_type=0";
$personalEvent =$iCal->returnArray($sql);
$i = 0;
# remove personal event
foreach($personalEvent as $event){
	//$UserID = $event["UserID"];
	$sql = "delete from CALENDAR_EVENT_ENTRY 
			where CalID in (
				select CalID from CALENDAR_CALENDAR where Owner = //$UserID
			) 
			and UserID = ".$event["user_id"]." and InputDate = ".$event["inputdate"]."
			and EventDate = ".$event["eventdate"]." and Title = '".$event["title"]."'
			";
	$sqlResult["delete_old_personal_event_$i"] =$iCal->db_db_query($sql);
	$sqlResult["delete_old_personal_event_Sql_$i"]=$sql;
	$i++;
}


#insert original eclass event
$sql = "select e.title,e.notes,
		e.share_type,
		c.CalID,
		e.is_important,e.is_allday,e.inputdate,
		e.durMi,e.durHr,e.share_type,e.eventdate,
		e.last_modified, e.link, u.UserID
		from {$eclass_db}.event as e 
		inner join {$eclass_db}.course as c on
			e.course_id = c.course_id 
		inner join INTRANET_USER as u on
			e.user_email = u.UserEmail
		";
$result = $iCal->returnArray($sql);

if (count($result) > 0){
	$calIDset ="";
	$hsql = "insert into CALENDAR_EVENT_ENTRY 
	(UserID,CalID,EventDate,InputDate,ModifiedDate,Duration,IsImportant,IsAllDay
	,Access,Title,Description,Url) values ";
	foreach($result as $r){
		$calID = $r["CalID"];
		if ($r['share_type']==0){
			//$UserID = $r["UserID"];
			$calID = $iCal->returnOwnerDefaultCalendar();
		}
		$duration = $r["is_allday"]?1440:$r["durMi"]+$r["durHr"]*60;
		$sql = $hsql."('".$r["user_id"]."','".$calID."','".$r["eventdate"]."',
				'".$r["inputdate"]."','".$r["last_modified"]."','$duration',
				'".$r["is_important"]."','".$r["is_allday"]."','P',
				'".$r["title"]."','".$r["notes"]."','".$r["link"]."'
				)";
		$iCal->db_db_query($sql);
		$calIDset .= $calID.",";
	}
	$calIDset = rtrim($calIDset,", ");
	$sql = "update CALENDAR_EVENT_ENTRY set 
			UID = concat(UserID,'-',CalID,'-',EventID,'@eclass')
			where CalID in ($calIDset) and UID is NULL;
			";
	$sqlResult["update_Event"] =$iCal->db_db_query($sql);
}

if (!in_array(false,$sqlResult)){
	$iCal->Commit_Trans();
	echo "update successful";
	echo "<br> debug: <br>";
	debug_r($sqlResult);
}
else{
	$iCal->RollBack_Trans();
	echo "update failure";
	echo "<br> debug: <br>";
	debug_r($sqlResult);
}
intranet_closedb();

?>