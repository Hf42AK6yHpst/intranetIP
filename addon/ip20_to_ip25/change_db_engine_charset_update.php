<?php
# using : yat
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($eclass_filepath.'/src/includes/php/lib-filesystem.php');

intranet_opendb();
$ldb = new libdb();

$x = "";

### Record the setup date
# load history file
$history_file = $PATH_WRT_ROOT."file/ip20_to_ip25_change_db_charset_history.txt";
$history_content = trim(get_file_content($history_file));
if ($history_content!="")
{
	# find if schema should be updated
	$tmp_arr = split("\n", $history_content);
	list($last_schema_date, $last_update, $last_update_ip) = split(",", $tmp_arr[sizeof($tmp_arr)-1]);
	
	if ($sql_eClassIP_update[sizeof($sql_eClassIP_update)-1][0]>$last_schema_date)
	{
		$is_new_available = true;
	}
	$new_update = ($is_new_available) ? "Yes" : "No";
} 
else
{
	$new_update = "-";
	$last_update = "-";
	$last_update_ip = "-";
}
$update_count = 0;

/* update intranetIP schema */
$after_date = ($flag==2 && $last_schema_date!="") ? $last_schema_date : "";
//updateSchema($li, $sql_eClassIP_update, $intranet_db, $after_date);

# add update info (latest schema date, time of update, IP of the client)
$time_now = date("Y-m-d H:i:s");
$client_ip = getenv("REMOTE_ADDR");
if (trim($client_ip)=="")
{
	$client_ip = $_SERVER["REMOTE_ADDR"];
}
$history_content .= ($history_content!="") ? "\n" : "";
$history_content .= "$last_schema_date,$time_now,$client_ip";

$lf = new phpduoFileSystem();
$lf->writeFile($history_content, $history_file);



## Get Database array
$DatabaseArr = unserialize(rawurldecode($DatabaseArr));
//$DatabaseArr = array('intranet_DB_REPORT_CARD_', 'IP25_DB_REPORT_CARD_2008');		// For testing only
$numOfDatabase = count($DatabaseArr);

for ($j=0; $j<$numOfDatabase; $j++)
{
	$thisDB = $DatabaseArr[$j];
	//echo 'Processing DB: <b>'.$thisDB.'</b><br><br>';
	
	# switch database
	$ldb->db = $thisDB;
	
	# get table list
	$sql = 'show tables';
	$TableList = $ldb->returnVector($sql);
	
	for ($i=0; $i< sizeof($TableList); $i++) {
		//echo 'Processing: '.$TableList[$i].'<br><br>';
		
		$sql = 'alter table '.$thisDB.'.'.$TableList[$i].' TYPE=innoDB;';
		$x .= $sql."<br>\r\n";
		
		$sql = 'alter table '.$thisDB.'.'.$TableList[$i].' CONVERT TO CHARACTER SET utf8;';
		$x .= $sql."<br>\r\n";
		
		//$x .= '-------------------------<br>';
	}
	//echo 'End of Processing DB: <b>'.$thisDB.'</b><br>';
	//echo '===================================================================<br><br>';
}
echo $x;

echo "/* Back to main */";

intranet_closedb();
?>
