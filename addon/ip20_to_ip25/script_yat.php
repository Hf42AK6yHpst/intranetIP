<?
#############################################################################################
## Do not run the script in Dev site (Since the data in dev site is updated already)
#############################################################################################

# using: 
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/lib.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");


###################################################
### Change file content in "Present Wordings" [Start]
###################################################
include_once($PATH_WRT_ROOT."includes/libwordtemplates.php");
$lf = new libwordtemplates();
$base_dir = "$intranet_root/file/templates";
$files = $lf->return_folderlist($base_dir);
if(!empty($files))
{
	foreach($files as $k=>$file_target)
	{
		$file_content = get_file_content($file_target);
		
		if(!empty($file_content))
		{
			$cur_encoding = mb_detect_encoding($file_content) ; 
			if(!($cur_encoding == "UTF-8" && mb_check_encoding($in_str,"UTF-8")))
			{
				$new_content = Big5ToUnicode($file_content);
				$lf->file_write($new_content,"$file_target");
				echo $file_target .".... file content convert to utf-8 <font color='blue'>successfully</font>.<br>"; 
			}
			else
			{
				echo $file_target .".... no need to convert to utf-8.<br>";
			}
		}
		else
		{
			echo $file_target .".... file empty.<br>"; 
		}
	}
}
echo "Present Wordings checked complete.<br>";

###################################################
### Change file content in "Present Wordings" [End]
###################################################



###################################################
### Change file content in Campus TV schedule
###################################################
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcampustv.php");
$lcampustv = new libcampustv();

$file_target = $lcampustv->programme_file;


$file_content = get_file_content($file_target);

if(!empty($file_content))
{
	$cur_encoding = mb_detect_encoding($file_content) ; 
	if(!($cur_encoding == "UTF-8" && mb_check_encoding($in_str,"UTF-8")))
	{
		$new_content = Big5ToUnicode($file_content);
		$lf->file_write($new_content,"$file_target");
		echo $file_target .".... file content convert to utf-8 <font color='blue'>successfully</font>.<br>"; 
	}
	else
	{
		echo $file_target .".... no need to convert to utf-8.<br>";
	}
}
else
{
	echo $file_target .".... file empty.<br>"; 
}
###################################################
### Change file content in Campus TV schedule [End]
###################################################


intranet_closedb();
?>