<?
# using: 

$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/lib.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_opendb();
$lexport = new libexporttext();

$sql = "SELECT
				iu.UserLogin,
				iu.EnglishName,
				iu.ChineseName,
				ic.ClassName,
				isubject.SubjectName
		FROM
				INTRANET_SUBJECT_TEACHER as ist
				INNER JOIN
				INTRANET_SUBJECT as isubject
				ON (ist.SubjectID = isubject.SubjectID)
				INNER JOIN
				INTRANET_CLASS as ic
				ON (ist.ClassID = ic.ClassID)
				INNER JOIN
				INTRANET_USER as iu
				ON (ist.UserID = iu.UserID)
		WHERE
				iu.RecordType = 1 
				And 
				iu.RecordStatus = 1
		ORDER BY
				iu.EnglishName
		";
$resultSet = $lexport->returnArray($sql);

$exportColumn = array("User Login ID", "English Name", "Chinese Name", "Class Name", "Subject Name", "Subject Group Code");
$export_content = $lexport->GET_EXPORT_TXT($resultSet, $exportColumn, "", "\r\n", "", 0, "00");

$filename = "ip20_subjec_teacher_list.csv";
$lexport->EXPORT_FILE($filename, $export_content);

intranet_closedb();
?>