<?
# using:  

$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include('../check.php');

intranet_opendb();

if ($flag==1)
{
	$x .= "enrolment_script.php .......... Start at ". date("Y-m-d H:i:s")."<br><br>\r\n";	

	$li = new libdb();

	############################################################
	# eEnrolment v1.2 [Start]
	############################################################
	if($plugin['eEnrollment'])
	{
		include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
		$lgs = new libgeneralsettings();
		
		include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
		$libenroll = new libclubsenrol();
		
		$x .= "===================================================================<br>\r\n";
		$x .= "eEnrolment v1.2 [Start]</b><br>\r\n";
		$x .= "===================================================================<br>\r\n";
		
		$eEnrolment_Module = "eEnrolment";
		
		# check if there is already containds settings data, if yes, no need to transfer
		$tmp_ary = $lgs->Get_General_Setting($eEnrolment_Module);
		
		if(empty($tmp_ary))
		{
			### Club Enrolment Settings
			$setting_file = "$intranet_root/file/clubenroll_setting.txt";
			if (is_file($setting_file))
			{
				$file_content = get_file_content($setting_file);
				$data = explode("\n",$file_content);
				$ary = array();
				// Config GpStart and GpEnd are not used anymore
				list($ary['Club_EnrolmentMode'],
					$ary['Club_ApplicationStart'],
					$ary['Club_ApplicationEnd'],
					$GpStart,
					$GpEnd,
					$ary['Club_DefaultMin'],
					$ary['Club_DefaultMax'],
					$ary['Club_TieBreak'],
					$ary['Club_EnrollMax'],
					$ary['Club_EnrollPersonType'],
					$ary['Club_DisableStatus'],
					$ary['Club_ActiveMemberPer'],
					$ary['Club_OneEnrol'],
					$ary['Club_CaterPriority']) = $data;
				
				$Success['ClubSettings'] = $lgs->Save_General_Setting($eEnrolment_Module, $ary);
				
				if ($Success['ClubSettings'])
					$x .= "Club Enrolment Settings are transferred successfully<br>\r\n";
				else
					$x .= "Club Enrolment Settings transfer failed<br>\r\n";
			}
			else
			{
				$x .= "eEnrolment...No \"clubenroll_setting.txt\"<br>\r\n";	
			}
			
			### Club Description
			$desp_file = "$intranet_root/file/clubenroll_desp.txt";
			if (is_file($desp_file))
			{
				$ary = array();
				$ary['Club_EnrollDescription'] = stripslashes(get_file_content($desp_file));
				$Success['ClubDescription'] = $lgs->Save_General_Setting($eEnrolment_Module, $ary);
				
				if ($Success['ClubDescription'])
					$x .= "Club Enrolment Description are transferred successfully<br>\r\n";
				else
					$x .= "Club Enrolment Description transfer failed<br>\r\n";
			}
			else
			{
				$x .= "eEnrolment...No \"clubenroll_desp.txt\"<br>";	
			}
			
			
			### Activity Enrolment Settings
			$setting_event_file = "$intranet_root/file/clubenroll_setting_event.txt";
			if (is_file($setting_event_file))
			{
				$file_content = get_file_content($setting_event_file);
				$data = explode("\n",$file_content);
				$ary = array();
				// Config GpStart and GpEnd are not used anymore
				// Config Tiebreak and PersonType are not applicable for Activity
				list($ary['Activity_EnrolmentMode'],
					$ary['Activity_ApplicationStart'],
					$ary['Activity_ApplicationEnd'],
					$ActivityGpStart,
					$ActivityGpEnd,
					$ary['Activity_DefaultMin'],
					$ary['Activity_DefaultMax'],
					$ActivityTieBreak,
					$ary['Activity_EnrollMax'],
					$ActivityEnrollPersonType,
					$ary['Activity_DisableStatus'],
					$ary['Activity_ActiveMemberPer'],
					$ary['Activity_OneEnrol']) = $data;
				
				$Success['ActivitySettings'] = $lgs->Save_General_Setting($eEnrolment_Module, $ary);
				
				if ($Success['ActivitySettings'])
					$x .= "Activity Enrolment Settings are transferred successfully<br>\r\n";
				else
					$x .= "Activity Enrolment Settings transfer failed<br>\r\n";
			}
			else
			{
				$x .= "eEnrolment...No \"clubenroll_setting_event.txt\"<br>\r\n";	
			}
			
			### Activity Description
			$desp_event_file = "$intranet_root/file/clubenroll_desp_event.txt";
			if (is_file($desp_event_file))
			{
				$ary = array();
				$ary['Activity_EnrollDescription'] = stripslashes(get_file_content($desp_event_file));
				$Success['ActivityDescription'] = $lgs->Save_General_Setting($eEnrolment_Module, $ary);
				
				if ($Success['ActivityDescription'])
					$x .= "Activity Enrolment Description are transferred successfully<br>\r\n";
				else
					$x .= "Activity Enrolment Description transfer failed<br>\r\n";
			}
			else
			{
				$x .= "eEnrolment...No \"clubenroll_desp_event.txt\"<br>";	
			}
			
			
			### Last Archive Info
			$archive_file = "$intranet_root/file/clubenroll_archive.txt";
			if (is_file($archive_file))
			{
				$ary = array();
				$ary['LastArchiveDateTime'] = stripslashes(get_file_content($archive_file));
				$Success['LastArchiveDateTime'] = $lgs->Save_General_Setting($eEnrolment_Module, $ary);
				
				if ($Success['LastArchiveDateTime'])
					$x .= "Last Archive Info is transferred successfully<br>\r\n";
				else
					$x .= "Last Archive Info transfer failed<br>\r\n";
			}
			else
			{
				$x .= "eEnrolment...No \"clubenroll_archive.txt\"<br>";	
			}
			
			
			### Enable Overall Performance Report 
			$setting_performance_file = "$intranet_root/file/clubenroll_setting_performance.txt";
			if (is_file($setting_performance_file))
			{
				$ary = array();
				$ary['EnableOverallPerformanceReport'] = stripslashes(get_file_content($setting_performance_file));
				$Success['EnableOverallPerformanceReport'] = $lgs->Save_General_Setting($eEnrolment_Module, $ary);
				
				if ($Success['EnableOverallPerformanceReport'])
					$x .= "Enable Overall PerformanceReport flag is transferred successfully<br>\r\n";
				else
					$x .= "Enable Overall PerformanceReport flag transfer failed<br>\r\n";
			}
			else
			{
				$x .= "eEnrolment...No \"clubenroll_setting_performance.txt\"<br>";	
			}
		}
		else
		{
			$x .= "Enrolment Settings has been transferred already. No settings transferred this time<br>\r\n";
		}
		
		
		$x .= "===================================================================<br>\r\n";
		$x .= "eEnrolment v1.2 [End]</b><br>\r\n";
		$x .= "===================================================================<br>\r\n";
	}
	############################################################
	# eEnrolment v1.2 [End]
	############################################################
	
	
	
	$x .= "<br>\r\n<br>\r\nEnd at ". date("Y-m-d H:i:s")."<br><br>";	
	echo $x;

	#  save log
	$lf = new libfilesystem();
	$log_folder = $PATH_WRT_ROOT."file/ip20_to_ip25_enrolment_script";
	if(!file_exists($log_folder))
		mkdir($log_folder);
	$log_file = $log_folder."/data_atch_log_". date("YmdHis") .".html";
	$lf->file_write($x, $log_file);
	echo "<br>File Log: ". $log_file;

}
else
{ ?>
	<html>
	<body>
	<font color=red size=+1>[Please Make DB Backup First]</font><br><br>
	
	This page will update the eClass IP 2.0 data compatible with eClass IP 2.5<br />
	Updates included in this script:<br /><br />
	
	1. Update settings of the following module(s):<br />
		<ul>
			<li>eEnrolment</li>
		</ul>
		
	<a href=?flag=1>Click here to proceed.</a> <br><br>
	
	<br>
	</body></html>

	
	
<? }

intranet_closedb();
 ?>
	