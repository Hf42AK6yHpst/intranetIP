<?
# modifying by : 

############################################################################################
################################# IntranetIP 2.5 schema update #################################
############################################################################################

/* Moved to script.php on 10 Aug 2009
$sql_eClassIP_update[] = array(
	"2009-05-12",
	"School Settings -> Location - Add Building Level",
	"CREATE TABLE IF NOT EXISTS INVENTORY_LOCATION_BUILDING (
		BuildingID int(11) NOT NULL auto_increment,
		Code varchar(10),
		NameChi varchar(255),
		NameEng varchar(255),
		DisplayOrder int(11),
		RecordType int(11),
		RecordStatus int(11),
		DateInput datetime,
		DateModified datetime,
		PRIMARY KEY (BuildingID),
		INDEX BuildingID (BuildingID),
		INDEX InventoryBuildingNameChi (NameChi),
		INDEX InventoryBuildingNameEng (NameEng),
		UNIQUE ChineseName (NameChi),
		UNIQUE EnglishName (NameEng),
		UNIQUE BuildingIDWithCode (BuildingID, Code)
	) ENGINE=InnoDB DEFAULT CHARSET=latin1
	"
);

$sql_eClassIP_update[] = array(
	"2009-05-12",
	"School Settings -> Location - Insert a default Building",
	"INSERT INTO INVENTORY_LOCATION_BUILDING 
		(Code, NameChi, NameEng, DisplayOrder, RecordType, RecordStatus, DateInput, DateModified)
	 VALUES
	 	('MB', '�D�j��', 'Main Building', 1, NULL, NULL, now(), now());"
);

$sql_eClassIP_update[] = array(
	"2009-05-12",
	"School Settings -> Location - Set all floors into active status",
	"UPDATE INVENTORY_LOCATION_LEVEL SET RecordStatus = '1';"
);

$sql_eClassIP_update[] = array(
	"2009-05-12",
	"School Settings -> Location - Set all rooms into active status",
	"UPDATE INVENTORY_LOCATION SET RecordStatus = '1';"
);

$sql_eClassIP_update[] = array(
	"2009-05-12",
	"School Settings -> Location - Assign floor, which has not assigned to any buildings yet, to the default building",
	"UPDATE INVENTORY_LOCATION_LEVEL SET BuildingID = 1 WHERE BuildingID IS NULL;"
);
*/
$sql_eClassIP_update[] = array(
	"2009-08-14",
	"iCalendar - Alter table CALENDAR_CALENDAR",
	"Alter Table CALENDAR_CALENDAR
		Add Column CalSharedType char(1),
		Add Column SyncURL varchar(200)
	"
);
$sql_eClassIP_update[] = array(
	"2009-08-14",
	"iCalendar - Alter table CALENDAR_CALENDAR_VIEWER",
	"Alter Table CALENDAR_CALENDAR_VIEWER
		Add Column GroupPath varchar(255)
	"
);
$sql_eClassIP_update[] = array(
	"2009-08-14",
	"iCalendar - Alter table CALENDAR_EVENT_ENTRY",
	"Alter Table CALENDAR_EVENT_ENTRY
		Add Column UID varchar(100),
		Add Column ExtraIcalInfo mediumtext
	"
);
$sql_eClassIP_update[] = array(
	"2009-08-14",
	"iCalendar - Alter table CALENDAR_EVENT_ENTRY_REPEAT",
	"Alter Table CALENDAR_EVENT_ENTRY_REPEAT
		Add Column ICalIsInfinite char(1) Default 0
	"
);

$sql_eClassIP_update[] = array(
	"2009-08-14",
	"iCalendar - Alter table CALENDAR_EVENT_PERSONAL_NOTE",
	"Alter Table CALENDAR_EVENT_PERSONAL_NOTE
		Add Column CalType tinyint(4)
	"
);


?>