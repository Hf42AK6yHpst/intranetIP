<?
// using: 
@SET_TIME_LIMIT(800);
include_once("../../includes/global.php");
include_once($eclass_filepath.'/addon/check.php');
include_once($eclass_filepath.'/src/includes/php/lib-filesystem.php');
include_once("../../includes/libdb.php");
//include_once('sql_table_update.php');
intranet_opendb();

$last_schema_date = "0000-00-00";
function updateSchema($lo, $sql_arr, $course_id, $after_date=""){
	global $update_count, $last_schema_date;
	for ($i=0; $i<sizeof($sql_arr); $i++)
	{
		$sql = $sql_arr[$i];
		if (is_array($sql))
		{
			if ($after_date=="" || $sql[0]>$after_date)
			{
				if ($sql[0]>$last_schema_date)
				{
					$last_schema_date = $sql[0];
				}
				echo "<b>[Date: ".$sql[0]." deployment]</b><br>";
				echo "<b>[Function: ".$sql[1]."]</b><br>";
				if (!$lo->db_db_query($sql[2]))
				{
					echo "warning: ".$sql[2]." <br><br>";
				} else
				{
					$update_count ++;
					echo "Ok<br><br>";
				}
			}
		}
	}

	return;
}

$li = new libdb();
$li->db = $intranet_db;


# load history file
$history_file = "../../file/ip20_to_ip25_script_update_history.txt";
$history_content = trim(get_file_content($history_file));
if ($history_content!="")
{
	# find if schema should be updated
	$tmp_arr = split("\n", $history_content);
	list($last_schema_date, $last_update, $last_update_ip) = split(",", $tmp_arr[sizeof($tmp_arr)-1]);
	
	if ($sql_eClassIP_update[sizeof($sql_eClassIP_update)-1][0]>$last_schema_date)
	{
		$is_new_available = true;
	}
	$new_update = ($is_new_available) ? "Yes" : "No";
} else
{
	$new_update = "-";
	$last_update = "-";
	$last_update_ip = "-";
}


############################################################################
if (isset($flag))
{
	$update_count = 0;

	/* update intranetIP schema */
	$after_date = ($flag==2 && $last_schema_date!="") ? $last_schema_date : "";
	//updateSchema($li, $sql_eClassIP_update, $intranet_db, $after_date);
	
	# add update info (latest schema date, time of update, IP of the client)
	$time_now = date("Y-m-d H:i:s");
	$client_ip = getenv("REMOTE_ADDR");
	if (trim($client_ip)=="")
	{
		$client_ip = $_SERVER["REMOTE_ADDR"];
	}
	$history_content .= ($history_content!="") ? "\n" : "";
	$history_content .= "$last_schema_date,$time_now,$client_ip";

	$lf = new phpduoFileSystem();
	$lf->writeFile($history_content, $history_file);
	
}
intranet_closedb();

if (!isset($flag))
{
?>

<HTML>
<HEAD>
<meta http-equiv='content-type' content='text/html; charset=utf-8' />
<TITLE>eClass</TITLE>
<style type="text/css">
BODY, P, TD {
	FONT-FAMILY: Helvetica, Mingliu, Sans-Serif;
}
</style>
</HEAD>

<BODY>

<hr>
<table border="0" cellpadding="5" cellspacing="5" align="center">
<tr><td colspan="2" height="30" bgcolor="#FAF0F0" align="center"><font color="#8826FF"><b>eClassIP Database Schema Update</b></font></td></tr>
<!-- <tr><td align="right" nowrap bgcolor="#FEFEFE">&nbsp; &nbsp; Available of schema update:</td><td bgcolor="#FEFEFE"><font color="<?=(($new_update=='Yes') ? '#FF3333' : '#000000')?>"><?=$new_update?></font></td></tr> -->
<tr><td align="right" bgcolor="#FEFEFE">Last update from:</td><td nowrap bgcolor="#FEFEFE"><?=$last_update_ip?></td></tr>
<tr><td align="right" bgcolor="#FEFEFE">Last update on:</td><td nowrap bgcolor="#FEFEFE"><?=$last_update?> &nbsp; &nbsp;</td></tr>

<? if ($last_update == '-') { ?>
	<tr><td align="center" colspan="2"><br><br><b>Please select to perform:</b></td></tr>
	<tr><td align="center" colspan="2">==> <a href="./?flag=1#toend">Run Script of IP20 to IP25</a> <==</td></tr>
	<tr><td align="center" colspan="2" style="color:red"><b>* This script can be run ONCE only</b></td></tr>
<? } else { ?>
	<tr><td align="center" colspan="2"><br><br></td></tr>
	<tr><td align="center" colspan="2">==> <a href="#">This script can be run ONCE only</a> <==</td></tr>
<? } ?>
</table>

</BODY>
</HTML>


<?php
}
else
{
?>

<body>
	<form id="MainForm" name="MainForm" action="script.php" method="POST">
		<input type="hidden" id="last_update" name="last_update" value="<?=$last_update?>" />
	</form>
</body>

<script>
	document.getElementById('MainForm').submit();
</script>
<?
}
?>