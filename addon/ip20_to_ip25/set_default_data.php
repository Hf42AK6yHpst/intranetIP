<?
# using: 

# Sports Day , Group, Group Category default value are set in addon/install/strat.hp
	
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include('../check.php');

if ($flag==1)
{
	$x .= "set_default_data.php .......... Start at ". date("Y-m-d H:i:s")."<br><br>\r\n";	

	include_once($PATH_WRT_ROOT."includes/libdb.php");
	intranet_opendb();
	$lib = new libdb();
	
	############################################################################
	# Swimming Gala[Start]
	############################################################################
	$x .= "============ Swimming Gala [Start] ============<br>";
	$sql = "INSERT IGNORE INTO SWIMMINGGALA_EVENT_TYPE_NAME 
			(EventTypeID, EnglishName, ChineseName, DateInput, DateModified) 
			VALUES
	        ('1','Track','個人賽事', now(), now())";
	$result = $lib->db_db_query($sql);
	$x .= "Create 'Track/個人賽事' type to Swimming Gala. ";
	if ($result == 1)
		$x .= "<b><font color='blue'>Success</font></b><br>\r\n";
	else
		$x .= "<b><font color='red'>Failed</font></b><br>\r\n";
			
	$sql = "INSERT IGNORE INTO SWIMMINGGALA_EVENT_TYPE_NAME 
			(EventTypeID, EnglishName, ChineseName, DateInput, DateModified) 
			VALUES
	        ('3','House Relay','社際接力', now(), now())";
	$result = $lib->db_db_query($sql);
	$x .= "Create 'House Relay/社際接力' type to Swimming Gala. ";
	if ($result == 1)
		$x .= "<b><font color='blue'>Success</font></b><br>\r\n";
	else
		$x .= "<b><font color='red'>Failed</font></b><br>\r\n";
		
	$sql = "INSERT IGNORE INTO SWIMMINGGALA_EVENT_TYPE_NAME 
			(EventTypeID, EnglishName, ChineseName, DateInput, DateModified) 
			VALUES
	        ('4','Class Relay','班際接力', now(), now())";
	$result = $lib->db_db_query($sql);
	$x .= "Create 'Class Relay/班際接力' type to Swimming Gala. ";
	if ($result == 1)
		$x .= "<b><font color='blue'>Success</font></b><br>\r\n";
	else
		$x .= "<b><font color='red'>Failed</font></b><br>\r\n";

	$x .= "============ Swimming Gala [End] ============<br><br>";
	############################################################################
	# Swimming Gala[ [End]
	############################################################################

	$x .= "<br>\r\n<br>\r\nEnd at ". date("Y-m-d H:i:s")."<br><br>";	
	echo $x;

	#  save log
	$lf = new libfilesystem();
	$log_folder = $PATH_WRT_ROOT."file/ip20_to_ip25_log";
	if(!file_exists($log_folder))
		mkdir($log_folder);
	$log_file = $log_folder."/set_default_data_log_". date("YmdHis") .".html";
	$lf->file_write($x, $log_file);
	echo "<br>File Log: ". $log_file;
	
	echo "<br>Back to main";
	
	intranet_closedb();

}
else
{
?>
<html>
<body>
<font color=red size=+1>Thie script will create the followings default data to database:</font><br>
<ul>
	<li>Swimming Gala </li>
<br><br><br>
<a href=?flag=1>Click here to proceed.</a>
</body></html>
<?
}
?>