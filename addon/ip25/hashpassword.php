<?php
// editing by

@SET_TIME_LIMIT(0);
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libauth.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
intranet_opendb();

$li = new libdb();
$li->db = $intranet_db;
$lgs = new libgeneralsettings();

$ModuleName = "InitSetting";
$GSary = $lgs->Get_General_Setting($ModuleName);


if ($intranet_authentication_method=="HASH" && $intranet_password_salt!="" && $GSary['HashedPassword']!=1)
{
	$sql = "UPDATE INTRANET_USER SET HashedPass = MD5( CONCAT(UserLogin, UserPassword, '$intranet_password_salt')) where UserPassword<>'' AND UserPassword Is NOT NULL";
	$li->db_db_query($sql);

	# INTRANET_USER_PERSONAL_SETTINGS - only need to update if mcrypt is not available
	if (!function_exists('mcrypt_get_iv_size'))
	{
		$liaut = new libauth();
		$sql = "SELECT UserID, EncPassword, EncPassword_DateModified FROM INTRANET_USER_PERSONAL_SETTINGS ORDER BY UserID";
		$rows = $li->returnArray($sql);
		for ($i=0; $i<sizeof($rows); $i++)
		{
			$user_now = $rows[$i];
			if ($user_now['EncPassword']!="")
			{
				$liaut->UpdateEncryptedPassword($user_now['UserID'], $user_now['EncPassword']);
			}
		}
	}
	
	# clear all passwords
	
	# update General Settings - markd the script is executed
	$sql = "insert ignore into GENERAL_SETTING 
						(Module, SettingName, SettingValue, DateInput) 
					values 
						('$ModuleName', 'HashedPassword', 1, now())";
	$Result['UpdateGeneralSettings'] = $li->db_db_query($sql);
	
}


# clear password in Intranet
if ($intranet_authentication_method=="HASH" && $intranet_password_salt!="" && $GSary['ClearPlainPassword']!=1)
{
	$sql = "SELECT count(*) FROM INTRANET_USER WHERE HashedPass<>'' AND HashedPass Is NOT NULL AND UserPassword<>'' AND UserPassword Is NOT NULL ";
	$rows = $li->returnVector($sql);

	# INTRANET_USER_PERSONAL_SETTINGS - only need to update if mcrypt is not available
	if ($rows[0]>0)
	{
		$sql = "UPDATE INTRANET_USER set UserPassword='' WHERE HashedPass<>'' AND HashedPass Is NOT NULL ";
		$li->db_db_query($sql);
		
		# update General Settings - markd the script is executed
		$sql = "insert ignore into GENERAL_SETTING 
							(Module, SettingName, SettingValue, DateInput) 
						values 
							('$ModuleName', 'ClearPlainPassword', 1, now())";
		$Result['UpdateGeneralSettings'] = $li->db_db_query($sql);
				
	}	
}


# clear password in Intranet
if ($intranet_authentication_method=="HASH" && $intranet_password_salt!="" && $GSary['ClearPlainPasswordClassrooms']!=1)
{
	$li->db = $eclass_db;
	$sql = "UPDATE user_course set user_password ='' ";
	$li->db_db_query($sql);
	$sql = "SELECT course_id FROM course order by course_id";
	$rows = $li->returnVector($sql);

	# INTRANET_USER_PERSONAL_SETTINGS - only need to update if mcrypt is not available
	for ($ci=0; $ci<sizeof($rows); $ci++)
	{
		$dbname_now = $eclass_prefix."c".$rows[$ci];
		$sql = "UPDATE {$dbname_now}.usermaster set user_password='' ";
		$li->db_db_query($sql);
	}	
	
	$li->db = $intranet_db;
	# update General Settings - markd the script is executed
	$sql = "insert ignore into GENERAL_SETTING 
						(Module, SettingName, SettingValue, DateInput) 
					values 
						('$ModuleName', 'ClearPlainPasswordClassrooms', 1, now())";
	$Result['UpdateGeneralSettings'] = $li->db_db_query($sql);
}

?>