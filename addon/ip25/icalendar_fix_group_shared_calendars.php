<?php
// editing by 
/****************************************** Description ******************************************
 * To fix calendar viewers for group members - i.e groups that a calendar has granted viewer right to it
 *************************************************************************************************/
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/icalendar.php");
include_once($PATH_WRT_ROOT."includes/libgrouping.php");

intranet_opendb();

$li = new libdb();
$libgrouping = new libgrouping();
$groups = $libgrouping->returnGroups();
$iCal = new icalendar();

$ModuleName = "InitSetting";
$GSary = $lgs->Get_General_Setting($ModuleName);

if($GSary['iCalendarFixGroupSharedCalendars']!=1){
	echo "<br>\n";
	echo "##############################################################################################################<br>\n";
	echo "############## iCalendar - Fix Group Shared Calenders for Group Members [BEGIN] ##############################<br>\n";
	echo "##############################################################################################################<br>\n";
	
	$result = array();
	for($i=0;$i<count($groups);$i++)
	{
		$sql = "SELECT u.UserID FROM INTRANET_USER as u 
				INNER JOIN INTRANET_USERGROUP as ug ON ug.UserID=u.UserID 
				INNER JOIN INTRANET_GROUP as g ON g.GroupID=ug.GroupID 
				WHERE u.RecordStatus=1 AND g.GroupID='".$groups[$i]['GroupID']."' ";
		$group_users = $li->returnVector($sql);
		
		$result['UpdateCalendarViewer_'.$groups[$i]['GroupID']] = $iCal->addCalendarViewerToGroup($groups[$i]['GroupID'],$group_users);
	}
	
	echo "Fixed ".count($result)." group members.<br>\n";
	
	# update General Settings - markd the script is executed
	$sql = "insert ignore into GENERAL_SETTING 
						(Module, SettingName, SettingValue, DateInput) 
					values 
						('$ModuleName', 'iCalendarFixGroupSharedCalendars', 1, now())";
	$li->db_db_query($sql);
	
	echo "##############################################################################################################<br>\n";
	echo "############## iCalendar - Fix Group Shared Calenders for Group Members [END] ##############################<br>\n";
	echo "##############################################################################################################<br>\n";
}
?>
