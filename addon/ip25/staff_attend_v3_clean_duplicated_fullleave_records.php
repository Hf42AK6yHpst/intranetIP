<?php
# using: 
/************************************** Description ****************************************
 * Purpose: clean all duplicated full day holiday and full day outgoing daily log records 
 *******************************************************************************************/
@SET_TIME_LIMIT(0);
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
intranet_opendb();

$li = new libdb();
$li->db = $intranet_db;
$lgs = new libgeneralsettings();

$ModuleName = "InitSetting";
$GSary = $lgs->Get_General_Setting($ModuleName);

if ($GSary['StaffAttendV3CleanDuplicatedFullLeaveRecords'] != 1 && $module_version['StaffAttendance'] == 3.0 && $plugin['attendancestaff']){
	include_once($PATH_WRT_ROOT."includes/libstaffattend2.php");
	include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
	echo '========== Cleaning Staff Attendance V3 duplicated full leave records... ===========<br>';
	
	//$debug_output = $_REQUEST['debug_output'];
	//$fix = $_REQUEST['fix']==1;
	$debug_output = false;
	$fix = true;
	//$StaffAttend3 = new libstaffattend3();
	/*
	if(isset($_REQUEST['outgoing'])){
		$RecordType = CARD_STATUS_OUTGOING;
	}else{
		$RecordType = CARD_STATUS_HOLIDAY;
	}
	if($RecordType == CARD_STATUS_HOLIDAY){
		$Header = "No. of full day holiday";
	}else{
		$Header = "No. of full day outgoing";
	}
	*/
	$RecordType = CARD_STATUS_OUTGOING.",".CARD_STATUS_HOLIDAY;
	$RecordTypeAssoc = array();
	$RecordTypeAssoc[CARD_STATUS_OUTGOING] = "Outing";
	$RecordTypeAssoc[CARD_STATUS_HOLIDAY] = "Holiday";
	//if($debug_output){
	//	echo "Handling ".($RecordType == CARD_STATUS_OUTGOING ? "Full Day Outgoing":" Full Day Holiday")." Records.<br>";
	//}
	$style = "style='border:1px solid black'";
	
	$sql = "show tables like 'CARD_STAFF_ATTENDANCE2_DAILY_LOG_%'";
	$DailyLogTables = $li->returnVector($sql);
	
	$totalFixed = 0;
	for ($i=0; $i< sizeof($DailyLogTables); $i++) {	
		$DailyLogTable = $DailyLogTables[$i];
		$temp = explode("_",$DailyLogTable);
		$YEAR = $temp[5];
		$MONTH = $temp[6];
		//$totalFixed = 0;
		
		if($debug_output) echo $DailyLogTable."<br>";
		
		$sql = "SELECT 
					 d.StaffID,DATE_FORMAT(CONCAT('$YEAR-$MONTH-',d.DayNumber),'%Y-%m-%d') as RecordDate, COUNT(d.RecordID) as NumOfFullDayRecord,
					 GROUP_CONCAT(d.RecordID) as CardLogID, GROUP_CONCAT(p.RecordID) as ProfileID, d.RecordType 
				FROM $DailyLogTable as d 
				INNER JOIN CARD_STAFF_ATTENDANCE2_PROFILE as p ON p.RecordID = d.InAttendanceRecordID 
				WHERE d.RecordType IN (".$RecordType.") AND d.Duty = 0 
					AND d.InSchoolStatus IN (".$RecordType.") AND d.OutSchoolStatus IN (".$RecordType.") 
					AND SlotName IS NULL 
				GROUP BY d.StaffID, d.DayNumber, d.RecordType ";
		
		$result = $li->returnArray($sql);
		if($debug_output){
			echo "<table $style><tbody>";
			echo "<tr><td $style>StaffID</td><td $style>RecordDate</td><td $style>No. of full day leave</td><td $style>CardLogID</td><td $style>ProfileID</td><td $style>Leave type</td><td $style>Remove CardLogID</td><td $style>Remove ProfileID</td>";
			if($fix) echo "<td $style>Fix cardlog status</td><td $style>Fix profile status</td>";
			echo "</tr>";
		}
		for($j=0;$j<count($result);$j++){
			if($result[$j]['NumOfFullDayRecord'] <= 1) continue;
			
			$remove_dailylogid = substr($result[$j]['CardLogID'],stripos($result[$j]['CardLogID'],',')+1);
			$remove_profileid = substr($result[$j]['ProfileID'],stripos($result[$j]['ProfileID'],',')+1);
			
			if($debug_output){
				echo "<tr>";
				for($k=0;$k<5;$k++){
					echo "<td $style>".$result[$j][$k]."</td>";
				}
				echo "<td $style>".$RecordTypeAssoc[$result[$j]['RecordType']]."</td>";
				echo "<td $style>$remove_dailylogid</td>";
				echo "<td $style>$remove_profileid</td>";
			}
			if($fix){
				$sql = "DELETE FROM $DailyLogTable WHERE RecordID IN ($remove_dailylogid) ";
				$del_dailylog_success = $li->db_db_query($sql);
				$sql = "DELETE FROM CARD_STAFF_ATTENDANCE2_PROFILE WHERE RecordID IN ($remove_profileid) ";
				$del_profile_success = $li->db_db_query($sql);
				if($debug_output){
					echo "<td $style>".($del_dailylog_success?"Success":"Failed")."</td>";
					echo "<td $style>".($del_profile_success?"Success":"Failed")."</td>";
				}
				$totalFixed++;
			}
			if($debug_output) echo "</tr>";
		}
		if($debug_output){
			echo "</tbody></table>";
			echo "<br><br>";
		}
		//echo "$DailyLogTable has $totalFixed records fixed.<br><br>";
		
		ob_flush();
		flush();
	}
	echo "$totalFixed records fixed.<br><br>";
	
	# update General Settings - markd the script is executed
	$sql = "insert ignore into GENERAL_SETTING 
						(Module, SettingName, SettingValue, DateInput) 
					values 
						('$ModuleName', 'StaffAttendV3CleanDuplicatedFullLeaveRecords', 1, now())";
	$Result['UpdateGeneralSettings'] = $li->db_db_query($sql);
	// end handle daily log schema change
	
	//debug_r($Result);
	echo '========== End of cleaning Staff Attendance V3 duplicated full leave records ============<br>';
}

?>