<?
# using: 
@SET_TIME_LIMIT(0);
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
intranet_opendb();

$li = new libdb();
$li->db = $intranet_db;
$lgs = new libgeneralsettings();

$ModuleName = "InitSetting";
$GSary = $lgs->Get_General_Setting($ModuleName);

if ($GSary['StaffAttendV3DailyLogAddConfirmFields'] != 1 && $module_version['StaffAttendance'] == 3.0 && $plugin['attendancestaff']) {
	echo 'adding field DateConfirmed, ConfirmBy to CARD_STAFF_ATTENDANCE2_DAILY_LOG_% Tables<br>';
	
	// Handle old Daily log schema change
	$sql = 'show tables like \'CARD_STAFF_ATTENDANCE2_DAILY_LOG_%\'';
	$DailyLogTables = $li->returnVector($sql);
	
	for ($i=0; $i< sizeof($DailyLogTables); $i++) {	
		$sql = 'alter table '.$DailyLogTables[$i].' add column DateConfirmed datetime ';
		$Result[$DailyLogTables[$i].':addDateConfirmed'] = $li->db_db_query($sql);
		$sql = 'alter table '.$DailyLogTables[$i].' add column ConfirmBy int(11) ';
		$Result[$DailyLogTables[$i].':addConfirmBy'] = $li->db_db_query($sql);
	}
	
	# Transfer DateModified and ModifyBy to DateConfirmed and ConfirmedBy correspondingly
	for ($i=0; $i< sizeof($DailyLogTables); $i++) {	
		$sql = 'update '.$DailyLogTables[$i].' set DateConfirmed = DateModified, ConfirmBy = ModifyBy 
				where RecordStatus = 1 AND DateModified IS NOT NULL ';
		$Result[$DailyLogTables[$i].':CopyToDateConfirmedAndConfirmBy'] = $li->db_db_query($sql);
	}
	
	# update General Settings - markd the script is executed
	$sql = "insert ignore into GENERAL_SETTING 
						(Module, SettingName, SettingValue, DateInput) 
					values 
						('$ModuleName', 'StaffAttendV3DailyLogAddConfirmFields', 1, now())";
	$Result['UpdateGeneralSettings'] = $li->db_db_query($sql);
	// end handle daily log schema change
	//debug_r($Result);
	echo 'Staff Attendance V3 Migration of adding confirm fields ended<br>';
}
?>