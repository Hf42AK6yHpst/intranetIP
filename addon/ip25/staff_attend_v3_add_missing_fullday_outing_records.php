<?php
# using: 
/************************************** Description ****************************************
 * Purpose: Add missing full day outing daily log records 
 *******************************************************************************************/
@SET_TIME_LIMIT(0);
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
intranet_opendb();

$li = new libdb();
$li->db = $intranet_db;
$lgs = new libgeneralsettings();

$ModuleName = "InitSetting";
$GSary = $lgs->Get_General_Setting($ModuleName);

//$fix = $_REQUEST['fix'] == 1;
$fix = 1;
//$debug_output = $_REQUEST['debug_output'] == 1; 
$debug_output = 0;

if ($GSary['StaffAttendV3AddMissingFullDayOutingRecords'] != 1 && $module_version['StaffAttendance'] == 3.0 && $plugin['attendancestaff']){
	include_once($PATH_WRT_ROOT."includes/libstaffattend2.php");
	include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
	
	$StaffAttend3 = new libstaffattend3();
	
	echo "###################### Staff Attendance V3 - Add missing full day outing records [Start] #######################<br />\n";
	
	$StartDate = '2012-01-01';
	$EndDate = date("Y-m-d"); // up to today
	
	$ts_startdate = strtotime($StartDate);
	$ts_enddate = strtotime($EndDate);
	
	// Prepare a ReasonID to DefaultWavie mapping array (Outing reasons)
	$sql = "SELECT ReasonID, DefaultWavie FROM CARD_STAFF_ATTENDANCE3_REASON WHERE ReasonActive = 1 AND ReasonType IN (".CARD_STATUS_OUTGOING.")";
	$ReasonIDDefaultWavieArray = $li->returnArray($sql);
	//debug_r($ReasonIDDefaultWavieArray);
	
	$ReasonIDToDefaultWavieArray = array();
	for($tt=0;$tt<count($ReasonIDDefaultWavieArray);$tt++){
		$ReasonIDToDefaultWavieArray[$ReasonIDDefaultWavieArray[$tt]['ReasonID']] = $ReasonIDDefaultWavieArray[$tt]['DefaultWavie'];
	}
	$day_count = 0;
	// go through day by day
	for($ts_cur=$ts_startdate;$ts_cur <= $ts_enddate;$ts_cur+=86400) {
		$cur_date = date("Y-m-d",$ts_cur);
		$cur_year = date("Y",$ts_cur);
		$cur_month = date("m",$ts_cur);
		$cur_day = date("d",$ts_cur);
		$day_count++;
		
		$CardLogTable = "CARD_STAFF_ATTENDANCE2_DAILY_LOG_".$cur_year."_".$cur_month;
		
		if($debug_output) {
			echo "Date: $cur_date<br />\n";
		}
		// Get full day outing leave setting
		$sql = "select 
					lr.StaffID,
					lr.RecordType,
					lr.ReasonID 
				from 
					CARD_STAFF_ATTENDANCE2_LEAVE_RECORD lr 
					inner join 
					INTRANET_USER u 
					on lr.StaffID = u.UserID 
				where 
					lr.RecordDate = '".$cur_date."' 
					and lr.RecordType='".CARD_STATUS_OUTGOING."' 
					and lr.SlotID IS NULL 
				order by 
					lr.StaffID ";
		$LeaveList = $li->returnArray($sql);
		
		$FullOutgoingLeave = array();
		$FullOutgoingLeaveReason = array();
		for ($i=0; $i< sizeof($LeaveList); $i++) {
			$FullOutgoingLeave[] = $LeaveList[$i]['StaffID'];
			$FullOutgoingLeaveReason[$LeaveList[$i]['StaffID']] = $LeaveList[$i]['ReasonID'];
		}
		
		if($debug_output) {
			echo "Full day outing users:<br />\n";
			debug_r($FullOutgoingLeave);
		}
		
		// no leave setting, ignore it
		if (sizeof($FullOutgoingLeave) == 0) {
			continue;
		}
		
		// Get Daily Log Records
		$CardLogDetail = $StaffAttend3->Get_Daily_Log_Record($FullOutgoingLeave,$cur_year,$cur_month,$cur_day,array());
		
		$LoggedUser = array();
		for ($i=0; $i< sizeof($CardLogDetail); $i++) {
			$LoggedUser[] = $CardLogDetail[$i]['StaffID'];
		}
		
		// Find existing full day outgoing daily log records to prevent duplicated records (because NULL SlotName does not treat as UNIQUE KEY) 
		$sql = "SELECT StaffID FROM $CardLogTable WHERE StaffID IN (".implode(",",$FullOutgoingLeave).") AND DayNumber = '$cur_day' AND SlotName IS NULL AND RecordType = '".CARD_STATUS_OUTGOING."' ";
		$ExistingFullDayOutgoingStaffID = $li->returnVector($sql);
		
		if($debug_output) {
			echo "Cardlog users:<br />\n";
			debug_r($LoggedUser);
			
			echo "Existing full day outing users: <br />\n";
			debug_r($ExistingFullDayOutgoingStaffID);
		}
		
		for ($i=0; $i< sizeof($FullOutgoingLeave); $i++) {
			if (!in_array($FullOutgoingLeave[$i],$LoggedUser) && !in_array($FullOutgoingLeave[$i],$ExistingFullDayOutgoingStaffID)) {
				$LeaveReasonID = $FullOutgoingLeaveReason[$FullOutgoingLeave[$i]];
				$ReasonDefaultWavie = $ReasonIDToDefaultWavieArray[$LeaveReasonID]==1? "1":"NULL";
				$sql = "INSERT INTO CARD_STAFF_ATTENDANCE2_PROFILE
								(
									StaffID, 
									ReasonID,
									RecordDate,
									RecordType,
									Waived,
									DateInput,
									DateModified
								)
								VALUES
								(
									'".$FullOutgoingLeave[$i]."',
									'$LeaveReasonID',
									'$cur_date',
									'".CARD_STATUS_OUTGOING."',
									".$ReasonDefaultWavie.",
									NOW(),
									NOW()
								)";
				if($debug_output) {
					echo $sql;
					echo "<br />\n";
				}
				if($fix) {
					$li->db_db_query($sql);
					$ProfileID = $li->db_insert_id();
				}
				
				$sql = "INSERT INTO ".$CardLogTable."
								(
									StaffID, 
									DayNumber,
									Duty,
									InSchoolStatus,
									OutSchoolStatus,
									RecordType,
									InAttendanceRecordID,
									InWaived,
									DateInput,
									DateModified
								)
								VALUES
								(
									'".$FullOutgoingLeave[$i]."',
									'$cur_day',
									'0',
									'".CARD_STATUS_OUTGOING."',
									'".CARD_STATUS_OUTGOING."',
									'".CARD_STATUS_OUTGOING."',
									'$ProfileID',
									".$ReasonDefaultWavie.",
									NOW(),
									NOW()
								)";
				if($debug_output) {
					echo $sql;
					echo "<br />\n";
				}
				
				if($fix) {
					$li->db_db_query($sql);
					$CardLogID = $li->db_insert_id();
				}
			}
		}
		
		if( $debug_output && ($day_count % 30) == 0) {
			ob_flush();
			flush();
		}
	}
	// #1. -- End of Preparing CardLog for Full Outgoing Users --
	
	if($fix) {
		# update General Settings - markd the script is executed
		$sql = "insert ignore into GENERAL_SETTING 
							(Module, SettingName, SettingValue, DateInput) 
						values 
							('$ModuleName', 'StaffAttendV3AddMissingFullDayOutingRecords', 1, now())";
		$Result['UpdateGeneralSettings'] = $li->db_db_query($sql);
		// end handle daily log schema change
	}
	//debug_r($Result);
	echo "################## Staff Attendance V3 - Add missing full day outing records [End] ####################<br>\n";
}
?>