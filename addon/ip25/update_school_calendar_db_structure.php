<?php

$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
intranet_opendb();

$li = new libdb();

$li->Start_Trans();
$result = array();

$sql = "ALTER TABLE INTRANET_PERIOD_TIMETABLE_RELATION DROP PRIMARY KEY";
$result[] = $li->db_db_query($sql);
if(!in_array(false,$result)){
	echo "Alter Table INTRANET_PERIOD_TIMETABLE_RELATION...Drop Primary Key...<font color='blue'>Success</font>. <br>\n";
}else{
	echo "Alter Table INTRANET_PERIOD_TIMETABLE_RELATION...Drop Primary Key...<font color='red'>Fail</font>. <br>\n";
	$li->RollBack_Trans();
}

$result = array();
$sql = "select max(datemodified), periodid from INTRANET_PERIOD_TIMETABLE_RELATION group by periodid";
$tmp_arr = $li->returnArray($sql,2);
if(sizeof($tmp_arr)>0){
	for($i=0; $i<sizeof($tmp_arr); $i++){
		list ($MaxDateModified, $PeriodID) = $tmp_arr[$i];
		$sql = "delete from INTRANET_PERIOD_TIMETABLE_RELATION where PeriodID = $PeriodID and DateModified != '$MaxDateModified'";
		$result[] = $li->db_db_query($sql);
	}
}
if(!in_array(false,$result)){
	echo "INTRANET_PERIOD_TIMETABLE_RELATION...Remove old data...<font color='blue'>Success</font>. <br>\n";
}else{
	echo "INTRANET_PERIOD_TIMETABLE_RELATION...Remove old data...<font color='red'>Fail</font>. <br>\n";
	$li->RollBack_Trans();
}

$result = array();
$sql = "ALTER TABLE INTRANET_PERIOD_TIMETABLE_RELATION ADD PRIMARY KEY(PeriodID)";
$result[] = $li->db_db_query($sql);
if(!in_array(false,$result)){
	echo "Alter Table INTRANET_PERIOD_TIMETABLE_RELATION...Add New Primary Key...<font color='blue'>Success</font>. <br>\n";
}else{
	echo "Alter Table INTRANET_PERIOD_TIMETABLE_RELATION...Add New Primary Key...<font color='red'>Fail</font>. <br>\n";
	$li->RollBack_Trans();
}
$li->Commit_Trans();
intranet_closedb();

?>