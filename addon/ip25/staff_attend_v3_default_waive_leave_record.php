<?php
// editing by 
/*
 * Fix Leave records (both fullday and timeslot)(Holiday, Outing), 
 * if Reason DefaultWavie is 1 
 * 1) set Daily log record(for those non-confirmed records ) InWaived to 1, 
 * 2) set Profile Waived to 1 
 */
@SET_TIME_LIMIT(0);
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
intranet_opendb();

$li = new libdb();
$li->db = $intranet_db;
$lgs = new libgeneralsettings();

$ModuleName = "InitSetting";
$GSary = $lgs->Get_General_Setting($ModuleName);

if ($GSary['StaffAttendV3DefaultWaiveLeaveRecords'] != 1 && $module_version['StaffAttendance'] == 3.0 && $plugin['attendancestaff']){
	include_once($PATH_WRT_ROOT."includes/libstaffattend2.php");
	include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
	
	echo '================ Staff Attendance V3 - Fix non-confirmed leave records that with reason deafult waived =============<br>';
	
	$debug_output = false;
	$fix = true;
	
	$RecordType = CARD_STATUS_OUTGOING.",".CARD_STATUS_HOLIDAY;
	$RecordTypeAssoc = array();
	$RecordTypeAssoc[CARD_STATUS_OUTGOING] = "Outing";
	$RecordTypeAssoc[CARD_STATUS_HOLIDAY] = "Holiday";
	
	$style = "style='border:1px solid black'";
	
	$sql = "show tables like 'CARD_STAFF_ATTENDANCE2_DAILY_LOG_%'";
	$DailyLogTables = $li->returnVector($sql);
	
	$totalFixed = 0;
	for ($i=0; $i< sizeof($DailyLogTables); $i++) {	
		$DailyLogTable = $DailyLogTables[$i];
		$temp = explode("_",$DailyLogTable);
		$YEAR = $temp[5];
		$MONTH = $temp[6];
		
		if($debug_output) echo $DailyLogTable."<br>";
		
		// find not yet confirmed dailylog records and profile records that reason is default waived
		$sql = "SELECT 
					u.UserID,
					u.EnglishName,
					DATE_FORMAT(CONCAT('$YEAR-$MONTH-',d.DayNumber),'%Y-%m-%d') as RecordDate,
					d.RecordID as DailylogID,
					p.RecordID as ProfileID,
					IFNULL(d.InWaived,'-') as DailylogWaived,
					IFNULL(p.Waived,'-') as ProfileWaived,
					r.ReasonID,
					r.ReasonText,
					r.DefaultWavie,
					d.RecordType 
				FROM INTRANET_USER as u 
				INNER JOIN $DailyLogTable as d ON u.UserID = d.StaffID 
				INNER JOIN CARD_STAFF_ATTENDANCE2_PROFILE as p ON p.RecordID = d.InAttendanceRecordID 
				INNER JOIN CARD_STAFF_ATTENDANCE3_REASON as r ON r.ReasonID = p.ReasonID AND r.DefaultWavie = '1'
				WHERE u.RecordStatus = 1 AND u.RecordType = 1 
				AND d.DateConfirmed IS NULL 
				AND d.RecordType IN (".$RecordType.") AND d.Duty = 0 
				AND d.InSchoolStatus IN (".$RecordType.") 
				AND (p.Waived IS NULL OR p.Waived <> '1') ";
		
		$result = $li->returnArray($sql);
		
		if($debug_output){
			echo "<table $style><tbody>";
			echo "<tr><td $style>Staff UserID</td><td $style>EnglishName</td><td $style>RecordDate</td><td $style>DailylogID</td><td $style>ProfileID</td><td $style>Dailylog Waived</td><td $style>Profile Waived</td><td $style>ReasonID</td><td $style>Reason</td><td $style>Reason Waive</td><td $style>Leave type</td>";
			if($fix) echo "<td $style>Fix status</td>";
			echo "</tr>";
		}
		
		for($j=0;$j<count($result);$j++){
			if($debug_output){
				echo "<tr>";
				for($k=0;$k<10;$k++){
					echo "<td $style>".htmlspecialchars($result[$j][$k])."</td>";
				}
				echo "<td $style>".$RecordTypeAssoc[$result[$j]['RecordType']]."</td>";
			}
			
			if($fix){
				$sql = "UPDATE $DailyLogTable SET InWaived = 1 WHERE RecordID = ".$result[$j]['DailylogID'];
				$update_dailylog_success = $li->db_db_query($sql);
				$sql = "UPDATE CARD_STAFF_ATTENDANCE2_PROFILE SET Waived = 1 WHERE RecordID = ".$result[$j]['ProfileID'];
				$update_profile_success = $li->db_db_query($sql);
				if($debug_output){
					$fix_status = $update_dailylog_success && $update_profile_success;
					echo "<td $style>".($fix_status?"Success":"<span style='color:red'>Failed</span>")."</td>";
				}
				$totalFixed++;
			}
			if($debug_output) echo "</tr>";
			
		}
		if($debug_output){
			echo "</tbody></table>";
			echo "<br><br>";
		}
		
		ob_flush();
		flush();
	}
	
	echo "$totalFixed records fixed.<br><br>";
	
	# update General Settings - markd the script is executed
	$sql = "insert ignore into GENERAL_SETTING 
						(Module, SettingName, SettingValue, DateInput) 
					values 
						('$ModuleName', 'StaffAttendV3DefaultWaiveLeaveRecords', 1, now())";
	$Result['UpdateGeneralSettings'] = $li->db_db_query($sql);
	// end handle daily log schema change
	
	echo '========== End of Staff Attendance V3 Fix non-confirmed leave records that with reason deafult waived ============<br>';
	
}

?>