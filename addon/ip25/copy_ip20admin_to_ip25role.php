<?
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/role_manage.php");
include_once($intranet_root."/includes/libfilesystem.php");

$li = new libdb();
$rm = new role_manage();

// intranet_opendb();

/*
# Confirmed by Wally, no need to copy the admin to role for eNotice
#############################################
# eNotice
#############################################
if($plugin['notice'])
{
	$RoleName = "eNotice Admin [default]";
	$DefaultRoleNotExists = $rm->Check_Role_Name($RoleName);
	
	$x .= $RoleName . "...";
	
	if($DefaultRoleNotExists)
	{
		include_once($PATH_WRT_ROOT."includes/libnotice.php");
		$lnotice = new libnotice();
		$AdminGroupID = $lnotice->fullAccessGroupID;
		if($AdminGroupID)
		{
			$sql = "SELECT 
						a.UserID 
					FROM 
						INTRANET_USERGROUP as a
						inner join INTRANET_USER as b on (b.UserID=a.UserID)
					WHERE 
						a.GroupID = $AdminGroupID
			";
			$AdminUser = $li->returnVector($sql);
			$AdminUser = trim_array($AdminUser);
			if(sizeof($AdminUser))
			{
				# create role 
				$RoleID = $rm->Create_Role($RoleName);
				
				if($RoleID)
				{
					# set Role access right
					$FunctionName = "eAdmin-eNotice";
					$sql = 'insert into ROLE_RIGHT (RoleID,FunctionName,RightFlag,DateInput,InputBy) 
							values (\''.$RoleID.'\',\''.$FunctionName.'\',1,NOW(),\''.$_SESSION['UserID'].'\')';
					$li->db_db_query($sql);
					
					# copy Admin User in Role
					$CreateRoleResult = $rm->Add_Role_Member($RoleID,$AdminUser);
					if($CreateRoleResult)
						$x .= " is created and assigned ". sizeof($AdminUser) ." user into the role. <br>";
				}
			}
			else
			{
				$x .= "No eNotice Admin User. <br>";	
			}
		}
		else
		{
			$x .= "No eNotice Admin Group setting. <br>";	
		}
	}
	else
	{
		$x .= "eNotice default role is already exists. <br>";	
	}
}
else
{
	$x .= "No eNotice plugin. <br>";	
}
#############################################
# eNotice END
#############################################
*/

#############################################
# eEnrolment
#############################################
if($plugin['eEnrollment'])
{
	$RoleName = "eEnrolment Admin [default]";
	$DefaultRoleNotExists = $rm->Check_Role_Name($RoleName);
	
	$x .= $RoleName . "...";
	
	if($DefaultRoleNotExists)
	{
		# UserLevel 2 <== ADMIN
		$sql = "SELECT 
						a.UserID 
					FROM 
						INTRANET_ENROL_USER_ACL as a
						inner join INTRANET_USER as b on (b.UserID=a.UserID)
					WHERE 
						UserLevel=2
			";
		$AdminUser = $li->returnVector($sql);
		
		if(sizeof($AdminUser))
		{
			# create role 
			$RoleID = $rm->Create_Role($RoleName);
			
			if($RoleID)
			{
				# set Role access right
				$FunctionName = "eAdmin-eEnrolment";
				$sql = 'insert into ROLE_RIGHT (RoleID,FunctionName,RightFlag,DateInput,InputBy) 
						values (\''.$RoleID.'\',\''.$FunctionName.'\',1,NOW(),\''.$_SESSION['UserID'].'\')';
				$li->db_db_query($sql);
				
				# copy Admin User in Role
				$CreateRoleResult = $rm->Add_Role_Member($RoleID,$AdminUser);
				if($CreateRoleResult)
					$x .= "'". $RoleName ."' is created and assigned ". sizeof($AdminUser) ." user into the role. <br>";
			}
		}
		else
		{
			$x .= "No eEnrollment Admin User. <br>";	
		}
	}
	else
	{
		$x .= "eEnrollment default role is already exists. <br>";	
	}
}
else
{
	$x .= "No eEnrolment plugin. <br>";	
}
#############################################
# eEnrolment END
############################################# 

#############################################
# eDiscipline v12
#############################################
if($plugin['Disciplinev12'])
{
	$RoleName = "eDiscipline Admin [default]";
	$DefaultRoleNotExists = $rm->Check_Role_Name($RoleName);
	
	$x .= $RoleName . "...";
	
	if($DefaultRoleNotExists)
	{
		include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
		$ldiscipline = new libdisciplinev12();
		
// 		$AdminUserStr = $ldiscipline->GET_ADMIN_USER();
		$lf = new libfilesystem();
		$AdminUserStr = trim($lf->file_read($intranet_root."/file/disciplinev12/admin_user.txt"));
		
		# check User is exists or not
		$sql = "select UserID from INTRANET_USER where UserID in ($AdminUserStr) and RecordStatus=1";
		$AdminUser = $li->returnVector($sql);
		
		if(sizeof($AdminUser))
		{
			# create role 
			$RoleID = $rm->Create_Role($RoleName);
			
			if($RoleID)
			{
				# set Role access right
				$FunctionName = "eAdmin-eDiscipline";
				$sql = 'insert into ROLE_RIGHT (RoleID,FunctionName,RightFlag,DateInput,InputBy) 
						values (\''.$RoleID.'\',\''.$FunctionName.'\',1,NOW(),\''.$_SESSION['UserID'].'\')';
				$li->db_db_query($sql);
				
				# copy Admin User in Role
				$CreateRoleResult = $rm->Add_Role_Member($RoleID,$AdminUser);
				if($CreateRoleResult)
					$x .= "'". $RoleName ."' is created and assigned ". sizeof($AdminUser) ." user into the role. <br>";
			}
		}
		else
		{
			$x .= "No eDiscipline Admin User. <br>";	
		}
	}
	else
	{
		$x .= "eDiscipline default role is already exists. <br>";	
	}
}
else
{
	$x .= "No eDiscipline plugin. <br>";	
}
#############################################
# eDiscipline v12 END
############################################# 

#############################################
# eReportCard 
#############################################
if($plugin['ReportCard2008'])
{
	$RoleName = "eReportCard Admin [default]";
	$DefaultRoleNotExists = $rm->Check_Role_Name($RoleName);
	
	$x .= $RoleName . "...";
	
	if($DefaultRoleNotExists)
	{
		include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
		$lrc = new libreportcard();
		
		//$AdminUserStr = $lrc->GET_ADMIN_USER();
		$lf = new libfilesystem();
		$AdminUserStr = trim($lf->file_read($intranet_root."/file/reportcard2008/admin_user.txt"));
		
		# check User is exists or not
		$sql = "select UserID from INTRANET_USER where UserID in ($AdminUserStr) and RecordStatus=1";
		$AdminUser = $li->returnVector($sql);
		
		if(sizeof($AdminUser))
		{
			# create role 
			$RoleID = $rm->Create_Role($RoleName);
			
			if($RoleID)
			{
				# set Role access right
				$FunctionName = "eAdmin-eReportCard";
				$sql = 'insert into ROLE_RIGHT (RoleID,FunctionName,RightFlag,DateInput,InputBy) 
						values (\''.$RoleID.'\',\''.$FunctionName.'\',1,NOW(),\''.$_SESSION['UserID'].'\')';
				$li->db_db_query($sql);
				
				# copy Admin User in Role
				$CreateRoleResult = $rm->Add_Role_Member($RoleID,$AdminUser);
				if($CreateRoleResult)
					$x .= "'". $RoleName ."' is created and assigned ". sizeof($AdminUser) ." user into the role. <br>";
			}
		}
		else
		{
			$x .= "No eReportCard Admin User. <br>";	
		}
	}
	else
	{
		$x .= "eReportCard default role is already exists. <br>";	
	}
}
else
{
	$x .= "No eReportCard plugin. <br>";	
}
#############################################
# eReportCard END
############################################# 

#############################################
# eInventory 
#############################################
if($plugin['Inventory'])
{
	$RoleName = "eInventory Admin [default]";
	$DefaultRoleNotExists = $rm->Check_Role_Name($RoleName);
	
	$x .= $RoleName . "...";
	
	if($DefaultRoleNotExists)
	{
		include_once($PATH_WRT_ROOT."includes/libinventory.php");
		$linventory = new libinventory();
		
		//$AdminUserStr = $linventory->GET_ADMIN_USER();
		$lf = new libfilesystem();
		$AdminUserStr = trim($lf->file_read($intranet_root."/file/inventory/admin_user.txt"));
		
		# check User is exists or not
		$sql = "select UserID from INTRANET_USER where UserID in ($AdminUserStr) and RecordStatus=1";
		$AdminUser = $li->returnVector($sql);
		
		if(sizeof($AdminUser))
		{
			# create role 
			$RoleID = $rm->Create_Role($RoleName);
			
			if($RoleID)
			{
				# set Role access right
				$FunctionName = "eAdmin-eInventory";
				$sql = 'insert into ROLE_RIGHT (RoleID,FunctionName,RightFlag,DateInput,InputBy) 
						values (\''.$RoleID.'\',\''.$FunctionName.'\',1,NOW(),\''.$_SESSION['UserID'].'\')';
				$li->db_db_query($sql);
				
				# copy Admin User in Role
				$CreateRoleResult = $rm->Add_Role_Member($RoleID,$AdminUser);
				if($CreateRoleResult)
					$x .= "'". $RoleName ."' is created and assigned ". sizeof($AdminUser) ." user into the role. <br>";
			}
		}
		else
		{
			$x .= "No eInventory Admin User. <br>";	
		}
	}
	else
	{
		$x .= "eInventory default role is already exists. <br>";	
	}
}
else
{
	$x .= "No eInventory plugin. <br>";	
}
#############################################
# eInventory END
############################################# 
               
#############################################
# Polling
#############################################
$RoleName = "ePolling Admin [default]";
$DefaultRoleNotExists = $rm->Check_Role_Name($RoleName);

$x .= $RoleName . "...";

if($DefaultRoleNotExists)
{
	include_once($PATH_WRT_ROOT."includes/libpolling.php");
	$lpolling = new libpolling();
	
	//$AdminUserStr = $lpolling->GET_ADMIN_USER();
	$lf = new libfilesystem();
	$AdminUserStr = trim($lf->file_read($intranet_root."/file/polling/admin_user.txt"));
	
	# check User is exists or not
	$sql = "select UserID from INTRANET_USER where UserID in ($AdminUserStr) and RecordStatus=1";
	$AdminUser = $li->returnVector($sql);
	
	if(sizeof($AdminUser))
	{
		# create role 
		$RoleID = $rm->Create_Role($RoleName);
		
		if($RoleID)
		{
			# set Role access right
			$FunctionName = "eAdmin-ePolling";
			$sql = 'insert into ROLE_RIGHT (RoleID,FunctionName,RightFlag,DateInput,InputBy) 
					values (\''.$RoleID.'\',\''.$FunctionName.'\',1,NOW(),\''.$_SESSION['UserID'].'\')';
			$li->db_db_query($sql);
			
			# copy Admin User in Role
			$CreateRoleResult = $rm->Add_Role_Member($RoleID,$AdminUser);
			if($CreateRoleResult)
				$x .= "'". $RoleName ."' is created and assigned ". sizeof($AdminUser) ." user into the role. <br>";
		}
	}
	else
	{
		$x .= "No ePolling Admin User. <br>";	
	}
}
else
{
	$x .= "ePolling default role is already exists. <br>";	
}

#############################################
# Polling END
############################################# 

#############################################
# School News
#############################################
$RoleName = "School News Admin [default]";
$DefaultRoleNotExists = $rm->Check_Role_Name($RoleName);

$x .= $RoleName . "...";

if($DefaultRoleNotExists)
{
	include_once("../../includes/libannounce.php");
	$la = new libannounce();
	
	//$AdminUserStr = $la->GET_ADMIN_USER();
	$lf = new libfilesystem();
	$AdminUserStr = trim($lf->file_read($intranet_root."/file/announcement/admin_user.txt"));
	
	# check User is exists or not
	$sql = "select UserID from INTRANET_USER where UserID in ($AdminUserStr) and RecordStatus=1";
	$AdminUser = $li->returnVector($sql);
	
	if(sizeof($AdminUser))
	{
		# create role 
		$RoleID = $rm->Create_Role($RoleName);
		
		if($RoleID)
		{
			# set Role access right
			$FunctionName = "other-schoolNews";
			$sql = 'insert into ROLE_RIGHT (RoleID,FunctionName,RightFlag,DateInput,InputBy) 
					values (\''.$RoleID.'\',\''.$FunctionName.'\',1,NOW(),\''.$_SESSION['UserID'].'\')';
			$li->db_db_query($sql);
			
			# copy Admin User in Role
			$CreateRoleResult = $rm->Add_Role_Member($RoleID,$AdminUser);
			if($CreateRoleResult)
				$x .= "'". $RoleName ."' is created and assigned ". sizeof($AdminUser) ." user into the role. <br>";
		}
	}
	else
	{
		$x .= "No School News Admin User. <br>";	
	}
}
else
{
	$x .= "School News default role is already exists. <br>";	
}
#############################################
# School News END
############################################# 

/*
# Confirmed by Wally, no need to copy the admin to role for eCircular
#############################################
# eCircular 
#############################################
if($special_feature['circular'])
{
	$RoleName = "eCircular Admin [default]";
	$DefaultRoleNotExists = $rm->Check_Role_Name($RoleName);
	
	$x .= $RoleName . "...";
	
	if($DefaultRoleNotExists)
	{
		include_once($PATH_WRT_ROOT."includes/libcircular.php");
		$lcircular = new libcircular();
		$admin_id_type = $lcircular->admin_id_type;
		
		# assume both admin type => role
		$sql = "SELECT 
						a.UserID 
					FROM 
						INTRANET_ADMIN_USER as a
						inner join INTRANET_USER as b on (b.UserID=a.UserID)
					WHERE 
						a.RecordType = $admin_id_type
			";
		$AdminUser = $li->returnVector($sql);
		
		if(sizeof($AdminUser))
		{
			# create role 
			$RoleID = $rm->Create_Role($RoleName);
			
			if($RoleID)
			{
				# set Role access right
				$FunctionName = "eAdmin-eCircular";
				$sql = 'insert into ROLE_RIGHT (RoleID,FunctionName,RightFlag,DateInput,InputBy) 
						values (\''.$RoleID.'\',\''.$FunctionName.'\',1,NOW(),\''.$_SESSION['UserID'].'\')';
				$li->db_db_query($sql);
				
				# copy Admin User in Role
				$CreateRoleResult = $rm->Add_Role_Member($RoleID,$AdminUser);
				if($CreateRoleResult)
					$x .= "'". $RoleName ."' is created and assigned ". sizeof($AdminUser) ." user into the role. <br>";
			}
		}
		else
		{
			$x .= "No eCircular Admin User. <br>";	
		}
	}
	else
	{
		$x .= "eCircular default role is already exists. <br>";	
	}
}
else
{
	$x .= "No eCircular plugin. <br>";	
}
#############################################
# eCircular END
############################################# 
*/

#############################################
# eSports 
#############################################
if($plugin['Sports'] || $plugin['swimming_gala'] )
{
	# Sport Day and Swimming Gala are use the same Role
	
	$RoleName = "eSports Admin [default]";
	$DefaultRoleNotExists = $rm->Check_Role_Name($RoleName);
	
	$x .= $RoleName . "...";
	
	if($DefaultRoleNotExists)
	{
		if($plugin['Sports'])
		{
			$sql = "SELECT 
						a.AdminUserID 
					FROM 
						SPORTS_ADMIN_USER_ACL as a
						inner join INTRANET_USER as b on (b.UserID=a.AdminUserID)
					WHERE 
						a.UserType = 'Admin'
			";
			$AdminUser = $li->returnVector($sql);
		}
		
		if($plugin['swimming_gala'])
		{
			$sql = "SELECT 
						a.AdminUserID 
					FROM 
						SWIMMINGGALA_ADMIN_USER_ACL as a
						inner join INTRANET_USER as b on (b.UserID=a.AdminUserID)
					WHERE 
						a.UserType = 'Admin'
			";
			$AdminUser2 = $li->returnVector($sql);
		}
		$AdminUser = array_unique(array_merge((array)$AdminUser, (array)$AdminUser2));
		
		if(sizeof($AdminUser))
		{
			# create role 
			$RoleID = $rm->Create_Role($RoleName);
			
			if($RoleID)
			{
				# set Role access right
				$FunctionName = "eAdmin-eSportsAdmin";
				$sql = 'insert into ROLE_RIGHT (RoleID,FunctionName,RightFlag,DateInput,InputBy) 
						values (\''.$RoleID.'\',\''.$FunctionName.'\',1,NOW(),\''.$_SESSION['UserID'].'\')';
				$li->db_db_query($sql);
				
				# copy Admin User in Role
				$CreateRoleResult = $rm->Add_Role_Member($RoleID,$AdminUser);
				if($CreateRoleResult)
					$x .= "'". $RoleName ."' is created and assigned ". sizeof($AdminUser) ." user into the role. <br>";
			}
		}
		else
		{
			$x .= "No eSports Admin User. <br>";	
		}
	}
	else
	{
		$x .= "eSports default role is already exists. <br>";	
	}
}
else
{
	$x .= "No eSports plugin. <br>";	
}
#############################################
# eSports END
############################################# 

// intranet_closedb();
?>