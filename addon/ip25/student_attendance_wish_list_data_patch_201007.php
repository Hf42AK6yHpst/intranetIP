<?
// 
@SET_TIME_LIMIT(0);
$PATH_WRT_ROOT = "../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");

intranet_opendb();

$lgs = new libgeneralsettings();

$ModuleName = "InitSetting";
$GSary = $lgs->Get_General_Setting($ModuleName);

if ($GSary['StudentAttendanceWishListDataPatch201007'] != 1 && $plugin['attendancestudent']) {
	echo 'Start on StudentAttendanceWishListDataPatch201007<br>';
	
	unset($Result);
	include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
	$lc = new libcardstudentattend2();
	
	$sql = "show tables like 'CARD_STUDENT_DAILY_LOG_%'";
	$DailyLog = $lc->returnVector($sql);
	
	for ($i=0; $i< sizeof($DailyLog); $i++) {
		$sql = 'alter table '.$DailyLog[$i].' add InputBy int(11) default NULL after DateInput';
		$lc->db_db_query($sql);
		$sql = 'alter table '.$DailyLog[$i].' add ModifyBy int(11) default NULL after DateModified';
		$lc->db_db_query($sql);
		$sql = 'alter table '.$DailyLog[$i].' add PMConfirmedUserID int(11) default NULL after IsConfirmed';
		$lc->db_db_query($sql);
		$sql = 'alter table '.$DailyLog[$i].' add PMIsConfirmed int default NULL after PMConfirmedUserID';
		$lc->db_db_query($sql);
	}
	unset($DailyLog);
	
	// add field the store if eNotice sent for a absent record
	$sql = 'alter table CARD_STUDENT_PROFILE_RECORD_REASON add eNoticeID int(11) default NULL after GM_RecordID_Str';
	$lc->db_db_query($sql);
	
	// create table for late/ ����/ �m��/ �а� session
	$sql = 'CREATE TABLE CARD_STUDENT_STATUS_SESSION_COUNT (
					  RecordID int(11) NOT NULL auto_increment,
					  RecordDate date NOT NULL,
					  StudentID int(11) default NULL,
					  DayType int(2) default NULL,
					  LateSession FLOAT default NULL,
					  RequestLeaveSession FLOAT default NULL,
					  PlayTruantSession FLOAT default NULL,
					  OfficalLeaveSession FLOAT default NULL,
					  AbsentSession FLOAT default NULL,
					  DateInput datetime default NULL,
					  InputBy int(11) default NULL,
					  DateModify datetime default NULL,
					  ModifyBy int(11) default NULL,
					  PRIMARY KEY  (RecordID),
					  UNIQUE KEY DateStudentDayType (RecordDate,StudentID,DayType),
					  KEY RecordDate (RecordDate),
					  KEY StudentID (StudentID),
					  KEY DayType (DayType)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8';
	$lc->db_db_query($sql);
	
	// set all preset outing record to AM and PM
	$sql = 'select 
						OutingID 
					from 
						CARD_STUDENT_OUTING 
					where 
						DayType IS NULL or DayType = \'\'';
	$OutingRecords = $lc->returnVector($sql);
	if (sizeof($OutingRecords) > 0) {
		$sql = 'update CARD_STUDENT_OUTING set 
							DayType = \''.PROFILE_DAY_TYPE_AM.'\' 
						where 
							OutingID in ('.implode(',',$OutingRecords).')';
		$Result['SetOutingDayType'] = $lc->db_db_query($sql);
		
		$sql = 'select 
							UserID,
							RecordDate,
							OutTime,
							BackTime,
							Location,
							FromWhere,
							Objective,
							PIC,
							Detail 
						from 
							CARD_STUDENT_OUTING 
						where 
							OutingID in ('.implode(',',$OutingRecords).')';
		$OutingDetails = $lc->returnArray($sql);
		
		$sql = 'insert into CARD_STUDENT_OUTING (
							UserID,
							RecordDate,
							DayType,
							OutTime,
							BackTime,
							Location,
							FromWhere,
							Objective,
							PIC,
							Detail,
							DateInput,
							DateModified
							)
						values ';
		for ($i=0; $i< sizeof($OutingDetails); $i++) {
			$sql .= "(
								'".$OutingDetails[$i]['UserID']."',
								'".$OutingDetails[$i]['RecordDate']."',
								'".PROFILE_DAY_TYPE_PM."',
								'".$OutingDetails[$i]['OutTime']."',
								'".$OutingDetails[$i]['BackTime']."',
								'".$OutingDetails[$i]['Location']."',
								'".$OutingDetails[$i]['FromWhere']."',
								'".$OutingDetails[$i]['Objective']."',
								'".$OutingDetails[$i]['PIC']."',
								'".$OutingDetails[$i]['Detail']."',
								NOW(),
								NOW() 
							),";
		}
		$sql = substr($sql,0,-1);
		$Result['InsertPMOutingRecord'] = $lc->db_db_query($sql);
	}
	unset($OutingRecords);
	
	# update General Settings - mark the script is executed
	$sql = "insert ignore into GENERAL_SETTING 
						(Module, SettingName, SettingValue, DateInput) 
					values 
						('$ModuleName', 'StudentAttendanceWishListDataPatch201007', 1, now())";
	$Result['UpdateGeneralSettings'] = $lc->db_db_query($sql);
	
	echo 'end of StudentAttendanceWishListDataPatch201007';
}
?>