<?php
// editing by 
@SET_TIME_LIMIT(0);
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
intranet_opendb();

$li = new libdb();
$li->db = $intranet_db;
$lgs = new libgeneralsettings();

$ModuleName = "InitSetting";
$GSary = $lgs->Get_General_Setting($ModuleName);

if($plugin['imail_gamma'] === true && $GSary['iMailGammaTransferPreference']!=1){
	echo "iMail Gamma - Start transferring intranet iMail preferences to iMail Gamma<br />";
	
	$sql = "CREATE TABLE IF NOT EXISTS MAIL_PREFERENCE (
				MailBoxName varchar(255) NOT NULL,
				DisplayName varchar(255) default NULL,
				ReplyEmail varchar(255) default NULL,
				Signature mediumtext,
				ForwardedEmail mediumtext,
				ForwardKeepCopy int(11) default NULL,
				DaysInSpam int(11) default NULL,
				DaysInTrash int(11) default NULL,
				SkipCheckEmail int(11) default NULL,
				AutoReplyTxt text,
				AutoSaveInterval int(3) default 0,
				GmailMode int(3) default 1,
				RecordType int(11) default NULL,
				RecordStatus int(11) default NULL,
				DateInput datetime default NULL,
				DateModified datetime default NULL,
				PRIMARY KEY (MailBoxName),
				INDEX MailBoxNameIndex (MailBoxName) 
			) ENGINE=InnoDB DEFAULT CHARSET=utf8 ";
	$Result['CreateTable'] = $li->db_db_query($sql);
	if($Result['CreateTable']){
		echo "Create table MAIL_PREFERENCE successfully<br />";
		$MailSubfix = "@".$SYS_CONFIG['Mail']['UserNameSubfix'];
		
		$sql = "SELECT 
					u.ImapUserEmail,
					u.UserLogin,
					p.UserID,
					p.DisplayName,
					p.ReplyEmail,
					p.Signature,
					p.ForwardedEmail,
					p.ForwardKeepCopy,
					p.DaysInSpam,
					p.DaysInTrash,
					p.SkipCheckEmail,
					p.AutoReplyTxt,
					p.AutoSaveInterval,
					p.GmailMode,
					p.RecordType,
					p.RecordStatus,
					p.DateInput,
					p.DateModified 
				FROM INTRANET_IMAIL_PREFERENCE as p 
				INNER JOIN INTRANET_USER as u ON p.UserID = u.UserID 
				";
		$res = $li->returnArray($sql);
		$size = sizeof($res);
		$insert_values = array();
		for($i=0;$i<$size;$i++){
			list($email,$user_login,$user_id,$display_name,$reply_email,$signature,$forwarded_email,$forward_keep_copy,$days_in_spam,$days_in_trash
				,$skip_check_email,$auto_reply_txt,$auto_save_interval,$gmail_mode,$record_type,$record_status,$date_input,$date_modified)=$res[$i];
			if(trim($email)==""){
				$email = $user_login.$MailSubfix;
			}
			$insert_values[] = "('$email','".$li->Get_Safe_Sql_Query($display_name)."','$reply_email','".$li->Get_Safe_Sql_Query($signature)."','$forwarded_email','$forward_keep_copy','$days_in_spam','$days_in_trash','$skip_check_email','".$li->Get_Safe_Sql_Query($auto_reply_txt)."','$auto_save_interval','$gmail_mode','$record_type','$record_status','$date_input','$date_modified')";
		}
		if(sizeof($insert_values)>0){
			$sql = "INSERT INTO MAIL_PREFERENCE (MailBoxName,DisplayName,ReplyEmail,Signature,ForwardedEmail,ForwardKeepCopy,DaysInSpam,DaysInTrash,SkipCheckEmail,AutoReplyTxt,AutoSaveInterval,GmailMode,RecordType,RecordStatus,DateInput,DateModified) VALUES ";
			$sql .= implode(",",$insert_values);
			$Result['InsertPreference'] = $li->db_db_query($sql);
			if($Result['InsertPreference']) echo "Insert Preference(s) successfully<br />";
			else echo "Failed to insert preference(s)<br />";
		}
	}else{
		echo "Failed to create table MAIL_PREFERENCE<br />";
	}
	
	# update General Settings - markd the script is executed
	$sql = "insert ignore into GENERAL_SETTING 
						(Module, SettingName, SettingValue, DateInput) 
					values 
						('$ModuleName', 'iMailGammaTransferPreference', 1, now())";
	$Result['UpdateGeneralSettings'] = $li->db_db_query($sql);
	
	echo "iMail Gamma - Transfer intranet iMail preferences to iMail Gamma ended<br />";
}

?>