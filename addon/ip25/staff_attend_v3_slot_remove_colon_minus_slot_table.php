<?
# using: kenneth chung
@SET_TIME_LIMIT(0);
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
intranet_opendb();

$li = new libdb();
$li->db = $intranet_db;
$lgs = new libgeneralsettings();

$ModuleName = "InitSetting";
$GSary = $lgs->Get_General_Setting($ModuleName);

if ($GSary['StaffAttendV3SlotNameRemoveColonMinusSlotTable'] != 1 && $module_version['StaffAttendance'] == 3.0 && $plugin['attendancestaff']) {
	echo 'staff v3:fix migration slot name<br>';
	
	$sql = "update CARD_STAFF_ATTENDANCE3_SLOT set 
							SlotName = REPLACE(REPLACE(SlotName,'-','to'),':','')";
		$Result['RenameSlotTable'] = $li->db_db_query($sql);	
	
	# update General Settings - markd the script is executed
	$sql = "insert ignore into GENERAL_SETTING 
						(Module, SettingName, SettingValue, DateInput) 
					values 
						('$ModuleName', 'StaffAttendV3SlotNameRemoveColonMinusSlotTable', 1, now())";
	$Result['UpdateGeneralSettings'] = $li->db_db_query($sql);
	
	//debug_r($Result);
	
	echo 'staff v3:fix migration slot name ended<br>';
}
?>