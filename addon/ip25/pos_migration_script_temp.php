<?
# using:

@SET_TIME_LIMIT(0);
$POS_PATH_WRT_ROOT = ($PATH_WRT_ROOT!="") ? $PATH_WRT_ROOT : "../../";
include_once($POS_PATH_WRT_ROOT."includes/global.php");
include_once($POS_PATH_WRT_ROOT."includes/libdb.php");
include_once($POS_PATH_WRT_ROOT."includes/libgeneralsettings.php");

$li->db = $intranet_db;
$lgs = new libgeneralsettings();

$ModuleName = "InitSetting";
$GSary = $lgs->Get_General_Setting($ModuleName);

if ($GSary['POSMigrationScript'] != 1 && $plugin['ePOS']) {
	echo 'POS migrate started... <BR>';
	// db schema change start
	$sql = 'alter table PAYMENT_PURCHASE_DETAIL_RECORD add ItemID int(11) after TransactionLogID';
	$CreateTableResult['Alter PAYMENT_PURCHASE_DETAIL_RECORD'] = $li->db_db_query($sql);
	
	$sql = 'CREATE TABLE POS_TRANSACTION (
					  LogID int(11) NOT NULL,
					  InvoiceNumber varchar(50) NOT NULL,
					  ProcessBy int(11) DEFAULT NULL,
					  ProcessDate datetime DEFAULT NULL,
					  VoidBy int(11) DEFAULT NULL,
					  VoidDate Datetime DEFAULT NULL,
					  VoidLogID int(11) DEFAULT NULL,
					  Remark varchar(255) DEFAULT NULL,
					  PRIMARY KEY (LogID),
					  UNIQUE KEY InvoiceNumber (InvoiceNumber)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8';
	$CreateTableResult['Create POS_TRANSACTION'] = $li->db_db_query($sql);
	
	$sql = 'CREATE TABLE POS_ITEM_CATEGORY (
					  CategoryID int(11) NOT NULL auto_increment,
					  CategoryCode varchar(20) NOT NULL,
					  CategoryName varchar(255) NOT NULL,
					  PhotoExt varchar(10) default NULL,
					  Sequence int(3) default NULL,
					  RecordStatus int(2) default NULL,
					  DateInput Datetime DEFAULT NULL,
					  InputBy int(11) DEFAULT NULL,
					  DateModify Datetime DEFAULT NULL,
					  ModifyBy int(11) DEFAULT NULL,
					  PRIMARY KEY  (CategoryID),
					  UNIQUE KEY CategoryCode (CategoryCode),
					  UNIQUE KEY CategoryName (CategoryName)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8';
	$CreateTableResult['Create POS_ITEM_CATEGORY'] = $li->db_db_query($sql);
	
	$sql = 'CREATE TABLE POS_ITEM (
					  ItemID int(11) NOT NULL auto_increment,
					  CategoryID int(11) NOT NULL,
					  Barcode varchar(100) DEFAULT NULL,
					  ItemName varchar(255) default NULL,
					  UnitPrice double(20,2) default NULL,
					  PhotoExt varchar(10) default NULL,
					  ItemCount int(20) default NULL,
					  ItemSalesTotal int(20) default NULL,
					  Sequence int(3) default NULL,
					  RecordStatus int(2) default NULL,
					  DateInput Datetime DEFAULT NULL,
					  InputBy int(11) DEFAULT NULL,
					  DateModify Datetime DEFAULT NULL,
					  ModifyBy int(11) DEFAULT NULL,
					  InventoryDateModify DATETIME DEFAULT NULL,
					  InventoryModifyBy int(11) DEFAULT NULL,
					  PRIMARY KEY (ItemID),
					  UNIQUE KEY ItemName (ItemName),
					  UNIQUE KEY Barcode (Barcode)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8';
	$CreateTableResult['Create POS_ITEM'] = $li->db_db_query($sql);
	
	$sql = 'CREATE TABLE POS_ITEM_HEALTH_INGREDIENT (
					  ItemID int(11) NOT NULL ,
					  HealthIngredientID int(11) NOT NULL,
					  IngredientInTake double(20,2) default NULL,
					  PRIMARY KEY (ItemID,HealthIngredientID)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8';
	$CreateTableResult['Create POS_ITEM_HEALTH_INGREDIENT'] = $li->db_db_query($sql);
	
	$sql = 'CREATE TABLE POS_HEALTH_INGREDIENT (
					  HealthIngredientID int(11) NOT NULL auto_increment,
					  HealthIngredientCode varchar(50) NOT NULL,
					  HealthIngredientName varchar(255) NOT NULL,
					  UnitName varchar(10) DEFAULT NULL,
					  StandardInTakePerDay double(20,2) default NULL,
					  DisplayOrder int(11) DEFAULT NULL,
					  RecordStatus int(2) DEFAULT NULL,
					  PRIMARY KEY (HealthIngredientID),
					  UNIQUE KEY HealthIngredientCode (HealthIngredientCode),
					  UNIQUE KEY HealthIngredientName (HealthIngredientName)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8';
	$CreateTableResult['Create POS_HEALTH_INGREDIENT'] = $li->db_db_query($sql);
					
	$sql = 'CREATE TABLE POS_ITEM_LOG (
					  ItemLogID int(11) NOT NULL auto_increment,
					  ItemID int(11) DEFAULT NULL,
					  LogType int(3) DEFAULT NULL,
					  RefLogID int(11) default NULL,
					  Amount int(5) DEFAULT NULL,
					  CountAfter int(20) DEFAULT NULL,
					  Remark varchar(255) DEFAULT NULL,
					  DateInput Datetime DEFAULT NULL,
					  InputBy int(11) DEFAULT NULL,
					  DateModify Datetime DEFAULT NULL,
					  ModifyBy int(11) DEFAULT NULL,
					  PRIMARY KEY (ItemLogID),
					  INDEX ItemID (ItemID)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8';
	$CreateTableResult['Create POS_ITEM_LOG'] = $li->db_db_query($sql);

	$sql = 'CREATE TABLE POS_TERMINAL_PHOTO_SYNC_HISTORY (
					  RecordID int(11) NOT NULL auto_increment,
					  TerminalID int(11) default NULL,
					  RequestDate datetime default NULL,
					  RequestBy int(11) default NULL,
					  PRIMARY KEY  (RecordID)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8';
	$CreateTableResult['Create POS_TERMINAL_PHOTO_SYNC_HISTORY'] = $li->db_db_query($sql);
					
	$sql = 'CREATE TABLE POS_TERMINAL_SITE_TEMPLATE (
					  TemplateID int(11) NOT NULL auto_increment,
					  TemplateName varchar(255) default NULL,
					  InputDate datetime default NULL,
					  InputBy int(11) default NULL,
					  ModifyDate datetime default NULL,
					  ModifyBy int(11) default NULL,
					  PRIMARY KEY (TemplateID),
					  UNIQUE KEY TemplateName (TemplateName)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8';
	$CreateTableResult['Create POS_TERMINAL_SITE_TEMPLATE'] = $li->db_db_query($sql);
					
	$sql = 'CREATE TABLE POS_TERMINAL_CATEGORY_LINKAGE (
					  TemplateID int(11) NOT NULL default \'0\',
					  CategoryID int(11) NOT NULL default \'0\',
					  DateInput datetime default NULL,
					  InputBy int(11) default NULL,
					  PRIMARY KEY  (TemplateID,CategoryID)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8';
	$CreateTableResult['Create POS_TERMINAL_CATEGORY_LINKAGE'] = $li->db_db_query($sql);
	
	$sql = 'CREATE TABLE POS_TERMINAL_INFO (
					  TerminalID int(11) NOT NULL auto_increment,
					  SiteName varchar(255) default NULL,
					  MacAddress varchar(20) default NULL,
					  IPAddress varchar(50) default NULL,
					  LastConnected datetime default NULL,
					  LastPhotoSync datetime default NULL,
					  LastPhotoRequest datetime default NULL,
					  LastRequestBy int(11) default NULL,
					  TemplateID int(11) default NULL,
					  InputDate datetime default NULL,
					  InputBy int(11) default NULL,
					  ModifyDate datetime default NULL,
					  ModifyBy int(11) default NULL,
					  PRIMARY KEY  (TerminalID),
					  UNIQUE KEY SiteName (SiteName),
					  UNIQUE KEY MacAddress (MacAddress)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8';
	$CreateTableResult['Create POS_TERMINAL_INFO'] = $li->db_db_query($sql);
	// db schema change end
	
	// Process Migration on Item
	// Create first Item Category for client
	$sql = 'insert into POS_ITEM_CATEGORY (
						CategoryCode,
						CategoryName,
						Sequence,
						RecordStatus,
						DateInput
						)
					values (
						\'Code001\',
						\'Category 1\',
						\'1\',
						\'1\',
						NOW()
						)';
	$Result['InsertDummyItemCat'] = $li->db_db_query($sql);
	$CatID = $li->db_insert_id();
	
	// get all item name from current transactions detail
	$sql = 'select 
						ItemName, 
						MAX(DateInput) as LastInput,
						Count(1) SalesTotal
					from 
						PAYMENT_PURCHASE_DETAIL_RECORD 
					group by 
						ItemName';
	$ItemList = $li->returnArray($sql);
	
	for ($i=0; $i< sizeof($ItemList); $i++) {
		// get the latest unit price
		$sql = 'select 
							ItemSubTotal 
						from 
							PAYMENT_PURCHASE_DETAIL_RECORD 
						where 
							ItemName = \''.$li->Get_Safe_Sql_Query($ItemList[$i]['ItemName']).'\' 
							and 
							DateInput = \''.$ItemList[$i]['LastInput'].'\' 
						Limit 
							1';
		$UnitPrice = $li->returnVector($sql);
		
		// insert the items to the new table structure
		$sql = 'insert into POS_ITEM (
							CategoryID,
							ItemName,
							UnitPrice,
							ItemSalesTotal,
							RecordStatus,
							DateInput, 
							Sequence
							)
						values (
							\''.$CatID.'\',
							\''.$li->Get_Safe_Sql_Query($ItemList[$i]['ItemName']).'\',
							\''.$UnitPrice[0].'\',
							\''.$ItemList[$i]['SalesTotal'].'\',
							\'1\',
							NOW(),
							\'1\'
							)';
		$Result['InsertItem:'.$ItemList[$i]['ItemName']] = $li->db_db_query($sql);
		$ItemID = $li->db_insert_id();
		
		// update the item ID back to old purchase detail records
		$sql = 'update PAYMENT_PURCHASE_DETAIL_RECORD set 
							ItemID = \''.$ItemID.'\' 
						where 
							ItemName = \''.$li->Get_Safe_Sql_Query($ItemList[$i]['ItemName']).'\'';
		$Result['UpdateItemDetail:'.$ItemList[$i]['ItemName']] = $li->db_db_query($sql);
	}
	// end process Item Migration
	
	// Process Transaction Migration
	$sql = 'select 
						dr.TransactionLogID,
						dr.InvoiceNumber,
						dr.DateInput,
            l.StudentID
					from 
						PAYMENT_PURCHASE_DETAIL_RECORD dr 
						inner join 
						PAYMENT_OVERALL_TRANSACTION_LOG l 
						on dr.TransactionLogID = l.LogID 
					Group By 
						TransactionLogID,InvoiceNumber';
	$TransactionLogIDs = $li->returnArray($sql);
	
	for ($i=0; $i< sizeof($TransactionLogIDs); $i++) {
		$sql = 'insert into POS_TRANSACTION (
							LogID,
							InvoiceNumber, 
							ProcessBy,
							ProcessDate
							)
						values (
							\''.$TransactionLogIDs[$i]['TransactionLogID'].'\',
							\''.$li->Get_Safe_Sql_Query($TransactionLogIDs[$i]['InvoiceNumber']).'\',
							\''.$TransactionLogIDs[$i]['StudentID'].'\',
							\''.$TransactionLogIDs[$i]['DateInput'].'\'
							)';
		//debug_r($sql);
		$Result['InsertToPOS_TRANSACTION:'.$TransactionLogIDs[$i]['TransactionLogID']] = $li->db_db_query($sql);
	}
	// End Transaction Migration

	# update General Settings - markd the script is executed
	$sql = "insert ignore into GENERAL_SETTING 
						(Module, SettingName, SettingValue, DateInput) 
					values 
						('$ModuleName', 'POSMigrationScript', 1, now())";
	$Result['UpdateGeneralSettings'] = $li->db_db_query($sql);
	
	echo 'POS Migrated!!<br><br>';
}
?>