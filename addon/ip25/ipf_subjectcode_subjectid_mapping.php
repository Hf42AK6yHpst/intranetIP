<?php
// editing by 
@SET_TIME_LIMIT(0);
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
intranet_opendb();

$li = new libdb();
$li->db = $intranet_db;
$lgs = new libgeneralsettings();

$ModuleName = "InitSetting";
$GSary = $lgs->Get_General_Setting($ModuleName);
$LogOutput = '';

if ($GSary['iPf_SubjectCode_SubjectID_Mapping'] != 1 && $plugin['iPortfolio']) {
	$LogOutput .= "iPortfolio - SubjectCode SubjectID Mapping Data Patch [Start] ".date("Y-m-d H:i:s")."<br><br>\r\n\r\n";
	
	# backup tables first
	$ASSESSMENT_STUDENT_SUBJECT_RECORD_BAK = $eclass_db.".ASSESSMENT_STUDENT_SUBJECT_RECORD_BAK_FOR_SUBJECT";
	$ASSESSMENT_STUDENT_SUBJECT_RECORD = $eclass_db.".ASSESSMENT_STUDENT_SUBJECT_RECORD";
	$sql = "Create Table $ASSESSMENT_STUDENT_SUBJECT_RECORD_BAK Select * From $ASSESSMENT_STUDENT_SUBJECT_RECORD";
	$SuccessArr['CreateBackUp'] = $li->db_db_query($sql);
	
	
	# get SubjectShortName SubjectID Mapping
	// Main Subject
	$sql = "Select
					RecordID,
					EN_SNAME,
					CH_SNAME
			From
					ASSESSMENT_SUBJECT 
			Where
					RecordStatus = 1
					And
					(CMP_CODEID Is Null Or CMP_CODEID = '')
			Order By
					CODEID
			";
	$MainSubjectArr = $li->returnArray($sql, 2);
	$MainSubjectShortNameEnIDAssoArr = BuildMultiKeyAssoc($MainSubjectArr, 'EN_SNAME', $IncludedDBField=array('RecordID'), $SingleValue=1, $BuildNumericArray=0);
	$MainSubjectShortNameChIDAssoArr = BuildMultiKeyAssoc($MainSubjectArr, 'CH_SNAME', $IncludedDBField=array('RecordID'), $SingleValue=1, $BuildNumericArray=0);
	
	// Component Subject
	$sql = "Select
					cmp.RecordID as SubjectComponentID,
					cmp.EN_SNAME,
					cmp.CH_SNAME,
					main.RecordID as MainSubjectID
			From
					ASSESSMENT_SUBJECT as cmp
					Inner Join
					ASSESSMENT_SUBJECT as main On (cmp.CODEID = main.CODEID)
			Where
					(cmp.CMP_CODEID Is Not NULL And cmp.CMP_CODEID != '')
					And (main.CMP_CODEID Is Null Or main.CMP_CODEID = '')
					And cmp.RecordStatus = 1
					And main.RecordStatus = 1 
			";
	$SubjectComponentInfoArr = $li->returnArray($sql);
	$numOfSubjectComponent = count($SubjectComponentInfoArr);
	
	$SubjectComponentShortNameEnIDAssoArr = array();
	$SubjectComponentShortNameChIDAssoArr = array();
	for ($i=0; $i<$numOfSubjectComponent; $i++)
	{
		$thisSubjectComponentID = $SubjectComponentInfoArr[$i]['SubjectComponentID'];
		$thisEN_SNAME 			= trim($SubjectComponentInfoArr[$i]['EN_SNAME']);
		$thisCH_SNAME 			= trim($SubjectComponentInfoArr[$i]['CH_SNAME']);
		$thisMainSubjectID 		= $SubjectComponentInfoArr[$i]['MainSubjectID'];
		
		$thisKey = $thisMainSubjectID.'_'.$thisEN_SNAME;
		$SubjectComponentShortNameEnIDAssoArr[$thisKey] = $thisSubjectComponentID;
		
		$thisKey = $thisMainSubjectID.'_'.$thisCH_SNAME;
		$SubjectComponentShortNameChIDAssoArr[$thisKey] = $thisSubjectComponentID;
	}
	
	
	
	$LogOutput .= "Main Subject Mapping Info (En): <br>\r\n";
	foreach ((array)$MainSubjectShortNameEnIDAssoArr as $thisCode => $thisID) {
		$LogOutput .= "$thisCode = $thisID <br>\r\n";
	}
	$LogOutput .= "<br>\r\n";
	
	$LogOutput .= "Main Subject Mapping Info (Ch): <br>\r\n";
	foreach ((array)$MainSubjectShortNameChIDAssoArr as $thisCode => $thisID) {
		$LogOutput .= "$thisCode = $thisID <br>\r\n";
	}
	$LogOutput .= "<br>\r\n";
	
	$LogOutput .= "Subject Component Mapping Info (En): <br>\r\n";
	foreach ((array)$SubjectComponentShortNameEnIDAssoArr as $thisCode => $thisID) {
		$LogOutput .= "$thisCode = $thisID <br>\r\n";
	}
	$LogOutput .= "<br>\r\n";
	
	$LogOutput .= "Subject Component Mapping Info (Ch): <br>\r\n";
	foreach ((array)$SubjectComponentShortNameChIDAssoArr as $thisCode => $thisID) {
		$LogOutput .= "$thisCode = $thisID <br>\r\n";
	}
	$LogOutput .= "<br>\r\n";
	
			
	# get all Academic Result records
	$ASSESSMENT_STUDENT_SUBJECT_RECORD = $eclass_db.".ASSESSMENT_STUDENT_SUBJECT_RECORD";
	$sql = "Select
					RecordID, SubjectCode, SubjectComponentCode
			From
					$ASSESSMENT_STUDENT_SUBJECT_RECORD
			Where
					(SubjectID Is Null Or SubjectID = '') And (SubjectComponentID Is Null Or SubjectComponentID = '')
			";
	$RecordInfoArr = $li->returnArray($sql);
	$numOfRecord = count($RecordInfoArr);
	
	$LogOutput .= "RecordID\tSubjectCode\tSubjectID\tSubjectComponentCode\tSubjectComponentID\tMappingStatus\tUpdateStatus<br>\r\n";
	$SuccessArr = array();
	$InvalidSubjectArr = array();
	for ($i=0; $i<$numOfRecord; $i++)
	{
		$thisValid = false;
		$thisRecordID = $RecordInfoArr[$i]['RecordID'];
		$thisSubjectCode = trim($RecordInfoArr[$i]['SubjectCode']);
		$thisSubjectComponentCode = trim($RecordInfoArr[$i]['SubjectComponentCode']);
		
		$thisSubjectID = $MainSubjectShortNameEnIDAssoArr[$thisSubjectCode];
		if ($thisSubjectID == '') {
			// if fail to map by eng short name => try to map by chinese short name
			$thisSubjectID = $MainSubjectShortNameChIDAssoArr[$thisSubjectCode];
		}
		
		if ($thisSubjectID == '') {
			// cannot map subject code
			$thisValid = false;
		}
		else {
			// can map subject code => map subject component if there are any.
			// if there are no component, it means the subjectID mapping of this record is completed
			
			if ($thisSubjectComponentCode == '') {
				$thisValid = true;
				$thisSubjectComponentID = 'null';
			}
			else {
				$thisKey = $thisSubjectID.'_'.$thisSubjectComponentCode;
				$thisSubjectComponentID = $SubjectComponentShortNameEnIDAssoArr[$thisKey];
				if ($thisSubjectComponentID == '') {
					// if fail to map by eng short name => try to map by chinese short name
					$thisSubjectComponentID = $SubjectComponentShortNameChIDAssoArr[$thisKey];
				}
				
				if ($thisSubjectComponentID == '') {
					$thisValid = false;
				}
				else {
					$thisValid = true;
				}
			}
		}
		
		if ($thisValid) {
			$SuccessArr['Mapping']['SuccessCount']++;
			$thisMappingStatusDisplay = '<font color="green">Success</font>';
			
			$sql = "Update $ASSESSMENT_STUDENT_SUBJECT_RECORD
						Set SubjectID = $thisSubjectID, SubjectComponentID = $thisSubjectComponentID
					Where
						RecordID = '".$thisRecordID."'
					";
			$SuccessArr['UpdateDB']['Info'][$thisRecordID] = $li->db_db_query($sql);
			
			if ($SuccessArr['UpdateDB']['Info'][$thisRecordID]) {
				$SuccessArr['UpdateDB']['SuccessCount']++;
				$thisUpdateStatusDisplay = '<font color="green">Success</font>';
			}
			else {
				$SuccessArr['UpdateDB']['FailedCount']++;
				$thisUpdateStatusDisplay = '<font color="red"><b>FAILED</b></font>';
			}
		}
		else {
			$SuccessArr['Mapping']['FailedCount']++;
			$thisMappingStatusDisplay = '<font color="red"><b>FAILED</b></font>';
			
			$thisUpdateStatusDisplay = '---';
			$InvalidSubjectArr[] = $thisSubjectCode.'_'.$thisSubjectComponentCode;
		}
		
		$LogOutput .= "$thisRecordID\t$thisSubjectCode\t$thisSubjectID\t$thisSubjectComponentCode\t$thisSubjectComponentID\t$thisMappingStatusDisplay\t$thisUpdateStatusDisplay<br>\r\n";
	}
	$LogOutput .= "<br>\r\n";
	
	if ($SuccessArr['Mapping']['SuccessCount'] == '') {
		$SuccessArr['Mapping']['SuccessCount'] = 0;
	}
	if ($SuccessArr['Mapping']['FailedCount'] == '') {
		$SuccessArr['Mapping']['FailedCount'] = 0;
	}
	if ($SuccessArr['UpdateDB']['SuccessCount'] == '') {
		$SuccessArr['UpdateDB']['SuccessCount'] = 0;
	}
	if ($SuccessArr['UpdateDB']['FailedCount'] == '') {
		$SuccessArr['UpdateDB']['FailedCount'] = 0;
	}
	$PatchResult = ($SuccessArr['UpdateDB']['SuccessCount'] == $numOfRecord)? "<font color='green'>Pass</font>" : "<font color='red'><b>FAILED</b></font>";
	
	$LogOutput .= "Total Number of Records = $numOfRecord <br>\r\n";
	$LogOutput .= "Number of Success Mapping Records = <font color='green'>".$SuccessArr['Mapping']['SuccessCount']."</font> <br>\r\n";
	$LogOutput .= "Number of Failed Mapping Records = <font color='red'><b>".$SuccessArr['Mapping']['FailedCount']."</b></font> <br>\r\n";
	$LogOutput .= "Number of Success DB Update Records = <font color='green'>".$SuccessArr['UpdateDB']['SuccessCount']."</font> <br>\r\n";
	$LogOutput .= "Number of Failed DB Update Records = <font color='red'><b>".$SuccessArr['UpdateDB']['FailedCount']."</b></font> <br>\r\n";
	$LogOutput .= "<br>\r\n";
	
	
	$InvalidSubjectArr = array_values(array_unique($InvalidSubjectArr));
	$numOfInvalidSubject = count($InvalidSubjectArr);
	if ($numOfInvalidSubject > 0) {
		$LogOutput .= "The following Subject Code cannot be mapped: <br>\r\n";
		
		for ($i=0; $i<$numOfInvalidSubject; $i++) {
			$LogOutput .= $InvalidSubjectArr[$i]."<br>\r\n";
		}
		
		$LogOutput .= "<br>\r\n";
	}
	
	
	$LogOutput .= "Patch Result = $PatchResult <br>\r\n";
	$LogOutput .= "<br>\r\n";
	
	# update General Settings - markd the script is executed
	$sql = "insert ignore into GENERAL_SETTING 
						(Module, SettingName, SettingValue, DateInput) 
					values 
						('$ModuleName', 'iPf_SubjectCode_SubjectID_Mapping', 1, now())";
	$SuccessArr['UpdateGeneralSettings'] = $li->db_db_query($sql);
	
	$LogOutput .= "iPortfolio - SubjectCode SubjectID Mapping Data Patch [End] ".date("Y-m-d H:i:s")."<br>";
	
	# show log
	echo $LogOutput;
	
	# save log
	$log_folder = $PATH_WRT_ROOT."file/iportfolio/log";
	if(!file_exists($log_folder))
		mkdir($log_folder);
	$log_file = $log_folder."/subjectcode_subjectid_mapping_log_". date("YmdHis") .".log";
	$lf = new libfilesystem();
	$lf->file_write(strip_tags($LogOutput), $log_file);
}
?>