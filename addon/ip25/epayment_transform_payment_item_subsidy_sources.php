<?php
// Editing by 
/*
 * 2013-10-22 (Carlos): Transform old payment item single subsidy source into new multiple subsidy source records
 */
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
intranet_opendb();

$li = new libdb();
$li->db = $intranet_db;
$lgs = new libgeneralsettings();

$ModuleName = "InitSetting";
$GSary = $lgs->Get_General_Setting($ModuleName);

if($plugin['payment'] && $GSary['ePaymentTransformPaymentItemSubsidySources']!=1)
{
	echo "================================================================================================<br>\r\n";
	echo "ePayment - Transforming payment item single subsidy source to multiple subsidy sources. [Start] <br>\r\n";
	echo "================================================================================================<br>\r\n";
	
	$result = array();
	// create table query is also added to sql_table_update.php
	$sql_create_table = "CREATE TABLE IF NOT EXISTS PAYMENT_PAYMENT_ITEM_SUBSIDY (
							PaymentID int(11) NOT NULL,
							UserID int(11) NOT NULL,
							SubsidyUnitID int(11) NOT NULL,
							SubsidyAmount double(20,2) NOT NULL,
							SubsidyPICAdmin varchar(255),
							SubsidyPICUserID int(11),
							DateInput datetime,
							DateModified datetime,
							InputBy int(11),
							ModifyBy int(11),
							UNIQUE KEY UniqueKey(PaymentID,UserID,SubsidyUnitID) 
						) ENGINE=InnoDB DEFAULT CHARSET=utf8";
	
	$result['CreateTable'] = $li->db_db_query($sql_create_table); 
	
	$sql = "SELECT PaymentID,ItemID,StudentID,SubsidyAmount,SubsidyUnitID,SubsidyPICUserID,SubsidyPICAdmin FROM PAYMENT_PAYMENT_ITEMSTUDENT WHERE SubsidyUnitID IS NOT NULL AND SubsidyUnitID!='' AND SubsidyAmount>0";
	$records = $li->returnResultSet($sql);
	$record_count = count($records);
	
	$sql_insert = "INSERT INTO PAYMENT_PAYMENT_ITEM_SUBSIDY 
					(PaymentID,UserID,SubsidyUnitID,SubsidyAmount,SubsidyPICAdmin,SubsidyPICUserID,DateInput,DateModified,InputBy,ModifyBy) VALUES ";
	$values = array();
	
	for($i=0;$i<$record_count;$i++) {
		
		$t_payment_id = $records[$i]['PaymentID'];
		$t_student_id = $records[$i]['StudentID'];
		$t_subsidy_amount = $records[$i]['SubsidyAmount'];
		$t_subsidy_unit_id = $records[$i]['SubsidyUnitID'];
		$t_subsidy_pic_user_id = $records[$i]['SubsidyPICUserID'];
		$t_subsidy_pic_admin = $li->Get_Safe_Sql_Query($records[$i]['SubsidyPICAdmin']);
		
		$values[] = "('$t_payment_id','$t_student_id','$t_subsidy_unit_id','$t_subsidy_amount','$t_subsidy_pic_admin','$t_subsidy_pic_user_id',NOW(),NOW(),NULL,NULL)";
	}
	
	$value_chunks = array_chunk($values,1000);
	$chunk_count = count($value_chunks);
	
	for($i=0;$i<$chunk_count;$i++) {
		
		$sql_do_insert = $sql_insert.implode(",",$value_chunks[$i]);
		//debug_r($sql_do_insert);
		$result['InsertChunk'.$i] = $li->db_db_query($sql_do_insert);
	}
	
	# update General Settings - markd the script is executed
	$sql = "insert ignore into GENERAL_SETTING 
						(Module, SettingName, SettingValue, DateInput) 
					values 
						('$ModuleName', 'ePaymentTransformPaymentItemSubsidySources', 1, now())";
	$result['UpdateGeneralSettings'] = $li->db_db_query($sql);
	
	echo "<br />";
	echo "<b>ePayment payment item source of subsidy transformation summary:</b><br />";
	echo "Total number of payment records involved: <b>".$record_count."</b><br />";
	echo "Number of records transformed: <b>".count($values)."</b><br />";
	echo "<br />";
	
	echo "==============================================================================================<br>\r\n";
	echo "ePayment - Transforming payment item single subsidy source to multiple subsidy sources. [End] <br>\r\n";
	echo "==============================================================================================<br>\r\n";
}

?>