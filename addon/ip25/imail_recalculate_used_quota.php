<?php
// Editing by 
// iMail - Recalculate all users used quota 
@SET_TIME_LIMIT(0);
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
intranet_opendb();

$li = new libdb();
$li->db = $intranet_db;
$lgs = new libgeneralsettings();

$ModuleName = "InitSetting";
$GSary = $lgs->Get_General_Setting($ModuleName);

if ($GSary['iMailRecalculateUsedQuota'] != 1 && !$plugin['imail_gamma']) {
	echo "#######################################################################################\n<br>";
	echo "################## iMail - Recalculating user used quota [Start] ######################\n<br>";
	echo "#######################################################################################\n<br>";
	
	$sql = "SELECT 
				c.UserID,SUM(c.AttachmentSize) as UsedQuota 
			FROM INTRANET_CAMPUSMAIL as c 
			INNER JOIN INTRANET_USER as u ON u.UserID = c.UserID 
			WHERE u.RecordStatus IN (0,1) AND u.RecordType IN (1,2,3) 
			GROUP BY c.UserID ";
	$userQuotaArray = $li->returnResultSet($sql);
	
	for($i=0;$i<sizeof($userQuotaArray);$i++){
		list($user_id, $used_quota) = $userQuotaArray[$i];
		$sql = "UPDATE INTRANET_CAMPUSMAIL_USED_STORAGE SET QuotaUsed = '$used_quota' WHERE UserID = '$user_id' ";
		$li->db_db_query($sql);
		
		if( $li->db_affected_rows() == 0) {
			$sql = "INSERT INTO INTRANET_CAMPUSMAIL_USED_STORAGE (UserID,QuotaUsed) VALUES ('$user_id','$used_quota')";
			$li->db_db_query($sql);
		}
		
		if( ($i % 20) == 0){
			echo " ";  // dummy output to prevent page timeout
			ob_flush();
			flush();
		}
	}
	
	# update General Settings - markd the script is executed
	$sql = "insert ignore into GENERAL_SETTING 
						(Module, SettingName, SettingValue, DateInput) 
					values 
						('$ModuleName', 'iMailRecalculateUsedQuota', 1, now())";
	$li->db_db_query($sql);
	
	echo "#######################################################################################\n<br>";
	echo "################## iMail - Recalculating user used quota [End] ########################\n<br>";
	echo "#######################################################################################\n<br>";
}
?>