<?
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

//intranet_auth();
intranet_opendb();

$li = new libdb();
		
###################################
# District Council
###################################
$sql = "select count(*) from PRESET_CODE_OPTION  where CodeType='DISTRICT'";
$result = $li->returnVector($sql);
if(!$result[0])
{
	$add_sql = "insert into PRESET_CODE_OPTION 
				(CodeType, Code, NameEng, NameChi, DisplayOrder, RecordType, RecordStatus, DateInput, InputBy, InputMethod)
				values
				('DISTRICT', 'A','Central & Western', '中西區', 1, 1, 1, now(), 0, 1),
				('DISTRICT', 'B','Wanchai', '灣仔', 2, 1, 1, now(), 0, 1),
				('DISTRICT', 'C','Eastern', '東區', 3, 1, 1, now(), 0, 1),
				('DISTRICT', 'D','Southern', '南區', 4, 1, 1, now(), 0, 1),
				('DISTRICT', 'E','Yau Tsim Mong', '油尖旺', 5, 1, 1, now(), 0, 1),
				('DISTRICT', 'F','Shum Shui Po', '深水埗', 6, 1, 1, now(), 0, 1),
				('DISTRICT', 'G','Kowloon City', '九龍城', 7, 1, 1, now(), 0, 1),
				('DISTRICT', 'H','Wong Tai Sin', '黃大仙', 8, 1, 1, now(), 0, 1),
				('DISTRICT', 'J','Kwun Tong', '觀塘', 9, 1, 1, now(), 0, 1),
				('DISTRICT', 'K','Tsuen Wan', '荃灣', 10, 1, 1, now(), 0, 1),
				('DISTRICT', 'L','Tuen Mun', '屯門', 11, 1, 1, now(), 0, 1),
				('DISTRICT', 'M','Yuen Long', '元朗', 12, 1, 1, now(), 0, 1),
				('DISTRICT', 'N','North', '北區', 13, 1, 1, now(), 0, 1),
				('DISTRICT', 'P','Tai Po', '大埔', 14, 1, 1, now(), 0, 1),
				('DISTRICT', 'Q','Sai Kung', '西貢', 15, 1, 1, now(), 0, 1),
				('DISTRICT', 'R','Shatin', '沙田', 16, 1, 1, now(), 0, 1),
				('DISTRICT', 'S','Kwai Tsing', '葵青', 17, 1, 1, now(), 0, 1),
				('DISTRICT', 'T','Islands', '離島', 18, 1, 1, now(), 0, 1),
				('DISTRICT', 'X','Inapplicable', '不適用', 19, 1, 1, now(), 0, 1)
				";
	$li->db_db_query($add_sql);
}


###################################
# Nationality
###################################
$sql = "select count(*) from PRESET_CODE_OPTION  where CodeType='NATIONALITY'";
$result = $li->returnVector($sql);
if(!$result[0])
{
	$add_sql = "insert into PRESET_CODE_OPTION 
				(CodeType, Code, NameEng, NameChi, DisplayOrder, RecordType, RecordStatus, DateInput, InputBy, InputMethod)
				values
				('NATIONALITY', '999','Others', '其他', 1, 1, 1, now(), 0, 1),
				('NATIONALITY', 'AUS','Australia', '澳洲', 2, 1, 1, now(), 0, 1),
				('NATIONALITY', 'AUT','Austria', '奧地利', 3, 1, 1, now(), 0, 1),
				('NATIONALITY', 'BCI','British Citizen', '英國公民', 4, 1, 1, now(), 0, 1),
				('NATIONALITY', 'BDT','British Dependent Territories Citizen', '英國屬土公民', 5, 1, 1, now(), 0, 1),
				('NATIONALITY', 'BNO','British National (Overseas)', '英國國民(海外)', 6, 1, 1, now(), 0, 1),
				('NATIONALITY', 'BOC','British Overseas Citizen', '英國海外公民', 7, 1, 1, now(), 0, 1),
				('NATIONALITY', 'BPP','British Protected Person', '英國保護人士', 8, 1, 1, now(), 0, 1),
				('NATIONALITY', 'BRA','Brazil', '巴西', 9, 1, 1, now(), 0, 1),
				('NATIONALITY', 'BSU','British Subject', '英國籍', 10, 1, 1, now(), 0, 1),
				('NATIONALITY', 'CAN','Canada', '加拿大', 11, 1, 1, now(), 0, 1),
				('NATIONALITY', 'CHI','China', '中國', 12, 1, 1, now(), 0, 1),
				('NATIONALITY', 'FRA','France', '法國', 13, 1, 1, now(), 0, 1),
				('NATIONALITY', 'GER','Germany', '德國', 14, 1, 1, now(), 0, 1),
				('NATIONALITY', 'IND','India', '印度', 15, 1, 1, now(), 0, 1),
				('NATIONALITY', 'JPN','Japan', '日本', 16, 1, 1, now(), 0, 1),
				('NATIONALITY', 'KOR','Korea', '韓國', 17, 1, 1, now(), 0, 1),
				('NATIONALITY', 'MYS','Malaysia', '馬來西亞', 18, 1, 1, now(), 0, 1),
				('NATIONALITY', 'NLD','Netherlands, The', '荷蘭', 19, 1, 1, now(), 0, 1),
				('NATIONALITY', 'NPL','Nepal', '尼泊爾', 20, 1, 1, now(), 0, 1),
				('NATIONALITY', 'NZL','New Zealand', '紐西蘭', 21, 1, 1, now(), 0, 1),
				('NATIONALITY', 'PAK','Pakistan', '巴基斯坦', 22, 1, 1, now(), 0, 1),
				('NATIONALITY', 'PHL','Philippines, The', '菲律賓', 23, 1, 1, now(), 0, 1),
				('NATIONALITY', 'PRT','Portugal', '葡萄牙', 24, 1, 1, now(), 0, 1),
				('NATIONALITY', 'SGP','Singapore', '新加坡', 25, 1, 1, now(), 0, 1),
				('NATIONALITY', 'SPA','Spain', '西班牙', 26, 1, 1, now(), 0, 1),
				('NATIONALITY', 'SWI','Switzerland', '瑞士', 27, 1, 1, now(), 0, 1),
				('NATIONALITY', 'THA','Thailand', '泰國', 28, 1, 1, now(), 0, 1),
				('NATIONALITY', 'TWN','Taiwan', '台灣', 29, 1, 1, now(), 0, 1),
				('NATIONALITY', 'USA','United States, The', '美國', 30, 1, 1, now(), 0, 1),
				('NATIONALITY', 'VNM','Vietnam', '越南', 31, 1, 1, now(), 0, 1)
				";
	$li->db_db_query($add_sql);
}

###################################
# Ethnicity
###################################
$sql = "select count(*) from PRESET_CODE_OPTION  where CodeType='ETHNICITY'";
$result = $li->returnVector($sql);
if(!$result[0])
{
	$add_sql = "insert into PRESET_CODE_OPTION 
				(CodeType, Code, NameEng, NameChi, DisplayOrder, RecordType, RecordStatus, DateInput, InputBy, InputMethod)
				values
				('ETHNICITY', 'CHI','Chinese', '華人', 1, 1, 1, now(), 0, 1),
				('ETHNICITY', 'BNG','Bangladeshi', '孟加拉人', 2, 1, 1, now(), 0, 1),
				('ETHNICITY', 'PHL','Filipino', '菲律賓人', 3, 1, 1, now(), 0, 1),
				('ETHNICITY', 'IND','Indian', '印度人', 4, 1, 1, now(), 0, 1),
				('ETHNICITY', 'IDN','Indonesian', '印尼人', 5, 1, 1, now(), 0, 1),
				('ETHNICITY', 'JPN','Japanese', '日本人', 6, 1, 1, now(), 0, 1),
				('ETHNICITY', 'KOR','Korean', '韓國人', 7, 1, 1, now(), 0, 1),
				('ETHNICITY', 'NPL','Nepalese', '尼泊爾人', 8, 1, 1, now(), 0, 1),
				('ETHNICITY', 'PAK','Pakistani', '巴基斯坦人', 9, 1, 1, now(), 0, 1),
				('ETHNICITY', 'SLK','Sri-Lankan', '斯里蘭卡人', 10, 1, 1, now(), 0, 1),
				('ETHNICITY', 'THA','Thai', '泰國人', 11, 1, 1, now(), 0, 1),
				('ETHNICITY', 'VNM','Vietnamese', '越南人', 12, 1, 1, now(), 0, 1),
				('ETHNICITY', 'ZAS','Other Asian', '其他亞洲人', 13, 1, 1, now(), 0, 1),
				('ETHNICITY', 'ZWH','White', '白人', 14, 1, 1, now(), 0, 1),
				('ETHNICITY', 'ZBK','Black', '黑人', 15, 1, 1, now(), 0, 1),
				('ETHNICITY', '999','Other ethnicities not classified above', '其他以上沒有分類的種族', 16, 1, 1, now(), 0, 1),
				('ETHNICITY', 'INA','Information not available', '沒有資料', 17, 1, 1, now(), 0, 1)
				";
	$li->db_db_query($add_sql);
}
		
		
###################################
# Ident. Doc Type
###################################
$sql = "select count(*) from PRESET_CODE_OPTION  where CodeType='IDENT_DOC_TYPE'";
$result = $li->returnVector($sql);
if(!$result[0])
{
	$add_sql = "insert into PRESET_CODE_OPTION 
				(CodeType, Code, NameEng, NameChi, DisplayOrder, RecordType, RecordStatus, DateInput, InputBy, InputMethod)
				values
				('IDENT_DOC_TYPE', '02','Passport', '護照', 1, 1, 1, now(), 0, 1),
				('IDENT_DOC_TYPE', '03','Re-entry Permit', '回港證', 2, 1, 1, now(), 0, 1),
				('IDENT_DOC_TYPE', '04','Certificate of Identity', '身份證明書', 3, 1, 1, now(), 0, 1),
				('IDENT_DOC_TYPE', '05','Document of Identity', '簽證身份書', 4, 1, 1, now(), 0, 1),
				('IDENT_DOC_TYPE', '06','Entry Permit (Dependent for N months)', '入境許可證(家屬)', 5, 1, 1, now(), 0, 1),
				('IDENT_DOC_TYPE', '07','Declaration of ID for Visa purpose', '簽證身份陳述書', 6, 1, 1, now(), 0, 1),
				('IDENT_DOC_TYPE', '08','One-way Permit', '單程證', 7, 1, 1, now(), 0, 1),
				('IDENT_DOC_TYPE', '09','Others', '其他', 8, 1, 1, now(), 0, 1)
				";
	$li->db_db_query($add_sql);
}		


###################################
# Religion
###################################
$sql = "select count(*) from PRESET_CODE_OPTION  where CodeType='RELIGION'";
$result = $li->returnVector($sql);
if(!$result[0])
{
	$add_sql = "insert into PRESET_CODE_OPTION 
				(CodeType, Code, NameEng, NameChi, DisplayOrder, RecordType, RecordStatus, DateInput, InputBy, InputMethod)
				values
				('RELIGION', '0','Not Applicable', '不適用', 1, 1, 1, now(), 0, 1),
				('RELIGION', '1','Catholicism', '天主教', 2, 1, 1, now(), 0, 1),
				('RELIGION', '2','Protestant/Christian', '基督教', 3, 1, 1, now(), 0, 1),
				('RELIGION', '3','Buddhism', '佛教', 4, 1, 1, now(), 0, 1),
				('RELIGION', '4','Taoism', '道教', 5, 1, 1, now(), 0, 1),
				('RELIGION', '5','Confucianism', '孔教', 6, 1, 1, now(), 0, 1),
				('RELIGION', '6','Islam', '伊斯蘭教', 7, 1, 1, now(), 0, 1),
				('RELIGION', '7','Hinduism', '印度教', 8, 1, 1, now(), 0, 1),
				('RELIGION', '9','Others', '其他', 9, 1, 1, now(), 0, 1)
				";
	$li->db_db_query($add_sql);
}		


###################################
# Spoken Language at Home
###################################
$sql = "select count(*) from PRESET_CODE_OPTION  where CodeType='FAMILY_LANG'";
$result = $li->returnVector($sql);
if(!$result[0])
{
	$add_sql = "insert into PRESET_CODE_OPTION 
				(CodeType, Code, NameEng, NameChi, DisplayOrder, RecordType, RecordStatus, DateInput, InputBy, InputMethod)
				values
				('FAMILY_LANG', 'CHI','Chinese', '華語', 1, 1, 1, now(), 0, 1),
				('FAMILY_LANG', 'ENG','English', '英語', 2, 1, 1, now(), 0, 1),
				('FAMILY_LANG', 'BNG','Bengali', '孟加拉語', 3, 1, 1, now(), 0, 1),
				('FAMILY_LANG', 'DUT','Dutch', '荷蘭語', 4, 1, 1, now(), 0, 1),
				('FAMILY_LANG', 'PHL','Filipino', '菲律賓語', 5, 1, 1, now(), 0, 1),
				('FAMILY_LANG', 'FRE','French', '法語', 6, 1, 1, now(), 0, 1),
				('FAMILY_LANG', 'GER','German', '德語', 7, 1, 1, now(), 0, 1),
				('FAMILY_LANG', 'HND','Hindi', '印地語(印度)', 8, 1, 1, now(), 0, 1),
				('FAMILY_LANG', 'IDN','Indonesian', '印尼語', 9, 1, 1, now(), 0, 1),
				('FAMILY_LANG', 'IRI','Irish', '愛爾蘭語', 10, 1, 1, now(), 0, 1),
				('FAMILY_LANG', 'ITA','Italian', '意大利語', 11, 1, 1, now(), 0, 1),
				('FAMILY_LANG', 'JPN','Japanese', '日本語', 12, 1, 1, now(), 0, 1),
				('FAMILY_LANG', 'KOR','Korean', '韓國語', 13, 1, 1, now(), 0, 1),
				('FAMILY_LANG', 'NPL','Nepali', '尼泊爾語', 14, 1, 1, now(), 0, 1),
				('FAMILY_LANG', 'PRT','Portuguese', '葡萄牙語', 15, 1, 1, now(), 0, 1),
				('FAMILY_LANG', 'RUS','Russian', '俄語(俄國)', 16, 1, 1, now(), 0, 1),
				('FAMILY_LANG', 'SNH','Sinhalese', '斯里蘭卡語', 17, 1, 1, now(), 0, 1),
				('FAMILY_LANG', 'SPA','Spanish', '西班牙語', 18, 1, 1, now(), 0, 1),
				('FAMILY_LANG', 'THA','Thai', '泰語(泰國)', 19, 1, 1, now(), 0, 1),
				('FAMILY_LANG', 'URD','Urdu', '烏爾都語', 20, 1, 1, now(), 0, 1),
				('FAMILY_LANG', 'VNM','Vietnamese', '越南語', 21, 1, 1, now(), 0, 1),
				('FAMILY_LANG', 'ZAS','Other Asian and Oceanian languages', '其他亞洲及大洋洲語', 22, 1, 1, now(), 0, 1),
				('FAMILY_LANG', 'ZEU','Other European language', '其他歐洲語言', 23, 1, 1, now(), 0, 1),
				('FAMILY_LANG', '999','Other languages not classified above', '其他以上沒有分類的語言', 24, 1, 1, now(), 0, 1),
				('FAMILY_LANG', 'INA','Information not available', '沒有資料', 25, 1, 1, now(), 0, 1)
				";
	$li->db_db_query($add_sql);
}

		
//intranet_closedb();		
?>