<?php
// editing by 
/**************************************
 * Created on 2013-02-07 by Carlos 
 **************************************/
@SET_TIME_LIMIT(0);
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");
intranet_opendb();

$li = new libinventory();
$li->db = $intranet_db;
$lgs = new libgeneralsettings();

$ModuleName = "InitSetting";
$GSary = $lgs->Get_General_Setting($ModuleName);

if ($plugin['Inventory'] && $GSary['eInventoryBarcodeForAdminGroupFundingSource']!=1) {
	
	echo "#######################################################################################\n<br>";
	echo "### eInventory - Generate barcodes fro Admin groups and Funding sources [Start] #######\n<br>";
	echo "#######################################################################################\n<br>";
	
	$schema_query = array();
	$schema_query[] = "ALTER TABLE INVENTORY_ADMIN_GROUP ADD COLUMN Barcode varchar(100) DEFAULT NULL AFTER NameEng";
	$schema_query[] = "ALTER TABLE INVENTORY_ADMIN_GROUP ADD INDEX InventoryAdminGroupBarcode (Barcode)";
	
	$schema_query[] = "ALTER TABLE INVENTORY_FUNDING_SOURCE ADD COLUMN Barcode varchar(100) DEFAULT NULL AFTER NameEng";
	$schema_query[] = "ALTER TABLE INVENTORY_FUNDING_SOURCE ADD INDEX FundingBarcode(Barcode)";
	
	$schema_query[] = "ALTER TABLE INVENTORY_ITEM_BULK_EXT ADD COLUMN Barcode varchar(100) DEFAULT NULL";
	$schema_query[] = "ALTER TABLE INVENTORY_ITEM_BULK_EXT ADD INDEX BulkBarcode(Barcode)";
	
	foreach($schema_query as $k => $q) {
		$li->db_db_query($q);
	}
	
	$sql = "SELECT AdminGroupID FROM INVENTORY_ADMIN_GROUP WHERE Barcode IS NULL OR Barcode=''";
	$admin_groups = $li->returnVector($sql);
	$num_of_group = count($admin_groups);
	
	$group_barcode_ary = $li->getUniqueBarcode($num_of_group, "group");
	$group_result = array("success"=>0,"fail"=>0);
	for($i=0;$i<$num_of_group;$i++) {
		$sql = "UPDATE INVENTORY_ADMIN_GROUP SET Barcode='".$group_barcode_ary[$i]."' WHERE AdminGroupID='".$admin_groups[$i]."'";
		$success = $li->db_db_query($sql);
		if($success) {
			$group_result["success"]++;
		}else{
			$group_result["fail"]++;
		}
	}
	
	echo "############### ".$group_result["success"]." Resources Mgmt Groups has been updated successfully. #################\n<br>";
	echo "############### ".$group_result["fail"]." Resources Mgmt Groups update failed. ####################################\n<br>";
	
	
	$sql = "SELECT FundingSourceID FROM INVENTORY_FUNDING_SOURCE WHERE Barcode IS NULL OR Barcode=''";
	$funding_sources = $li->returnVector($sql);
	$num_of_funding = count($funding_sources);
	
	$funding_barcode_ary = $li->getUniqueBarcode($num_of_funding, "funding");
	$funding_result = array("success"=>0,"fail"=>0);
	for($i=0;$i<$num_of_funding;$i++) {
		$sql = "UPDATE INVENTORY_FUNDING_SOURCE SET Barcode='".$funding_barcode_ary[$i]."' WHERE FundingSourceID='".$funding_sources[$i]."'";
		$success = $li->db_db_query($sql);
		if($success) {
			$funding_result["success"]++;
		}else{
			$funding_result["fail"]++;
		}
	}
	
	echo "############### ".$funding_result["success"]." Funding Sources has been updated successfully. #################\n<br>";
	echo "############### ".$funding_result["fail"]." Funding Sources update failed. ####################################\n<br>";
	
	$sql = "SELECT ItemID FROM INVENTORY_ITEM_BULK_EXT WHERE Barcode IS NULL OR Barcode=''";
	$bulk_items = $li->returnVector($sql);
	$num_of_item = count($bulk_items);
	
	$item_barcode_ary = $li->getUniqueBarcode($num_of_item, "item");
	$item_result = array("success"=>0,"fail"=>0);
	for($i=0;$i<$num_of_item;$i++) {
		$sql = "UPDATE INVENTORY_ITEM_BULK_EXT SET Barcode='".$item_barcode_ary[$i]."' WHERE ItemID='".$bulk_items[$i]."'";
		$success = $li->db_db_query($sql);
		if($success) {
			$item_result["success"]++;
		}else{
			$item_result["fail"]++;
		}
	}
	
	echo "############### ".$item_result["success"]." bulk items has been updated successfully. #################\n<br>";
	echo "############### ".$item_result["fail"]." bulk items update failed. ####################################\n<br>";
	
	# update General Settings - markd the script is executed
	$sql = "insert ignore into GENERAL_SETTING 
						(Module, SettingName, SettingValue, DateInput) 
					values 
						('$ModuleName', 'eInventoryBarcodeForAdminGroupFundingSource', 1, now())";
	$li->db_db_query($sql);
	
	echo "#######################################################################################\n<br>";
	echo "### eInventory - Generate barcodes fro Admin groups and Funding sources [End]   #######\n<br>";
	echo "#######################################################################################\n<br>";
}
?>