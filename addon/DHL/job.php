<?php
// Editing by 
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcrontab.php");
include_once($PATH_WRT_ROOT."includes/DHL/libdhl.php");

include("../check.php");

intranet_opendb();

$libdhl = new libdhl();
$crontab = new libcrontab();

$task = strtolower($_REQUEST['task']);

$site = (checkHttpsWebProtocol()?"https":"http")."://".$_SERVER['SERVER_NAME'].($_SERVER["SERVER_PORT"]!= 80?":".$_SERVER["SERVER_PORT"]:"");
$script = $site."/schedule_task/dhl.php";

switch ($task)
{
	case "listjob":
		$jobs = $crontab->getAllJobs();
		echo "<PRE>";
		print_r($jobs);
		echo "</PRE>";
	break;
	
	case "setjob":
		$hour = $_REQUEST['hour'];
		$minute = $_REQUEST['minute'];
		$crontab->removeJob($script);
		$success = $crontab->setJob($minute, $hour, '*', '*', '*', $script);
		echo "Job setup ".($success?"<span style=\"color:green\">SUCCESS</span>":"<span style=\"color:red\">FAILED</span>").".";
	break;
	
	case "removejob":
		$crontab->removeJob($script);
		echo "Job removed.";
	break;
}

intranet_closedb();
?>